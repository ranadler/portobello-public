! Note  - Originally copied from Sang's code.
!         this is  DEPRACATED - we changed the way we expand orbitals - they are not resolved on
!         k anymore. But the code should be much simpler - it should just use the real space rep of the
!         MT orbitals.
!         Also the next generation should use VTK directly.


module visualize_orbitals






contains

    subroutine indexing(ntot,ndivision,divisionarray,flag,n1,n2)
        use parallel_mod
        use solid_mod
        implicit none
        integer, intent(in) :: ntot,ndivision,&
            divisionarray(ndivision),flag
        integer, intent(inout) :: n1, n2(ndivision)
        integer :: ii,jj,tempcnt,tmpsize

        tmpsize=1
        do ii=1, ndivision
            tmpsize=tmpsize*divisionarray(ii)
        enddo
        if (tmpsize .ne.  ntot) then
            if (maswrk) then
                write(993,*) 'array_division wrong'
                write(993,'(100(i10,2x))') tmpsize, ntot,&
                    divisionarray
                call flush(993)
            endif
            write(179,*) 'array_division wrong'
            write(179,'(100(i10,2x))') tmpsize, ntot,&
                divisionarray
            call ending
        endif
        if (flag .eq. 1) then
            n1=n2(1)
            do ii=2, ndivision
                tempcnt=1
                do jj=1, ii-1
                    tempcnt=tempcnt*divisionarray(jj)
                enddo
                n1=n1+(n2(ii)-1)*tempcnt
            enddo
        else
            n2=0
            tempcnt=n1
            do ii=1, ndivision-1
                n2(ii)=tempcnt-((tempcnt-1)/divisionarray(ii))&
                    *divisionarray(ii)
                tempcnt=(tempcnt-n2(ii))/divisionarray(ii)+1
            enddo
            n2(ndivision)=tempcnt
        endif
    end subroutine



    subroutine index_shift(vec,dimmat, outvec)

        !$$$  from 1,2,3,4,5,6-> 0,1,2,-3,-2,-1
        !$$$  from 1,2,3,4,5 -> 0,1,2,-2,-1
        implicit none

        integer, intent(in) :: vec(3), dimmat(3)
        integer, intent(out) :: outvec(3)

        integer :: n_crit,ii

        outvec=0

        do ii=1, 3
            n_crit=dimmat(ii)/2
            if ((dimmat(ii)-n_crit*2) .ne. 0) then
                n_crit=n_crit+1
            endif
            if (vec(ii) .le. n_crit) then
                outvec(ii)=vec(ii)-1
            else
                outvec(ii)=vec(ii)-dimmat(ii)-1
            endif
        enddo
    end subroutine




    subroutine fftshift3d(Nfft,fftboxx,fftboxxout)
        implicit none
        integer, intent(in) :: Nfft(3)
        complex*16, dimension(Nfft(1),Nfft(2),Nfft(3)), intent(in)&
            :: fftboxx
        complex*16, dimension(Nfft(1),Nfft(2),Nfft(3)), intent(out)&
            :: fftboxxout
        integer :: ii, jj, kk

        fftboxxout=0.0d0

        do ii=1, Nfft(1)
            do jj=1, Nfft(2)
                do kk=1, Nfft(3)
                    if (ii .le. (Nfft(1)+1)/2) then
                        if (jj .le. (Nfft(2)+1)/2) then
                            if (kk .le. (Nfft(3)+1)/2) then
                                fftboxxout(ii+nfft(1)/2, jj+nfft(2)/2, kk+nfft(3)/2)&
                                    =fftboxx(ii, jj, kk)
                            else
                                fftboxxout(ii+nfft(1)/2, jj+nfft(2)/2,&
                                    kk-(nfft(3)+1)/2)&
                                    =fftboxx(ii, jj, kk)
                            end if
                        else
                            if (kk .le. (Nfft(3)+1)/2) then
                                fftboxxout(ii+nfft(1)/2, jj-(nfft(2)+1)/2,&
                                    kk+nfft(3)/2)&
                                    =fftboxx(ii, jj, kk)
                            else
                                fftboxxout(ii+nfft(1)/2, jj-(nfft(2)+1)/2,&
                                    kk-(nfft(3)+1)/2)&
                                    =fftboxx(ii, jj, kk)
                            end if
                        end if
                    else
                        if (jj .le. (Nfft(2)+1)/2) then
                            if (kk .le. (Nfft(3)+1)/2) then
                                fftboxxout(ii-(nfft(1)+1)/2, jj+nfft(2)/2,&
                                    kk+nfft(3)/2)&
                                    =fftboxx(ii, jj, kk)
                            else
                                fftboxxout(ii-(nfft(1)+1)/2, jj+nfft(2)/2,&
                                    kk-(nfft(3)+1)/2)&
                                    =fftboxx(ii, jj, kk)
                            end if
                        else
                            if (kk .le. (Nfft(3)+1)/2) then
                                fftboxxout(ii-(nfft(1)+1)/2,&
                                    jj-(nfft(2)+1)/2, kk+nfft(3)/2)&
                                    =fftboxx(ii, jj, kk)
                            else
                                fftboxxout(ii-(nfft(1)+1)/2, jj-(nfft(2)+1)/2,&
                                    kk-(nfft(3)+1)/2)=fftboxx(ii, jj, kk)
                            end if
                        end if
                    end if
                end do
            end do
        end do

    end subroutine


    subroutine mt_pw_projection(pw_k,ispin,bvec,outvec)
        use parallel_mod
        use manager_mod
        use solid_mod
        use atom_mod
        implicit none
        integer, intent(in) :: ispin
        double precision, intent(in) :: pw_k(3), bvec(3)
        complex*16 :: outvec(nfun,nrel)

        integer :: iatom,ind0,isort,nl,ndimb,ir,&
            ml1,ml2,l1,i1,mj1,li1,nm1,in1,km1,km2,lf1,mt,&
            limu1,ie1,jn1, mdim,m1,m2,ss,lm
        double precision :: pi,rv,mulmu,cmat1(2,2),&
            bess(0:maxb,0:maxnrad),bessd(0:maxb),norm_k_b_c,&
            unit_k_b(3),work(0:maxnrad),ylm((maxb+1)**2),&
            radint,pw_k_c(3),bvec_c(3), theta, phi, &
            jval,mval,j1val,m1val,j2val,m2val
        complex*16 :: ai,c1, cmat2(2,2)
        integer, external :: ind2jm, jm2ind,lmindex,lget
        double precision, external :: dqdall, clebschg, spharmonics

        !     bvec in the unit of reciprocal lattice vector
        !     pw_k in the unit of reciprocal lattice vector



        outvec=0.0d0
        !$$$  if (maswrk) then
        !$$$  write(iun,*) 'amega', amega
        !$$$  endif

        ai=dcmplx(0.0d0,1.0d0)
        pi=datan2(1.0d0,1.0d0)*4.0d0


        ylm=0.0d0

        pw_k_c=matmul(gbas,pw_k)*2.0d0*pi/par
        bvec_c=matmul(gbas,bvec)*2.0d0*pi/par

        norm_k_b_c=dsqrt(sum((pw_k_c+bvec_c)**2))

        if(norm_k_b_c.gt.1.0d-9) then
            unit_k_b=(pw_k_c+bvec_c)/norm_k_b_c
            call sphharm(unit_k_b(1),unit_k_b(2),unit_k_b(3),&
                maxb,ylm)
        else
            ylm=0.d0
            ylm(1)=1.0d0/dsqrt(4.0d0*pi)
        endif

        outvec=0.0d0
        do iatom=1,natom
            ind0=io_lem(iatom)-1    ! the starting value of the {n,l,i,mu} f
            isort=is(iatom)
            nl=lfunm(isort)
            ndimb=nrel*(lmb(isort)+1)**2 ! dimemsion of the angular part
            c1=4.0d0*pi*cdexp(-ai*sum((pw_k_c+bvec_c)*tau(:,iatom)*par))&
                /dsqrt(amega)/dsqrt(dble(nqdiv))
            do ir=0,nrad(isort)     ! bessel function
                rv=r(ir,isort)
                call BESSR(norm_k_b_c,rv,bess(0,ir),bessd(0),&
                    lmb(isort))
            enddo
            do limu1=1,ndimb        ! {l,i,mu}
                if(irel<=1) then
                    l1=lget(limu1)
                else
                    call getlimj(limu1,l1,i1,mj1,li1,0)
                endif
                nm1=ntle(l1,isort)    ! radial function index dimension
                do ie1=1,nm1          ! {ie1,jn1} index for radial functions
                    in1=1
                    if(augm(ie1,l1,isort)/='LOC') in1=2
                    do jn1=1,in1
                        km1=indbasa(jn1,ie1,limu1,isort)
                        km2=ind0+km1
                        lf1=lf_isz(km1,isort) ! radial function index
                        mt=ind_wf(lf1,isort) ! starting index for the radial grid              
          
                        work=0.0d0
                        do ir=0,nrad(isort)
                          rv=r(ir,isort)                  
                          work(ir)=work(ir) +gfun(mt+ir,ispin)*bess(l1,ir)*rv**2*dr(ir,isort)
                        enddo
                        radint=dqdall(h(isort),work,nrad(isort))                              
                        if (irel .le. 1) then
                          outvec(km2,1)=outvec(km2,1)+c1*(-1.0d0*ai)**l1*radint*ylm(limu1)
                        else
                            do ss=1, 2
                                jval=l1+i1/2.0d0
                                mval=mj1/2.0d0
            
                                j2val=1.0d0/2.0d0
                                m2val=((ss-1)*2-1)/2.0d0
            
                                lm=nint(mval-m2val)
                                j1val=dble(l1)
                                m1val=dble(lm)
            
                                if (abs(lm) .le. l1) then
                                outvec(km2,ss)=outvec(km2,ss) &
                                +c1*(-1.0d0*ai)**l1*radint &
                                *spharmonics(l1,lm,theta,phi) &
                                *clebschG(jval,mval,j1val,m1val,j2val,m2val)
                                endif
                            enddo
                        endif
                    enddo
                enddo
            enddo
        enddo
    end subroutine


    subroutine wan_r0_fft(wout, pw_k,ispin,wan_fft)
        use wannier
        use atom_mod
        use solid_mod
        use manager_mod
        implicit none

        type(WannierOutput),intent(in),pointer :: wout
        integer, intent(in) :: ispin
        double precision, intent(in) :: pw_k(3)

        complex*16,intent(out) ::wan_fft(wout%num_orbs, nrel)

        integer :: iq,ii,iwan, irl,q0, ibas1
        double precision :: zerovec(3),kshift(3)
        complex*16 :: outvec_mt(nfun,nrel), outvec_it(nbasmpw), &
         zwan(nfun, wout%num_orbs), awan(nbasmpw,wout%num_orbs)

        wan_fft=0.0d0
        outvec_mt=0.0d0

        zerovec=0.0d0

        call mt_pw_projection(pw_k,ispin,zerovec,outvec_mt)

        do iq=1, nqdiv
            q0=i_kref(iq)
            kshift=matmul(transpose(rbas),pnt(:,iq))-pw_k
            outvec_it=0.0d0
            if (&
                    (dabs(kshift(1)-nint(kshift(1))).gt.1.0d0/dble(ndiv(1))/100d0)&
                    .or.&
                    (dabs(kshift(2)-nint(kshift(2))).gt.1.0d0/dble(ndiv(2))/100d0)&
                    .or.&
                    (dabs(kshift(3)-nint(kshift(3))).gt.1.0d0/dble(ndiv(3))/100d0)&
                    ) then
                cycle
            endif

            call it_pw_projection(iq,pw_k,ispin,zerovec,&
                outvec_it)

                
            call zgemm('n','n',nfun,wout%num_orbs,wout%expansion%num_expanded,(1.0d0,0.0d0), &
                wout%expansion%Z_(1,1,iq, ispin),nfun,wout%P_(1,1,iq,ispin),wout%expansion%num_expanded, &
                (0.0d0,0.0d0),zwan(1,1),nfun)
            call zgemm('n','n',nbasmpw,wout%num_orbs,wout%expansion%num_expanded,(1.0d0,0.0d0), &
                wout%expansion%A_(1,1,iq, ispin),nbasmpw,wout%P_(1,1,iq,ispin),wout%expansion%num_expanded, &
                (0.0d0,0.0d0),awan(1,1),nbasmpw)
            

            do iwan=1, wout%num_orbs        
                do ii=1, nfun
                    do irl=1, nrel
                        wan_fft(iwan,irl) = wan_fft(iwan,irl) + outvec_mt(ii,irl)*zwan(ii,iwan)
                    enddo
                enddo
            enddo
                
            !print *, "z", wan_fft(1,1), wan_fft(1,2), outvec_mt(1,1), outvec_mt(1,2), zwan(1,1)

            do iwan=1, wout%num_orbs                
                do irl=1, nrel
                    ibas1=nbask(q0)/nrel*(irl-1)            
                    do ii=1, nbask(q0)/nrel
                        wan_fft(iwan,irl) = wan_fft(iwan,irl) + outvec_it(ibas1+ii)*awan(ibas1+ii,iwan)
                    enddo
                enddo
            enddo

            !print *, "a", wan_fft(1,1), wan_fft(1,2), outvec_it(1), outvec_it(1+nbask(q0)/nrel), awan(1,1), awan(1+nbask(q0)/nrel,1)

        enddo
    end subroutine


    subroutine it_pw_projection(kk,pw_k,ispin,bvec,outvec)

        use parallel_mod
        use manager_mod
        use solid_mod
        implicit none
        integer, intent(in) :: ispin, kk
        double precision, intent(in) :: pw_k(3), bvec(3)
        complex*16 :: outvec(nbasmpw)

        integer :: k0,ibas,ind,ia,ib,ic,j,ibas1, irl, ibas0

        double precision :: kshift(3)
        complex*16 :: ai,c1


        !     bvec in the unit of reciprocal lattice vector
        !     pw_k in the unit of reciprocal lattice vector

        !     normalization of the plane wave
        outvec=0.0d0

        k0=i_kref(kk)

        kshift=matmul(transpose(rbas),pnt(:,kk))-(pw_k+bvec)

        do irl=1, nrel
            ibas0=nbask(k0)/nrel*(irl-1)  
            do ibas=1,nbask(k0)/nrel
                ind=indgb(ibas,kk)
                ia=igbs(1,ind)+nint(kshift(1))
                ib=igbs(2,ind)+nint(kshift(2))
                ic=igbs(3,ind)+nint(kshift(3))
                j=indplw(ia,ib,ic)
                if (&
                        (ia.lt.-maxia) .or. (ia.gt.maxia).or.&
                        (ib.lt.-maxib) .or. (ib.gt.maxib).or.&
                        (ic.lt.-maxic) .or. (ic.gt.maxic).or.&
                        (j .le. 0).or. (j .gt. numplw)&
                        ) then
                    cycle
                endif
                if(complex_ro) then
                    outvec(ibas0+ibas)=dcmplx(sovr(j),sovi(j))&
                        /dsqrt(dble(nqdiv))
                else
                    outvec(ibas0+ibas)=dcmplx(sovr(j),0.0d0)&
                        /dsqrt(dble(nqdiv))
                endif
            enddo
        enddo

    end subroutine


    function WriteXSFImp(selfpath) result(str)
        use stringifor
        use wannier
        use solid_mod, only: nrdiv
        type(string) :: str
        type(string), intent(in) :: selfpath
        type(WannierOutput),target :: wout
        type(wannierOutput),pointer :: woutp

        woutp => wout
        print *, "write XSF loading output from: " // selfpath
        call wout%load(selfpath%chars())
        print *, "calling wannier_realgrid", wout%max_band- wout%min_band + 1, wout%num_orbs
        call wannier_realgrid(woutp, nrdiv, 1)

    end function


    subroutine wannier_realgrid(wout, rrdiv,ispin)
        use manager_mod
        use atom_mod
        use solid_mod
        !use parallel_mod
        use units_mod
        use wannier
        use penf_stringify, only: str


        implicit none

        type(WannierOutput),intent(in),pointer :: wout
        integer, intent(in) :: ispin,rrdiv(3)
        complex*16, allocatable:: wan_rgrid(:,:,:,:,:)
        integer :: irk,iband1,iband2,iq,&
            ind_gg,ind6(6),&
            ind3(3),ind3_shifted(3),pw_s,&
            iwan,i1,i2,i3,ntot,dim3(3),ii,jj,tmp, ir
        double precision :: tempdble1,tempdble2,pw_k(3),fac,&
            tempval,wan_norm_r(wout%num_orbs),&
            wan_center_r(3,wout%num_orbs),&
            wan_spreading_r(wout%num_orbs),&
            wan_r2,r2,rvec(3)

        complex*16 :: &
            wan_tmp(wout%num_orbs, nrel)

        complex*16,allocatable :: wan_out(:,:,:,:),wan_kgrid(:,:,:,:,:)

        type(string) :: orbital_name

        character :: ch
        character*10 :: ch_spin

        integer :: counter


        counter = 0


        allocate(wan_out(rrdiv(1)*ndiv(1),rrdiv(2)*ndiv(2),rrdiv(3)*ndiv(3), nrel))
        allocate(wan_kgrid(rrdiv(1)*ndiv(1),rrdiv(2)*ndiv(2),rrdiv(3)*ndiv(3),nrel, wout%num_orbs))

        allocate(wan_rgrid(rrdiv(1)*ndiv(1),rrdiv(2)*ndiv(2),rrdiv(3)*ndiv(3),nrel, wout%num_orbs))

        ntot=ndiv(1)*ndiv(2)*ndiv(3)*rrdiv(1)*rrdiv(2)*rrdiv(3)
        dim3=(/ndiv(1)*rrdiv(1),ndiv(2)*rrdiv(2),ndiv(3)*rrdiv(3)/)


        fac=1.0/dsqrt(amega)/dsqrt(dble(nqdiv))
        wan_rgrid=0.0d0
        wan_kgrid=0.0d0
        do ind_gg=1, ntot
            tmp = ind_gg
            call indexing(ntot,3,dim3,-1,tmp,ind3)
            call index_shift(ind3,dim3,ind3_shifted)
            pw_k=dble(ind3_shifted)/dble(ndiv)
            wan_tmp=0.0d0
            call wan_r0_fft(wout, pw_k,ispin, wan_tmp)
            do iwan=1, wout%num_orbs
                do ii=1, nrel
                    wan_kgrid(ind3(1),ind3(2),ind3(3),ii,iwan) = wan_tmp(iwan, ii)*fac
                enddo
            enddo
        enddo

        wan_rgrid(:,:,:,:,:)=wan_kgrid(:,:,:,:,:)

        call fft3(dim3(1),dim3(2),dim3(3),wout%num_orbs*nrel,wan_rgrid,1)

        do iwan=1, wout%num_orbs
            wan_norm_r=0.0d0
            wan_center_r=0.0d0
            wan_spreading_r=0.0d0             
            do ir=1, nrel
                call fftshift3d(dim3,wan_rgrid(1,1,1,ir,iwan),wan_out(1,1,1,ir))
                !     normalization test
                wan_norm_r(iwan)=sum(cdabs(wan_rgrid(:,:,:,ir,iwan))**2)&
                    *amega/dble(rrdiv(1)*rrdiv(2)*rrdiv(3))
                !     center
                wan_r2=0.0d0
                do i1=1, dim3(1)
                    do i2=1, dim3(2)
                        do i3=1, dim3(3)
                            ind3=(/i1,i2,i3/)
                            call index_shift(ind3,dim3,ind3_shifted)
                            rvec=matmul(rbas, ind3_shifted)/dble(rrdiv)*par*bohr
                            r2=sum((rvec-wout%centers_(:,iwan))**2)

                            do ii=1,3
                                wan_center_r(ii,iwan)=wan_center_r(ii,iwan)&
                                    +rvec(ii)*cdabs(wan_rgrid(i1,i2,i3,ir,iwan))**2&
                                    *amega/dble(rrdiv(1)*rrdiv(2)*rrdiv(3))
                            enddo
                            wan_r2=wan_r2&
                                +r2*cdabs(wan_rgrid(i1,i2,i3,ir,iwan))**2&
                                *amega/dble(rrdiv(1)*rrdiv(2)*rrdiv(3))
                        enddo
                    enddo
                enddo
            enddo


            do ir=1, nrel
                if (nrel .eq. 1) then
                  write(ch_spin,'(a)') ''      
                else
                  if (ir .eq. 1) then
                    write(ch_spin,'(a)') 'dn'
                  else
                    write(ch_spin,'(a)') 'up'              
                  endif
                endif

                orbital_name = trim(str(iwan,no_sign=.True.))

                open(unit=992, file='./wannier'//orbital_name%chars()//'_'//trim(ch_spin)//'.xsf')
                write(992,'(a)') "CRYSTAL"
                write(992,'(a)') "PRIMVEC"
                write(992,'(3(f12.7,3x))') (rbas(ii,1)*par*bohr, ii=1,3)
                write(992,'(3(f12.7,3x))') (rbas(ii,2)*par*bohr, ii=1,3)
                write(992,'(3(f12.7,3x))') (rbas(ii,3)*par*bohr, ii=1,3)
                !                    write(992,'(a)') "CONVVEC"
                !                    if (rbas(2,1)==0.d0 .and. rbas(3,1)==0.d0) then
                !                        write(992,'(3(f12.7,3x))')&
                    !                            (rbas(ii,1)*par*bohr, ii=1,3)
                !                    else if (rbas(1,1)==0.d0 .and. (rbas(2,1)==rbas(3,1))) then
                !                        write(992,'(3(f12.7,3x))')&
                    !                            2*(rbas(2,1)*par*bohr), rbas(1,1), rbas(1,1)
                !                    else
                !                        write(992,'(3(f12.7,3x))')&
                    !                            (rbas(ii,1)*par*bohr*ndiv(1), ii=1,3)
                !                    end if
                !                    if (rbas(1,2)==0.d0 .and. rbas(3,2)==0.d0) then
                !                        write(992,'(3(f12.7,3x))')&
                    !                            (rbas(ii,2)*par*bohr, ii=1,3)
                !                    else if (rbas(2,2)==0.d0 .and. (rbas(1,2)==rbas(3,2))) then
                !                        write(992,'(3(f12.7,3x))')&
                    !                            rbas(2,2), 2*(rbas(1,2)*par*bohr), rbas(2,2)
                !                    else
                !                        write(992,'(3(f12.7,3x))') &
                    !                            (rbas(ii,2)*par*bohr*ndiv(2), ii=1,3)
                !                    end if
                !                    if (rbas(1,3)==0.d0 .and. rbas(2,3)==0.d0) then
                !                        write(992,'(3(f12.7,3x))')&
                    !                            (rbas(ii,3)*par*bohr, ii=1,3)
                !                    else if (rbas(3,3)==0.d0 .and. (rbas(1,3)==rbas(2,3))) then
                !                        write(992,'(3(f12.7,3x))')&
                    !                            rbas(3,3), rbas(3,3), 2*(rbas(1,3)*par*bohr)
                !                    else
                !                        write(992,'(3(f12.7,3x))')&
                    !                            (rbas(ii,3)*par*bohr*ndiv(3), ii=1,3)
                !                    end if
                write(992,'(a)') "PRIMCOORD"
                write(992,'(2(i5, 2x))') natom, 1
                do jj=1, natom
                    write(992,'(i5, 2x, 3(f12.6,3x))') nint(z(is(jj))),&
                        (tau(ii,jj)*par*bohr, ii=1,3)
                enddo

                write(992,'(a)') "BEGIN_BLOCK_DATAGRID_3D"
                write(992,'(a)') "real and imaginary for all orbitals"

                counter = 0

                orbital_name = "_orbr"
                call WriteDatagrid3D(dreal(wan_out(:,:,:,ir)), orbital_name)

                orbital_name = "_orbi"
                call WriteDatagrid3D(dimag(wan_out(:,:,:,ir)), orbital_name)


                write(992,'(a)') "END_BLOCK_DATAGRID_3D"

                close(992)
            enddo
        enddo


        contains

        subroutine WriteDatagrid3D(arr, name)
            use penf_stringify, only: str
            real*8,intent(in) :: arr(:,:,:)
            type(string) :: name
            type(string) :: countstr

            counter = counter + 1
            countstr = trim(str(counter,no_sign=.True.))

            write(992,'(a)') "BEGIN_DATAGRID_3D"// name // "#" //countstr

            write(992,'(3i5)') dim3
            write(992,'(3(f20.12,2x))')&
                -rbas(:,1)*par*bohr*(ndiv(1)/2)&
                -rbas(:,2)*par*bohr*(ndiv(2)/2)&
                -rbas(:,3)*par*bohr*(ndiv(3)/2)
            write(992,'(3(f20.12,2x))')&
                (rbas(ii,1)*par*bohr*ndiv(1),ii=1,3)
            write(992,'(3(f20.12,2x))') &
                (rbas(ii,2)*par*bohr*ndiv(2),ii=1,3)
            write(992,'(3(f20.12,2x))')&
                (rbas(ii,3)*par*bohr*ndiv(3),ii=1,3)
            write(992,*)&
                (((arr(i1,i2,i3),i1=1,dim3(1)),i2=1,dim3(2))&
                ,i3=1,dim3(3))

            write(992,'(a)') "END_DATAGRID_3D" // name // "#" //countstr

        end subroutine


    end subroutine  
  
    

end module
