    !supporting functions from ComWann we need for visualize_orbitals 
    !from file of the same name (except spharmonics -- from wannier_mmn.F)
    
    subroutine cal_rotmat_cmplx2real(norb, rotmat)
    implicit none
    integer, intent(in) :: norb
    complex*16, intent(out) :: rotmat(norb,norb)

    integer :: iorb

    rotmat=0.0d0

    rotmat(norb/2+1,norb/2+1)=1.0d0       
    do iorb=1, norb/2
      rotmat(iorb+norb/2+1,iorb+norb/2+1)=1.0d0/dsqrt(2.0d0)*(-1)**iorb
      rotmat(iorb+norb/2+1,-iorb+norb/2+1)=1.0d0/dsqrt(2.0d0)
      rotmat(-iorb+norb/2+1,iorb+norb/2+1)=1.0d0/dsqrt(2.0d0)*dcmplx(0.0d0, 1.0d0)*(-1)**(iorb+1)
      rotmat(-iorb+norb/2+1,-iorb+norb/2+1)=1.0d0/dsqrt(2.0d0)*dcmplx(0.0d0, 1.0d0)
    enddo
    end          
    
    SUBROUTINE  Swap_dble(a, b)
    IMPLICIT  NONE
    double precision, INTENT(INOUT) :: a, b
    double precision                :: Temp
    
    Temp = a
    a    = b
    b    = Temp
    END SUBROUTINE  Swap_dble
    
    SUBROUTINE  Swap_int(a, b)
    IMPLICIT  NONE
    integer, INTENT(INOUT) :: a, b
    integer                :: Temp
    
    Temp = a
    a    = b
    b    = Temp
    END SUBROUTINE  Swap_int
    
    DOUBLE PRECISION function iFactorial(j)
    IMPLICIT NONE
    INTEGER, intent(in) :: j
    INTEGER :: i
    DOUBLE PRECISION :: x
    if (j<0) print *, "iFactorial defined only for non-negative numbers!"
    x=1
    iFactorial = x
    if (j.eq.1) return
    DO i=2,j
      x = x*i
    END DO
    iFactorial = x
    return
    end function iFactorial

    DOUBLE PRECISION function dFactorial(x)
    IMPLICIT NONE
    DOUBLE PRECISION, intent(in) :: x
    DOUBLE PRECISION, PARAMETER :: spi2 = 0.8862269254527579
    DOUBLE PRECISION :: y, r
    r=1
    y=x
    DO WHILE(y.gt.1.0)
      r= r * y
      y= y -1.
    ENDDO
    IF (abs(y-0.5).LT.1e-10) r = r*spi2
    dFactorial = r
    return
    END function dFactorial

    DOUBLE PRECISION function mone(i)
    INTEGER, intent(in) :: i
    mone = 1 - 2*MOD(abs(i),2)
    return
    end function mone

    DOUBLE PRECISION function Ddelta(j1, j2, j)
    IMPLICIT NONE
    DOUBLE PRECISION, intent(in) :: j1, j2, j
!     function calls
    DOUBLE PRECISION :: dFactorial
    Ddelta = sqrt(dFactorial(j1+j2-j)*dFactorial(j1-j2+j)*dFactorial(-j1+j2+j)/dFactorial(j1+j2+j+1))
    return
    END function Ddelta

    DOUBLE PRECISION function f3j(j1, m1, j2, m2, j3, m3)
    IMPLICIT NONE
    DOUBLE PRECISION, intent(in) :: j1, j2, j3, m1, m2, m3
    INTEGER            :: tmin, tmax, t
    DOUBLE PRECISION             :: sum, v1, v2, dn
!     function calls
    DOUBLE PRECISION             :: dFactorial
    DOUBLE PRECISION             :: iFactorial
    DOUBLE PRECISION             :: Ddelta
    DOUBLE PRECISION             :: mone
    f3j=0
    IF (abs(m1+m2+m3) .GT. 1e-10) return
    IF (abs(j1-j2)-1e-14 .GT. j3 .OR. j3 .GT. j1+j2+1e-14) return
    if (abs(m1) .GT. j1 .OR. abs(m2) .GT. j2 .OR. abs(m3) .GT. j3) return
    tmin = INT(max(max(0.0,j2-j3-m1),j1-j3+m2)+1e-14)
    tmax = INT(min(min(j1+j2-j3,j1-m1),j2+m2)+1e-14)
    sum=0
    DO t=tmin, tmax
      v1 = dFactorial(j3-j2+m1+t)*dFactorial(j3-j1-m2+t)
      v2 = dFactorial(j1+j2-j3-t)*dFactorial(j1-m1-t) *dFactorial(j2+m2-t)
      sum = sum + mone(t)/(iFactorial(t)*v1*v2)
    END DO
    dn = dFactorial(j1+m1)*dFactorial(j1-m1)*dFactorial(j2+m2) *dFactorial(j2-m2)*dFactorial(j3+m3)*dFactorial(j3-m3)
    f3j = mone(INT(j1-j2-m3))*Ddelta(j1,j2,j3)*sqrt(dn)*sum
    return
    END function f3j
    

    DOUBLE PRECISION function ClebschG(j,m,j1,m1,j2,m2)
    IMPLICIT NONE
    DOUBLE PRECISION, intent(in) :: j,m,j1,m1,j2,m2
    INTEGER            :: tmin, tmax, t
    DOUBLE PRECISION             :: sum, v1, v2
!     function calls
    DOUBLE PRECISION, external :: iFactorial, dFactorial,mone,Ddelta
    
    ClebschG = 0
    IF (m1+m2 .NE. m) return
    IF ((j .gt. j1+j2) .or. (j .lt. abs(j1-j2))) return      
    tmin = INT(max(max(0.0,j2-j-m1),j1-j+m2)+1e-14)
    tmax = INT(min(min(j1+j2-j,j1-m1),j2+m2)+1e-14)
    sum=0;
    DO t=tmin, tmax
      v1 = sqrt((2*j+1)*dFactorial(j1+m1)*dFactorial(j1-m1) &
        *dFactorial(j2+m2)*dFactorial(j2-m2) &
        *dFactorial(j+m)*dFactorial(j-m))
      v2 = iFactorial(t)*dFactorial(j1+j2-j-t)*dFactorial(j1-m1-t) &
        *dFactorial(j2+m2-t)*dFactorial(j-j2+m1+t) &
        *dFactorial(j-j1-m2+t)
      sum = sum + mone(t)*v1/v2
    END DO
    ClebschG = sum*Ddelta(j1,j2,j)
    return
    END function ClebschG

    complex*16 function spharmonics(l, m, theta, phi)
!     with Condon-Shortley phase
      implicit none
      integer, intent(in) :: l, m
      double precision, intent(in) :: theta, phi
      integer :: lmm, lpm
      double precision :: pi, costheta
      double precision, external :: asso_lp,ifactorial
      complex*16 :: ai

      pi=4.0d0*datan2(1.0d0, 1.0d0)
      ai=dcmplx(0.0d0, 1.0d0)

      lmm=l-m
      lpm=l+m
      costheta=dcos(theta)
      spharmonics=dsqrt((2*l+1.0d0)/(4.0d0*pi) &
       *ifactorial(lmm)/ifactorial(lpm)) &
       *asso_lp(l,m,costheta)*cdexp(ai*m*phi)
      
      end


      double precision FUNCTION asso_lp(l,m,x)

      implicit none
      integer, intent(in) :: l, m
      double precision, intent(in) :: x
      integer :: absm,lmm,lpm
      double precision, external :: plgndr, ifactorial

      if (m .ge. 0) then
        asso_lp=plgndr(l,m,x)
      else
        absm=-m
        lmm=l-absm
        lpm=l+absm
        asso_lp=(-1)**absm*ifactorial(lmm)/ifactorial(lpm)*plgndr(l,absm,x)
      endif
      end
      

      double precision FUNCTION plgndr(l,m,x)
!     from numerical recipe
      
      implicit none
      INTEGER, intent(in) ::  l,m
      double precision, intent(in) :: x      
      INTEGER :: i,ll
      double precision  :: fact,pll,pmm,pmmp1,somx2
      pmm=1.0d0 !Compute P mm .
      if(m.gt.0) then
        somx2=dsqrt((1.0d0-x)*(1.0d0+x))
        fact=1.0d0
        do i=1,m
          pmm=-pmm*fact*somx2
          fact=fact+2.0d0
        enddo
      endif
      if(l.eq.m) then
        plgndr=pmm
      else
        pmmp1=x*(2*m+1)*pmm     !Compute P m m+1.
        if(l.eq.m+1) then
          plgndr=pmmp1
        else                    !Compute P m l , l>m + 1.
          pll=0.0d0
          do ll=m+2,l
            pll=(x*(2*ll-1)*pmmp1-(ll+m-1)*pmm)/(ll-m)
            pmm=pmmp1
            pmmp1=pll
          enddo
          plgndr=pll
        endif
      endif
      END      