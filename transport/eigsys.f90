module SortedComplexEigsys

contains

    subroutine SVD(ham, zek, evl, evr, ndim)
        complex*16, intent(inout):: ham(ndim,ndim)  ! Hamiltonian / overwritten
        complex*16, intent(out) :: zek(ndim)       ! Vector of eigenvalues
        complex*16, intent(out) :: evl(ndim,ndim)   ! left eigenvectors
        complex*16, intent(out) :: evr(ndim,ndim)   ! right eigenvectors
        integer, intent(in)     :: ndim            ! Dimension of hamiltonian
        complex*16, allocatable :: cwork(:)
        real*8, allocatable :: rcondu(:), rcondv(:), rwork(:), sva(:)
        integer, allocatable :: iwork(:)
        integer :: n,m

        n = ndim
        m = ndim

        lda = m
        ldu = m
        ldv = n
        lwork = 5*n + n*n + 1000
        lrwork = n + 2*m + 7
        allocate (rcondu(m), rcondv(m), cwork(lwork), rwork(lrwork), iwork(m+3*n+3))

        call zgejsv('C', 'U', 'V', 'R', 'N', 'N', n, m, ham, lda, zek, evl, ldu, evr, &
            ldv, cwork, lwork, rwork, lrwork, iwork, info)

        if (info /= 0) then
            print *, "ZGEJSV failed!"
            stop
        end if

        evr = transpose(conjg(evr))

        deallocate(rcondu,rcondv,cwork,rwork,iwork)

    end subroutine



    subroutine eigsys(ham, zek, evl, evr, ndim)
        !----------------------------------------------------------------------!
        ! This routine solves the generalized right/left eigenvalue problems   !
        !       H.Ev^{R} = E Ev^{R}      and    Ev^{L}H = E Ev^{L}             !
        ! ( . denotes matrix multiplication )  where H is a general matrix and.!
        !                                                                      !
        ! The problem is solved in several steps:                              !
        !                                                                      !
        !                                                                      !
        ! Variable contents on entry:                                          !
        ! ===========================                                          !
        !                                                                      !
        ! ham        : Hamiltonian                                             !
        ! ndim       : Dimension of matrices                                   !
        ! fh_stderr  : Standard error file handle (Unit number)                !
        !                                                                      !
        ! Variable contents on exit:                                           !
        ! ==========================                                           !
        !                                                                      !
        ! evr        : Right eigenvectors as columns                           !
        ! evl        : Left eigenvectors as rows                               !
        ! zek        : Eigenvalues                                             !
        !----------------------------------------------------------------------!
        implicit none
        !---------- Passed variables ----------
        complex*16, intent(inout):: ham(ndim,ndim)  ! Hamiltonian / overwritten
        complex*16, intent(out) :: zek(ndim)       ! Vector of eigenvalues
        complex*16, intent(out) :: evl(ndim,ndim)   ! left eigenvectors
        complex*16, intent(out) :: evr(ndim,ndim)   ! right eigenvectors
        integer, intent(in)     :: ndim            ! Dimension of hamiltonian
        !f2py integer intent(hide), depend(ham)  :: ndim=shape(ham,0)
        !---------- Local variables ----------
        real*8, parameter :: smalleps = 1e-5
        character(len=1), parameter :: uplo  = "U"  ! Job parameter for zpotrf/ztrtri
        character(len=1), parameter :: diag  = "N"  ! Job parameter for ztrtri
        character(len=1), parameter :: jobvl = "V"  ! Jop parameter for zgeev
        character(len=1), parameter :: jobvr = "V"  ! Job parameter for zgeev
        character(len=1), parameter :: transn  = "N" ! Job parameter for ztrmm
        character(len=1), parameter :: transc  = "C" ! Job parameter for ztrmm
        character(len=1), parameter :: left   = "L" ! Job parameter for ztrmm
        character(len=1), parameter :: right  = "R" ! Job parameter for ztrmm
        integer :: ierr                        ! Error parameter for lapack
        integer :: irow                        ! Loop index for rows
        integer :: icol                        ! Loop index for columns
        integer :: p,q,r                       ! Loop index
        complex*16 :: ctmp                     ! Temporary variable
        !COMPLEX*16 :: htmp(ndim,ndim)  ! Transformed Hamiltonian
        complex*16 :: scaler(ndim)     ! Array of normalization parameters
        complex*16 :: cworkvec(8*ndim) ! Work array for zgeev
        real*8     :: rworkvec(8*ndim) ! Work array for zgeev
        integer    :: idxarr(ndim)
        logical :: hasDegen, doExit
        complex*16, allocatable :: prod(:,:)

        hasDegen = .false.

        !htmp = ham
        !========== Step 4, Solve eigenvalue problem, H'v' = Ev' ==========
        call zgeev(jobvl,jobvr,ndim,ham,ndim,zek,evl,ndim,evr,ndim,cworkvec,8*ndim,rworkvec,ierr)
        if(ierr.NE.0) then
            print *, 'Error code of zgeev ', ierr
            print *, 'Error in dia_gho! Stopp the code!'
        endif
        ! transpose left eigenvectors
        evl = dconjg(transpose(evl))

        !========== Step 5, Make degenerate eigenvectors orthogonal
        do q=1,ndim
            do p=1,q-1
                if (abs(zek(p)-zek(q)).LT.smalleps .AND. abs(scalprod(evl(p,:),evr(:,q),ndim)) .GT.smalleps) then
                    evr(:,q) = evr(:,q) - scalprod(evl(p,:),evr(:,q),ndim)/scalprod(evl(p,:),evr(:,p),ndim) * evr(:,p)
                    hasDegen = .True.
                endif
            enddo
            do p=1,q-1
                if (abs(zek(p)-zek(q)).LT.smalleps .AND. abs(scalprod(evl(q,:),evr(:,p),ndim)) .GT.smalleps) then
                    evl(q,:) = evl(q,:) - scalprod(evl(q,:),evr(:,p),ndim)/scalprod(evl(p,:),evr(:,p),ndim) * evl(p,:)
                endif
            enddo
        enddo

        !========= Step 6, Normalize eigenvectors
        do p = 1,ndim
            ctmp = 0.d0
            do q = 1,ndim
                ctmp = ctmp+evl(p,q)*evr(q,p)
            enddo
            scaler(p) = sqrt(ctmp)
        enddo
        do p = 1,ndim
            evl(p,:) = evl(p,:)/scaler(p)
            evr(:,p) = evr(:,p)/scaler(p)
        enddo

        !========= Sorting acording to real parts
        call eig_order_real_part(zek, idxarr, ndim)
        call permute_eigensystem(idxarr, zek, evl, evr, ndim)

        ! testing code
        if (.false. .and. hasDegen) then
            allocate(prod(ndim,ndim))
            prod = matmul(evl, evr)
            doExit = .False.
            do q=1,ndim
                do p=1,ndim
                    if ((p==q .and. abs(prod(p,q)-1.0)>1.0d-4).or.(p/=q .and.  abs(prod(p,q))>1.0d-4)) then
                        print *, "*** Matrix product diagonal not I", prod(p,q), p,q
                        doExit = .True.
                        exit
                    end if
                end do
                if (doExit) exit
            end do

            deallocate(prod)
        end if

        return

    contains
        complex*16 function scalprod(a,b,ndim)
            implicit none
            complex*16 :: a(:), b(:)
            integer    :: ndim
            integer    :: i
            scalprod = 0.0
            do i=1,ndim
                scalprod = scalprod + a(i)*b(i)
            enddo
        end function scalprod

    end subroutine eigsys

    !===========================================================================
    subroutine eig_order_real_part(ev, idxarr, ndim)
        implicit none
        !-----------------------------------------------------------------!
        ! This routine sorts complex eigenvalues of a matrix according to !
        ! its real parts with the smallest in the first slot and reorders !
        ! the matrices of left (row) and right (column) eigenvectors in a !
        ! corresponding manner.                                           !
        !-----------------------------------------------------------------!
        !---------- Passed variables ----------
        complex*16, intent(in) :: ev(ndim)         ! Array of eigenvalues
        integer, intent(out)   :: idxarr(ndim)     ! Index array which gives proper order
        integer :: ndim                            ! Dimension of matrices
        !f2py integer intent(hide), depend(ev)  :: ndim=shape(ev,0)
        !---------- Parameters ----------
        real*8, parameter :: maxval = 1000.d0
        !---------- Local variables ----------
        logical, allocatable :: sorted(:)
        real*8,  allocatable :: sortonr(:)
        integer :: p
        integer :: q
        integer :: idx
        real*8  :: min
        !---------- Allocate dynamic memory storage ----------
        allocate(sortonr(ndim), sorted(ndim))
        !---------- Initialize arrays ----------
        idxarr = 0
        sortonr = dble(ev)
        sorted = .FALSE.
        !---------- Create index array for real value ----------
        sorted = .FALSE.
        do p = 1,ndim
            min = maxval
            do q = 1,ndim
                if(.not.sorted(q).AND.min.GT.sortonr(q)) then
                    min = sortonr(q)
                    idx = q
                endif
            enddo
            idxarr(p) = idx
            sorted(idx) = .TRUE.
        enddo
        deallocate(sortonr, sorted)
        return
    end subroutine eig_order_real_part

    subroutine permute_eigensystem(idxarr, ev, evl, evr, ndim)
        implicit none
        !---------- Passed variables ----------
        integer, intent(in)       :: idxarr(ndim)     ! Index array which gives proper order
        complex*16, intent(inout) :: ev(ndim)         ! Array of eigenvalues
        complex*16, intent(inout) :: evl(ndim,ndim)   ! Matrix of left eigenvectors  (row)
        complex*16, intent(inout) :: evr(ndim,ndim)   ! Matrix of right eigenvectors (column)
        integer :: ndim                               ! Dimension of matrices
        !f2py integer intent(hide), depend(ev)  :: ndim=shape(ev,0)
        !---------- Local variables ------------------
        integer :: p
        complex*16, allocatable :: eval(:)
        complex*16, allocatable :: evec(:,:)
        allocate(eval(ndim), evec(ndim,ndim))
        !---------- Permute the eigenvalues ----------
        do p = 1,ndim
            eval(p) = ev(idxarr(p))
        enddo
        ev = eval
        !---------- Permute the right eigenvectors ----------
        do p = 1,ndim
            evec(:,p) = evr(:,idxarr(p))
        enddo
        evr = evec
        !---------- Permute the left eigenvectors ----------
        do p = 1,ndim
            evec(p,:) = evl(idxarr(p),:)
        enddo
        evl = evec
        !---------- Deallocate dynamic memory storage ----------
        deallocate(eval, evec)
        return
    end subroutine permute_eigensystem

    subroutine eigvals(ham, zek, ndim)
        !----------------------------------------------------------------------!
        ! Eigenvalues only, no eigenvectors                                    !
        ! This routine solves the generalized right/left eigenvalue problems   !
        !       H.Ev^{R} = E Ev^{R}      and    Ev^{L}H = E Ev^{L}             !
        ! ( . denotes matrix multiplication )  where H is a general matrix and.!
        !                                                                      !
        ! The problem is solved in several steps:                              !
        !                                                                      !
        !                                                                      !
        ! Variable contents on entry:                                          !
        ! ===========================                                          !
        !                                                                      !
        ! ham        : Hamiltonian                                             !
        ! ndim       : Dimension of matrices                                   !
        ! fh_stderr  : Standard error file handle (Unit number)                !
        !                                                                      !
        ! Variable contents on exit:                                           !
        ! ==========================                                           !
        !                                                                      !
        ! evr        : Right eigenvectors as columns                           !
        ! evl        : Left eigenvectors as rows                               !
        ! zek        : Eigenvalues                                             !
        !----------------------------------------------------------------------!
        implicit none
        !---------- Passed variables ----------
        complex*16, intent(in)  :: ham(ndim,ndim)  ! Hamiltonian
        complex*16, intent(out) :: zek(ndim)       ! Vector of eigenvalues
        integer, intent(in)     :: ndim            ! Dimension of hamiltonian
        !f2py integer intent(hide), depend(ham)  :: ndim=shape(ham,0)
        !---------- Local variables ----------
        real*8, parameter :: smalleps = 1e-5
        character(len=1), parameter :: uplo  = "U"  ! Job parameter for zpotrf/ztrtri
        character(len=1), parameter :: diag  = "N"  ! Job parameter for ztrtri
        character(len=1), parameter :: jobvl = "N"  ! Jop parameter for zgeev
        character(len=1), parameter :: jobvr = "N"  ! Job parameter for zgeev
        character(len=1), parameter :: transn  = "N" ! Job parameter for ztrmm
        character(len=1), parameter :: transc  = "C" ! Job parameter for ztrmm
        character(len=1), parameter :: left   = "L" ! Job parameter for ztrmm
        character(len=1), parameter :: right  = "R" ! Job parameter for ztrmm
        integer :: ierr                        ! Error parameter for lapack
        integer :: irow                        ! Loop index for rows
        integer :: icol                        ! Loop index for columns
        integer :: p,q,r,i                     ! Loop index
        complex*16 :: ctmp                     ! Temporary variable
        complex*16 :: cworkvec(8*ndim) ! Work array for zgeev
        real*8     :: rworkvec(8*ndim) ! Work array for zgeev
        complex*16 :: evl(1,1)   ! left eigenvectors
        complex*16 :: evr(1,1)   ! right eigenvectors
        integer    :: idxarr(ndim)

        !========== Step 4, Solve eigenvalue problem, H'v' = Ev' ==========
        call zgeev(jobvl,jobvr,ndim,ham,ndim,zek,evl,ndim,evr,ndim,cworkvec,8*ndim,rworkvec,ierr)
        if(ierr.NE.0) then
            print *, 'Error code of zgeev ', ierr
            print *, 'Error in dia_gho! Stopp the code!'
        endif

        !========= Sorting acording to real parts
        call eig_order_real_part(zek, idxarr, ndim)
        call permute_eigenvals(idxarr, zek, ndim)

        return
    end subroutine eigvals

    subroutine permute_eigenvals(idxarr, ev, ndim)
        implicit none
        !---------- Passed variables ----------
        integer, intent(in)       :: idxarr(ndim)     ! Index array which gives proper order
        complex*16, intent(inout) :: ev(ndim)         ! Array of eigenvalues
        integer :: ndim                               ! Dimension of matrices
        !f2py integer intent(hide), depend(ev)  :: ndim=shape(ev,0)
        !---------- Local variables ------------------
        integer :: p
        complex*16, allocatable :: eval(:)
        allocate(eval(ndim))
        !---------- Permute the eigenvalues ----------
        do p = 1,ndim
            eval(p) = ev(idxarr(p))
        enddo
        ev = eval
        !---------- Deallocate dynamic memory storage ----------
        deallocate(eval)
        return
    end subroutine permute_eigenvals


end module
