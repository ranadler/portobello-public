!
!!  Transport module
!
!  Author:  Ran Adler
!  Date:    Sep, 2017
!  Version: 2.0
!>
!!  This is the abstract interface to coordinate between two decoupled parties:
!!  1.  An analysis program ("consumer"), such as the Transport calculation.
!!      we call these Algorithm.
!!  2.  A "producer" of data, which we call an ElectronicStructureProvider
!!  Note that these two interfaces interact back an forth in a simple protocol.
!!
module ElectronicStructureMethod
    implicit none

    type        :: MatrixInfo
        integer :: kp_index, sym_index
            !! number of k point, and its symmetry for debug
        real*8  :: VOL     !! volume of a unit cell
        integer :: n_basis
            !! dimension of the Hilbert subspace for the current K point. The basis spans 1:n_basis.
        integer :: n_basis_max
            !! maximum number of bands over all k
        integer :: n_corr, min_corr, max_corr
            !! bounds for the correlated basis subspace (or bands)
        integer :: spin_factor
        real*8  :: wgh  !! weight of current k point
    end type


    type, abstract :: ElectronicStructureProvider
    contains
        procedure(GetHamltonianPlus), deferred, pass(self) :: GetHamltonianPlus
        procedure(GetMomentumMatrix), deferred, pass(self)    :: GetMomentumMatrix

        procedure(InvokeOverKPoints), deferred, pass(self)    :: InvokeOverKPoints

        procedure(IsSameBand), deferred, pass(self)           :: IsSameBand
    end type

    type, abstract :: Algorithm
    contains
        procedure(PrepareKPoint), deferred, pass(self) :: PrepareKPoint
        procedure(ProcessKPoint), deferred, pass(self) :: ProcessKPoint
        procedure(EndKPoint),     deferred, pass(self) :: EndKPoint
    end type


    interface

        subroutine GetHamltonianPlus(self, womega, diagonal, L, R)
            !! Here the diagonal returned is of the operator
            !!  $$ G^{-1}(\bf{k}, \omega) = \omega + \mu - H(\bf{k}) - \Sigma(\omega, \bf{k}) $$
            !!  The return value has dimensions 1:NE - a full matrix siagonal in the basis.
            !!  The diagonal is expected to be complex, in general. For example, for a non-interacting
            !!  GF in the k-basis, the diagonal will actually be real, and \\( L=R^{-1} \\) will be real.
            import ElectronicStructureProvider
            class(ElectronicStructureProvider), intent(inout) :: self
            real*8, intent(in)                                :: womega
            complex*16, intent(out)                           :: diagonal(:)
                !! assumed to have the range 1:NE
            complex*16, intent(out)                           :: L(:,:), R(:, :)
                !! square matrix of size n_corr. The following relationship needs to be satisfied:
                !!  $$ L \cdot D \cdot R = G^{-1}(\bf{k}, \omega)$$
                !! where D=diagonal, \\( \omega \\)=womega
        end subroutine

        function GetMomentumMatrix(self,nb_min,nb_max) result(MM)
            !! return the momentum matrix and the range of relevant basis indices.
            import ElectronicStructureProvider, Algorithm
            class(ElectronicStructureProvider), intent(inout) :: self
            complex*16, pointer                               :: MM(:,:,:)
                !! Pointer to the momentum matrix. The matrix bounds are
                !! (1:NE,1:NE,3) where NE is the number of bands for the k-point
            integer,intent(out)                               :: nb_min,nb_max
                !! these are the range of (band/basis) indices for which we actually have
                !! momentum data.
                !! Typically the range is almost the full 1:NE.
        end function

        subroutine InvokeOverKPoints(self, alg, symmetrize_bz, with_mm)
            !! The main entry point into this object.
            !! Someone who has an Algorithm alg (which could do some analysis), will
            !! invoke this method, so that we can make call backs to the algorithm.
            import ElectronicStructureProvider, Algorithm
            class(ElectronicStructureProvider), intent(inout) :: self
            class(Algorithm), intent(inout)                   :: alg
            logical, intent(in)                               :: symmetrize_bz
                !! when false, calls will be made to the algorithm only for k points
                !! in the reduce BZ. When true, calls will be made to the algorithm for
                !! *all* k-points kn the BZ, and the momentum matrix, if available, may
                !! need to be transformed specifically for the k-point.
            logical, intent(in)                               :: with_mm
                !! if true, the momentum matrix will be read and prepared for calculation
                !! for each one of the k-points.
        end subroutine

        function IsSameBand(self, p, q) result(res)
            !! This is a refinement to the analysis algorithm. ESPs can just return
            !! .FALSE. for all pairs, without risking the validity of the algorithm.
            !! **If there is a notion of bands** in the ESP, such as in DFT, then the algorithm
            !! can be refined by selectively returning .True. here,
            !! and then one can use the option to integrate only inter-band contribution.
            import ElectronicStructureProvider
            class(ElectronicStructureProvider), intent(inout) :: self
            integer, intent(in)             :: p, q
            logical                         :: res
        end function

        !-----------------------------------------------------------------------


        subroutine PrepareKPoint(self, esp, mi)
            !! This method is invoked by the ElectronicStructureProvider per irreducible
            !! k-point, that is, once for each equivalence class of k-points.
            !! Work that should be done once for the equivalence class may be done here.
            !!! 2019: wrong!!! This was a bug in the original code.
            !!!                the green's function is NOT the same for all k's in a star! Neither are the L and R matrices
            import Algorithm, MatrixInfo, ElectronicStructureProvider
            class(Algorithm),  intent(inout)                   :: self
            class(ElectronicStructureProvider), intent(inout)  :: esp
            type(MatrixInfo), intent(inout)                   :: mi
        end subroutine

        subroutine ProcessKPoint(self, esp, mi)
            !! This method is invoked by the ElectronicStructureProvider per k-point,
            !! always after PrepareKPoint has been called.
            import Algorithm, MatrixInfo, ElectronicStructureProvider
            class(Algorithm),  intent(inout)                   :: self
            class(ElectronicStructureProvider), intent(inout)  :: esp
            type(MatrixInfo), intent(inout)                   :: mi
        end subroutine


        subroutine EndKPoint(self, esp, mi)
            !! This method is invoked by the ElectronicStructureProvider once
            !! per irreducible k-point, always after ProcessKPoint has been called
            !! at least once.
            import Algorithm, MatrixInfo, ElectronicStructureProvider
            class(Algorithm),  intent(inout)                   :: self
            class(ElectronicStructureProvider), intent(inout)  :: esp
            type(MatrixInfo), intent(inout)                   :: mi
        end subroutine

    end interface

end module
