!
!!  Transport module
!
!  Author:  Ran Adler
!  Date:    Sep, 2017
!  Version: 2.0
!>
!!  General-purpose transport singleton object, which can work with an
!!  an ElectronicStructureProvider object, typically DMFT or GW.
!!
module TransportImp
!!   We calculate
!!   $$A_{n}(\Omega)=\sum_{\mathbf{k}\in BZ}\int_{-\infty}^{\infty}d\omega\,
!!       \omega^{n}\,\bigg\{\frac{f(\omega)-f(\omega+\Omega)}{\Omega}\bigg\} tr\{\mathbf{V}_{\mathbf{k}}^{\alpha}\rho_{\mathbf{k}}
!!       (\omega+\Omega)\mathbf{V}_{\mathbf{k}}^{\beta}\rho_{\mathbf{k}}(\omega)\}\qquad n=0,1,2 $$

    use ElectronicStructureMethod, only: ElectronicStructureProvider, Algorithm
    use TransportOptics, only: Input, GreensFunction, WorkArea, Output, IntegrandForDebug
    implicit none

    private
    public :: Compute, Ry2eV

    type(WorkArea), allocatable :: work

    ! constants
    real*8, parameter         :: K2eV        = 0.0000861733
    real*8, parameter         :: Ry2eV       = 13.60569193
    real*8, parameter         :: a0R_K       = 0.00013659549 ! R_K * a0 in \Omega*cm where R_K is the Kilzing quantum of resistivity constant h/e^2
                                                             ! note this is just pi in Rydberg atomic units, which is how we convert units to Omega*cm
    real*8, parameter         :: PI          = 3.14159265358979
    complex*16, parameter     :: IMG        = (0.0D0,1.0D0)
    real*8, parameter         :: PI2O3       = PI * PI /3.0
    real*8, parameter         :: BIGQuantity = 1.0d10

    integer, parameter :: maxpower = 2


    type, extends(Algorithm)  :: Integrator
    contains
        procedure, pass(self) :: PrepareKPoint => InitializeDatastructurePerKPoint
        procedure, pass(self) :: ProcessKPoint => IntegrateKPointTerm
        procedure, pass(self) :: EndKPoint     => ReleaseDatastructure
    end type

    real*8                    :: prefactor

    type(GreensFunction),allocatable :: gf

    integer                   :: n_basis  !! size of basis (# bands) corresponding to current K-point
    integer                   :: min_corr, max_corr, n_corr !! range of correlated bands

    logical, allocatable      :: is_corr(:)
        !! true in the range min_corr:max_corr (<--> correlated in the *input*, eg. in DMFT U)

    type(Input)      :: inp
    type(Integrator) :: singleton
    type(IntegrandForDebug),allocatable :: integrand

    logical :: maswrk
    integer :: master

contains


    subroutine Compute(esp, trinp)
        !! **This is the main public entry point of the module.**
        !! After setting up the output array, Compute calls the ElectronicStructureProvider to
        !! invoked the singleton object on every k-point.
        !! When this end, it just copies the output to a TransportResults type, and ends.
        !!
        !use com_mpi, only: MPI_start, MPI_end, MPI_SUM_ALL
        use ElectronicStructureMethod
        use parallel_mod, only: goparr
        use stringifor
        use mpi

        class(ElectronicStructureProvider), intent(inout) :: esp
        type(Input), intent(inout) :: trinp
        type(Output), allocatable :: cout
        integer :: i,dir
        real*8 :: TK, seebeck, kappa
        type(string) :: tmp_name
        type(string) :: out_location
        integer :: myid, ierror

        myid = 0
        if (goparr) call MPI_COMM_RANK(MPI_COMM_WORLD, myid,   ierror)
        maswrk = myid == 0
        master = 0

        inp = trinp
        TK = inp%Temperature * Ry2eV / K2eV ! in Kelvin
        if (maswrk) print *, "- transport temperature [K] is ", TK, " symmetrize_bz is", inp%symmetrize_bz
        allocate(work, integrand,cout)


        cout%num_omega = inp%num_omega  ! number of points (non-negative) to calculate
        work%num_omega = inp%num_omega 
        work%maxpower= maxpower ! this is hard coded in the program
        work%num_dirs = inp%num_dirs
        integrand%max_omega =inp%max_omega_mesh
        ! integration mesh
        call work%allocate()
        call integrand%allocate()

        work%total_weight=0.d0

        cout%Input = trinp

        if (maswrk) print *, '- #mesh ω=', 2*inp%max_omega_mesh+1, '#active ω=', inp%active_omega_points, &
                             '#Ω=', cout%num_omega + 1

        ! This is doing all the work ----------------------------------
        call esp%InvokeOverKPoints(singleton, inp%symmetrize_bz, .True.)
        ! -------------------------------------------------------------

        ! sum the integrand and the total work are
        if (goparr) call SumMPI()

        if (maswrk) then
            print *, 'total weight:', work%total_weight
            work%total_ = work%total_ / work%total_weight

            ! copy to results variable
            call cout%allocate()
            do i=0,cout%num_omega
                cout%omega_(i)   = inp%delta*i *Ry2eV
                cout%A_(i, :, :) = work%total_(:,:, i) ! these dimension match, they are num_dirs, and max_power
            end do

           out_location = './transport.h5:/output'
           call cout%store(out_location%chars(), .True.)
    
            do i=-integrand%max_omega,integrand%max_omega
                integrand%omega_(i) = i*inp%delta * Ry2eV
            end do
            tmp_name = './transport_integrand.h5:/'
            call integrand%store(tmp_name%chars(), .True.)

        end if
        deallocate(work,integrand,cout)
    end subroutine

    subroutine SumMPI()
        use mpi
        integer :: ierr
        
        ierr = 0
        CALL MPI_ALLREDUCE(MPI_IN_PLACE, work%total_weight, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ierr)
        if (ierr.ne.0) print *, 'ERROR in MPI_REDUCE of total_weight', ierr
      
        ierr = 0
        CALL MPI_ALLREDUCE(MPI_IN_PLACE, work%total_, size(work%total_), MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ierr)
        if (ierr.ne.0) print *, 'ERROR in MPI_REDUCE of total', ierr

        ierr = 0
        CALL MPI_ALLREDUCE(MPI_IN_PLACE, integrand%G_, size(integrand%G_), MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, ierr)
        if (ierr.ne.0) print *, 'ERROR in MPI_REDUCE of integrand G', ierr
    end subroutine

    subroutine InitializeDatastructurePerKPoint(self, esp, mi)
        use ElectronicStructureMethod, only: MatrixInfo, ElectronicStructureProvider
        class(Integrator), intent(inout)                  :: self
        class(ElectronicStructureProvider), intent(inout) :: esp
        type(MatrixInfo), intent(inout)                   :: mi
        integer                :: i, bi

        n_basis = mi%n_basis

        ! do these on the 1st k point in our mpi task
        if (.not. allocated(gf)) then
            min_corr = mi%min_corr; max_corr = mi%max_corr; n_corr = mi%n_corr

            allocate(gf)
            gf%max_omega = inp%max_omega_mesh
            gf%num_correlated = n_corr  ! does not change through the run
            gf%num_basis = mi%n_basis_max
            call gf%allocate()

            do i=-gf%max_omega,gf%max_omega
                gf%w_(i) =  i*inp%delta ! => Ry
                if (i == -gf%max_omega) then
                    gf%a_(i) = gf%w_(i); gf%b_(i) = gf%w_(i) + inp%delta*0.5
                elseif (i == gf%max_omega) then
                    gf%b_(i) = gf%w_(i); gf%a_(i) = gf%w_(i) - inp%delta*0.5
                else
                    gf%a_(i) = gf%w_(i) - inp%delta*0.5; gf%b_(i) = gf%w_(i) + inp%delta*0.5
                end if
            end do

            if (.not. allocated(is_corr)) then
                allocate(is_corr(mi%n_basis_max)); is_corr(:) = .FALSE.; is_corr(min_corr:max_corr) = .TRUE.
            end if

            prefactor = -4.0*mi%spin_factor/(a0R_K*mi%VOL)
        end if

        do i=-gf%max_omega,gf%max_omega

            ! note the switched R and L: it is an inverse!
            call esp%GetHamltonianPlus(gf%w_(i), gf%HPlus_(1:,i), gf%L_(:,:,i), gf%R_(:,:,i))

            do bi=1,n_basis
                if (.not. is_corr(bi)) then
                    gf%HPlus_(bi,i) = gf%HPlus_(bi,i) - inp%gamm*IMG
                else
                    gf%HPlus_(bi,i) = gf%HPlus_(bi,i) - inp%gammc*IMG
                end if
                integrand%G_(i) = integrand%G_(i) + 1.d0 / ( gf%w_(i) - gf%HPlus_(bi,i)) * mi%wgh / Ry2eV
            end do
        enddo
    end subroutine

    subroutine ReleaseDatastructure(self, esp, mi)
        use ElectronicStructureMethod, only: MatrixInfo, ElectronicStructureProvider
        class(Integrator), intent(inout)                  :: self
        class(ElectronicStructureProvider), intent(inout) :: esp
        type(MatrixInfo), intent(inout)                   :: mi
        gf%HPlus_ = 0.0
        gf%R_ = 0.0
        gf%L_ = 0.0
    end subroutine

    !> This is invoked for each K point in the BZ, including the reducible ones
    subroutine IntegrateKPointTerm(self, esp, mi)
        use ElectronicStructureMethod, only: MatrixInfo, ElectronicStructureProvider
        class(Integrator), intent(inout)                  :: self
        class(ElectronicStructureProvider), intent(inout) :: esp
        type(MatrixInfo),intent(inout)                    :: mi

        complex*16, pointer     :: MM(:,:,:)
        integer                 :: nb_min, nb_max
        complex*16, allocatable :: Cnc(:, :, :) !! non correlated C
        complex*16              :: total(inp%num_dirs,0:maxpower)
        real*8                  :: w
        integer                 :: iw, i

        MM => esp%GetMomentumMatrix(nb_min, nb_max)
        allocate(Cnc(n_basis,n_basis,inp%num_dirs))
        Cnc = 0.0
        call TransformVelocityMatrix(nb_min, nb_max, MM, MM, Cnc)
        write(*,'(A,I3,1x,A,I3)') 'k=', mi%kp_index, ' sym=', mi%sym_index

        work%total_weight = work%total_weight + mi%wgh
        ! there is 1 iter. for Transport, but for optics it iterates over all requested omega.
        do iw=0,inp%num_omega
            w = gf%w_(iw)

            ! The non-correlated contribution is integrated completely analytically
            total = NonCorrelatedContribution(Cnc,  w, nb_min, nb_max)

            ! TODO: decrease the upper bound to max(4kT, energy_limit) window above iw
            do i=max(iw-inp%active_omega_points,-gf%max_omega), min(iw+inp%active_omega_points, gf%max_omega)
                total = total + FermiFactor()*CorrelatedContribution(w, i, i-iw, MM) !i => x, i-iw=>x-omega
            enddo
            work%total_(:,:,iw) = work%total_(:,:,iw) + prefactor*mi%wgh * dreal(total)
        enddo
        deallocate(Cnc)
        contains
        function FermiFactor() result(ff)
            real*8 :: ff, Ep, Em, fp
            Ep = gf%w_(i) /inp%Temperature; Em = gf%w_(i-iw) / inp%Temperature
            fp = ferm(Ep)
            if (iw == 0) then ! w=0
                ff = fp * ferm(-Ep) / inp%Temperature
            else
                ff = (ferm(Em) - fp) / w  ! >0
            end if
        end function
    end subroutine

    function NonCorrelatedContribution(Cnc, w, nb_min, nb_max) result(res)
        complex*16                     :: res(inp%num_dirs,0:maxpower)
        complex*16, intent(in)         :: Cnc(:,:,:)
        real*8,intent(in)              :: w
        integer, intent(in)            :: nb_min, nb_max

        complex*16, pointer :: Hp(:)
        integer    :: p, q, pw
        complex*16 :: denom_c, denom_d,            &
            cterm(0:maxpower), dterm(0:maxpower),  &
            ANIR(0:maxpower,nb_min:nb_max),  &
            ANIL(0:maxpower,nb_min:nb_max),  &
            ANI_SC(0:maxpower,nb_min:nb_max)

        res=0.0
        ! conveniently use the Green's function at w=0. Here we reference only the
        ! non-correlated part of the diagonal, which is assumed to be real (there is
        ! no conjugation operator below).
        Hp => gf%HPlus_(:,0)
        ! The following initializes all elements of ANI* arrays
        do p=nb_min,nb_max
            if (is_corr(p)) cycle
            ! the terms with the lower bounds will cancel
            call AnalyticIntegrals(0.0d+0, w,  -Hp(p),-Hp(p)- w, &
                ANIL(:,p), ANIR(:,p), ANI_SC(:,p))
        enddo
        do p=nb_min,nb_max
            do q=nb_min,nb_max
                if ( is_corr(p) .or. is_corr(q) ) cycle
                denom_c = Hp(p) - Hp(q) - w
                denom_d = Hp(p) - Hp(q) - w
                if (abs(denom_c) < 1.0d-5) then
                    cterm(:) = ANI_SC(:, p)
                else
                    cterm(:) = (ANIL(:,p) - ANIR(:,q)) /denom_c
                endif
                dterm(:) = (conjg(ANIL(:,p)) - ANIR(:,q)) / denom_d
                do pw=0,maxpower
                    res(:,pw) = res(:,pw) + Cnc(p,q,:)*(cterm(pw) - dterm(pw))
                enddo
            enddo
        enddo
    end function

    function CorrelatedContribution(w, meshL, meshR, MM) result(res)
        use ElectronicStructureMethod, only: MatrixInfo,ElectronicStructureProvider
        complex*16                          :: res(inp%num_dirs,0:maxpower)
        integer                             :: n_min, n_max
        integer                             :: meshL, meshR ! x, x-w corresp.
        complex*16, pointer, intent(in)     :: MM(:,:,:)
        real*8,     intent(in)              :: w

        complex*16,allocatable :: C(:,:,:), D(:,:,:)
        complex*16, allocatable:: ANIR(:,:),ANIL(:,:), ANI_SC(:,:)
        integer    :: p, q, pw
        complex*16 :: denom_c, denom_d, cterm(0:maxpower), dterm(0:maxpower)

        if (n_corr <= 0) return  ! no correlated bands

        !n_min = max(1, min_corr - 20)
        !n_max = min(max_corr + 20, n_basis)
        n_min = min_corr
        n_max = max_corr

        allocate(ANIR(0:maxpower,n_min:n_max),ANIL(0:maxpower,n_min:n_max),ANI_SC(0:maxpower,n_min:n_max))
        allocate(C(n_basis,n_basis,inp%num_dirs))
        allocate(D(n_basis,n_basis,inp%num_dirs))

        res=0.0
        call GetCandDMatrices(C, D, meshL, meshR, MM, n_min, n_max)
        do p=n_min,n_max
            call AnalyticIntegrals(gf%a_(meshL), gf%b_(meshL),&
                                  -gf%HPlus_(p,meshL),-gf%HPlus_(p,meshR) - w,&
                                   ANIL(:,p), ANIR(:,p), ANI_SC(:,p))
        enddo

        do p=n_min,n_max
            do q=n_min,n_max
                if (.not.is_corr(p) .and. .not.is_corr(q) ) cycle

                denom_c = gf%HPlus_(p,meshL)       - gf%HPlus_(q,meshR) - w
                denom_d = conjg(gf%HPlus_(p,meshL))- gf%HPlus_(q,meshR) - w

                if (abs(denom_c) < 1.0d-8) then
                    ! this is the special case when there is only one integral
                    cterm(:) =  ANI_SC(:, p)
                else
                    cterm(:) =  (ANIL(:,p)-ANIR(:,q)) / denom_c
                endif
                !D term always has 2 integrals (never has 0/0 problem in expansion)
                dterm(:) = (conjg(ANIL(:,p))- ANIR(:,q)) / denom_d

                if (maxval(abs(cterm)) > BIGQuantity) then
                    print *, 'Warning: cterm is very big at ', w, p, q, cterm,dterm
                endif
                if (maxval(abs(dterm)) > BIGQuantity) then
                    print *, 'Warning: dterm is very big at ', w, p, q, dterm,cterm
                endif

                do pw=0,maxpower
                    res(:,pw) = res(:,pw) + C(p,q,:)*cterm(pw) - D(p,q,:)*dterm(pw)
                end do
            enddo
        enddo

        deallocate(ANIR,ANIL, ANI_SC)
        deallocate(C,D)

    end function

    subroutine AnalyticIntegrals(a,b,cL,cR,ANIL,ANIR,ANI_SC)
        !! Calculates
        !!\\[
        !!\int dx\frac{x^{n}}{x+c}=\begin{cases}
        !!\ln(x+c) & n=0\\
        !!x-c\ln(x+c) & n=1\\
        !!\frac{x^{2}}{2}-cx+c^{2}\ln(x+c) & n=2
        !!\end{cases}
        !!\\]
        !!   between a and b, for c=cL and c=cR, putting the results in ANIL, ANIR correspondingly.
        !! ANI_SC is the special-integral, computed using the following expression:
        !!\\[
        !!\int dx\frac{x^{n}}{(x+c)^{2}}=\begin{cases}
        !!-\frac{1}{x+c} & n=0\\
        !!\frac{c}{c+x}+\ln(x+c) & n=1\\
        !!x-\frac{c^{2}}{x+c}-2c\ln(x+c) & n=2
        !!\end{cases}
        !!\\]
        !! using c=cL.

        real*8, intent(in)      :: a,b
        complex*16, intent(in)  :: cL,cR
        complex*16, intent(out) :: ANIR(0:maxpower), ANIL(0:maxpower), ANI_SC(0:maxpower)
        real*8                  :: d
        d = b-a
        ANIL(0) = log(1+d/(a+cL))  !log(b+cL) - log(a+cL)
        ANIR(0) = log(1+d/(a+cR))  !log(b+cR) - log(a+cR)

        ANIL(1) = d -cL*ANIL(0)
        ANIR(1) = d -cR*ANIR(0)

        ANIL(2) = (b*b-a*a)/2.0 - cL*ANIL(1)
        ANIR(2) = (b*b-a*a)/2.0 - cR*ANIR(1)

        ! in the special case there is only one term, therefore only *L terms
        ! we could separate them, but this way it is more efficient
        ANI_SC(0) =  -1/(b+cL) + 1/(a+cL)
        ANI_SC(1) = -cL*ANI_SC(0) + ANIL(0)
        ANI_SC(2) = d -cL*(ANI_SC(1) + ANIL(0))
    end subroutine

    subroutine AnalyticIntegrals2(cL,cR,ANIL,ANIR,ANI_SC)
        complex*16, intent(in)  :: cL,cR
        complex*16, intent(out) :: ANIR(0:maxpower), ANIL(0:maxpower),&
                                   ANI_SC(0:maxpower)
        complex*16 :: rcL, rcR
        real*8     :: dL,dR,T2
        ! L and R correspond to the 2 terms integrated with the C multiplier.
        T2 = inp%Temperature*inp%Temperature  ! adler: document this, relation to sommerfeld expansion
        dL = real(cL)*real(cL) +  aimag(cL)* aimag(cL)
        dR = real(cR)*real(cR) +  aimag(cR)* aimag(cR)
        rcL = conjg(cL) / dL  !1.0/cL
        rcR = conjg(cR) / dR  !1.0/cR

        ANIL(2) = PI2O3* rcL * T2
        ANIR(2) = PI2O3* rcR * T2

        ANIL(1) = -ANIL(2)*rcL
        ANIR(1) = -ANIR(2)*rcR

        ANIL(0) = rcL*(1.0 - ANIL(1))
        ANIR(0) = rcR*(1.0 - ANIR(1))

        ANI_SC(2) = -ANIL(1)
        ANI_SC(1) = -2.0*ANI_SC(2)*rcL

        ANI_SC(0) = (rcL - 1.5*ANI_SC(1))*rcL
    end subroutine


    subroutine GetCandDMatrices(C, D, meshL, meshR, MM, n_min, n_max)
        use ElectronicStructureMethod, only: MatrixInfo
        complex*16, intent(out)             :: C(n_basis,n_basis,inp%num_dirs),&
                                               D(n_basis,n_basis,inp%num_dirs)
        complex*16, intent(in)              :: MM(n_basis,n_basis,3)
        integer, intent(in)                 :: n_min, n_max
        integer                             :: meshL, meshR

        complex*16,pointer     :: Lp(:,:), Rp(:,:), Lm(:,:), Rm(:,:)
        complex*16,allocatable :: Lp_c(:,:), Rp_c(:,:),M1(:,:,:), M2(:,:,:), M3(:,:,:),&
                                  M4(:,:,:), tmp(:,:)
        integer    :: i

        allocate(Lp_c(n_corr,n_corr), Rp_c(n_corr,n_corr),&
                 M1(n_basis,n_basis,3), M2(n_basis,n_basis,3), M3(n_basis,n_basis,3),&
                 M4(n_basis,n_basis,3), tmp(n_corr,n_corr))

        Lp => gf%L_(:,:, meshL); Rp => gf%R_(:,:,meshL); Lm => gf%L_(:,:,meshR); Rm => gf%R_(:,:,meshR)
        ! non correlated orbitals assigned first
        M1 = MM; M2 = MM; M3 = MM; M4 = MM
        ! Calculation of matrices C and D that are multiplied to the double denominator
        ! C is for equal sign and D for opposite sign
        Lp_c = conjg(transpose(Lp))
        Rp_c = conjg(transpose(Rp))

        do i=1,3
            ! inside the small window
            tmp = matmul(Lm, MM(min_corr:max_corr,min_corr:max_corr,i))
            M1(min_corr:max_corr,   min_corr:max_corr,   i) = matmul(tmp, Rp)
            M3(min_corr:max_corr,   min_corr:max_corr,   i) = matmul(tmp, Lp_c)

            ! cross terms, where one index is inside window, and one is outsideGetCandDMatrices
            if (n_min < min_corr) then
                M1(n_min:min_corr-1,min_corr:max_corr,   i) = matmul(MM(n_min:min_corr-1,min_corr:max_corr,i),Rp)
                M1(min_corr:max_corr,   n_min:min_corr-1,i) = matmul(Lm, MM(min_corr:max_corr,n_min:min_corr-1,i))
                M3(n_min:min_corr-1,min_corr:max_corr,   i) = matmul(MM(n_min:min_corr-1,min_corr:max_corr,i), Lp_c)
                M3(min_corr:max_corr,   n_min:min_corr-1,i) = M1(min_corr:max_corr,n_min:min_corr-1,i)
            endif
            if (n_max > max_corr) then
                M1(max_corr+1:n_max,min_corr:max_corr,   i) = matmul(MM(max_corr+1:n_max,min_corr:max_corr,i),Rp)
                M1(min_corr:max_corr,   max_corr+1:n_max,i) = matmul(Lm, MM(min_corr:max_corr,max_corr+1:n_max,i))
                M3(max_corr+1:n_max,min_corr:max_corr,   i) = matmul(MM(max_corr+1:n_max,min_corr:max_corr,i), Lp_c)
                M3(min_corr:max_corr,max_corr+1:n_max,   i) = M1(min_corr:max_corr,max_corr+1:n_max,i)
            endif

            ! inside the small window
            tmp = matmul(MM(min_corr:max_corr,min_corr:max_corr,i), Rm)
            M2(min_corr:max_corr,   min_corr:max_corr,   i) = matmul(Lp, tmp)
            M4(min_corr:max_corr,   min_corr:max_corr,   i) = matmul(Rp_c, tmp)

            ! cross terms, where one index is inside window, and one is outside
            if (n_min < min_corr) then
                M2(n_min:min_corr-1,min_corr:max_corr,   i) = matmul(MM(n_min:min_corr-1,min_corr:max_corr,i),Rm)
                M2(min_corr:max_corr,   n_min:min_corr-1,i) = matmul(Lp, MM(min_corr:max_corr,n_min:min_corr-1,i))
                M4(n_min:min_corr-1,min_corr:max_corr,   i) = M2(n_min:min_corr-1,min_corr:max_corr,   i)
                M4(min_corr:max_corr,   n_min:min_corr-1,i) = matmul(Lp_c, MM(min_corr:max_corr,n_min:min_corr-1,i))
            endif
            if (n_max > max_corr) then
                M2(max_corr+1:n_max,min_corr:max_corr,   i) = matmul(MM(max_corr+1:n_max,min_corr:max_corr,i),Rm)
                M2(min_corr:max_corr,   max_corr+1:n_max,i) = matmul(Lp, MM(min_corr:max_corr,max_corr+1:n_max,i))
                M4(max_corr+1:n_max,min_corr:max_corr,   i) = M2(max_corr+1:n_max,min_corr:max_corr,   i)
                M4(min_corr:max_corr,   max_corr+1:n_max,i) = matmul(Lp_c, MM(min_corr:max_corr,max_corr+1:n_max,i))
            endif
        enddo

        call TransformVelocityMatrix(n_min,n_max, M2, M1, C)
        call TransformVelocityMatrix(n_min,n_max, M4, M3, D)

        C = dreal(C)
        D = dreal(D)

        deallocate(Lp_c, Rp_c,M1, M2, M3, M4, tmp)
    end subroutine

    subroutine TransformVelocityMatrix(lb, ub, A, B, C)
        use ElectronicStructureMethod, only: MatrixInfo
        integer, intent(in)            :: lb, ub
        complex*16, intent(in)         :: A(:,:,:), B(:,:,:)
        complex*16, intent(inout)      :: C(:,:,:)
        integer                        :: ip, iq, dir
        !complex*16                     :: tmp(inp%num_dirs)

        do ip=lb,ub
            do iq=lb,ub
                if (.not.is_corr(ip) .AND. .not.is_corr(iq)) cycle
                do dir=1,inp%num_dirs  ! assume diagonal dirs, num_dirs = 3
                    C(ip,iq,dir) = A(ip,iq,dir) * B(iq,ip,dir)
                enddo
            enddo
        enddo
    end subroutine

    real*8 function ferm(x)
        real*8, intent(in) :: x
        if (x > 100) then
            ferm = 0.0
            return
        endif
        if (x < -100) then
            ferm=1.0
            return
        endif
        ferm = 1./(1.+exp(x))
        return
    end function

end module TransportImp
