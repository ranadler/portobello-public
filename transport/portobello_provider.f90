module PortobelloProvider
    !!
    !!  Interface portobello Greens function and velocity matrix.
    !!  This provides an implementation of the abstract interface [[ElectronicStructureMethod:ElectronicStructureProvider]],
    !!  allowing algorithms to traverse the data structures of DMFT/G outputs, without being aware of all the technical details.
    !!
    use ElectronicStructureMethod, only:ElectronicStructureProvider
    use TransportOptics

    implicit none
    private


    ! There is no data here, only methods
    type, extends(ElectronicStructureProvider) :: ESP
        private
    contains
        procedure, public, pass(self) :: InvokeOverKPoints => SumOverKPoints
        procedure, public, pass(self) :: GetHamltonianPlus => GetHamltonianPlus
        procedure, public, pass(self) :: GetMomentumMatrix => GetMomentumMatrix
        procedure, public, pass(self) :: IsSameBand => IsSameBand

    end type
    public :: ComputeFromPortobelloImp

    type(EmbeddedSelfEnergyOnRealOmega),allocatable :: ese
    type(Input), allocatable :: inp
    complex*16, allocatable,target :: VMat(:,:,:)
    complex*16,allocatable :: ham(:,:)
    integer :: k, k0, ispin, iomega, ind_k

contains

    function ComputeFromPortobelloImp(inp_location) result(out_location)
        use transportImp, only:Compute
        use stringifor
        use solid_mod, only: amega
        use parallel_mod, only: maswrk
        use manager_mod, only:chem_pot


        real*8 :: start_time, end_time
        type(string), intent(in) :: inp_location

        type(string) :: out_location
        type(ESP)  :: dmft_provider
        type(Output):: res

        if (maswrk) then
            print *, "- unit volume is ", amega
            print *, "- chemical potential in Fortran:", chem_pot
        end if
        allocate(inp)

        call inp%load(inp_location%chars())
        if (maswrk) call cpu_time(start_time)

        ! allocate the hamiltonian once, its size, the number of bands in the low energy subspace
        ! does not change in portobello
        allocate(ham(inp%min_band:inp%max_band,inp%min_band:inp%max_band))

        call Compute(dmft_provider, inp)

        if (maswrk) call cpu_time(end_time)

        deallocate(inp, ham)
        ! TODO: Print time elapsed

        out_location = './transport.h5:/output'
    end function


    subroutine SumOverKPoints(self, alg, symmetrize_bz, with_mm)
        use atom_mod
        use manager_mod
        use parallel_mod
        use units_mod
        use solid_mod
        use penf_stringify
        use stringifor
        use velocity_matrix
        !use com_mpi, only: nprocs, myrank
        use ElectronicStructureMethod
        class(ESP), intent(inout) :: self
        logical, intent(in) :: symmetrize_bz, with_mm
        class(Algorithm), intent(inout) ::  alg
        integer :: n, ib, ibnd, star,i_star,i, max_omega, maxbands
        type(MatrixInfo) :: matrix_info
        complex*16, allocatable ::  ZZ(:,:), AA(:,:)
        type(string) :: istr
        integer :: spin_factor

        allocate(AA(nbasmpw, nbndf), ZZ(nfun, nbndf), ese)

        istr = trim(str(me3_k,no_sign=.True.))

        ! load the shard that corresponds to our k-points
        if (inp%min_band <= inp%max_band) then
            call ese%load("./shard-"// istr // ".core.h5:/")
        end if

        max_omega = ese%max_omega  ! TODO: what if ese does not exist?
        maxbands = maxval(n_bnd)
        spin_factor = nspin
        if (nrel == 2) spin_factor = 1

        do ispin=1,nspin
            ! iterate over *my* shard of IRR BZ kpoints
            do ind_k=1,ndim3_k(me3_k+1)
                k0=n3_mpi_k(me3_k+1)+ind_k
                k = k0 
                n=n_bnd(k0, ispin)
                allocate(VMat(n,n,3))

                matrix_info = MatrixInfo(&
                    kp_index=k0,&
                    spin_factor=spin_factor,&
                    sym_index=1,&  !sym_index will be overridden
                    VOL=amega,&
                    n_basis=n, &
                    n_corr=inp%max_band - inp%min_band + 1, &
                    n_basis_max=maxbands,&
                    min_corr=inp%min_band, max_corr=inp%max_band,&
                    wgh=1.0) ! will be overridden

                if (symmetrize_bz) then
                    star = k_star(k0)
                else
                    star = 1
                endif
                ! weight for GF
                matrix_info%wgh = k_star(k0) *1.0d0 / nqdiv
                matrix_info%sym_index = 1
                iomega = -max_omega  ! starting omega index, for GetHamltonianPlus() calls,
                ! which increment it by one
                call alg%PrepareKPoint(self, matrix_info)

                matrix_info%wgh = k_star(k0) *1.0d0 / nqdiv / star
                do i_star=1,star
                    k=k_list(i_star,k0)

                    matrix_info%sym_index = i_star

                    ! calculate VMat, the gradient matrix
                    if (inp%momentum_kind == 'gradient') then
                        VMat = 0.0
                        if (nrel < 2) then
                            call VKMatrixNonrel(nbndf,n,nbask(k0),k,ispin, z_bnd(:,:,ind_k,ispin),ev_bnd(:,:,ind_k,ispin),VMat, indgb(1,k), .false.)
                       else
                            ! here we rotate the matrix within the call
                            call VKMatrixRel(nbndf,n,nbask(k0),k,ispin, z_bnd(:,:,ind_k,ispin),ev_bnd(:,:,ind_k,ispin),VMat, indgb(1,k), .false.)
                        endif
                        if (inp%test_velocity) then  
                            call testVelocity
                        end if
                    elseif (inp%momentum_kind == 'k') then
                        VMat = 0.0
                        do i=1,n
                            VMat(i,i,:) = pnt(:,k)
                        end do
                    endif
                    call alg%ProcessKPoint(self, matrix_info)
                end do ! over a star
                
                deallocate(VMat)
                call alg%EndKPoint(self, matrix_info)

            end do !! loop over k0 points (npnt of these)
        end do

        deallocate(AA, ZZ, ese)

    contains
        subroutine TestVelocity()
            integer :: i,j,dd
            do dd=1,3
                do i=1,n
                    do j=i,n
                        if (abs(VMat(i,j,dd)-conjg(VMat(j,i,dd))) > 1.0e-5) then
                            if (i==j) then
                                print *, "Not Hermitian diagonal: ", i,VMat(i,j,dd)-conjg(VMat(j,i,dd))
                            else
                                print *, "Not Hermitian: ", i,j
                                print *, VMat(i,j,dd)-conjg(VMat(j,i,dd))
                            endif
                        end if
                    end do
                end do
            end do
        end subroutine
    end subroutine SumOverKPoints

    function GetMomentumMatrix(self,nb_min,nb_max) result(MM)
        use solid_mod, only: n_bnd
        class(ESP), intent(inout) :: self
        complex*16, pointer :: MM(:,:,:)
        integer,intent(out) :: nb_min,nb_max
        MM => VMat
        nb_min = 1
        nb_max = n_bnd(k0, ispin)
    end function


    function IsSameBand(self, p, q) result(res)
        class(ESP), intent(inout) :: self
        integer,intent(in)        :: p,q
        logical                   :: res
        res = .FALSE. ! they are never the same
    end function


    subroutine ClipBasisAroundEF(self, w, nb_min, nb_max, n_min, n_max)
        class(ESP), intent(inout) :: self
        real*8, intent(in)  :: w
        integer, intent(in) :: nb_min, nb_max
        integer, intent(out):: n_min, n_max
        ! these come from the projector defined in python
        n_min = inp%min_band
        n_max = inp%max_band
    end subroutine

    ! MISNOMER
    ! This is \epsilon_k
    subroutine GetHamltonianPlus(self, womega, diagonal, L, R)
        use SortedComplexEigsys
        use solid_mod, only:e_bnd, i_kref, n_bnd
        use manager_mod, only:chem_pot
        use TransportImp, only:Ry2eV
        use parallel_mod, only:me3_k, n3_mpi_k
        use mpi
        class(ESP), intent(inout) :: self
        complex*16, intent(out)   :: diagonal(1:), L(:,:), R(:, :)
        real*8, intent(in)        :: womega
        integer :: ib, ierr

        !initialized the non-correlated part
        diagonal(1:inp%min_band-1)                = e_bnd(1:inp%min_band-1, k0, ispin)  - chem_pot
        diagonal(inp%max_band+1:n_bnd(k0, ispin)) = e_bnd(inp%max_band+1:n_bnd(k0, ispin), k0, ispin)  - chem_pot

        if (inp%max_band >= inp%min_band) then
            ! initialize the correlated part
            ham = 0.d0
            do ib=inp%min_band,inp%max_band
                ham(ib,ib) =  e_bnd(ib,k0,ispin) - chem_pot
            end do
            if (abs(ese%omegaRy_(iomega) - womega) > 1.0d-10) then
                print *, "Mismatch in omega, this is a bug!", iomega, ese%omegaRy_(iomega), womega
                call MPI_ABORT(MPI_COMM_WORLD, 30, ierr)
            end if

            if (ese%actual_k_(ind_k + n3_mpi_k(me3_k+1)) /= k0) then
                print *, "Mismatch in k for self energy:", ind_k, ese%actual_k_(ind_k+n3_mpi_k(me3_k+1)), k0, me3_k
                call MPI_ABORT(MPI_COMM_WORLD, 40, ierr)
            end if

            ham(:,:) = ham(:,:) + ese%M_(:,:,iomega, k0, ispin) / Ry2eV

            ! diagonalize the correlated part (with ordered eigenvalus)
            call eigsys(ham, diagonal(inp%min_band:inp%max_band), L(:,:), R(:,:), inp%max_band-inp%min_band+1)
            !print *, 'EVs:', diagonal(inp%min_band-2:inp%min_band+2)
        end if

        iomega = iomega+1 ! more efficient than searching womega


    end subroutine


end module
