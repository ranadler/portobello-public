# Portobello  - Portable Object Bus for Electronic Structure Exploration

<img src="portobello.png"  width="120">


## Setting the Environment for running Portobello

The full list of **python3** dependencies are listed in required.txt:

```
# this works in python3.6
# run
#      pip install -r required.txt
# to install all of this list automatically
#
cython
pymatgen==2020.4.29
pyhull==1.5.7
vtk==8.1.2
numba==0.48.0
mpi4py==3.0.2
fortranformat==0.2.5
quadprog
watchdog==0.9.0
matplotlib==2.2.2
nptyping
# get python3.6-tk through apt-get install
# Important: see separate instructions for installing h5py from *source*
```
Once the Portobello libraries are compiled, running the code just requires modifying the LD_LIBRARY_PATH and PYTHONPATH accordingly. In environments where Portobello is deployed this is usually encapsulated in a shell-module.

We recommend setting PORTOBELLO_ROOT to the directory where Portobello is installed (although this variable is not used by the applications).
Portobello loads fortran code from the LD_LIBRARY_PATH in the form of shared object files.
The files loaded by Python are designated in the variable FORTRAN_INTEL_LD_PATH, which is a list of files, separated by : . For example:
```
export FORTRAN_INTEL_LD_PATH=librhobusta.so
```
is necessary for running rhobusta.
The LD_LIBRARY_PATH needs to contain the directory that contains this shared object, as well as the lib directories for HDF5 and for the linker (for example Intel's linker):

```
export LD_LIBRARY_PATH=${PORTOBELLO_ROOT}/build/intel18.0.3/:${HDF5_INTEL_LIB}:${MPI2_BASE}/lib:${LD_LIBRARY_PATH}
```

Also in order to run the Python code, the PYTHONPATH needs to include the Portobello installation:

```
export PYTHONPATH=${PORTOBELLO_ROOT}
```

## Compilation

Portobello is now supported exclusively on **python3**, there will be no ongoing development on python2. We require python3.5 or higher.

Portobello requires a specific choice of compiler (either Intel or GNU). 
* Intel 18.0 is the version that is being used for development, and currently we do not recommend working with higher versions, as they have not been tested. 
* GNU platform version 7.4 is supported, although only the dft+dmft aspect was tested thoroughly. The GNU compiler still does not offer full support of all modern Fortran featrues. Specifically missing is support of object finalization. Currently we cannot automatically destroy Portobello sub-objects in a finalizer, and code that relies on this feature needs to destroy the objects explicitly. Makefile.gfort can be used to compile the dft code, and Makefile.CTQMC-gnu compiles the CTQMC code.

All code must be compiled with the same compiler technology, as fortran module compiled in different compilers are *not* compatible.

Portobello requires MPI2 (or higher). We tested it only on openmpi-3, which was compiled with the same version of the Intel compiler (C, C++ and Fortran). 
The mpi4py package in Python is required, but it does not need to be compiled in any specific way.

Portobello also requires **HDF5**, and its source needs to be compiled with the same compiler as the source. The same is true for the **h5py** plugin for Python.

For compatibility and easy debugging, we require that the HDF5 version is current >= 1.8.20. It can be downloaded from their website.
To make the HDF5 installation, for example with ifort, use:

```
make clean
./configure --enable-fortran2003 --enable-fortran FC=ifort CC=icc
make install
```

To install **h5py**, the Python interface to HDF5, download it from https://github.com/h5py/h5py and compile it with the same compiler and the HDF5 that was installed above:

```
python3 setup.py configure --hdf5=<path/to/hdf5>
python3 ./setup.py build 
python3 ./setup.py install
```

Beofre running configure, you may want to delete the .build direcotry, since it may contain object files compiled with different options.
We assume that this installation hides and precedes any other installation of the h5py package.

In order to use the provided Makefile, the following environment variables have to be defined:

```
export TARGET_OUTPUT_DIR=<location where the final libraries and executables are deployed>

export INTEL_PATH=<root under which include/ and /compiler/lib/intel64/ file are available, usually defined by the administrator when ifort is installed>
export MPI2_BASE=<the MPI2 installation directory with include/ and lib/>
export HDF5_INTEL=<the above HDF5 directory>
export MKLROOT=<top directory for MKL>

# for CTQMC only
export BOOST_DIR=<path for boost (required by CTQMC)>

```

Now the last step is just:
```
make
```

### Notes for MPICH

There is a bug in the interaction between mpi4py and MPICH ( https://lists.mpich.org/pipermail/discuss/2020-June/006006.html ) which causes certain MPI operations to fail. This leads to mysterious looking errors -- segfaults, NaN results, and/or complaints linear algebra libraries. To solve the problem, one must first locate the fortran mpi library on their system, e.g., libmpifort.so or libfmpich_intel_dpm.so. Then, before running portobello modules, you should preload this library. To do so, add the library to the environmental variable ```LD_PRELOAD``` as follows:
```
export LD_PRELOAD=/path/to/mpi/lib/libmpifort.so
```

## Standard settings and shortcuts


## Running Executables


### Local Symmetry Analysis (symm -> portobello/rhobusta/local_symmetry.py)
**symm** takes a structure file (cif, struct, etc.) and a name of a species (atom name, for example Fe, or Mn), as well as the name of a basis (see below), and using group representation theory (specifically a theorem by Wigner) outputs the structure of any local Hamiltonian that H that commutes with the local point group: [H,g]=0 for all g in the local group. It also visualizes the locality of the given atom in the crystal.
The basis is modified if -R is used, in which case the default native basis is FC.

For example, analyzing symmetry of LaVO3 in the default non-relativistic basis (which is real spherical harmonics) is done using:
```
symm -sV LaVO3.struct
```

![Alt](LaVO3.png "LaVO3 symmetry analysis")

```
Options:
  -h, --help            show this help message and exit
  -s SPECIES, --species=SPECIES
  -l L, --ang-momentum=L
  -R, --fully-relativistic
  -B BASIS, --basis=BASIS
     Name basis to use:
     "native": for non-relativistic = RSH labeld by L,L_z, 
               for relativistic - RSH labeled by J=L+S,J_z
     "FC": basis for f that diagonalizes H,G in cubic field, real-spherical harmonics for l>3
           this is more useful than RSH because it diagonalizes the rep. for a lot of f crystal fields
           whereas RSH doesnt (default)
     "RSH": real spherical harmonics,
     "SH": spherical harmonics

```
### DFT, DFT+X (rhobusta -> portobello/rhobusta/rhobusta.py)
**rhobusta** runs FLAPWMBPT after generating input on the fly.
An easy way to get started is just by typing:
```
rhobusta --info <input-file>
```
which prints some self-document information about the run but does not do anything. This typically looks like:

```
- structure does not contain magnetic information
- equivalent sites in final cell (considering spin pol. as well): [[0, 1], [2, 3]]
- number of symmetry ops (in final cell): 16
- Generating hdf5 input for _Fe2Se2
space group: 129, lattice: tetragonal, Hall: P 4ab 2ab -1ab
----- NOTE: PyMatGen lengths below are in Angstrom, not Bohr as later ------
SymmetrizedStructure
Full Formula (Fe2 Se2)
Reduced Formula: FeSe
abc   :   3.765800   3.765800   5.498800
angles:  90.000000  90.000000  90.000000
Sites (4)
  #  SP      a    b       c  Wyckoff
---  ----  ---  ---  ------  ---------
  0  Fe2+  0.5  0.5  0       2a
  1  Se2-  0    0.5  0.2663  2c
----------------------------------------------------------------------------
distance matrix (Å):
[[2.6628 2.3853]
 [2.3853 3.7008]]
finished radius optimization
   optimal MT radius of Fe2+: 1.272771Å for RK: 7.761 [goal 8.000000]
   optimal MT radius of Se2-: 1.112512Å for RK: 6.784 [goal 7.500000]
Kmax is 3.227
ndiv (k-mesh for Band basis) is [10, 10, 6], size: 600
nrdiv (mesh for unit cell / PW basis) is [6, 6, 8], size: 288
mdiv (mesh for potential) is [14, 14, 20], size: 3920
Fe2+[z=26] 1s² 2s² 2p⁶ ⟻   22.191 eV ⟼   3s² 3p⁶ 3d⁶ 4s² 
    valence cutoff: -3.360621 eV, #electrons: 16, max l: 8
    excited states: 4p 5s 4f 5g 6h 7i 8j 9k 
    FLAPW valence: 3s²  4s²⁎  5s⁰  3p⁶  4p⁰⁎  3d³©  3d³⁎  4f⁰⁎  5g⁰⁎  6h⁰⁎  7i⁰⁎  8j⁰⁎  9k⁰⁎  
Se2-[z=34] 1s² 2s² 2p⁶ 3s² 3p⁶ ⟻   3.542 eV ⟼   3d¹⁰ 4s² 4p⁴ 
    valence cutoff: -2.011392 eV, #electrons: 16, max l: 7
    excited states: 5s 4f 5g 6h 7i 8j 
    FLAPW valence: 4s²⁎  5s⁰  4p⁴⁎  3d¹⁰⁎  4f⁰⁎  5g⁰⁎  6h⁰⁎  7i⁰⁎  8j⁰⁎ 

```

Using the following command:

```
rhobusta <input-file> -T10
```

runs DFT with default parameters at temperature 10K. These can be tuned using the command line.

The LAPW basis can be controlled with the command line below, specifically:
* -K is used to multiply the default number of k-points
* --RK defines the RKmin constant - increase it above the default value to increase quality of the results (not above 12)
* --valence-cutoff or --hard-cutoff define a cutoff (in eV) for the valence electrons

Other parameters include (try rhobusta --help):

```
usage: rhobusta [-h] [--info] [-T TEMPERATURE] [--mpi-workers MPI_WORKERS]
                [-i NUM_CHARGE_ITERATIONS] [-R] [-K KMESHFACTOR] [-Q GOALRK]
                [--MT-radii MTRADII] [-f REDUCTION]
                [-V VALENCE_CUTOFF | --hard-cutoff HARD_CUTOFF] [--restart]
                [-S] [--SDFT] [-m B_SPEC] [--plus-sigma | -G]
                [--matsubara-exact-energy-limit MATSUBARA_EXACT_ENERGY_LIMIT]
                [-x ADMIX] [--fix-mu] [--use-given-cell]
                [-C CHARGE_CONVERSION_GOAL] [-P PLOTS]
                [input_file]

positional arguments:
  input_file            required if there is no ini.h5 file in this directory
                        (default: None)

optional arguments:
  -h, --help            show this help message and exit
  --info                Prepare input, don't run Rhobusta, only print
                        information. (default: True)
  -T TEMPERATURE, --Temperature TEMPERATURE
                        temperature is required, in Kelvin (default: None)
  --mpi-workers MPI_WORKERS
                        [0 -> not parallel], is the number of parallel workers
                        spawned. There should be this number + 1 slots
                        allocated in MPI for the workers and the supervisor.
                        (default: 0)
  -i NUM_CHARGE_ITERATIONS, --iterations NUM_CHARGE_ITERATIONS
                        number of iterations of the charge self consistency
                        equations (default: 10)
  -R, --fully-relativistic
                        run DFT in relativistic mode, which includes a
                        relativistic muffin-tin and a relativistic
                        interstitial (default: False)
  --restart             restart from syncpoint - do not create input, and read
                        sync point (default: False)
  -S, --spin-polarized  Run with 2 spins instead of 1 per state (default:
                        False)
  --SDFT                break spin symmetry in DFT (default: False)
  -m B_SPEC, --magnetic-field B_SPEC
                        if vector (3 numbers separated by ','), it describes
                        the full magnetic field, if a number, it is the size
                        of B along the z axis. (default: )
  -x ADMIX, --admix ADMIX
                        should be a floating point number between 0.0 and 1.0
                        - it is the admix used on every iteration (default:
                        0.1)
  --fix-mu              do not re-calculate mu any more - use the current
                        value (this can be runed on and off) (default: True)
  --use-given-cell      use the unit cell defined in the input file, do not
                        try to reduce it (mostly usefull if there is a bug in
                        the reduction) (default: False)
  -C CHARGE_CONVERSION_GOAL, --charge-conversion-goal CHARGE_CONVERSION_GOAL
                        goal of charge conversion. Computation will end when
                        it is hit (it may end before because of maxed out
                        iterations) (default: 1e-07)
  -P PLOTS, --plots PLOTS
                        number of ticks in one band-plot segment. If 0, it is
                        calculated. (default: None, no plotting). (default:
                        None)

LAPW inputs:
  parameters that control the generated LAPW basis and meshes

  -K KMESHFACTOR, --kmesh-factor KMESHFACTOR
                        size multiplier for the k-mesh used for integration
                        (default: 1.0)
  -Q GOALRK, --RK GOALRK
                        sets the Lmax=RKmax - all other RK factors scale
                        accordingly (default: 7.0)
  --MT-radii MTRADII    Name of strategy to generate MT radii: "equal-ratios":
                        divide the distance equally between neighbors
                        "optimized": radii to optimize basis (default:
                        optimized)
  -f REDUCTION, --radius-reduction-factor REDUCTION
                        1.0 means the spheres will possibly touch (no
                        reduction) (default: 1.0)
  -V VALENCE_CUTOFF, --valence-cutoff VALENCE_CUTOFF
                        an optimal cutoff will be searched higher than this
                        energy level [eV] (default: -6.0)
  --hard-cutoff HARD_CUTOFF
                        sets the valence cutoff to exactly this number [eV]
                        (default: None)

correlations:
  DFT+X parameters

  --plus-sigma          run DFT+Sigma, not just DFT. If self-energy is not
                        present, it is assumed 0 (default: False)
  -G, --with-gutz-density
                        run DFT+G, that is, DFT with a present Gutzwiller
                        solution (consisting of R and \lambda) (default:
                        False)
  --matsubara-exact-energy-limit MATSUBARA_EXACT_ENERGY_LIMIT
                        energy limit for exact self-energy frequency for the
                        summation algorithm [eV] (default: 16.0)
                        

```

The main output file of rhobusta which is used by further programs is flapw_image.h5.


Typical convergence output will look like:

```
 ...
 charge density self-consistency= 0.5858879E-05
 charge density self-consistency= 0.4945335E-05
 charge density self-consistency= 0.4174315E-05
 charge density self-consistency= 0.3523570E-05
 charge density self-consistency= 0.2974328E-05
 charge density self-consistency= 0.2510749E-05
 charge density self-consistency= 0.2119463E-05
 charge density self-consistency= 0.1789191E-05
 charge density self-consistency= 0.1510414E-05
 charge density self-consistency= 0.1275097E-05
 charge density self-consistency= 0.1076463E-05
 charge density self-consistency= 0.9087887E-06
 charge density self-consistency= 0.7672466E-06
 charge density self-consistency= 0.6477615E-06
 charge density self-consistency= 0.5468942E-06
 charge density self-consistency= 0.4617422E-06
 charge density self-consistency= 0.3898555E-06
 charge density self-consistency= 0.3291666E-06
 charge density self-consistency= 0.2779301E-06
 charge density self-consistency= 0.2346731E-06
 charge density self-consistency= 0.1981521E-06
 charge density self-consistency= 0.1673176E-06
 charge density self-consistency= 0.1412837E-06
 charge density self-consistency= 0.1193027E-06
 charge density self-consistency= 0.1007432E-06
 charge density self-consistency= 0.8507232E-07
charge conversion goal of 1.00e-07 reached, stopping DFT.

```

One can easily create a band plot using the -P0 command option, and use other tools to analyze the results.


#### Running Magnetic calculations

To run FM/AFM calculations, instead of .cif file, a **magnetic cif** file needs to be provided (mcif).
(elaborate how to create an mcif file)

### Creating a projector (proj -> portobello/rhobusta/ProjectorEmbedder.py)

After running rhobusta, a projector may be created using **proj**. It takes basically 2 parameters: a specification of a shell (or a list of shells) with -s, and an energy window, -w. For example:

```
proj -w-10:10 -sMn:3d

```
will create a projector on the 3d shell of Mn, using the real-spherical harmonics basis on the shell, from bands that are in the energy range -10eV to 10eV.
The result will be saved in the file projector.h5. Note that only the projector definition is saved in projector.h5, and it does not need to be re-created after the bandstructure is recalculated.

```
  -h, --help            show this help message and exit
  -s ORBITALS, --orbital-subshells=ORBITALS
                        List of comma-separated orbitals in the form N(el):n[spdfe] where:                             N is
                        N is the number of the atom in the unit cell (starting at
                        0), useful if there are multiple equivalence classes of the same kind of atom.
                        N can be dropped, in which case the first distinct element will (el) will be used.
                        el is the element (which can be dropped, but then N is needed)
                        For example 1Fe:2d,Se:1s,2:3d
  -w WINDOW, --energy-window=WINDOW
                        energy window for the bands (in eV) [default: -1.0:1.0]
  -B BASIS, --basis=BASIS
  					same as in symm, see above.                       
  -X APPROX_REP, --approx-rep=APPROX_REP
                        kind of approximation to use for the representation:
                        "diag": keep the diagonal only (even if there are nonzero offdiagonals),
                                if there are negative numbers on diagonal, number them
                                after the irreps thatwere identified.
                        "none": keep all off-diagonals, and number them,
                        "jj": J^2 - 2 blocks in the J basis, each for the j value,                         
                        "full": consider the full matrix, do not use the rep
  --store               

```

### Gutzwiller (gutz -> portobello/rhobusta/g/GA_complex.py)

**gutz** can be run after **rhobusta** creates the bands structure, and **proj** is run to create a projector.
An example run is:

```
gutz -T15 -U4.5 -J0.512 -N5 --solver=ied

```
This runs in temperature 15K, U=4.5eV, J=0.512 and FLL double-counting parameter Nf=5, with the default projector (projector.h5) on the given bandstructure. The solver can be defined per-run. **ied** is a parallel MPI solver using ARPACK with some optimizations.

```
Options:
  -h, --help            show this help message and exit
  --mpi-workers=MPI_WORKERS
                        [default: 0 -> not parallel], is the number of
                        parallel workers spawned.
                        There should be this number + 1 slots allocated in MPI
                        for the workers and the supervisor.
  -T TEMPERATURE, --Temperature=TEMPERATURE
                        Temperature in Kelvin
  -b BETA, --beta=BETA  [default: none]
  -w WINDOW, --energy-window=WINDOW
                        energy window for the projector (in eV) [default:
                        -5.0:5.0]
  -U U, --U_eV=U        U [default: 9.0 eV]
  -J J, --J_eV=J        J [default: 1.14 eV]
  -N N0, --N0=N0        Either a number (float, int), or 3 numbers separated
                        by colon (:).                       If number, it is
                        just the occupation used for double counting.
                        Otherwise, the 1st and last number designate a range
                        of occupations which sohuld                       be
                        used for the fock space, and the middle number should
                        be the double counting occupation as above.
                        For example 1:3:5 means the double counting N0=3, and
                        the range 1,2,3,4,5 will be used for construction
                        of the fock space. [default: 5]
  --no-rotate-projector
                        rotate the projector so that the local orbital resides
                        in the last dim bands for ever k-space
  --inputs              Only create inputs R=I Lambda=Eloc
  --no-crys-sym         using the irreducible representation for the local
                        crystal symmetry adapted basis
  -v VERBOSE            verbosity for the GA iterations. 1: print all
                        intermediate. 2: Print only the maxerror. 0: No
                        verbosity.
  --solver=SOLVER       ED solvers. spci, simple_ed, ged, ied
  -e ENERGY_LIMIT, --energy-limit=ENERGY_LIMIT
                        energy limit for self-energy frequency [default: 90.0
                        eV]
  -P, --dos             
  -p INITIAL_SPIN_POLARIZATION, --initial-polarization=INITIAL_SPIN_POLARIZATION
                        Fixed double counting [eV]
  -d FIXED_DOUBLE_COUNTING, --DC=FIXED_DOUBLE_COUNTING
                        Fixed double counting [eV]


```

### DMFT (**dmft** -> portobello/rhobusta/dmft.py)

**dmft** is run with very similar options to gutz.
for example 

```
dmft -T500 -U4.5 -J0.512 -N5 --mpi-workers=15
```

will run dmft with CTQMC on 15 mpi workers. This is more computationally intensive, so mpi-workers are usually necessary.

The runtime options for dmft are as follows:

```
usage: dmft [-h] [--full-interaction] [--no-green-bulla]
            [--not-precise-density-matrix] [--dmft-dry-run]
            [--projector PROJECTOR] [-U U] [-J J] [-N N0]
            [-D NUM_IMPURITY_ITERS] [-e ENERGY_LIMIT] [-E EXCLUSION_MARGIN]
            [-d FIXED_DOUBLE_COUNTING] [-p INITIAL_SPIN_POLARIZATION]
            [-b BETA | -T TEMPERATURE] [--mpi-workers MPI_WORKERS] [-w WINDOW]

optional arguments:
  -h, --help            show this help message and exit
  --dmft-dry-run        Dry run - don't call ImpuritySolver (default: False)
  --projector PROJECTOR
                        designates an object of type SelectedOrbitals, but
                        this one should have only correlated subspaces
                        (default: ./projector.h5:/def/)
  -D NUM_IMPURITY_ITERS, --imp-iterations NUM_IMPURITY_ITERS
                        number of iterations to solve the DMFT self-
                        consistency condition (default: 6)
  -e ENERGY_LIMIT, --energy-limit ENERGY_LIMIT
                        energy limit for self-energy frequency (default: 15.0)
  -E EXCLUSION_MARGIN, --exclusion-margin EXCLUSION_MARGIN
                        margin for exclusion beyond the disentanglement window
                        (in eV) (default: 2.0)
  -d FIXED_DOUBLE_COUNTING, --DC FIXED_DOUBLE_COUNTING
                        Fixed double counting [eV] (default: -1)
  -p INITIAL_SPIN_POLARIZATION, --initial-polarization INITIAL_SPIN_POLARIZATION
                        Fixed double counting [eV] (default: 0.0)
  -b BETA, --beta BETA
  -T TEMPERATURE, --Temperature TEMPERATURE
                        Temperature in Kelvin (default: None)
  --mpi-workers MPI_WORKERS
                        [default:1 -> not parallel, forks 1 worker], is the
                        number of parallel workers spawned. There should be
                        this number + 1 slots allocated in MPI for the workers
                        and the supervisor. (default: 1)
  -w WINDOW, --energy-window WINDOW
                        energy window for the cubic_harmonicsands (in eV)
                        (default: -10.0:10.0)

ctqmc parameters:
  modify ctqmc computation

  --full-interaction    True iff full interaction is to be used, instead of
                        Ising (default: False)
  --no-green-bulla
  --not-precise-density-matrix

model parameters:
  physical parameters of the local (many-body) Hamiltonian

  -U U, --U_eV U        U [eV] (default: None)
  -J J, --J_eV J        J [eV] (default: None)
  -N N0, --N0 N0        occupation number for nominal double counting
                        (default: None)

```

When this is run, one will typically see the projected Hamiltonian, Hloc printed, as well as the hybridization, evaluated at some high matsubara frequency, and at the 1st and last frequencies (the cuttof is defined by the ENERGY_LIMIT parameter above).
Here is a typical output (for a d-shell):

```
---> continuing after CTQMC
 - continuing after CTQMC from iteration 1.0
- DMFT energy difference from DFT:  -65.58341858
 - DC= 25.44 25.44
 - Hybridization matrix at very large omega 1000.000000 eV:
[[-0.0002-0.0216j -0.    +0.j      0.    +0.j      0.    -0.j     -0.    -0.j     -0.    +0.j      0.    -0.j     -0.    -0.j     -0.    -0.j     -0.    -0.j    ]
 [ 0.    +0.j     -0.0002-0.0216j  0.    +0.j      0.    -0.j      0.    +0.j      0.    -0.j     -0.    +0.j     -0.    -0.j      0.    -0.j     -0.    -0.j    ]
 [-0.    +0.j     -0.    +0.j     -0.0002-0.0205j -0.    +0.j     -0.    +0.j      0.    +0.j     -0.    -0.0014j -0.    -0.j      0.    -0.j     -0.    -0.j    ]
 [-0.    -0.j     -0.    -0.j      0.    +0.j     -0.0002-0.0205j -0.    -0.j      0.    -0.j     -0.    -0.j     -0.    -0.0014j  0.    +0.j     -0.    +0.j    ]
 [ 0.    -0.j     -0.    +0.j      0.    -0.j      0.    -0.j     -0.0002-0.0205j -0.    +0.j     -0.    +0.j     -0.    +0.j     -0.    -0.0013j -0.    -0.j    ]
 [ 0.    -0.j      0.    -0.j     -0.    +0.j     -0.    -0.j      0.    -0.j     -0.0002-0.0205j  0.    -0.j      0.    +0.j      0.    -0.j     -0.    -0.0013j]
 [-0.    -0.j      0.    +0.j     -0.    -0.0014j  0.    -0.j      0.    -0.j     -0.    -0.j     -0.0002-0.02j    0.    +0.j     -0.    +0.j      0.    +0.j    ]
 [ 0.    -0.j      0.    -0.j      0.    -0.j     -0.    -0.0014j  0.    +0.j     -0.    +0.j     -0.    +0.j     -0.0002-0.02j   -0.    -0.j      0.    -0.j    ]
 [ 0.    -0.j     -0.    -0.j     -0.    +0.j     -0.    +0.j     -0.    -0.0013j -0.    -0.j      0.    -0.j      0.    -0.j     -0.0002-0.02j    0.    -0.j    ]
 [ 0.    +0.j      0.    -0.j      0.    -0.j      0.    +0.j      0.    -0.j     -0.    -0.0013j -0.    +0.j     -0.    -0.j     -0.    -0.j     -0.0002-0.02j  ]]
iomega: 0 (0.243632 eV) [spin=0]
[[-1.5021-2.6116j -0.    +0.j      0.    -0.j      0.    -0.j      0.    -0.j     -0.    -0.j      0.    -0.j     -0.    -0.j      0.    -0.j     -0.    +0.j    ]
 [-0.    +0.j     -1.5021-2.6116j -0.    +0.j      0.    +0.j      0.    +0.j     -0.    +0.j      0.    +0.j      0.    -0.j      0.    -0.j     -0.    +0.j    ]
 [-0.    -0.j     -0.    +0.j     -1.4652-2.0184j -0.    -0.j      0.    +0.j      0.    -0.j     -1.4544-0.545j   0.    +0.j     -0.    -0.j     -0.    +0.j    ]
 [ 0.    -0.j      0.    +0.j     -0.    +0.j     -1.4652-2.0184j -0.    +0.j     -0.    -0.j      0.    -0.j     -1.4544-0.545j   0.    -0.j      0.    +0.j    ]
 [-0.    +0.j     -0.    -0.j     -0.    -0.j     -0.    -0.j     -1.4662-2.0181j -0.    -0.j      0.    +0.j      0.    +0.j     -1.4531-0.5454j  0.    +0.j    ]
 [ 0.    +0.j      0.    +0.j      0.    +0.j     -0.    +0.j     -0.    +0.j     -1.4662-2.0181j -0.    -0.j      0.    -0.j      0.    -0.j     -1.4531-0.5454j]
 [-0.    +0.j      0.    +0.j     -1.4544-0.545j   0.    +0.j     -0.    -0.j     -0.    +0.j     -0.4644-1.43j   -0.    -0.j      0.    +0.j      0.    -0.j    ]
 [-0.    -0.j     -0.    +0.j      0.    -0.j     -1.4544-0.545j   0.    -0.j      0.    +0.j     -0.    +0.j     -0.4644-1.43j   -0.    +0.j     -0.    -0.j    ]
 [-0.    -0.j     -0.    +0.j      0.    +0.j      0.    +0.j     -1.4531-0.5454j  0.    +0.j     -0.    -0.j     -0.    -0.j     -0.4661-1.4297j -0.    -0.j    ]
 [ 0.    -0.j      0.    -0.j     -0.    -0.j      0.    -0.j      0.    -0.j     -1.4531-0.5454j  0.    +0.j     -0.    +0.j     -0.    +0.j     -0.4661-1.4297j]]
iomega: 29 (14.374280 eV) [spin=0]
[[-0.5958-0.9271j -0.    +0.j      0.    +0.j     -0.    -0.j     -0.    -0.j     -0.    +0.j     -0.    -0.j     -0.    -0.j     -0.    -0.j     -0.    +0.j    ]
 [ 0.    +0.j     -0.5958-0.9271j  0.    +0.j      0.    -0.j      0.    -0.j      0.    -0.j      0.    +0.j     -0.    -0.j      0.    -0.j     -0.    -0.j    ]
 [-0.    +0.j      0.    +0.j     -0.559 -0.8592j  0.    -0.j     -0.    +0.j      0.    +0.j     -0.0454-0.0826j -0.    +0.j      0.    -0.j     -0.    -0.j    ]
 [-0.    -0.j     -0.    -0.j     -0.    +0.j     -0.559 -0.8592j -0.    -0.j      0.    -0.j      0.    -0.j     -0.0454-0.0826j  0.    +0.j     -0.    +0.j    ]
 [ 0.    -0.j     -0.    +0.j      0.    -0.j     -0.    -0.j     -0.5593-0.8596j  0.    -0.j     -0.    +0.j      0.    +0.j     -0.045 -0.0821j -0.    +0.j    ]
 [ 0.    -0.j     -0.    -0.j      0.    +0.j     -0.    +0.j     -0.    +0.j     -0.5593-0.8596j -0.    -0.j      0.    -0.j      0.    -0.j     -0.045 -0.0821j]
 [-0.    -0.j      0.    +0.j     -0.0454-0.0826j -0.    +0.j      0.    -0.j     -0.    -0.j     -0.5423-0.8275j  0.    -0.j     -0.    +0.j      0.    +0.j    ]
 [-0.    -0.j      0.    +0.j      0.    -0.j     -0.0454-0.0826j  0.    +0.j     -0.    +0.j     -0.    +0.j     -0.5423-0.8275j -0.    -0.j      0.    -0.j    ]
 [ 0.    -0.j     -0.    +0.j     -0.    +0.j      0.    +0.j     -0.045 -0.0821j -0.    +0.j      0.    -0.j     -0.    -0.j     -0.5428-0.8281j  0.    -0.j    ]
 [ 0.    -0.j      0.    -0.j     -0.    -0.j      0.    -0.j      0.    -0.j     -0.045 -0.0821j  0.    +0.j     -0.    +0.j     -0.    +0.j     -0.5428-0.8281j]]
- G measurement cutoff: 10 [eV]
Running 25 simulations per GPU
- Thermalization time: 14.000000 min., measurement time: 12.000000 min.
**** Spawning 35 MPI workers ****
Start task at Tue Apr 28 10:06:50 2020

```

Next, the CTQMC will run one iteration, which will take exactly 26 minutes (as written above), and a few dmft+ctqmc iterations will follow to solve the DMFT equations.


### Script for DFT+g (dft+g -> portobello/rhobusta/dft__g.py)


### Script for DFT+dmft (dft+dmft -> portobello/rhobusta/dft__dmft.py)



### Histograms (histogram-> portobello/rhobusta/histogram_plots.py)

### Dashboard (dashboard-> portobello/rhobusta/dashboard.py)

### Runtime updates to parameters


