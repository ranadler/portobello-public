
#include "ftlMacros.inc"

#define TNAME matrix
#define RANGE :,:
#define RANK 2
#define SECTION dims(1),dims(2)
#define EXTENTS extents(1)%lbound:extents(1)%ubound, extents(2)%lbound:extents(2)%ubound
#define INDICES m,n
#define ONES 1,1


module matrix

use, intrinsic :: iso_fortran_env, only : int8, int16, int32, int64, real32, real64
use hdf5_base

#include "tensors.inc"


end module
