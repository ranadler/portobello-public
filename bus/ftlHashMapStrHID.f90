
#define FTL_TEMPLATE_KEYTYPE type(string)
#define FTL_TEMPLATE_KEYTYPE_NAME Str
#define FTL_TEMPLATE_TYPE integer(HID_T)
#define FTL_TEMPLATE_TYPE_NAME HID
#define FTL_INSTANTIATE_TEMPLATE

module ftlHashMapStrHIDModule
    use hdf5
    use stringifor
#include <ftlHashMap.F90_template>
end module


! gcc CB does not recognize the dependencies in the included files
module ftlHashModuleDummy
    use stringifor
    use hdf5
    use ftlHashModule
    use ftlHashMapStrHIDModule
contains
function defaultMapStrID() result(res)
    type(ftlHashMapStrHID) :: res
    call res%New(10)
end function
end module

