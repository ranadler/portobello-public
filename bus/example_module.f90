
module example_module
    use example
    implicit none

contains

    ! this is the most minimal way I found to test that the interface of ComputeImp matches that of ComputeI - otherwise we don't
    ! have anything in the code to verify that, since the interface is invoked by Python.
    subroutine VerifyImpInterface(f)
        procedure(ComputeI) :: f
    contains
        subroutine CompileTimeTest()
            call VerifyImpInterface(ComputeImp)
        end subroutine
    end subroutine

    ! this is an implementation of Compute from ComputeInput, it can be invoked from python using generated code
    function ComputeImp(selfpath, low_energy, high_energy, i1, other) result(res)
            use iso_c_binding
            use stringifor
            use hdf5_base
            type(string) :: res
            type(string), intent(in) :: selfpath
            real(kind=c_float),intent(in),value :: low_energy
            real(kind=c_float),intent(in),value :: high_energy
            integer(kind=c_int),intent(in),value :: i1
            type(string), intent(in)            :: other
            type(ExampleOutput) :: eo

            print *, "Called Example's compute with ", selfpath%chars(), " ", low_energy, high_energy, i1, " ", other%chars()

            call eo%load(selfpath%chars())

            res = "output-string-from-compute" // eo%inp_per_orb(1,1)%full_path // "#" // eo%inp_per_orb(1,2)%full_path // "#"
     end function


    function SimpleCallImp(selfpath) result(res)
            use iso_c_binding
            use stringifor
            use hdf5_base
            type(string) :: res
            type(string), intent(in) :: selfpath

        type(Simple) :: s
        real(kind=dp) :: D(3,5)

        call s%load(selfpath%chars())
        !s%m = 3
        !s%n = 5
        !call s%allocate()

        print *, "Fortran shape:", shape(s%d_)
        print *, s%d_

        !D(1,:) = 1.1d0
        !D(2,:) = 2.d0
        !D(3,:) = 0.d0


        !s%d = D
        !s.d[0,1] = 10.0
        print *, "s.d(0,1):", s%d_(1,2)
        print *, "correct - transposed - s.d(1,0):", s%d_(2,1)

        s%d_(3,:) = 0.d0

        call s%store(selfpath%chars())

        res = ""

    end function


end module
