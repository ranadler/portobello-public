
#include "ftlMacros.inc"

#define TNAME vector
#define RANGE :
#define RANK 1
#define SECTION dims(1)
#define EXTENTS extents(1)%lbound:extents(1)%ubound
#define INDICES i
#define ONES 1


module vector

use, intrinsic :: iso_fortran_env, only : int8, int16, int32, int64, real32, real64
use hdf5_base

#include "tensors.inc"

end module
