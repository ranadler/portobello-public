

#define H5CALL(X) call h5eclear_f(self%hdf5error);  call X
#define H5CALL2(X) call h5eclear_f(pstore%hdf5error);  call X

module persistence
    use hdf5
    use stringifor
    use ftlHashModuleDummy
    use ftlHashMapStrHIDModule
    implicit none

    private
    public :: persistent_store, persistent_path, relative_path, GetStore, Die

    integer, parameter, public          :: dp = kind(1.0d0)

    integer, parameter :: debug_hdf5_ops = 0
    public :: debug_hdf5_ops


    ! this needs to be stored for compound type ids, such as complex.
    type type_ids
        integer(HID_T) :: logical_tid, complex_tid, datetime_tid
    contains

        procedure :: InitTIDS
    end type

    ! This is a singleton, and we don't want anyone to instantiate it
    ! or its implementation directly, only through the GetStore() method,
    ! which returns a pointer
    type,  abstract :: persistent_store
        private
        integer :: hdf5error                     = 0
        type(type_ids),public :: tids  !TODO: hide it
        type(ftlHashMapStrHID) :: file_ids

        logical :: is_initialized                = .false.
    contains
        ! need a map of (open, initialized) files, associated with their logical paths
        procedure :: InitStore  ! note this is private
        procedure,private :: Associate
        procedure, public :: Fetch
        procedure, public :: FetchSubGroup
        procedure,public :: FetchIndexInSubGroup
        procedure :: GetFileId

        generic :: DisableH5Msgs => DisableH5MsgsPS
        generic :: EnableH5Msgs => EnableH5MsgsPS
        generic :: HasError => HasErrorPS

        procedure :: DisableH5MsgsPS
        procedure :: EnableH5MsgsPS
        procedure :: HasErrorPS

        procedure :: DumpAllFieldIDs

        procedure :: flush

    end type


    type :: persistent_path
        private
        ! first component of the path is the logical name of the file
        type(string) :: full_path
        class(persistent_store), pointer,public :: pstore => null()

    contains
        procedure :: FetchGroup
        procedure, public :: print
    end type

    type, extends(persistent_path) :: relative_path
        private
        integer(HID_T) :: gid
    contains
        procedure :: FetchGroup => FetchRelativePath
    end type


    type, private, extends(persistent_store) :: persistent_store_imp
    contains
        final :: FinalizePersistentStore  ! flushes, and closes files
    end type


    ! the instance is private, so the only way to get it is using GetStore(), which will also run Init
    type(persistent_store_imp), target, save :: store_

    interface assignment (=)
        module procedure AssignPersistentPath
        module procedure AssignRelativePath
    end interface

    !make this private - it should be public only to friends, like Matrix.
    type, public :: complex_t
        real(kind=dp) :: r = 0.d0
        real(kind=dp) :: i = 0.d0
    end type

    enum, bind(C)
        enumerator :: FALSE = 0, TRUE
    end enum

    public :: FALSE, TRUE

    ! TODO: consider changing this from interface to generic pattern to circumvent ifort bug
    public :: AssignPersistentPath  ! if AssignPersistentPath is not public, ifort does not generate the correct assignment in client classes
    public :: AssignRelativePath


    interface
        subroutine FortranCBForOpenI(fid, logical_name) bind(C)
            use iso_c_binding
            use hdf5
            integer(HID_T), intent(in), value :: fid
            type(c_ptr),intent(in),value           :: logical_name
        end subroutine
    end interface

    procedure(FortranCBForOpenI), pointer :: fortranCBForOpen => null()


contains

subroutine Die()
    !use mpi
    include "mpif.h"
    integer :: ierror
    stop
    call MPI_FINALIZE(ierror)
end subroutine

! translate C pointer string to a fortran string
function FromCStr(str) result(res)
    use iso_c_binding
    type(c_ptr),intent(in),value :: str
    type(string) :: res
    character(kind=c_char,len=strlen(str)), pointer :: tmp
    interface
        pure function strlen ( string ) bind ( C ) result ( res )
            use iso_c_binding, only: c_ptr, c_size_t
            implicit none
            type(c_ptr), intent(in), value :: string
            integer(c_size_t) :: res
        end function
    end interface
    call c_f_pointer(str, tmp)
    res = tmp
end function


subroutine AssignFromPython(fid, logical_name) bind(C,name='persistence_mp_assignfrompython_')
    use iso_c_binding
    integer(HID_T), intent(in), value :: fid
    type(c_ptr),intent(in),value           :: logical_name

    class(persistent_store),pointer :: ps
    type(string) :: key

    key = FromCStr(logical_name)
    ps => GetStore()
    call ps%file_ids%Set(key, fid)
end subroutine


! Note this pointer needs to be freed eventually - if the call is made
! by python, than it is owned by it
function NewCStr(str) result(ret)
    use iso_c_binding
    character(*), intent(in) :: str
    type(c_ptr) :: ret

    character(len=len(str)+1), pointer :: ptr

    allocate(ptr)
    ptr(:) = str(:) // c_null_char
    ret = c_loc(ptr)
end function


subroutine register_open_cb(cb) bind(C,name="persistence_mp_register_open_cb_")
    use iso_c_binding
    type(c_funptr),intent(in),value        :: cb
    integer :: lng
    !print *, "***** registered Python CB for open", transfer(cb,lng)
    call c_f_procpointer(cb, fortranCBForOpen)
end subroutine


function GetStore() result(ret)
    class(persistent_store), pointer :: ret
    call store_%InitStore()
    ret => store_
end function


subroutine InitTIDS(self)
    use ISO_C_BINDING
    use datetime_module
    use h5t
    class(type_ids), intent(inout) :: self
    integer :: hdf5error
    type(complex_t),target :: inst(2)
    integer(kind(FALSE)) :: i
    integer(kind(FALSE)), target :: vali
    character(len=5) :: lnames(2) = ['FALSE', 'TRUE ']
    integer(HID_T) :: logical_file_tid
    type(c_ptr) :: f_ptr;
    type(datetime) :: dtinst(2)

    ! complex_tid
    call H5Tcreate_f(H5T_COMPOUND_F,&
        H5OFFSETOF(c_loc(inst(1)), c_loc(inst(2))),&
        self%complex_tid, hdf5error)
    call H5Tinsert_f(self%complex_tid, "r", &
        H5OFFSETOF(c_loc(inst(1)),c_loc(inst(1)%r)),&
        h5kind_to_type(8,H5_REAL_KIND), hdf5error)
    call H5Tinsert_f(self%complex_tid, "i",&
        H5OFFSETOF(c_loc(inst(1)),c_loc(inst(1)%i)),&
        h5kind_to_type(8,H5_REAL_KIND), hdf5error)

    !logical_tid
    call h5tenum_create_f(H5T_STD_I8LE, &  ! we pick this type to be h5py - compatible
        logical_file_tid, hdf5error)
    call h5tenum_create_f(h5kind_to_type(kind(FALSE), H5_INTEGER_KIND),self%logical_tid, hdf5error)
    do i = FALSE, TRUE
        vali = i
        call h5tenum_insert_f(self%logical_tid, trim(lnames(i+1)), vali, hdf5error)
        f_ptr = c_loc(vali)
        call h5tconvert_f(self%logical_tid, H5T_STD_I8LE, &
            int(1,SIZE_T), f_ptr, hdf5error)
        call h5tenum_insert_f(logical_file_tid, trim(lnames(i+1)), vali, hdf5error)
    end do
    call h5tclose_f(logical_file_tid,hdf5error) ! don't need to keep it around
end subroutine


subroutine DumpAllFieldIDs(self)
    class(persistent_store),intent(inout) :: self
    type(ftlHashMapStrHIDIterator) :: iter
    iter = self%file_ids%Begin()
    do while (iter /= self%file_ids%End())
#ifndef __GFORTRAN__
        print *, "file_ids: key=", iter%Key(), "value=", self%file_ids%Get(iter%Key())
        call iter%Inc()
#endif
    end do
end subroutine

subroutine FinalizePersistentStore(self)
    type(persistent_store_imp),intent(inout) :: self

    type(ftlHashMapStrHIDIterator) :: iter
    integer(HID_T) :: file_id
    print *, "------------- in Fortran flush persistent store -----------------"
    iter = self%file_ids%Begin()
    do while (iter /= self%file_ids%End())
        file_id = self%file_ids%Get(iter%Key())
        H5CALL( h5fflush_f(file_id, H5F_SCOPE_GLOBAL_F, self%hdf5error) )
        !H5CALL( h5fclose_f(file_id,  self%hdf5error) )  ! python will close them, or they will close at the end
        call iter%Inc()
    end do
    call self%file_ids%Clear()
end subroutine

subroutine DisableH5MsgsPS(self)
    class(persistent_store),intent(inout) :: self
    call h5eclear_f(self%hdf5error)
    call h5eset_auto_f(debug_hdf5_ops, self%hdf5error)
end subroutine

subroutine EnableH5MsgsPS(self)
    class(persistent_store),intent(inout) :: self
    call h5eset_auto_f(1, self%hdf5error)
    call h5eclear_f(self%hdf5error)
end subroutine


function HasErrorPS(self) result(res)
    class(persistent_store),intent(inout) :: self
    logical :: res
    res = self%hdf5error < 0
end function


function GetFileID(self, logical_name, create) result(fid)
    class(persistent_store),intent(inout) :: self
    type(string),intent(inout) :: logical_name
    logical, intent(in) :: create
    integer(HID_T) :: fid
    integer(HSIZE_T) :: file_size

    if (logical_name .in. self%file_ids) then
        fid = self%file_ids%Get(logical_name)
        self%hdf5error = 0
        call self%DisableH5Msgs()
        ! check validity of the fid from the file_ids
        call h5fget_filesize_f(fid, file_size, self%hdf5error)
        if (self%HasError()) then
            ! if not valid - continue to create a new association
            self%hdf5error = 0
            call self%EnableH5Msgs()
        else
            ! valid - return the fid that was found
            call self%EnableH5Msgs()
            return
        end if
    end if
    call self%Associate(logical_name%chars(), logical_name%chars(), create)
    fid = self%file_ids%Get(logical_name)
end function

subroutine print(self)
    class(persistent_path),intent(inout) :: self
    print *, "->Fortran persistent path=", self%full_path%chars()
end subroutine

subroutine AssignPersistentPath(self, right)
    type(persistent_path),intent(inout) :: self
    type(persistent_path),intent(in) :: right
    self%full_path  = right%full_path
    self%pstore => right%pstore
end subroutine

subroutine AssignRelativePath(self, right)
    type(relative_path),intent(inout) :: self
    type(relative_path),intent(in) :: right
    self%full_path  = right%full_path
    self%pstore => right%pstore
    self%gid = right%gid
end subroutine


subroutine flush(self, pp)
    class(persistent_store),intent(inout) :: self
    class(persistent_path),intent(in) :: pp

    type(ftlHashMapStrHIDIterator) :: iter
    integer(HID_T) :: file_id
    type(string),allocatable :: tokens(:)

    call pp%full_path%split(tokens, ':')
    if (tokens(1)%len() /= 0) then
          !print *, "-> Fortran flush ", tokens(1)
          file_id = self%file_ids%Get(tokens(1))
          H5CALL( h5fflush_f(file_id, H5F_SCOPE_GLOBAL_F, self%hdf5error) )
          !adler: should not erase the key!!!! call self%file_ids%EraseKey(tokens(1))
    end if
end subroutine

subroutine InitStore(self)
    class(persistent_store),intent(inout) :: self
    if (self%is_initialized) return
    !print *, "*** -> Fortran initialize persistent store"
    H5CALL( h5open_f(self%hdf5error) )
    call self%tids%InitTIDS()
    call self%file_ids%New(10)
    self%is_initialized = .True.
end subroutine

subroutine Associate(self, logical_name, name, create)
    class(persistent_store),intent(inout) :: self
    character(len=*), intent(in) :: name, logical_name
    logical, intent(in) :: create

    integer(HID_T) :: fid, prp_id
    type(string) :: key
    type(string) :: name_s
    logical :: is_core
    integer(8) :: increment

    is_core = .False.
    name_s = logical_name
    if (name_s%count('.core.') > 0) is_core = .True.

    if (is_core) then
        if (.not. create) then
            print *, "*** re-opening core files is a bug - this should not happen:", name
            call Die()
        end if

        if (create) then
            increment =  2**20
            call h5pcreate_f(H5P_FILE_ACCESS_F, prp_id, self%hdf5error)
            call h5pset_fapl_core_f(prp_id, increment, .False., self%hdf5error)
        end if
    else
        prp_id = H5P_DEFAULT_F
    end if

    ! if the logical name is already occupied, then override (closing other file).
    ! here we need to disconnect() all the objects that belong to this file.

    call self%DisableH5Msgs()
    if (create) then
        H5CALL( h5fcreate_f(name, H5F_ACC_TRUNC_F, fid, self%hdf5error, access_prp=prp_id) )
    else
        H5CALL( h5fopen_f(name, H5F_ACC_RDWR_F, fid, self%hdf5error, H5P_DEFAULT_F) )

        if (self%HasError()) then
            ! file does not exist
            !print *, '-> Fortran creating file ', name
            H5CALL( h5fcreate_f(name, H5F_ACC_TRUNC_F, fid, self%hdf5error, H5P_DEFAULT_F, H5P_DEFAULT_F) )
        end if
    end if
    if (self%HasError()) then
        print *, '--- cannot create / truncate file ', name, ' error=', self%hdf5error
        call Die()
    end if
    key = logical_name
    call self%file_ids%Set(key, fid)
    call self%EnableH5Msgs()
    if (associated(fortranCBForOpen)) then
        !print *, "!!!!!!!!!!!!! calling Python CB for open:", logical_name, fid
        call fortranCBForOpen(fid, NewCStr(logical_name))
        !print *, "after CB"
    end if
end subroutine


function Fetch(self, str) result(pp)
    class(persistent_store), target, intent(inout) :: self
    type(persistent_path) :: pp
    character(*), intent(in)   :: str
    !print *, pp%full_path%chars()
    pp%full_path = str
    pp%pstore => self
end function


function FetchIndexInSubGroup(self, gid, name, strindex, create) result(pp)
    class(persistent_store), target, intent(inout) :: self
    character(*), intent(in) :: name
    integer(HID_T), intent(in) :: gid
    type(string), intent(in) :: strindex
    logical, intent(in) :: create
    type(relative_path) :: pp

    type(relative_path) :: container
    container = self%FetchSubGroup(gid, name)
    pp = self%FetchSubGroup(container%FetchGroup(create), strindex%chars())
end function

function FetchSubGroup(self, gid, name) result(pp)
    class(persistent_store), target, intent(inout) :: self
    type(relative_path) :: pp
    integer(HID_T) :: gid
    character(*), intent(in)   :: name
    pp%pstore => self
    pp%full_path = "" ! not available here
    ! check if the group exists, if no - create it
    call self%DisableH5Msgs()
    call h5gopen_f(gid, name, pp%gid , self%hdf5error)
    if (self%HasError()) then
        H5CALL( h5gcreate_f(gid, name, pp%gid, self%hdf5error) )
    end if
    call self%EnableH5Msgs()
end function

function FetchGroup(self, create) result(gid)
    class(persistent_path), intent(in) :: self
    logical, intent(in) :: create
    class(persistent_store),pointer :: pstore
    integer(HID_T) :: gid
    type(string),allocatable :: tokens(:)
    type(string) :: path
    integer(HID_T) :: fid

    pstore => self%pstore
    call self%full_path%split(tokens, ':')
    if (tokens(1)%len() /= 0) then
        fid = pstore%GetFileID(tokens(1), create)
        path = tokens(2)
        gid = RecursiveOpenGroup(path)
    else
        gid = -1
    end if
    deallocate(tokens)
contains

    function RecursiveOpenGroup(path) result(gg)
        type(string),intent(inout) :: path
        integer(HID_T) :: gg, gg_tmp
        type(string),allocatable :: components(:)
        integer :: i
        call path%split(components, '/')
        call pstore%DisableH5Msgs()

        gg = fid
        do i=1,size(components)
            if (components(i)%len() == 0) return
            if (components(i) == '//') cycle
            call h5gopen_f(gg, components(i)%chars(), gg_tmp, pstore%hdf5error)

            if (pstore%HasError()) then
                ! creating group
                H5CALL2( h5gcreate_f(gg, components(i)%chars(), gg_tmp, pstore%hdf5error) )
                !print *, '-> Fortran created group, path=', path%chars(), ' fid=', fid, ' gid=',gid,&
                !    pstore%hdf5error
            end if

            gg = gg_tmp
        end do
        call pstore%EnableH5Msgs()
        deallocate(components)
    end function
end function

! this overrides FetchGroup for relative_path - the gid is already known
! but the absolute path is not known
function FetchRelativePath(self, create) result(gid)
    class(relative_path), intent(in) :: self
    logical, intent(in) :: create
    integer(HID_T) :: gid
    gid = self%gid
end function


end module
