
module TestBaseClasses

use hdf5_base

use matrix

use vector

use array5

use stringifor

use datetime_module

implicit none



!type, extends(persistent) :: ContainedRecord


!end type


type, extends(persistent) :: MyRecord

    complex(kind=dp)  :: cx = (0.d0, 0.d0)

    integer(kind=8)    :: i = 0

    real(kind=8)      :: r = 0.d0

    type(matrix_real) :: rmat
    real(kind=8),pointer :: rmat_(:,:)  ! this is a section variable

    type(matrix_real) :: rmat2
    real(kind=8),pointer :: rmat2_(:,:)  ! this is a section variable

    type(matrix_complex) :: cmat
    complex(kind=dp),pointer :: cmat_(:,:)  ! this is a section variable

    type(matrix_complex) :: cmat2
    complex(kind=dp),pointer :: cmat2_(:,:)  ! this is a section variable

    type(array5_real) :: ar5

    type(string)      :: s1

    type(string)      :: s2

    type(datetime)    :: dt

    logical :: l1 = .false.
    logical :: l2 = .false.

    type(vector_real) :: vec1
    real(kind=8),pointer :: vec1_(:)

    type(vector_real) :: vec2
    real(kind=8),pointer :: vec2_(:)

    type(vector_complex) :: vec3
    complex(kind=dp),pointer :: vec3_(:)

    contains

    procedure :: GetCmatExtents
    ! procedure like this for each dataset

    !------ next is only repeated once.

    procedure :: AllocateObjectFields => AllocateObjectFieldsImp

    procedure :: ResetSectionFields => ResetMyRecordSectionFields

    procedure :: StoreScalarFields => StoreScalarFieldsImp
    procedure :: StoreObjectFields => StoreObjectFieldsImp

    procedure :: LoadScalarFields => LoadScalarFieldsImp
    procedure :: LoadObjectFields => LoadObjectFieldsImp

    procedure :: DisconnectObjectFields => DisconnectObjectFieldsImp

    procedure :: IsEqual => IsMyRecordEqual
    procedure :: AssignmentOperator => AssignmentMyRecord

    procedure :: clear => ClearMyRecord

    final :: FinalizeMyRecord

end type

contains


subroutine ClearMyRecord(self)
    class(MyRecord), intent(inout) :: self
    type(MyRecord), save :: empty
    self = empty
end subroutine

subroutine ResetMyRecordSectionFields(self)
    class(MyRecord), intent(inout) :: self
    self%cmat_ => self%cmat%GetWithExtents(self%GetCmatExtents())
    ! etc ... for all other extent fields
end subroutine


subroutine AllocateObjectFieldsImp(self)
    class(MyRecord), intent(inout) :: self
    ! here all the extent information from the data def. is used
end subroutine


pure elemental function IsMyRecordEqual(lhs, rhs) result(iseq)
        class(MyRecord), intent(in) :: lhs
        class(ObjectContainer), intent(in) :: rhs
        logical :: iseq
    ! check rsh is also a MyRecord

    select type(rhs)
        type is (MyRecord)
            iseq = .true.
            iseq = iseq .and. (lhs%cx == rhs%cx)
            iseq = iseq .and. (lhs%i == rhs%i)
            iseq = iseq .and. (lhs%r == rhs%r)
            iseq = iseq .and. (lhs%s1 == rhs%s1)
            iseq = iseq .and. (lhs%s2 == rhs%s2)
            iseq = iseq .and. (lhs%dt == rhs%dt)
            iseq = iseq .and. (lhs%l1 .eqv. rhs%l1)
            iseq = iseq .and. (lhs%l2 .eqv. rhs%l2)
            if (.not. iseq) return

            iseq = iseq .and. (lhs%vec1 == rhs%vec1)
            iseq = iseq .and. (lhs%vec2 == rhs%vec2)
            iseq = iseq .and. (lhs%vec3 ==  rhs%vec3)

            iseq = iseq .and. (lhs%rmat == rhs%rmat)
            iseq = iseq .and. (lhs%rmat2 == rhs%rmat2)
            iseq = iseq .and. (lhs%cmat ==  rhs%cmat)
            iseq = iseq .and. (lhs%cmat2 == rhs%cmat2)

            iseq = iseq .and. (lhs%ar5 ==  rhs%ar5)
        class default
            iseq = .false.
    end select
end function

subroutine LoadObjectFieldsImp(self)
    class(MyRecord), intent(inout) :: self
    class(persistent_store),pointer :: ps
    integer(HID_T) :: gid
    ps  => self%GetPersistentStore()
    gid = self%GetGroupId()

    call self%vec1%LoadObject(ps, gid,  'vec1')
    call self%vec2%LoadObject(ps, gid, 'vec2')
    call self%vec3%LoadObject(ps, gid,  'vec3')

    call self%rmat%LoadObject(ps, gid,  'rmat')
    call self%rmat2%LoadObject(ps, gid, 'rmat2')
    call self%cmat%LoadObject(ps, gid,  'cmat')
    call self%cmat2%LoadObject(ps, gid, 'cmat2')

    call self%ar5%LoadObject(ps, gid,  'ar5')
end subroutine

subroutine StoreObjectFieldsImp(self)
    class(MyRecord), intent(inout) :: self

    class(persistent_store), pointer :: ps
    integer(HID_T) :: gid
    ps => self%GetPersistentStore()
    gid = self%GetGroupId()

    call self%vec1%StoreObject(ps, gid, 'vec1')
    call self%vec2%StoreObject(ps, gid, 'vec2')
    call self%vec3%StoreObject(ps, gid, 'vec3')

    call self%ar5%StoreObject(ps, gid, 'ar5')

    call self%rmat%StoreObject(ps, gid, 'rmat')
    call self%rmat2%StoreObject(ps, gid, 'rmat2')
    call self%cmat%StoreObject(ps, gid, 'cmat')
    call self%cmat2%StoreObject(ps, gid, 'cmat2')
end subroutine

subroutine DisconnectObjectFieldsImp(self)
    class(MyRecord), intent(inout) :: self

    call self%vec1%DisconnectFromStore()
    call self%vec2%DisconnectFromStore()
    call self%vec3%DisconnectFromStore()

    call self%ar5%DisconnectFromStore()

    call self%rmat%DisconnectFromStore()
    call self%rmat2%DisconnectFromStore()
    call self%cmat%DisconnectFromStore()
    call self%cmat2%DisconnectFromStore()

end subroutine

subroutine LoadScalarFieldsImp(self)
    class(MyRecord), intent(inout) :: self
    call self%read('i', self%i)
    call self%read('r', self%r)
    call self%read('dt', self%dt)
    call self%read('s1', self%s1)
    call self%read('s2', self%s2)
    call self%read('cx', self%cx)
    call self%read('l1', self%l1)
    call self%read('l2', self%l2)
end subroutine

subroutine StoreScalarFieldsImp(self)
    class(MyRecord), intent(inout) :: self
    call self%write('i', self%i)
    call self%write('r', self%r)
    call self%write('dt', self%dt)
    call self%write('s1', self%s1)
    call self%write('s2', self%s2)
    call self%write('cx', self%cx)
    call self%write('l1', self%l1)
    call self%write('l2', self%l2)
end subroutine

subroutine FinalizeMyRecord(self)
    type(MyRecord), intent(inout) :: self
    call self%disconnect()
end subroutine


subroutine AssignmentMyRecord(lhs, rhs)
    class(MyRecord), intent(inout) :: lhs
    class(ObjectContainer), intent(in) :: rhs

    select type(rhs)
        type is (MyRecord)
            lhs%cx = rhs%cx
            lhs%i = rhs%i
            lhs%r = rhs%r
            lhs%s1 = rhs%s1
            lhs%s2 = rhs%s2
            lhs%dt = rhs%dt
            lhs%l1 = rhs%l1
            lhs%l2 = rhs%l2

            lhs%vec1 = rhs%vec1
            lhs%vec2 =  rhs%vec2
            lhs%vec3 = rhs%vec3

            lhs%ar5 = rhs%ar5

            lhs%rmat = rhs%rmat
            lhs%rmat2 = rhs%rmat2
            lhs%cmat =  rhs%cmat
            lhs%cmat2 = rhs%cmat2

        class default
    end select
end subroutine


! these are per-dataset field
function GetCmatExtents(self) result(res)
    class(MyRecord), intent(inout) :: self
    type(extent) :: res(2)
    ! initialize extents
    res = [ extent(-1,0), extent(4,8) ]
end function

end module


subroutine test_matrix
    use hdf5_base

    use matrix
    use stringifor
    use TestBaseClasses

    implicit none

    type(matrix_real) :: mat1, mat11
    type(matrix_complex) :: mat2
    real(kind=8) :: arr1(2,3)
    complex(kind=dp) :: arr2(2,3)

    call mat1%init(2,3)

    arr2 = mat1%Get()

    arr2 = reshape([0.d0, 1.d0, 1.d0, 1.d0, -1.d0, 1.d0], [2,3] )

    print *, 'mat1: should be 0.0 ', mat1%Get()


    mat11 = mat1  ! overloaded operator

    arr1 = reshape([1.d0, 0.d0, 0.d0, 0.d0, 2.d0, 0.d0], [2,3] )

    mat1 =  arr1  ! overloaded operator


    print *, "mat1, should be as arr2", mat1%Get()

    arr1 = mat1%Get()  ! intel can't understand arr1 = mat1

    print *, "arr1, should be as arr2", arr1


end subroutine



program main
    use hdf5_base
    use stringifor
    use mod_datetime, only: datetime
    use TestBaseClasses
    use iso_c_binding
    use iso_fortran_env

    use tester

    implicit none

    !integer, parameter :: ascii = selected_char_kind ("ascii")

    class(persistent_store),pointer :: ps => null()
    type(persistent_path) :: pp
    type(MyRecord) :: rec1
    type(MyRecord) :: rec2
    type(MyRecord) :: res
    type(MyRecord) :: rec2copy
    type(MyRecord) :: res2copy
    complex(kind=dp),allocatable :: com(:,:)
    real(kind=8) ::ar32(32)
    integer :: i


    complex(kind=dp) :: CI = (0.d0, 0.d0), cmat(8)

    type(tester_t) :: test

    !call test%init()  ! TODO: not necessary, it is automatic on the first operation
    ps=> GetStore()
    !call ps%Associate('run_results', './test.hdf5')
    !call ps%Associate('wan_results', './test2.hdf5')

    pp = ps%Fetch('./test2.hdf5:/10')

    rec1%i = 999
    rec1%s1 = "Blah-blah-12345678" // c_null_char
    rec1%s2 = "not a very long string"//" with special char " // c_new_line // " --"
    rec1%r = 0.314d0
    rec1%cx = (70.0,28.0)
    rec1%l1 = .true.
    rec1%l2 = .false.

    rec1%dt = rec1%dt%now() + timedelta(days=-1)

    rec1%rmat = reshape([1.d0, 2.d0, 3.d0, 4.d0, 5.d0, 6.d0], [3,2])
    rec1%rmat2 = reshape([1.d0, 2.d0, 3.d0, 4.d0, 5.d0, 6.d0], [6,1])

    cmat = (/(0.0d0,0.0d0) ,(-3.496991526333D001,0.0d0) , &
        ( -3.944481647220D+000 , 0.0d0 ) , (-4.294180799072D+000 , 0.0d0 ) , &
        (-4.294180799072D+000, -1.0d0) , ( -3.944481647220D+000,-1.0d0 ) , &
        (-3.496991526333D-001,-1.0d0 ) , (0.0d0,-1.0d0)/)

    rec1%cmat = reshape(cmat, [2,4])
    rec1%cmat2 = reshape(cmat, [4,2])
    rec1%vec1 = [ 4.d0, 3.d0, 2.d0, 0.d0 ]
    rec1%vec2 = [ 1.d0, 0.d0, 1.d0, 0.d0 ]

    do i=1,32
        ar32(i) = sin(i * 0.2d0)
    end do

    print *, ar32

    rec1%ar5 = reshape(ar32, [2,2,2,2,2])

    rec1%vec3 = cmat

    call rec1%store(ps%Fetch('./test.hdf5:/10'))

    rec1%s2 = "this string is specifically for wan_results"

    call rec1%store(ps%Fetch('./test2.hdf5:/10'))

    call rec1%disconnect()
    rec1%s1 = 'xxx'
    rec1%s2 = 'XXX'

    call ps%flush()

    call rec2%load(ps%Fetch('./test.hdf5:/10'))

    call test%assert_equal(rec2%s1 == 'xxx', .false.)
    call test%assert_equal(rec2%s2 == 'XXX', .false.)
    call test%assert_equal(rec2%r == 0.314d0, .true.)
    call test%assert_equal(rec2%cx == (70.0,28.0), .true.)
    call test%assert_equal(rec2%rmat == rec1%rmat, .true.)

    call rec2%store(ps%Fetch('./test.hdf5:/mycopy'))

    call test%assert_equal(rec2%s1 == 'xxx', .false.)
    call test%assert_equal(rec2%s2 == 'XXX', .false.)
    call test%assert_equal(rec2%r == 0.314d0, .true.)
    call test%assert_equal(rec2%cx == (70.0,28.0), .true.)


    call ps%flush()

    ! try writing an empty object
    rec1%s2 = ''
    rec1%dt = rec1%dt%now()

    !rec1%dt = rec1%dt -timedelta(milliseconds=rec1%dt%getMillisecond())  !TODO: fix problem with comparing datetime - make sure to zero the unused parts

    rec2%cmat2 = 2.0 * rec1%cmat2%Get()

    call rec2%store('./test.hdf5:/rec2')
    call rec1%store('./test.hdf5:/rec1')

    call ps%flush()

    call res%load(ps%Fetch('./test.hdf5:/rec1'))
    call res%store(ps%Fetch('./test.hdf5:/rec1copy'))

    call ps%flush()

    call test%assert_equal(rec1 .eq. rec1, .true.)
    call test%assert_equal(rec1 .eq. rec2, .false.)
    call test%assert_equal(rec2 .eq. rec2, .true.)
    call test%assert_equal(res == rec1, .true.)
    call  test%assert_equal(res == rec2, .false.)

    rec2copy = rec2

    call  test%assert_equal(rec2copy == rec2, .true.)

    call rec2copy%load(ps%Fetch('./test.hdf5:/rec1'))

    call  test%assert_equal(rec2copy == rec2, .false.)

    call rec1%ResetSections()

    rec1%cmat_(-1,4) = (654.d0,-1.d0)   ! note the indices correspond to 1,1

    com = rec1%cmat%Get()

    call  test%assert_equal(com(1,1) == (654.d0,-1.d0), .true.)

    rec1 = res2copy ! this clears the values

    call rec2%clear()

    call  test%assert_equal(rec1==rec2, .true.)

    call test%print()

end program
