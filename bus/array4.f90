
#include "ftlMacros.inc"

#define TNAME array4
#define RANGE :,:,:,:
#define RANK 4
#define SECTION dims(1),dims(2),dims(3),dims(4)
#define EXTENTS extents(1)%lbound:extents(1)%ubound, extents(2)%lbound:extents(2)%ubound, \
                extents(3)%lbound:extents(3)%ubound, extents(4)%lbound:extents(4)%ubound
#define INDICES l,m,n,o
#define ONES 1,1,1,1

module array4
use, intrinsic :: iso_fortran_env, only : int8, int16, int32, int64, real32, real64
use hdf5_base

#include "tensors.inc"

end module
