module simple_mpi

    use mpi
    implicit none

    public :: bcast
contains

subroutine bcast(buffer, root, comm)
   ! integer,intent(inout)  :: buffer(:)
    type(*),DIMENSION(..) :: buffer
    integer :: s, l, i
    integer, allocatable :: sh(:)

    TYPE(MPI_Datatype) :: datatype
    integer, INTENT(IN) ::   root
    TYPE(MPI_Comm), INTENT(IN) :: comm
    integer :: ierror


    sh = shape(buffer)

    l = size(sh)
    s = 1
    do i=1,l
        s = s * sh(i)
    end do

    if (mod(s,8) == 0) then
        datatype = MPI_DOUBLE_PRECISION
        s = s / 8
    elseif (mod(s,4) == 0) then
        datatype = MPI_INTEGER
        s = s / 4
    else
        datatype = MPI_CHARACTER
        s = s
    end if

    call MPI_Bcast(buffer, s, datatype, root, comm, ierror)

    deallocate(sh)
end subroutine

end module simple_mpi
