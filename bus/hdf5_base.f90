

#define H5CALL(X) call h5eclear_f(self%hdf5error);  call X
#define H5CALL2(X) call h5eclear_f(self%pstore%hdf5error);  call X

module hdf5_base


    use hdf5
    use stringifor
    use ftlHashModuleDummy
    use ftlHashMapStrHIDModule
    use persistence
    use, intrinsic :: iso_fortran_env, only : int8, int16, int32, int64, real32, real64

    implicit none


    character(*), parameter     :: datetime_cformat = "%d %b %Y %H:%M:%S"
    integer(HSIZE_T), parameter :: size_one = 1
    integer(HSIZE_T), parameter :: size_one_1(1) = [size_one]


    type,abstract :: ObjectContainer
        integer :: hdf5error                     = 0
    contains
        procedure :: getScalarFieldID

        ! methods to be overriden by subclasses
        procedure :: StoreObject
        procedure :: LoadObject
        procedure :: DisconnectFromStore

        ! shared error handling
        generic :: DisableH5Msgs => DisableH5MsgsP
        generic :: EnableH5Msgs => EnableH5MsgsP
        generic :: HasError => HasErrorP
        procedure :: DisableH5MsgsP
        procedure :: EnableH5MsgsP
        procedure :: HasErrorP


        ! methods for a name->field-id map
        procedure(HasFieldIDI), deferred  :: HasFieldID
        procedure(GetFieldIDI), deferred  :: GetFieldID
        procedure(SetFieldIDI), deferred  :: SetFieldID

        procedure(IsEqualI), deferred  :: IsEqual
        procedure(AssignmentI), deferred :: AssignmentOperator

        generic :: operator(.eq.) => ObjectContainerEquality
        procedure :: ObjectContainerEquality

        generic :: assignment(=) => AssignObjectContainer
        procedure :: AssignObjectContainer
    end type


    type, extends(ObjectContainer), abstract :: Dataset
        integer(HID_T) :: ds_id               = int(-1,HID_T)
        contains
        procedure :: getDataFieldID
        procedure :: getExistingDataField
        procedure :: DisconnectFromStore => DisconnecDatasetFromStore
        procedure :: DisconnecDatasetFromStore

        ! methods for a name->field-id map
        procedure :: HasFieldID => HasDatasetFieldID
        procedure :: GetFieldID => GetDatasetFieldID
        procedure :: SetFieldID => SetDatasetFieldID

        procedure :: HasDatasetFieldID
        procedure :: GetDatasetFieldID
        procedure :: SetDatasetFieldID

    end type


    type, extends(ObjectContainer), abstract :: persistent
        private
        class(persistent_store),pointer :: pstore => null()

        integer(HID_T) :: group_id               = int(-1,HID_T)
        logical :: is_initialized                = .false.

        type(string) :: field_name         ! if this object is attached

        ! the field ids are set by generated code
        type(ftlHashMapStrHID) :: field_ids      ! = defaultMapStrID() has to be intrinsic


        contains

        procedure, public :: connect
        procedure, public :: disconnect

        generic :: store => StorePP, StoreString
        procedure :: StorePP
        procedure :: StoreString

        generic :: load => LoadPP, LoadString
        procedure :: LoadPP
        procedure :: LoadString
        procedure, public :: allocate
        procedure, public :: ResetSections


        procedure(NoArgsMethodI), deferred :: clear

        procedure(NoArgsMethodI), deferred :: StoreScalarFields
        procedure(NoArgsMethodI), deferred :: StoreObjectFields

        procedure(NoArgsMethodI), deferred :: LoadScalarFields
        procedure(NoArgsMethodI), deferred :: LoadObjectFields

        procedure(NoArgsMethodI), deferred :: AllocateObjectFields
        procedure(NoArgsMethodI), deferred :: ResetSectionFields

        procedure :: DisconnectObjectFields  ! for the class heirarchy, doesn't have to be overriden

        procedure, public :: init => InitPersistent
        procedure :: InitPersistent

        ! generic write, read to be used in subclasses
        generic :: write => write_logical, write_int, write_real,&
            write_string, write_datetime, write_complex

        generic :: read => read_logical, read_int, read_real,&
            read_string, read_datetime, read_complex

        procedure :: write_logical
        procedure :: write_int
        procedure :: write_real
        procedure :: write_complex
        procedure :: write_string
        procedure :: write_datetime


        procedure :: read_real
        procedure :: read_logical
        procedure :: read_int
        procedure :: read_complex
        procedure :: read_string
        procedure :: read_datetime


        procedure :: GetGroupId

        procedure :: HasFieldID => HasFieldIDImp
        procedure :: GetFieldID => GetFieldIDImp
        procedure :: SetFieldID => SetFieldIDImp

        procedure :: GetPersistentStore
end type

type :: extent
    integer :: lbound, ubound
end type

interface assignment (=)
    module procedure AssignComplexToComplexType
    module procedure AssignComplexTypeToComplex
end interface



interface
    function HasFieldIDI(self,fn) result(has)
        import :: ObjectContainer
        class(ObjectContainer),intent(inout) :: self
        character(*),intent(in) :: fn
        logical :: has
    end function
end interface


interface
    function GetFieldIDI(self,fn) result(fid)
        use H5GLOBAL
        import :: ObjectContainer
        class(ObjectContainer),intent(inout) :: self
        character(*),intent(in) :: fn
        integer(HID_T) :: fid
    end function
end interface

interface
    subroutine SetFieldIDI(self,fn, field_id)
        use H5GLOBAL
        import :: ObjectContainer
        class(ObjectContainer),intent(inout) :: self
        character(*),intent(in) :: fn
        integer(HID_T),intent(in) :: field_id
    end subroutine
end interface

interface
    pure elemental function IsEqualI(lhs, rhs) result(iseq)
        import :: ObjectContainer
        class(ObjectContainer),intent(in) :: lhs
        class(ObjectContainer),intent(in) :: rhs
        logical :: iseq
    end function
end interface

interface
    subroutine AssignmentI(lhs, rhs)
        import :: ObjectContainer
        class(ObjectContainer),intent(inout) :: lhs
        class(ObjectContainer),intent(in) :: rhs
        logical :: iseq
    end subroutine
end interface

interface
    !TODO: should be called "StoreAttributes"
    subroutine NoArgsMethodI(self)
        import :: persistent
        class(persistent), intent(inout) :: self
    end subroutine
end interface


contains

subroutine ReverseArray(arr)
    integer(HSIZE_T), intent(inout) :: arr(:)
    integer :: n
    integer :: i
    integer(HSIZE_T) :: v
    n = size(arr,1)
    do i=1,int(n/2)
        v = arr(i)
        arr(i) = arr(n-i+1)
        arr(n-i+1) = v
    end do
end subroutine


! Note this pointer needs to be freed eventually - if the call is made
! by python, than it is owned by it
function NewCStr(str) result(ret)
    use iso_c_binding
    character(*), intent(in) :: str
    type(c_ptr) :: ret

    character(len=len(str)+1), pointer :: ptr

    allocate(ptr)
    ptr(:) = str(:) // c_null_char
    ret = c_loc(ptr)
end function


! translate C pointer string to a fortran string
function FromCStr(str) result(res)
    use iso_c_binding
    type(c_ptr),intent(in),value :: str
    type(string) :: res
    character(kind=c_char,len=strlen(str)), pointer :: tmp
    interface
        pure function strlen ( string ) bind ( C ) result ( res )
            use iso_c_binding, only: c_ptr, c_size_t
            implicit none
            type(c_ptr), intent(in), value :: string
            integer(c_size_t) :: res
        end function
    end interface
    call c_f_pointer(str, tmp)
    res = tmp
end function

pure elemental function ObjectContainerEquality(lhs, rhs) result(iseq)
        class(ObjectContainer), intent(in) :: lhs
        class(ObjectContainer), intent(in) :: rhs
        logical :: iseq
    iseq = lhs%IsEqual(rhs)
end function

subroutine AssignObjectContainer(lhs, rhs)
    class(ObjectContainer), intent(inout) :: lhs
    class(ObjectContainer), intent(in) :: rhs
    call lhs%AssignmentOperator(rhs)
end subroutine


subroutine DisconnectObjectFields(self)
    class(persistent), intent(inout) :: self
    ! this serves as the interface definition - since derived classes may skip implementing this one
end subroutine

pure elemental subroutine AssignComplexToComplexType(left, right)
    type(complex_t), intent(out) :: left
    complex(kind=dp), intent(in) :: right
    left = complex_t(r=real(right), i=aimag(right))
end subroutine

pure elemental subroutine AssignComplexTypeToComplex(left, right)
    complex(kind=dp), intent(out) :: left
    type(complex_t), intent(in) :: right
    left = cmplx(right%r, right%i, kind=16)
end subroutine

subroutine StoreObject(self, ps, group_id, field_name)
    class(ObjectContainer), intent(inout) :: self
    class(persistent_store), intent(in) :: ps
    integer(HID_T),intent(in) :: group_id
    character(*), intent(in) :: field_name
    ! this is an empty implementation. We could have made this an abstract method
    ! but defining an interface is more cumbersome.
end subroutine


subroutine LoadObject(self, ps, group_id, field_name)
    class(ObjectContainer), intent(inout) :: self
    class(persistent_store), intent(in) :: ps
    integer(HID_T),intent(in) :: group_id
    character(*), intent(in) :: field_name
    ! this is an empty implementation. We could have made this an abstract method
    ! but defining an interface is more cumbersome.
end subroutine

function GetGroupId(self)  result(gid)
    class(persistent), intent(in) :: self
    integer(HID_T) :: gid
    gid = self%group_id
end function

subroutine DisableH5MsgsP(self)
    class(ObjectContainer),intent(inout) :: self
    call h5eclear_f(self%hdf5error)
    call h5eset_auto_f(debug_hdf5_ops, self%hdf5error)
end subroutine


subroutine EnableH5MsgsP(self)
    class(ObjectContainer),intent(inout) :: self
    call h5eset_auto_f(1, self%hdf5error)
    call h5eclear_f(self%hdf5error)
end subroutine

function HasErrorP(self) result(res)
    class(ObjectContainer),intent(inout) :: self
    logical :: res
    res = self%hdf5error < 0
end function


function getScalarFieldID(self, fn, gid, type_id, is_dataset) result(fid)
    class(ObjectContainer),intent(inout) :: self
    character(*),intent(in) :: fn
    integer(HID_T),intent(in) :: gid, type_id
    integer(HID_T) :: fid, sid
    logical,optional :: is_dataset

    logical :: dataset_choise = .false.

    if (present(is_dataset)) then 
         if (is_dataset) dataset_choise = .true.
    end if
    if (.not. self%HasFieldID(fn)) then
        call self%DisableH5Msgs()
        H5CALL( h5aopen_f(gid, fn,fid,self%hdf5error) )
        if (self%HasError()) then
            ! need to create the field
            ! create data space first
            H5CALL( h5screate_f(H5S_SCALAR_F , sid, self%hdf5error) )
            if (dataset_choise) then
                ! create the field as a dataset
                H5CALL( h5dcreate_f(gid, fn, type_id, sid, fid, self%hdf5error) )
            else
                ! create the field as an attribute
                H5CALL( h5acreate_f(gid, fn, type_id, sid, fid, self%hdf5error) )
            end if
            H5CALL( h5sclose_f(sid,self%hdf5error) )
        end if
        call self%SetFieldID(fn, fid)
        call self%EnableH5Msgs()
    else
        fid = self%GetFieldId(fn)
    end if
end function

function getExistingDataField(self, fn, gid, dims) result(fid)
    class(Dataset),intent(inout) :: self
    character(*),intent(in) :: fn
    integer(HID_T),intent(in) :: gid
    integer(HSIZE_T),intent(inout) :: dims(:)
    integer(HSIZE_T) :: maxdims(size(dims))
    integer(HID_T) :: fid, sid   !, array_tid

    dims = 0
    if (.not. self%HasFieldID(fn)) then
        call self%DisableH5Msgs()
        H5CALL( h5dopen_f(gid, fn,fid,self%hdf5error) )
        if (self%HasError()) then
            print *, 'cannot load ', fn, ' - it does not exist'
            ! need to create the field
            fid = -1
            call self%EnableH5Msgs()
            return
        end if

        call self%EnableH5Msgs()
        call self%SetFieldID(fn, fid)
    else
        fid = self%GetFieldId(fn)
    end if

    H5CALL( H5Dget_space_f(fid, sid, self%hdf5error) )
    H5CALL( H5Sget_simple_extent_dims_f(sid, dims, maxdims, self%hdf5error) )

    ! do we need to close the type_id
end function

function getDataFieldID(self, fn, gid, type_id, dims) result(fid)
    class(Dataset),intent(inout) :: self
    character(*),intent(in) :: fn
    integer(HID_T),intent(in) :: gid, type_id
    integer(HSIZE_T),intent(in) :: dims(:)
    integer(HID_T) :: fid, sid   !, array_tid

    if (.not. self%HasFieldID(fn)) then
        call self%DisableH5Msgs()
        H5CALL( h5dopen_f(gid, fn,fid,self%hdf5error) )
        if (self%HasError()) then
            ! need to create the field

            ! create data space id
            H5CALL( h5screate_simple_f(size(dims), dims, sid, self%hdf5error) )

            ! create the field as an attribute
            H5CALL( h5dcreate_f(gid, fn, type_id, sid, fid, self%hdf5error) )


            H5CALL( h5sclose_f(sid,self%hdf5error) )
        end if
        call self%SetFieldID(fn, fid)
        call self%EnableH5Msgs()
    else
        fid = self%GetFieldId(fn)
    end if
end function


subroutine DisconnectFromStore(self)
    class(ObjectContainer),intent(inout) :: self
end subroutine


subroutine DisconnecDatasetFromStore(self)
    class(Dataset),intent(inout) :: self
    if (self%ds_id /= -1) then
        call h5dclose_f(self%ds_id, self%hdf5error)
        self%ds_id = -1
    end if
end subroutine

function HasDatasetFieldID(self,fn) result(has)
    class(Dataset),intent(inout) :: self
    character(*),intent(in) :: fn
    logical :: has
    has = (self%ds_id  /= -1)
end function

function GetDatasetFieldID(self,fn) result(fid)
    class(Dataset),intent(inout) :: self
    character(*),intent(in) :: fn
    integer(HID_T) :: fid
    fid = self%ds_id
end function


subroutine SetDatasetFieldID(self,fn, field_id)
    class(Dataset),intent(inout) :: self
    character(*),intent(in) :: fn
    integer(HID_T),intent(in) :: field_id
    self%ds_id = field_id
end subroutine



function HasFieldIDImp(self,fn) result(has)
    class(persistent),intent(inout) :: self
    character(*),intent(in) :: fn
    logical :: has
    type(string) :: fns
    fns = fn
    has = (fns .in. self%field_ids)
end function



function GetFieldIDImp(self,fn) result(fid)
    class(persistent),intent(inout) :: self
    character(*),intent(in) :: fn
    integer(HID_T) :: fid
    type(string) :: fns
    fns = fn
    fid = self%field_ids%Get(fns)
end function


subroutine SetFieldIDImp(self,fn, field_id)
    class(persistent),intent(inout) :: self
    character(*),intent(in) :: fn
    integer(HID_T),intent(in) :: field_id
    type(string) :: fns
    fns = fn
    call self%field_ids%Set(fns, field_id)
end subroutine



subroutine write_int(self, fn, fv)
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    integer(kind=int32), intent(in) :: fv

    integer(HID_T) :: fid
    integer :: tmp
    tmp = fv ! TODO: hdf5 doesn't have a write for 64 bit integer
    fid = self%getScalarFieldID(fn, self%GetGroupId(), H5T_NATIVE_INTEGER)
    H5CALL( h5awrite_f(fid, H5T_NATIVE_INTEGER, tmp, size_one_1, self%hdf5error) )
end subroutine




subroutine read_int(self, fn, fv)
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    integer(kind=int32), intent(inout) :: fv
    integer :: tmp
    integer(HID_T) :: fid

    fid = self%getScalarFieldID(fn, self%GetGroupId(), H5T_NATIVE_INTEGER)
    H5CALL( h5aread_f(fid, H5T_NATIVE_INTEGER, tmp, size_one_1, self%hdf5error) )
    fv = int(tmp, 8)
end subroutine


subroutine write_real(self, fn, fv)
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    real(kind=dp), intent(in) :: fv

    H5CALL( h5awrite_f(self%getScalarFieldID(fn, self%GetGroupId(), H5T_NATIVE_DOUBLE), H5T_NATIVE_DOUBLE, fv, size_one_1, self%hdf5error) )
end subroutine

subroutine read_real(self, fn, fv)
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    real(kind=dp), intent(inout) :: fv

    H5CALL( h5aread_f(self%getScalarFieldID(fn, self%GetGroupId(), H5T_NATIVE_DOUBLE), H5T_NATIVE_DOUBLE, fv, size_one_1, self%hdf5error) )
end subroutine


subroutine write_complex(self, fn, fv)
    use ISO_C_BINDING, only : c_ptr, c_loc
    use h5global
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    complex(kind=dp), intent(in) :: fv
    integer(HID_T) :: fid
    type(complex_t),target :: tmp


    tmp = fv
    fid = self%getScalarFieldID(fn, self%GetGroupId(), self%pstore%tids%complex_tid)
    ! h5awrite_f_c is local c binding, see below
    self%hdf5error = h5awrite_f_c(fid, self%pstore%tids%complex_tid, c_loc(tmp))

    ! TODO:
    ! for some unclear reason the h5awrite_f or even h5awrite_ptr calls do not compile or link
    ! call h5awrite_f(fid, self%pstore%tids%complex_tid, c_loc(tmp), self%hdf5error)
end subroutine


subroutine read_complex(self, fn, fv)
    use ISO_C_BINDING, only : c_ptr, c_loc
    use h5global
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    complex(kind=dp), intent(inout) :: fv
    integer(HID_T) :: fid
    type(complex_t),target :: tmp
    fid = self%getScalarFieldID(fn, self%GetGroupId(), self%pstore%tids%complex_tid)
    self%hdf5error = h5aread_f_c(fid, self%pstore%tids%complex_tid, c_loc(tmp))
    fv = tmp
end subroutine

! write it as the enumeration type that PYTHON likes
subroutine write_logical(self, fn, fv)
    use ISO_C_BINDING, only : c_ptr, c_loc
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    logical, intent(in) :: fv

    integer(HID_T) :: fid
    integer(kind(FALSE)),target :: tmp
    tmp = FALSE
    if (fv) tmp = TRUE

    fid = self%getScalarFieldID(fn, self%GetGroupId(), self%pstore%tids%logical_tid)
    self%hdf5error = h5awrite_f_c(fid, self%pstore%tids%logical_tid, c_loc(tmp))
end subroutine

subroutine read_logical(self, fn, fv)
    use ISO_C_BINDING, only : c_ptr, c_loc
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    logical, intent(inout) :: fv
    integer(kind(FALSE)),target :: tmp
    integer(HID_T) :: fid

    fid = self%getScalarFieldID(fn, self%GetGroupId(), self%pstore%tids%logical_tid)
    self%hdf5error = h5aread_f_c(fid, self%pstore%tids%logical_tid, c_loc(tmp))
    fv = (tmp == TRUE)
end subroutine


subroutine write_fl_string(self, fn, fv)
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    type(string), intent(in) :: fv
    integer(HID_T) :: fid, typeid

    integer(HSIZE_T) :: length
    length = fv%len()
    H5CALL( h5tcopy_f(H5T_NATIVE_CHARACTER, typeid, self%hdf5error) )
    H5CALL( h5tset_size_f(typeid, length, self%hdf5error) )

    fid = self%getScalarFieldID(fn, self%GetGroupId(), typeid)
    H5CALL( h5awrite_f(fid, typeid, fv%chars(), size_one_1, self%hdf5error) )

    H5CALL( h5Tclose_f(typeid, self%hdf5error) )

end subroutine

subroutine read_fl_string(self, fn, fv)
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    type(string), intent(inout) :: fv

    integer(HID_T) :: fid, typeid
    integer(HSIZE_T) :: strlen

    ! here we don't expect typeid to be returned, but the actual field's type, since it is a read
    fid = self%getScalarFieldID(fn, self%GetGroupId(), typeid)
    H5CALL( H5Aget_type_f(fid, typeid, self%hdf5error) )
    H5CALL( H5Tget_size_f(typeid, strlen, self%hdf5error) )
    fv = ReadIt()
    H5CALL( h5Tclose_f(typeid, self%hdf5error) )
contains
    function ReadIt() result(res)
        character(len=strlen) :: res
        H5CALL( h5aread_f(fid, typeid, res, size_one_1, self%hdf5error) )
    end function

end subroutine

! writes variable length strings, terminated by \0
subroutine write_string(self, fn, fv)
    USE ISO_C_BINDING
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    type(string), target, intent(in) :: fv
    integer(HID_T) :: fid, typeid
    character(len=fv%len() + 1), target :: buffer
    type(c_ptr),target :: ptr
    integer(HSIZE_T) :: length

    buffer = fv%chars() // c_null_char
    ptr = c_loc(buffer(1:))

    length = fv%len() + 1

    H5CALL( h5tcopy_f(H5T_STRING, typeid, self%hdf5error) )
    fid = self%getScalarFieldID(fn, self%GetGroupId(), typeid)
    self%hdf5error = h5awrite_f_c(fid, typeid, c_loc(ptr))
end subroutine


! reads variable length strings, terminated by \0
subroutine read_string(self, fn, fv)
    use ISO_C_BINDING
    use stringifor
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    type(string), intent(inout) :: fv

    integer(HID_T) :: fid, typeid
    type(c_ptr),target :: cstr(2) ! one is enough, but we can verify that cstr(2) == c_null_ptr after the read

    interface
        function h5Free_memory_f( ptr ) bind (C, name='H5free_memory') result ( res )
            use iso_c_binding, only: c_ptr, c_size_t
            implicit none
            type(c_ptr), intent(in), value :: ptr
            integer :: res
        end function
    end interface

    fid = self%getScalarFieldID(fn, self%GetGroupId(), typeid)
    H5CALL( H5Aget_type_f(fid, typeid, self%hdf5error) )
    ! is_variable_str returns if it is variable length

    cstr(:) = c_null_ptr
    self%hdf5error = h5aread_f_c(fid, typeid, c_loc(cstr(1)))
    !if there are issues , uncomment the following and loook at cstr(2)
    !print *, "result is:", cstr(1), cstr(2) ! check cstr(2) is 0, as initialized above
    fv =  c_ptr_to_f_char_ptr ( cstr(1) )

    ! now deallocate the cstr(1)
    self%hdf5error =  H5Free_memory_f(cstr(1))
    H5CALL( h5Tclose_f(typeid, self%hdf5error) )

contains

    function c_ptr_to_f_char_ptr ( str_c_ptr) result(f_str)
        use iso_c_binding
        interface
            pure function strlen ( string ) bind ( C ) result ( res )
                use iso_c_binding, only: c_ptr, c_size_t
                implicit none
                type(c_ptr), intent(in), value :: string
                integer(c_size_t) :: res
            end function
        end interface
        type(c_ptr),                           intent(in)  :: str_c_ptr
        character(kind=c_char,len=:), pointer :: f_str
        character(kind=c_char,len=strlen(str_c_ptr)), pointer :: tmp
        call c_f_pointer( str_c_ptr, tmp )
        f_str => tmp
    end function
end subroutine


subroutine write_datetime(self, fn, fv)
    use datetime_module
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    type(datetime), intent(in) :: fv

    type(string) :: str
    str = fv%strftime(datetime_cformat)
    call write_fl_string(self, fn, str)
end subroutine

subroutine read_datetime(self, fn, fv)
    use datetime_module
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: fn
    type(datetime), intent(inout) :: fv

    type(string) :: str
    call read_fl_string(self, fn, str)
    fv = strptime(str%chars(), datetime_cformat)
end subroutine


subroutine InitPersistent(self)
    class(persistent), intent(inout) :: self
    if (self%is_initialized) return
    call self%field_ids%New(30)
    self%group_id = -1
    self%is_initialized = .true.
end subroutine


subroutine ResetSections(self)
    class(persistent), intent(inout) :: self
    call self%InitPersistent()
    call self%ResetSectionFields()
end subroutine


subroutine connect(self, pp, create)
    class(persistent), intent(inout) :: self
    class(persistent_path), intent(in) :: pp
    logical, intent(in) :: create

    call self%InitPersistent()  ! does nothing if already initialized
    ! here we disconnect EVEN if it is the same repository. One should not do this for no use.
    if (associated(self%pstore)) then
        call self%disconnect()
    end if

    self%group_id = pp%FetchGroup(create)
    self%pstore => pp%pstore

    ! handle case that the group id is already set, but
    ! different from the new one
end subroutine


! HMM - crashes the application after used (usually in store)
! TODO: debug this
subroutine disconnect(self)
    class(persistent), intent(inout) :: self
    type(ftlHashMapStrHIDIterator) :: iter
    integer(HID_T) :: field_id

    call self%InitPersistent()  ! does nothing if already initialized
    if (.not. associated(self%pstore)) return

    iter = self%field_ids%Begin()
    ! close all the attribute field ids from the list, and nullify it
    do while (iter /= self%field_ids%End())
        field_id = self%field_ids%Get(iter%Key())
        H5CALL( h5aclose_f(field_id, self%hdf5error) )
        ! TODO: this will fail if there are dataset or object fields
        call iter%Inc()
    end do

    call self%field_ids%Clear()

    !call generated code to disconnect the dataset fields (matrices,etc)
    call self%DisconnectObjectFields()

    nullify( self%pstore )
    self%group_id = -1

end subroutine

! this one is a convenience routine, which takes the singleton store
! and obtains the persistent path from it

! TODO: Note below: this doesn't compile well in ifort for some reason
subroutine StoreString(self, str, flush_opt)
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: str
    logical,intent(in), optional :: flush_opt

    type(persistent_path) :: pp
    class(persistent_store),pointer :: ps

    logical :: do_flush
    do_flush = .False.
    if (present(flush_opt)) do_flush = flush_opt

    ps => GetStore()
    pp = ps%Fetch(str)
    call self%StorePP(pp, flush_opt=do_flush)
end subroutine

subroutine StorePP(self, pp, flush_opt)
    class(persistent), intent(inout) :: self
    class(persistent_path), intent(in) :: pp
    logical,intent(in), optional :: flush_opt
    class(persistent_store),pointer :: ps
    logical :: do_flush
    do_flush = .False.
    if (present(flush_opt)) do_flush = flush_opt

    call self%InitPersistent()  ! does nothing if already initialized

    ! set the pstore of the fields and their group before calling their "store"
    call self%connect(pp, .True.)
    call self%StoreScalarFields()
    call self%StoreObjectFields()

    if (do_flush) then
        ps => GetStore()
        call ps%flush(pp)
    end if
end subroutine


subroutine LoadPP(self, pp)
    class(persistent), intent(inout) :: self
    class(persistent_path), intent(in) :: pp
    call self%InitPersistent()  ! does nothing if already initialized
    call self%connect(pp, .False.)
    call self%LoadScalarFields()
    call self%LoadObjectFields()

    call self%ResetSectionFields()
end subroutine

subroutine LoadString(self, str)
    class(persistent), intent(inout) :: self
    character(*), intent(in) :: str

    type(persistent_path) :: pp
    class(persistent_store),pointer :: ps

    ps => GetStore()
    pp = ps%Fetch(str)
    call self%LoadPP(pp)
end subroutine

subroutine Allocate(self)
    class(persistent), intent(inout) :: self
    call self%InitPersistent()  ! does nothing if already initialized
    call self%AllocateObjectFields() ! should this be generated with parameters?

    call self%ResetSectionFields()
end subroutine



function GetPersistentStore(self) result(ps)
    class(persistent), intent(inout) :: self
    class(persistent_store),pointer :: ps
    ps => self%pstore
end function

end module
