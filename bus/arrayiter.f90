module arrayiter

    use persistence
    use stringifor

    implicit none

    private

    public :: iterator, Init,Done , Inc, StringIndex

    type :: iterator

        private

        integer,public,allocatable :: i(:)   ! this is what we want, the current multi-index

        integer,allocatable :: lboundA(:), uboundA(:)

        integer :: d
        logical :: finished

    contains

        procedure :: Init
        procedure :: Done
        procedure :: Inc
        procedure :: StringIndex

    end type



contains

    subroutine Init(self, lboundA, uboundA)
        class(Iterator),intent(inout) :: self
        integer, intent(in) :: lboundA(:), uboundA(:)
        self%d = size(lboundA)
        self%finished = .False.
        self%lboundA = lboundA
        self%uboundA = uboundA

        self%i = lboundA  ! this allocated it and sets it
    end subroutine

    function Done(self) result(res)
        class(Iterator),intent(inout) :: self
        logical :: res
        res = self%finished
    end function

    ! this returns the string representation of the object key that is stored / loaded in/from the hdf5 file
    function StringIndex(self)result(res)
        use penf_stringify
        class(Iterator),intent(inout) :: self
        type(string) :: res
        integer :: i
        type(string) :: istr
        if (self%d == 1) then
            res = trim(str(self%i(1) - self%lboundA(1),no_sign=.True.))
        else
            res = "("
            do i=1,self%d
                if (i/=1) res=res // ", "  ! note that numpy adds a space after the comma
                ! for compatibility with C++/Python we start stored arrays at 0
                istr = trim(str(self%i(i) - self%lboundA(i),no_sign=.True.))
                res = res // istr
            end do
            res = res // ")"
        end if
        !print *, "iterator: ", res
    end function

    subroutine Inc(self)
        class(Iterator),intent(inout) :: self
        integer :: idx
        if (self%finished) then
            print *, "error in iterate Inc, iteration is over"
            return
        end if
        ! this works like adding +1 to a digital number and carrying over
        do idx=1,self%d
            if (self%i(idx) < self%uboundA(idx)) then
                self%i(idx) = self%i(idx) + 1
                ! now all indices to the left should be reset
                self%i(1:idx-1) = self%lboundA(1:idx-1)
                return
            end if
        end do
        ! not found an index: we're done
        self%finished = .True.
    end subroutine


end module
