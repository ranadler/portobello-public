
#include "ftlMacros.inc"

#define TNAME cube
#define RANGE :,:,:
#define RANK 3
#define SECTION dims(1),dims(2),dims(3)
#define EXTENTS extents(1)%lbound:extents(1)%ubound, extents(2)%lbound:extents(2)%ubound,  extents(3)%lbound:extents(3)%ubound
#define INDICES l,m,n
#define ONES 1,1,1

module cube

use, intrinsic :: iso_fortran_env, only : int8, int16, int32, int64, real32, real64
use hdf5_base

#include "tensors.inc"

end module
