


implicit none

!
!  TODO: add Transpose operation, multiplication, addition.
!

type, extends(Dataset) :: CAT(TNAME,_int)
    private

    integer(kind=int32), allocatable :: a( RANGE )

    contains

    generic,public :: Get => CAT(TNAME,GetI)
    generic, public :: init => CAT(TNAME,initI)

    generic,public :: GetWithExtents => CAT(TNAME,GetWithExtentsI)
    procedure :: CAT(TNAME,GetWithExtentsI)

    procedure,public :: Length => CAT(TNAME,LengthI)

    procedure :: IsEqual => IsEqualI64
    procedure :: IsEqualI64

    procedure  :: CAT(TNAME,GetI)
    procedure  :: CAT(TNAME,initI)

    procedure :: StoreObject =>  CAT(TNAME,StoreObjectIImp)
    procedure :: LoadObject =>  CAT(TNAME,LoadObjectIImp)

    procedure,public :: AssignmentOperator => CAT(TNAME,AssignI)

    generic :: assignment(=) => CAT(TNAME,AssignIFromArray)

    procedure :: CAT(TNAME,AssignIFromArray)
    procedure :: CAT(TNAME,LoadObjectIImp)
    procedure :: CAT(TNAME,StoreObjectIImp)
    procedure :: CAT(TNAME,AssignI)

    final :: CAT(TNAME,finalizeI)
end type

type, extends(Dataset) :: CAT(TNAME,_real)
    private

    real(kind=dp), allocatable :: a( RANGE )

    contains

    generic,public :: Get => CAT(TNAME,GetR)
    generic, public :: init => CAT(TNAME,initR)

    generic,public :: GetWithExtents => CAT(TNAME,GetWithExtentsR)
    procedure :: CAT(TNAME,GetWithExtentsR)

    procedure,public :: Length => CAT(TNAME,LengthR)

    procedure :: IsEqual => IsEqualR
    procedure :: IsEqualR

    procedure  :: CAT(TNAME,GetR)
    procedure  :: CAT(TNAME,initR)

    procedure :: StoreObject =>  CAT(TNAME,StoreObjectRImp)
    procedure :: LoadObject =>  CAT(TNAME,LoadObjectRImp)

    procedure,public :: AssignmentOperator => CAT(TNAME,AssignR)

    generic :: assignment(=) => CAT(TNAME,AssignRFromArray)

    procedure :: CAT(TNAME,AssignRFromArray)
    procedure :: CAT(TNAME,LoadObjectRImp)
    procedure :: CAT(TNAME,StoreObjectRImp)
    procedure :: CAT(TNAME,AssignR)

    final :: CAT(TNAME,finalizeR)

end type

type, extends(Dataset) :: CAT(TNAME,_complex)
    private

    complex(kind=dp), allocatable :: a( RANGE )

    contains

    generic,public :: Get => CAT(TNAME,GetC)
    generic,public :: init => CAT(TNAME,initC)

    generic,public :: GetWithExtents => CAT(TNAME,GetWithExtentsC)
    procedure :: CAT(TNAME,GetWithExtentsC)

    procedure,public :: Length => CAT(TNAME,LengthC)

    procedure :: IsEqual => IsEqualC
    procedure :: IsEqualC

    procedure :: CAT(TNAME,GetC)
    procedure :: CAT(TNAME,initC)

    procedure :: StoreObject => CAT(TNAME,StoreObjectCImp)
    procedure :: LoadObject => CAT(TNAME,LoadObjectCImp)

    procedure,public :: AssignmentOperator => CAT(TNAME,AssignC)

    generic :: assignment(=) => CAT(TNAME,AssignCFromArray)

    procedure :: CAT(TNAME,AssignCFromArray)
    procedure :: CAT(TNAME,StoreObjectCImp)
    procedure :: CAT(TNAME,LoadObjectCImp)

    final :: CAT(TNAME,finalizeC)
end type

interface assignment(=)
    module procedure AssignRToArray
    module procedure AssignCToArray
end interface

integer :: RI( RANK ) = 0

contains

function  CAT(TNAME,LengthR)(self) result(res)
    use hdf5
    integer :: res
    class(CAT(TNAME,_real)), intent(inout) :: self

    integer(HSIZE_T) :: dims( RANK )
    res = 0

    if (.not. allocated(self%a)) return
    dims = shape(self%a)
    res = product(dims)
end function

function  CAT(TNAME,LengthC)(self) result(res)
    use hdf5
    integer :: res
    class(CAT(TNAME,_complex)), intent(inout) :: self

    integer(HSIZE_T) :: dims( RANK )
    res = 0

    if (.not. allocated(self%a)) return
    dims = shape(self%a)
    res = product(dims)
end function

function  CAT(TNAME,LengthI)(self) result(res)
    use hdf5
    integer :: res
    class(CAT(TNAME,_int)), intent(inout) :: self

    integer(HSIZE_T) :: dims( RANK )
    res = 0

    if (.not. allocated(self%a)) return
    dims = shape(self%a)
    res = product(dims)
end function


subroutine patch_h5dread_ptr(dset_id, mem_type_id, buf, hdferr, &
        mem_space_id, file_space_id, xfer_prp)
    use, intrinsic :: ISO_C_BINDING
    implicit none
    integer(HID_T), intent(in) :: dset_id     ! Dataset identifier
    integer(HID_T), intent(in) :: mem_type_id ! Memory datatype identifier
    type(c_ptr), intent(in) :: buf
    integer, intent(out) :: hdferr      ! Error code
    integer(HID_T), optional, intent(in) :: mem_space_id  ! Memory dataspace identfier
    integer(HID_T), optional, intent(in) :: file_space_id ! File dataspace identfier
    integer(HID_T), optional, intent(in) :: xfer_prp      ! Transfer property list identifier

    integer(HID_T) :: xfer_prp_default
    integer(HID_T) :: mem_space_id_default
    integer(HID_T) :: file_space_id_default

    xfer_prp_default = H5P_DEFAULT_F
    mem_space_id_default = H5S_ALL_F
    file_space_id_default = H5S_ALL_F

    if (present(xfer_prp)) xfer_prp_default = xfer_prp
    if (present(mem_space_id))  mem_space_id_default = mem_space_id
    if (present(file_space_id)) file_space_id_default = file_space_id

    hdferr = h5dread_f_c(dset_id, mem_type_id, mem_space_id_default, &
        file_space_id_default, xfer_prp_default, buf)
end subroutine patch_h5dread_ptr


subroutine InitRI
    integer :: i
    if (RI(1) == 0) then
        do i=1,RANK
            RI(i) = RANK - i + 1
        end do
    end if
end subroutine

function CAT(TNAME,GetWithExtentsI)(self,extents) result(res)
    class(CAT(TNAME,_int)), intent(inout) :: self
    type(extent), intent(in) :: extents( RANK )
    integer(kind=int32),pointer :: res( RANGE )
    res=> GetIt(self%a)
    contains

    function GetIt(actual) result(ptr)
         integer(kind=int32),intent(inout), target  :: &
              actual( EXTENTS )
         integer(kind=int32),pointer :: ptr( RANGE )
        ptr =>  actual
    end function
end function


function CAT(TNAME,GetWithExtentsR)(self,extents) result(res)
    class(CAT(TNAME,_real)), intent(inout) :: self
    type(extent), intent(in) :: extents( RANK )
    real(kind=dp),pointer :: res( RANGE )
    res=> GetIt(self%a)
    contains

    function GetIt(actual) result(ptr)
        real(kind=dp),intent(inout), target  :: actual( EXTENTS )
        real(kind=dp),pointer :: ptr( RANGE )
        ptr =>  actual
    end function
end function



function CAT(TNAME,GetWithExtentsC)(self,extents) result(res)
    class(CAT(TNAME,_complex)), intent(inout) :: self
    type(extent), intent(in) :: extents( RANK )
    complex(kind=dp),pointer :: res( RANGE )
    res=> GetIt(self%a)
    contains

    function GetIt(actual) result(ptr)
        complex(kind=dp),intent(inout), target  :: &
              actual( EXTENTS )
        complex(kind=dp),pointer :: ptr( RANGE )
        ptr =>  actual
    end function
end function

subroutine CAT(TNAME,finalizeI)(self)
    type(CAT(TNAME,_int)), intent(inout) :: self
    call self%DisconnectFromStore()
end subroutine


subroutine CAT(TNAME,finalizeR)(self)
    type(CAT(TNAME,_real)), intent(inout) :: self
    call self%DisconnectFromStore()
end subroutine

subroutine CAT(TNAME,finalizeC)(self)
    type(CAT(TNAME,_complex)), intent(inout) :: self
    call self%DisconnectFromStore()
end subroutine

pure elemental function IsEqualI64(lhs, rhs) result(iseq)
        class(CAT(TNAME,_int)), intent(in) :: lhs
        class(ObjectContainer), intent(in) :: rhs
        logical :: iseq
    select type(rhs)
        type is (CAT(TNAME,_int))
            iseq = .true.
            if (allocated(lhs%a) .neqv. allocated(rhs%a)) then
                iseq = .false.
            elseif (.not. allocated(lhs%a)) then
                iseq = .true. ! both are unallocated
            elseif (any(shape(lhs%a) /= shape(rhs%a))) then
                iseq = .false.
            elseif (any(lhs%a /= rhs%a)) then
                iseq = .False.
            end if
        type is (CAT(TNAME,_real))
            iseq = .true.
            if (allocated(lhs%a) .neqv. allocated(rhs%a)) then
                iseq = .false.
            elseif (.not. allocated(lhs%a)) then
                iseq = .true. ! both are unallocated
            elseif (any(shape(lhs%a) /= shape(rhs%a))) then
                iseq = .false.
            else
                iseq = all(abs(lhs%a - rhs%a) < 1.0d-12)
            end if
        class default
            iseq = .false.
    end select
end function

pure elemental function IsEqualR(lhs, rhs) result(iseq)
        class(CAT(TNAME,_real)), intent(in) :: lhs
        class(ObjectContainer), intent(in) :: rhs
        logical :: iseq
    select type(rhs)
        type is (CAT(TNAME,_real))
            iseq = .true.
            if (allocated(lhs%a) .neqv. allocated(rhs%a)) then
                iseq = .false.
            elseif (.not. allocated(lhs%a)) then
                iseq = .true. ! both are unallocated
            elseif (any(shape(lhs%a) /= shape(rhs%a))) then
                iseq = .false.
            else
                iseq = all(abs(lhs%a - rhs%a) < 1.0d-12)
            end if
        class default
            iseq = .false.
    end select
end function

subroutine CAT(TNAME,AssignI)(lhs, rhs)
    class(CAT(TNAME,_int)), intent(inout) :: lhs
    class(ObjectContainer), intent(in) :: rhs
    select type(rhs)
        type is (CAT(TNAME,_int))
            if (allocated(rhs%a)) then
                lhs%a = rhs%a
            else
                deallocate(lhs%a)
            end if
        type is (CAT(TNAME,_real))
            if (allocated(rhs%a)) then
                lhs%a = rhs%a
            else
                deallocate(lhs%a)
            end if
        type is (CAT(TNAME,_complex))

            if (allocated(rhs%a)) then
                lhs%a = rhs%a
            else
                deallocate(lhs%a)
            end if
        class default
    end select
end subroutine

subroutine CAT(TNAME,AssignR)(lhs, rhs)
    class(CAT(TNAME,_real)), intent(inout) :: lhs
    class(ObjectContainer), intent(in) :: rhs
    select type(rhs)
        type is (CAT(TNAME,_real))
            if (allocated(rhs%a)) then
                lhs%a = rhs%a
            else
                deallocate(lhs%a)
            end if
        type is (CAT(TNAME,_complex))

            if (allocated(rhs%a)) then
                lhs%a = rhs%a
            else
                deallocate(lhs%a)
            end if
        class default
    end select
end subroutine


subroutine CAT(TNAME,AssignC)(lhs, rhs)
    class(CAT(TNAME,_complex)), intent(inout) :: lhs
    class(ObjectContainer), intent(in) :: rhs

    select type(rhs)
        type is (CAT(TNAME,_real))
            if (allocated(rhs%a)) then
                lhs%a = rhs%a
            else
                deallocate(lhs%a)
            end if
        type is (CAT(TNAME,_complex))

            if (allocated(rhs%a)) then
                lhs%a = rhs%a
            else
                deallocate(lhs%a)
            end if
        class default
    end select
end subroutine

pure elemental function IsEqualC(lhs, rhs) result(iseq)
        class(CAT(TNAME,_complex)), intent(in) :: lhs
        class(ObjectContainer), intent(in) :: rhs
        logical :: iseq
    select type(rhs)
        type is (CAT(TNAME,_complex))
            iseq = .true.
            if (allocated(lhs%a) .neqv. allocated(rhs%a)) then
                iseq = .false.
            elseif (.not. allocated(lhs%a)) then
                iseq = .true. ! both are unallocated
            elseif (any(shape(lhs%a) /= shape(rhs%a))) then
                iseq = .false.
            else
                iseq = all(abs(lhs%a - rhs%a) < 1.0d-12)
            end if
        class default
            iseq = .false.
    end select
end function

subroutine LayoutTranspose(a, b)
    use ISO_C_BINDING, only : c_loc, c_double
    real(kind=dp), intent(inout) :: a ( RANGE )
    real(c_double), intent(inout) :: b ( RANGE )
    integer(HSIZE_T) :: dims( RANK )
    integer(HSIZE_T) :: ubound( RANK )
    integer :: count, i, j
    real(kind=dp) :: val

    count = product(shape(a))
    dims = 1
    ubound = shape(a)

    do i=1,count
        val = a( SECTION )
        call ReverseArray( dims )
        b( SECTION ) = val
        call ReverseArray( dims )
        do j=1,size(dims)
            if (dims(j) < ubound(j)) then
                dims(j) = dims(j) + 1
                exit
            else
                dims(j) = 1
            end if
        end do
    end do

end subroutine

subroutine  CAT(TNAME,StoreObjectRImp)(self, ps, group_id, field_name)
    use hdf5
    use ISO_C_BINDING, only : c_loc, c_double
    class(CAT(TNAME,_real)), intent(inout) :: self
    class(persistent_store), intent(in) :: ps
    integer(HID_T),intent(in) :: group_id
    character(*), intent(in) :: field_name

    integer(HID_T) :: fid
    integer(HSIZE_T) :: dims( RANK )

    if (.not. allocated(self%a)) return
    dims = shape(self%a)

    fid = self%getDataFieldID(field_name, group_id, H5T_NATIVE_DOUBLE, dims)
    call DoWrite(self%a)


contains
    subroutine DoWrite(self_a)
        real(kind=dp), target, intent(inout) :: self_a( RANGE )
        call h5dwrite_f(fid, H5T_NATIVE_DOUBLE, c_loc(self_a), self%hdf5error)
    end subroutine
end subroutine

subroutine  CAT(TNAME,LoadObjectRImp)(self, ps, group_id, field_name)
    use hdf5
    use ISO_C_BINDING, only : c_loc, c_double
    use hdf5_base
    class(CAT(TNAME,_real)), intent(inout) :: self
    class(persistent_store), intent(in) :: ps
    integer(HID_T),intent(in) :: group_id
    character(*), intent(in) :: field_name
    integer(HSIZE_T) :: dims( RANK )
    integer(HID_T) :: fid

    fid = self%getExistingDataField(field_name, group_id, dims)

    if (.not. allocated(self%a)) allocate(self%a(SECTION))
    call DoRead(self%a)

    contains
    subroutine DoRead(self_a)
        real(kind=dp), target, intent(inout) :: self_a( RANGE )
        call patch_h5dread_ptr(fid, H5T_NATIVE_DOUBLE, c_loc(self_a), self%hdf5error)
    end subroutine

end subroutine

subroutine  CAT(TNAME,StoreObjectIImp)(self, ps, group_id, field_name)
    use hdf5
    use ISO_C_BINDING, only : c_loc
    class(CAT(TNAME,_int)), intent(inout) :: self
    class(persistent_store), intent(in) :: ps
    integer(HID_T),intent(in) :: group_id
    character(*), intent(in) :: field_name

    integer(HID_T) :: fid
    integer(HSIZE_T) :: dims( RANK )

    if (.not. allocated(self%a)) return

    dims = shape(self%a)

    fid = self%getDataFieldID(field_name, group_id, H5T_NATIVE_INTEGER, dims)
    call DoWrite(self%a)

    contains
    subroutine DoWrite(self_a)
        integer(kind=int32), target :: self_a( RANGE )
        call h5dwrite_f(fid, H5T_NATIVE_INTEGER, c_loc(self_a), self%hdf5error)
    end subroutine
end subroutine


subroutine  CAT(TNAME,LoadObjectIImp)(self, ps, group_id, field_name)
    use hdf5
    use ISO_C_BINDING, only : c_loc
    use hdf5_base
    class(CAT(TNAME,_int)), intent(inout) :: self
    class(persistent_store), intent(in) :: ps
    integer(HID_T),intent(in) :: group_id
    character(*), intent(in) :: field_name
    integer(HSIZE_T) :: dims( RANK )
    integer(HID_T) :: fid

    fid = self%getExistingDataField(field_name, group_id, dims)
    ! TODO: check none of the dims is 0

    ! don't re-allocate if it is already allocated


    if (.not. allocated(self%a)) allocate(self%a(SECTION))
    call DoRead(self%a(ONES))


    contains
    subroutine DoRead(self_a)
        integer(kind=int32),target,intent(inout) :: self_a(SECTION)
        call patch_h5dread_ptr(fid, H5T_NATIVE_INTEGER, c_loc(self_a), self%hdf5error)
    end subroutine

end subroutine


subroutine CAT(TNAME,StoreObjectCImp)(self,  ps, group_id, field_name)
    use hdf5_base
    use,intrinsic :: ISO_C_BINDING, only : c_ptr, c_loc
    class(CAT(TNAME,_complex)), intent(inout) :: self
    class(persistent_store), intent(in) :: ps
    integer(HID_T),intent(in) :: group_id
    character(*), intent(in) :: field_name
    integer(HID_T) :: fid
    integer(HSIZE_T) :: dims( RANK )

    if (.not. allocated(self%a)) return

    dims = shape(self%a)

    fid = self%getDataFieldID(field_name, group_id, ps%tids%complex_tid, dims)
    call DoWrite(self%a)

    contains
    subroutine DoWrite(self_a)
        complex(kind=dp), target, intent(inout) :: self_a( RANGE )
        call h5dwrite_f(fid, ps%tids%complex_tid, c_loc(self_a), self%hdf5error)
    end subroutine
end subroutine

subroutine CAT(TNAME,LoadObjectCImp)(self, ps, group_id, field_name)
    use hdf5
    use ISO_C_BINDING, only : c_ptr, c_loc
    use hdf5_base
    class(CAT(TNAME,_complex)), intent(inout) :: self
    class(persistent_store), intent(in) :: ps
    integer(HID_T),intent(in) :: group_id
    character(*), intent(in) :: field_name

    integer(HSIZE_T) :: dims( RANK )
    integer(HID_T) :: fid

    fid = self%getExistingDataField(field_name, group_id, dims)

    if (.not. allocated(self%a)) allocate(self%a(SECTION))

    call DoRead(self%a)

    contains
    subroutine DoRead(self_a)
        complex(kind=dp), target, intent(inout) :: self_a( RANGE )
        call patch_h5dread_ptr(fid, ps%tids%complex_tid, c_loc(self_a), self%hdf5error)
    end subroutine


end subroutine

function CAT(TNAME,GetI)(self) result(a)
    class(CAT(TNAME,_int)), intent(inout) :: self
    integer(kind=int32),pointer              :: a( RANGE )
    call GetPointer(self%a)

    contains
    subroutine GetPointer(actual)
        integer(kind=int32),target             :: actual( RANGE )
        a => actual
    end subroutine
end function


function CAT(TNAME,GetR)(self) result(a)
    class(CAT(TNAME,_real)), intent(inout) :: self
    real(kind=dp),pointer              :: a( RANGE )
    call GetPointer(self%a)

    contains
    subroutine GetPointer(actual)
        real(kind=dp),target             :: actual( RANGE )
        a => actual
    end subroutine
end function

function CAT(TNAME,GetC)(self) result(a)
    class(CAT(TNAME,_complex)), intent(inout) :: self
    complex(kind=dp),pointer             :: a( RANGE )
    call GetPointer(self%a)
    contains
    subroutine GetPointer(actual)
        complex(kind=dp),target             :: actual( RANGE )
        a => actual
    end subroutine
end function

subroutine CAT(TNAME,initI)(self,INDICES)
    class(CAT(TNAME,_int)), intent(inout) :: self
    integer,intent(in) :: INDICES
    if (allocated(self%a)) deallocate(self%a)
    allocate(self%a(INDICES))
    self%a(RANGE) = 0.d0
end subroutine

subroutine CAT(TNAME,initR)(self,INDICES)
    class(CAT(TNAME,_real)), intent(inout) :: self
    integer,intent(in) :: INDICES
    if (allocated(self%a)) deallocate(self%a)
    allocate(self%a(INDICES))
    self%a(RANGE) = 0.d0
end subroutine

subroutine CAT(TNAME,initC)(self,INDICES)
    class(CAT(TNAME,_complex)), intent(inout) :: self
    integer,intent(in) :: INDICES
    if (allocated(self%a)) deallocate(self%a)
    allocate(self%a(INDICES))
    self%a(RANGE) = (0.d0,0.d0)
end subroutine

subroutine CAT(TNAME,AssignIFromArray)(left, right)
    class(CAT(TNAME,_int)), intent(inout) :: left
    integer(kind=int32), intent(in) :: right( RANGE )

    integer :: dims( RANK )
    if (allocated(left%a)) then
        deallocate(left%a)
    end if
    dims = shape(right)
    call left%init(SECTION)
    left%a = right
end subroutine

subroutine CAT(TNAME,AssignRFromArray)(left, right)
    class(CAT(TNAME,_real)), intent(inout) :: left
    real(kind=dp), intent(in) :: right( RANGE )

    integer :: dims( RANK )
    if (allocated(left%a)) then
        deallocate(left%a)
    end if
    dims = shape(right)
    call left%init(SECTION)
    left%a = right
end subroutine


subroutine CAT(TNAME,AssignCFromArray)(left, right)
    class(CAT(TNAME,_complex)), intent(inout) :: left
    complex(kind=dp), intent(in) :: right( RANGE )

    integer :: dims( RANK )
    if (allocated(left%a)) then
        deallocate(left%a)
    end if
    dims = shape(right)
    call left%init(SECTION)
    left%a = right
end subroutine


subroutine AssignIToArray(left, right)
    integer(kind=int32), intent(inout) :: left( RANGE )
    type(CAT(TNAME,_int)), intent(in) :: right
    integer :: sl( RANK ),sr( RANK )

    if (.not. allocated(right%a)) then
        ! this is really undefined, but we handle gracefully
        left( RANGE ) = 0.d0
        return
    end if

    sl = shape(left)
    sr = shape(right%a)
    if (any(sl /= sr)) then
        print *, "Error: cannot preform uAssignRToArraynsafe assignment & no re-shaping done."
        ! this sohuld throw an exception ...
        return
    end if

    left( RANGE ) = right%a( RANGE )
end subroutine

subroutine AssignRToArray(left, right)
    real(kind=dp), intent(inout) :: left( RANGE )
    type(CAT(TNAME,_real)), intent(in) :: right
    integer :: sl( RANK ),sr( RANK )

    if (.not. allocated(right%a)) then
        ! this is really undefined, but we handle gracefully
        left( RANGE ) = 0.d0
        return
    end if

    sl = shape(left)
    sr = shape(right%a)
    if (any(sl /= sr)) then
        print *, "Error: cannot preform uAssignRToArraynsafe assignment & no re-shaping done."
        ! this sohuld throw an exception ...
        return
    end if

    left( RANGE ) = right%a( RANGE )
end subroutine


subroutine AssignCToArray(left, right)
    complex(kind=dp), intent(inout) :: left( RANGE )
    type(CAT(TNAME,_complex)), intent(in) :: right
    integer :: sl( RANK ), sr( RANK )

    if (.not. allocated(right%a)) then
        ! this is really undefined, but we handle gracefully
        left( RANGE ) = (0.d0,0.d0)
        return
    end if

    sl= shape(left)
    sr = shape(right%a)

    if (any(sl /= sr)) then
        print *, "Error: cannot preform unsafe assignment & no re-shaping done."
        ! this sohuld throw an exception ...
        return
    end if

    left( RANGE ) = right%a( RANGE )
end subroutine

