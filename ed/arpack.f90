module arpack_interface
    use m_constants, only: dp, czero
    use iso_c_binding
    private
    public :: diag_ham_arpack, pspmv_csr

    ! local variables
    integer      :: ido
    integer      :: ishfts
    integer      :: mode
    integer      :: lworkl
    integer      :: iparam(11)
    integer      :: ipntr(14)

    logical      :: rvec
    character(1) :: bmat
    character(2) :: which
    complex(dp)  :: sigma

    real(dp)   , allocatable  :: rwork(:)
    logical    , allocatable  :: selec(:)
    complex(dp), allocatable  :: resid(:)
    complex(dp), allocatable  :: v(:,:)
    complex(dp), allocatable  :: workd(:)
    complex(dp), allocatable  :: workl(:)
    complex(dp), allocatable  :: workev(:)
    complex(dp), allocatable  :: d(:)


    interface
        subroutine pznaupd_int( comm, ido, bmat, n, which, nev, tol, resid, ncv, v, ldv,&
                iparam, ipntr, workd, workl, lworkl, rwork, info)
            integer          :: comm, ido, iparam(11), ipntr(14)
            integer          :: info, ldv, lworkl, n, ncv, nev
            complex*16       ::   v(ldv,ncv) , resid(n), workd(3*n), workl(lworkl)
            Double precision :: tol, rwork(ncv)
            character        :: bmat, which*2
        end subroutine
    end interface

    interface
        subroutine pzneupd_int(comm , rvec  , howmny, select, d,&
                z    , ldz   , sigma , workev, bmat ,&
                n    , which , nev   , tol   , resid,&
                ncv  , v     , ldv   , iparam, ipntr,&
                workd, workl , lworkl, rwork , info )
            integer            :: comm, iparam(11), ipntr(14)
            integer            :: info, ldz, ldv, lworkl, n, ncv, nev
            complex*16         :: sigma, v(ldv,ncv) , d(nev), resid(n), workev(2*ncv), z(ldz, nev), workd(3*n), workl(lworkl)
            Double precision   :: tol, rwork(ncv)
            logical            :: rvec, select(ncv)
            character          :: bmat, howmny, which*2
        end subroutine
    end interface


    type(c_ptr) :: handle = c_null_ptr
    procedure(pznaupd_int), pointer :: pznaupd => null()
    procedure(pzneupd_int), pointer :: pzneupd => null()

    integer(kind=c_int64_t), parameter :: rtld_lazy=1 !  from the C header file
    integer(kind=c_int64_t), parameter :: rtld_now=2  !  from the C header file


    ! interface to linux API
    interface
        function dlopen(filename,mode) bind(c,name="dlopen")
            ! void *dlopen(const char *filename, int mode);
            use iso_c_binding
            implicit none
            type(c_ptr) :: dlopen
            character(kind=c_char), intent(in) :: filename(*)
            integer(kind=c_int64_t), value :: mode
        end function

        function dlsym(handle,name) bind(c,name="dlsym")
            ! void *dlsym(void *handle, const char *name);
            use iso_c_binding
            implicit none
            type(c_funptr) :: dlsym
            type(c_ptr), value :: handle
            character(kind=c_char), intent(in) :: name(*)
        end function

        function dlclose(handle) bind(c,name="dlclose")
            ! int dlclose(void *handle);
            use iso_c_binding
            implicit none
            integer(kind=c_int) :: dlclose
            type(c_ptr), value :: handle
        end function
    end interface

contains

    !! Use parallel arpack to find a few lowest eigenstates of a large sparse Hamiltonian
    subroutine diag_ham_arpack(nblock, needed, nloc, nev, eval, &
            n_vecs, evec, ncv, maxiter, tol, info, actual_step, nconv)

        use m_constants, only: dp, czero
        use m_control,   only: nprocs
        use edx, only: T_csr
        use mpi
        use iso_c_binding
        use persistence, only: die
        implicit none

        ! external variables
        integer, intent(in)       :: nblock
        integer, intent(in)       :: needed(nprocs, nprocs)
        integer, intent(in)       :: nloc
        integer, intent(in)       :: nev
        real(dp), intent(out)     :: eval(nev)
        integer, intent(in)       :: n_vecs
        complex(dp), intent(out)  :: evec(nloc,n_vecs)
        integer, intent(in)       :: ncv
        integer, intent(in)       :: maxiter
        real(dp), intent(in)      :: tol
        integer, intent(out)      :: info
        integer, intent(out)      :: actual_step
        integer, intent(out)      :: nconv
        integer                   :: i
        integer                   :: sorted_indx(nev)


        if (.not. associated(pznaupd) .or. .not. associated(pzneupd) ) then
            if (.not. c_associated(handle)) handle=dlopen("libARPACK.so"//c_null_char, rtld_lazy)
            if (.not. c_associated(handle)) handle=dlopen("libARPACK.dylib"//c_null_char, rtld_lazy)
            if (.not. c_associated(handle)) then
                print *, "** cannot load libarpack - stopping program"
                call Die()
            end if
            call c_f_procpointer( dlsym(handle, "pznaupd_"//c_null_char), pznaupd)
            if (.not. associated(pznaupd)) then
                print *, "** cannot load pznaupd - stopping program"
                call Die()
            end if

            call c_f_procpointer( dlsym(handle, "pzneupd_"//c_null_char), pzneupd)
            if (.not. associated(pzneupd)) then
                print *, "** cannot load pzneupd - stopping program"
                call Die()
            end if
        end if


        lworkl  = 3*ncv**2 + 5*ncv

        info      = 1
        if (.not. allocated(rwork)) then
            allocate(rwork(ncv))
            allocate(selec(ncv))
            allocate(resid(nloc))
            allocate(v(nloc,ncv))
            allocate(workd(3*nloc))
            allocate(workl(lworkl))
            allocate(workev(2*ncv))
            allocate(d(ncv))
            info      = 0  ! don't use previous resid
            resid = 0.0
        end if
        rwork = 0.0
        v = 0.0
        d = czero

        ishfts    = 1
        mode      = 1       ! therefore no calls with ido=1
        iparam(:)  = 0
        iparam(1) = ishfts  ! no shift calls with ido=3
        iparam(3) = maxiter
        iparam(7) = mode
        ipntr(:) = 0
        bmat      = 'I'   ! no multiplicative matrix
        which     = 'SR'  ! smallest real part
        ido       = 0
        selec     = .true.
        workd(:) = czero
        workl(:) = czero
        workl(:) = czero
        workev(:) = czero

        do while (ido /= 99)
            call pznaupd(MPI_COMM_WORLD, ido, bmat, nloc, which, nev, tol, resid, ncv, &
                v, nloc, iparam, ipntr, workd, workl, lworkl, rwork, info)
            if (info /= 0) exit
            if (ido .eq. -1 .or. ido .eq. 1) then
                !print *, "multiplying vector at ", ipntr(1), ipntr(2)
                workd(ipntr(2):ipntr(2)+nloc-1) = czero
                call pspmv_csr(MPI_COMM_WORLD, nblock, needed, nloc, nloc, &
                    workd(ipntr(1)), workd(ipntr(2)))
            endif
            if ((ido==3).or.(ido==2)) then
                ! this is a bug! Code is not designed for this case - it is assumed to work in a mode
                ! where these ido's are never encountered
                print *, "******* unexpected IDO ******", ido
                call Die()
            endif
        enddo
        actual_step = iparam(3)
        nconv = iparam(5)

        if (info .lt. 0) then
            return
        else
            rvec = .true.
            call pzneupd(MPI_COMM_WORLD, rvec, 'A', selec, d, v, nloc, sigma, workev, &
                bmat, nloc, which, nev, tol, resid, ncv, v, nloc, iparam, &
                ipntr, workd, workl, lworkl, rwork, info)
            if (info .ne. 0) then
                return
            endif
            ! sort eigenvalues to ascending order
            eval = real(d(1:nev))
            do i=1,nev
                sorted_indx(i) = i
            enddo
            call sort_eigvals(nev, eval, sorted_indx)
            do i=1,n_vecs
                evec(:,i) = v(:,sorted_indx(i))
            enddo
            resid(:) = evec(:,1)  ! starting vector for next run
        endif

    end subroutine diag_ham_arpack

    subroutine pspmv_csr(comm, nblock, needed, mloc, nloc, vec_in, vec_out)
        use m_constants, only: dp, czero
        use m_control, only: myid, nprocs
        use ed_ham, only: end_indx, scr_blocks
        use mpi

        implicit none

        ! externval variables
        integer, intent(in)         :: comm
        integer, intent(in)         :: nblock
        integer, intent(in)         :: needed(nprocs, nprocs)
        integer, intent(in)         :: mloc
        integer, intent(in)         :: nloc
        complex(dp), intent(in)     :: vec_in(nloc)
        complex(dp), intent(inout)  :: vec_out(mloc)

        ! local variables
        integer :: i
        integer :: ireq
        integer :: ierror
        integer :: request(nprocs)
        integer :: send_stat(MPI_STATUS_SIZE, nprocs)
        integer :: stat(MPI_STATUS_SIZE)
        integer :: other_size
        integer :: max_size
        complex(dp), allocatable :: tmp_vec(:)

        ! send the data
        ireq = 0
        do i=1,nprocs
            if (needed(i,myid+1) == 1) then
                ireq = ireq + 1
                call MPI_ISEND(vec_in, nloc, MPI_DOUBLE_COMPLEX, i-1, i*(10*nprocs)+myid+1, &
                    comm, request(ireq), ierror)
            endif
        enddo

        ! do matrix vector multiplication
        ! diagonal part
        call matvec_csr(mloc, nloc, scr_blocks%ham_csr(myid+1), vec_in, vec_out)

        ! off-diagonal parts
        if (nblock == nprocs .and. nprocs > 1) then
            max_size = end_indx(2,2,1)-end_indx(1,2,1) + 1
            do i=2,nprocs
                if ( (end_indx(2,2,i) - end_indx(1,2,i) + 1) > max_size ) then
                    max_size = end_indx(2,2,i) - end_indx(1,2,i) + 1
                endif
            enddo
            allocate(tmp_vec(max_size))
            do i=1,nprocs
                if (needed(myid+1,i) == 1) then
                    ! receive data from other processor
                    other_size = end_indx(2,2,i) - end_indx(1,2,i) + 1
                    tmp_vec = czero
                    call MPI_RECV(tmp_vec(1:other_size), other_size, MPI_DOUBLE_COMPLEX, i-1, MPI_ANY_TAG,&
                        comm, stat, ierror)
                    ! do matrix vector multiplication
                    call matvec_csr(mloc, other_size, scr_blocks%ham_csr(i), tmp_vec(1:other_size), vec_out)
                endif
            enddo
            if (allocated(tmp_vec)) deallocate(tmp_vec)
        endif

        call MPI_WAITALL(ireq, request, send_stat, ierror)

        return
    end subroutine pspmv_csr

    subroutine matvec_csr(m, n, mat, vec_in, vec_out)
        use m_constants, only: dp
        use edx, only: T_csr
        implicit none

        ! externval variables
        integer, intent(in) :: m
        integer, intent(in) :: n
        type (T_csr), intent(in) :: mat
        complex(dp), intent(in) :: vec_in(n)
        complex(dp), intent(inout) :: vec_out(m)

        ! local variables
        complex(dp) :: tmp
        integer :: i,j
        integer :: begin_col, end_col

        do i=1,mat%m
            begin_col = mat%iaa_(i)
            end_col = mat%iaa_(i+1)-1
            if (begin_col > end_col) cycle
            tmp = 0
            do j=begin_col, end_col
                tmp = tmp + mat%aa_(j) * vec_in(mat%jaa_(j) - mat%col_shift)
            enddo
            vec_out(i) = vec_out(i) + tmp
        enddo

        return
    end subroutine matvec_csr


end module
