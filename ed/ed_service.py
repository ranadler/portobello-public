#!/usr/bin/env python3

'''
Created on March 15, 2021

@author: adler@physics.rutgers.edu
'''

import sys
from portobello.generated import edx
from portobello.generated.edx import Tensor2, Tensor4
from portobello.bus.persistence import Persistent
from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer
from portobello.bus.fortran import Fortran
from portobello.rhobusta.orbitals import PrintMatrix
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import numpy as np

initialized = False

def Diagonalize(DiagonalizerWorker, H1e, Utensor, MakeFockStates, rep, opts) -> edx.Output:
    """ Diagonalize the matrix H1e + Utensor in the space containing the fock states
        made by MakeFockStates() using parallel exact diagonalization.
    Args:
        H1e ([Matrix]): The one body Hamiltonian
        Utensor ([Tensor4): 4 dim tensor, containing the interaction
        MakeFockStates ([function]]): Function to make the fock state (used only on the setup run)
        rep ([Matrix]): representation matrix for the entire one body space
        mpi_workers ([int]): number of workers to use
        opts ([Namespace]): more optsions from command line (must be defined): verbise, solver_num_evs, solver_tmp_dir 

    Returns:
        edx.Output: [description]
    """
    rtol = max(opts.interaction_truncation, 1.0e-10)
    ed_solver = 2 # ARPACK = 2, lanczos = 1, matrix - 0
    # only for 1st iteration prepare the setup data structure
    global initialized
    if not initialized:
        fock_states = MakeFockStates()
        setup = edx.Setup(num_fock = len(fock_states))
        setup.allocate()
        setup.num_val_orbs = H1e.shape[0]
        setup.nvector = 1
        setup.neval = opts.solver_num_evs
        setup.debug_print = opts.verbose == 1
        which_shell = 0
        if opts.which_shell >= 0:
            which_shell = opts.which_shell
        setup.prefix = f"{opts.solver_tmp_dir}/V0.{opts.mpi_workers}-shards.shell-{which_shell}.tmp.shard-"
        

        #for arpack
        setup.ncv = 30
        setup.ed_solver = ed_solver
        setup.eigval_tol = opts.ed_tolerance
        setup.maxiter = 500
        
        setup.fock[:] = fock_states[:]
        ulen = 0
        for (i1,i2,i3,i4), val in np.ndenumerate(Utensor):
            if np.abs(val) >= rtol and i1 != i2 and i3 != i4:
                ulen += 1
            
        setup.coulomb = Tensor4(len = ulen)
        setup.coulomb.allocate()
        idx = 0
        for (i1,i2,i3,i4), val in np.ndenumerate(Utensor):
            if np.abs(val) >= rtol and i1 != i2 and i3 != i4: # fermionic exclusion: two incoming and two outoing are different states (note these are full states) 
                setup.coulomb.i1[idx] = i1 + 1
                setup.coulomb.i2[idx] = i2 + 1
                setup.coulomb.i3[idx] = i3 + 1 
                setup.coulomb.i4[idx] = i4 + 1
                setup.coulomb.value[idx] = val
                idx+=1
        
        setup.hopping_rep = Tensor2(len=np.sum(rep != 0))
        setup.hopping_rep.allocate()
        idx = 0
        for (i1,i2), val in np.ndenumerate(rep):
            if val != 0:
                setup.hopping_rep.i1[idx] = i1 + 1
                setup.hopping_rep.i2[idx] = i2 + 1
                setup.hopping_rep.value[idx] = val*1.0 # the value doesn't matter, it's just for documentation
                idx+=1
        setup.store("./edx.tmp.h5:/setup/", flush=True)
        initialized = True

    inp = edx.Input()

    if opts.verbose==1: PrintMatrix("H1e", H1e)

    inp.hopping = Tensor2(len = np.sum(rep != 0))
    inp.hopping.allocate()
    idx = 0
    num_off_diag = 0
    for (i1,i2), val in np.ndenumerate(H1e):
        # the condition about rep here maintains assumption ASSUME:HOPPING_IN_REP, search
        # this string in Fortran to find where it is used
        if abs(val) > 1.0e-10 and rep[i1,i2] > 0:
            if i1 != i2: num_off_diag +=1
            inp.hopping.i1[idx] = i1 + 1
            inp.hopping.i2[idx] = i2 + 1
            inp.hopping.value[idx] = val
            #print "** added hopping ", idx, i1+1, i2+1, val
            idx+=1
    inp.store("./edx.tmp.h5:/input/", flush=True)

    if num_off_diag >0 and opts.verbose == 1:
        PrintMatrix(" - hopping matrix has off diagonals:", H1e)

    out = edx.Output()
    repeats = 1
    if ed_solver == 1: 
        repeats = 6
    for i in range(repeats):
        if i>0:
            print("convergence failed ... RETRYING solver")
            sys.stdout.flush()
        DiagonalizerWorker(opts)
        out.load("./edx_out.tmp.h5:/", disconnect=True)
        #if not os.path.exists("./edx_first.h5"):
        #    out.store("./edx_first.h5:/", flush=True)
        if out.is_valid:
            break
    if not out.is_valid:
        print("*** diagonalization solver convergence failed! ***")

    return out
