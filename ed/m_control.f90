!!>>> set control parameters
module m_control
    use m_constants, only: dp

    implicit none

    ! which ed solver is used
    integer, public, save :: ed_solver = 1

    ! number of valence orbitals
    integer, public, save :: num_val_orbs = 1


    ! how many smallest eigenvalues to be found
    integer, public, save :: neval = 1

    integer, public, save :: ncv = 20

    ! how many eigenvectors are dumped out
    integer, public, save :: nvector = 1

    ! maximum of the dimension of Krylov space
    ! for diagonalizing initial Hamiltonian
    integer, public, save :: maxiter = 1

    ! minimum dimension for Lanczos or Arpack
    integer, public, save :: min_ndim = 100

    ! dimension of Krylov space, for building XAS and RIXS
    integer, public, save :: nkryl = 500

    ! the tolerance of eigen values
    real(dp), public, save :: eigval_tol = 1E-10

    ! number of process
    integer, public, save :: origin_nprocs = 1
    integer, public, save :: nprocs = 1

    ! the index of process
    integer, public, save :: origin_myid = 0
    integer, public, save :: myid = 0

    ! the index of master process
    integer, public, save :: master = 0


end module m_control
