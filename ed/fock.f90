module ed_fock

contains

subroutine cal_combination(n, m, cnm)
    use m_constants, only: dp

    implicit none

    ! external variables
    integer, intent(in) :: n
    integer, intent(in) :: m
    integer, intent(out) :: cnm

    ! local variables
    integer :: small
    integer :: large
    integer :: i

    real(dp) :: x
    real(dp) :: y

    small = min(m,n-m)
    large = max(m,n-m)

    x = 1.0_dp
    y = 1.0_dp
    do i=large+1, n
        x = x * dble(i)
    enddo

    do i=1, small
        y = y * dble(i)
    enddo

    cnm = nint(x / y)

    return
end subroutine cal_combination

!!>>> get new fock state and the sign
subroutine make_newfock(c_type, idx, old, new, sgn)
    use m_constants, only: mystd, dp
    use persistence, only: Die

    implicit none

    character(1), intent(in)  :: c_type
    integer,      intent(in)  :: idx
    integer(dp),  intent(in)  :: old
    integer(dp),  intent(out) :: new
    integer,      intent(out) :: sgn

    integer :: i

    sgn = 0
    do i=1,idx-1
       if (btest(old, i-1) .eqv. .true.) sgn = sgn + 1
    enddo
    sgn = mod(sgn,2)

    !sgn = (-1)**sgn
    if (sgn == 0) then
        sgn = 1
    elseif (sgn == 1) then
        sgn = -1
    endif

    if (c_type == '+') then
        new = IBSET(old, idx-1)
    elseif (c_type == '-') then
        new = IBCLR(old, idx-1)
    else
        write(mystd, "(58a,a)")  " fedrixs >>> error of create and destroy operator type: ", c_type
        call Die()
    endif

    return
end subroutine make_newfock

function binary_search(n, vec, m) result(retval)
    use m_constants, only: dp

    implicit none

! external variables
    integer, intent(in) :: n
    integer, intent(in) :: vec(n)
    integer(dp), intent(in) :: m

! return value
    integer :: retval

! local variables
    integer :: left
    integer :: right
    integer :: middle

    retval = -1

    left=1
    right=n
    do while (left <= right)
        middle = left + (right-left)/2

        ! find the index
        if (vec(middle) < m) then
            left = middle + 1
        elseif (vec(middle) > m) then
            right = middle -1
        else
            retval = middle
            return
        endif
    enddo
end function binary_search


end module
