
module ed_ham
    use edx, only: T_csr, CSRBlocks
    use m_constants, only: dp, czero
    implicit none
    private

    public :: end_indx,  nblock, scr_blocks, &
              Initialize, ObtainV0Scr,  AddHoppingsToCSR, &
              EraseHamSCR, &
              get_number_nonzeros, get_needed_indx, csr_to_full


    type(CSRBlocks),allocatable :: scr_blocks

    ! end index of the Fock basis for each cpu core
    integer,allocatable :: end_indx(:,:,:)


    integer :: nblock

    !-------------------- following stuff is all private ---------------


    ! one column in one row of a sparse matrix
    type T_col
        ! current column index
        integer :: col
        ! value in this position
        complex(dp) :: val
        ! next column position
        type (T_col), pointer :: next
    end type T_col

    ! simulate array (just one row) of pointers of T_col
    type T_container_col
        ! just a pointer to T_col
        type (T_col), pointer ::  col_ptr
    end type T_container_col

    type (T_container_col), allocatable :: cols(:)


contains

    subroutine partition_task(nprocs, m, n)
        implicit none

        ! external variables
        integer, intent(in)  :: nprocs
        integer, intent(in)  :: m
        integer, intent(in)  :: n

        ! local variables
        integer :: step
        integer :: i

        end_indx = 0
        ! nprocs <= m, n
        step = m / nprocs
        do i=1,nprocs-1
            end_indx(1,1,i) = (i-1)*step+1
            end_indx(2,1,i) =     i*step
        enddo
        end_indx(1,1,nprocs) = (nprocs-1)*step+1
        end_indx(2,1,nprocs) = m

        step = n / nprocs
        do i=1,nprocs-1
            end_indx(1,2,i) = (i-1)*step+1
            end_indx(2,2,i) =     i*step
        enddo
        end_indx(1,2,nprocs) = (nprocs-1)*step+1
        end_indx(2,2,nprocs) = n

    end subroutine partition_task


    subroutine Initialize(nprocs, num_fock)
        implicit none
        integer, intent(in) :: nprocs, num_fock
        nblock = nprocs
        if (.not. allocated(scr_blocks))  then
            allocate(scr_blocks)
            allocate(end_indx(2,2,nblock))
            allocate(cols(nblock))
            call partition_task(nprocs, num_fock, num_fock)
        end if
    end subroutine Initialize

    subroutine ObtainV0Scr(sp)
        use edx, only: Setup
        use m_control, only: master, myid
        type(Setup), intent(in) :: sp

        logical::exists
        character(len=40) :: fname
        character(len=10) :: char_I

        write(char_I, '(i5)') myid
        fname=sp%prefix//trim(adjustl(char_I))//".h5"
        exists = .false.
        inquire(file = fname, exist=exists)

        ! if V file exists for this processor
        !    read it
        if (exists) then
            ! read SCR matrices
            if (myid==master .and. sp%debug_print) print *, "- reading partial hamiltonian"
            call scr_blocks%load(fname // ":/")
        else
            if (myid==master .and. sp%debug_print) print *, "- building the U term in the Hamiltonian SCR (once, takes a while)"
            scr_blocks%nblock = nblock
            call scr_blocks%allocate()
            !create the V for this processor and save it
            call CreateV0SCR( sp%num_fock, sp%fock_, sp%hopping_rep, sp%coulomb)

            ! we should not write multiple times, because sizes may change!
            ! Indeed we write exactly once in the beginning of a run, if the files don't exist
            call scr_blocks%store(fname // ":/", flush_opt=.True.)

        end if
    end subroutine


    subroutine EraseHamSCR()
        if (allocated(scr_blocks)) then
            deallocate(scr_blocks) ! destroys all blocks and the object that contains them
            allocate(scr_blocks)  ! we always keep this object allocated
        end if
    end subroutine



        subroutine AddToCSR(csr, cmplx_val, icfg, jcfg, alpha,beta, i, which_block, ncfgs, fock)
            use edx, only: T_csr
            use persistence, only:Die
            integer :: icfg, jcfg, alpha, beta
            type(T_csr), intent(inout) :: csr
            complex(dp), intent(in) :: cmplx_val
            integer :: m,n,jj, start_col,end_col, i, which_block
            integer, intent(in) :: ncfgs
            integer, intent(in) :: fock(ncfgs)
            logical :: found

            found = .False.

            ! these start at 0 or 1?
            m = icfg - csr%row_shift
            n = jcfg !- csr%col_shift

            if (m > csr%m) then
                print *, "m exceeds matrix bound:", m, csr%m
                print *, "ihopp=", i
                print *, "alpha, beta =", alpha, beta
                stop
            end if

            start_col = csr%iaa_(m)  ! this is the row start index
            end_col = csr%iaa_(m+1) - 1  ! always valid

            ! TODO: make this a binary search if interval is big
            do jj=start_col,end_col
                if (csr%jaa_(jj) == n) then
                    csr%aa_(jj) = csr%aa_(jj) + cmplx_val
                    found = .True.
                    return
                end if
            end do

            if (.not. found) then
                ! find the string ASSUME:HOPPING_IN_REP in Python to verify where this assumption is maintained
                print *, "Assumption ASSUME:HOPPING_IN_REP violated - configuration not found in the CSR matrix", icfg, jcfg
                print *, "ihopp=", i
                print *, "alpha, beta=", alpha, beta
                WRITE(*,'(B64)') fock(icfg)
                WRITE(*,'(B64)') fock(jcfg)
                print *, "value", cmplx_val
                print *, "start, end columns, which block", start_col, end_col, which_block
                call Die()  ! crash the program, there is a bug in the code
            end if

        end subroutine

    subroutine AddHoppingsToCSR(hopping, ncfgs, fock)
        use edx, only: Tensor2
        use m_control, only: myid
        use ed_fock, only: binary_search, make_newfock

        ! number of nonzero hopping terms
        integer, intent(in) :: ncfgs
        ! the hopping terms
        type (Tensor2), intent(in) :: hopping
        integer, intent(in) :: fock(ncfgs) ! todo: do we need double precision?? are ints long?


        ! local variables
        integer(dp) :: old, new
        integer :: icfg, jcfg
        integer :: alpha,beta
        integer :: tot_sign
        integer :: sgn
        integer :: i,j
        integer :: which_block

        do icfg=end_indx(1,1,myid+1), end_indx(2,1,myid+1)
            do i=1,hopping%len
                alpha = hopping%i1_(i)
                beta  = hopping%i2_(i)

                if (.not. btest(fock(icfg), alpha-1)) cycle
                old = fock(icfg)

                tot_sign = 1
                call make_newfock('-', alpha, old, new, sgn)
                tot_sign = tot_sign * sgn
                old = new

                if (btest(old, beta-1)) cycle
                call make_newfock('+', beta,  old, new, sgn)
                tot_sign = tot_sign * sgn

                jcfg = binary_search(ncfgs, fock, new)

                if (jcfg == -1) cycle

                do j=1,nblock
                    if (jcfg >= end_indx(1,2,j) .and. jcfg <= end_indx(2,2,j)) then
                        which_block = j
                    endif
                enddo

                ! add the hopping value to which_block CSR
                ! at row icfg, column jcfg

                call AddToCSR(scr_blocks%ham_csr(which_block), hopping%value_(i) * tot_sign, icfg, jcfg, alpha, beta, i, which_block, ncfgs, fock)
            enddo
        enddo


    end subroutine


    ! copy elements from csr to full
    subroutine csr_to_full(m, n, m_csr, m_full)
        implicit none

        ! external variables
        integer, intent(in)      :: m
        integer, intent(in)      :: n
        type (T_csr)             :: m_csr
        complex(dp), intent(out) :: m_full(m,n)

        ! local variables
        integer :: i
        integer :: j
        integer :: begin_col
        integer :: end_col

        ! init m_full
        do i=1,m_csr%m
            begin_col = m_csr%iaa_(i)
            end_col = m_csr%iaa_(i+1)-1
            if (begin_col > end_col) cycle
            do j=begin_col, end_col
                m_full(i+m_csr%row_shift, m_csr%jaa_(j)) = m_csr%aa_(j)
            enddo
        enddo

        return
    end subroutine csr_to_full


    subroutine add_zeroed_hoppings_to_pointer_matrix(icfg, ncfgs, hopping, fock)
        use edx, only: Tensor2
        use ed_fock, only: binary_search, make_newfock

        integer, intent(in) :: icfg, ncfgs
        ! the hopping terms
        type (Tensor2), intent(in) :: hopping
        integer, intent(in) :: fock(ncfgs)

        ! local variables
        integer(dp) :: old, new
        integer :: jcfg
        integer :: alpha,beta
        integer :: tot_sign
        integer :: sgn
        integer :: i,j
        integer :: which_block

        do i=1,hopping%len
            alpha = hopping%i1_(i)
            beta  = hopping%i2_(i)

            if (.not. btest(fock(icfg), alpha-1)) cycle
            old = fock(icfg)

            tot_sign = 1
            call make_newfock('-', alpha, old, new, sgn)
            tot_sign = tot_sign * sgn
            old = new

            if (btest(old, beta-1)) cycle
            call make_newfock('+', beta,  old, new, sgn)
            tot_sign = tot_sign * sgn

            jcfg = binary_search(ncfgs, fock, new)

            if (jcfg == -1) cycle

            do j=1,nblock
                if (jcfg >= end_indx(1,2,j) .and. jcfg <= end_indx(2,2,j)) then
                    which_block = j
                endif
            enddo

            ! init one row
            if (.not. associated(cols(which_block)%col_ptr)) then
                call init_col(cols(which_block)%col_ptr, jcfg, (0.d0,0.d0))
            else
                call insert_into_row(cols(which_block)%col_ptr, jcfg, (0.d0,0.d0))
            endif
        enddo
    end subroutine

    subroutine add_coul_to_pointer_matrix(icfg, ncfgs, coulomb, fock)
        use edx, only: Tensor4
        use ed_fock, only: binary_search, make_newfock

        ! number of nonzero hopping terms
        integer, intent(in) :: icfg, ncfgs
        ! the hopping terms
        type (Tensor4), intent(in) :: coulomb
        integer, intent(in) :: fock(ncfgs)

        integer(dp) :: old, new
        integer :: jcfg
        integer :: alpha,beta,gama,delta
        integer :: tot_sign
        integer :: sgn
        integer :: i,j
        integer :: which_block

        do i=1,coulomb%len
            alpha = coulomb%i1_(i)
            beta  = coulomb%i2_(i)
            gama  = coulomb%i3_(i)
            delta = coulomb%i4_(i)

            tot_sign = 1
            old = fock(icfg)
            if (.not. btest(old, alpha-1)) cycle
            call make_newfock('-', alpha, old, new, sgn)
            tot_sign = tot_sign * sgn
            old = new

            if (.not.btest(old, beta-1)) cycle
            call make_newfock('-', beta, old, new, sgn)
            tot_sign = tot_sign * sgn
            old = new

            if (btest(old, gama-1)) cycle
            call make_newfock('+', gama, old, new, sgn)
            tot_sign = tot_sign * sgn
            old = new

            if (btest(old, delta-1)) cycle
            call make_newfock('+', delta, old, new, sgn)
            tot_sign = tot_sign * sgn
            old = new

            jcfg = binary_search(ncfgs, fock, new)
            if (jcfg == -1) cycle

            do j=1,nblock
                if (jcfg >= end_indx(1,2,j) .and. jcfg <= end_indx(2,2,j)) then
                    which_block = j
                endif
            enddo
            if (.not. associated(cols(which_block)%col_ptr)) then
                call init_col(cols(which_block)%col_ptr, jcfg, coulomb%value_(i) * tot_sign)
            else
                call insert_into_row(cols(which_block)%col_ptr, jcfg, coulomb%value_(i) * tot_sign)
            endif
        enddo
    end subroutine

    subroutine CreateV0SCR(ncfgs, fock, hopping, coulomb)
        use m_constants, only: dp
        use m_control, only: myid, master
        use edx, only: Tensor2, Tensor4
        use mpi
        implicit none
        ! external variables
        ! the dimension of this Hilbert space
        integer, intent(in) :: ncfgs
        ! the fock basis of this Hilbert space
        integer, intent(in) :: fock(ncfgs)

        ! the hopping terms
        type (Tensor2), intent(in) :: hopping
        ! the interaction terms
        type (Tensor4), intent(in) :: coulomb

        ! local variables
        integer :: icfg
        integer :: i,j
        integer :: which_block
        integer :: row_shift
        integer :: begin_col

        integer, allocatable :: num_of_cols(:,:)
        integer :: num_of_tot(nblock)

        type (T_col), pointer :: next_col, curr_col

        which_block = 0
        row_shift = end_indx(1,1,myid+1) - 1
        allocate(num_of_cols(end_indx(2,1,myid+1)-end_indx(1,1,myid+1)+1, nblock))
        num_of_cols = 0
        num_of_tot = 0

        do icfg=end_indx(1,1,myid+1), end_indx(2,1,myid+1)
            do i=1,nblock
                cols(i)%col_ptr => null()
            enddo
            call add_zeroed_hoppings_to_pointer_matrix(icfg, ncfgs, hopping, fock)

            call add_coul_to_pointer_matrix(icfg, ncfgs, coulomb, fock)

            do i=1,nblock
                next_col => cols(i)%col_ptr
                do while (associated(next_col))
                    num_of_cols(icfg-row_shift, i) = num_of_cols(icfg-row_shift, i) + 1
                    curr_col => next_col
                    next_col => next_col%next
                    deallocate(curr_col)
                enddo
                num_of_tot(i) = num_of_tot(i) + num_of_cols(icfg-row_shift, i)
            enddo
        enddo

        if (myid==master) then
            print *, "    Allocate memory for SCR matrix"
        endif
        ! change to csr format
        do i=1,nblock
            if (num_of_tot(i) /= 0) then
                scr_blocks%ham_csr(i)%m = end_indx(2,1,myid+1) - end_indx(1,1,myid+1) + 1
                scr_blocks%ham_csr(i)%nnz = num_of_tot(i)
                scr_blocks%ham_csr(i)%row_shift = end_indx(1,1,myid+1) - 1
                scr_blocks%ham_csr(i)%col_shift = end_indx(1,2,i) - 1
                print *, "Allocating block", [myid+1, i], "nnz=", num_of_tot(i), "m=",  scr_blocks%ham_csr(i)%m
                call scr_blocks%ham_csr(i)%allocate()
                scr_blocks%ham_csr(i)%iaa_(1) = 1
                do j=2,scr_blocks%ham_csr(i)%m+1
                    scr_blocks%ham_csr(i)%iaa_(j) = scr_blocks%ham_csr(i)%iaa_(j-1) + num_of_cols(j-1, i)
                enddo
            else
                scr_blocks%ham_csr(i)%m = 0
                scr_blocks%ham_csr(i)%nnz = 0
                scr_blocks%ham_csr(i)%row_shift = end_indx(1,1,myid+1) - 1
                scr_blocks%ham_csr(i)%col_shift = end_indx(1,2,i) - 1
                ! for consistency allocate iaa(1) when m=0
                call scr_blocks%ham_csr(i)%allocate()
            endif
        enddo
        deallocate(num_of_cols)

        ! really building Hamiltonian here
        if (myid==master) then
            print *, "    Building SCR matrix from input"
        endif
        do icfg=end_indx(1,1,myid+1), end_indx(2,1,myid+1)
            do i=1,nblock
                cols(i)%col_ptr => null()
            enddo

            call add_zeroed_hoppings_to_pointer_matrix(icfg, ncfgs, hopping, fock)

            call add_coul_to_pointer_matrix(icfg, ncfgs, coulomb, fock)

            ! copy to T_csr
            do i=1,nblock
                if (num_of_tot(i) == 0) cycle
                begin_col = scr_blocks%ham_csr(i)%iaa_(icfg-row_shift) - 1
                next_col => cols(i)%col_ptr
                do while (associated(next_col))
                    begin_col = begin_col + 1
                    scr_blocks%ham_csr(i)%jaa_(begin_col) = next_col%col
                    scr_blocks%ham_csr(i)%aa_(begin_col)  = next_col%val
                    curr_col => next_col
                    next_col => next_col%next
                    deallocate(curr_col)
                enddo
            enddo
        enddo

        return
    end subroutine

    subroutine get_needed_indx(nblock, needed)
        use m_control, only: myid, nprocs
        use mpi
        implicit none

        integer, intent(in)  :: nblock
        integer, intent(out) :: needed(nprocs, nprocs)

        integer :: i
        integer :: needed_mpi(nprocs,nprocs)
        integer :: ierror

        needed = 0
        needed_mpi = 0
        if (nblock==nprocs) then
            do i=1,nprocs
                if (myid+1 /= i .and. scr_blocks%ham_csr(i)%nnz > 0) then
                    needed(myid+1, i) =  1
                endif
            enddo
            call MPI_BARRIER(MPI_COMM_WORLD, ierror)
            call MPI_ALLREDUCE(needed, needed_mpi, size(needed), MPI_INTEGER, MPI_SUM, MPI_COMM_WORLD, ierror)
            call MPI_BARRIER(MPI_COMM_WORLD, ierror)
            needed = needed_mpi
        endif

        return
    end subroutine get_needed_indx

    subroutine get_number_nonzeros(nblock, num_nonzeros)
        use m_constants, only: dp

        use mpi
        implicit none

        integer, intent(in)  :: nblock
        integer(dp), intent(out) :: num_nonzeros

        integer :: i
        integer(dp) :: num_nonzeros_mpi
        integer :: ierror

        num_nonzeros = 0_dp
        num_nonzeros_mpi = 0_dp
        do i=1,nblock
            num_nonzeros = num_nonzeros + scr_blocks%ham_csr(i)%nnz
        enddo
        call MPI_BARRIER(MPI_COMM_WORLD, ierror)
        call MPI_ALLREDUCE(num_nonzeros, num_nonzeros_mpi, 1, MPI_INTEGER16, MPI_SUM, MPI_COMM_WORLD, ierror)
        call MPI_BARRIER(MPI_COMM_WORLD, ierror)
        num_nonzeros = num_nonzeros_mpi

        return
    end subroutine get_number_nonzeros


    !!! important! do NOT make this optimized in case val=0.0: it should create the entry!
    subroutine init_col(self, col, val)
        implicit none

        ! external variables
        type (T_col), pointer :: self
        integer, intent(in) :: col
        complex(dp), intent(in) :: val

        allocate(self)
        self%col = col
        self%val = val
        self%next => null()

        return
    end subroutine init_col

    !!! important! do NOT make this optimized in case val=0.0: it should create the entry!
    subroutine insert_into_row(self, col, val)
        implicit none

        ! external variables
        type (T_col), pointer :: self
        integer, intent(in) :: col
        complex(dp), intent(in) :: val

        ! local variables
        type (T_col), pointer :: curr
        type (T_col), pointer :: prev
        type (T_col), pointer :: tmp

        curr => self
        prev => null()
        do while (associated(curr))
            ! compare col with current node
            if (col == curr%col) then
                curr%val = curr%val + val
                return
            endif

            ! insert before current node
            if (col < curr%col) then
                allocate(tmp)
                tmp%col = col
                tmp%val = val
                tmp%next => curr
                if (.not. associated(prev)) then
                    self => tmp
                    return
                endif
                prev%next => tmp
                return
            endif
            prev => curr
            curr => curr%next
        enddo

        ! arrive at the tail of the list
        allocate(tmp)
        tmp%col = col
        tmp%val = val
        tmp%next => null()
        prev%next => tmp

        return
    end subroutine insert_into_row



end module
