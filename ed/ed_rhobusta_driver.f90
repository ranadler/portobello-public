module ed_rhobusta_driver
    use edx, only: Setup, Input, Output
    implicit none

    type(Setup), allocatable :: sp

    type(Input), allocatable :: inp
    type(Output), allocatable :: res

contains

    subroutine run()
        use m_constants
        use m_control
        use m_lanczos
        use mpi
        use ed_ham
        use arpack_interface, only: diag_ham_arpack
        use ed_fock, only: binary_search, make_newfock
        implicit none

        integer :: i,j,kk
        integer :: icfg, jcfg
        integer :: nloc
        integer,allocatable :: needed(:,:)
        integer :: ierror
        integer  ::  request
        integer  :: stat(MPI_STATUS_SIZE)
        integer :: sgn
        integer :: tot_sign
        integer(dp) :: old, new
        integer(dp) :: num_of_nonzeros
        integer :: info, actual_step, nconv

        real(dp) :: norm1
        real(dp) :: time_begin
        real(dp) :: time_end
        real(dp) :: time_used
        real(dp), allocatable :: eigvals(:)
        real(dp), allocatable :: eigval_full(:)
        real(dp), allocatable :: saved_eigvecs(:, :)

        complex(dp), allocatable :: denmat(:, :, :)

        complex(dp), allocatable :: ham_full(:,:)
        complex(dp), allocatable :: ham_full_mpi(:,:)
        complex(dp), allocatable :: eigvecs_full(:,:)

        complex(dp), allocatable :: eigvecs(:,:)
        complex(dp), allocatable :: eigvecs_mpi(:)


        real(dp),allocatable :: histogram(:)
        integer(dp) :: num_orbital_fock_states, orbsmask
        real(dp) :: checksum

        logical :: first_run
        logical :: is_valid

        first_run = .False.
        time_begin = 0.0_dp
        time_end   = 0.0_dp
        time_used  = 0.0_dp
        call cpu_time(time_begin)

        ! this is taken from the main program, which we skipped

        call MPI_COMM_RANK(MPI_COMM_WORLD, myid,   ierror)
        call MPI_COMM_SIZE(MPI_COMM_WORLD, nprocs, ierror)

        allocate(needed(nprocs,nprocs))

        if (.not. allocated(sp)) then
            ! TODO: this is badly designed (since copied and modified)
            !       all these must be global variables (otherwise it will make bugs)
            first_run = .True.
            allocate(sp)
            call sp%load("./edx.tmp.h5:/setup/")
            ed_solver = sp%ed_solver
            num_val_orbs = sp%num_val_orbs
            maxiter = sp%maxiter
            eigval_tol = sp%eigval_tol
            nvector = sp%nvector
            neval = sp%neval
            ncv = sp%ncv
        end if

        is_valid = .True.
        allocate(inp)

        call inp%load("./edx.tmp.h5:/input/")

        if (sp%num_fock < min_ndim ) then
            if (first_run .and. (myid == master)) then
                print *, " - using full-matrix solver for a small problem: dimension is", sp%num_fock
            endif
            ed_solver = 0
        endif

        if(sp%num_fock < neval) then
            neval = sp%num_fock
        endif
        if(sp%num_fock < nvector) then
            nvector=sp%num_fock
        endif

        if (first_run .and. (myid == master)) then
            write(mystd,"(a20, i15)")   "# orbitals:      ", num_val_orbs
            write(mystd,"(a20, i15)")   "# hopping terms: ", inp%hopping%len
            write(mystd,"(a20, i15)")   "# int. terms:    ", sp%coulomb%len
            write(mystd,"(a20, i15)")   "# fock states:   ", sp%num_fock
            write(mystd,"(a20, i15)")   "ed_solver:       ", ed_solver
            write(mystd,"(a20, i15)")   "neval:           ", neval
            write(mystd,"(a20, i15)")   "nvector:         ", nvector
            write(mystd,"(a20, i15)")   "maxiter:         ", maxiter
            write(mystd,"(a20, i15)")   "nprocs:          ", nprocs
            write(mystd,"(a20, e15.2)") "eigval_tol:      ", eigval_tol
        endif

        nblock = nprocs
        call Initialize(nprocs, sp%num_fock)
        nloc = end_indx(2,2,myid+1)-end_indx(1,2,myid+1) + 1
        call ObtainV0Scr(sp)

        ! Now actually add the hoppings on top of V0
        call AddHoppingsToCSR(inp%hopping, sp%num_fock, sp%fock_)

        deallocate(inp)

        call MPI_BARRIER(MPI_COMM_WORLD, ierror)
        call get_needed_indx(nblock,  needed)
        call get_number_nonzeros(nblock, num_of_nonzeros)

        call cpu_time(time_end)
        time_used = time_used + time_end - time_begin
        if (myid==master .and. sp%debug_print) then
            print *, " - Number of nonzero elements of the Hamiltonian", num_of_nonzeros
            print *, " - Time used for building matrix: ", time_end - time_begin, "  seconds"
            print *, " - Diagonalizing Hamiltonian to find a few lowest states ..."
            print *
        endif
        time_begin = time_end

        allocate(eigvals(neval))
        ! full diagonalization
        if (ed_solver == 0) then
            allocate(ham_full(sp%num_fock, sp%num_fock))
            allocate(ham_full_mpi(sp%num_fock, sp%num_fock))
            allocate(eigvecs_full(sp%num_fock, sp%num_fock))
            allocate(eigval_full(sp%num_fock))
            allocate(eigvecs(sp%num_fock,nvector))
            ham_full = czero
            ham_full_mpi = czero
            eigvecs_full = czero
            eigval_full = zero
            do i=1,nblock
                call csr_to_full(sp%num_fock, sp%num_fock, scr_blocks%ham_csr(i), ham_full_mpi)
            enddo
            call MPI_ALLREDUCE(ham_full_mpi, ham_full, size(ham_full_mpi), MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, ierror)
            if (myid==master) then
                call full_diag_ham(sp%num_fock, ham_full, eigval_full, eigvecs_full)
            endif
            call MPI_BARRIER(MPI_COMM_WORLD, ierror)
            call MPI_BCAST(eigval_full,   size(eigval_full),  MPI_DOUBLE_PRECISION, master, MPI_COMM_WORLD, ierror)
            call MPI_BCAST(eigvecs_full,  size(eigvecs_full), MPI_DOUBLE_COMPLEX, master, MPI_COMM_WORLD, ierror)
            eigvals = eigval_full(1:neval)
            eigvecs = eigvecs_full(:,1:nvector)
            deallocate(ham_full)
            deallocate(ham_full_mpi)
            deallocate(eigvecs_full)
            deallocate(eigval_full)
        ! ordinary Lanczos method
        elseif (ed_solver == 1) then
            eigvals = zero
            allocate(eigvecs(nloc,nvector))
            eigvecs = czero
            call diag_ham_lanczos(nblock, needed, nloc, neval, maxiter, eigval_tol, eigvals, nvector, eigvecs)
        elseif (ed_solver == 2) then
            eigvals = zero
            allocate(eigvecs(nloc,nvector))
            eigvecs = czero
            call diag_ham_arpack(nblock, needed, nloc, neval, eigvals, nvector, &
                                 eigvecs, ncv, maxiter, eigval_tol, info, actual_step, nconv)
            if (info /=0) is_valid = .False.
            if (myid==master .and. sp%debug_print) then
                print *, "    arpack return info:  ", info
                print *, "    actual arnoldi step:  ", actual_step
                print *, "    number of converged Ritz values:  ", nconv
                print *
            endif
        endif
        call MPI_BARRIER(MPI_COMM_WORLD, ierror)

        call EraseHamSCR()

        call cpu_time(time_end)
        time_used = time_used + time_end - time_begin
        if (myid==master .and. sp%debug_print) then
            print *, " - Time used for lanczos: ", time_end-time_begin, "  seconds"
        endif
        time_begin=time_end

        if (myid==master .and. sp%debug_print) then
            print *, " - Calculating the density matrix ... "
        endif

        num_orbital_fock_states = 0
        num_orbital_fock_states = IBSET(num_orbital_fock_states, sp%num_val_orbs/2)
        allocate(histogram(num_orbital_fock_states)) ! sets it to 2**(num_val_orbs/2)
        orbsmask = num_orbital_fock_states - 1 ! which masks all the relevant bits
        histogram = 0.0
        allocate(denmat(num_val_orbs, num_val_orbs, nvector))
        denmat = czero

        if (is_valid) then
            allocate(eigvecs_mpi(sp%num_fock))
            allocate(saved_eigvecs(sp%num_fock, nvector))

            do kk=1, nvector
                eigvecs_mpi = czero
                if (ed_solver==0) then
                    eigvecs_mpi = eigvecs(:,kk)
                else
                    ! now the eigenvectors are collected from all processors but
                    ! we do it in chunks by SEND/RCV because doing it using REDUCE will be inefficient
                    ! since the nonsparse vector is very large.
                    ! send the data
                    do i=1,nprocs
                       if (myid+1 /= i) then
                           call MPI_ISEND(eigvecs(:,kk), nloc, MPI_DOUBLE_COMPLEX, i-1, &
                                  i*(10*nprocs)+myid+1, MPI_COMM_WORLD, request, ierror)
                       endif
                    enddo
                    eigvecs_mpi(end_indx(1,2,myid+1):end_indx(2,2,myid+1)) = eigvecs(:,kk)

                    ! receive data
                    do i=1,nprocs
                       if (myid+1 /= i) then
                           call MPI_RECV(eigvecs_mpi(end_indx(1,2,i):end_indx(2,2,i)), &
                                end_indx(2,2,i)-end_indx(1,2,i)+1, MPI_DOUBLE_COMPLEX, &
                                i-1, MPI_ANY_TAG, MPI_COMM_WORLD, stat, ierror)
                       endif
                    enddo
                endif
                call MPI_BARRIER(MPI_COMM_WORLD, ierror)

                if (kk ==1) then
                    !should we normalize?
                    norm1 = sqrt(dot_product(eigvecs_mpi, eigvecs_mpi))
                    if (isnan(norm1) .or. (norm1 < 1.0d-10)) then
                        is_valid = .False.
                    else
                        eigvecs_mpi = eigvecs_mpi / norm1
                    end if
                    do icfg=end_indx(1,1,myid+1), end_indx(2,1,myid+1)
                        do j=1,num_val_orbs
                            if (btest(sp%fock_(icfg), j-1)) then
                                denmat(j,j,kk) = denmat(j,j,kk) + conjg(eigvecs_mpi(icfg)) * eigvecs_mpi(icfg)
                            endif
                        enddo
                        new = sp%fock_(icfg)
                        histogram(and(new,orbsmask)+1) = histogram(and(new,orbsmask)+1) + conjg(eigvecs_mpi(icfg)) * eigvecs_mpi(icfg)

                        if ( abs(eigvecs_mpi(icfg)) < 1E-10 ) cycle
                        do i=1,num_val_orbs-1
                            do j=i+1,num_val_orbs
                                if (.not. btest(sp%fock_(icfg), i-1)) cycle
                                old = sp%fock_(icfg)

                                tot_sign = 1
                                call make_newfock('-', i, old, new, sgn)
                                tot_sign = tot_sign * sgn
                                old = new

                                if (btest(old, j-1)) cycle
                                call make_newfock('+', j,  old, new, sgn)
                                tot_sign = tot_sign * sgn

                                jcfg = binary_search(sp%num_fock, sp%fock_, new)

                                if (jcfg == -1) cycle
                                denmat(i,j,kk) = denmat(i,j,kk) + conjg(eigvecs_mpi(icfg)) * eigvecs_mpi(jcfg) * tot_sign
                                denmat(j,i,kk) = denmat(j,i,kk) + conjg(eigvecs_mpi(jcfg)) * eigvecs_mpi(icfg) * tot_sign

                            enddo
                        enddo
                    enddo
                end if ! kk==1

                ! store eigenvectors
                if (myid==master) then
                    saved_eigvecs(:,kk) = eigvecs_mpi(:)
                endif
            enddo  ! over kk=1, nvector


            call MPI_BARRIER(MPI_COMM_WORLD, ierror)
            call MPI_ALLREDUCE(MPI_IN_PLACE, denmat,    size(denmat),    MPI_DOUBLE_COMPLEX,    MPI_SUM,  MPI_COMM_WORLD, ierror)
            call MPI_ALLREDUCE(MPI_IN_PLACE, histogram, size(histogram), MPI_DOUBLE_PRECISION,  MPI_SUM,  MPI_COMM_WORLD, ierror)
        end if ! is_valid


        call MPI_BARRIER(MPI_COMM_WORLD, ierror)

        !!! write output back to rhobusta
        if (myid == master) then
            allocate(res)
            res%num_val_orbs = num_val_orbs
            res%nvector = nvector
            res%neval = neval
            res%num_fock = sp%num_fock
            res%is_valid = is_valid
            res%num_orbital_fock_states = num_orbital_fock_states
            call res%allocate()

            if (is_valid) then
                res%denmat_(:,:,:) = denmat(:,:,:)
                res%E_(:neval) = eigvals(:)
                res%eigvecs_(:,:) = saved_eigvecs(:,:)
                res%histogram_(:) = histogram(:)
            end if

            call res%store("./edx_out.tmp.h5:/", flush_opt=.True.)
            deallocate(res)
        endif

        deallocate(eigvecs)
        deallocate(eigvals)
        if (is_valid) then
            deallocate(eigvecs_mpi)
            deallocate(saved_eigvecs)
        endif

        deallocate(denmat)
        deallocate(histogram)

        call cpu_time(time_end)
        time_used = time_used + time_end - time_begin
        if (myid==master .and. sp%debug_print) then
            print *, " - time used: ", time_end-time_begin, "  seconds"
            print *
            print *, " - total ed call time: ", time_used, "  seconds"
            print *
        endif
        return

    end subroutine run


end module

