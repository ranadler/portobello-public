!!>>> binary search

subroutine sort_eigvals(n, eigvals, sorted_indx)
    use m_constants, only: dp, zero
    implicit none

    ! external variables
    integer,  intent(in)    :: n
    real(dp), intent(inout) :: eigvals(n)
    integer,  intent(inout) :: sorted_indx(n)

    ! local variables
    real(dp) :: tmp_eigval(n)
    integer  :: tmp_indx(n)
    integer  :: i
    integer  :: j

    tmp_eigval = eigvals
    tmp_indx   = sorted_indx

    ! we use simple insert sort algorithm
    eigvals = zero
    sorted_indx = 0
    eigvals(1) = tmp_eigval(1)
    sorted_indx(1) = tmp_indx(1)
    do i=2, n
        j=i-1
        do while (tmp_eigval(i) < eigvals(j))
            eigvals(j+1)     = eigvals(j)
            sorted_indx(j+1) = sorted_indx(j)
            j = j - 1
            if (j==0) EXIT
        enddo
        eigvals(j+1)     = tmp_eigval(i)
        sorted_indx(j+1) = tmp_indx(i)
    enddo

    return
end subroutine sort_eigvals


subroutine get_prob(n, eigval, beta, prob)
    use m_constants
    implicit none

    integer, intent(in) :: n
    real(dp), intent(in) :: eigval(n)
    real(dp), intent(in) :: beta
    real(dp), intent(out) :: prob(n)

    real(dp) :: rtemp(n)
    real(dp) :: norm
    integer :: i

    rtemp = eigval - minval(eigval)
    norm = 0.0
    do i=1,n
        norm = norm + exp(-beta * rtemp(i))
    enddo

    prob = exp(-beta * rtemp) / norm

    return
end subroutine get_prob
