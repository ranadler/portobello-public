#!/usr/bin/env python3
from glob import glob
import os
from pathlib import Path
import shutil
import sys
import subprocess
from argparse import ArgumentDefaultsHelpFormatter
import unittest
import numpy as np
from portobello.bus.mpi import GetMPIProxyOptionsParser

from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.bus.persistence import AttributeTypes, Store

def TestOptionsParser():
    # todo once we upgrade to python3.9 or higher, add the new exit_on_error=False argument to this construction of Argumentparser.   At present if the user requests help using -h, parse_known_args prints out the help and then exits before unittest has a chance to print out its help. If we use the exit_on_error = False that might allow unittest its chance to print out its help.
    parser = ArgumentParserThatStoresArgv('test_rhobusta', add_help=True,
                            parents=[GetMPIProxyOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--init-reference-data", dest="initialize_data",
                    action="store_true",
                    default=False,
                    help = "Do not run the test suite; instead create the suite's directory structure and populate it with data files.")


    parser.add_argument("--tests", dest="tests",
                    default="ParamagneticIronSimulation",
                    choices=["ParamagneticIronSimulation", "SpinIronSimulation", "RelativisticIronSimulation", "ParamagneticIronAnalysis", "RelativisticIronAnalysis"],
                    help = "Run this selection of (comma seperated) tests from the test suite.")

    parser.add_argument("--module", dest="module",
                    default="",
                    choices = ["dft", "proj", "gutz", "dmft", "dft+rho", "dft+sigma", "dft+g", "dft+dmft", "dos", "sf", "bands", "fs", "maxent", "transport"],
                    help = "Instead of running all modules in the testsuite, only run the specified module/")

    parser.add_argument("--abbreviated", dest="abbreviated",
                    default=False,
                    action = "store_true",
                    help = "Run a minimal set of tests rather than a comprehensive suite.")

    parser.add_argument("--no-gutz", dest="no_gutz",
                    default=False,
                    action = "store_true",
                    help = "Do not run the gutzwiller tests.")

    parser.add_argument("--no-dmft", dest="no_dmft",
                    default=False,
                    action = "store_true",
                    help = "Do not run the dmft tests.")


    parser.add_argument("--fail-fast", dest="failfast",
                    default=False,
                    action = "store_true",
                    help = "Conduct tests until there is an error, then end tests (rather than completing all tests).")

    return parser

class RhobustaTestSuiteBase(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.base_dir = os.getcwd()
             
        parser = TestOptionsParser()
        self.opts, argv_ = parser.parse_known_args(sys.argv)

    @classmethod
    def tearDownClass(self):
        os.chdir(self.base_dir)

    def cif(self):
        return None

    def name(self):
        return None

    def argv(self):
        return []
    

#The main interface for unittest.TestCases
class TesterClass:

    #tolerance for error
    def tol(self):
        return 1e-13

    #cumulative tolerance for error
    def atol(self):
        return 1e-8

    def __init__(self, name, cif, opts, extra_args = []):
        self.name = name
        self.cif = cif
        self.opts = opts
        self.extra_args = extra_args
        self.extra_args += [f"--mpi={opts.mpi_workers}"]

        self.base_dir = os.getcwd()
        self.ref_dir = self.base_dir + "/ref/" + self.name
        self.test_dir = self.base_dir + "/test/" + self.name
        print("\n", flush=True) #unittest will put some characters in stdout


    def skip(self, testcase):

        if self.opts.module != "" and self.opts.module != self.module_alias():
            print(f"Skipping {self.module_alias()}")
            testcase.skipTest(f"User specified test of module {self.opts.module}")

    def module(self):
        return ""

    def module_alias(self):
        return self.module()

    def default_options(self):
        return []

    def requires_dmft(self):
        return False

    def requires_gutz(self):
        return False

    def cleanup(self):
        pass

    def files_to_remove_before_running(self):
        return []

    def prepare(self):

        def prepare_dir(dir):
            
            Path(dir).mkdir(parents=True, exist_ok=True)

            if self.cif is not None:
                shutil.copyfile(f"{self.base_dir}/{self.cif}", f"{dir}/{self.cif}")

            def get_additional(method):
                tree = dir.split("/")
                source_dir = "/".join(tree[:-2])
                for file in ["ini", "flapw_image", "projector"]:
                    shutil.copyfile(f"{source_dir}/{file}.h5", f"{dir}/{file}.h5")

                shutil.copyfile(f"{source_dir}/{method}.h5", f"{dir}/{method}.h5")
                shutil.copyfile(f"{source_dir}/projector.h5", f"{dir}/projector.h5")

            if self.requires_dmft():
                get_additional("dmft")

            if self.requires_gutz():
                get_additional("gutz")

            os.chdir(dir)

        if self.opts.initialize_data:
            prepare_dir(self.ref_dir)
        else:
            prepare_dir(self.test_dir)

        try:
            for f in self.files_to_remove_before_running():
                assert(f[-2:]=='h5')
                os.unlink(f)
        except:
            pass

    def run(self):
        
        cmd = f"{sys.executable} -m portobello.rhobusta.{self.module()}".split() + self.default_options() + self.extra_args
        cmd = " ".join(cmd)
        print(f"Running {cmd} in {os.getcwd()}")
        stdout = open(f"{self.module()}.out.log","w")
        stderr = open(f"{self.module()}.err.log","w")
        p = subprocess.run(cmd, shell = True, stdout = stdout, stderr = stderr) 
        stdout.close()
        stderr.close()
        return p.returncode

    def RunTest(self, testcase):

        self.skip(testcase)

        self.prepare()
        
        returncode = self.run()

        if self.opts.initialize_data:
            errors = {}
        else:
            errors = self.compare()

        self.cleanup()

        os.chdir(self.base_dir)

        testcase.assertTrue( returncode == 0, "Program crashed during execution" )
        self.print_errors(errors)
        testcase.assertTrue(len(errors.keys())==0, "Mismatch in data structures between the reference and the test")

        return len(errors.keys())==0 and returncode == 0


    def add_schema_to_compare(self, filename, schema):
        return schema_to_compare(self.ref_dir, filename, schema)

    def schema_to_compare(self):
        return []

    def compare(self):
        Store.Singleton().ClearAll()
        errors = {}
        for todo in self.schema_to_compare():
            error = todo.compare()
            if len(error):
                errors[todo.filename] = error
        return errors
    
    def print_errors(self, errors):
        for key in errors.keys():
            print(f"hdf5 file {key} errors:")
            for error in errors[key]:
                print(f" -- {error}")

            
class schema_to_compare:
    def __init__(self, reference_dir, filename, schema):

        self.filename = filename
        self.schema = schema

        self.ref = schema()
        self.ref.load(f"{reference_dir}/{filename}")

        self.test = schema()
        self.test.load(f"./{filename}")

        self.value_error = False

    def compare_recursively_thru_objects(self, ref_obj, test_obj, errors, tol, atol):

        for type_name in ["_attributes", "_datasets"]:
            list_of_attributes = getattr(ref_obj, type_name) #map of attribute or dataset names: name->dtype
            for attr in list_of_attributes: 

                ref_attr = getattr(ref_obj, attr) #attribute or dataset
                ref_dtype = list_of_attributes[attr] #datatype of attribute or dataset

                nice_type = type_name[1:-1] #get rid of underscore and plural for a nicer printout
                
                try:
                    test_attr = getattr(test_obj, attr)
                except:
                    errors.append( f"{self.filename}: {nice_type} {attr} missing in test object" )
                    continue

                if ref_dtype != getattr(test_obj, type_name)[attr]:
                    errors.append( f"{self.filename}: type mismatch in {nice_type} {attr}" )
                    continue
                    
                #Note that if the attributes match, so too should the sizes
                #but it would be good to check sizes here -- need to deal with the structure of sizes though -- x.shape is odd
                if ref_dtype in [AttributeTypes.INT, AttributeTypes.FLOAT, AttributeTypes.COMPLEX]:
                    err = np.abs(ref_attr - test_attr)
                    if  err > tol:
                        errors.append( f"{self.filename}: value mismatch in {nice_type} {attr} with error {err}| {ref_attr} {test_attr}" )

                if ref_dtype in [AttributeTypes.BOOLEAN]:
                    if  ref_attr != test_attr:
                        errors.append( f"{self.filename}: value mismatch in boolean {nice_type} {attr}" )

        if hasattr(ref_obj, "_objects"):
            for obj in ref_obj._objects:
                try:
                    new_ref_obj = getattr(ref_obj, obj)
                    new_test_obj = getattr(test_obj, obj)
                except:
                    errors.append( f"{self.filename}: object {obj} missing" )
                    continue

                self.compare_recursively_thru_objects(new_ref_obj, new_test_obj, errors, tol, atol)

        

    def compare(self, tol=5e-8, atol=1e-5):
        errors = []
        self.compare_recursively_thru_objects(self.ref, self.test, errors, tol, atol)
        return errors