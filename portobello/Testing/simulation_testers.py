

from glob import glob
from pathlib import Path
import unittest
from portobello.Testing.interface import RhobustaTestSuiteBase, TesterClass

class DFT(TesterClass):

    def module(self):
        return "rhobusta"

    def module_alias(self):
        return "dft"

    def default_options(self):
        return f"{self.cif} -i15 -K1.0 --RK=7 -T3000".split()

    def schema_to_compare(self):
        from portobello.generated.FlapwMBPT import Input, Image, FullZB
        schemas = [
            self.add_schema_to_compare("ini.h5:/", Input),
            self.add_schema_to_compare("flapw_image.h5:/", Image),
        ]
        return schemas
                   
class Proj(TesterClass):

    def module(self):
        return "ProjectorEmbedder"

    def module_alias(self):
        return "proj"

    def schema_to_compare(self):
        from portobello.generated.atomic_orbitals import OrbitalsBandProjector
        schemas = [
            self.add_schema_to_compare("projector.h5:/def/", OrbitalsBandProjector)
        ]
        return schemas

class Gutz(TesterClass):

    def module(self):
        return "gutzwiller"

    def module_alias(self):
        return "gutz"

    def default_options(self):
        return "-U10 -J1 -T10 --solver-admix=0.9".split()

    def schema_to_compare(self):
        from portobello.generated.gutz import GState
        schemas = [
            self.add_schema_to_compare("gutz.h5:/", GState)
        ]
        return schemas

    def files_to_remove_before_running(self):
        return ["./gutz.h5"]

class DMFT(TesterClass):

    def module(self):
        return "dmft"

    def default_options(self):
        return "-U10 -J1 -D1 -T10000 --measurement-steps=1000 --thermalization-steps=0".split()

    def schema_to_compare(self):
        from portobello.generated.dmft import DMFTState
        schemas = [
            self.add_schema_to_compare("dmft.h5:/", DMFTState)
        ]
        return schemas

    def cleanup(self):
        files = glob('./Impurity/config*')
        for f in files:
            Path(f).unlink()

    def files_to_remove_before_running(self):
        return ["./dmft.h5"]

class DFT__Rho(DFT):

    def module_alias(self):
        return "dft+rho"

    def default_options(self):
        return "-G -y3".split() + DFT.default_options(self)

    def requires_gutz(self):
        return True

class DFT__Sigma(DFT):

    def module_alias(self):
        return "dft+sigma"

    def default_options(self):
        return "--plus-sigma -y3".split() + DFT.default_options(self)

    def requires_dmft(self):
        return True


class DFT__G(Gutz, DFT, Proj):

    def module(self):
        return "dft__g"
        
    def module_alias(self):
        return "dft+g"

    def default_options(self):
        return DFT.default_options(self) + Gutz.default_options(self) + Proj.default_options(self) + "-I1 -y3".split()

    def schema_to_compare(self):
        return DFT.schema_to_compare(self) + Gutz.schema_to_compare(self) + Proj.default_options(self)

class DFT__DMFT(DMFT, DFT, Proj):

    def module(self):
        return "dft__dmft"

    def module_alias(self):
        return "dft+dmft"

    def default_options(self):
        return DFT.default_options(self) + DMFT.default_options(self) + Proj.default_options(self) + "-I1 -y3".split()

    def schema_to_compare(self):
        return DFT.schema_to_compare(self) + DMFT.schema_to_compare(self) + Proj.default_options(self)


class RhobustaSimulationTests(RhobustaTestSuiteBase):

    def test1_dft(self):

        if self.opts.abbreviated:
            self.skipTest("Abbreviated test suite -- skipping sub-modules")

        Tester = DFT(self.name(), self.cif(), self.opts, extra_args = self.argv())

        success = Tester.RunTest(self)

    def test2_proj(self):

        if self.opts.abbreviated:
            self.skipTest("Abbreviated test suite -- skipping sub-modules")
            
        Tester = Proj(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test3_gutz(self):

        if self.opts.abbreviated:
            self.skipTest("Abbreviated test suite -- skipping sub-modules")

        if self.opts.no_gutz:
            self.skipTest("Abbreviated test suite -- skipping gutz modules")

        Tester = Gutz(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test4_dmft(self):

        if self.opts.abbreviated:
            self.skipTest("Abbreviated test suite -- skipping sub-modules")

        if self.opts.no_dmft:
            self.skipTest("Abbreviated test suite -- skipping dmft modules")

        Tester = DMFT(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test5_dft_plus_rho(self):

        if self.opts.abbreviated:
            self.skipTest("Abbreviated test suite -- skipping sub-modules")

        if self.opts.no_gutz:
            self.skipTest("DMFT test suite -- skipping gutzwiller modules")

        Tester = DFT__Rho(self.name()+"/dft+rho/", self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test6_dft_plus_sigma(self):

        if self.opts.abbreviated:
            self.skipTest("Abbreviated test suite -- skipping sub-modules")

        if self.opts.no_dmft:
            self.skipTest("Gutzwiller test suite -- skipping dmft modules")

        Tester = DFT__Sigma(self.name()+"/dft+sigma/", self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test7_dft_plus_g(self):

        if self.opts.no_gutz:
            self.skipTest("DMFT test suite -- skipping gutzwiller modules")

        Tester = DFT__G(self.name()+"/dft+g/", self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test8_dft_plus_dmft(self):

        if self.opts.no_dmft:
            self.skipTest("Gutzwiller test suite -- skipping dmft modules")

        Tester = DFT__DMFT(self.name()+"/dft+dmft/",  self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)
    