from portobello.Testing.interface import RhobustaTestSuiteBase, TesterClass

class DOS(TesterClass):

    def module(self):
        return "dos"

    def default_options(self):
        return f"-W0.1".split()

class DOSWithObs(DOS):

    def default_options(self):
        return DOS.default_options(self) + f"---obs=rep".split()

class SF(TesterClass):

    def module(self):
        return "bandstructure"

    def module_alias(self):
        return "sf"

    def default_options(self):
        return f"-W0.1 -K2".split()

class SFWithObs(SF):

    def default_options(self):
        return SF.default_options(self) + f"--obs=rep".split()
        

class Bands(TesterClass):

    def module(self):
        return "bandstructure"

    def module_alias(self):
        return "bands"

    def default_options(self):
        return f"-W0.1 -K2 --bands-only ".split()

class BandsWithObs(Bands):

    def default_options(self):
        return Bands.default_options(self) + f"--obs=rep".split()

class FS(TesterClass):

    def module_alias(self):
        return "fs"

    def module(self):
        return "fermi_surface"

class FSWithObs(FS):

    def default_options(self):
        return f"--obs=rep".split()

class Maxent(TesterClass):

    def module(self):
        return "maxent.Maxent"
    
    def module_alias(self):
        return "maxent"

    def schema_to_compare(self):
        from portobello.generated.self_energies import LocalOmegaFunction
        schemas = [
            self.add_schema_to_compare("real-imp-self-energy.h5:/", LocalOmegaFunction)
        ]
        return schemas

    def default_options(self):
        return f"-i5".split()

class Transport(TesterClass):

    def module(self):
        return "transport"

    def schema_to_compare(self):
        from portobello.generated.Transport import Output
        schemas = [
            self.add_schema_to_compare("transport.h5:/", Output)
        ]
        return schemas


class RhobustaAnalysisTests(RhobustaTestSuiteBase):

    def test0_maxent(self):
        Tester = Maxent(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test1_dos(self):
        Tester = DOS(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test2_dos_w_obs(self):
        Tester = DOSWithObs(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test3_bands(self):
        Tester = Bands(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test4_bands_w_obs(self):
        Tester = BandsWithObs(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test5_sf(self):
        Tester = SF(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test6_sf_w_obs(self):
        Tester = SFWithObs(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test7_fs(self):
        Tester = FS(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test8_fs_w_obs(self):
        Tester = FSWithObs(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)

    def test9_transport(self):
        Tester = Transport(self.name(), self.cif(), self.opts, extra_args = self.argv())
        success = Tester.RunTest(self)