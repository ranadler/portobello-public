from portobello.Testing.simulation_testers import *
from portobello.Testing.analysis_testers import *        

class IronSimulation(RhobustaSimulationTests):

    def cif(self):
        return "Fe.cif"

    def argv(self):
        return "-sFe:3d -N5 -Xnone -Badapt".split()

class ParamagneticIronSimulation(IronSimulation):

    def name(self):
        return "Fe-paramagnetic"

class SpinIronSimulation(IronSimulation):

    def name(self):
        return "Fe-spin"

    def argv(self):
        return IronSimulation.argv(self) + "-S --break=0,0,1 -p0.1".split()

class RelativisticIronSimulation(IronSimulation):

    def name(self):
        return "Fe-rel"

    def argv(self):
        return IronSimulation.argv(self) + "-R -Badapt -Xdiag --pre-adapt=Mz --break=0,0,1 -p0.1".split()

class ParamagneticIronAnalysis(RhobustaAnalysisTests):

    def name(self):
        return "Fe-paramagnetic"

class RelativisticIronAnalysis(RhobustaAnalysisTests):

    def name(self):
        return "Fe-rel"