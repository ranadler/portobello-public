#!/usr/bin/env python3
from pydoc import locate
import sys
import unittest
import io
import os
import xmlrunner
from xmlrunner.extra.xunit_plugin import transform

#something needs to be imported for Q test to work...
import portobello.rhobusta.rhobusta
import portobello.rhobusta.dft_plus_sigma 
import portobello.rhobusta.dmft

from portobello.Testing.interface import TestOptionsParser


if __name__ == '__main__':    

    parser = TestOptionsParser()    
    opts, _args = parser.parse_known_args(sys.argv)      

    #unittest will run anything we import 
    for test in opts.tests.split(","):
        this_imports_the_test_ = locate("portobello.Testing.TestSuites."+test)

    
    out = io.BytesIO()
    unittest.main(
        testRunner=xmlrunner.XMLTestRunner(output=out),
        module='__main__', defaultTest=None,  
        argv=_args, 
        exit=False,
        failfast = opts.failfast)

    # should we actually remove these extra values?
    # can we just use the output as is?
    print(f"- writing report.xml in {os.getcwd()}")
    with open('./report.xml', 'wb') as report:
        report.write(transform(out.getvalue()))
