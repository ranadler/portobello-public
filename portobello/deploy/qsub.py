#!/usr/bin/env python3

'''
Created on Jul 25, 2018

@author: adler
'''
from subprocess import Popen, PIPE, STDOUT
import sys
import glob
import os
from getpass import getuser
import datetime
import time
from pathlib import Path
from shlex import quote

from portobello.rhobusta.options import ArgumentParserThatStoresArgv


def CreateJobName0():
    fnames = glob.glob('*.struct') + glob.glob('*.cif') \
             + glob.glob('POSCAR')
    if len(fnames) == 0:
        return "rJ-Unk"
    else:
        name = fnames[0]
        if name == 'POSCAR':
            return "rJ"+open('POSCAR').readline().split(' ')[0]
        else:
            return "rJ"+name.split(".")[0] 
        
        
def CreateJobName():
    return os.environ['PWD'].split("/")[-1]
        
def qsub(mod_name, opts, args):
    nworkers = opts.mpi_workers
    if opts.organizer_mpi_workers >= 0:
        nworkers = opts.organizer_mpi_workers
    if nworkers+1 > 36 or opts.big:
        one = ""
    else:
        one = "_one"
    if opts.queue == "amd":
        Q = "o64m4"
    elif opts.queue == "rutgers": 
        Q = "i16m4"
        if nworkers+1 > 16:
            Q ="i36m5"
        if nworkers+1>36 or opts.big:
            Q = "i16m6"
        opts.variant = "intel2-prod"
    else:
        Q = opts.queue
    if len(args):
        arglist = " ".join([quote(a) for a in args])
    else:
        arglist = ""


    p1 = Popen(["%s/select_production.py" % os.getenv("PORTOBELLO_BASE", None), ''], stdout=PIPE, stdin=PIPE, stderr=STDOUT)    
    p1_stdout = p1.communicate()[0]
    portobello_dest = p1_stdout.decode('utf-8')
    
    remove_big_files = ''
    if opts.mod in ('transport', 'fs', 'script', 'D', 'sf', 'dos', 'fe', 'maxent'):
        remove_big_files = 'rm flapw_image.h5 gutz.h5 dmft.h5\n '

    has_gpu = False
    for arg in args:
        if arg.find("simulations-per-GPU") >= 0:
            has_gpu =True
 
    protect = opts.protect.split(",")
    delete_unprotected = "ls *tmp*"
    for pp in protect:
        delete_unprotected += f"| grep -v {pp}"


    SCRIPT = f"""
#!/bin/bash
#set -x

###### SUN Grid Engine job wrapper
#$ -N {opts.job_name}
#$ -pe orte{one} {nworkers+1}
#$ -q {Q}
#$ -j y
#$ -M $USER
#$ -v LD_LIBRARY_PATH,PATH
{"#$ -l h=(b01|b02|b03|b04)" if has_gpu else ""}
# b09 has strange numerical issues, even in single machine mode (??)
#$ -l h=!b22
######

#other options can be added, like -l virtual_free=100G

######   DON'T remove the following line!
source $TMPDIR/sge_init.sh
######

export SMPD_OPTION_NO_DYNAMIC_HOSTS=1
export MODULEPATH=/opt/apps/modulefiles:/opt/intel/modulefiles:/opt/pgi/modulefiles:/opt/gnu/modulefiles:/opt/sw/modulefiles
export OMP_NUM_THREADS=1
export PATH=.:/home/${{USER}}/bin:${{PATH}}{":/opt/sw/ompi4/intel/18.0/bin/" if opts.variant == "intel2-prod" else ""}

cat $TMPDIR/machines  | uniq > machines

module purge
module load hdf5

######   PORTOBELLO stuff
export PORTOBELLO_BASE={portobello_dest}
export LD_LIBRARY_PATH=${{PORTOBELLO_BASE}}/build/{opts.variant}/:${{LD_LIBRARY_PATH}}:/opt/intel/compilers_and_libraries_2018.0.128/linux/mkl/lib/intel64/:/opt/intel/intelpython2/pkgs/mkl-2018.0.0-intel_3/lib/:/opt/intel/compilers_and_libraries_2018.0.128/linux/compiler/lib/intel64_lin/:/opt/intel/compilers_and_libraries_2018.0.128/linux/mkl/lib/intel64_lin/:/home/adler/libs/
export PYTHONPATH=${{PORTOBELLO_BASE}}
export FORTRAN_INTEL_LD_PATH=librhobusta.so:libCTQMC.so:libARPACK.so
# make sure mkl is used in numpy
#echo "LOCATION OF libiomp5.so"
#ls -l /opt/intel/compilers_and_libraries_2018.0.128/linux/compiler/lib/intel64_lin/libiomp5.so
#export LD_PRELOAD=/opt/intel/compilers_and_libraries_2018.0.128/linux/compiler/lib/intel64_lin/libiomp5.so:${{PORTOBELLO_BASE}}/build/{opts.variant}/librhobusta.so
export LD_PRELOAD=/opt/intel/compilers_and_libraries_2018.0.128/linux/mkl/lib/intel64_lin/libmkl_rt.so;/home/adler/libs/libcudart.so.10.1.105
#export LD_PRELOAD=/lib64/liblapack.so;/lib64/libblas.so
#export LD_PRELOAD=/opt/intel/compilers_and_libraries_2018.0.128/linux/mkl/lib/intel64_lin/libmkl_intel_lp64.so;/opt/intel/compilers_and_libraries_2018.0.128/linux/mkl/lib/intel64_lin/libmkl_intel_thread.so;/opt/intel/compilers_and_libraries_2018.0.128/linux/mkl/lib/intel64_lin/libmkl_core.so;/opt/intel/compilers_and_libraries_2018.0.128/linux/compiler/lib/intel64_lin/libiomp5.so

module load intel/ompi4
#export PYTHONPATH=${{PYTHONPATH}}:/home/adler/mpi4py/install/usr/local/lib64/python3.6/site-packages/

export OMPI_MCA_rmaps_base_oversubscribe=yes
export OMPI_MCA_btl=^ucx
export OMPI_MCA_pml=^ucx
export OMPI_MCA_osc=^ucx
export OMPI_MCA_rmaps_base_inherit=0
#export OMPI_MCA_plm_rsh_no_tree_spawn=true

for f in *.h5.gz; do
 [ -e "$f" ] && gunzip $f
done

cat README >> all.out
{f"cd {opts.root_dir}" if opts.root_dir != "" else ""}
python3 -m {mod_name} --mpi-workers={opts.mpi_workers} {arglist} >> all.out 2>&1
rm `{delete_unprotected}` */*tmp* debug.h5 core*

{remove_big_files}
"""

    memo = " notes:\n"
    if opts.memo:
        raw = input('Enter memo:')
        if raw != "":
            memo = " notes: "+ raw + "\n"
    
    log = f"""
Q {" ".join(filter(lambda x: x!='-m', sys.argv[1:]))}
{memo}"""
    
    rdme = open("README", "w")
    logfile = open("LOG", "a")
       
    if not opts.nogit:
        #run in the portobello dir: git diff HEAD > DIFF
        os.system("git -C %s diff HEAD > git_uncommited" % os.getenv("PORTOBELLO_BASE", None))
        os.system("git -C %s log -n1 > git_log" % os.getenv("PORTOBELLO_BASE", None))
     
    rdme.write(log)
    logfile.write(log)
    
    print(SCRIPT)
        
    p = Popen(['qsub', ''], stdout=PIPE, stdin=PIPE, stderr=STDOUT)    
    qsub_stdout = p.communicate(input=SCRIPT.encode('ascii'))[0]
    output = qsub_stdout.decode('utf-8')
    
    split_output = output.split()
    job_number = split_output[2]
    
    # create the link (override the old file if exists)
    new_dir = job_number + getuser() 
    # this removes an old link if one exists with the same name
    if os.path.lexists(opts.job_name):
        print("- removing link to re-create it", opts.job_name)
        os.system("rm "+opts.job_name)
    os.system("ln -s -f " + new_dir + " " + opts.job_name)
    
    job_name=output.split('\"')[1]
    
  
    #rdme.write(output)
    rdme.write(" job: %s\n"%job_name)
    rdme.write(" job_number: %s\n" % job_number)
    rdme.write(" parent dir: " + os.path.abspath(".") + "\n")
    rdme.write(" start time: " + str(datetime.datetime.now())+ "\n")
    rdme.close()
    
    logfile.write(" job: %s\n"%job_name)
    logfile.write(" job_number: %s\n" % job_number)
    logfile.write(" parent dir: " + os.path.abspath(".") + "\n")
    logfile.write(" start time: " + str(datetime.datetime.now())+ "\n")
    logfile.close()

    #job_number = int(output.split(" ")[2])
    #print "job number is", job_number

if __name__ == '__main__':
    
    parser = ArgumentParserThatStoresArgv()
        
    parser.add_argument("-j", "--job-name", dest="job_name",
                      help="The name for the submitted job (required)",
                      default="")
    parser.add_argument("-n", "--mpi-workers", dest="mpi_workers", default=0, type=int)
    parser.add_argument("--organizer-mpi-workers", dest="organizer_mpi_workers", default=-1, type=int)
    parser.add_argument("-m", "--mod",
                      action="store",
                      dest="mod",
                      choices=["dft+dmft", "dft+g", "dft+g2", "rhobusta", "ctqmc", "dmft", "test", "fs", "dfe", "fe", "dos", "zz",
                               "g", "g+dft", "transport", "sf", "proj", "D", "script", "contributor", "maxent", "arpes", "evc"],
                      default="rhobusta",
                      help="""Module to run""")
    
    parser.add_argument("-v", "--variant",
                      action="store",
                      dest="variant",
                      choices=["intel-prod", "intel-debug", "gcc-prod", "gcc-debug", "intel2-prod"],
                      default="intel-prod",
                      help="CMake variant of the make")

    parser.add_argument("--no-memo", dest="memo",
                      action="store_false",
                      default=True,
                      help="Don't ask for user memo from input") 
    
    parser.add_argument("--big", dest="big",
                      action="store_true",
                      default=False,
                      help="use a queue for big jobs") 
    
    parser.add_argument("-Q", "--queue", dest="queue",
                      type=str,
                      default="rutgers",
                      help="Queue name: rutgers,amd,bnl") 
                      
    parser.add_argument("--protect", dest="protect",
                      type=str,
                      default="",
                      help="tmp files to protect from deletion at the end - append the middle of the names") 
                      
    parser.add_argument("--nogit", dest="nogit",
                      action="store_true",
                      default=False,
                      help="do not snapshot git") 
    
    parser.add_argument("-u", "--using", dest="using",
                      type=str,
                      default=None,
                      help="directory of a contributor/organizer queue to use") 
    
    
    parser.add_argument("--with", dest="with_contributors", default=0, type=int,
                        help="specification number of contributors")
    
    parser.add_argument("-d", "--at", dest="root_dir", default=None,
                      type=str, help="dir to run in")
 
    (opts, args) = parser.parse_known_args(sys.argv[1:])
    
    
    opts.nogit = True # for now it is disabled.
    
    #
    # note: the only requirement for these classes is to have the parameter
    #       --mpi-slots, so that they can spawn their workers
    #
    mod = None

    if opts.mod == "rhobusta":
        mod = "portobello.rhobusta.rhobusta"
    elif opts.mod == "ctqmc":
        mod = "portobello.rhobusta.ctqmc"
    elif opts.mod == "dmft":
        mod = "portobello.rhobusta.dmft"
    elif opts.mod == "dft+dmft":
        mod = "portobello.rhobusta.dft__dmft"
    elif opts.mod == "dft+g":
        mod = "portobello.rhobusta.dft__g"
    elif opts.mod == "dft+g2":
        mod = "portobello.rhobusta.dft__g2"
    elif opts.mod == "g":
        mod = "portobello.rhobusta.gutzwiller"
    elif opts.mod == "fs":
        mod = "portobello.rhobusta.fermi_surface"
    elif opts.mod == "D":
        mod = "portobello.rhobusta.dashboard"
    elif opts.mod == "sf":
        mod = "portobello.rhobusta.bandstructure"
    elif opts.mod == "fe":
        mod = "portobello.rhobusta.energy"
    elif opts.mod == "dfe":
        mod = "portobello.rhobusta.dmft_energy"
    elif opts.mod == "dos":
        mod = "portobello.rhobusta.dos"
    elif opts.mod == "zz":
        mod = "portobello.rhobusta.zz"
    elif opts.mod == "transport":
        mod = "portobello.rhobusta.transport"
    elif opts.mod == "proj":
        mod = "portobello.rhobusta.ProjectorEmbedder" 
    elif opts.mod == "script":
        mod = "portobello.rhobusta.script"
    elif opts.mod == "contributor":
        mod = "portobello.jobs.Contributor"
    elif opts.mod == "maxent":
        mod = "portobello.rhobusta.maxent.Maxent"
    elif opts.mod == "arpes":
        mod = "portobello.rhobusta.arpes.simulation"
    elif opts.mod == "evc":
        mod = "portobello.rhobusta.energy_volume_curve"
    if opts.job_name == "":
        opts.job_name = CreateJobName()
        print("job name: ", opts.job_name)
    
    if opts.using is not None:
        args = ['-m', mod, '--using', opts.using] + args
        mod = "portobello.jobs.Organizer"
        
    qsub(mod, opts, args)

    saved_job_name = opts.job_name

    cpath = Path("./contribs/")
    
    if opts.with_contributors > 0:
        cpath.mkdir(exist_ok=True)
        try:
            os.chdir(str(cpath))
            for i in range(opts.with_contributors):
                print(f"Starting contributor {i+1}  in 10 sec. ... ")
                # TODO: make it possible to define different number of workers for the contributors.
                time.sleep(10) # wait for the system to copy all files correctly before we change them
                args = ['--for', saved_job_name]
                opts.job_name = "%s_%d" %(saved_job_name,i+1)
                opts.memo = False
                opts.with_contributors = 0
                qsub("portobello.jobs.Contributor", opts, args) # automatically links this contributor job to the organizer
        finally:
            os.chdir("..")
    
    
    
    
