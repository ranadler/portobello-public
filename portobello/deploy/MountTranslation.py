'''
Created on Dec 30, 2019

@author: adler
'''
import socket

# return the universal name where some directories are translate to mounts
def Translate(original) -> str:
    hostname = socket.gethostname()
    if hostname.endswith(".rutgers.edu") and (hostname.startswith('n') or hostname.startswith('b') or hostname.startswith('rupc')):
        short_name,*_rest = hostname.split(".")
        original= original.replace("/scratch", "/mnt/%s/"%short_name)
        original= original.replace("/home", "/mnt/home/")
    return original
    

if __name__ == '__main__':
    pass