'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class RhoInfo(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['location'] = AttributeTypes.STRING
        self.location = ""




class RhoInBands(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['min_band'] = AttributeTypes.INT
        self.min_band =  0

        self._attributes['max_band'] = AttributeTypes.INT
        self.max_band =  0

        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._datasets['rho'] = (np.complex128, '(int(self.max_band- (self.min_band) + 1), int(self.max_band- (self.min_band) + 1), int(self.num_k), int(self.num_si))' )
        self.rho : NDArray[(Any, Any, Any, Any), Complex128]
        self.rho = None
        self._datasets['Hqp'] = (np.complex128, '(int(self.max_band- (self.min_band) + 1), int(self.num_k), int(self.num_si))' )
        self.Hqp : NDArray[(Any, Any, Any), Complex128]
        self.Hqp = None
        self._datasets['Z'] = (np.complex128, '(int(self.max_band- (self.min_band) + 1), int(self.num_k), int(self.num_si))' )
        self.Z : NDArray[(Any, Any, Any), Complex128]
        self.Z = None



# this is the end of the generated file