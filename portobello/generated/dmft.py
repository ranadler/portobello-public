'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict

from portobello.generated.PEScf import *
from portobello.generated.self_energies import *

class DMFTFreeEnergy(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_temperatures'] = AttributeTypes.INT
        self.num_temperatures =  0

        self._datasets['temperatures'] = (np.float64, '(int(self.num_temperatures),)' )
        self.temperatures : NDArray[(Any,), Float64]
        self.temperatures = None
        self._datasets['internal_energies'] = (np.float64, '(int(self.num_temperatures),)' )
        self.internal_energies : NDArray[(Any,), Float64]
        self.internal_energies = None
        self._attributes['entropy'] = AttributeTypes.FLOAT
        self.entropy =  0.0

        self._attributes['result'] = AttributeTypes.FLOAT
        self.result =  0.0

        self._attributes['ready'] = AttributeTypes.BOOLEAN
        self.ready = False




class DMFTState(SolverState):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        SolverState.__InitMetadata__(self, True)

        self._objects['sig'] = ('self_energies', 'LocalOmegaFunction')
        self.sig = LocalOmegaFunction()
        self._objects['hyb'] = ('self_energies', 'LocalOmegaFunction')
        self.hyb = LocalOmegaFunction()
        self._objects['GImp'] = ('self_energies', 'LocalOmegaFunction')
        self.GImp = LocalOmegaFunction()
        self._attributes['num_freqs'] = AttributeTypes.INT
        self.num_freqs =  0

        self._attributes['sign'] = AttributeTypes.FLOAT
        self.sign =  0.0

        self._attributes['measurement_time'] = AttributeTypes.FLOAT
        self.measurement_time =  0.0




class ImpurityProblem(SolverProblem):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        SolverProblem.__InitMetadata__(self, True)

        self._attributes['os_dir'] = AttributeTypes.STRING
        self.os_dir = "./Impurity"

        self._attributes['num_freqs'] = AttributeTypes.INT
        self.num_freqs =  0

        self._attributes['hamiltonian_approximation'] = AttributeTypes.STRING
        self.hamiltonian_approximation = "ising"

        self._attributes['max_sampling_energy'] = AttributeTypes.FLOAT
        self.max_sampling_energy = 10.000000
        self._attributes['two_body_size'] = AttributeTypes.INT
        self.two_body_size =  0

        self._datasets['two_body'] = (np.complex128, '(int(self.two_body_size),)' )
        self.two_body : NDArray[(Any,), Complex128]
        self.two_body = None
        self._objects['hyb'] = ('self_energies', 'LocalOmegaFunction')
        self.hyb = LocalOmegaFunction()



class ImpurityMeasurement(SolverSolution):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        SolverSolution.__InitMetadata__(self, True)

        self._attributes['num_freqs'] = AttributeTypes.INT
        self.num_freqs =  0

        self._attributes['dim'] = AttributeTypes.INT
        self.dim =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._attributes['num_orbs'] = AttributeTypes.INT
        self.num_orbs =  0

        self._attributes['num_x'] = AttributeTypes.INT
        self.num_x =  20
        self._datasets['RhoImp'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.RhoImp : NDArray[(Any, Any, Any), Complex128]
        self.RhoImp = None
        self._objects['GImp'] = ('self_energies', 'LocalOmegaFunction')
        self.GImp = LocalOmegaFunction()
        self._objects['Sigma'] = ('self_energies', 'LocalOmegaFunction')
        self.Sigma = LocalOmegaFunction()
        self._objects['OccSusc'] = ('self_energies', 'LocalOmegaFunction')
        self.OccSusc = LocalOmegaFunction()
        self._datasets['CoVariance'] = (np.complex128, '(int(self.num_x), int(self.num_x), int(self.num_orbs), int(self.num_si))' )
        self.CoVariance : NDArray[(Any, Any, Any, Any), Complex128]
        self.CoVariance = None
        self._objects['GAux'] = ('self_energies', 'LocalOmegaFunction')
        self.GAux = LocalOmegaFunction()
        self._attributes['sign'] = AttributeTypes.FLOAT
        self.sign =  0.0

        self._attributes['lnZ'] = AttributeTypes.FLOAT
        self.lnZ =  0.0

        self._attributes['k'] = AttributeTypes.FLOAT
        self.k =  0.0

        self._attributes['num_samples'] = AttributeTypes.INT
        self.num_samples =  0

        self._datasets['expansion_histogram'] = (np.float64, '(int(self.num_samples),)' )
        self.expansion_histogram : NDArray[(Any,), Float64]
        self.expansion_histogram = None
        self._attributes['relative_error'] = AttributeTypes.FLOAT
        self.relative_error =  0.0




# this is the end of the generated file