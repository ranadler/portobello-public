'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class EmptyObject(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)




class ComputationInput(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['date_computed'] = AttributeTypes.DATE
        self.date_computed = None



class ComputationOutput(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['binary_name'] = AttributeTypes.STRING
        self.binary_name = ""

        self._attributes['binary_release'] = AttributeTypes.STRING
        self.binary_release = ""

        self._attributes['date_computed'] = AttributeTypes.DATE
        self.date_computed = None
        self._attributes['researcher'] = AttributeTypes.STRING
        self.researcher = ""

        self._attributes['description'] = AttributeTypes.STRING
        self.description = ""




class LDADescriptor(ComputationOutput):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ComputationOutput.__InitMetadata__(self, True)

        self._attributes['method'] = AttributeTypes.STRING
        self.method = ""

        self._attributes['location'] = AttributeTypes.STRING
        self.location = ""

        self._attributes['seed_name'] = AttributeTypes.STRING
        self.seed_name = ""




class Window(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['low'] = AttributeTypes.FLOAT
        self.low =  0.0

        self._attributes['high'] = AttributeTypes.FLOAT
        self.high =  0.0




# this is the end of the generated file