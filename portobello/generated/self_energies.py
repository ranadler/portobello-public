'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class LocalOmegaFunction(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['isReal'] = AttributeTypes.BOOLEAN
        self.isReal = False

        self._attributes['num_omega'] = AttributeTypes.INT
        self.num_omega =  0

        self._attributes['num_orbs'] = AttributeTypes.INT
        self.num_orbs =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._datasets['omega'] = (np.float64, '(int(self.num_omega),)' )
        self.omega : NDArray[(Any,), Float64]
        self.omega = None
        self._datasets['M'] = (np.complex128, '(int(self.num_omega), int(self.num_orbs), int(self.num_orbs), int(self.num_si))' )
        self.M : NDArray[(Any, Any, Any, Any), Complex128]
        self.M = None
        self._datasets['moments'] = (np.float64, '(int(3), int(self.num_orbs), int(self.num_orbs), int(self.num_si))' )
        self.moments : NDArray[(Any, Any, Any, Any), Float64]
        self.moments = None



class OmegaMesh(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['n_omega'] = AttributeTypes.INT
        self.n_omega =  0

        self._datasets['omega'] = (np.float64, '(int(self.n_omega- (0) + 1),)' )
        self.omega : NDArray[(Any,), Float64]
        self.omega = None
        self._datasets['index'] = (np.int32, '(int(self.n_omega- (0) + 1),)' )
        self.index : NDArray[(Any,), Int32]
        self.index = None


    @classmethod
    def GetOmegaMesh(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("self_energies","DFTPlusSigma", "GetOmegaMesh",
            selfpath)

class LocalOmegaFunctionInfo(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['location'] = AttributeTypes.STRING
        self.location = ""




class OmegaFunctionInBands(OmegaMesh):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        OmegaMesh.__InitMetadata__(self, True)

        self._attributes['isReal'] = AttributeTypes.BOOLEAN
        self.isReal = False

        self._attributes['min_band'] = AttributeTypes.INT
        self.min_band =  0

        self._attributes['max_band'] = AttributeTypes.INT
        self.max_band =  0

        self._attributes['num_k_irr'] = AttributeTypes.INT
        self.num_k_irr =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._datasets['M'] = (np.complex128, '(int(self.max_band - self.min_band + 1), int(self.max_band - self.min_band + 1), int(self.n_omega- (0) + 1), int(self.num_k_irr), int(self.num_si))' )
        self.M : NDArray[(Any, Any, Any, Any, Any), Complex128]
        self.M = None
        self._datasets['Minf'] = (np.complex128, '(int(self.max_band - self.min_band + 1), int(self.max_band - self.min_band + 1), int(self.num_k_irr), int(self.num_si))' )
        self.Minf : NDArray[(Any, Any, Any, Any), Complex128]
        self.Minf = None



# this is the end of the generated file