'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class FreeEnergy(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['min_band'] = AttributeTypes.INT
        self.min_band =  0

        self._attributes['max_band'] = AttributeTypes.INT
        self.max_band =  0

        self._attributes['num_k_irr'] = AttributeTypes.INT
        self.num_k_irr =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._attributes['num_reps'] = AttributeTypes.INT
        self.num_reps =  1
        self._attributes['num_atoms'] = AttributeTypes.INT
        self.num_atoms =  1
        self._datasets['Hqp'] = (np.float64, '(int(self.max_band- (self.min_band) + 1), int(self.num_k_irr), int(self.num_si))' )
        self.Hqp : NDArray[(Any, Any, Any), Float64]
        self.Hqp = None
        self._datasets['weight'] = (np.float64, '(int(self.num_reps), int(self.num_k_irr))' )
        self.weight : NDArray[(Any, Any), Float64]
        self.weight = None
        self._attributes['corr_energy_term'] = AttributeTypes.FLOAT
        self.corr_energy_term = 0.000000
        self._attributes['Fx'] = AttributeTypes.FLOAT
        self.Fx = 0.000000
        self._attributes['Fc'] = AttributeTypes.FLOAT
        self.Fc = 0.000000
        self._attributes['z_vnucl'] = AttributeTypes.FLOAT
        self.z_vnucl = 0.000000
        self._attributes['ro_vh_new'] = AttributeTypes.FLOAT
        self.ro_vh_new = 0.000000
        self._attributes['exch_dft'] = AttributeTypes.FLOAT
        self.exch_dft = 0.000000
        self._attributes['e_coul'] = AttributeTypes.FLOAT
        self.e_coul = 0.000000
        self._attributes['tr_vxc_valence'] = AttributeTypes.FLOAT
        self.tr_vxc_valence = 0.000000
        self._attributes['tr_vh_valence'] = AttributeTypes.FLOAT
        self.tr_vh_valence = 0.000000
        self._attributes['total_energy_core'] = AttributeTypes.FLOAT
        self.total_energy_core = 0.000000
        self._attributes['e_h_core'] = AttributeTypes.FLOAT
        self.e_h_core = 0.000000
        self._attributes['e_xc_core'] = AttributeTypes.FLOAT
        self.e_xc_core = 0.000000
        self._attributes['core_energy'] = AttributeTypes.FLOAT
        self.core_energy = 0.000000
        self._attributes['muN'] = AttributeTypes.FLOAT
        self.muN = 0.000000
        self._attributes['correlated_term_ry'] = AttributeTypes.FLOAT
        self.correlated_term_ry = 0.000000
        self._attributes['trLogHloc'] = AttributeTypes.FLOAT
        self.trLogHloc = 0.000000
        self._attributes['trLogHlocCorr'] = AttributeTypes.FLOAT
        self.trLogHlocCorr = 0.000000
        self._attributes['muRy'] = AttributeTypes.FLOAT
        self.muRy =  0.0

        self._attributes['N'] = AttributeTypes.FLOAT
        self.N =  0.0

        self._attributes['Nqp'] = AttributeTypes.FLOAT
        self.Nqp =  0.0

        self._attributes['result'] = AttributeTypes.FLOAT
        self.result = 0.000000


    @classmethod
    def CalculateFreeEnergyDFT(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("energy","flapw_driver", "CalculateFreeEnergyDFT",
            selfpath)
    @classmethod
    def CalculateFreeEnergyGutz(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("energy","flapw_driver", "CalculateFreeEnergyGutz",
            selfpath)
    @classmethod
    def CalculateFreeEnergyDMFT(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("energy","flapw_driver", "CalculateFreeEnergyDMFT",
            selfpath)

# this is the end of the generated file