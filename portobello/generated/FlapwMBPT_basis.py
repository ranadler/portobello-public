'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class MuffinTin(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_muffin_orbs'] = AttributeTypes.INT
        self.num_muffin_orbs =  0

        self._attributes['num_radial_functions'] = AttributeTypes.INT
        self.num_radial_functions =  0

        self._attributes['nrad'] = AttributeTypes.INT
        self.nrad =  0

        self._datasets['lapw2self'] = (np.int32, '(int(self.num_muffin_orbs),)' )
        self.lapw2self : NDArray[(Any,), Int32]
        self.lapw2self = None
        self._datasets['radial_functions'] = (np.float64, '(int(self.num_radial_functions), int(self.nrad))' )
        self.radial_functions : NDArray[(Any, Any), Float64]
        self.radial_functions = None
        self._datasets['radial_functions2'] = (np.float64, '(int(self.num_radial_functions), int(self.nrad))' )
        self.radial_functions2 : NDArray[(Any, Any), Float64]
        self.radial_functions2 = None
        self._datasets['radial_mesh'] = (np.float64, '(int(self.nrad),)' )
        self.radial_mesh : NDArray[(Any,), Float64]
        self.radial_mesh = None
        self._datasets['dr_mesh'] = (np.float64, '(int(self.nrad),)' )
        self.dr_mesh : NDArray[(Any,), Float64]
        self.dr_mesh = None



class MuffinTinBasis(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['nrel'] = AttributeTypes.INT
        self.nrel =  0

        self._attributes['nsort'] = AttributeTypes.INT
        self.nsort =  0

        self._attributes['natom'] = AttributeTypes.INT
        self.natom =  0

        self._attributes['nops'] = AttributeTypes.INT
        self.nops =  0

        self._datasets['atom_to_equiv_under_op'] = (np.int32, '(int(self.natom), int(self.nops))' )
        self.atom_to_equiv_under_op : NDArray[(Any, Any), Int32]
        self.atom_to_equiv_under_op = None
        self._objars['tins'] = ( ('FlapwMBPT_basis', 'MuffinTin'), '(int(self.nsort),)') 
        self.tins : NDarray[(Any,), FlapwMBPT_basis.MuffinTin]
        self.tins = None


    @classmethod
    def GetMtBasis(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_basis","flapw_basis", "GetMtBasis",
            selfpath)

class InterstitialBasis(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['max_num_pw'] = AttributeTypes.INT
        self.max_num_pw =  0

        self._attributes['max_g_basis'] = AttributeTypes.INT
        self.max_g_basis =  0

        self._attributes['nrel'] = AttributeTypes.INT
        self.nrel =  0

        self._attributes['num_expanded'] = AttributeTypes.INT
        self.num_expanded =  0

        self._attributes['num_k_all'] = AttributeTypes.INT
        self.num_k_all =  0

        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._datasets['reciprical_lattice_vectors'] = (np.float64, '(int(3), int(self.max_g_basis))' )
        self.reciprical_lattice_vectors : NDArray[(Any, Any), Float64]
        self.reciprical_lattice_vectors = None
        self._datasets['vector_index'] = (np.int32, '(int(self.max_num_pw), int(self.num_k_all))' )
        self.vector_index : NDArray[(Any, Any), Int32]
        self.vector_index = None
        self._datasets['num_vectors_at_k'] = (np.int32, '(int(self.num_k),)' )
        self.num_vectors_at_k : NDArray[(Any,), Int32]
        self.num_vectors_at_k = None
        self._datasets['A'] = (np.complex128, '(int(self.max_num_pw * self.nrel), int(self.num_expanded), int(self.num_k), int(self.num_si))' )
        self.A : NDArray[(Any, Any, Any, Any), Complex128]
        self.A = None


    @classmethod
    def GetInterstitialBasis(cls, selfpath, min_band, max_band):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_basis","flapw_basis", "GetInterstitialBasis",
            selfpath, c_int(min_band), c_int(max_band))

# this is the end of the generated file