'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict

from portobello.generated.FlapwMBPT_interface import *
from portobello.generated.atomic_orbitals import *
from portobello.generated.base_types import *

class Histogram(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_configs'] = AttributeTypes.INT
        self.num_configs =  0

        self._datasets['p'] = (np.float64, '(int(self.num_configs),)' )
        self.p : NDArray[(Any,), Float64]
        self.p = None
        self._datasets['N'] = (np.int32, '(int(self.num_configs),)' )
        self.N : NDArray[(Any,), Int32]
        self.N = None
        self._datasets['energy'] = (np.float64, '(int(self.num_configs),)' )
        self.energy : NDArray[(Any,), Float64]
        self.energy = None
        self._datasets['Jz'] = (np.float64, '(int(self.num_configs),)' )
        self.Jz : NDArray[(Any,), Float64]
        self.Jz = None



class SolverState(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['super_iter'] = AttributeTypes.INT
        self.super_iter =  1
        self._attributes['sub_iter'] = AttributeTypes.INT
        self.sub_iter =  0
        self._attributes['num_equivalents'] = AttributeTypes.INT
        self.num_equivalents =  0

        self._attributes['num_anti_equivalents'] = AttributeTypes.INT
        self.num_anti_equivalents =  0
        self._attributes['dim'] = AttributeTypes.INT
        self.dim =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._attributes['nrel'] = AttributeTypes.INT
        self.nrel =  1
        self._attributes['double_counting_method'] = AttributeTypes.STRING
        self.double_counting_method = ""

        self._attributes['nominal_dc'] = AttributeTypes.FLOAT
        self.nominal_dc =  0.0

        self._datasets['DC'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.DC : NDArray[(Any, Any, Any), Complex128]
        self.DC = None
        self._attributes['initial_spin_polarization'] = AttributeTypes.FLOAT
        self.initial_spin_polarization =  0.0

        self._attributes['state'] = AttributeTypes.STRING
        self.state = "start"

        self._attributes['mu'] = AttributeTypes.FLOAT
        self.mu =  0.0

        self._attributes['U'] = AttributeTypes.FLOAT
        self.U =  0.0

        self._attributes['J'] = AttributeTypes.FLOAT
        self.J =  0.0

        self._attributes['beta'] = AttributeTypes.FLOAT
        self.beta =  0.0

        self._attributes['Nimp'] = AttributeTypes.FLOAT
        self.Nimp =  0.0

        self._attributes['Nlatt'] = AttributeTypes.FLOAT
        self.Nlatt =  0.0

        self._attributes['ising'] = AttributeTypes.BOOLEAN
        self.ising = False

        self._attributes['kanamori'] = AttributeTypes.BOOLEAN
        self.kanamori = False

        self._datasets['rep'] = (np.int32, '(int(self.dim), int(self.dim))' )
        self.rep : NDArray[(Any, Any), Int32]
        self.rep = None
        self._attributes['num_unique_orbs'] = AttributeTypes.INT
        self.num_unique_orbs =  0

        self._objects['proj_energy_window'] = ('base_types', 'Window')
        self.proj_energy_window = Window()
        self._attributes['window_min_band'] = AttributeTypes.INT
        self.window_min_band =  1
        self._attributes['window_max_band'] = AttributeTypes.INT
        self.window_max_band =  1
        self._attributes['energyImp'] = AttributeTypes.FLOAT
        self.energyImp =  0.0

        self._attributes['NN'] = AttributeTypes.FLOAT
        self.NN =  0.0

        self._datasets['RhoImp'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.RhoImp : NDArray[(Any, Any, Any), Complex128]
        self.RhoImp = None
        self._attributes['energyBands'] = AttributeTypes.FLOAT
        self.energyBands =  0.0

        self._objects['hist'] = ('PEScf', 'Histogram')
        self.hist = Histogram()
        self._objects['cvg'] = ('FlapwMBPT_interface', 'Convergence')
        self.cvg = Convergence()
        self._attributes['quality'] = AttributeTypes.FLOAT
        self.quality = 0.000000
        self._objects['NRange'] = ('base_types', 'Window')
        self.NRange = Window()



class SolverProblem(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['self_path'] = AttributeTypes.STRING
        self.self_path = ""

        self._attributes['isReal'] = AttributeTypes.BOOLEAN
        self.isReal = False

        self._attributes['beta'] = AttributeTypes.FLOAT
        self.beta =  0.0

        self._attributes['mu'] = AttributeTypes.FLOAT
        self.mu =  0.0

        self._attributes['U'] = AttributeTypes.FLOAT
        self.U =  0.0

        self._attributes['J'] = AttributeTypes.FLOAT
        self.J =  0.0

        self._attributes['Uprime'] = AttributeTypes.FLOAT
        self.Uprime =  0.0

        self._attributes['F0'] = AttributeTypes.FLOAT
        self.F0 =  0.0

        self._attributes['F2'] = AttributeTypes.FLOAT
        self.F2 =  0.0

        self._attributes['F4'] = AttributeTypes.FLOAT
        self.F4 =  0.0

        self._attributes['dim'] = AttributeTypes.INT
        self.dim =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._attributes['nrel'] = AttributeTypes.INT
        self.nrel =  0

        self._datasets['E'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.E : NDArray[(Any, Any, Any), Complex128]
        self.E = None
        self._objects['subshell'] = ('atomic_orbitals', 'AtomicShell')
        self.subshell = AtomicShell()
        self._attributes['correlated_orbitals_id'] = AttributeTypes.INT
        self.correlated_orbitals_id =  0




class SolverSolution(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['E'] = AttributeTypes.FLOAT
        self.E =  0.0

        self._attributes['N'] = AttributeTypes.FLOAT
        self.N =  0.0

        self._attributes['NN'] = AttributeTypes.FLOAT
        self.NN =  0.0

        self._objects['hist'] = ('PEScf', 'Histogram')
        self.hist = Histogram()
        self._attributes['quality'] = AttributeTypes.FLOAT
        self.quality = 0.000000



# this is the end of the generated file