'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class Velocities(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_expanded'] = AttributeTypes.INT
        self.num_expanded =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0

        self._datasets['data'] = (np.complex128, '(int(self.num_k), int(self.num_si), int(self.num_expanded), int(self.num_expanded), int(3))' )
        self.data : NDArray[(Any, Any, Any, Any, Any), Complex128]
        self.data = None



class Input(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['Temperature'] = AttributeTypes.FLOAT
        self.Temperature = 1.000000
        self._attributes['num_omega'] = AttributeTypes.INT
        self.num_omega =  1
        self._attributes['symmetrize_bz'] = AttributeTypes.BOOLEAN
        self.symmetrize_bz = False

        self._attributes['gamm'] = AttributeTypes.FLOAT
        self.gamm = 0.000500
        self._attributes['gammc'] = AttributeTypes.FLOAT
        self.gammc = 0.000500
        self._attributes['delta'] = AttributeTypes.FLOAT
        self.delta = 0.001000
        self._attributes['omega_max'] = AttributeTypes.FLOAT
        self.omega_max = 0.150000
        self._attributes['max_omega_mesh'] = AttributeTypes.INT
        self.max_omega_mesh =  0
        self._attributes['active_omega_points'] = AttributeTypes.INT
        self.active_omega_points =  1
        self._attributes['min_band'] = AttributeTypes.INT
        self.min_band =  1
        self._attributes['max_band'] = AttributeTypes.INT
        self.max_band =  1
        self._attributes['momentum_kind'] = AttributeTypes.STRING
        self.momentum_kind = "gradient"

        self._attributes['test_velocity'] = AttributeTypes.BOOLEAN
        self.test_velocity = False

        self._attributes['num_dirs'] = AttributeTypes.INT
        self.num_dirs =  3
        self._datasets['dirs'] = (np.float64, '(int(3), int(3), int(self.num_dirs))' )
        self.dirs : NDArray[(Any, Any, Any), Float64]
        self.dirs = None


    @classmethod
    def ComputeFromPortobello(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("TransportOptics","PortobelloProvider", "ComputeFromPortobello",
            selfpath)

class GreensFunction(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['max_omega'] = AttributeTypes.INT
        self.max_omega =  0

        self._attributes['num_basis'] = AttributeTypes.INT
        self.num_basis =  0

        self._attributes['num_correlated'] = AttributeTypes.INT
        self.num_correlated =  0

        self._datasets['w'] = (np.float64, '(int(self.max_omega- (-self.max_omega) + 1),)' )
        self.w : NDArray[(Any,), Float64]
        self.w = None
        self._datasets['a'] = (np.float64, '(int(self.max_omega- (-self.max_omega) + 1),)' )
        self.a : NDArray[(Any,), Float64]
        self.a = None
        self._datasets['b'] = (np.float64, '(int(self.max_omega- (-self.max_omega) + 1),)' )
        self.b : NDArray[(Any,), Float64]
        self.b = None
        self._datasets['HPlus'] = (np.complex128, '(int(self.num_basis), int(self.max_omega- (-self.max_omega) + 1))' )
        self.HPlus : NDArray[(Any, Any), Complex128]
        self.HPlus = None
        self._datasets['L'] = (np.complex128, '(int(self.num_correlated), int(self.num_correlated), int(self.max_omega- (-self.max_omega) + 1))' )
        self.L : NDArray[(Any, Any, Any), Complex128]
        self.L = None
        self._datasets['R'] = (np.complex128, '(int(self.num_correlated), int(self.num_correlated), int(self.max_omega- (-self.max_omega) + 1))' )
        self.R : NDArray[(Any, Any, Any), Complex128]
        self.R = None



class WorkArea(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_dirs'] = AttributeTypes.INT
        self.num_dirs =  3
        self._attributes['maxpower'] = AttributeTypes.INT
        self.maxpower =  2
        self._attributes['num_omega'] = AttributeTypes.INT
        self.num_omega =  0

        self._datasets['total'] = (np.float64, '(int(self.num_dirs), int(self.maxpower- (0) + 1), int(self.num_omega- (0) + 1))' )
        self.total : NDArray[(Any, Any, Any), Float64]
        self.total = None
        self._attributes['total_weight'] = AttributeTypes.FLOAT
        self.total_weight =  0.0




class IntegrandForDebug(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['max_omega'] = AttributeTypes.INT
        self.max_omega =  0

        self._datasets['omega'] = (np.float64, '(int(self.max_omega- (-self.max_omega) + 1),)' )
        self.omega : NDArray[(Any,), Float64]
        self.omega = None
        self._datasets['G'] = (np.complex128, '(int(self.max_omega- (-self.max_omega) + 1),)' )
        self.G : NDArray[(Any,), Complex128]
        self.G = None



class EmbeddedSelfEnergyOnRealOmega(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['min_band'] = AttributeTypes.INT
        self.min_band =  0

        self._attributes['max_band'] = AttributeTypes.INT
        self.max_band =  0

        self._attributes['shard_start'] = AttributeTypes.INT
        self.shard_start =  0

        self._attributes['shard_end'] = AttributeTypes.INT
        self.shard_end =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._attributes['max_omega'] = AttributeTypes.INT
        self.max_omega =  0

        self._datasets['omegaRy'] = (np.float64, '(int(self.max_omega- (-self.max_omega) + 1),)' )
        self.omegaRy : NDArray[(Any,), Float64]
        self.omegaRy = None
        self._datasets['actual_k'] = (np.int32, '(int(self.shard_end- (self.shard_start) + 1),)' )
        self.actual_k : NDArray[(Any,), Int32]
        self.actual_k = None
        self._datasets['M'] = (np.complex128, '(int(self.max_band - self.min_band + 1), int(self.max_band - self.min_band + 1), int(self.max_omega- (-self.max_omega) + 1), int(self.shard_end- (self.shard_start) + 1), int(self.num_si))' )
        self.M : NDArray[(Any, Any, Any, Any, Any), Complex128]
        self.M = None



class Output(Input):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Input.__InitMetadata__(self, True)

        self._attributes['maxpower'] = AttributeTypes.INT
        self.maxpower =  2
        self._datasets['omega'] = (np.float64, '(int(self.num_omega- (0) + 1),)' )
        self.omega : NDArray[(Any,), Float64]
        self.omega = None
        self._datasets['A'] = (np.float64, '(int(self.num_omega- (0) + 1), int(self.num_dirs), int(self.maxpower- (0) + 1))' )
        self.A : NDArray[(Any, Any, Any), Float64]
        self.A = None



# this is the end of the generated file