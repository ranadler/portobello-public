'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class PWOverlap(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_pw'] = AttributeTypes.INT
        self.num_pw =  0

        self._datasets['overlap'] = (np.complex128, '(int(self.num_pw), int(self.num_pw))' )
        self.overlap : NDArray[(Any, Any), Complex128]
        self.overlap = None



class Basis(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_muffin_orbs'] = AttributeTypes.INT
        self.num_muffin_orbs =  0

        self._attributes['num_k_all'] = AttributeTypes.INT
        self.num_k_all =  0

        self._attributes['num_k_irr'] = AttributeTypes.INT
        self.num_k_irr =  0

        self._attributes['num_k_shard'] = AttributeTypes.INT
        self.num_k_shard =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  1
        self._attributes['nrel'] = AttributeTypes.INT
        self.nrel =  1
        self._objars['pw_overlap'] = ( ('LAPW', 'PWOverlap'), '(int(self.num_k_irr),)') 
        self.pw_overlap : NDarray[(Any,), LAPW.PWOverlap]
        self.pw_overlap = None
        self._datasets['atom_number'] = (np.int32, '(int(self.num_muffin_orbs),)' )
        self.atom_number : NDArray[(Any,), Int32]
        self.atom_number = None
        self._datasets['n'] = (np.int32, '(int(self.num_muffin_orbs),)' )
        self.n : NDArray[(Any,), Int32]
        self.n = None
        self._datasets['l'] = (np.int32, '(int(self.num_muffin_orbs),)' )
        self.l : NDArray[(Any,), Int32]
        self.l = None
        self._datasets['m'] = (np.int32, '(int(self.num_muffin_orbs),)' )
        self.m : NDArray[(Any,), Int32]
        self.m = None
        self._datasets['correlated'] = (np.int32, '(int(self.num_muffin_orbs),)' )
        self.correlated : NDArray[(Any,), Int32]
        self.correlated = None
        self._datasets['kind'] = (np.int32, '(int(self.num_muffin_orbs),)' )
        self.kind : NDArray[(Any,), Int32]
        self.kind = None
        self._datasets['mo_overlap'] = (np.float64, '(int(self.num_muffin_orbs), int(self.num_muffin_orbs))' )
        self.mo_overlap : NDArray[(Any, Any), Float64]
        self.mo_overlap = None
        self._datasets['weight'] = (np.float64, '(int(self.num_k_shard),)' )
        self.weight : NDArray[(Any,), Float64]
        self.weight = None
        self._datasets['KPoints'] = (np.float64, '(int(self.num_k_all), int(3))' )
        self.KPoints : NDArray[(Any, Any), Float64]
        self.KPoints = None



class MTAtomInfo(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_orbitals'] = AttributeTypes.INT
        self.num_orbitals =  0
        self._datasets['l'] = (np.float64, '(int(self.num_orbitals),)' )
        self.l : NDArray[(Any,), Float64]
        self.l = None
        self._datasets['energy'] = (np.float64, '(int(self.num_orbitals),)' )
        self.energy : NDArray[(Any,), Float64]
        self.energy = None
        self._datasets['core_overlap'] = (np.float64, '(int(self.num_orbitals),)' )
        self.core_overlap : NDArray[(Any,), Float64]
        self.core_overlap = None



class BasisMTInfo(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_unique_atoms'] = AttributeTypes.INT
        self.num_unique_atoms =  0

        self._objars['atoms'] = ( ('LAPW', 'MTAtomInfo'), '(int(self.num_unique_atoms),)') 
        self.atoms : NDarray[(Any,), LAPW.MTAtomInfo]
        self.atoms = None



class Expansion(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['is_sharded'] = AttributeTypes.BOOLEAN
        self.is_sharded = False

        self._attributes['shard_offset'] = AttributeTypes.INT
        self.shard_offset =  0
        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0

        self._attributes['num_expanded'] = AttributeTypes.INT
        self.num_expanded =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  1
        self._attributes['max_num_pw'] = AttributeTypes.INT
        self.max_num_pw =  0

        self._attributes['num_muffin_orbs'] = AttributeTypes.INT
        self.num_muffin_orbs =  0

        self._datasets['Z'] = (np.complex128, '(int(self.num_muffin_orbs), int(self.num_expanded), int(self.num_k), int(self.num_si))' )
        self.Z : NDArray[(Any, Any, Any, Any), Complex128]
        self.Z = None



class KPath(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0

        self._datasets['k'] = (np.float64, '(int(3), int(self.num_k))' )
        self.k : NDArray[(Any, Any), Float64]
        self.k = None



class BandStructure(Expansion):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Expansion.__InitMetadata__(self, True)

        self._datasets['num_bands'] = (np.int32, '(int(self.num_k), int(self.num_si))' )
        self.num_bands : NDArray[(Any, Any), Int32]
        self.num_bands = None
        self._datasets['energy'] = (np.float64, '(int(self.num_expanded), int(self.num_k), int(self.num_si))' )
        self.energy : NDArray[(Any, Any, Any), Float64]
        self.energy = None
        self._datasets['Occ'] = (np.complex128, '(int(self.num_expanded), int(self.num_expanded), int(self.num_k), int(self.num_si))' )
        self.Occ : NDArray[(Any, Any, Any, Any), Complex128]
        self.Occ = None
        self._attributes['chemical_potential'] = AttributeTypes.FLOAT
        self.chemical_potential =  0.0




class EquivalentAtomSymmetries(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_atoms'] = AttributeTypes.INT
        self.num_atoms =  0

        self._datasets['g'] = (np.int32, '(int(self.num_atoms),)' )
        self.g : NDArray[(Any,), Int32]
        self.g = None



# this is the end of the generated file