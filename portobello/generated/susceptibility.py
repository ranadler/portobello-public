'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class LatticeFunction(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_omega'] = AttributeTypes.INT
        self.num_omega =  0

        self._attributes['num_nu'] = AttributeTypes.INT
        self.num_nu =  0

        self._attributes['num_bands'] = AttributeTypes.INT
        self.num_bands =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0

        self._attributes['num_q'] = AttributeTypes.INT
        self.num_q =  0

        self._datasets['omega'] = (np.float64, '(int(self.num_omega),)' )
        self.omega : NDArray[(Any,), Float64]
        self.omega = None
        self._datasets['nu'] = (np.float64, '(int(self.num_nu),)' )
        self.nu : NDArray[(Any,), Float64]
        self.nu = None



class LatticeGreensFunctions(LatticeFunction):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        LatticeFunction.__InitMetadata__(self, True)

        self._datasets['M'] = (np.complex128, '(int(self.num_k), int(self.num_si), int(self.num_omega), int(self.num_bands), int(self.num_bands))' )
        self.M : NDArray[(Any, Any, Any, Any, Any), Complex128]
        self.M = None



class LatticeBubble_at_qnu(LatticeFunction):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        LatticeFunction.__InitMetadata__(self, True)

        self._datasets['M'] = (np.complex128, '(int(self.num_k), int(self.num_omega), int(self.num_bands), int(self.num_bands), int(self.num_bands), int(self.num_bands))' )
        self.M : NDArray[(Any, Any, Any, Any, Any, Any), Complex128]
        self.M = None



class IrreducibleVertex(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_omega'] = AttributeTypes.INT
        self.num_omega =  0

        self._attributes['num_bands'] = AttributeTypes.INT
        self.num_bands =  0

        self._attributes['num_kind'] = AttributeTypes.INT
        self.num_kind =  0

        self._datasets['M'] = (np.complex128, '(int(self.num_omega), int(self.num_omega), int(self.num_kind), int(self.num_bands), int(self.num_bands))' )
        self.M : NDArray[(Any, Any, Any, Any, Any), Complex128]
        self.M = None



class LatticeSusceptibility(LatticeFunction):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        LatticeFunction.__InitMetadata__(self, True)

        self._attributes['num_kind'] = AttributeTypes.INT
        self.num_kind =  0

        self._datasets['M'] = (np.complex128, '(int(self.num_q), int(self.num_nu), int(self.num_k), int(self.num_kind), int(self.num_bands), int(self.num_bands))' )
        self.M : NDArray[(Any, Any, Any, Any, Any, Any), Complex128]
        self.M = None



class ContinuedLatticeSusceptibility(LatticeFunction):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        LatticeFunction.__InitMetadata__(self, True)

        self._attributes['num_kind'] = AttributeTypes.INT
        self.num_kind =  0

        self._datasets['M'] = (np.complex128, '(int(self.num_q), int(self.num_nu))' )
        self.M : NDArray[(Any, Any), Complex128]
        self.M = None



class SusceptibilityCalculation(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['next_todo'] = AttributeTypes.STRING
        self.next_todo = ""

        self._attributes['current_nu'] = AttributeTypes.INT
        self.current_nu =  0
        self._attributes['current_q'] = AttributeTypes.INT
        self.current_q =  0
        self._attributes['num_q'] = AttributeTypes.INT
        self.num_q =  0
        self._attributes['num_nu'] = AttributeTypes.INT
        self.num_nu =  0
        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0
        self._attributes['num_omega'] = AttributeTypes.INT
        self.num_omega =  0
        self._attributes['num_bands'] = AttributeTypes.INT
        self.num_bands =  0



# this is the end of the generated file