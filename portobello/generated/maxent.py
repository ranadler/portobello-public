'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class Input(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_x'] = AttributeTypes.INT
        self.num_x =  0

        self._attributes['beta'] = AttributeTypes.FLOAT
        self.beta =  0.0

        self._attributes['nomega'] = AttributeTypes.INT
        self.nomega =  0

        self._attributes['FERMIONIC_TAU_KERNEL'] = AttributeTypes.INT
        self.FERMIONIC_TAU_KERNEL =  1
        self._attributes['FERMIONIC_IOMEGA_KERNEL'] = AttributeTypes.INT
        self.FERMIONIC_IOMEGA_KERNEL =  2
        self._attributes['BOSONIC_TAU_KERNEL'] = AttributeTypes.INT
        self.BOSONIC_TAU_KERNEL =  3
        self._attributes['BOSONIC_IOMEGA_KERNEL'] = AttributeTypes.INT
        self.BOSONIC_IOMEGA_KERNEL =  4
        self._attributes['kernel_choice'] = AttributeTypes.INT
        self.kernel_choice =  1
        self._datasets['kernel'] = (np.complex128, '(int(self.nomega- (-self.nomega) + 1), int(self.num_x))' )
        self.kernel : NDArray[(Any, Any), Complex128]
        self.kernel = None
        self._attributes['normalization'] = AttributeTypes.FLOAT
        self.normalization = 1.000000
        self._datasets['omega'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.omega : NDArray[(Any,), Float64]
        self.omega = None
        self._datasets['d_omega'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.d_omega : NDArray[(Any,), Float64]
        self.d_omega = None
        self._datasets['dlda'] = (np.float64, '(int(2 * self.nomega + 1), int(2 * self.nomega + 1))' )
        self.dlda : NDArray[(Any, Any), Float64]
        self.dlda = None
        self._datasets['model'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.model : NDArray[(Any,), Float64]
        self.model = None
        self._datasets['Dp'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.Dp : NDArray[(Any,), Float64]
        self.Dp = None
        self._datasets['Dm'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.Dm : NDArray[(Any,), Float64]
        self.Dm = None
        self._datasets['signal'] = (np.complex128, '(int(self.num_x),)' )
        self.signal : NDArray[(Any,), Complex128]
        self.signal = None
        self._datasets['invVar'] = (np.float64, '(int(self.num_x),)' )
        self.invVar : NDArray[(Any,), Float64]
        self.invVar = None
        self._datasets['X'] = (np.complex128, '(int(self.num_x),)' )
        self.X : NDArray[(Any,), Complex128]
        self.X = None


    @classmethod
    def CompleteSetup(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("maxent","maxent_driver", "CompleteSetup",
            selfpath)

class Workspace(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_x'] = AttributeTypes.INT
        self.num_x =  0

        self._attributes['nomega'] = AttributeTypes.INT
        self.nomega =  0

        self._attributes['alpha'] = AttributeTypes.FLOAT
        self.alpha = 1000.000000
        self._attributes['rfac'] = AttributeTypes.FLOAT
        self.rfac = 1.000000
        self._attributes['temp'] = AttributeTypes.FLOAT
        self.temp = 10.000000
        self._datasets['A'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.A : NDArray[(Any,), Float64]
        self.A = None
        self._datasets['Ap'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.Ap : NDArray[(Any,), Float64]
        self.Ap = None
        self._datasets['Am'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.Am : NDArray[(Any,), Float64]
        self.Am = None
        self._datasets['CA'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.CA : NDArray[(Any,), Float64]
        self.CA = None
        self._datasets['dA'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.dA : NDArray[(Any,), Float64]
        self.dA = None
        self._datasets['dAp'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.dAp : NDArray[(Any,), Float64]
        self.dAp = None
        self._datasets['dAm'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.dAm : NDArray[(Any,), Float64]
        self.dAm = None
        self._datasets['dCA'] = (np.float64, '(int(self.nomega- (-self.nomega) + 1),)' )
        self.dCA : NDArray[(Any,), Float64]
        self.dCA = None
        self._datasets['signal1'] = (np.complex128, '(int(self.num_x),)' )
        self.signal1 : NDArray[(Any,), Complex128]
        self.signal1 = None
        self._datasets['signal2'] = (np.complex128, '(int(self.num_x),)' )
        self.signal2 : NDArray[(Any,), Complex128]
        self.signal2 = None
        self._attributes['entropy'] = AttributeTypes.FLOAT
        self.entropy =  0.0

        self._attributes['tr'] = AttributeTypes.FLOAT
        self.tr =  0.0



    @classmethod
    def Maxent(cls, selfpath, num_steps):
        fort = Fortran.Instance()
        return fort.CallWrapper("maxent","maxent_driver", "Maxent",
            selfpath, c_int(num_steps))
    @classmethod
    def MaxentOffDiag(cls, selfpath, num_steps):
        fort = Fortran.Instance()
        return fort.CallWrapper("maxent","maxent_driver", "MaxentOffDiag",
            selfpath, c_int(num_steps))

# this is the end of the generated file