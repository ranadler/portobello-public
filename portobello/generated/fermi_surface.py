'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class LowEnergyHamiltonian(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['min_band'] = AttributeTypes.INT
        self.min_band =  0

        self._attributes['max_band'] = AttributeTypes.INT
        self.max_band =  0

        self._attributes['num_k_irr'] = AttributeTypes.INT
        self.num_k_irr =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._attributes['name'] = AttributeTypes.STRING
        self.name = ""

        self._datasets['eps'] = (np.float64, '(int(self.max_band- (self.min_band) + 1), int(self.num_k_irr), int(self.num_si))' )
        self.eps : NDArray[(Any, Any, Any), Float64]
        self.eps = None
        self._datasets['scalar'] = (np.float64, '(int(self.max_band- (self.min_band) + 1), int(self.num_k_irr), int(self.num_si))' )
        self.scalar : NDArray[(Any, Any, Any), Float64]
        self.scalar = None


    @classmethod
    def WriteFRMSF(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("fermi_surface","fermisurfer_driver", "WriteFRMSF",
            selfpath)

# this is the end of the generated file