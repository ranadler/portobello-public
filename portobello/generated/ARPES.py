'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class label(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['text'] = AttributeTypes.STRING
        self.text = ""




class HSKP(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['npoints'] = AttributeTypes.INT
        self.npoints =  0

        self._objars['labels'] = ( ('ARPES', 'label'), '(int(self.npoints),)') 
        self.labels : NDarray[(Any,), ARPES.label]
        self.labels = None



class ARPES(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['title'] = AttributeTypes.STRING
        self.title = ""

        self._attributes['num_equivalents'] = AttributeTypes.INT
        self.num_equivalents =  0

        self._attributes['num_directions'] = AttributeTypes.INT
        self.num_directions =  0

        self._attributes['num_values'] = AttributeTypes.INT
        self.num_values =  0

        self._attributes['nx'] = AttributeTypes.INT
        self.nx =  0

        self._attributes['ny'] = AttributeTypes.INT
        self.ny =  0

        self._datasets['Z'] = (np.float64, '(int(self.num_equivalents), int(self.num_directions), int(self.num_values), int(self.nx), int(self.ny))' )
        self.Z : NDArray[(Any, Any, Any, Any, Any), Float64]
        self.Z = None
        self._datasets['values'] = (np.float64, '(int(self.num_values),)' )
        self.values : NDArray[(Any,), Float64]
        self.values = None
        self._objects['hskp'] = ('ARPES', 'HSKP')
        self.hskp = HSKP()



class PathARPES(ARPES):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ARPES.__InitMetadata__(self, True)

        self._attributes['is_plane'] = AttributeTypes.BOOLEAN
        self.is_plane = False

        self._datasets['omegas'] = (np.float64, '(int(self.ny),)' )
        self.omegas : NDArray[(Any,), Float64]
        self.omegas = None
        self._attributes['xlabel'] = AttributeTypes.STRING
        self.xlabel = "k"

        self._attributes['ylabel'] = AttributeTypes.STRING
        self.ylabel = "\omega"




class PlaneARPES(ARPES):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ARPES.__InitMetadata__(self, True)

        self._attributes['is_plane'] = AttributeTypes.BOOLEAN
        self.is_plane = False

        self._attributes['width'] = AttributeTypes.FLOAT
        self.width =  0.0

        self._attributes['xlabel'] = AttributeTypes.STRING
        self.xlabel = ""

        self._attributes['ylabel'] = AttributeTypes.STRING
        self.ylabel = ""




# this is the end of the generated file