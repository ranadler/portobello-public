'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran


class Input(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['Temperature'] = AttributeTypes.FLOAT
        self.Temperature = 1.000000
        self._attributes['maxpower'] = AttributeTypes.INT
        self.maxpower =  2
        self._attributes['symmetrize_bz'] = AttributeTypes.BOOLEAN
        self.symmetrize_bz = False

        self._attributes['interband'] = AttributeTypes.BOOLEAN
        self.interband = False

        self._attributes['gamma'] = AttributeTypes.FLOAT
        self.gamma = 0.000500
        self._attributes['gamma_correlated'] = AttributeTypes.FLOAT
        self.gamma_correlated = 0.000500
        self._attributes['delta'] = AttributeTypes.FLOAT
        self.delta = 0.001000
        self._attributes['omega_max'] = AttributeTypes.FLOAT
        self.omega_max = 0.150000
        self._attributes['energy_window'] = AttributeTypes.FLOAT
        self.energy_window = 0.100000
        self._attributes['min_band'] = AttributeTypes.INT
        self.min_band =  1
        self._attributes['max_band'] = AttributeTypes.INT
        self.max_band =  1
        self._attributes['num_omega'] = AttributeTypes.INT
        self.num_omega =  0
        self._attributes['run_optics'] = AttributeTypes.BOOLEAN
        self.run_optics = False

        self._attributes['num_dirs'] = AttributeTypes.INT
        self.num_dirs =  3
        self._datasets['dirs'] = (np.float64, '(int(3), int(self.num_dirs))' )
        self.dirs = None


    @classmethod
    def ComputeFromPortobello(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("Transport","PortobelloProvider", "ComputeFromPortobello",
            selfpath)

class WorkArea(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_dirs'] = AttributeTypes.INT
        self.num_dirs =  3
        self._attributes['maxpower'] = AttributeTypes.INT
        self.maxpower =  2
        self._attributes['num_omega'] = AttributeTypes.INT
        self.num_omega =  0

        self._datasets['total'] = (np.float64, '(int(self.num_dirs), int(self.maxpower- (0) + 1), int(self.num_omega- (0) + 1))' )
        self.total = None



class Output(Input):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Input.__InitMetadata__(self, True)

        self._datasets['omega'] = (np.float64, '(int(self.num_omega- (0) + 1),)' )
        self.omega = None
        self._datasets['A'] = (np.float64, '(int(self.num_omega- (0) + 1), int(self.num_dirs), int(self.maxpower- (0) + 1))' )
        self.A = None
        self._datasets['sigma'] = (np.float64, '(int(self.num_dirs),)' )
        self.sigma = None
        self._datasets['A1'] = (np.float64, '(int(self.num_dirs),)' )
        self.A1 = None
        self._datasets['A2'] = (np.float64, '(int(self.num_dirs),)' )
        self.A2 = None
        self._datasets['Seebeck'] = (np.float64, '(int(self.num_dirs),)' )
        self.Seebeck = None
        self._datasets['kappa'] = (np.float64, '(int(self.num_dirs),)' )
        self.kappa = None
        self._datasets['ZT'] = (np.float64, '(int(self.num_dirs),)' )
        self.ZT = None



# this is the end of the generated file