'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class Tensor2(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['len'] = AttributeTypes.INT
        self.len =  0

        self._datasets['i1'] = (np.int32, '(int(self.len),)' )
        self.i1 : NDArray[(Any,), Int32]
        self.i1 = None
        self._datasets['i2'] = (np.int32, '(int(self.len),)' )
        self.i2 : NDArray[(Any,), Int32]
        self.i2 = None
        self._datasets['value'] = (np.complex128, '(int(self.len),)' )
        self.value : NDArray[(Any,), Complex128]
        self.value = None



class Tensor4(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['len'] = AttributeTypes.INT
        self.len =  0

        self._datasets['i1'] = (np.int32, '(int(self.len),)' )
        self.i1 : NDArray[(Any,), Int32]
        self.i1 = None
        self._datasets['i2'] = (np.int32, '(int(self.len),)' )
        self.i2 : NDArray[(Any,), Int32]
        self.i2 = None
        self._datasets['i3'] = (np.int32, '(int(self.len),)' )
        self.i3 : NDArray[(Any,), Int32]
        self.i3 = None
        self._datasets['i4'] = (np.int32, '(int(self.len),)' )
        self.i4 : NDArray[(Any,), Int32]
        self.i4 = None
        self._datasets['value'] = (np.complex128, '(int(self.len),)' )
        self.value : NDArray[(Any,), Complex128]
        self.value = None



class T_csr(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['nnz'] = AttributeTypes.INT
        self.nnz =  0

        self._attributes['m'] = AttributeTypes.INT
        self.m =  0

        self._attributes['row_shift'] = AttributeTypes.INT
        self.row_shift =  0

        self._attributes['col_shift'] = AttributeTypes.INT
        self.col_shift =  0

        self._datasets['iaa'] = (np.int32, '(int(self.m + 1),)' )
        self.iaa : NDArray[(Any,), Int32]
        self.iaa = None
        self._datasets['jaa'] = (np.int32, '(int(self.nnz),)' )
        self.jaa : NDArray[(Any,), Int32]
        self.jaa = None
        self._datasets['aa'] = (np.complex128, '(int(self.nnz),)' )
        self.aa : NDArray[(Any,), Complex128]
        self.aa = None



class CSRBlocks(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['nblock'] = AttributeTypes.INT
        self.nblock =  0

        self._objars['ham_csr'] = ( ('edx', 'T_csr'), '(int(self.nblock),)') 
        self.ham_csr : NDarray[(Any,), edx.T_csr]
        self.ham_csr = None



class Setup(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['ed_solver'] = AttributeTypes.INT
        self.ed_solver =  1
        self._attributes['num_val_orbs'] = AttributeTypes.INT
        self.num_val_orbs =  2
        self._attributes['maxiter'] = AttributeTypes.INT
        self.maxiter =  500
        self._attributes['eigval_tol'] = AttributeTypes.FLOAT
        self.eigval_tol = 0.000000
        self._attributes['nvector'] = AttributeTypes.INT
        self.nvector =  0

        self._attributes['neval'] = AttributeTypes.INT
        self.neval =  0

        self._attributes['num_fock'] = AttributeTypes.INT
        self.num_fock =  0

        self._attributes['debug_print'] = AttributeTypes.BOOLEAN
        self.debug_print = False

        self._datasets['fock'] = (np.int32, '(int(self.num_fock),)' )
        self.fock : NDArray[(Any,), Int32]
        self.fock = None
        self._objects['coulomb'] = ('edx', 'Tensor4')
        self.coulomb = Tensor4()
        self._objects['hopping_rep'] = ('edx', 'Tensor2')
        self.hopping_rep = Tensor2()
        self._attributes['ncv'] = AttributeTypes.INT
        self.ncv =  0

        self._attributes['prefix'] = AttributeTypes.STRING
        self.prefix = "V0-"




class Input(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._objects['hopping'] = ('edx', 'Tensor2')
        self.hopping = Tensor2()



class Output(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['is_valid'] = AttributeTypes.BOOLEAN
        self.is_valid = False

        self._attributes['num_val_orbs'] = AttributeTypes.INT
        self.num_val_orbs =  0

        self._attributes['nvector'] = AttributeTypes.INT
        self.nvector =  0

        self._attributes['neval'] = AttributeTypes.INT
        self.neval =  0

        self._attributes['num_fock'] = AttributeTypes.INT
        self.num_fock =  0

        self._attributes['num_orbital_fock_states'] = AttributeTypes.INT
        self.num_orbital_fock_states =  0

        self._datasets['denmat'] = (np.complex128, '(int(self.num_val_orbs), int(self.num_val_orbs), int(self.nvector))' )
        self.denmat : NDArray[(Any, Any, Any), Complex128]
        self.denmat = None
        self._datasets['E'] = (np.complex128, '(int(self.neval),)' )
        self.E : NDArray[(Any,), Complex128]
        self.E = None
        self._datasets['eigvecs'] = (np.complex128, '(int(self.num_fock), int(self.nvector))' )
        self.eigvecs : NDArray[(Any, Any), Complex128]
        self.eigvecs = None
        self._datasets['histogram'] = (np.float64, '(int(self.num_orbital_fock_states),)' )
        self.histogram : NDArray[(Any,), Float64]
        self.histogram = None



class EDSolverState(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_x'] = AttributeTypes.INT
        self.num_x =  0

        self._datasets['x'] = (np.float64, '(int(self.num_x),)' )
        self.x : NDArray[(Any,), Float64]
        self.x = None



# this is the end of the generated file