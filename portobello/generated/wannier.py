'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict

from portobello.generated.atomic_orbitals import *
from portobello.generated.base_types import *

class WannierInput(OrbitalsBandProjector):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        OrbitalsBandProjector.__InitMetadata__(self, True)

        self._attributes['num_k_full'] = AttributeTypes.INT
        self.num_k_full =  0

        self._objects['dis_window'] = ('base_types', 'Window')
        self.dis_window = Window()
        self._attributes['location_of_partial_bs_all_k'] = AttributeTypes.STRING
        self.location_of_partial_bs_all_k = ""

        self._datasets['A'] = (np.complex128, '(int(self.max_band - self.min_band + 1), int(self.num_orbs), int(self.num_k_full))' )
        self.A : NDArray[(Any, Any, Any), Complex128]
        self.A = None


    @classmethod
    def Compute(cls, selfpath, outputpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("wannier","wannier_driver", "Compute",
            selfpath, outputpath)

class WannierOutput(OrbitalsBandProjector):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        OrbitalsBandProjector.__InitMetadata__(self, True)

        self._datasets['nearest_atom'] = (np.int32, '(int(self.num_orbs),)' )
        self.nearest_atom : NDArray[(Any,), Int32]
        self.nearest_atom = None
        self._datasets['distance_to_nearest_atom'] = (np.float64, '(int(3), int(self.num_orbs))' )
        self.distance_to_nearest_atom : NDArray[(Any, Any), Float64]
        self.distance_to_nearest_atom = None
        self._datasets['centers'] = (np.float64, '(int(3), int(self.num_orbs))' )
        self.centers : NDArray[(Any, Any), Float64]
        self.centers = None


    @classmethod
    def WriteXSF(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("wannier","visualize_orbitals", "WriteXSF",
            selfpath)

# this is the end of the generated file