'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict

from portobello.generated.atomic_orbitals import *

class ModelShell(AtomicShell):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        AtomicShell.__InitMetadata__(self, True)

        self._datasets['correlated_orb_indices'] = (np.int32, '(int(self.dim),)' )
        self.correlated_orb_indices : NDArray[(Any,), Int32]
        self.correlated_orb_indices = None



class ModelState(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['model_type'] = AttributeTypes.STRING
        self.model_type = "wannier90"




# this is the end of the generated file