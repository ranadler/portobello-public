'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class ImpurityProblem(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['file_name'] = AttributeTypes.STRING
        self.file_name = ""

        self._attributes['description'] = AttributeTypes.STRING
        self.description = ""




class MultiImpuritySupervisor(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_impurities'] = AttributeTypes.INT
        self.num_impurities =  0

        self._attributes['ready'] = AttributeTypes.BOOLEAN
        self.ready = False

        self._attributes['solver_class'] = AttributeTypes.STRING
        self.solver_class = ""

        self._attributes['solver_module'] = AttributeTypes.STRING
        self.solver_module = ""

        self._objars['impurity_problem'] = ( ('MI', 'ImpurityProblem'), '(int(self.num_impurities),)') 
        self.impurity_problem : NDarray[(Any,), MI.ImpurityProblem]
        self.impurity_problem = None



# this is the end of the generated file