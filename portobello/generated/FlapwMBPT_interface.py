'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict

from portobello.generated.symmetry import *

class CrystalStructure(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_atoms'] = AttributeTypes.INT
        self.num_atoms =  0

        self._attributes['num_distinct'] = AttributeTypes.INT
        self.num_distinct =  0

        self._datasets['a'] = (np.float64, '(int(3),)' )
        self.a : NDArray[(Any,), Float64]
        self.a = None
        self._datasets['b'] = (np.float64, '(int(3),)' )
        self.b : NDArray[(Any,), Float64]
        self.b = None
        self._datasets['c'] = (np.float64, '(int(3),)' )
        self.c : NDArray[(Any,), Float64]
        self.c = None
        self._datasets['xyz'] = (np.float64, '(int(3), int(self.num_atoms))' )
        self.xyz : NDArray[(Any, Any), Float64]
        self.xyz = None
        self._datasets['Z'] = (np.float64, '(int(self.num_distinct),)' )
        self.Z : NDArray[(Any,), Float64]
        self.Z = None
        self._datasets['distinct_number'] = (np.int32, '(int(self.num_atoms),)' )
        self.distinct_number : NDArray[(Any,), Int32]
        self.distinct_number = None
        self._datasets['max_l_per_distinct'] = (np.int32, '(int(self.num_distinct),)' )
        self.max_l_per_distinct : NDArray[(Any,), Int32]
        self.max_l_per_distinct = None
        self._attributes['max_l'] = AttributeTypes.INT
        self.max_l =  0




class Convergence(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_valence_electrons'] = AttributeTypes.INT
        self.num_valence_electrons =  0
        self._attributes['sum_valence_electrons'] = AttributeTypes.FLOAT
        self.sum_valence_electrons =  0.0

        self._attributes['charge_convergence'] = AttributeTypes.FLOAT
        self.charge_convergence =  0.0

        self._attributes['mag_convergence'] = AttributeTypes.FLOAT
        self.mag_convergence =  0.0




class DFT(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_k_all'] = AttributeTypes.INT
        self.num_k_all =  0

        self._attributes['num_k_irr'] = AttributeTypes.INT
        self.num_k_irr =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  1
        self._attributes['nrel'] = AttributeTypes.INT
        self.nrel =  1
        self._objects['sg'] = ('symmetry', 'SpaceGroup')
        self.sg = SpaceGroup()
        self._objects['st'] = ('FlapwMBPT_interface', 'CrystalStructure')
        self.st = CrystalStructure()
        self._attributes['is_sharded'] = AttributeTypes.BOOLEAN
        self.is_sharded = False

        self._attributes['shard_offset'] = AttributeTypes.INT
        self.shard_offset =  0
        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0

        self._datasets['k_irr'] = (np.int32, '(int(self.num_k_all),)' )
        self.k_irr : NDArray[(Any,), Int32]
        self.k_irr = None
        self._datasets['ig'] = (np.int32, '(int(self.num_k_all),)' )
        self.ig : NDArray[(Any,), Int32]
        self.ig = None
        self._attributes['num_bands'] = AttributeTypes.INT
        self.num_bands =  0

        self._attributes['num_valence_electrons'] = AttributeTypes.INT
        self.num_valence_electrons =  0



    @classmethod
    def AdjustLAPWBasis(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","DFTPlusSigma", "AdjustLAPWBasis",
            selfpath)
    @classmethod
    def DFTPlusSigmaOneIteration(cls, selfpath, sigma_location, checkpoint, search_mu, update_lapw, admix, save_potentials, mu_search_variant):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","DFTPlusSigma", "DFTPlusSigmaOneIteration",
            selfpath, sigma_location, c_int(checkpoint), c_int(search_mu), c_int(update_lapw), c_float(admix), c_int(save_potentials), mu_search_variant)
    @classmethod
    def ConnectFlapwMBPT(cls, selfpath, inpfile, num_workers, opts):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "ConnectFlapwMBPT",
            selfpath, inpfile, c_int(num_workers), opts)
    @classmethod
    def Reload(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "Reload",
            selfpath)
    @classmethod
    def GetDFTInfo(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "GetDFTInfo",
            selfpath)
    @classmethod
    def GetLAPWBasis(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "GetLAPWBasis",
            selfpath)
    @classmethod
    def GetBasisMTInfo(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "GetBasisMTInfo",
            selfpath)
    @classmethod
    def GetPartialBandStructure(cls, selfpath, min_band, max_band, k):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "GetPartialBandStructure",
            selfpath, c_int(min_band), c_int(max_band), c_int(k))
    @classmethod
    def GetPartialBandStructureAndGradient(cls, selfpath, gradpath, min_band, max_band, k):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "GetPartialBandStructureAndGradient",
            selfpath, gradpath, c_int(min_band), c_int(max_band), c_int(k))
    @classmethod
    def GetKpathBands(cls, selfpath, kpathpath, min_band, max_band):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "GetKpathBands",
            selfpath, kpathpath, c_int(min_band), c_int(max_band))
    @classmethod
    def GetKPathBandsAndGradient(cls, selfpath, gradpath, kpathpath, min_band, max_band):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "GetKPathBandsAndGradient",
            selfpath, gradpath, kpathpath, c_int(min_band), c_int(max_band))
    @classmethod
    def GetEquivalentAtomSymmetries(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "GetEquivalentAtomSymmetries",
            selfpath)
    @classmethod
    def CreatePlots(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "CreatePlots",
            selfpath)
    @classmethod
    def GetConvergence(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","DFTPlusSigma", "GetConvergence",
            selfpath)
    @classmethod
    def Disconnect(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("FlapwMBPT_interface","flapw_driver", "Disconnect",
            selfpath)

# this is the end of the generated file