'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class FlapwForce(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_atoms'] = AttributeTypes.INT
        self.num_atoms =  0

        self._attributes['num_cont'] = AttributeTypes.INT
        self.num_cont =  5
        self._datasets['force_cont'] = (np.float64, '(int(3), int(self.num_atoms), int(self.num_cont))' )
        self.force_cont : NDArray[(Any, Any, Any), Float64]
        self.force_cont = None


    @classmethod
    def CalculateForce(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("force","flapw_driver", "CalculateForce",
            selfpath)

# this is the end of the generated file