'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class BasisIds(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['muffin_tin'] = AttributeTypes.INT
        self.muffin_tin =  0
        self._attributes['bands'] = AttributeTypes.INT
        self.bands =  0
        self._attributes['correlated_orbitals'] = AttributeTypes.INT
        self.correlated_orbitals =  0
        self._attributes['k_mesh'] = AttributeTypes.INT
        self.k_mesh =  0
        self._attributes['omega_mesh'] = AttributeTypes.INT
        self.omega_mesh =  0



# this is the end of the generated file