'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict

from portobello.generated.base_types import *

class Wannier90Bounds(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['min_band'] = AttributeTypes.INT
        self.min_band =  0

        self._attributes['max_band'] = AttributeTypes.INT
        self.max_band =  0

        self._attributes['num_bands'] = AttributeTypes.INT
        self.num_bands =  0

        self._attributes['num_wann'] = AttributeTypes.INT
        self.num_wann =  0

        self._attributes['num_k_all'] = AttributeTypes.INT
        self.num_k_all =  0




class Wannier90Overlap(Wannier90Bounds):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Wannier90Bounds.__InitMetadata__(self, True)

        self._attributes['nntot'] = AttributeTypes.INT
        self.nntot =  0

        self._datasets['M'] = (np.complex128, '(int(self.num_bands), int(self.num_bands), int(self.nntot), int(self.num_k_all))' )
        self.M : NDArray[(Any, Any, Any, Any), Complex128]
        self.M = None



class Wannier90SymAdapted(Wannier90Bounds):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Wannier90Bounds.__InitMetadata__(self, True)

        self._attributes['num_k_irr'] = AttributeTypes.INT
        self.num_k_irr =  0

        self._attributes['num_ops'] = AttributeTypes.INT
        self.num_ops =  0

        self._datasets['KSRep'] = (np.complex128, '(int(self.num_bands), int(self.num_bands), int(self.num_ops), int(self.num_k_irr))' )
        self.KSRep : NDArray[(Any, Any, Any, Any), Complex128]
        self.KSRep = None
        self._datasets['OrbitalRep'] = (np.complex128, '(int(self.num_wann), int(self.num_wann), int(self.num_ops), int(self.num_k_irr))' )
        self.OrbitalRep : NDArray[(Any, Any, Any, Any), Complex128]
        self.OrbitalRep = None



class Wannier90Workspace(Wannier90Bounds):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Wannier90Bounds.__InitMetadata__(self, True)

        self._attributes['num_nnmax'] = AttributeTypes.INT
        self.num_nnmax =  12
        self._objects['dis_window'] = ('base_types', 'Window')
        self.dis_window = Window()
        self._datasets['k_lat'] = (np.float64, '(int(3), int(self.num_k_all))' )
        self.k_lat : NDArray[(Any, Any), Float64]
        self.k_lat = None
        self._datasets['nnlist'] = (np.int32, '(int(self.num_k_all), int(self.num_nnmax))' )
        self.nnlist : NDArray[(Any, Any), Int32]
        self.nnlist = None
        self._datasets['nncell'] = (np.int32, '(int(3), int(self.num_k_all), int(self.num_nnmax))' )
        self.nncell : NDArray[(Any, Any, Any), Int32]
        self.nncell = None
        self._datasets['eigenvalues'] = (np.float64, '(int(self.num_bands), int(self.num_k_all))' )
        self.eigenvalues : NDArray[(Any, Any), Float64]
        self.eigenvalues = None
        self._objects['overlap'] = ('wannier_internal', 'Wannier90Overlap')
        self.overlap = Wannier90Overlap()
        self._datasets['U'] = (np.complex128, '(int(self.num_wann), int(self.num_wann), int(self.num_k_all))' )
        self.U : NDArray[(Any, Any, Any), Complex128]
        self.U = None
        self._datasets['U_opt'] = (np.complex128, '(int(self.num_bands), int(self.num_wann), int(self.num_k_all))' )
        self.U_opt : NDArray[(Any, Any, Any), Complex128]
        self.U_opt = None
        self._datasets['M0'] = (np.complex128, '(int(self.num_bands), int(self.num_bands), int(self.num_k_all))' )
        self.M0 : NDArray[(Any, Any, Any), Complex128]
        self.M0 = None



# this is the end of the generated file