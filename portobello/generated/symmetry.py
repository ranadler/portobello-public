'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class Bootstrap(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_ops'] = AttributeTypes.INT
        self.num_ops =  0

        self._attributes['max_l'] = AttributeTypes.INT
        self.max_l =  0

        self._attributes['maxIndexDl'] = AttributeTypes.INT
        self.maxIndexDl =  0

        self._attributes['maxIndexDj'] = AttributeTypes.INT
        self.maxIndexDj =  0

        self._datasets['Dl'] = (np.float64, '(int(self.maxIndexDl), int(self.num_ops))' )
        self.Dl : NDArray[(Any, Any), Float64]
        self.Dl = None
        self._datasets['Dj'] = (np.complex128, '(int(self.maxIndexDj), int(self.num_ops))' )
        self.Dj : NDArray[(Any, Any), Complex128]
        self.Dj = None
        self._datasets['rots'] = (np.float64, '(int(3), int(3), int(self.num_ops))' )
        self.rots : NDArray[(Any, Any, Any), Float64]
        self.rots = None
        self._datasets['shifts'] = (np.float64, '(int(3), int(self.num_ops))' )
        self.shifts : NDArray[(Any, Any), Float64]
        self.shifts = None
        self._datasets['recip'] = (np.float64, '(int(3), int(3))' )
        self.recip : NDArray[(Any, Any), Float64]
        self.recip = None



class GL3Group(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_ops'] = AttributeTypes.INT
        self.num_ops =  0

        self._datasets['rots'] = (np.float64, '(int(3), int(3), int(self.num_ops))' )
        self.rots : NDArray[(Any, Any, Any), Float64]
        self.rots = None
        self._datasets['WigD0'] = (np.float64, '(int(1), int(1), int(self.num_ops))' )
        self.WigD0 : NDArray[(Any, Any, Any), Float64]
        self.WigD0 = None
        self._datasets['WigD1'] = (np.float64, '(int(3), int(3), int(self.num_ops))' )
        self.WigD1 : NDArray[(Any, Any, Any), Float64]
        self.WigD1 = None
        self._datasets['WigD2'] = (np.float64, '(int(5), int(5), int(self.num_ops))' )
        self.WigD2 : NDArray[(Any, Any, Any), Float64]
        self.WigD2 = None
        self._datasets['WigD3'] = (np.float64, '(int(7), int(7), int(self.num_ops))' )
        self.WigD3 : NDArray[(Any, Any, Any), Float64]
        self.WigD3 = None
        self._attributes['hasWigJ'] = AttributeTypes.BOOLEAN
        self.hasWigJ = False

        self._datasets['WigD0J'] = (np.complex128, '(int(2), int(2), int(self.num_ops))' )
        self.WigD0J : NDArray[(Any, Any, Any), Complex128]
        self.WigD0J = None
        self._datasets['WigD1J'] = (np.complex128, '(int(6), int(6), int(self.num_ops))' )
        self.WigD1J : NDArray[(Any, Any, Any), Complex128]
        self.WigD1J = None
        self._datasets['WigD2J'] = (np.complex128, '(int(10), int(10), int(self.num_ops))' )
        self.WigD2J : NDArray[(Any, Any, Any), Complex128]
        self.WigD2J = None
        self._datasets['WigD3J'] = (np.complex128, '(int(14), int(14), int(self.num_ops))' )
        self.WigD3J : NDArray[(Any, Any, Any), Complex128]
        self.WigD3J = None


    @classmethod
    def GetWignerMatrices(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("symmetry","flapw_driver", "GetWignerMatrices",
            selfpath)

class SpaceGroup(GL3Group):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        GL3Group.__InitMetadata__(self, True)

        self._datasets['shifts'] = (np.float64, '(int(3), int(self.num_ops))' )
        self.shifts : NDArray[(Any, Any), Float64]
        self.shifts = None



class GroupRepresentation(GL3Group):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        GL3Group.__InitMetadata__(self, True)

        self._attributes['dim'] = AttributeTypes.INT
        self.dim =  0

        self._datasets['rep'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_ops))' )
        self.rep : NDArray[(Any, Any, Any), Complex128]
        self.rep = None



class OrbitalLabels(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['short_label'] = AttributeTypes.STRING
        self.short_label = ""

        self._attributes['latex_label'] = AttributeTypes.STRING
        self.latex_label = ""




class AnnotatedRepresentation(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_ops'] = AttributeTypes.INT
        self.num_ops =  0

        self._attributes['l'] = AttributeTypes.INT
        self.l =  0

        self._attributes['nrel'] = AttributeTypes.BOOLEAN
        self.nrel = False

        self._attributes['num_si'] = AttributeTypes.BOOLEAN
        self.num_si = False

        self._attributes['dim'] = AttributeTypes.INT
        self.dim =  0

        self._attributes['num_blocks'] = AttributeTypes.INT
        self.num_blocks =  0

        self._attributes['blocking_tol'] = AttributeTypes.FLOAT
        self.blocking_tol =  0.0

        self._attributes['G_tolerance'] = AttributeTypes.FLOAT
        self.G_tolerance =  0.0

        self._datasets['rep'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_ops))' )
        self.rep : NDArray[(Any, Any, Any), Complex128]
        self.rep = None
        self._datasets['blocks'] = (np.int32, '(int(self.dim),)' )
        self.blocks : NDArray[(Any,), Int32]
        self.blocks = None
        self._datasets['characters'] = (np.complex128, '(int(self.num_blocks), int(self.num_ops))' )
        self.characters : NDArray[(Any, Any), Complex128]
        self.characters = None
        self._datasets['chiHchi'] = (np.complex128, '(int(self.num_blocks), int(self.num_blocks))' )
        self.chiHchi : NDArray[(Any, Any), Complex128]
        self.chiHchi = None
        self._datasets['H'] = (np.int32, '(int(self.dim), int(self.dim))' )
        self.H : NDArray[(Any, Any), Int32]
        self.H = None
        self._datasets['rot'] = (np.float64, '(int(3), int(3))' )
        self.rot : NDArray[(Any, Any), Float64]
        self.rot = None
        self._datasets['rotD'] = (np.complex128, '(int(self.dim), int(self.dim))' )
        self.rotD : NDArray[(Any, Any), Complex128]
        self.rotD = None
        self._datasets['basisChangeD'] = (np.complex128, '(int(self.dim), int(self.dim))' )
        self.basisChangeD : NDArray[(Any, Any), Complex128]
        self.basisChangeD = None
        self._objars['labels'] = ( ('symmetry', 'OrbitalLabels'), '(int(self.dim),)') 
        self.labels : NDarray[(Any,), symmetry.OrbitalLabels]
        self.labels = None



class PeriodicSite(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['site_index'] = AttributeTypes.INT
        self.site_index =  0

        self._datasets['xyz'] = (np.float64, '(int(3),)' )
        self.xyz : NDArray[(Any,), Float64]
        self.xyz = None



class LocalSymmetry(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['site_index'] = AttributeTypes.INT
        self.site_index =  0

        self._objects['local_group'] = ('symmetry', 'GL3Group')
        self.local_group = GL3Group()
        self._objars['annotated_reps_l'] = ( ('symmetry', 'AnnotatedRepresentation'), '(int(3- (0) + 1),)') 
        self.annotated_reps_l : NDarray[(Any,), symmetry.AnnotatedRepresentation]
        self.annotated_reps_l = None



class StructLocalSymmetry(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_distinct'] = AttributeTypes.INT
        self.num_distinct =  0

        self._attributes['num_atoms'] = AttributeTypes.INT
        self.num_atoms =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._attributes['nrel'] = AttributeTypes.INT
        self.nrel =  0

        self._objars['distinct_symm'] = ( ('symmetry', 'LocalSymmetry'), '(int(self.num_distinct),)') 
        self.distinct_symm : NDarray[(Any,), symmetry.LocalSymmetry]
        self.distinct_symm = None



# this is the end of the generated file