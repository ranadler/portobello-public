'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class ComplexArray(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num'] = AttributeTypes.INT
        self.num =  0

        self._datasets['imag'] = (np.float64, '(int(self.num),)' )
        self.imag : NDArray[(Any,), Float64]
        self.imag = None
        self._datasets['real'] = (np.float64, '(int(self.num),)' )
        self.real : NDArray[(Any,), Float64]
        self.real = None



class Hyb(ComplexArray):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ComplexArray.__InitMetadata__(self, True)

        self._attributes['beta'] = AttributeTypes.FLOAT
        self.beta =  0.0




class ComplexSquareMatrix(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['dim'] = AttributeTypes.INT
        self.dim =  0

        self._datasets['imag'] = (np.float64, '(int(self.dim), int(self.dim))' )
        self.imag : NDArray[(Any, Any), Float64]
        self.imag = None
        self._datasets['real'] = (np.float64, '(int(self.dim), int(self.dim))' )
        self.real : NDArray[(Any, Any), Float64]
        self.real = None



class BasisStruct(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['orbitals'] = AttributeTypes.STRING
        self.orbitals = ""

        self._objects['transformation'] = ('CTQMC_internal', 'ComplexSquareMatrix')
        self.transformation = ComplexSquareMatrix()
        self._attributes['type'] = AttributeTypes.STRING
        self.type = "product"




class QuantumNumbersPlaceHolder(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)




class CTQMCObservable(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._objects['one_body'] = ('CTQMC_internal', 'ComplexSquareMatrix')
        self.one_body = ComplexSquareMatrix()



class ObservablesPlaceHolder(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)




class TwoBodyStruct(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['F0'] = AttributeTypes.FLOAT
        self.F0 =  0.0

        self._attributes['F2'] = AttributeTypes.FLOAT
        self.F2 =  0.0

        self._attributes['F4'] = AttributeTypes.FLOAT
        self.F4 =  0.0

        self._attributes['F6'] = AttributeTypes.FLOAT
        self.F6 =  0.0

        self._attributes['approximation'] = AttributeTypes.STRING
        self.approximation = "none"

        self._attributes['parametrisation'] = AttributeTypes.STRING
        self.parametrisation = "slater-condon"




class HlocStruct(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._objects['one_body'] = ('CTQMC_internal', 'ComplexSquareMatrix')
        self.one_body = ComplexSquareMatrix()
        self._objects['two_body'] = ('CTQMC_internal', 'TwoBodyStruct')
        self.two_body = TwoBodyStruct()



class HybridisationStruct(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['dim'] = AttributeTypes.INT
        self.dim =  0

        self._attributes['functions'] = AttributeTypes.STRING
        self.functions = "Hyb.json"

        self._datasets['matrix'] = (np.int32, '(int(self.dim), int(self.dim))' )
        self.matrix : NDArray[(Any, Any), Int32]
        self.matrix = None



class WormStruct(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['basis'] = AttributeTypes.STRING
        self.basis = "matsubara"

        self._attributes['meas'] = AttributeTypes.STRING
        self.meas = ""

        self._attributes['diagonal'] = AttributeTypes.BOOLEAN
        self.diagonal = False




class OneTimeWormStruct(WormStruct):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        WormStruct.__InitMetadata__(self, True)

        self._attributes['cutoff'] = AttributeTypes.INT
        self.cutoff =  50



class MultTimeWormStruct(WormStruct):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        WormStruct.__InitMetadata__(self, True)

        self._attributes['fermion_cutoff'] = AttributeTypes.INT
        self.fermion_cutoff =  50
        self._attributes['boson_cutoff'] = AttributeTypes.INT
        self.boson_cutoff =  10



class AnalyticalContinuation(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['ntau'] = AttributeTypes.INT
        self.ntau =  400
        self._attributes['nf'] = AttributeTypes.INT
        self.nf =  1000



class PartitionSpaceStruct(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['green_matsubara_cutoff'] = AttributeTypes.FLOAT
        self.green_matsubara_cutoff =  0.0

        self._objects['quantum_numbers'] = ('CTQMC_internal', 'QuantumNumbersPlaceHolder')
        self.quantum_numbers = QuantumNumbersPlaceHolder()
        self._objects['observables'] = ('CTQMC_internal', 'ObservablesPlaceHolder')
        self.observables = ObservablesPlaceHolder()
        self._attributes['susceptibility_cutoff'] = AttributeTypes.INT
        self.susceptibility_cutoff =  10
        self._attributes['susceptibility_tail'] = AttributeTypes.INT
        self.susceptibility_tail =  100
        self._attributes['quantum_number_susceptibility'] = AttributeTypes.BOOLEAN
        self.quantum_number_susceptibility = False

        self._attributes['occupation_susceptibility'] = AttributeTypes.BOOLEAN
        self.occupation_susceptibility = False

        self._attributes['occupation_susceptibility_bulla'] = AttributeTypes.BOOLEAN
        self.occupation_susceptibility_bulla = False

        self._attributes['occupation_susceptibility_direct'] = AttributeTypes.BOOLEAN
        self.occupation_susceptibility_direct = False

        self._attributes['green_bulla'] = AttributeTypes.BOOLEAN
        self.green_bulla = False

        self._attributes['density_matrix_precise'] = AttributeTypes.BOOLEAN
        self.density_matrix_precise = False

        self._attributes['sweepA'] = AttributeTypes.INT
        self.sweepA =  50
        self._attributes['storeA'] = AttributeTypes.INT
        self.storeA =  100
        self._attributes['sweepB'] = AttributeTypes.INT
        self.sweepB =  250
        self._attributes['storeB'] = AttributeTypes.INT
        self.storeB =  20
        self._attributes['green_basis'] = AttributeTypes.STRING
        self.green_basis = "matsubara"

        self._attributes['probabilities'] = AttributeTypes.STRING
        self.probabilities = ""




class Parameters(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['beta'] = AttributeTypes.FLOAT
        self.beta =  0.0

        self._attributes['mu'] = AttributeTypes.FLOAT
        self.mu =  0.0

        self._attributes['complex'] = AttributeTypes.BOOLEAN
        self.complex = False

        self._objects['basis'] = ('CTQMC_internal', 'BasisStruct')
        self.basis = BasisStruct()
        self._objects['hloc'] = ('CTQMC_internal', 'HlocStruct')
        self.hloc = HlocStruct()
        self._objects['hybridisation'] = ('CTQMC_internal', 'HybridisationStruct')
        self.hybridisation = HybridisationStruct()
        self._objects['partition'] = ('CTQMC_internal', 'PartitionSpaceStruct')
        self.partition = PartitionSpaceStruct()
        self._objects['analytical_continuation'] = ('CTQMC_internal', 'AnalyticalContinuation')
        self.analytical_continuation = AnalyticalContinuation()
        self._objects['green'] = ('CTQMC_internal', 'OneTimeWormStruct')
        self.green = OneTimeWormStruct()
        self._objects['susc_ph'] = ('CTQMC_internal', 'OneTimeWormStruct')
        self.susc_ph = OneTimeWormStruct()
        self._objects['susc_pp'] = ('CTQMC_internal', 'OneTimeWormStruct')
        self.susc_pp = OneTimeWormStruct()
        self._objects['hedin_ph'] = ('CTQMC_internal', 'MultTimeWormStruct')
        self.hedin_ph = MultTimeWormStruct()
        self._objects['hedin_pp'] = ('CTQMC_internal', 'MultTimeWormStruct')
        self.hedin_pp = MultTimeWormStruct()
        self._objects['vertex'] = ('CTQMC_internal', 'MultTimeWormStruct')
        self.vertex = MultTimeWormStruct()
        self._attributes['measurement_steps'] = AttributeTypes.INT
        self.measurement_steps =  0
        self._attributes['thermalisation_steps'] = AttributeTypes.INT
        self.thermalisation_steps =  0
        self._attributes['measurement_time'] = AttributeTypes.INT
        self.measurement_time =  20
        self._attributes['thermalisation_time'] = AttributeTypes.INT
        self.thermalisation_time =  5
        self._attributes['sim_per_device'] = AttributeTypes.INT
        self.sim_per_device =  25
        self._attributes['quad_insert'] = AttributeTypes.BOOLEAN
        self.quad_insert = False

        self._attributes['all_errors'] = AttributeTypes.BOOLEAN
        self.all_errors = False

        self._attributes['error'] = AttributeTypes.STRING
        self.error = "parallel"

        self._attributes['trunc_dim'] = AttributeTypes.INT
        self.trunc_dim =  0

        self._attributes['interaction_truncation'] = AttributeTypes.INT
        self.interaction_truncation =  0




# this is the end of the generated file