'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict


class Control(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['iter_dft'] = AttributeTypes.INT
        self.iter_dft =  80
        self._attributes['iter_hf'] = AttributeTypes.INT
        self.iter_hf =  0
        self._attributes['iter_gw'] = AttributeTypes.INT
        self.iter_gw =  0
        self._attributes['iter_qp'] = AttributeTypes.INT
        self.iter_qp =  0
        self._attributes['iter_psi'] = AttributeTypes.INT
        self.iter_psi =  0
        self._attributes['iter_bsp'] = AttributeTypes.INT
        self.iter_bsp =  0
        self._attributes['restart_begin'] = AttributeTypes.INT
        self.restart_begin =  0
        self._attributes['restart_end'] = AttributeTypes.INT
        self.restart_end =  0
        self._attributes['admix'] = AttributeTypes.FLOAT
        self.admix = 0.050000
        self._attributes['adspin'] = AttributeTypes.FLOAT
        self.adspin = 0.700000
        self._attributes['adm_gw'] = AttributeTypes.FLOAT
        self.adm_gw = 0.300000
        self._attributes['acc_it_gw'] = AttributeTypes.FLOAT
        self.acc_it_gw = 0.600000
        self._attributes['iexch'] = AttributeTypes.INT
        self.iexch =  5
        self._attributes['scal_spin'] = AttributeTypes.FLOAT
        self.scal_spin = 1.000000
        self._attributes['psi_fncl_use'] = AttributeTypes.INT
        self.psi_fncl_use =  0
        self._attributes['nproc_tau'] = AttributeTypes.INT
        self.nproc_tau =  1
        self._attributes['nproc_k'] = AttributeTypes.INT
        self.nproc_k =  1
        self._attributes['nproc_pbr'] = AttributeTypes.INT
        self.nproc_pbr =  1
        self._attributes['irel'] = AttributeTypes.INT
        self.irel =  1
        self._attributes['irel_core'] = AttributeTypes.INT
        self.irel_core =  1
        self._attributes['clight'] = AttributeTypes.FLOAT
        self.clight = 274.074000
        self._attributes['rel_interst'] = AttributeTypes.BOOLEAN
        self.rel_interst = False

        self._attributes['temperature'] = AttributeTypes.FLOAT
        self.temperature = 900.000000



class AtomicData(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['txtel'] = AttributeTypes.STRING
        self.txtel = ""

        self._attributes['z'] = AttributeTypes.FLOAT
        self.z =  0.0

        self._attributes['z_dop'] = AttributeTypes.FLOAT
        self.z_dop = 0.000000
        self._attributes['magn_shift'] = AttributeTypes.FLOAT
        self.magn_shift = 0.000000
        self._attributes['smt'] = AttributeTypes.FLOAT
        self.smt =  0.0

        self._attributes['h'] = AttributeTypes.FLOAT
        self.h = 0.012000
        self._attributes['nrad'] = AttributeTypes.INT
        self.nrad =  1200
        self._attributes['lmb'] = AttributeTypes.INT
        self.lmb =  0

        self._attributes['lmpb'] = AttributeTypes.INT
        self.lmpb =  0

        self._attributes['maxntle'] = AttributeTypes.INT
        self.maxntle =  10
        self._datasets['lim_pb_mt'] = (np.int32, '(int(self.lmpb- (0) + 1),)' )
        self.lim_pb_mt : NDArray[(Any,), Int32]
        self.lim_pb_mt = None
        self._datasets['lim_pb_mt_red'] = (np.int32, '(int(self.lmpb- (0) + 1),)' )
        self.lim_pb_mt_red : NDArray[(Any,), Int32]
        self.lim_pb_mt_red = None
        self._datasets['ntle'] = (np.int32, '(int(self.lmb- (0) + 1),)' )
        self.ntle : NDArray[(Any,), Int32]
        self.ntle = None
        self._datasets['augm'] = (np.int32, '(int(self.maxntle), int(self.lmb- (0) + 1))' )
        self.augm : NDArray[(Any, Any), Int32]
        self.augm = None
        self._datasets['atoc'] = (np.float64, '(int(self.maxntle), int(self.lmb- (0) + 1))' )
        self.atoc : NDArray[(Any, Any), Float64]
        self.atoc = None
        self._datasets['ptnl'] = (np.float64, '(int(self.maxntle), int(self.lmb- (0) + 1))' )
        self.ptnl : NDArray[(Any, Any), Float64]
        self.ptnl = None
        self._datasets['correlated'] = (np.int32, '(int(self.maxntle), int(self.lmb- (0) + 1))' )
        self.correlated : NDArray[(Any, Any), Int32]
        self.correlated = None
        self._datasets['idmd'] = (np.float64, '(int(self.maxntle), int(self.lmb- (0) + 1))' )
        self.idmd : NDArray[(Any, Any), Float64]
        self.idmd = None



class Structure(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['par'] = AttributeTypes.FLOAT
        self.par = 1.000000
        self._attributes['b_a'] = AttributeTypes.FLOAT
        self.b_a = 1.000000
        self._attributes['c_a'] = AttributeTypes.FLOAT
        self.c_a = 1.000000
        self._attributes['nsort'] = AttributeTypes.INT
        self.nsort =  0

        self._attributes['natom'] = AttributeTypes.INT
        self.natom =  0

        self._attributes['istruc'] = AttributeTypes.INT
        self.istruc =  1
        self._datasets['a'] = (np.float64, '(int(3),)' )
        self.a : NDArray[(Any,), Float64]
        self.a = None
        self._datasets['b'] = (np.float64, '(int(3),)' )
        self.b : NDArray[(Any,), Float64]
        self.b = None
        self._datasets['c'] = (np.float64, '(int(3),)' )
        self.c : NDArray[(Any,), Float64]
        self.c = None
        self._datasets['tau'] = (np.float64, '(int(3), int(self.natom))' )
        self.tau : NDArray[(Any, Any), Float64]
        self.tau = None
        self._datasets['isA'] = (np.int32, '(int(self.natom),)' )
        self.isA : NDArray[(Any,), Int32]
        self.isA = None
        self._objars['ad'] = ( ('FlapwMBPT', 'AtomicData'), '(int(self.nsort),)') 
        self.ad : NDarray[(Any,), FlapwMBPT.AtomicData]
        self.ad = None



class Generator(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['sign'] = AttributeTypes.INT
        self.sign =  1
        self._attributes['kind'] = AttributeTypes.STRING
        self.kind = "R"

        self._datasets['v'] = (np.float64, '(int(3),)' )
        self.v : NDArray[(Any,), Float64]
        self.v = None
        self._attributes['n'] = AttributeTypes.INT
        self.n =  0

        self._datasets['t'] = (np.float64, '(int(3),)' )
        self.t : NDArray[(Any,), Float64]
        self.t = None



class SpacegroupGens(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_generators'] = AttributeTypes.INT
        self.num_generators =  0

        self._objars['gens'] = ( ('FlapwMBPT', 'Generator'), '(int(self.num_generators),)') 
        self.gens : NDarray[(Any,), FlapwMBPT.Generator]
        self.gens = None
        self._datasets['rots'] = (np.float64, '(int(3), int(3), int(self.num_generators))' )
        self.rots : NDArray[(Any, Any, Any), Float64]
        self.rots = None



class KGrid(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._datasets['ndiv'] = (np.int32, '(int(3),)' )
        self.ndiv : NDArray[(Any,), Int32]
        self.ndiv = None
        self._datasets['ndiv_c'] = (np.int32, '(int(3),)' )
        self.ndiv_c : NDArray[(Any,), Int32]
        self.ndiv_c = None
        self._attributes['metal'] = AttributeTypes.BOOLEAN
        self.metal = False

        self._attributes['n_k_div'] = AttributeTypes.INT
        self.n_k_div =  9
        self._attributes['k_line'] = AttributeTypes.STRING
        self.k_line = "010"




class RealSpaceMeshes(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._datasets['nrdiv'] = (np.int32, '(int(3),)' )
        self.nrdiv : NDArray[(Any,), Int32]
        self.nrdiv = None
        self._datasets['mdiv'] = (np.int32, '(int(3),)' )
        self.mdiv : NDArray[(Any,), Int32]
        self.mdiv = None
        self._datasets['nrdiv_red'] = (np.int32, '(int(3),)' )
        self.nrdiv_red : NDArray[(Any,), Int32]
        self.nrdiv_red = None



class Basis(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['cut_lapw_ratio'] = AttributeTypes.FLOAT
        self.cut_lapw_ratio = 0.610000
        self._attributes['cut_pb_ratio'] = AttributeTypes.FLOAT
        self.cut_pb_ratio = 0.980000
        self._attributes['eps_pb'] = AttributeTypes.FLOAT
        self.eps_pb = 0.001000



class Zones(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['nbndf'] = AttributeTypes.INT
        self.nbndf =  0
        self._datasets['nbndf_bnd'] = (np.int32, '(int(2),)' )
        self.nbndf_bnd : NDArray[(Any,), Int32]
        self.nbndf_bnd = None



class BandPlot(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['n_k_div'] = AttributeTypes.INT
        self.n_k_div =  10



class DensityOfStates(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['emindos'] = AttributeTypes.FLOAT
        self.emindos = 2.000000
        self._attributes['emaxdos'] = AttributeTypes.FLOAT
        self.emaxdos = 2.000000
        self._attributes['ndos'] = AttributeTypes.INT
        self.ndos =  600
        self._attributes['e_small'] = AttributeTypes.FLOAT
        self.e_small = 0.005000



class MultiSCF(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['v_v0'] = AttributeTypes.FLOAT
        self.v_v0 = 1.000000



class Magnetism(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['b_extval'] = AttributeTypes.FLOAT
        self.b_extval = 0.000000
        self._attributes['iter_h_ext'] = AttributeTypes.INT
        self.iter_h_ext =  0
        self._datasets['b_ext'] = (np.float64, '(int(3),)' )
        self.b_ext : NDArray[(Any,), Float64]
        self.b_ext = None



class Coulomb(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['eps_coul'] = AttributeTypes.FLOAT
        self.eps_coul = 0.000100



class TauMesh(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['n_tau'] = AttributeTypes.INT
        self.n_tau =  46
        self._attributes['exp_tau_gw'] = AttributeTypes.FLOAT
        self.exp_tau_gw = 4.000000



class OmegaMesh(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['n_omega_exa'] = AttributeTypes.INT
        self.n_omega_exa =  10
        self._attributes['n_omega_asy'] = AttributeTypes.INT
        self.n_omega_asy =  36
        self._attributes['omega_max'] = AttributeTypes.FLOAT
        self.omega_max = 50.000000



class NuMesh(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['n_nu_exa'] = AttributeTypes.INT
        self.n_nu_exa =  10
        self._attributes['n_nu_geom'] = AttributeTypes.INT
        self.n_nu_geom =  30
        self._attributes['n_nu_asy'] = AttributeTypes.INT
        self.n_nu_asy =  6
        self._attributes['nu_geom'] = AttributeTypes.FLOAT
        self.nu_geom = 100.000000
        self._attributes['nu_max'] = AttributeTypes.FLOAT
        self.nu_max = 400.000000



class AFMSymmetry(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['magmom'] = AttributeTypes.FLOAT
        self.magmom =  0.0

        self._attributes['site'] = AttributeTypes.INT
        self.site =  0

        self._attributes['opposite_site'] = AttributeTypes.INT
        self.opposite_site =  0

        self._datasets['op'] = (np.float64, '(int(4), int(4))' )
        self.op : NDArray[(Any, Any), Float64]
        self.op = None



class RhobustaExtra(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['has_magnetic_field'] = AttributeTypes.BOOLEAN
        self.has_magnetic_field = False

        self._attributes['is_corr_spin_polarized'] = AttributeTypes.BOOLEAN
        self.is_corr_spin_polarized = False

        self._attributes['is_dft_spin_polarized'] = AttributeTypes.BOOLEAN
        self.is_dft_spin_polarized = False

        self._attributes['local_off_diagonals'] = AttributeTypes.BOOLEAN
        self.local_off_diagonals = False

        self._attributes['simplify_diagonal'] = AttributeTypes.BOOLEAN
        self.simplify_diagonal = False




class AFMExtra(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_afm'] = AttributeTypes.INT
        self.num_afm =  0
        self._objars['afm_symm'] = ( ('FlapwMBPT', 'AFMSymmetry'), '(int(self.num_afm),)') 
        self.afm_symm : NDarray[(Any,), FlapwMBPT.AFMSymmetry]
        self.afm_symm = None



class Input(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['text'] = AttributeTypes.STRING
        self.text = ""

        self._attributes['allfile'] = AttributeTypes.STRING
        self.allfile = ""

        self._objects['ctrl'] = ('FlapwMBPT', 'Control')
        self.ctrl = Control()
        self._objects['strct'] = ('FlapwMBPT', 'Structure')
        self.strct = Structure()
        self._objects['sgg'] = ('FlapwMBPT', 'SpacegroupGens')
        self.sgg = SpacegroupGens()
        self._objects['rsm'] = ('FlapwMBPT', 'RealSpaceMeshes')
        self.rsm = RealSpaceMeshes()
        self._objects['bas'] = ('FlapwMBPT', 'Basis')
        self.bas = Basis()
        self._objects['zns'] = ('FlapwMBPT', 'Zones')
        self.zns = Zones()
        self._objects['bp'] = ('FlapwMBPT', 'BandPlot')
        self.bp = BandPlot()
        self._objects['dos'] = ('FlapwMBPT', 'DensityOfStates')
        self.dos = DensityOfStates()
        self._objects['kg'] = ('FlapwMBPT', 'KGrid')
        self.kg = KGrid()
        self._objects['mscf'] = ('FlapwMBPT', 'MultiSCF')
        self.mscf = MultiSCF()
        self._objects['mag'] = ('FlapwMBPT', 'Magnetism')
        self.mag = Magnetism()
        self._objects['coul'] = ('FlapwMBPT', 'Coulomb')
        self.coul = Coulomb()
        self._objects['tmesh'] = ('FlapwMBPT', 'TauMesh')
        self.tmesh = TauMesh()
        self._objects['omesh'] = ('FlapwMBPT', 'OmegaMesh')
        self.omesh = OmegaMesh()
        self._objects['nmesh'] = ('FlapwMBPT', 'NuMesh')
        self.nmesh = NuMesh()



class ExpansionSizes(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['npnt'] = AttributeTypes.INT
        self.npnt =  0

        self._attributes['nbndf'] = AttributeTypes.INT
        self.nbndf =  0

        self._attributes['nspin'] = AttributeTypes.INT
        self.nspin =  0

        self._attributes['nbasmpw'] = AttributeTypes.INT
        self.nbasmpw =  0

        self._attributes['nfun'] = AttributeTypes.INT
        self.nfun =  0




class Sizes(ExpansionSizes):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ExpansionSizes.__InitMetadata__(self, True)

        self._attributes['ncormax'] = AttributeTypes.INT
        self.ncormax =  0

        self._attributes['nspin_0'] = AttributeTypes.INT
        self.nspin_0 =  0

        self._attributes['nsort'] = AttributeTypes.INT
        self.nsort =  0

        self._attributes['maxmtcor'] = AttributeTypes.INT
        self.maxmtcor =  0

        self._attributes['maxel'] = AttributeTypes.INT
        self.maxel =  0

        self._attributes['natom'] = AttributeTypes.INT
        self.natom =  0

        self._attributes['maxwf'] = AttributeTypes.INT
        self.maxwf =  0

        self._attributes['nlmb'] = AttributeTypes.INT
        self.nlmb =  0

        self._attributes['maxntle'] = AttributeTypes.INT
        self.maxntle =  0

        self._attributes['maxnrad'] = AttributeTypes.INT
        self.maxnrad =  0

        self._attributes['has_gf0'] = AttributeTypes.BOOLEAN
        self.has_gf0 = False




class ElDensity(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['maxmt'] = AttributeTypes.INT
        self.maxmt =  0

        self._attributes['nspin_0'] = AttributeTypes.INT
        self.nspin_0 =  1
        self._attributes['maxmtb'] = AttributeTypes.INT
        self.maxmtb =  1
        self._attributes['nplwro'] = AttributeTypes.INT
        self.nplwro =  1
        self._attributes['nrmax'] = AttributeTypes.INT
        self.nrmax =  0
        self._attributes['nsort'] = AttributeTypes.INT
        self.nsort =  0
        self._datasets['ro'] = (np.float64, '(int(self.maxmt),)' )
        self.ro : NDArray[(Any,), Float64]
        self.ro = None
        self._datasets['rointr'] = (np.complex128, '(int(self.nplwro), int(self.nspin_0))' )
        self.rointr : NDArray[(Any, Any), Complex128]
        self.rointr = None
        self._datasets['spmt'] = (np.float64, '(int(self.maxmtb),)' )
        self.spmt : NDArray[(Any,), Float64]
        self.spmt = None
        self._datasets['spintr'] = (np.complex128, '(int(3), int(self.nplwro))' )
        self.spintr : NDArray[(Any, Any), Complex128]
        self.spintr = None



class ElDensityWithCore(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['nrmax'] = AttributeTypes.INT
        self.nrmax =  0
        self._attributes['nsort'] = AttributeTypes.INT
        self.nsort =  0
        self._attributes['nspin_0'] = AttributeTypes.INT
        self.nspin_0 =  1
        self._datasets['ro_core'] = (np.complex128, '(int(self.nrmax- (0) + 1), int(self.nsort), int(self.nspin_0))' )
        self.ro_core : NDArray[(Any, Any, Any), Complex128]
        self.ro_core = None



class Image(Sizes):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Sizes.__InitMetadata__(self, True)

        self._attributes['chem_pot'] = AttributeTypes.FLOAT
        self.chem_pot =  0.0

        self._datasets['e_core'] = (np.float64, '(int(self.ncormax), int(self.nspin_0), int(self.nsort))' )
        self.e_core : NDArray[(Any, Any, Any), Float64]
        self.e_core = None
        self._datasets['pcor'] = (np.float64, '(int(self.maxmtcor),)' )
        self.pcor : NDArray[(Any,), Float64]
        self.pcor = None
        self._datasets['qcor'] = (np.float64, '(int(self.maxmtcor),)' )
        self.qcor : NDArray[(Any,), Float64]
        self.qcor = None
        self._datasets['n_bnd'] = (np.int32, '(int(self.npnt), int(self.nspin_0))' )
        self.n_bnd : NDArray[(Any, Any), Int32]
        self.n_bnd = None
        self._datasets['e_bnd'] = (np.float64, '(int(self.nbndf), int(self.npnt), int(self.nspin_0))' )
        self.e_bnd : NDArray[(Any, Any, Any), Float64]
        self.e_bnd = None
        self._datasets['g_loc_0'] = (np.complex128, '(int(self.maxel), int(self.maxel), int(self.natom), int(self.nspin_0))' )
        self.g_loc_0 : NDArray[(Any, Any, Any, Any), Complex128]
        self.g_loc_0 = None
        self._datasets['gfun'] = (np.float64, '(int(self.maxwf), int(self.nspin))' )
        self.gfun : NDArray[(Any, Any), Float64]
        self.gfun = None
        self._datasets['p_f'] = (np.float64, '(int(self.maxnrad- (0) + 1), int(self.maxntle), int(self.nlmb), int(self.nsort), int(self.nspin))' )
        self.p_f : NDArray[(Any, Any, Any, Any, Any), Float64]
        self.p_f = None
        self._datasets['pd_f'] = (np.float64, '(int(self.maxnrad- (0) + 1), int(self.maxntle), int(self.nlmb), int(self.nsort), int(self.nspin))' )
        self.pd_f : NDArray[(Any, Any, Any, Any, Any), Float64]
        self.pd_f = None
        self._datasets['pd2_f'] = (np.float64, '(int(self.maxnrad- (0) + 1), int(self.maxntle), int(self.nlmb), int(self.nsort), int(self.nspin))' )
        self.pd2_f : NDArray[(Any, Any, Any, Any, Any), Float64]
        self.pd2_f = None
        self._datasets['q_f'] = (np.float64, '(int(self.maxnrad- (0) + 1), int(self.maxntle), int(self.nlmb), int(self.nsort), int(self.nspin))' )
        self.q_f : NDArray[(Any, Any, Any, Any, Any), Float64]
        self.q_f = None
        self._datasets['qd_f'] = (np.float64, '(int(self.maxnrad- (0) + 1), int(self.maxntle), int(self.nlmb), int(self.nsort), int(self.nspin))' )
        self.qd_f : NDArray[(Any, Any, Any, Any, Any), Float64]
        self.qd_f = None
        self._datasets['qd2_f'] = (np.float64, '(int(self.maxnrad- (0) + 1), int(self.maxntle), int(self.nlmb), int(self.nsort), int(self.nspin))' )
        self.qd2_f : NDArray[(Any, Any, Any, Any, Any), Float64]
        self.qd2_f = None
        self._datasets['gfund'] = (np.float64, '(int(self.maxwf), int(self.nspin))' )
        self.gfund : NDArray[(Any, Any), Float64]
        self.gfund = None
        self._datasets['eny'] = (np.float64, '(int(self.maxntle), int(self.nlmb), int(self.nsort), int(self.nspin_0))' )
        self.eny : NDArray[(Any, Any, Any, Any), Float64]
        self.eny = None
        self._datasets['ffsmt'] = (np.float64, '(int(2), int(2), int(self.maxntle), int(self.maxntle), int(self.nlmb), int(self.nsort), int(self.nspin))' )
        self.ffsmt : NDArray[(Any, Any, Any, Any, Any, Any, Any), Float64]
        self.ffsmt = None
        self._objects['rho'] = ('FlapwMBPT', 'ElDensity')
        self.rho = ElDensity()



class FullEVB(ExpansionSizes):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ExpansionSizes.__InitMetadata__(self, True)

        self._datasets['evb'] = (np.complex128, '(int(self.nbasmpw), int(self.nbndf), int(self.npnt), int(self.nspin))' )
        self.evb : NDArray[(Any, Any, Any, Any), Complex128]
        self.evb = None



class FullZB(ExpansionSizes):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ExpansionSizes.__InitMetadata__(self, True)

        self._datasets['zb'] = (np.complex128, '(int(self.nfun), int(self.nbndf), int(self.npnt), int(self.nspin))' )
        self.zb : NDArray[(Any, Any, Any, Any), Complex128]
        self.zb = None



class FullGF0(ExpansionSizes):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ExpansionSizes.__InitMetadata__(self, True)

        self._datasets['gf0'] = (np.complex128, '(int(self.nbndf), int(self.nbndf), int(self.npnt), int(self.nspin))' )
        self.gf0 : NDArray[(Any, Any, Any, Any), Complex128]
        self.gf0 = None



# this is the end of the generated file