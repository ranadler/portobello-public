'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict

from portobello.generated.LAPW import *

class OrbitalLabels(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['short_label'] = AttributeTypes.STRING
        self.short_label = ""

        self._attributes['latex_label'] = AttributeTypes.STRING
        self.latex_label = ""




class AtomicShell(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['atom_number'] = AttributeTypes.INT
        self.atom_number =  0

        self._attributes['l'] = AttributeTypes.INT
        self.l =  0

        self._attributes['lapw_kind'] = AttributeTypes.INT
        self.lapw_kind =  0

        self._attributes['num_equivalents'] = AttributeTypes.INT
        self.num_equivalents =  0

        self._attributes['num_anti_equivalents'] = AttributeTypes.INT
        self.num_anti_equivalents =  0

        self._attributes['cluster_size'] = AttributeTypes.INT
        self.cluster_size =  1
        self._attributes['is_correlated'] = AttributeTypes.BOOLEAN
        self.is_correlated = False

        self._attributes['nrel'] = AttributeTypes.INT
        self.nrel =  1
        self._attributes['dim'] = AttributeTypes.INT
        self.dim =  0

        self._attributes['start_index'] = AttributeTypes.INT
        self.start_index =  0
        self._attributes['end_index'] = AttributeTypes.INT
        self.end_index =  0
        self._datasets['raw_rep'] = (np.int32, '(int(self.dim), int(self.dim))' )
        self.raw_rep : NDArray[(Any, Any), Int32]
        self.raw_rep = None
        self._datasets['rep'] = (np.int32, '(int(self.dim), int(self.dim))' )
        self.rep : NDArray[(Any, Any), Int32]
        self.rep = None
        self._datasets['basis'] = (np.complex128, '(int(self.dim), int(self.dim))' )
        self.basis : NDArray[(Any, Any), Complex128]
        self.basis = None
        self._objars['labels'] = ( ('atomic_orbitals', 'OrbitalLabels'), '(int(self.dim),)') 
        self.labels : NDarray[(Any,), atomic_orbitals.OrbitalLabels]
        self.labels = None
        self._datasets['lapw_indices'] = (np.int32, '(int(self.dim * self.num_equivalents + self.dim * self.num_anti_equivalents),)' )
        self.lapw_indices : NDArray[(Any,), Int32]
        self.lapw_indices = None
        self._datasets['det_signs'] = (np.int32, '(int(self.num_equivalents + self.num_anti_equivalents),)' )
        self.det_signs : NDArray[(Any,), Int32]
        self.det_signs = None
        self._datasets['rotations'] = (np.float64, '(int(self.num_equivalents + self.num_anti_equivalents), int(self.dim), int(self.dim))' )
        self.rotations : NDArray[(Any, Any, Any), Float64]
        self.rotations = None
        self._datasets['translations'] = (np.float64, '(int(3), int(self.num_equivalents + self.num_anti_equivalents))' )
        self.translations : NDArray[(Any, Any), Float64]
        self.translations = None



class SelectedOrbitals(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_muffin_orbs'] = AttributeTypes.INT
        self.num_muffin_orbs =  0

        self._attributes['num_shells'] = AttributeTypes.INT
        self.num_shells =  0

        self._attributes['total_dim'] = AttributeTypes.INT
        self.total_dim =  0

        self._attributes['subspace_expr'] = AttributeTypes.STRING
        self.subspace_expr = ""

        self._attributes['exclusion_margin'] = AttributeTypes.FLOAT
        self.exclusion_margin = 2.000000
        self._datasets['V'] = (np.complex128, '(int(self.num_muffin_orbs), int(self.total_dim))' )
        self.V : NDArray[(Any, Any), Complex128]
        self.V = None
        self._objars['shells'] = ( ('atomic_orbitals', 'AtomicShell'), '(int(self.num_shells),)') 
        self.shells : NDarray[(Any,), atomic_orbitals.AtomicShell]
        self.shells = None



class OrbitalsBandProjector(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['num_k_all'] = AttributeTypes.INT
        self.num_k_all =  0

        self._attributes['num_orbs'] = AttributeTypes.INT
        self.num_orbs =  0

        self._attributes['min_band'] = AttributeTypes.INT
        self.min_band =  0

        self._attributes['max_band'] = AttributeTypes.INT
        self.max_band =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  1
        self._datasets['P'] = (np.complex128, '(int(self.max_band - self.min_band + 1), int(self.num_orbs), int(self.num_k_all), int(self.num_si))' )
        self.P : NDArray[(Any, Any, Any, Any), Complex128]
        self.P = None



class OrbitalMTProjector(Expansion):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Expansion.__InitMetadata__(self, True)




# this is the end of the generated file