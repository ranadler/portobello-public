'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict

from portobello.generated.PEScf import *

class GState(SolverState):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        SolverState.__InitMetadata__(self, True)

        self._attributes['num_k_all'] = AttributeTypes.INT
        self.num_k_all =  0

        self._datasets['Hloc'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.Hloc : NDArray[(Any, Any, Any), Complex128]
        self.Hloc = None
        self._datasets['R'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.R : NDArray[(Any, Any, Any), Complex128]
        self.R = None
        self._datasets['Lambda'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.Lambda : NDArray[(Any, Any, Any), Complex128]
        self.Lambda = None
        self._datasets['Z'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.Z : NDArray[(Any, Any, Any), Complex128]
        self.Z = None
        self._attributes['success'] = AttributeTypes.INT
        self.success =  0

        self._attributes['egutz'] = AttributeTypes.FLOAT
        self.egutz = 0.000000



class GProblem(SolverProblem):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        SolverProblem.__InitMetadata__(self, True)

        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0

        self._attributes['num_k_irr'] = AttributeTypes.INT
        self.num_k_irr =  0

        self._attributes['num_bands'] = AttributeTypes.INT
        self.num_bands =  0

        self._attributes['norb2'] = AttributeTypes.INT
        self.norb2 =  0

        self._attributes['gutz_name'] = AttributeTypes.STRING
        self.gutz_name = ""

        self._datasets['weight'] = (np.float64, '(int(self.num_k),)' )
        self.weight : NDArray[(Any,), Float64]
        self.weight = None
        self._datasets['Hk'] = (np.complex128, '(int(self.num_k), int(self.num_bands), int(self.num_bands), int(self.num_si))' )
        self.Hk : NDArray[(Any, Any, Any, Any), Complex128]
        self.Hk = None
        self._datasets['HkBands'] = (np.complex128, '(int(self.num_k), int(self.num_bands), int(self.num_bands), int(self.num_si))' )
        self.HkBands : NDArray[(Any, Any, Any, Any), Complex128]
        self.HkBands = None
        self._datasets['UTensor'] = (np.complex128, '(int(self.norb2), int(self.norb2), int(self.norb2), int(self.norb2))' )
        self.UTensor : NDArray[(Any, Any, Any, Any), Complex128]
        self.UTensor = None



class GSolution(SolverSolution):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        SolverSolution.__InitMetadata__(self, True)

        self._attributes['dim'] = AttributeTypes.INT
        self.dim =  0

        self._attributes['num_si'] = AttributeTypes.INT
        self.num_si =  0

        self._datasets['R'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.R : NDArray[(Any, Any, Any), Complex128]
        self.R = None
        self._datasets['Lambda'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.Lambda : NDArray[(Any, Any, Any), Complex128]
        self.Lambda = None
        self._datasets['Z'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.Z : NDArray[(Any, Any, Any), Complex128]
        self.Z = None
        self._datasets['RhoImp'] = (np.complex128, '(int(self.dim), int(self.dim), int(self.num_si))' )
        self.RhoImp : NDArray[(Any, Any, Any), Complex128]
        self.RhoImp = None
        self._attributes['energyImp'] = AttributeTypes.FLOAT
        self.energyImp =  0.0

        self._attributes['success'] = AttributeTypes.INT
        self.success =  0




# this is the end of the generated file