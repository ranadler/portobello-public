'''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran

from portobello.generated.base_types import *

class ExampleInput(ComputationInput):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ComputationInput.__InitMetadata__(self, True)

        self._attributes['full_path'] = AttributeTypes.STRING
        self.full_path = ""



    @classmethod
    def SomeBasicCalculation(cls, selfpath, inp):
        fort = Fortran.Instance()
        return fort.CallWrapper("example","wannier_interpolate", "SomeBasicCalculation",
            selfpath, c_int(inp))

class Orbitals(ComputationOutput):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        ComputationOutput.__InitMetadata__(self, True)

        self._attributes['num_atoms'] = AttributeTypes.INT
        self.num_atoms =  0

        self._attributes['num_bands'] = AttributeTypes.INT
        self.num_bands =  0

        self._attributes['num_orbs'] = AttributeTypes.INT
        self.num_orbs =  0

        self._datasets['nearest_atom'] = (np.int32, '(int(self.num_orbs),)' )
        self.nearest_atom = None
        self._datasets['distance_to_nearest_atom'] = (np.float64, '(int(3), int(self.num_orbs))' )
        self.distance_to_nearest_atom = None
        self._datasets['centers'] = (np.float64, '(int(3), int(self.num_orbs))' )
        self.centers = None



class ExampleOutput(Orbitals):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Orbitals.__InitMetadata__(self, True)

        self._attributes['num_k'] = AttributeTypes.INT
        self.num_k =  0

        self._objects['input'] = ('example', 'ExampleInput')
        self.input = ExampleInput()
        self._datasets['k_lat'] = (np.float64, '(int(3), int(self.num_k))' )
        self.k_lat = None
        self._datasets['V'] = (np.complex128, '(int(self.num_bands), int(self.num_orbs), int(self.num_k))' )
        self.V = None
        self._objars['inp_per_orb'] = ( ('example', 'ExampleInput'), '(int(self.num_orbs), int(self.num_atoms))') 
        self.inp_per_orb = None


    @classmethod
    def Compute(cls, selfpath, low_energy, high_energy, i1, other):
        fort = Fortran.Instance()
        return fort.CallWrapper("example","example_module", "Compute",
            selfpath, c_float(low_energy), c_float(high_energy), c_int(i1), other)

class Simple(Persistent):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        Persistent.__InitMetadata__(self, True)

        self._attributes['m'] = AttributeTypes.INT
        self.m =  0

        self._attributes['n'] = AttributeTypes.INT
        self.n =  0

        self._datasets['d'] = (np.float64, '(int(self.m), int(self.n))' )
        self.d = None


    @classmethod
    def SimpleCall(cls, selfpath):
        fort = Fortran.Instance()
        return fort.CallWrapper("example","example_module", "SimpleCall",
            selfpath)

# this is the end of the generated file