'''
Created on Jan 29, 2020

@author: adler
'''
   
import numpy as np
from portobello.bus.Matrix import Matrix
from numpy.linalg.linalg import norm
from cmath import cos, sin
from portobello.rhobusta.options import ReadOneOption
import sys

SPIN_PARITY_REP = None

def SpinRotationMatrix(rot, check_crystal_constraint=False):
    global SPIN_PARITY_REP
    if SPIN_PARITY_REP is None:
        # expensive read - done once
        SPIN_PARITY_REP, _opts = ReadOneOption("spin_parity_representation", 1)
        if SPIN_PARITY_REP == 'old': SPIN_PARITY_REP=1j
        SPIN_PARITY_REP = complex(SPIN_PARITY_REP)
        if "--worker" not in sys.argv: print(f" - parity represented as {SPIN_PARITY_REP}*I")

    S = Matrix(np.eye(2, dtype=np.complex128))
    # from Paul Strange, page 168
    Sinv = Matrix(np.eye(2)) * SPIN_PARITY_REP
    
    if np.all(abs(rot-np.eye(3)) < 1.0e-8):
        return S, 1  # identity
    if np.all(abs(rot-np.eye(3)*(-1.0)) < 1.0e-8):
        return Sinv,-1  # identity
    
    det = np.linalg.det(rot)
    
    rotSO = rot * det  # SO representation of spin, TODO: is this the correct way to represent inversion?
      
    # rotSO is the proper part of the rot-ref
    es, evs = np.linalg.eig(rotSO)
    n = np.zeros((3,1), dtype=np.float64) #column vector
        
    N = 0 
    for i, v in enumerate(es):
        if abs(v-1.0) < 1e-6:
            
            # this should be a real vector (TODO: normalize with phase if not)            
            n = evs[:,i]   
            other = i-1
            if other<0: other = i+1
            v1 = np.cross(n.T,[0,0,1]).T # => orthogonal to both
            if np.linalg.norm(v1) < 0.1: # n was almost along z
                v1 = np.cross(n.T,[1,0,0]).T
            v1 = v1 / norm(v1)
                
            # rotate v1 around n
            v2 = np.dot(rotSO, v1)
            assert(norm(np.dot(v2.T,n))< 1.0e-6), (norm(np.dot(v2.T,n)), n, v2, rot)
            cost = np.dot(v1.T ,v2)
            sint = np.dot(np.cross(v1.T,v2.T),n)
            theta = np.arctan2(np.real(sint),np.real(cost))
                      
            theta *= -1.0  # this is passive rotation

            N = 2*np.pi / theta 
            cht = cos(theta/2.0)  # cos half theta
            sht = sin(theta/2.0)  
            break
    if check_crystal_constraint:
        assert(N != 0), N       
        assert(abs(N-np.round(N))<0.0001), (N, theta, v1, v2)
    
    # this is a positive theta (e^(1/2 \sigma * n * theta)
    # note the convention here, spin down first, spin up second (opposite of pauli matrices)
    # TODO: change the convention everywhere. Note that this convention comes from Patrick's DMFT
    # and is compatible with Andrey's code
    S[0,0] = cht - 1j * sht* n[2]
    S[1,1] = cht + 1j * sht* n[2]
    S[0,1] = sht*1j*(n[0]+n[1]*1j)
    S[1,0] = sht*1j*(n[0]-n[1]*1j)
    
    #CheckIsUnitary(S)
    
    if det < 0:
        return S*Sinv, det
    return S, det
    
    
def CheckIsUnitary(m):
    # check unitarity
    M = m.H * m
    I = np.eye(M.shape[0], dtype=np.complex128)
    if not np.all(abs(M-I) < 5.0e-10):
        raise Exception("matrix is not unitary") 
    
    
# rots - list of rotation matrices (3x3)
