'''
Created on Sep 19, 2019

@author: adler
'''

import warnings
from pathlib import Path
warnings.filterwarnings("ignore")
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from numpy.linalg.linalg import norm
from pymatgen.core.structure import Structure

from _collections import defaultdict
from pymatgen.electronic_structure.core import Magmom
import numpy as np

from pymatgen.core.operations import SymmOp
from pymatgen.core import periodic_table
from portobello.generated.FlapwMBPT import AFMSymmetry, AFMExtra, Input

# We use magmom in the struct for the following purposes: 
# 1. To identify anti-parallel or equivalent moments. Different values are taken to mean that the sites are neither
#     equivalent nor anti-parallel. Same absolute value (with different sign) is an anti-ferromagnetic relation. 
#     Same signs are taken to mean equivalent magnetic moment. 
# 2. In SDFT we use them as a hint for the initial spin polarization
#
class GroupAnalyzerWithMagmom():
    def __init__(self, st, angle_tolerance = 5): 
        self.siteMoments = [None for site in st.sites]
        self.moments = {}  # unique set of moments, each maps to an element (with sign)
        self.momentSet = defaultdict(set) # for each moment, a site number
        self.isAFM = False
        lastSign = None
        allMoments = []
        self.elements = set()
    
        # copy from the struct, because we clear it at some point. 
        # Also we need this to iterate when saving the extra info
        self.magmoms = {} 
        
        # for each site, an anti-symmetrical site which is equivalent to it in the non-mag. crystal.
        self.representiveSite = {}
    
        for i, site in enumerate(st.sites):
            self.elements.add(site.specie)
            if 'magmom' in site.properties and site.properties['magmom'] != 0.0:
                m = site.properties['magmom']
                allMoments.append(m)
                m = float(m)  # assume all collinear, we will verify later
                self.siteMoments[i] = m
                if lastSign==None:
                    lastSign = np.sign(m)
                if np.sign(m) != lastSign:
                    self.isAFM = True
                lastSign = np.sign(m)
                if m in self.moments:
                    assert(self.moments[m] == site.specie) # sites with same moment should have same specie
                self.moments[m] = site.specie
                self.momentSet[m].add(i) # add representative for this magnetization
                # record magmom and then erase it
                self.magmoms[site] = (i, m)
                  
        # we only handle collinear moments
        assert(Magmom.are_collinear(allMoments))
        
        # note: for FM crystals we could also have some sites (of a magnetic atom) with 0 magnetism
        moment2sub, sub2SpecAndMom = self.GetSubstitutes()        
        
        # analyze the space group without regard to the magnetic moments - this is the bigger group
        # we need it in order to find the isometries that map equivalent / anti-equivalent sites to each other
        # Note: if pymatgen fix the bug that ignores the magnetic moments in spacegroups, then this will need to change!
        
        prim_nonmag_st = Structure(st.lattice, 
                                   [periodic_table.Element.from_Z(s.Z) for s in st.species], 
                                   [s.coords for s in st.sites], 
                                   coords_are_cartesian=True)
        # note: doing this with fractal coords is not working
        
        prim_nonmag_sga = SpacegroupAnalyzer(prim_nonmag_st, symprec=0.001, angle_tolerance=angle_tolerance) # TODO: remove the copy()
                 
        fullGroup = prim_nonmag_sga.get_symmetry_operations(cartesian=True)

        # Don't use the pymatgen primitive, it induces further standardization
        # which is not recorded in _space_group_data['std_rotation_matrix']
        #
        #prim_nonmag_st = prim_nonmag_sga.get_primitive_standard_structure() 
        prim_nonmag_st = prim_nonmag_sga.find_primitive()
        
        #print "*********** nonmagnetic structure ***********"
        #print prim_nonmag_st
        #print prim_nonmag_sga.get_crystal_system(), prim_nonmag_sga.get_hall(), prim_nonmag_sga.get_lattice_type()
        #print "space group:",  prim_nonmag_sga.get_space_group_number(), prim_nonmag_sga.get_space_group_symbol()

        # Find the mapping between the two axis systems (of st, and prim_nonmag_st)
        # These are conventionally standardaized space groups.
        
        Mop = SymmOp.from_rotation_and_translation(prim_nonmag_sga._space_group_data['std_rotation_matrix'])
        #Mop = SymmOp.from_rotation_and_translation(prim_nonmag_sga._space_group_data['transformation_matrix'])
        #print "------------- MOP ---------------"
        #print Mop
        
            
        def FindIsometry(lattice, fullGroup, toCoords, fromCoords):
            # but this can be a periodic image
                      
            diff = fromCoords-toCoords
            if norm(lattice.get_fractional_coords(diff)) < 0.001:
                print("- anti-ferromagnetism implemented by a simple translation")
                return SymmOp.from_rotation_and_translation(translation_vec=fromCoords - toCoords)
                       
            for op in fullGroup:    # op's act on the primitive structure        
                #y = np.dot(op.rotation_matrix, fromSite.coords) + op.translation_vector
                diff = Mop.operate(toCoords) -op.operate(Mop.operate(fromCoords))
                fc = lattice.get_fractional_coords(diff)
                #print fc
                fcr = [round(f) for f in fc]
                if norm(fcr-fc) < 0.005:  # 0.005 angstrom is our tolerance
                    return op
  
            return None
        
        #Mop = SymmOp.from_rotation_and_translation(np.eye(3))  
                
        self.isometries = {}
        # for each "AFM atom", find the isometry from the opposite moment
        # do this per atom, not per moment (since there may be multiple isometries)
        for i, moment in list(self.magmoms.values()):
            if -moment in self.moments:
                op = None
                for other in self.momentSet[-moment]:
                    op = FindIsometry(prim_nonmag_st.lattice, fullGroup, 
                                      toCoords=st[i].coords, 
                                      fromCoords=st[other].coords)
                    if op is not None:
                        self.representiveSite[i] = other
                        break
                
                if op is None:
                    print("AFM equivalence operation not found:")
                    print("lattice:") 
                    print(prim_nonmag_st.lattice.matrix)
                    print("group len:", len(fullGroup))
                    print("inequivalent coords:",  st.sites[i].coords, self.momentSet[-moment])
                    assert(False)
                self.isometries[i] = Mop.inverse * op * Mop  # op operates on the magnetic crystal, Mop on the primitive one 
                #print " - AFM isometry is:"
                #print self.isometries[i]
                
        # now do the substitutions              
        for site in list(self.magmoms.keys()):
            i, m = self.magmoms[site]
            st.replace(i, moment2sub[m], site.frac_coords, 
                       coords_are_cartesian=False)         
        # ------------------------------------------------------------------------------------
        # Here struct is the original structure, with substituted elements where moments were.  
        # ------------------------------------------------------------------------------------
                        
        # first find the primitive structure, so as not to have too many group operations (>48)
        # note: we need to do this first because of a bug in the pymatgen implementation
        
        # now update the sga to reflect the reduced structure
        self.sga = SpacegroupAnalyzer(st, symprec=0.001, angle_tolerance=angle_tolerance)
        # find the primitive structure of this AFM
        self.ops = self.sga.get_symmetry_operations(cartesian=True)   
        #self.struct = self.sga.get_primitive_standard_structure()  
        self.struct = self.sga.get_symmetrized_structure()
                    
        # substitute elements back
        for i, site in enumerate(self.struct.sites):
            if site.specie in sub2SpecAndMom:
                origSpecie, moment = sub2SpecAndMom[site.specie]
                self.struct.replace(i, origSpecie, site.frac_coords, 
                               coords_are_cartesian=False,
                               properties={'magmom':  moment})   
    
    # Note: this substitution is a workaround around a bug in pyglib, which doesn't take into account the moments
    def GetSubstitutes(self):
        # these are the different "classes" of moments (usaully only one)
        SUBEL = [None for m in list(self.moments.keys())]
        
        # for each moment we map it to an unused atom specie for the purpose of symmetry analysis
        isub = 0
        for i in range(4, 80):
            el = periodic_table.Element.from_Z(i)
            if el not in self.elements:
                SUBEL[isub] = el
                self.elements.add(el)
                isub+=1
                if isub == len(SUBEL):
                    break
        assert(all([s is not None for s in SUBEL]) and  isub == len(SUBEL))  # there should be enough elements to perform substitution
        
        #print "Substitutes: ", SUBEL
        
        # maps moments to substitutes, and reversely substitutes to moments and elements
        moment2sub = {}
        sub2SpecAndMom = {}

        for m, moment in enumerate(self.moments.keys()):
            moment2sub[moment] = SUBEL[m]
            sub2SpecAndMom[SUBEL[m]] = [self.moments[moment], moment]
        return moment2sub, sub2SpecAndMom 
    
    def get_primitive_standard_structure(self):
        return self.struct
    
    def get_symmetry_operations(self):
        return self.ops
    
    def get_sga(self):     
        return self.sga
    
    # for each atom with negative moment, record a site with opposite moment
    # and the isometry which maps it to the other atom 
    def GetAFMSymmetry(self): # inp is of type input
        ini = Input()
        ini.load("./ini.h5:/") # make sure  we don't overwrite the file
        ae = AFMExtra(num_afm=len(list(self.magmoms.values())))
        ae.afm_symm = []
        for i,m in list(self.magmoms.values()):
            ae.afm_symm.append(AFMSymmetry(site=i, magmom=m, 
                                   opposite_site=self.representiveSite[i], 
                                   op=self.isometries[i].affine_matrix[:,:])) 
        return ae
        
    @staticmethod
    def RecoverMagneticMoments(st):
        afm=None
        try:
            if Path("./ini.h5").exists():
                afm = AFMExtra()
                afm.load("./ini.h5:/extra/")
        except:
            return None
        if afm is None:
            return None
        for iafm in range(afm.num_afm):
            afms =  afm.afm_symm[iafm]
            i = afms.site
            st.replace(i, st[i].specie, st[i].frac_coords, 
                       coords_are_cartesian=False,
                       properties={'magmom':  afms.magmom})   
        return afm
