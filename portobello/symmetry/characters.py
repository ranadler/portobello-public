'''
Created on Jan 6, 2020

@author: adler
'''

import numpy as np
from itertools import product
from portobello.bus.Matrix import Matrix
from typing import List
from portobello.symmetry.spins import SpinRotationMatrix
from operator import itemgetter
from tabulate import tabulate
from numpy.random.mtrand import randint
from math import sqrt
from retry import retry
import random



def PrintMatrix(M, precision=4, max_line_width=220):
        print(np.array_str(M, precision=precision, max_line_width=max_line_width, suppress_small=True))
    
class Element:
    def __init__(self, group, idx:int):
        self.group = group
        self.idx = idx
        
    def __mul__(self, other):
        return self.group[self.group.MultTab[self.idx, other.idx]]
    
    # TODO: cache it
    def inv(self):
        return self.group[self.group.Inverse[self.idx]]
    
    def order(self):
        return self.group.elementOrder(self.idx)
    
    def isProper(self):
        return self.group.isProper(self.idx)
        
    def Print(self):
        g = self.group.o3matrices[self.idx]
        if isinstance(g, Matrix):
            PrintMatrix(g)
        else:
            print("O3:")
            PrintMatrix(g.o3)
            PrintMatrix(g.su2)

def isMatrixClose(g1, g2, eps=1.0e-4):
    # note that this tolerance should not be enlarged.
    # The symmetry projection methods seems to need an accurate group representation. 
    # If matrices are not accurate, then issues arise when trying to find the 1st vectors of the representation:
    # there seems to be a numerical instability in the space spanned.
    #
    return np.all(abs(g1-g2) <eps)

def isCloseTo(g, g0):
    if isinstance(g, Matrix):
        return isMatrixClose(g, g0)
    else:
        return g.isCloseTo(g0) # assumed to have this interface

def Det(g):
    if isinstance(g, Matrix):
        return np.linalg.det(g)
    else:
        return g.det() # has to provide this interface

def ToString(g):
    if isinstance(g, Matrix):
        return np.array_str(g, suppress_small=True)
    else:
        return g.ToString()

def IsInverse(g):
    if isinstance(g, Matrix):
        return isCloseTo(g, -np.eye(g.shape[0], dtype=np.float32))
    else:
        return g.IsInverse()


class AbstractGroup(list):

    def __init__(self, O3group, extra = {},  close=False, verify_group=False):
        """    
        This builds the abstract group elements from an O(3) representation
        O3group - should be a list of UNIQUE generators
        extra - should map extra elements which may repeat some of O3group to  
              a corresponding rep-description (which describes how to generate the rep).
              this is used to build the closure list, which is how we complete the representations 
              from partial to full (partial representations correspond only to O3group)
        """
        self.verify_group = verify_group
        original = O3group.copy()
        self.closure = [] 
        toappend = [] # so as not to change what we are iterating on
    
        def find(g0, verify=True) -> int:
            for ig, g in enumerate(original + toappend):
                if isCloseTo(g, g0):
                    return ig
            if verify or self.verify_group:
                for g in original:
                    print("Element:")
                    print(ToString(g))
                print("Missing! : ------------------------------")
                print(ToString(g0))
                assert(False), f"Missing elements in the group"
            return -1
            
        def VerifyUniqueness():
            for i1, o1 in enumerate(original):
                for i2, o2 in enumerate(original):
                    if i1 <= i2: continue
                    if isCloseTo(o1,o2):
                        raise Exception("Elements {i1} and {i2} repeat")

        def ExpandGroup():
            print(" - generating / testing full group")
            cacheFactor = 3
            cache = np.zeros((len(original)*cacheFactor, len(original)*cacheFactor), dtype=np.int16)
            cache[:,:] = -1
            changed = True
            while changed:
                changed = False
                toappend.clear()
                for ig1, g1 in enumerate(original):
                    for ig2, g2 in enumerate(original):
                        if cache[ig1,ig2] !=-1:
                            continue
                        g = g1*g2
                        found = find(g,verify=False)
                        if  found == -1:
                            # found new product
                            cache[ig1,ig2] = len(original) + len(toappend)
                            toappend.append(g)
                            self.closure.append((ig1, ig2))
                            changed = True
                        else:
                            cache[ig1,ig2] = found
   
                original.extend(toappend)
                if len(original) > cache.shape[0]:
                    # grow the cache exponentially (so that it grows only once at most (it shouldn't even happen
                    # once for double groups)
                    newCacheSize = cacheFactor*len(original)
                    cache.resize((newCacheSize,newCacheSize))

            print(" - done generating group")
            return cache
                        
        if self.verify_group:
            VerifyUniqueness()
        for g, repDesc in extra.items():
            if find(g, verify=False) == -1:
                original.append(g)
                self.closure.append(repDesc)
        
        cache = None
        if close:
            cache = ExpandGroup()
       
        # build the multiplication table
        list.__init__(self, [Element(self,i) for i in range(len(original)) ])
        self.proper = [Det(O) > 0.5 for O in original ]
        self.N = len(self)
        self.order = [self.N for O in original]
        self.MultTab = np.zeros((self.N, self.N), dtype=np.int16)
        for i, j in product(range(self.N), range(self.N)):
            if cache is not None and cache[i,j]> -1:
                self.MultTab[i,j] = cache[i,j]
            else:
                self.MultTab[i,j] = find(original[i] * original[j])
                    
        self.oneIdx = -1
        for i in range(self.N):
            if all([self.MultTab[i,j] == j for j in range(self.N)]):
                if self.oneIdx == -1:
                    self.oneIdx = i
                else:
                    assert(False), "Two elements in the group are units"          

        assert(self.oneIdx != -1), "Cannot find a unity element"
    
        self.barEIdx = -1
        for i in range(self.N):
            if self.UnderlyingEbar is not None and isCloseTo(original[i], self.UnderlyingEbar):
                self.barEIdx = i
                break

        self.invIdx = -1
        for i in range(self.N):
            if IsInverse(original[i]):
                self.invIdx = i
                break
        
        self.o3matrices= original
        
        self.ComputeInverses()
        self.ComputeOrders()

    def Generators(self, withInverse=True):
        gens  = [False for  i in range(self.N)]
        cover = [False for  i in range(self.N)]
        if self.invIdx != -1:
            gens[self.invIdx] = True
            cover[self.invIdx] = True
            cover[self.oneIdx] = True
        i = 0
        countG = 0
        orders = sorted([(g,self.order[g],self.o3matrices[g].det()) for g in range(self.N)], key=itemgetter(2,1), reverse=True)

        def GenerateProduct(i,j):
            nonlocal cover, countG
            if not cover[self.MultTab[i,j]]:
                cover[self.MultTab[i,j]] = True
                countG += 1

        # add by decsending order
        for i,_ord,_det in orders:
            if cover[i]: continue
            if i == self.oneIdx: continue
            # i not generated yet, add it to gens
            gens[i] = True
            cover[i] = True
            # multiply to the left and right by the new generator
            # until no more new elements are added
            countG = 1 # to get started
            while countG > 0:
                countG = 0
                for j in range(self.N):
                    if cover[j]: 
                        GenerateProduct(i,j)
                        GenerateProduct(j,i)

        gen_ords = []
        res = []
        for g in range(self.N):
            if gens[g]: 
                if g != self.invIdx or withInverse:
                    res.append(self.o3matrices[g])
                    gen_ords.append(self.order[g])
        return res, gen_ords

    def ComputeOrders(self):
        for i in range(self.N):
            m = i
            order = 1
            while m != self.oneIdx:
                m = self.MultTab[m,i]
                order += 1
            self.order[i] = order
            
    @property
    def UnderlyingEbar(self):
        """
         no group element be equal to it - it is overriden in the double group
         """
        return None
        
    def ComputeInverses(self):        
        self.Inverse = np.zeros(self.N, dtype=np.int16)
        self.Inverse[:] = -1
        for i in range(self.N):
            for j in range(self.N):
                if self.MultTab[i,j] == self.oneIdx:
                    self.Inverse[i] = j
            if self.Inverse[i] == -1:
                print("cannot find inverse for element %d"%i)
                g= self.o3matrices[i]
                print(ToString(g))
                assert(False)
                
                
    def ExtendRep(self, rep: List):
        for i1, i2 in self.closure:
            if i1>=0:
                rep.append(rep[i1]*rep[i2])
            else:
                # this is somewhat hacky, we describe an extension by multiplying by -1 by (-1, i2)
                # TODO: this needs to depend on the representations
                rep.append(-rep[i2])

    def Print(self):
        print("Multiplication table:")
        PrintMatrix(self.MultTab)
        
        
    def isProper(self, i):
        """
        return if element i is proper (det = 1)
        """
        return self.proper[i]
    
    
    def elementOrder(self,i):
        return self.order[i]

class DoubleElement:
    def __init__(self, o3, su2):
        self.o3 = Matrix(o3)
        self.su2 = Matrix(su2)
        
    def __mul__(self, other):
        return DoubleElement(self.o3 * other.o3, self.su2 * other.su2)
    
    # TODO: cache it
    @property
    def I(self):
        return DoubleElement(Matrix(self.o3.I), Matrix(self.su2.I))  
    
    def isCloseTo(self, g0):
        g=self
        return isMatrixClose(g.o3, g0.o3) and isMatrixClose(g.su2, g0.su2)

    def det(self):
        return np.linalg.det(self.o3)

    def IsInverse(self):
        return self.isCloseTo(DoubleElement(-np.eye(3), np.eye(2)))

     

class DoubleCover(AbstractGroup):
    """
    This is constructted using the matrices corresponding to the single group
    """
    def __init__(self, O3group, rep0=None, debug=False):
        gens = [DoubleElement(Matrix(g), SpinRotationMatrix(g)[0]) for g in O3group]
        # Add Ebar as an extra generator (which adds it if it is not there)
        extra = { DoubleElement(Matrix(np.eye(3)) , -np.eye(2)) : (-1,0) }
        
        self.singleGroup = AbstractGroup([Matrix(g) for g in O3group], close=False)
        
        AbstractGroup.__init__(self, gens, extra=extra, close=True)
        if rep0 is not None:
            self.ExtendRep(-rep0)
                        
    @property
    def UnderlyingEbar(self):
        return DoubleElement(np.eye(3), -np.eye(2))
        
    # Hack - what do we want here? it is used only in the label
    def elementOrder(self, i):
        return AbstractGroup.elementOrder(self, i)
    
    # TODO: return the correct single order also for the rest of the indices
    def elementOrderSingle(self, i):
        if i < self.singleGroup.N:
            return self.singleGroup.elementOrder(i)
        else:
            return None
            
# for subgroups of O(3) Matrices
class ConjClasses(object):


    def __init__(self, group: AbstractGroup):
        
        """
            These are the conjugate classes of the group, numbered from 0, 1 ..
        """
        self.cc = [i for i in range(len(group))]
        self.group = group
        
           
        for g in self.group:
            for a in self.group:

                b = g.inv() * a * g
                # join the 2 conjugacy classes    
                cc = min(self.cc[a.idx], self.cc[b.idx])
                
                self.cc[a.idx] = cc
                self.cc[b.idx] = cc
        # Now count the number
        self.num_cc = 0
        
        ccs = {}
        for g in group:
            if self.cc[g.idx] in ccs:
                self.cc[g.idx] = ccs[self.cc[g.idx]]
            else:
                newcc =  len(ccs)
                ccs[self.cc[g.idx]] = newcc
                self.cc[g.idx] = newcc
                self.num_cc += 1

        self.oneClsIdx = self.cc[self.group.oneIdx]
        
        # for each class number, the set of elements
        self.classes = [[] for _i in range(self.num_cc)]
        for i in range(len(self.group)):
            ccls = self.classes[self.cc[i]]
            ccls.append(self.group[i])
            
        self.AddLabels()
            
                    
    def AddLabels(self):
            
        self.labels = ["%d"%len(self.classes[cc]) for cc in range(self.num_cc)]
        for cc in range(self.num_cc):
            cls = self.classes[cc]
            g = cls[0] # sample elemnet from the class
            # this is either 1, 𝑖 (inversion), or Ȇ (rot by 2pi)
            if self.cc[self.group.oneIdx] == cc:
                self.labels[cc] += " I"
                assert(len(cls) == 1)
            elif self.group.barEIdx > -1 and self.cc[self.group.barEIdx ] == cc:
                # the 2pi rotation of spins
                self.labels[cc] += " Ȇ"
                assert(len(cls) == 1)
            elif self.group.invIdx > -1 and self.cc[self.group.invIdx ] == cc:
                # spatial inversion (parity)
                self.labels[cc] += " 𝑖"
                assert(len(cls) == 1)
            elif g.isProper():
                self.labels[cc] += " C%d"%g.order()
            else: # not proper
                if g.order() == 2:
                    if  len(cls) > 1 or self.group.barEIdx == -1: # not the iȆ element
                        self.labels[cc] += " σ"
                    else:
                        self.labels[cc] += " 𝑖Ȇ"
                elif g.order() > 2:
                    self.labels[cc] += " S%d"%g.order()
            for g in cls:
                if isinstance(g, DoubleElement):
                    singleOrder = g.elementOrderSingle()
                    if singleOrder is None:
                        break
                    if g.order() == singleOrder:
                        print("order:", singleOrder)
                        print("Element #%d (class %s):"%(g.idx,self.labels[cc]))
                        g.Print()
                    
        
    # get the ith class matrix
    def GetM(self, i:int):
        M = np.zeros((self.num_cc, self.num_cc), dtype=np.int16)
        clsI = self.classes[i]
        for j in range(self.num_cc):
            # represent the class product in terms of sum of elements (multiplied by Z)
            prodJ = np.zeros((len(self.group),), dtype=np.int16)
            clsJ = self.classes[j]
            for gi, gj in product(clsI, clsJ):
                g = gi * gj 
                prodJ[g.idx] += 1
            # now write prodJ as a sum of classes (this is unique cover, we just
            # have to cover the vector prodJ)
            for k in range(self.num_cc):
                indicesK = [i for i in range(len(self.group)) if self.cc[i] == k]
                M[j,k] = min(prodJ[indicesK])
                prodJ[indicesK] -= M[j,k]
            assert(all(prodJ == 0)), str(prodJ)
        return M
        
        
        
class Irreps():
    
    def __init__(self, group: AbstractGroup, printout=True):
        self.group = group
        cc = ConjClasses(group)
        self.FindCharacters(cc)
        
        if printout:
            chi = [[0 for x in range(cc.num_cc+1)] for y in  range(cc.num_cc)]
            for i in range(cc.num_cc): chi[i][0] = i
             
            for i,j in product(range(cc.num_cc), range(cc.num_cc+1)):
                if j>0:
                    chi[i][j] = self.chi[i,j-1]
                x = chi[i][j]
                if abs(x) < 1.0e-3:           
                    chi[i][j]=0.0
                elif abs(np.real(x)) < 1.0e-3:  
                    chi[i][j]="%3.2fj"%(np.imag(x))
                elif abs(np.imag(x)) < 1.0e-3:
                    chi[i][j]="%3.2f" % np.real(x)
                else:
                    chi[i][j]="%3.2f% 3.2fj"%(np.real(x), np.imag(x))
            print(tabulate(chi, headers=["#"]+cc.labels,numalign='left'))         
            self.printableChi = np.array(chi)         
    
    def NumUnique(self, arr):
        # sort by real value
        sar = sorted(arr)
        num = 1
        for i in range(0,len(sar)-1):
            if abs(sar[i] - sar[i+1]) > 1.0e-2:
                num += 1
        return num
    
    @retry(tries=3, delay=1)
    def FindCharacters(self, cc, debug=False): 
        ncc = cc.num_cc       
        chi = np.zeros((ncc, ncc), dtype=np.complex128)
        Mis = [cc.GetM(i) for i in range(ncc)]
        
        success = False
        count = 0
        random.seed(1023)
        while not success:
            x = randint(1, 1000, size=ncc)          
            M = np.zeros((ncc, ncc), dtype=np.float64)
            for i in range(ncc):
                M += x[i] * Mis[i]
            
            if debug:            
                print("class %d length: %d" % (i,len(cc.classes[i])))
            evals, evecs = np.linalg.eig(M)
            num_unique = self.NumUnique(evals)
            if num_unique == ncc or np.any(evecs[cc.oneClsIdx,:] < 1.0e-6):
                success = True
                break
            count +=1
            if count > 5:
                raise Exception("cannot find simultaneous diagonalization using random number, tried %d times - check random number generator"%count)
                
        if debug:   
            print("*** FOUND fullly non-degenarate matrix ***") 
            PrintMatrix(M)
                
        for a in range(ncc):
            chi[a,:] = evecs[:, a] /  evecs[cc.oneClsIdx, a]
        for i in range(ncc):
            chi[:,i] *= len(cc.classes[i]) / chi[cc.oneClsIdx,i]
     
        # rows    - reps (index alpha - a)
        # columns - conj-classes
        d_a = [0.0 for  a in range(ncc)]
        for a in range(ncc):
            for j in range(ncc):
                d_a[a] += chi[a,j]*np.conj(chi[a,j])/len(cc.classes[j])
            d_a[a] = sqrt(len(self.group) / np.real(d_a[a]))
        
        # make sure these are really close to integers
        if not all(abs(d_a - np.round(d_a))<1.0e-6):
            print("characters {d_a} are not integer-like - raising exception")
            raise Exception("characters are not integers")
        
        # d_a are supposed to by integers
        d_a = np.round(d_a)
        
        sumChi = np.zeros(ncc, np.float64)
        for a in range(ncc):
            for j in range(ncc):
                chi[a,j] *= d_a[a]/len(cc.classes[j])
                
                # sanitize a bit - they cannot be infinitesimally small
                if abs(chi[a,j]) < 1.0e-8: chi[a,j] = 0.0
                elif abs(np.imag(chi[a,j])) < 1.0e-8: chi[a,j] = np.real(chi[a,j])
                elif abs(np.real(chi[a,j])) < 1.0e-8:  chi[a,j] = np.imag(chi[a,j])*1j
                sumChi[a] += chi[a,j] * np.conj(chi[a,j])*len(cc.classes[j])

        for a in range(ncc):
            # verify 2.2.25
            assert(abs(sumChi[a]-self.group.N)<1.0e-5), f"{sumChi} squared is not a {self.group.N} in irrep {a}, {chi[a,:]}"

        if debug:   
            print("Characters:") 
            PrintMatrix(chi)
            print("Dimensions:", d_a)
        sumd2 = 0
        for a in range(ncc):
            sumd2 += d_a[a] * d_a[a]
        # verify 2.2.30
        assert(round(sumd2)== self.group.N), (sumd2, self.group.N)
        
        # now fill the chiG by group element    
        self.chi = chi
        self.ncc = ncc
        self.chiG =  np.zeros((ncc, len(self.group)), dtype=np.complex128)
        self.cc = cc
        for a in range(ncc):
            for g in range(len(self.group)):
                self.chiG[a, g] = self.chi[a, cc.cc[g]]
        self.dims = np.array(d_a,dtype=np.int64)

        
    def FindFusionCoefficients(self, set1):
        F = np.zeros((self.ncc,self.ncc,self.ncc), np.float64)
        for l in range(self.ncc):
            for mu,nu in product(range(self.ncc), range(self.ncc)):
                for g in range(len(self.group)):
                    if mu in set1 and nu in set1:
                        F[l, mu,nu] += np.real(self.chiG[mu,g].conj() * self.chiG[nu,g] * np.conj(self.chiG[l,g]).real)
        F[...] /= len(self.group)
        F2 = np.zeros((self.ncc,self.ncc,self.ncc), np.int32)
        F2[...] = np.round(F[...])
        # order parameter - even spin built out of 2 spin 1/2 representations
        print("** compatible irreps for integral-spin order parameters:")
        f3 = np.sum(np.sum(F2, axis=1), axis=1)
        print(tabulate(self.printableChi[f3>0,:], 
                       headers=["#"]+self.cc.labels,numalign='left'))   
        

