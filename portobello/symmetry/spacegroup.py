

'''
Created on May 6, 2021

@author: adler
'''


from portobello.generated.FlapwMBPT import SpacegroupGens, Generator
from portobello.symmetry.characters import AbstractGroup, isMatrixClose
from numpy.linalg.linalg import norm
from pymatgen.core.operations import SymmOp
import numpy as np
from numpy.linalg import det
from math import modf
from itertools import product

angstrom2bohr = 1.8897261254535

# return the fractional part of f
def PositiveFractional(f : float):
    frac =  modf(f)[0]
    if abs(frac) < 1.0e-6:
        return 0.0
    if abs(frac+0.5) < 1.0e-5 or abs(frac-0.5) < 1.0e-5: 
        return 0.5
    if frac <= -0.5:
        return frac + 1.0
    if frac > 0.5:
        return frac - 1.0
    return frac

class IsometryModCrystal:

    def __init__(self, op : SymmOp, lattice):
        # normalize the translation vector to be positive
        self.op = op
        self.lattice = lattice
        self.NormalizeTranslationVector()

    def __mul__(self, other):
        # this composes translations and rotations (Ra(Rb+tb)+ta)
        new_matrix = np.dot(self.op.affine_matrix, other.op.affine_matrix)
        op = SymmOp(new_matrix)
        return IsometryModCrystal(op, self.lattice)
       

    # reset the translation modulu the lattice
    def NormalizeTranslationVector(self):
        t0f = self.lattice.get_fractional_coords(self.op.translation_vector)
        tf = [PositiveFractional(t0f[i]) for i in range(3)]
        self.op = SymmOp.from_rotation_and_translation(
                      self.op.rotation_matrix, 
                      self.lattice.get_cartesian_coords(tf))

    def isCloseTo(self, other):
        return isMatrixClose(self.op.affine_matrix, other.op.affine_matrix, eps=1.0e-4)

    def det(self):
        return np.linalg.det(self.op.rotation_matrix)

    def ToString(self):
        return np.array_str(self.op.affine_matrix, suppress_small=True)

    def IsInverse(self):
        return isMatrixClose(self.op.rotation_matrix, -np.eye(3, dtype=np.float32)) and np.all(abs(self.op.translation_vector) < 1.0e-5)

def IsTranslation0(op):
    return np.all(abs(op.translation_vector) < 1.0e-8)

def sanitize(v):
    for i in range(3):
        if abs(v[i])<1.0e-5:
            v[i]=0.0
    return v

def BuildRotRefGen(imc : IsometryModCrystal, order) -> Generator:
    op = imc.op
    g = Generator()
    g.allocate()
    # copy the translation part (which is not fractional here)
    g.t[:] = op.translation_vector[:] * angstrom2bohr
    g.sign=1
    # analyze the rotref
    mat = np.copy(op.rotation_matrix)
    if np.all(abs(mat-np.eye(3)) < 1.0e-8):
        # identity
        g.kind = 'E'
        assert(order >= 1)
        return g
    if np.all(abs(mat-np.eye(3)*(-1.0)) < 1.0e-8):
        # inversion - can have a nonzero vector
        g.sign=-1
        g.kind = 'E'
        assert(order >= 2)
        return g

    dd = det(mat)
    if dd < 0:
        g.sign = -1
        mat *= -1.0

    # now mat is a rot-ref in SO(3)
    es, evs = np.linalg.eig(mat)
    for i, v in enumerate(es):
        if abs(v-1.0) < 1e-6:
            # this is the rotation axis
            g.kind = 'R'
            n = evs[:,i].real

            v1 = np.cross(n.T,[0,0,1]).T # => orthogonal to both
            if np.linalg.norm(v1) < 0.1: # n was almost along z
                v1 = np.cross(n.T,[1,0,0]).T
            v1 = v1 / norm(v1)
                
            # rotate v1 around n
            v2 = np.dot(mat, v1)
            assert(norm(np.dot(v2.T,n))< 1.0e-4), (norm(np.dot(v2.T,n)), n, v2, mat, v)
            cost = np.dot(v1.T ,v2)
            sint = np.dot(np.cross(v1,v2).T,n)
            theta = np.arctan2(np.real(sint),np.real(cost))
                          
            g.n = int(round(2*np.pi / theta))
            g.v[:] = sanitize(n[:]) 
            # orders are only lower bounded by these bounds, because they may also
            # contain translations
            if g.sign == 1: assert(g.n <= order), (g.n, order)
            else:
                if g.n%2 == 1: #odd
                    assert(g.n*2 <= order), (g.n, order)
                else:
                    assert(g.n <= order), (g.n, order, mat)
            return g
        elif abs(v+1.0) < 1e-6: 
            # this is a reflection 
            g.kind = 'M'
            g.n = 0
            g.sign = 1 # check how z mirror works, for example
            g.v[:] = sanitize(evs[:,i])   # normalized, there is only 1 vector in the subspace
            assert(order >= 2)
            return g    
    # this would be a bug
    raise Exception(f"cannot determine type of group generator {str(mat)}")
    
def SanitizeRot(M):
    for i,j in product(range(3),range(3)):
        if abs(M[i,j])<1.0e-9: M[i,j] = 0.0 
    return M


def BuildSpaceGroupGens(ops, struct, opts) -> SpacegroupGens:
    sg2 = []
    if not opts.dft_no_symm:
        for iop, op in enumerate(ops):
            imy = IsometryModCrystal(op, struct.lattice)

            # test group action on sites
            for site in struct.sites:
                coords = op.operate(site.coords)
                found = False
                for site2 in struct.sites:
                    if site2.specie != site.specie:
                        continue
                    dc = site2.coords - coords
                    fc = [PositiveFractional(f) for f in struct.lattice.get_fractional_coords(dc)]
                    if all([abs(f) < 1.0e-8 for f in fc]):
                        found = True
                        break
                if not found:
                    raise Exception(f"operation {op} does not preserve crystal")
                
            latticeVs = (struct.lattice.matrix[0,:],struct.lattice.matrix[1,:],struct.lattice.matrix[2,:])
            # check the rotational part action on the lattice vectors conserves the lattice
            for lv in latticeVs:
                coords = op.apply_rotation_only(lv)
                fc = [PositiveFractional(f) for f in struct.lattice.get_fractional_coords(coords)]
                if not all([abs(f) < 1.0e-8 for f in fc]):
                    raise Exception(f"operation {op} does not map lattice vector {lv} to lattice vector")
            
            rl = struct.lattice.reciprocal_lattice_crystallographic
            for rv in (rl.matrix[0,:],rl.matrix[1,:],rl.matrix[2,:]):
                coords = op.apply_rotation_only(rv)
                fc = [PositiveFractional(f) for f in rl.get_fractional_coords(coords)]
                if not all([abs(f) < 1.0e-8 for f in fc]):
                    raise Exception(f"operation {op} does not map reciprocal lattice vector {rv} to equivalent")

            sg2.append(imy)
    else:
        # just add the identity
        sg2.append(IsometryModCrystal(SymmOp.from_rotation_and_translation(), struct.lattice))

    ag = AbstractGroup(sg2, close=False) # no close - this is supposed to be a complete group

    # note that we end up sending the whole group, not the generators, since the generator
    # code in Kutepov's code is buggy
    #gens, orders = ag.Generators()
    gens = ag.o3matrices 
    orders = ag.order

    sgg = SpacegroupGens()
    sgg.num_generators = len(gens)
    sgg.allocate()

    for i, g in enumerate(gens):
        sgg.gens[i] = BuildRotRefGen(g, orders[i])
        sgg.rots[:,:,i]=SanitizeRot(g.op.rotation_matrix[:,:])

        #gen =sgg.gens[i]
        #print(f"Added gen {gen.kind} {gen.v}@{gen.n} {gen.t} sgn={gen.sign} order={orders[i]}")
        #print(f"full op:       {g.op} {det(g.op.rotation_matrix)}")

    return sgg

