#!/usr/bin/env python3
'''
Created on May 21, 2018

@author: adler
'''
from scipy.linalg.special_matrices import block_diag
from pymatgen.symmetry.analyzer import PointGroupAnalyzer, SpacegroupAnalyzer
from math import sqrt
import numpy as np
from pymatgen.core.operations import SymmOp
from pymatgen.core.structure import Molecule, Neighbor, Structure
from portobello.symmetry.general import Representation,\
    SymmetryAdaptedRepresentation, GenerateGroup, CheckIsUnitary
from portobello.generated.symmetry import GL3Group, StructLocalSymmetry, LocalSymmetry,\
    OrbitalLabels, SpaceGroup
from itertools import product, combinations
from numpy import conjugate
from numpy.linalg.linalg import norm
from pymatgen.core.sites import PeriodicSite
import portobello.generated
from portobello.bus.Matrix import Matrix
from portobello.symmetry.characters import AbstractGroup, DoubleCover, Irreps
from portobello.rhobusta.observables import CoupledBasisObs, ProductBasisRSHObs
import pymatgen

def GetRotationFromV1ToV2(v1, v2):
    #from https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
    v1 = v1/np.linalg.norm(v1)
    v2 = v2/np.linalg.norm(v2)
    v = np.cross(v1, v2)
    s = np.linalg.norm(v)
    c = np.dot(v1, v2)
    I = np.eye(3, dtype=np.float64)
    if s < 1e-8:
        R = I
    elif np.linalg.norm(c-[0,0,-1]) < 1e-8:
        R = -I
    else:
        vx = Matrix([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0],0]]) 
        R =  I + vx + vx*vx/(1+c) 
    return R

def FindLocalNeighborsInCluster(st, sites, min_number=6, r=0, search_from_edges = False):
    if len(sites) == 1:
        return FindLocalNeighbors(st,sites[0],min_number,r)
    else:
        #look from the 
        coords = [site.coords for site in sites]
        center = np.average( np.array(coords), axis = 0 )
        tmp_site = PeriodicSite(sites[0].species, coords = center, lattice = st.lattice, coords_are_cartesian=True)
        nns, r_found = FindLocalNeighbors(st,tmp_site,min_number,r)
        if search_from_edges: #Don't just look from the center of the cites, but from the edges too
            for site in sites:
                nns_sites, r_sites = FindLocalNeighbors(st,site,min_number//2,r)
                nns+=nns_sites
                r_found = max(r_found,r_sites)
        return nns, r_found


#TODO: this can be discretized instead of doing a binary search
def FindLocalNeighbors(st, site, min_number=6, r=0):
    if r > 1.0e-3:
        return Neighbors(st.get_sites_in_sphere(site.coords, r)), r
    nn_radius = max(st.lattice.abc)
    nn_cands = st.get_sites_in_sphere(site.coords, nn_radius)
    rad = nn_radius
    while len(nn_cands) >= min_number:
        rad = rad / 2.0
        nn_cands = st.get_sites_in_sphere(site.coords, rad)
    # now increase by some% until we get >3
    while len(nn_cands) < min_number:
        rad = rad * 1.1
        nn_cands = st.get_sites_in_sphere(site.coords, rad)
    return Neighbors(nn_cands), rad

# do the right thing in different API of pymatgen
def Neighbors(nns):
    if '__version__' in pymatgen.__dict__ and pymatgen.__version__.split(".")[0] in ("2019", "2020"):
        return nns
    else:
        return [(n, n.nn_distance) for n in nns]

class MyAnalyzer(PointGroupAnalyzer):
    def _analyze(self):
        if len(self.centered_mol) == 1:
            self.sch_symbol = "Kh"
        else:
            inertia_tensor = np.zeros((3, 3))
            total_inertia = 0
            for site in self.centered_mol:
                c = site.coords
                wt = sqrt(sqrt(site.species.weight)) # adler: this works better than the pure weight, when the atomic numbers are bigger
                for i in range(3):
                    inertia_tensor[i, i] += wt * (c[(i + 1) % 3] ** 2
                                                  + c[(i + 2) % 3] ** 2)
                for i, j in [(0, 1), (1, 2), (0, 2)]:
                    inertia_tensor[i, j] += -wt * c[i] * c[j]
                    inertia_tensor[j, i] += -wt * c[j] * c[i]
                total_inertia += wt * np.dot(c, c)
            
            # Normalize the inertia tensor so that it does not scale with size
            # of the system.  This mitigates the problem of choosing a proper
            # comparison tolerance for the eigenvalues.
            inertia_tensor /= total_inertia
            eigvals, eigvecs = np.linalg.eigh(inertia_tensor)
            self.principal_axes = eigvecs.T
            self.eigvals = eigvals
            v1, v2, v3 = eigvals
            eig_zero = abs(v1 * v2 * v3) < self.eig_tol ** 3
            eig_all_same = abs(v1 - v2) < self.eig_tol and abs(v1 - v3) < self.eig_tol
            eig_all_diff = abs(v1 - v2) > self.eig_tol and abs(v1 - v3) > self.eig_tol and abs(v2 - v3) > self.eig_tol
                       
            self.rot_sym = []
            self.symmops = [SymmOp(np.eye(4))]
            if eig_zero:
                self._proc_linear()
            elif eig_all_same:
                self._proc_sph_top()
            elif eig_all_diff:
                self._proc_asym_top()
            else:
                self._proc_sym_top()
         
    def _proc_dihedral(self):
        """
        Handles dihedral group molecules, i.e those with intersecting R2 axes
        and a main axis.
        """
        main_axis, rot = max(self.rot_sym, key=lambda v: v[1])
        self.sch_symbol = "D{}".format(rot)
        mirror_type = self._find_mirror(main_axis)
        if mirror_type == "h":
            self.sch_symbol += "h"
        elif mirror_type == "d":  # pymatgen had this in any case that it's not empty
            self.sch_symbol += "d"
        #TODO : shouldn't the mirror operation be added?


    
class LocalMolecule(Molecule):
    def __init__(self, sites, neighbors, max_rad):

        species = [site.specie for site in sites]
        coords = [site.coords for site in sites]
        center = np.average( np.array(coords), axis = 0 )
        distances = np.linalg.norm( coords-center, axis=0 )

        rad = (max_rad + np.max(distances)) / 2.0  
                # so that all numbers are roughly between 1.0 and 2.0
                # note: this normalization is important
                # because it allows us to use the finely tuned tolerances that we use.
                # Material Project's tolerances were given without normalization, which caused problems 
                # in analysis of symmetries.

        coords = [(c-center)/rad for c in coords]
        self.site_coords = [] + coords
        for site in neighbors:
            species.append(site.specie)
            xyz = (site.coords - center)/rad  
            coords.append(xyz)
            print("        - neighbor: " + str(site.specie) + " " + str(xyz))

        #for testing purposes only -- rotate things so that its human readable

        Molecule.__init__(self,species, coords)
        self.point_group = None
        self.equivalent_sites = None
    
        if 0:
            import matplotlib.pyplot as plt

            fig = plt.figure()
            ax = fig.add_subplot(projection='3d')

            xs = [c[0] for c in coords]
            ys = [c[1] for c in coords]
            zs = [c[2] for c in coords]

            ax.scatter(xs,ys,zs)

            plt.show()
        
        
    def GetLocalGroup(self,strictSO=False, tol=1.0e-4):
        # the tolerance here is 0.2 because distances are 2x as in the Angstrom system, so tolerance needs to increase
        pga = MyAnalyzer(self, tolerance=tol, eigen_tolerance=1.0e-1)#, matrix_tol=1.0e-3)
        print("symmetry operations:")
        print(pga.symmops)
        return GenerateGroup([Matrix(x.rotation_matrix) for x in pga.symmops], strictSO=strictSO)
        
    @property
    def center(self):
        return (0.0,0.0,0.0)
    def get_centered_molecule(self):
        return self
    
# expresses the Real Spherical Harmonics in terms of the Spherical Harmonic 
# The rows are the coeeficients of the RSH in terms of SH (columns are SH)
def GetRealSHMatrix(l):
    def oddSign(m):  
        if m % 2 != 0:
            return -1.0
        return 1.0
        
    inv_sqrt2 = 1/sqrt(2.0)
    U = np.zeros((2*l+1,2*l+1), np.complex128)
    U[l,l] = 1.0
    for m in range(1,l+1):
        neg_m = l-m
        pos_m = l+m
        assert(neg_m>=0)
        assert(pos_m>=0)
        sgn = oddSign(m)
        U[neg_m, neg_m] = inv_sqrt2*(-1j)
        U[pos_m, neg_m] = inv_sqrt2
        
        U[neg_m, pos_m] = sgn*inv_sqrt2*1j
        U[pos_m, pos_m] = sgn*inv_sqrt2
        
    B = Matrix(U)
    #print "Basis matrix B.H*B = should be I:" + str(B.H*B)
    return B
     
# expresses the Real Spherical Harmonics in terms of the Spherical Harmonic 
# see the convention in GetRealSHMatrix
def GetRealSHMatrixEvenDim(j):
    l = int(j+0.5)
    B0 = GetRealSHMatrix(l)
    B0 = np.delete(B0, l,0)
    B0 = np.delete(B0, l,1)
    return B0

# calculated from the representation in http://symmetry.jacobs-university.de/cgi-bin/group.cgi?group=904&option=4
def GetTABasis():
    l = 3
    U = np.zeros((2*l+1,2*l+1), np.complex128)
    # now all pairs of others
    for m in range(-l,l+1):
        if m%2 == 0:
            #even case - leave as is
            U[l+m, l+m] = 1.0
        else:
            m1 = l+m
            m2 = l+m+2*np.sign(m)
            if m2 >= 2*l+1: m2 -= (l+1)
            if m2 < 0: m2 += (l+1)
            U[m1, m1] = sqrt(10.0)/4.0
            U[m1, m2] = sqrt(6.0)/4.0*np.sign(m1-m2)
             
    B = Matrix(U)
    return B

# rows: SH indices with increasing m, spin down first, then spin up.
# columns: 2l indices of j=l-1/2 and then 2l+2 indices of j=l+1/2
def GetCGMatrix2(l):
    U = Matrix(np.zeros((2*(2*l+1),2*(2*l+1)), np.float64))
    mi = 0
    for s in (-0.5,0.5): # we always put the spin down first: TODO: flip this (with everything else)
        for m in range(-l,l+1):
            m2i = 0
            for s2 in (-0.5,0.5):
                ii = int(s2*2.0)
                for m2 in range(-2*l-ii, 2*l+ii+1, 2):  # non integral m2
                    if s<0:
                        pf = 1.0
                        sign1 = -ii
                    else:
                        pf = ii
                        sign1= ii
                    if m2 == 2*m+int(2*s):
                        U[mi, m2i] = pf*sqrt(0.5)*sqrt(1.0+sign1*((m+s)/(l+0.5)))
                    m2i += 1
            mi += 1
    CheckIsUnitary(U)
    return U

# rows: SH indices with increasing m, spin down first, then spin up.
# columns: 2l indices of j=l-1/2 and then 2l+2 indices of j=l+1/2
def GetCGMatrix(l):
    if l ==0:
        return Matrix(np.eye(2,dtype=np.float64))
    ll = 2*l+1
    U = Matrix(np.zeros((2*ll,2*ll), dtype=np.float64))
    for m in range(-l,l):
        j=l-0.5
        mu = m + 0.5 # up to l-0.5=l-1+0.5
        # kappa > 0
        U[l+m+1, m+l] = sqrt((j+1+mu)/(2*j+2)) # down * m=mu+0.5

        # 2l+m+1 = 2l+1 (offset) + m + l
        U[3*l+m+1, m+l] = -sqrt((j+1-mu)/(2*j+2)) # up * m=mu-0.5
    for m in range(-l-1,l+1):
        j=l+0.5
        mu = m + 0.5 # up to l-0.5
        U[l+m+1, m+3*l+1] = sqrt((j-mu)/(2*j)) # down x mu+0.5
        U[3*l+m+1, m+3*l+1] = sqrt((j+mu)/(2*j)) # up x mu-0.5

    CheckIsUnitary(U)
    return U

def CheckIsUnitary(m):
    # check unitarity
    M = m.H * m
    I = np.eye(M.shape[0], dtype=np.complex128)
    if not np.all(abs(M-I) < 5.0e-10):
        print(m)
        raise Exception("matrix is not unitary") 
    

def GetWignerDForRots(rots, nrel, uid):
    sg = SpaceGroup()
    sg.num_ops = len(rots)
    sg.allocate()
    
    for op in range(sg.num_ops):
        sg.rots[:,:,op] = rots[op][:,:]
        sg.shifts[:,op] = 0.0
    
    selfpath = "./symmetry.core.h5:/" + uid
    
    sg.store(selfpath)
    sg.GetWignerMatrices(selfpath)
    
    sg = GL3Group()
    sg.load(selfpath)
    
    mats = None
    if nrel == 1:
        mats = [sg.WigD0, sg.WigD1, sg.WigD2, sg.WigD3] 
    else:    
        assert(nrel==2)
        mats = [sg.WigD0J, sg.WigD1J, sg.WigD2J, sg.WigD3J]
    
    for l in range(4):
        for op in  range(sg.num_ops):
            try:
                CheckIsUnitary(Matrix(mats[l][:,:,op]))
            except Exception:
                print(f"rotation matrix is not unitary for op={op} l={l}:")
                raise
            
    return sg, mats

def GetL2GammaMatrix():
    l = 2
    U = Matrix(np.zeros((2*(2*l+1),2*(2*l+1)), np.complex128))
    
    U[0,0] = -1.0
    U[1,2] = 1j
    U[2,3] = -1j
    U[3,1] = 1.0
    
    a5 = sqrt(5.0/6.0)
    a1 = sqrt(1.0/6.0)

    U[8,4] = a5
    U[4,4] = -a1 
    U[5,5] = -a5
    U[9,5] = a1 
    
    U[8,7] = -a1 
    U[4,7] = -a5 
    U[5,6] = -a1 
    U[9,6] = -a5
    
    U[6,8] = -1.0*1j
    U[7,9] = -1.0*1j
    
    #print(U)
    CheckIsUnitary(U)
    return U
    
# see https://journals.jps.jp/doi/full/10.7566/JPSJ.84.024722
def GetL3GammaMatrix():
    l = 3 # magic is now only for l=3
    U = Matrix(np.zeros((2*(2*l+1),2*(2*l+1)), np.complex128))
    
    # 5/2 sector
    ############
    a5 = sqrt(5.0/6.0)
    a1 = sqrt(1.0/6.0)
    # gamma 7
    U[4,1] = -a5 
    U[0,1] = a1 
    U[1,0] = a5 
    U[5,0] = -a1
    
    # gamma 8(1)
    U[4,5] = -a1 *1j
    U[0,5] = -a5 *1j
    U[1,4] = a1 
    U[5,4] = a5 
    
    #gamma 8(2) 
    U[2,2] = -1.0*1j
    U[3,3] = 1.0
    
    # 7/2 sector
    ############
    a5 = sqrt(5.0/12.0)
    a7 = sqrt(7.0/12.0)
    a3 = sqrt(3.0/4.0)
    a1 = sqrt(1.0/4.0)
    
    # gamma6
    U[13,6] = a5
    U[9,6] = a7
    U[6,7] = a5
    U[10,7] = a7
    
    # gamma7
    U[12,8] = a3
    U[8,8] = -a1
    U[7,9] = a3
    U[11,9] = -a1
    
    # gamma8 (1)
    U[13,10] = a7 * 1j
    U[9,10] = -a5 *1j
    U[6,11] = a7
    U[10,11] = -a5
    
    # gamma8 (2)
    U[12,12] = a1
    U[8,12] = a3
    U[7,13] = a1 *1j
    U[11,13] = a3 *1j

    CheckIsUnitary(U)
    #print(U)
    return U
        
def ToLabels(*latex_labels):
    def GetShort(ll):
        s = ll.replace("^","").replace("_","").replace("\\frac{","").replace("}{","/").replace("\Gamma","gam").replace("};",";")
        if s[-1] == "}" and not s.startswith("gam"):
            s = s[:-1]
        return s
        
    ols = []
    for ll in latex_labels:
        ols.append(OrbitalLabels(latex_label=ll, short_label=GetShort(ll)))
    return ols
    
            
def GetRSHLabels(l):
    if l == 0:
        return ToLabels("s")
    if l == 1:
        return ToLabels("y","z","x")
    if l == 2:
        return ToLabels("xy","yz","z^2","xz","x^2-y^2")
    if l == 3:
        return ToLabels("y(3x^2-y^2)","xyz","yz^2","z^3","xz^2","z(x^2-y^2)","x(x^2-3y^2)")
    else:
        return None

def GetSHLabels(l):
    return ToLabels(*["Y_%s^%s" %(l,m) for m in range(-l,l+1)])


def GetJLabels(l):
    orb_names = []
    orb_names += ["\\frac{%d}{%d};-\\frac{%d}{%d}"%(2*l-1,2,abs(m),2) for m in range(-(2*l-1),0, 2) ]
    orb_names += ["\\frac{%d}{%d};\\frac{%d}{%d}"%(2*l-1,2,m,2) for m in range(1, 2*l, 2) ]
           
    orb_names += ["\\frac{%d}{%d};-\\frac{%d}{%d}"%(2*l+1,2,abs(m),2) for m in range(-(2*l+1), 0, 2) ]
    orb_names += ["\\frac{%d}{%d};\\frac{%d}{%d}"%(2*l+1,2,m,2) for m in range(1, 2*l+2, 2) ]

    return ToLabels(*orb_names)

def GetTALables():
    return ToLabels("T_1","A","T_2","T_1","T_2","T_2","T_1") # TODO: add the subscript according to convention
    
def GetGammaLabels():
    return ToLabels("\Gamma_7^{5/2}",
                    "\Gamma_7^{5/2}",
                    "\Gamma_8^{1,5/2}",
                    "\Gamma_8^{1,5/2}",
                    "\Gamma_8^{2,5/2}",
                    "\Gamma_8^{2,5/2}",
    
                    "\Gamma_6^{2,7/2}",
                    "\Gamma_6^{2,7/2}",
                    "\Gamma_7^{2,7/2}",
                    "\Gamma_7^{2,7/2}",
                    "\Gamma_8^{1,7/2}",
                    "\Gamma_8^{1,7/2}",
                    "\Gamma_8^{2,7/2}",
                    "\Gamma_8^{2,7/2}")
    
def GetRSHPlusSpinLabels(l):
    rshL = GetRSHLabels(l)
    return ToLabels(*([lbl.short_label+"_up" for lbl in rshL] + [lbl.short_label+ "_dn" for lbl in rshL]))
    
    
def GetBasisChangeD(basis, l, dim, nrel):
    I = np.eye(dim, dtype=np.complex128)
    if nrel == 1:
        if basis in ('FC'):
            if l !=3:
                return I, GetRSHLabels(l)
            return GetTABasis(), GetTALables()
        elif basis == 'SH':
            return GetRealSHMatrix(l), GetSHLabels(l)
        # native, RSH
        return I,GetRSHLabels(l)
    else:
        # we support running in RSH + spin (uncoupled basis) even in relativistic mode
        if basis == 'RSH':
            return  GetCGMatrix(l).H * block_diag(GetRealSHMatrix(l).H, GetRealSHMatrix(l).H), GetRSHPlusSpinLabels(l)
        if basis in ('FC') and l == 3:
            return GetL3GammaMatrix(), GetGammaLabels()
        if basis in ('FC') and l == 2:
            return GetL2GammaMatrix(), GetJLabels(2)[:4] + GetGammaLabels()[:6]
        return I, GetJLabels(l)

class NonParallelVectors(object):
    def __init__(self,st):
        self.eps = 1.0e-12
        self.vecs = [] 
        # all are column vectors
        self.add(np.array([1.0, 0.0, 0.0]).T)
        self.add(np.array([0.0, 1.0, 0.0]).T)
        self.add(st.lattice.matrix[:,0])
        self.add(st.lattice.matrix[:,1])
        self.add(st.lattice.matrix[:,2])
    # add a vector only if it is not parallel to any of the existing ones
    # vec - should be a triplet of real numbers
    def add(self, vec):
        parallel = False
        for i in range(vec.shape[0]):
            if abs(vec[i].imag) > 1.0e-16:
                raise Exception("vector should be real")
        vec = np.array(vec.real,np.float64) / norm(vec)  # normalize first
        for ev in self.vecs:
            dp = np.dot(conjugate(ev).T, vec)
            if ((abs(dp - 1.0) < self.eps) or (abs(dp + 1.0) < self.eps)):
                parallel = True
                break
        if not parallel:
            vec = np.real(vec)  # the vector is should be real, but we remove the imaginary part 
            self.vecs.append(vec)
        
    def SameEigen(self, w, ind1, ind2):
        return abs(w[ind1]-w[ind2]) < 1.0e-10
        
    def addAxesOf3x3Matrix(self, rot):
        w, v = np.linalg.eig(rot)
        s01 = self.SameEigen(w,0,1)
        s12 = self.SameEigen(w,1,2)
        s02 = self.SameEigen(w,0,2)
        
        if not s01 and not s12 and not s02:
            # if all eigenvalues are different, one has to be real, and the others are conjugates of each other
            # add the real one, and add the others only if they are real.
            for i in range(3):
                if abs(w[i].imag) < self.eps:
                    self.add(v[:,i])
        elif s01 and s12 and s02:
            # if all eigenvalues are the same, we need not add anything, since x,y,z will be added outside.
            None
        else:
            # if we have a degenerate subspace of dim 2, then all eigenvalues are real.
            # we need to add only the normal, as well as the eigenvectors in the plane
            # these are axes of high symmetry
            if s01:
                self.add(v[:,2])
            elif s12:
                self.add(v[:,0])
            else:
                self.add(v[:,1])
                      
    def GetCandidateRotations(self):
        n = len(self.vecs)
        all_dotprods = np.zeros((n,n), np.float64) 
        for i1,i2 in product(list(range(n)), repeat=2):
            all_dotprods[i1, i2] = abs(np.dot(self.vecs[i1][:].T,self.vecs[i2][:]))
        rhs_mats = []
        
        def addMatrix(mat):
            assert(abs(np.linalg.det(mat)-1.0)<1.0e-6) # assert proper rotation
            for m in rhs_mats:
                if np.all(abs(mat-m) < self.eps):
                    return
            rhs_mats.append(mat)
            
        for i1,i2 in combinations(list(range(n)), r=2):
            if all_dotprods[i1, i2] < self.eps:
                z = np.cross(self.vecs[i1][:].T,self.vecs[i2][:].T).T
                cmat = np.zeros((len(z),len(z)), np.float64)
                cmat[:,0] = np.squeeze(self.vecs[i1])
                cmat[:,1] = np.squeeze(self.vecs[i2])
                cmat[:,2] = np.squeeze(z[:])
                addMatrix(cmat)
                addMatrix(cmat[:,[1,2,0]])
                addMatrix(cmat[:,[2,0,1]])
        return rhs_mats
   
def FindSiteIndex(st, site):
    ps = PeriodicSite(site.species, site.coords, st.lattice, coords_are_cartesian=True)
    for i, s in enumerate(st.sites):
        ps2 = PeriodicSite(s.species, s.coords, st.lattice, coords_are_cartesian=True)
        if ps.is_periodic_image(ps2):
            sps = portobello.generated.symmetry.PeriodicSite()
            sps.site_index = i
            sps.xyz = site.coords
            return sps
    return None
  
class RhobustaSymmetrizedStruct(Structure):
    def __init__(self, st):
        Structure.__init__(self, st.lattice, st.species, st.coords)
        self.equivalent_indices = []
                
        

# what are the use cases?  
# 1. In rhobusta init - 
#    given pymatgen struct st, and a bunch of magnetic parameters, find the symmetry group
#    and return a SkeletalSymmetrizedStruct, which inherits the original struct, and contains
#           equivalent_indices
#           lattice_type  - should be the appropriate lattice type  # used in GetIstruc
#           hall - the appropriate hall symbol                      # GetIstruc
#           space_group_number - for printing
#
#
# 2. In wannier analysis - given the input file, read the structure and symmetry operations, and obtain the local symmetries
# 

#
# note that the object analyzes *all* atoms, but is specific to a choice of l.
class StructLocalSymmetryAnalyzer(StructLocalSymmetry):
    def __init__(self, st, l, num_si, nrel, species, basis='native', sga=None, moment=None, r=0, symmetry_breaking=None, opts=None):
        StructLocalSymmetry.__init__(self)
        self.opts = opts
        
        # get the symmetrized structure, so that we get equivalent_indices
        if sga is None: 
            sga = SpacegroupAnalyzer(st, symprec=0.01, angle_tolerance=5)
            self.symst = sga.get_symmetrized_structure() #SkeletalSymmetrizedStruct(st)
        else:
            self.symst = st
            
        self.num_distinct = len(self.symst.equivalent_indices)
        self.num_atoms = len(self.symst.sites)
        self.allocate()
        
        self.num_si = num_si
        self.nrel = nrel
        self.basis = basis
        
        if species=="":
            splist = [str(s.specie) for s in self.symst.sites]
        else:
            splist = species.split(",")
        
        mag = None
        if moment is not None and moment != "":
            v = moment.split(",")
            if len(v) == 1:
                mag = [0.0, 0.0, float(v[0])]
            else:
                mag = [float(vi) for vi in v]
        else:
            symetry_breaking = None
        
        # run symmetry analysis only for the distinct atoms - the others are equivalent to them
        for i, eqcls in enumerate(self.symst.equivalent_indices):

            if opts.cluster:
                assert self.basis == 'adapt', "cluster calculation requires adapted basis"
                site_nums = eqcls
                sites = [self.symst.sites[j] for j in eqcls]
            else:
                site_nums = [eqcls[0]]
                sites = [self.symst.sites[site_nums[0]]]
            
            found = False
            for sp in splist:
                if str(sites[0].specie).startswith(sp):
                    found=True
                    break
            
            if not found:
                self.distinct_symm[i] = None
                continue

            #print "**** local analysis of index %d is of atom " % site_num, site
            self.distinct_symm[i] = self.AnalyzeSymmetry(site_nums, sites, l, magnetization=mag, 
                                                         symmetry_breaking=symmetry_breaking, r0=r)


    # for clusters -- dont repeatedly include neighbors
    # return a subset of nns with unique coords
    def FindUniqueCoords(self, nns, avoided_coords):
        coords = [n.coords for n, r in nns]
        toDelete = [False for n, r in nns]
        for i,coord in enumerate(coords):
            for j,coord0 in enumerate(coords):
                if j==i: break
                if np.all(abs(coord- coord0) < 1.0e-5):
                    toDelete[i] = True
        for ac in avoided_coords:
            for i,coord in enumerate(coords):
                if np.all(abs(coord- ac) < 1.0e-5):
                    toDelete[i] = True
                    break
        nodupsNns = []
        for i, isdup in enumerate(toDelete):
            if not isdup:
                nodupsNns.append(nns[i])
        return nodupsNns

    # this version of the local group is extracted from the local molecule
    def GetLocalGroup(self, sites, adaptation, strictSO=False, r0=0, scaling=None):
        print(f"**** cluster size {len(sites)}")
        neighbors = []
        nns, rad = FindLocalNeighborsInCluster(self.symst, sites, r=r0)

        print(f"    - num neighbors in local environment with radius {rad}: {len(nns)-len(sites)}") # center is also in list
        
        coords = [n.coords for n, r in nns]
        nns = self.FindUniqueCoords(nns, avoided_coords=[s.coords for s in sites])
                
        for s, r in nns:
            if r > 1.0e-10: #no longer needed but keep it in-case we change the above checks
                if scaling is not None:
                    coords = s.coords
                    for i in range(3): coords[i] *= scaling[i]
                    s = Neighbor(s.species, coords, nn_distance=r)
                neighbors.append(s)
                    
        self.neighbors = (nns, rad)
        self.sites = sites
                
        mol = LocalMolecule(sites, neighbors, rad)
        tol = 0.1    # adaptation by rotation does not require high tolerance
        return mol.GetLocalGroup(strictSO, tol=tol)    

    def FilterForMagnetization(self, M, group):
        newGroup = []
        mVec = np.array(M).T
        mVec /= norm(mVec)
        for g in group:
            # only proper rotations. if 2 axes of reflection are available, they amount to \sigma_z
            # which will symmetrize the spin
            #if det(g) < 0:
            #    continue
            if np.all(abs(np.dot(mVec, np.array(g) @ mVec)-mVec)) < 1.0e-6:
                newGroup.append(g)
        return newGroup

    def GetAtomToAtomInMoleculeUnderRots(self, sites, group):
        coords = np.array([site.coords for site in sites])
        center = np.average( coords, axis = 0 )
            
        for i in range(coords.shape[0]):
            coords[i,:] = coords[i,:] - center[:]

        def findAtom(c):
            for i in range(coords.shape[0]):
                if np.allclose(c, coords[i,:],atol=1.0e-5):
                    return i
            return None
        
        atom2atomInMol_G = []
        for r, rot in enumerate(group):
            atom2atomInMol_G.append([])
            for at in range(coords.shape[0]):
                cM = Matrix(coords[at,:])
                tc = rot.H*cM.H
                atom = findAtom(tc.H)
                if atom is None:
                    raise Exception(f"cannot find atom for g={rot} of {at}")
                atom2atomInMol_G[-1].append([at, atom])       
                    
        return atom2atomInMol_G
                
                
    def AnalyzeSymmetry(self, site_nums, sites, l, strictSO=False, magnetization=None, symmetry_breaking=None, debug=False, r0=0):
        """
            strictSO : if only SO(3) symmetries should be used (otherwise the group may contain rot-ref's from O(3))
        """
        print("- distinct site:" + str(sites[0]))
        scaling = (1.0,1.0,1.0) 
        if symmetry_breaking == "geometry":
            magnetization = np.array(magnetization, dtype=np.float64)
            zvec = np.array([0.0,0.0,1.0])
            mag2 = np.cross(zvec, magnetization)
            if np.all(abs(mag2)<1.0e-7):
                zvec = np.array([1.0,0.0,0.0])
                mag2 = np.cross(zvec, magnetization)
            assert not np.all(abs(mag2)<1.0e-7)
            mag3 = np.cross(magnetization, mag2)
            scaling = abs(magnetization*0.6+ mag2 + mag3)
            print(f"scaling vector is {scaling}")
        group = self.GetLocalGroup(sites, strictSO=strictSO, adaptation=self.basis == 'adapt',r0=r0, scaling=scaling)

        a2aG = self.GetAtomToAtomInMoleculeUnderRots(sites, group)
       
        # support for macroscopic magnetization vector
        #if self.nrel == 2 and magnetization is not None:
        #    group = self.FilterForMagnetization(magnetization, group)
         
        print(f"    - num of rotations in group: {len(group)}")   
    
        name = f"{sites[0].specie}-{site_nums[0]}"
        gl3group, Dlgs = GetWignerDForRots(group, self.nrel, f"local_group{l}-{name}")

       
        noAxisRot = False
        if magnetization is not None and self.basis == 'adapt': # if not adapted, we don't use the group for the representation
            noAxisRot = True
            mm = magnetization
            if self.nrel == 2:
                obs = CoupledBasisObs(l)
            else:
                obs = ProductBasisRSHObs(l)
                
            gis = None
            if symmetry_breaking == "moment":
                group = self.FilterForMagnetization(mm, group)
                print(f"Size filtered by conservation of vector: {len(group)}")
            else:
                # we filter for commutation with M (also in the geometric case)
                OO = symmetry_breaking
                assert(OO is not None)
                M = Matrix(obs.Get("Mx") * mm[0] + obs.Get("My") * mm[1] + obs.Get("Mz") * mm[2])
                newGroup=[]
                gis=[]
                for i in range(Dlgs[l].shape[2]):
                    gi = Matrix(Dlgs[l][:,:,i]) # in native basis - e.g. J if in rel. mode
                    if self.num_si == 2:
                        gi = block_diag(gi,gi)
                    if obs.Commute(M, gi):
                        newGroup.append(group[i])
                        gis.append(gi)
    
                print(f"Restricted dlg's for l={l}: {len(newGroup)}", flush=True)
                group = newGroup
                ag = AbstractGroup(group, close=False) # check group
                group = ag.o3matrices
            print("New Group:")  
            for i, g in enumerate(group):
                print(g)
                if gis is not None: print(gis[i])
            gl3group, Dlgs = GetWignerDForRots(group, self.nrel, f"restricted_local_group{l}-{name}")
            
        repClass = Representation
        if self.basis == 'adapt':
            repClass = SymmetryAdaptedRepresentation
            noAxisRot = True # maybe enable this, we may want to rotate a symmetry adapted rep (test on LaVO3 example)
        
        if self.opts.pre_adapt_eig: noAxisRot=True # do not also rotate if doing pre-adaptation

        def I3d(l, extra_dim=True):
            dim = (2*l+1)*self.nrel
            M = np.zeros(shape=(dim, dim, 1), dtype=np.float64)
            for i in range(dim): M[i,i,0] = 1.0
            if extra_dim:
                return M
            else:
                return Matrix(M[:,:,0])
        
        obsBasis = [I3d(0,extra_dim=False), I3d(1,extra_dim=False), I3d(2,extra_dim=False), I3d(3,extra_dim=False)]
        """
        if len(sites) > 1:
            if len(sites) > 2:
                print(" - rotating axis to the normal of the correlated molecule's primary plane")
                vecs = [sites[0].coords - s.coords for s in sites[1:]]
                normal = abs(np.cross(vecs[0],vecs[1]))
                normal /= np.linalg.norm(normal)
                for vec in vecs[1:]:
                    norm2 = abs(np.cross(vecs[0],vec)) 
                    norm2 /= np.linalg.norm(norm2)
                    assert np.linalg.norm(norm2 - normal) < 1e-5, "sites are not all on the same plane... not handled yet"
            else:
                print(" - rotating axis to the bond between atoms in cluster")
                normal = sites[0].coords - sites[1].coords
            
            new_axis = normal / np.linalg.norm(normal)
            initial_axis = [0,0,1]
            #from https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
            v = np.cross(initial_axis, new_axis)
            s = np.linalg.norm(v)
            c = np.dot(initial_axis,new_axis)
            I = np.eye(3, dtype=np.float64)
            if s < 1e-8:
                axis_rots = [I]
            elif np.linalg.norm(c-[0,0,-1]) < 1e-8:
                axis_rots = [-I]
            else:
                vx = Matrix([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0],0]]) 
                axis_rots = [ I + vx + vx*vx/(1+c) ]
            axis_rots = [I]
            _rotsGroup, DlRs = GetWignerDForRots(axis_rots, self.nrel, f"axis_rotations{l}-{name}")
        """

        if noAxisRot:
            print(" - no axis rotation of basis (adaptation by unitary)")
            axis_rots = [np.eye(3, dtype=np.float64)]
            DlRs = [I3d(0), I3d(1), I3d(2), I3d(3)]

            if self.opts is not None and self.opts.pre_adapt_eig != "":
                obsBasis = []
                for ll in range(4):
                    if self.nrel == 2:
                        obs2 = CoupledBasisObs(ll)
                    else:
                        obs2 = ProductBasisRSHObs(ll)
                    Sz_sym = self.nrel<2 # not self.num_si==1 - note that even in si=2 we still have Sz symm in the basis (but not in solution)
                    uM =  obs2.CoDiagonalize(self.opts.pre_adapt_eig, prepend_Sz=Sz_sym)
                    if Sz_sym: #nonrel with one spin, we block diagonalized to spin too
                        uM = uM[:2*l+1, :2*l+1]
                    obsBasis.append(uM)

        else:
            print(" - trying rotations to adapt the matrices")
            npv = NonParallelVectors(self.symst) 
            
            for rot in group:
                npv.addAxesOf3x3Matrix(rot) # should we look only at the generators?
            axis_rots = npv.GetCandidateRotations()
            if debug:
                axis_rots = axis_rots[:1]
            # Here we get the D rotations for different quantization orientations
            _rotsGroup, DlRs = GetWignerDForRots(axis_rots, self.nrel, f"axis_rotations{l}-{name}")

        print("    - num of coordinate candidates:" + str(len(axis_rots)))
        
        ls = LocalSymmetry(site_index = site_nums[0]) #TODO: not sure about only holding one of the site nums (cluster)
        ls.allocate()
        ls.local_group = gl3group
        
        # construct the abstract group, with its closure list & verification, exactly once
        # even when we don't need the reps in non-adapted basis
        if self.nrel < 2:
            irreps = Irreps(AbstractGroup([Matrix(g) for g in group]))
        else:
            irreps = Irreps(DoubleCover([Matrix(g) for g in group]))
        
        if self.basis != 'adapt':
            irreps = None
        
        numOffDiags = None
        dim = self.nrel*(2*l+1)
        # this is the basis change matrix from native (usually Identity)
        basisChangeD, basisLabels = GetBasisChangeD(self.basis, l, dim, self.nrel)
        

        cluster_dim = len(sites)

        labels = []
        for site_ in sites:
            labels += basisLabels
        basisChangeD = block_diag(*[basisChangeD*obsBasis[l] for i in range(cluster_dim)])
        Dlgs_cluster = np.zeros((dim*cluster_dim, dim*cluster_dim, len(group)), dtype = Dlgs[l].dtype)
        for r, rot in enumerate(group):
            for b in a2aG[r]:
                slices = [slice(b[i]*dim, (b[i]+1)*dim) for i in range(2)]
                Dlgs_cluster[slices[0],slices[1],r] = Dlgs[l][:,:,r]
            #print(Matrix(Dlgs_cluster[:,:,r]))
            CheckIsUnitary(Matrix(Dlgs_cluster[:,:,r]))
  
        # verify that the cluster matrices form a group
        if cluster_dim > 1:
            _group = AbstractGroup([Matrix(Dlgs_cluster[:,:,r]) for r, rot in enumerate(group)],
                                  close=False,
                                  verify_group=True)                               

        # loop over all axis rotations and pick the best (in adaptation this is only the I rotation)
        for ari, axis_rot in enumerate(axis_rots):
            nbRep = repClass(l, dim*cluster_dim,
                        nrel=self.nrel, 
                        num_si=self.num_si, 
                        irreps=irreps,
                        o3group=group,
                        Dlgs=Dlgs_cluster, 
                        rot=axis_rot, 
                        rotD=block_diag(*[Matrix(DlRs[l][:,:,ari]) for i in range(cluster_dim)]), 
                        basisChangeD=basisChangeD,
                        labels=labels)
            nod0 = nbRep.Num0Offdiagonals()
            if numOffDiags is None or nod0 > numOffDiags:
                numOffDiags = nod0                             
                ls.annotated_reps_l[l] = nbRep

        print("rep for:"+str(l)+"\n"+ str(ls.annotated_reps_l[l].H))
        print("basis: "+str([l.short_label for l in ls.annotated_reps_l[l].labels]) )

        return ls
