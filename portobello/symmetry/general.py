'''
Created on May 21, 2018

@author: adler
'''

from portobello.rhobusta.observables import CoupledBasisObs
import numpy as np
import itertools
from portobello.generated.symmetry import AnnotatedRepresentation
from portobello.bus.Matrix import Matrix
from scipy.linalg.special_matrices import block_diag
from typing import List
from portobello.symmetry.characters import Irreps
from numpy import linalg
from math import sqrt
import sys
from itertools import combinations

def PrintMatrix(M, precision=4, max_line_width=220):
        print(np.array_str(M, precision=precision, max_line_width=max_line_width, suppress_small=True))
    
def cnorm(A):
    return sqrt(np.vdot(A[:],A[:]))

def ColVector(A, i=0):
    return Matrix(np.reshape(A[:,i],(A.shape[0],1)))
       
def CheckIsUnitary(m):
    # check unitarity
    M = m.H * m
    I = np.eye(M.shape[0], dtype=np.complex128)
    if not np.all(abs(M-I) < 5.0e-9):
        PrintMatrix(m)
        raise Exception("matrix is not unitary") 
    
             
# I rewrote this, based on code from PyMatGen, because their code updates
# the list at the same time that they're iterating it, which I'm not comfortable with -
# I think it does not necessarily work in Python.
# takes a set of rotation matrices
# N - the bound is actually n! (factorial), n-num of neighbors, but most of our group are smaller
def GenerateGroup(generators, tol=1.0e-1, N=130, strictSO=False) -> List[Matrix]:  # 120 is for Ih
       
    if strictSO:
        SOgens = []
        for g in generators:
            if np.linalg.det(g) > 0:
                SOgens.append(g)
        generators = SOgens
        
    if not generators:
        return None
    I = Matrix(np.eye(generators[0].shape[0])) 
    
    def InGroup(op, group):
        for g in group:
            if np.allclose(op, g, rtol=0.0, atol=tol):
                return True
        return False
    
    group0 = []
    for g in generators:
        if not InGroup(g, group=group0):
            group0.append(Matrix(g))
    group = group0
    
    print("    - actual number of  generators:" + str(len(group0)))
    
    if not InGroup(I, group):
        group.insert(0, I)
    more = True
    while more:
        more = False
        if len(group) > N:
            raise Exception("generated group is too large - increase tolerance?" + str(group))
        for g in generators:
         
            op  = g.I 
            if not InGroup(op, group):
                #print "adding group elmenet", op
                group.append(op)
                more = True
                break
                
            for s in group:
                op  = g * s
                if not InGroup(op, group):
                    #print "adding group elmenet", op
                    group.append(op)
                    more = True
                    break
    return group

class Representation(AnnotatedRepresentation):
    # Dlg - Wigner D matrices for the group (see how it's used)
    # DlR - rotation matrix in dim dimensions to apply on the Dlg's (could be I)
    def __init__(self, l: int, dim: int, nrel: int, num_si: int, irreps : Irreps, o3group, Dlgs, rot, rotD, basisChangeD, labels, analyze=True):
        AnnotatedRepresentation.__init__(self, l=l, dim=dim, num_si=num_si, nrel=nrel)
        self.blocking_tol = 1.0e-1
        self.g_tolerance = 0.95
        self.chi_tolerance = 0.2
        self.num_ops = Dlgs.shape[2]
        self.rep = np.zeros((self.dim, self.dim, self.num_ops), np.complex128)
        assert(rot.shape == (3,3))
        self.rot = rot
        assert(rotD.shape == (dim,dim))
        self.rotD = rotD
        assert(basisChangeD.shape == (dim,dim))
        self.basisChangeD = basisChangeD
        assert(len(labels) == dim)
        self.labels = labels
        self.Dlgs = Dlgs
        self.group = o3group # can be None if this is a double group

        self.CalculateBlockCharacters()
        self.H = Matrix(np.zeros((self.dim,self.dim), np.int32))
            
        if analyze: 
            self.CheckSymmetryAdaptation()
            self.ApplyWignerTheorem()
        
    def Sanitize(self, A, phaseToo=True):
        A = np.array(A, dtype=np.complex128)
        for i in range(A.shape[0]):
            for j in range(A.shape[1]):
                if abs(A[i,j]) < 1.0e-8:
                    A[i,j] = 0.0
                if abs(A[i,j] - 1.0) < 1.0e-8:
                    A[i,j] = 1.0
                if abs(A[i,j] + 1.0) < 1.0e-8:
                    A[i,j] = -1.0
                    
                    
        for j in range(A.shape[1]):
            phase = 1.0
            if phaseToo:
                for i in range(A.shape[0]):
                    if abs(A[i,j]) > 1.0e-5:
                        phase = A[i,j] / abs(A[i,j])
                        break
            nn = phase * cnorm(A[:,j])
            if (abs(nn)>1.0e-05): A[:,j] /= nn
        return A
    
    # note that P.H*P=P and therefore svd gives the evs
    def GetOrthonormalBasisForColumns(self,A, doc = ""):
        A = self.Sanitize(A)
        u,s,_vh  = linalg.svd(A)
        print(f"symm. evs for {doc}: {s}")
        # ad__ler: don't use scipy.linalg.orth or qr, it fails once in a while.
        # numpy svd is more numerically stable, and should work for projectors
        k = np.sum(abs(s) > 0.05)
        return self.Sanitize(u[:, :k])
            

    # try to find equivalent vectors to the columns in range colRange,
    # with more 0's 
    # this is important to reduce the size of 4-tensors (U) which will be used to build
    # manybody matrices
    def Simplify(self, M, colRange):
        #PrintMatrix(M)
        nz = np.array(abs(M) > 1.0e-5)
        counts = np.sum(nz, axis=0)
        for i1, i2 in combinations(colRange,2):
            if counts[i1] !=2 or counts[i2] !=2:
                continue
            if np.any(nz[:, i1] != nz[:,i2]):
                continue
            S = M[np.argwhere(nz[:,i1]), [i1,i2]]
            d = np.linalg.det(S)
            if abs(d) > 1.0e-2:
                #print("Substituting sub matrix", d)
                M[np.argwhere(nz[:,i1]), [i1,i2]] = d*np.eye(2,dtype=np.complex128)
                counts[i1] = 1
                counts[i2] = 1
                nz = np.array(abs(M) > 1.0e-5)
            #PrintMatrix(S)
        
        
        
    def CompleteIrrepFrom1stVec(self, u0, irrDim):
        # Now we have to calculate the closure under Dg, and 
        # use P11 to find the projectors to the corresponding subspace
        # for the 1st vector
        C1 = np.array(u0)
        u0i = 0   # points to the current vector that the group acts on
        d = C1.shape[0]
        N = len(self.group)
        while C1.shape[1] < irrDim:
            cols = np.ndarray((d,N), dtype=np.complex128)
            counts = [[0, g] for g in range(N)]
            for g in range(N):
                Dg = Matrix(self.rep[...,g]) 
                cols[:, g] = (Dg * ColVector(C1 ,u0i)).reshape(d)
                
            cols = self.Sanitize(cols, phaseToo=True)
            for g in range(N):     
                counts[g][0] = np.sum([abs(cols[:,g])>1.0e-8])
                
            # order cols by smallest number of non-small elements
            counts.sort()
            indices = [c[1] for c in counts]
            cols[:,:] = cols[:, indices]
            
            # orthogonalize by order until we have enough vectors
            for g in range(N):
                if C1.shape[1] == irrDim:
                    break
                for j in range(C1.shape[1]):
                    cols[:,g]-= np.vdot(C1[:,j],cols[:,g]) *C1[:,j]
                
                nn = cnorm(cols[:,g])
                if abs(nn) > 1.0e-5:    
                    # orthogonal to previous vectors - add it
                    C1 = np.append(C1, cols[:, [g]] / nn, axis=1)
            u0i += 1
                    
        self.Simplify(C1,range(2,C1.shape[1])) # simplify the 2nd to last columns if possible
                    
        Di0g = np.zeros((d,N), dtype=np.complex128)
        # recalculate representation in basis C1
        for g in range(N):
            Dg = Matrix(self.rep[...,g])
            v0 =  ColVector(C1, 0)
            for i in range(irrDim):
                vi = ColVector(C1, i)
                Di0g[i,g] =  (vi.H *Dg * v0)[0,0]
            
        return C1, Di0g

        
    def CalculateSymmetryProjectors(self, irrs):
        U0 = np.zeros((self.dim, 0), dtype=np.complex128)
           
        # project to these reps using their character
        N = len(self.group)
        set1 = []
        cc = irrs.cc # conjugtion classes
        for alpha in range(irrs.ncc):
            r = 0
            # Formula 2.2.28
            for g in range(N):
                r += irrs.chiG[alpha, g]* np.trace(self.rep[...,g])
            assert(np.imag(r) < 1.0e-4)
            r /= N
            # r is the number of repetition of this representation
            r = int(np.round(np.real(r)))

            print(f"* irrep {alpha} (dim={irrs.dims[alpha]} * {r}):")
  
            pf = irrs.dims[alpha] / N
            # see (2.2.38) in the "Site Symmetries" text book.
            # this is the projection operator of the representation # alpha
            P = np.zeros((self.dim, self.dim), dtype=np.complex128)
            for g in range(N):
                P[:,:] +=  pf * np.conj(irrs.chiG[alpha, g]) * Matrix(self.rep[:,:,g])
            if np.allclose(np.zeros_like(P), P, rtol=0.0, atol=1.0e-5):
                continue
                  
            set1.append(alpha)
            P = Matrix(P)
            # B is the full basis for the representation including repetitions
            B = self.GetOrthonormalBasisForColumns(P.H*P, doc="Projection")
                  
            d = B.shape[0]
            k = B.shape[1]
            assert(r == int(k / irrs.dims[alpha]))
         
            B = Matrix(B)
            # V are the non-orthonormalized first vectors => we orthonormalize to 
            # pick v0 - an arbitrary "first" vector of the 1st irrep
            first = 0  # pick v0
            v0 = ColVector(B,first)
            v0 = P * v0 # P * Dg * P is Dg^{\alpha}, easiest to stick it here
            V = Matrix(np.zeros_like(P))
            # formula 2.2.32: V projects on the 1st r vectors space (of this rep)
            for g in range(N):
                Dg = Matrix(self.rep[...,g]) 
                Dg00 = np.conj(v0.H * Dg * v0)[0,0] # just a complex number in 1x1 matrix
                V += (pf * Dg00) * Dg
                
            # B1, the r linearly independent first vectors
            B1 = self.GetOrthonormalBasisForColumns(V*B, doc="First vectors")
            if B1.shape[1] != r: 
                print(f"P_{alpha} matrix")
                PrintMatrix(P)
                print(d,k,r,irrs.dims[alpha])
                print(B1.shape[1], r)
                print("B: FULL basis for the representation including repetitions:")
                PrintMatrix(B, precision=4, max_line_width=250)
                print("VB: first vectors:")
                PrintMatrix(V*B, precision=4, max_line_width=250)
                print("B1: orthonormal basis of first vectors:")
                PrintMatrix(B1, precision=4, max_line_width=250)
                assert(False), f"dimensions don't match {B1.shape[1]} != {r}"
                        
            irrDim = int(irrs.dims[alpha])
            
            B0, Di0g = self.CompleteIrrepFrom1stVec(
                                u0=ColVector(B1,0),
                                irrDim=irrDim)
            
            Bordered = B0.copy()
            # add the replicas of the 1st representation
            for p in range(1,r):
                for i in range(irrDim):
                    vpi = np.zeros((d,1), dtype=np.complex128)
                    for g in range(N):
                        vpi += pf* np.conj(Di0g[i,g]) * (Matrix(self.rep[...,g])*ColVector(B1,p))
                    Bordered = np.append(Bordered, vpi, axis=1)
            U0 = np.append(U0, Bordered, axis=1)
            
        assert(U0.shape[1] == U0.shape[0])
        #print("COMPLETE basis for rep:")
        #PrintMatrix(U0, max_line_width=400)
        sys.stdout.flush()
        det = linalg.det(U0)
        assert(abs(det) >= 1.0e-3), det
        
        irrs.FindFusionCoefficients(set1)
        
        U0 =  Matrix(U0) / det
        CheckIsUnitary(U0)
        return U0
        
        
    def CalculateBlockCharacters(self):
        # save 1/2 of the multiplication operations
        combinedD = Matrix(self.rotD) * Matrix(self.basisChangeD)
        for g in range(self.num_ops):
            self.rep[:,:, g] = combinedD.H * Matrix(self.Dlgs[:,:,g]) * combinedD
            
        self.num_blocks = 0
        self.blocks = np.zeros((self.dim,), np.int32)
        self.blocks[:] = -1
        for g in range(self.num_ops):
            m = self.rep[..., g]
            #PrintMatrix(abs(combinedD))
            for i,j in itertools.combinations_with_replacement(list(range(self.dim)), 2): 
                # here i<=j
                if abs(m[i,j]) > self.blocking_tol:
                    if self.blocks[i] == -1 and self.blocks[j] == -1:
                        self.blocks[i] = self.num_blocks
                        self.blocks[j] = self.num_blocks
                        self.num_blocks = self.num_blocks+1
                    elif self.blocks[j] == -1:
                        self.blocks[j] = self.blocks[i]
                    elif self.blocks[i] == -1:
                        self.blocks[i] = self.blocks[j]
                    else:
                        # they both have assigned blocks - merge the blocks
                        block = min(self.blocks[i], self.blocks[j])
                        for k in range(self.dim):
                            if self.blocks[k] == self.blocks[i] or \
                                self.blocks[k] == self.blocks[j]:
                                self.blocks[k] = block

        # block numbers may be discontiguous, because we merge blocks
        # now we fix them to start from 0 and be contiguous
        new_block_number = {}
        count = 0
        for k in range(self.dim):
            if self.blocks[k] in list(new_block_number.keys()):
                self.blocks[k] =  new_block_number[self.blocks[k]]
            else:
                new_block_number[self.blocks[k]] = count
                self.blocks[k] = count
                count = count + 1
        self.num_blocks = count
        
        # now find the character of each block representation
        self.characters = Matrix(np.zeros((self.num_blocks,self.num_ops),np.complex128))
        for g in range(self.num_ops):
            m = self.rep[..., g]
            for k in range(self.dim):
                self.characters[self.blocks[k], g] += m[k,k]
                
        self.block_dims = [0 for _b in range(self.num_blocks)]
        for k in range(self.dim):
            self.block_dims[self.blocks[k]] += 1 
        
        
        self.chiHchi = self.characters * self.characters.H
        
        # block representations are irreducible iff sum of x*x for all characters is == N_G.
        # Irreducible blocks constitue equivalent representations iff the off-diag element 
        # is non-zero.
        self.is_irr = [abs(self.chiHchi[k,k] - self.num_ops) < self.g_tolerance for k in range(self.num_blocks)]
        
        #print("number of blocks: %d"%self.num_blocks)
        #print("dim=%d, chi=%s"%(self.dim,str(self.chiHchi)))
        #print("is_irr=%s"%str(self.is_irr))       
        
    # sets self.isSymmetryAdapted accordingly
    # also sets the sad_test variable
    def CheckSymmetryAdaptation(self):              
        # sad's columns will contain the symmetry adapted vectors of the block
        self.sad_test = [ np.zeros((self.block_dims[b], self.block_dims[b]), np.complex128) for b in range(self.num_blocks)]
        for g in range(self.num_ops):
            Dg = self.rep[..., g]
            for b in range(self.num_blocks):
                Dgb =  Matrix(Dg[self.Block(b)])
                # find the 1st equivalent irrep (p=1)
                for b0 in range(b+1):            
                    if self.is_irr[b0] and abs(self.chiHchi[b,b0]) >  self.g_tolerance and self.block_dims[b] == self.block_dims[b0]:
                        break
                # the corresponding irrep
                Dgb0 = Matrix(Dg[self.Block(b0)])
                
                # use formulae (2.2.32) (2.2.36) in "Site Symmetries In Crystals" (page 26)
                # to construct the "i-th" vector in the "p-th" copy of the irrep
                # from a 1-st vector of the 1-st copy of the irrepc (Dgb)
                # *IFF* this vector (1st vector of our copy) is adapted to be compatible to the 1st 
                # vector of the 1-st irrep, *THEN* this matrix will be unitary. Otherwise it will be all zero!
                self.sad_test[b] +=  self.block_dims[b] * 1.0/ self.num_ops * np.outer(Dgb0[:,0].H, Dgb[:,0]) 
        
        self.isSymmetryAdapted = True
        for b in range(self.num_blocks):
            if not self.is_irr[b] or not self.IsUnit(Matrix(self.sad_test[b])):
                self.isSymmetryAdapted = False
                #print("*** block %d is not unit, blocks=%s"%(b,str(self.blocks)))
                #PrintMatrix(self.sad_test[b])
                break
            
    def PrintGroup(self):
        for g in range(self.num_ops):
            PrintMatrix(self.rep[:,:,g])
       
    def IsUnit(self, m):
        I = np.eye(m.shape[0], dtype=np.complex128)
        if np.all(abs(m-I) < 5.0e-2):
            return True
        return False
             
    # determine block structure of a matrix that commutes with G (like the Hamiltonian)
    def ApplyWignerTheorem(self):
        
        if not self.isSymmetryAdapted:
            self.MakePariallyAdaptedRep()
        else:
            self.MakeSymmetryAdaptedRep()
            
    def Block(self, b):
        return np.ix_(self.blocks==b, self.blocks==b)

    def BlockOf(self, j):
        b = []
        for i in range(self.dim):
            if self.blocks[i] == self.blocks[j]:
                b.append(i)
        return b
    
    # The advantage of this representation is that 
    # == we do not change the given basis === 
    # we provide the non-diagonal categorization and irrep enumeration as much as possible
    # using symmetry theory.
    # This means that the labeling is the original labeling. But one can have non-diagonal elements
    # even if the actual symmetry-adapted basis is a permutation away from this basis, or even
    # just an phase prefactor away from being symmetry adapted (this should be fixed, for example
    # by allowing to multiply the basis by phases. But what should be the interface?)
    # To find a symmetry adapted basis, use the subclass SymmetryAdaptedRepresentation instead.
    def MakePariallyAdaptedRep(self):
        #self.H = Matrix(np.chararray((self.dim,self.dim)))
        self.offdiag0count = 0 
        rep = 1
        self.H[:,:] = -1
        # this is an application of Schur's lemmas
        for i,j in itertools.combinations_with_replacement(list(range(self.dim)), 2): 
            if self.H[i,j] >=0: continue
            if i == j:
                block_i = self.blocks[i]
                if self.is_irr[block_i]:
                    blk = self.BlockOf(i)
                    for i1,j1 in itertools.product(blk, blk):
                        if i1 == j1:
                            # from Schur's first lemma
                            self.H[i1,j1] = rep # so that it starts at 1
                        elif i1 < j1:
                            # from Schur's second lemma
                            self.H[i1,j1] = 0
                            self.H[j1,i1] = 0
                            self.offdiag0count += 2
                    rep +=1
            else:
                block_i = self.blocks[i]
                block_j = self.blocks[j]
                # both blocks are irreducible. Off diagonals are zero iff the blocks are inequivalent (iff orthogonal)
                if self.is_irr[block_i] and self.is_irr[block_j] and abs(self.chiHchi[block_i,block_j]) < self.chi_tolerance:
                    # from Schur's second lemma
                    # TODO: get here also if the dims of the reps are different - then they are 
                    #       guaranteed to be inequivalent even if the dot product is numerically away from 0
                    # if this is the same block, or the representations are non-equivalent
                    self.H[i,j] = 0
                    self.H[j,i] = 0
                    self.offdiag0count += 2

    def MakeSymmetryAdaptedRep(self):
        assert(self.isSymmetryAdapted), (str(self.blocks),str(self.is_irr)) 
      
        # enter a symmetry adapted matrix - it is completely reduced to diagonal blocks 
        label = 1
        self.offdiag0count = self.dim*(self.dim -1)
        for b in range(self.num_blocks):
            for i in range(self.dim):
                if self.blocks[i] == b: 
                    self.H[i,i] = label
            label += 1
        
        # label equivalent block 
        for b in range(self.num_blocks):
            for b2 in range(b):
                if b2 == b: continue
                if abs(self.chiHchi[b,b2]) < self.chi_tolerance or self.block_dims[b] != self.block_dims[b2]: continue # not equivalent (=> orthogonal)
                # form the diagonal between b, b2, and vice versa
                for i,j in zip(filter(lambda x: self.blocks[x] == b2, list(range(self.dim))),
                                          filter(lambda x: self.blocks[x] == b, list(range(self.dim)))):
                    self.H[i,j] = label
                    self.H[j,i] = label
                    self.offdiag0count -= 2
                label += 1
                               
    def Num0Offdiagonals(self):
        return self.offdiag0count
    
    def HStructure(self):
        maxD = max(np.amax(self.H[:,:]),0)
        HH = np.zeros_like(self.H)
        HH[:,:] = self.H[:,:]
        # first relabel the diagonal
        for i in range(self.dim):
            if HH[i,i] < 0:
                HH[i,i] = maxD + 1
                maxD +=1
        for i in range(self.dim):
            for j in range(self.dim):
                if i == j: continue
                if HH[i,j] < 0:
                    HH[i,j] = maxD + 1
                    maxD +=1
        return HH
    
        
    """ approx_rep: kind of approximation to use for the representation:
            "diag": keep the diagonal only (even if there are nonzero offdiagonals),
                    if there are negative numbers on diagonal, number them after the
                    irreps that were identified.
            "none": keep all off-diagonals, and number them,
            "jj": (J^2) 2 blocks in the J basis, each for the j value
    """    
    def ApproximateRepresentation(self, approx_rep, basisChangeM):
        print(" - using representation approximation:", approx_rep)
        # note that this option may return negative numbers in the rep, which need to be handled according to
        # the definitions above
        if approx_rep == 'none' or approx_rep == 'full':
            if  approx_rep == 'none':
                maxD = max(np.amax(self.H[:,:]),0)
            else:
                maxD = 0
                self.H[:,:] = -1 # make it enumerate the whole matrix
            # number the diagonal first, easier to track in diagonal approximations
            for i in  range(self.dim):
                if self.H[i,i] < 0:
                    self.H[i,i] = maxD + 1
                    maxD +=1

            for i in range(self.dim):
                for j in range(i+1, self.dim):
                    if self.H[i,i] < 0:
                        self.H[i,j] = maxD + 1
                        self.H[j,i] = maxD + 2
                        maxD +=2
            return self.H            
        elif approx_rep == 'jj':
            # this is appropriate only for J basis, which means that dim=2*(2l+1)
            # check that dim is even
            assert(self.dim %2 == 0), "dimension %d should be even for J rep"%self.dim
            ll = (self.dim /2-1)
            l = int(ll //2)
            assert(ll %2 == 0), "dimension should correspond to 2(2l+1)"
            obs = CoupledBasisObs(l)
            J2 = obs.Get("J2")
            J2InBasis = np.array(np.real(np.diag(basisChangeM.H * J2 * basisChangeM)))
            js, jjRep = np.unique(J2InBasis.round(decimals=5), return_inverse=True)
            if len(js) != 2 or abs(js[0]- (l-0.5)*(l+0.5)) > 1.0e-5 or abs(js[1]- (l+0.5)*(l+1.5)) > 1.0e-5:
                raise Exception("chosen basis is not suitable for -Xjj: J^2 is not conserved")
            return np.diag(jjRep+1)        
        elif approx_rep == '0CF':
            # TODO: defined ONLY on the native basis, and when pre-adapt contains J2 and another J axis.
            assert(self.dim %2 == 0), "dimension %d should be even for 0CF rep"%self.dim
            ll = (self.dim /2-1)
            l = int(ll //2)
            assert(ll %2 == 0), "dimension should correspond to 2(2l+1)"
            return np.diag([1]*l+[2]*l+[3]*(l+1)+[4]*(l+1))
        elif approx_rep == "diag":
            HH = np.eye(self.dim, dtype=np.int8)
            maxD = max(self.H[i,i] for i in range(self.dim))
            if maxD < 0: # irreducible blocks only
                maxD = 0
            for i in range(self.dim):
                if self.H[i,i] < 0:
                    maxD +=1 
                    HH[i,i] = maxD 
                else:
                    HH[i,i] = self.H[i,i]
            return HH
        elif approx_rep == 'full-diag':
            HH = np.eye(self.dim, dtype=np.int8)
            for i in range(self.dim):
                HH[i,i] = i+1
            return HH 
            
         
 
class DoubleGroupAdaptedSymmetry(Representation):   
    
    # for spinful groups, when computing the irreducible representations
    # we have to consider the full double-cover of the group
    # because the single-cover is not a complete group
    
    def __init__(self, l, dim, nrel, num_si, irreps, o3group, Dlgs, rot, rotD, basisChangeD, labels):       
        allDlgs = [Matrix(Dlgs[:,:,g]) for g in range(len(o3group))]
        group = irreps.group
        group.ExtendRep(allDlgs)
        
        doubleDlgs = np.zeros(Dlgs.shape[:2]+(len(allDlgs),), dtype=np.complex128)
        for g in range(len(allDlgs)):
            doubleDlgs[:,:,g] = allDlgs[g]
        assert(len(allDlgs) == 2*len(o3group)), (len(o3group),len(doubleDlgs))
        
        Representation.__init__(self, l, dim, nrel, num_si, irreps, group, doubleDlgs,
                                rot, rotD, basisChangeD, labels,
                                analyze=False)

        U0 = self.CalculateSymmetryProjectors(irreps)
        self.basisChangeD = self.basisChangeD * U0
        
        # recalculate the representation
        self.CalculateBlockCharacters()
        
        self.CheckSymmetryAdaptation()
        self.MakeSymmetryAdaptedRep()
       
    
         
class SymmetryAdaptedRepresentation(Representation):   

    def __init__(self, l, dim, nrel, num_si, irreps, o3group, Dlgs, rot, rotD, basisChangeD, labels):
            self.irreps = irreps
            Representation.__init__(self, l, dim, nrel, num_si, irreps, o3group, Dlgs, rot, rotD, basisChangeD, labels)
  
    def ApplyWignerTheorem(self):
        # if this is not completely irreducible, we have to reduce some of the sub-matrices     
        # find all irreducible reps for this group
        
        if self.nrel > 1:
            dgas = DoubleGroupAdaptedSymmetry(self.l, self.dim, self.nrel, 
                        self.num_si, self.irreps, self.group, 
                        self.Dlgs, self.rot, self.rotD, self.basisChangeD, self.labels)
            self.offdiag0count = dgas.offdiag0count
            self.H = dgas.H
            self.basisChangeD= dgas.basisChangeD
            return

        U0 = self.CalculateSymmetryProjectors(self.irreps)
        self.basisChangeD = self.basisChangeD * U0
        self.CalculateBlockCharacters()
        self.CheckSymmetryAdaptation()
        
        if not all(self.is_irr):
            #assert(False) ## not implemented yet
            self.MakePariallyAdaptedRep()
            return # remove
            
        elif not self.isSymmetryAdapted:
            print("* not symmetry adapted,",str(self.is_irr))
            self.AdaptBasis()
                 
        self.MakeSymmetryAdaptedRep()
        
        # TODO: rewrite the labels according to irrep rules
        
        
    def IsUnitary(self, M):
        return np.allclose(M*M.H, np.eye(M.shape[0]), atol=1.0e-2)
    
    # return value is either 0 or unitary
    def GetP11e(self, b, b0):
        e0b = np.zeros((self.sad_test[b].shape[0],), dtype=np.complex128)
        pf = self.block_dims[b] * 1.0/ self.num_ops
        for g in range(self.num_ops):
            Dg = self.rep[..., g]
            Dgb =  Dg[self.Block(b)]
            Dgb000 = Dg[self.Block(b0)][0,0].conj()
        
            for j in range(self.block_dims[b]):
                e0b +=  pf * Dgb000 * Dgb[:,j]
        return e0b

    # b is a block number
    # This returns the unitary  
    def AdaptFirstVector(self, b:int):
        for b0 in range(b+1):            
            if abs(self.chiHchi[b,b0]) >  self.g_tolerance and self.block_dims[b] == self.block_dims[b0]:
                break
            
        # get the first vector
        e0b = self.GetP11e(b, b0)
        # TODO: does it need to be normalized? This may be unnecessary
        e0b /= cnorm(e0b)
        #print("************ e0b:", e0b, b, b0)
        
        Ub = Matrix(np.zeros_like(self.sad_test[b]))
        pf = self.block_dims[b] * 1.0/ self.num_ops
        for g in range(self.num_ops):
            Dg = self.rep[..., g]
            Dgb =  Matrix(Dg[self.Block(b)])
            Dgb0 = Matrix(Dg[self.Block(b0)])
            Ub +=   pf * np.outer(Dgb0[:,0].H, Dgb*(Matrix(e0b).T)) 
        assert(self.IsUnitary(Ub)), Ub
        return Ub
           
    def AdaptBasis(self):
        toFindFirstVector = [not self.IsUnit(Matrix(self.sad_test[b])) for b in range(self.num_blocks)]
                
        U = Matrix(np.eye(self.basisChangeD.shape[0], dtype=np.complex128))
        
        for b in range(self.num_blocks):
            if (toFindFirstVector[b]):
                U[self.Block(b)] = self.AdaptFirstVector(b)
                   
        # change basis by multiplying self.BasisChangeD
        self.basisChangeD = self.basisChangeD * U.T
        
        # recalculate the representation
        self.CalculateBlockCharacters()
        
        