'''
Created on Sep 26, 2019

@author: adler
'''
from portobello.generated.symmetry import Bootstrap
import numpy as np
from portobello.symmetry.local import GetRealSHMatrix, GetCGMatrix, CheckIsUnitary
from portobello.bus.Matrix import Matrix
from ctypes import CFUNCTYPE, c_void_p, c_uint
from portobello.bus.fortran import Fortran
from portobello.symmetry.spins import SpinRotationMatrix
from scipy import linalg

def ReadDl(bs, l:int, op:int):
    dim = 2*l+1
    wigD = np.zeros((dim, dim), dtype=np.complex128)
    l1=l+1
    l2m=l1+l
    ll2m  = l2m*l
    nls   = (l2m-2)*ll2m//3+l1+ll2m
    lm=0
    for m in range(-l,l+1):
        lm += 1
        nls1=nls+m
        for m1 in range(-l,l+1):
            l1m1=l1+m1
            wigD[lm-1,l1m1-1] = bs.Dl[nls1+l2m*m1-1,op]
    return Matrix(wigD)
         
def WriteDj(bs, wigDlJ, l, ii, op):
    iwig0=(8*l**3+12*l**2+10*l+3)//3+l*(2*l+1)*ii
    lm=0
    jj = 2*l + ii
    for m in range(-jj, jj+1, 2):
        lm = lm +1
        nls1=iwig0+(ii+m)//2
        l1m1=0
        for m1 in range(-jj, jj+1, 2):
            l1m1=l1m1+1
            bs.Dj[nls1+(2*l+ii+1)*(ii+m1)//2-1, op] = wigDlJ[lm-1,l1m1-1]
    

# This code goes over all l's and all ops, and builds the J-basis wigner matrices
# The input is the Dl matrices (for e^(theta*L) rotations), read from bs as an array (u in Kutepov's code)
# The output is another flat array, into which the matrices are written.
# This the only place in the code where this kind of construction is made. The code is usually called from
# Frotran through a callback (see below). The calling function in Fortran is called CalculateDjFromDl.
# It is used for:
#   - construction of the Dj matrices for the operation of relativistic code (uj)
#   - by calls from Python, for construction of Dj matrices for various rotations, and local groups
#     in order to find the local representations of projectors.
def PrepareDJfromDL(num_ops : int , lmm : int):
    bs = Bootstrap()
    path = "./bootstrap.core.h5:/D"+str(num_ops)+"_"+str(lmm)
    bs.load(path)
    
    assert(num_ops == bs.num_ops), (num_ops, bs.num_ops)
    assert(lmm == bs.max_l)
    
    # record the spin matrices so that we don't compute them for every l
    spinU = np.zeros((2,2,bs.num_ops),dtype=np.complex128)
    
    #group = AbstractGroup([bs.rots[:,:,g] for g in range(bs.num_ops)])
    
    prevL = [None for _i in range(bs.num_ops)]
    sgns = [1.0 for _i in range(bs.num_ops)]
      
    for l in range(0,bs.max_l+1):
        sh = GetRealSHMatrix(l)
        cg = Matrix(GetCGMatrix(l))
        
        repl = []
        
        for op in  range(bs.num_ops):   
            if l == 0:
                mat, sgn = SpinRotationMatrix(bs.rots[:,:, op])
                spinU[:,:,op] = mat[...]
                sgns[op] = sgn
                
            #theta = np.sum(Matrix(bs.shifts[:,op]) *  Matrix(bs.recip[:,:]))
            #phase = cmath.cos(theta) + cmath.sin(theta) *1j
            #print("PHASE: ", bs.shifts[:,op], bs.recip[:,:],theta, phase)
            phase = 1.0

            Dl = ReadDl(bs, l, op)   
            CheckIsUnitary(Dl)
            
            sp = spinU[:,:,op]
            Dg2 = Matrix(linalg.kron(sp[:,:], sh.H*Dl*sh))
            wigDlJ = phase*cg.H*Dg2*cg
        
            CheckIsUnitary( wigDlJ )
            repl.append(Dl)

            # check that this has 2 reps, one for l-0.5, one for l+0.5
            if not np.all(abs(wigDlJ[2*l:,:2*l]) < 5.0e-6):
                print("Rotation matrix has off diagonals between l-0.5,l+0.5")
                print(wigDlJ[2*l:,:2*l])
                assert(False) # DlJ has off diagonal blocks!

            if not np.all(abs(wigDlJ[:2*l,2*l:]) < 5.0e-6):
                print("Rotation matrix has off diagonals between l-0.5,l+0.5")
                print(wigDlJ[:2*l,2*l:])
                assert(False) # DlJ has off diagonal blocks!

        
            if prevL[op] is not None:                
                if not np.all(abs(wigDlJ[:2*l,:2*l] - sgns[op] * prevL[op]) < 5.0e-8):
                    print(bs.rots[:,:, op])
                    assert(False), "relativistic equation violated (\Xi_{-k}=-r*\sigma \Xi_k)"
            prevL[op] = Matrix(wigDlJ[2*l:,2*l:])
                
                
            if l> 0:
                WriteDj(bs, wigDlJ[:2*l,:2*l], l, -1, op)
            WriteDj(bs, wigDlJ[2*l:,2*l:], l, 1, op)
                      
    bs.store(path, flush=True)
    return 0
    
    
     
callback = CFUNCTYPE(c_void_p, c_uint, c_uint)(PrepareDJfromDL) 
        
registered = False
def RegisterInFortran(reg):
    if not reg:
        fortran = Fortran.Instance()
        fortran.invoke("flapwgroups.mp.register_prepare_dj_cb", callback)
        registered = True
        
RegisterInFortran(registered)

    

    
