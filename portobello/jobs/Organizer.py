#!/usr/bin/env python3

'''
Created on Dec 28, 2019

@author: adler

typical usecase for some computation script:
    
    org = Organizer(shareddir=str(Path.cwd()))
    org.AddWork(...)
    org.AddWork(...)
    org.AddWork(...)
    
    org.wait()
    
This will add parametrized work (including a work directory parameter) to the queue,
which will be executed as JOBS in a round-robin fashion by the ContributorJOB's who are 
associated with this organizer. 
This gives the organizer full control over concurrency, invocation, and termination, as well as the location of 
potentially big files (which do not need to be duplicated).
Also, it provides a way to offload work to more JOBS without restarting everything - 
one just needs to add more contributors, which imeediately get assigned any pending work. This is possible
because no MPI allocations ever change, we just add and remove computing power at the job level.

For more dtails, see the class ContributorJOB.

In order to complete the added work, ContributorJOBs need to be created with the shared directory parameter, for example
by running 

    Q contributor -n<num-mpi-workers> -j<job-name> --for=<job-name-of-organizer>
    
these can be added (or taken down) any time after the organizer starts (within reason).
In fact we added a parameter for Q

    Q  <anything> --with=<num>

that for any script, adds <num> number of contributors automatically pointing to that script, which can be used 
through its lifetime.

    
'''
import os
from pathlib import Path, PurePath

import dill as pickle
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer
from collections import OrderedDict
import time
from random import randint
import shutil
from portobello.deploy import MountTranslation
from filelock import FileLock
from argparse import ArgumentDefaultsHelpFormatter, Namespace
import sys
import functools

from portobello.rhobusta.options import ArgumentParserThatStoresArgv

class Organizer:
    
    class Logger():
        def __init__(self,fn):
            self.f = open(fn, "w")
        def info(self, msg):
            self.f.write(msg+"\n")
            self.f.flush()


    class RegistrationHandler(FileSystemEventHandler):
        def __init__(self, org):
            self.org = org
            org.regpath.mkdir(parents=True, exist_ok=True)            
            org.lockpath.mkdir(parents=True, exist_ok=True)
            obs = Observer()
            obs.schedule(self, str(org.regpath), recursive=False)  
            obs.start() 
            for p in org.regpath.iterdir():
                p.touch()
            
            
        def GetWorkerId(self, event):
            return event.src_path.split("/")[-1]
            
        # this happens when a contributor signs up or refreshes its registration
        def on_created(self, event):

            while self.org.assigning: #Is this fool-proof?
                time.sleep(10)
                print("assigning -- wait a moment to deal with new registration!")

            unique_id = PurePath(event.src_path).name
            reg_lock = self.org.lockpath / (unique_id + "_reg.lock")
            lock = FileLock(reg_lock)
            with lock:
                with Path(event.src_path).open('r') as f:
                    worker_id = self.GetWorkerId(event)
                    msg_dir, expertise_filter, *_rest = f.read().split("\n")
                    self.org.contributors[worker_id] = (msg_dir, expertise_filter)
                    self.org.AssignWork()
            
        # this happens when we find that a contributor hasn't checked-in for a while
        def on_deleted(self, event):

            while self.org.assigning:
                time.sleep(10)
                print("assigning -- wait a moment to deal with new registration!")

            worker_id = self.GetWorkerId(event)
            # also need to consider if work can be re-assigned
            del self.org.contributors[worker_id]  
            if worker_id in self.org.assigned_work:
                work_id = self.org.assigned_work[worker_id]
                del self.org.assigned_work[worker_id]
                self.org.pending_work.insert(0,work_id)
            
            self.org.AssignWork()
            
        # this happens when a contributor checks in - treat it the same as creation
        def on_modified(self, event):
            if  Path(event.src_path).is_dir():
                return # this is the top dir
            self.on_created(event)

    # update the status of work to be done
    class NotificationsHandler(FileSystemEventHandler):
        
        def __init__(self, org, notifpath : Path):
            self.org = org
            notifpath.mkdir(parents=True, exist_ok=True)
                
            obs = Observer()
            obs.schedule(self, str(notifpath), recursive=False)  
        
            obs.start() 
           
        # returns pair (work_id, "ended" "crashed" or "started")
        def GetWorkStatusAndId(self, event):
            status_lock = self.org.lockpath / (PurePath(event.src_path).name + '.lock')
            lock = FileLock(status_lock)
            with lock:
                return event.src_path.split("/")[-1].split('_')
         
        def on_created(self, event):
            work_id, status = self.GetWorkStatusAndId(event)
            
            if status == 'started':
                self.org.pending_work.append(work_id)
                
            elif status == 'crashed':
                print(f"{work_id} crashed!")
                self.org.pending_work.remove(work_id)
                for cid in [cid for cid in self.org.assigned_work if self.org.assigned_work[cid] == work_id]:
                    del self.org.assigned_work[cid]

            else:
                assert(status == 'ended'), event
                self.org.pending_work.remove(work_id)
                
                for cid in [cid for cid in self.org.assigned_work if self.org.assigned_work[cid] == work_id]:
                    del self.org.assigned_work[cid]
                                        
                del self.org.work_description[work_id]

            self.org.AssignWork()
  
    # On restart, some contributors may have left behind notifications which we need to look through to update the organizer state
    def CheckForOldNotifications(self):
        for work_id in self.pending_work:
            crashed = self.orgdir / ".notification/" / f"{work_id}_crashed"
            ended = self.orgdir / ".notification/" / f"{work_id}_ended"
            if crashed.exists():
                print(f" -- work id {work_id} crashed while organizer was not running")

                self.pending_work.remove(work_id)
                for cid in [cid for cid in self.assigned_work if self.assigned_work[cid] == work_id]:
                    del self.assigned_work[cid]

            elif ended.exists():
                print(f" -- work id {work_id} finished while organizer was not running")
                self.pending_work.remove(work_id)
                
                for cid in [cid for cid in self.assigned_work if self.assigned_work[cid] == work_id]:
                    del self.assigned_work[cid]

                if work_id in self.work_description.keys():                      
                    del self.work_description[work_id]

    # gets called periodically to check on liveliness of contributors
    def RemoveOldRegistrations(self):
        currtime = time.time()
        for reg in self.regpath.iterdir():
            regpath = Path(reg)
            time0 = regpath.stat().st_mtime
            if currtime - time0 > 60*self.expected_notification:
                self.logger.info(f"removing old registration {str(regpath)}")
                regpath.unlink()
        self.AssignWork()
        
    def __init__(self, name = 'organizer', shareddir = None, opts = None):
        self.logger = Organizer.Logger("organizer.log")
        if opts is None:
            parser = WithOrganizerParser()
            opts = parser.parse_args([])
        
        if opts.expected_notification < 0:
            import numpy as np
            self.expected_notification = np.inf
        else:
            self.expected_notification = opts.expected_notification
       
        ## ====================== main datastructures ======================
        self.workid = randint(10000000,99999999)
        self.contributors = OrderedDict()     # maps each contributor id to its last registration time
        self.work_description = OrderedDict() # maps work id to the work record by order received
        self.assigned_work = {}    # map contributor id to the assigned work
        self.pending_work = []  # list of work ids by ordered received
                                # work first gets assigned, and then becomes pending (with back notification)
        ## ==================================================================
        self.name = name
        
        cwd = Path.cwd()  # something like /scratch/*adler/
        if shareddir is None:
            shareddir = str(cwd)
        shareddir = MountTranslation.Translate(shareddir)
        

        # advertize my own shared path
        self.logger.info(f"shared directory is {shareddir}")
        
        with (cwd / '.status').open('w') as f:
            f.write("running")
            f.close()

        self.orgdir = Path(shareddir)
        self.restartdir = self.orgdir / ".restart/"
        self.regpath = self.orgdir / ".registration/"
        self.lockpath = self.orgdir / ".lock/"

        if opts.restart:
            self.assigning = False
            noStateFound = self.SaveOrLoadState(write = False)
            
            if not noStateFound:
                print("* Loaded saved organizer state")
                self.CheckForOldNotifications()
            else:
                self.restartdir.mkdir(parents=True, exist_ok=True)  
        else:
            self.restartdir.mkdir(parents=True, exist_ok=True)  

        if not opts.no_clean or noStateFound:
            print("* Cleaning old organizer state")
            for path in [self.regpath, self.lockpath, self.orgdir / ".notification/", self.orgdir / ".queue/"]:
                try:
                    shutil.rmtree(path)
                    print(f" -- removed {path}")
                except:
                    pass

        self.reg   = Organizer.RegistrationHandler(self)
        self.logger.info(f"listening to registerations on {str(self.regpath)}")  
        self.notif = Organizer.NotificationsHandler(self, self.orgdir / ".notification/")
        self.logger.info(f"listening to notifications on {str(self.notif)}")  

        
        
    def GetOrganizerDir(self) -> Path:
        return self.orgdir
        
    def NewWorkId(self) -> str:
        self.workid +=1 
        return "%s%s"%(self.name, self.workid)
        
    def WorkDoesntExist(self, work_record): 
        options_dict_in = work_record['args']['opts'].__dict__

        for work in self.work_description.values():
            mod = work['module'] is None or work['module'].__name__ == work_record['module'].__name__
            func = work['function'] is None or work['function'].__name__ == work_record['function'].__name__
            
            arg = True
            options_dict = work['args']['opts'].__dict__
            for k in options_dict:
                if options_dict_in[k] != options_dict[k]:
                    arg = False
                    break

            if (work['dir'] == work_record['dir'] and mod and func and arg):
                return False
                
        return True
        
    def AddWork(self, workdir, module, function, args):
        workdir_full = str(Path(workdir).resolve())
        wid = self.NewWorkId()
        rec = { 'id'       : wid,
                'dir'      : MountTranslation.Translate(workdir_full),
                'module'   : module,
                'function' : function,
                'args'     : args}

        if self.WorkDoesntExist(rec):
            self.work_description[wid] = rec
            self.logger.info(f"Added work for {wid}: {rec}")
        
        self.AssignWork()
        
    # This does most of the smart work here, it is a scheduler.
    # fill up work for each of the available contributors
    def AssignWork(self):
        self.assigning = True
        self.logger.info(f"AssignWork(): #workers: {len(self.contributors)} #incomplete: {len(self.work_description)}, #assigned: {len(self.assigned_work.values())}")

        for work_id in self.work_description:
            if not work_id in self.assigned_work.values():
                for cont_id in self.contributors:
                    # TODO: take into account the filter
                    if cont_id not in self.assigned_work.keys():
                        # unassigned
                        self.assigned_work[cont_id] = work_id
                        # inform the contributor
                        self.WriteWork(self.work_description[work_id], 
                                       str(Path(self.contributors[cont_id][0]) / work_id))

        self.SaveOrLoadState()
        self.assigning = False

    def SaveOrLoadState(self, write = True):
        noStateFound = False

        mode = 'wb' if write else 'rb'
        to_io = {"work_description" : self.work_description, 
                "contributors" : self.contributors,
                "assigned_work" : self.assigned_work,
                "pending_work" : self.pending_work, 
                "workid" : self.workid}
                
        for key in to_io.keys():
            fname = self.restartdir / (key + '.pickle')
            
            if write or fname.exists():
                with open(fname, mode) as f:
                    if write:
                        pickle.dump(to_io[key],f)
                    else:
                        to_io[key] = pickle.load(f)
            else:
                noStateFound = True

        if not write:
            self.work_description = to_io["work_description"]
            self.contributors = to_io["contributors"]
            self.assigned_work = to_io["assigned_work"]
            self.pending_work = to_io["pending_work"]
            self.workid = to_io["workid"]

        return noStateFound


    # waits (polls) until *all* queued work is done
    def Wait(self):
        self.logger.info(f"wait(), #work: {len(self.work_description.keys())}")
        while len(self.work_description.keys()) > 0:
            self.RemoveOldRegistrations()
            time.sleep(10)
        
    # this deletes the status file, and signals for all the contributors
    # that they should terminate
    # It should be called at the end of execution.
    def Terminate(self):
        self.Wait()
        (self.orgdir / '.status').unlink()
        self.logger.info(f"work is done for {self.name}")  

    # just write a work record in a workfile path
    def WriteWork(self, rec, workfile):
        self.logger.info(f"assigned work {self.assigned_work.values()}")
        self.logger.info(f"assigning work {workfile} {rec}")

        *_rest, unique_id, work_id = workfile.split('/')
        work_lock = self.lockpath / unique_id / (work_id + '.lock')
        lock = FileLock(work_lock)
        with lock:
            with open(workfile, 'wb') as f:
                pickle.dump(rec,f, pickle.HIGHEST_PROTOCOL)
                f.flush()
     
    # for testing
    def ContinueRhobusta(self, atdir):
        self.AddWork(atdir, "portobello.rhobusta.rhobusta", "DFT",  {'num_charge_iterations':10, 'input_file':'Fe-ICSD.cif'})
     

# this is singleton instantiation that may be used for convenience
Instance = None
def AddWork(shareddir, workdir, module, function, args, organizer_opts = None):
    global Instance
    if Instance is None:
        Instance = Organizer(shareddir, opts = organizer_opts)
    Instance.AddWork( workdir, module, function, args)
        
def Wait():
    global Instance
    if Instance is None:
        return
    Instance.Wait()

def Terminate():
    global Instance
    if Instance is None:
        return
    Instance.Terminate()

# note that this decorator should act on functions where
# the options were already formed, so that it has access to 'outsource'
# also, the opts argument should be listed as the FIRST positional argument
# ALL other variables (if exist) must be keyword like.
# A special keywork variable, organizer opts, contains an sys.argv-like list of strings which
# WithOrganizerParser iterprets during initialization of the Organizer
def with_organizer(func):
    @functools.wraps(func)
    def wrapper_with_organizer(*args, **kwargs):
        if len(args)==1:
            opts = args[0]
            if isinstance(opts, Namespace):
                if 'outsource' in opts:
                    funcs = opts.outsource.split(",")
                    if func.__name__  in funcs:
                        # note that this is not an infinite loop, because
                        # the contributor will call the unwrapped code
                        remoteArgs = {'opts':opts}
                        organizer_opts = None
                        if "organizer_opts" in kwargs:
                            parser = WithOrganizerParser()
                            organizer_opts = parser.parse_args(kwargs["organizer_opts"])
                            del kwargs["organizer_opts"]

                        remoteArgs.update(kwargs)
                        AddWork(opts.orgdir, ".", None, func, remoteArgs, organizer_opts = organizer_opts)
                        return 
        # if not met all the above condition - just call the function directly
        func(*args, **kwargs)
    return wrapper_with_organizer


def WithOrganizerParser():
    parser = ArgumentParserThatStoresArgv('dft+g', add_help=True,
                            parents=[],
                            formatter_class=ArgumentDefaultsHelpFormatter)  

    parser.add_argument("-n", "--no-clean", dest = "no_clean",
                        default=True,
                        action="store_false",
                        help = "Don't clean up old organizer / contributor files in directory"
                        )

    parser.add_argument("-r", "--restart", dest = "restart",
                        default=False,
                        action="store_true",
                        help = "Pick up where the last organizer in this directory left off"
                        )

    parser.add_argument("-e", "--expected-notification-time", dest = "expected_notification",
                        type=float,
                        default = 20,
                        help = """after this many minutes, if the organizer has not heard from a contributor 
                            (it hasn't completed a job, e.g.), remove the contributor from the list of active contributors
                            """
                        )
    
    return parser
    
    
def main():

    parser = ArgumentParserThatStoresArgv('dft+g', add_help=True,
                            parents=[],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
      
    
    parser.add_argument("-u", "--using", dest="using",
                      type=str,
                      default=None,
                      help="directory of a contributor/organizer queue to use") 
    
    parser.add_argument("-m", "--module", dest="module",
                      type=str,
                      default=None,
                      help="module to run") 
    
    opts, args = parser.parse_known_args(sys.argv[1:],)
    assert(opts.module is not None)
    assert(opts.using is not None)

    AddWork(str(opts.using), ".", opts.module, function=None, args={'args' : args})
                    
    Wait()


if __name__ == '__main__':
    main()
    

    
    
