import ctypes

spawn_lib = ctypes.CDLL('libexspawn.so', ctypes.RTLD_GLOBAL)

def set_prefix(prefix):
    spawn_lib.set_spool_prefix(str(prefix).encode('ascii'))

def set_spool(spool_file):
    spawn_lib.set_spawn_spool(str(spool_file).encode('ascii'))

