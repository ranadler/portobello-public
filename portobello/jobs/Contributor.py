#!/usr/bin/env python3
'''
Created on Dec 28, 2019

@author: adler

Created on Dec 28, 2019

This is a loosely-coupled command-pattern Master (organizer) - slave (contributor) protocol
between JOBS running on different machines possibly.

The only assumption is that they have a *shared direcoty*, possibly created first by the organizer job
such as an /mnt/<organizer dir>.
The organizer job may do work too, but it should also do the duties described below. It may also be a contributer for
another organizer job.

The communication between organizer/contributor is assumed to be done between their master MPI thread (usually number 0), 
so here we want to formalize how the organizer master thread sends and receives messages to contributor master threads.

Contributors may be added dynamically (unlike a running MPI scheme), in which case they will share the work
created by their organizer. This should be easy to do, as they just need to be pointed to the organizer's 
shared direcotry:
           contributor(<shared dir of the organizer>)
           
The idea is that contributors send (re)registration messages with their unique job id to the organizer (filename is unique
inside a registration directory). Organize manage an (in-memory) TODO list.

The organizer constantly polls for registration requests from the registration directory.
After registration, the contributor's communication  with the organizer is continued in a unique sub-directory created by them 
(pointed to by the registration message), in which they expect description of chunks of work to be executed. 

The organizer assigns work from a TODO list in the following way:
The organizer needs to be poll for messages about beginning and end of work, and manage the state of each chunk of work accordingly.
If a contributor does not re-register for a long time, it may decide that the work was not done, and add it back to 
the TODO list.

After registration, a contributor requests its organizer for work. It then polls its communication directory until it finds 
description of work, and then informs the organizer that it is starting that work.
While it is working, it should inform the orgnaizer that it is alive, once in a while.
When it is done working, it informs the organizer that it is done working and goes back to waiting for work.


Note that the type of calculation done is completely determined by one side - the organizer.
Contributors are COMPLETELY generic (other than having a certain JOB allocation on the MPI server). 
Contributors just need to be started or killed (after the organizer is there). They only know which organizer they are assigned to in runtime.

Therefore contributors can be coded once. All the wisdom about what gets done resides in the organizer, for which
we have a framework.

Directories

-----------
organizer_dir denotes the shared dir.

organizer_dir/.registration/  - contains the registration messages for organizer
                                messages contain (message_dir, expertise_filter)
                                
organizer_dir/.queue/XX       - contains the work pending for worker with identifier XX (message dir)
                                messages are pickled structures described in Organizer.AddWork
                                
organizer_dir/.notification   - work notifications for organizer (work started or done)  
                                messages are just empty files of the form *_started, *_ended
                                
organizer_dir/.status         - should contain "running" iff it is not done: written and deleted by the organizer

TODO: make all dirs and .status have the organizer name

@author: adler
'''

import getpass
import importlib
import os
import traceback
import dill as pickle
import re
import subprocess
import sys
import time
from optparse import OptionParser
from pathlib import Path, PurePath
from subprocess import call
from sys import stdout
from runpy import run_module

from filelock import FileLock
from portobello.bus.persistence import Store
from portobello.deploy import MountTranslation
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer
from argparse import ArgumentDefaultsHelpFormatter

from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.options import ArgumentParserThatStoresArgv


class ContributorJob:
    
    def __init__(self, organizer_dir: str, max_contributions : int = 100, unique_id=None, expertise_filter=".*"):
        self.num_contributions = 0
        self.queue = []
        self.error=False
        
        # find my unique job id
        if unique_id is None:
            cwd = Path.cwd()
            *_rest, self.unique_id = str(cwd).split("/")
        else:
            self.unique_id = unique_id
            
        print("* my unique id: %s" % self.unique_id)
        stdout.flush()
        

        # create unique dir (we want it to be at the organizer's dir, in case we disappear)  
        self.orgdir = Path(organizer_dir)
        self.message_dir = Path.cwd() / ".queue" / self.unique_id 

        # the message dir should not have existed - for jobs registering on the queue
        # but we don't enforce it to make local testing easier
        self.message_dir.mkdir(parents=True, exist_ok=True)
        
        self.todo = None
        
        # TODO: translate message dir to mounted form, which others can use       
        print("* listening for messages on %s"% str(self.message_dir))
 
        self.Register(expertise_filter)
            
        notifydir =  self.orgdir / ".notification"
        lockdir = self.orgdir / ".lock"
        
        class TODOWorkFileCreated(FileSystemEventHandler):
            def __init__(self, contributor): # : ContributorJob):
                FileSystemEventHandler.__init__(self)
                self.contributor = contributor
                #self.mylog = Path(".log").open("w")
                
            def on_created(self, event):
                ct = self.contributor
                if ct.num_contributions >= max_contributions:
                    # terminate
                    print("* stopping")
                    ct.obs.stop()
                    return
           
                work_id = event.src_path.split("/")[-1]
                print("new work id:", work_id)
                
                # inform the ogranizer work is pending
                work_started_notify = notifydir / (work_id + '_started')
                work_started_notify_lock = lockdir / (work_id + '_started.lock')
                lock = FileLock(work_started_notify_lock)
                with lock:
                    if not work_started_notify.parent.exists():
                        print("** organizer still has not created notifications dir - creating it first")
                        work_started_notify.parent.mkdir(parents=True, exist_ok=True) 
           
                 
                    with work_started_notify.open('w') as f:
                        f.write(str(time.time())+ '\n') # TODO: write in human readable format
                        f.close()
                    
                try :
                    # do the given work
                    ct.DoWork(event.src_path)
                    ct.num_contributions += 1
                    
                finally:
                    # delete the work file 
                    Path(event.src_path).unlink()
                    
                    if ct.error:
                        work_crashed_notify = notifydir / (work_id +'_crashed')
                        work_crashed_notify_lock = lockdir / (work_id +'_crashed.lock')
                        lock = FileLock(work_crashed_notify_lock)
                        with lock:
                            with work_crashed_notify.open('w') as f:
                                f.write(str(time.time())+ '\n') # TODO: write in human readable format
                                f.close()
                        ct.error = False

                    else:
                        # inform the organizer work is done
                        work_ended_notify = notifydir / (work_id +'_ended')
                        work_ended_notify_lock = lockdir / (work_id +'_ended.lock')
                        lock = FileLock(work_ended_notify_lock)
                        with lock:
                            with work_ended_notify.open('w') as f:
                                f.write(str(time.time())+ '\n') # TODO: write in human readable format
                                f.close()
                        
                stdout.flush()
                                  
        #if self.OrganizerNotRunning():
        #    print("* organizer not running - terminating")
        #    return
            
        self.obs = Observer()
        self.handler = TODOWorkFileCreated(self)
        self.obs.schedule(self.handler, str(self.message_dir), recursive=True)  
        stdout.flush()
        self.obs.start()
        for p in self.message_dir.iterdir():
            p.touch()

    # send registration message to organizer with my id
    def Register(self, expertise_filter):
        regdir = self.orgdir / ".registration"
        lockdir = self.orgdir / ".lock"
        self.lockworkdir = lockdir / self.unique_id
        my_registration = regdir / self.unique_id
        my_registration_lock = lockdir / (self.unique_id + "_reg.lock")
  
        if not my_registration_lock.parent.exists():
            my_registration_lock.parent.mkdir(parents=True, exist_ok=True)
        if not self.lockworkdir.exists():
            self.lockworkdir.mkdir(parents=True, exist_ok=True)
        lock = FileLock(my_registration_lock)
        with lock:
            if not my_registration.parent.exists():
                print("** organizer still has not created registration dir - creating it first")
                my_registration.parent.mkdir(parents=True, exist_ok=True) 
                                           
            with my_registration.open('w') as f:
                trans_message_dir = MountTranslation.Translate(str(self.message_dir))
                f.write(trans_message_dir + '\n')
                f.write(expertise_filter) # TODO: this will contain a filter of possible work contributor can do
                f.close()
    
    def DeRegister(self):
        regdir = self.orgdir / ".registration"
        lockdir = self.orgdir / ".lock"
        self.lockworkdir = lockdir / self.unique_id
        my_registration = regdir / self.unique_id
        my_registration_lock = lockdir / (self.unique_id + "_reg.lock")
        lock = FileLock(my_registration_lock)
        with lock:
            my_registration.unlink()
    
    def OrganizerNotRunning(self):
        return not (self.orgdir / '.status').exists()  
        
    def DoWork(self, todo_path : str):
        while self.todo is not None:
            time.sleep(10)
       
        work_item = PurePath(todo_path).name
        work_lock = self.lockworkdir / (work_item + '.lock')
        lock = FileLock(work_lock)
        with lock:
            with Path(todo_path).open('rb') as f:
                try:
                    self.todo = pickle.load(f)
                    while self.todo is not None:
                        time.sleep(10)
                except Exception as e:
                    print("* Could not process work %s: %s"%(todo_path,str(e)))


def ContributorMainLoop(opts):

    print(f"* Running ContributorJob(dir={opts.org_dir})")
    max_contributions = 100 
    expertise_filter=".*"
    c=ContributorJob(opts.org_dir, max_contributions=max_contributions, expertise_filter=expertise_filter, unique_id = opts.unique_id)

    count_tries = 0
    registered = False
    MAX_REGISTER_TRIES = 1000

    while c.num_contributions < max_contributions:
    
        if c.OrganizerNotRunning():
            if registered:
                print("* terminate sent from organizer")
                break
            count_tries += 1
            if count_tries > MAX_REGISTER_TRIES:
                print("* organizer not running - terminating")
                break
        else:
            count_tries = 0   
            c.Register(expertise_filter)  # notify the organizer we are still here
            registered = True

        if c.todo is None:
            time.sleep(10)
        else:
            _id = c.todo['id']
            indir = c.todo['dir']
            module_name = c.todo['module']
            function_name = c.todo['function']
            args = c.todo['args']
            while c.error: # let errors be propagated before the next iteration of work
                time.sleep(10)
        
            # if asked to call a specific function
            if function_name is not None:  #This side -- oustource w/ the organizer decorator
                if module_name is not None: 
                    mod = importlib.import_module(module_name)
                    method = getattr(mod, function_name)
                else:
                    module_name = ".."  # for print
                    method = function_name # it is already unpickled

                cwd = Path.cwd()
                
                try:
                    # make and get into the run directory
                    print(f"* Running in {indir}: {module_name}.{function_name}({args})", flush=True)
                    Path(indir).mkdir(parents=True, exist_ok=True)
                    os.chdir(str(indir))
                    
                    opts = args['opts']
                    allout = "./all.out"
                    if hasattr(opts, 'dir'):
                        allout = f"{opts.dir}/all.out"
                    del opts.outsource
                    opts = str(opts)

                    # can't split this up -- python complains about length of string with newlines
                    cmd = fr"""{sys.executable} -c "from {method.__module__} import {function_name.__name__}; from argparse import Namespace; opts = {opts}; {function_name.__name__}(opts)"  >> {allout} 2>&1"""

                    subprocess.run(cmd, shell = True)
                    
                    print("* after run", flush=True)
                    
                except Exception as e:
                    c.error = True
                    print(traceback.format_exc())
                    print(f"* Could not process work: {str(e)}")

                finally:
                    os.chdir(str(cwd))
                    c.todo = None
            else: #TODO: This side -- outsource w/out the organizer decorator -- will break if a contributor picks up multiple jobs. 
                # call the module itself (like the main code)

                cwd = Path.cwd()
                
                try: 
                    # make and get into the run directory
                    print(f"* Running in {indir}: {module_name}.__main__({args})", flush=True)
                    Path(indir).mkdir(parents=True, exist_ok=True)
                    os.chdir(str(indir))

                    # modify sys args : watch for problems with this
                    sys.argv = [module_name] + args['args']
                    run_module(module_name, run_name='__main__')

                    print("* after run", flush=True)

                except Exception as e:
                    print(traceback.format_exc())
                    print(f"* Could not process work: {str(e)}")

                finally:
                    os.chdir(str(cwd))
                    c.todo = None
                    
    c.DeRegister()
    
if __name__ == '__main__':
    
    
    parser = ArgumentParserThatStoresArgv('contributor', add_help=True,
                            parents=[GetMPIProxyOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
    
    # unused (to participate in qsub)    
    parser.add_argument("-d", "--at", dest="org_dir", default=None,
                      type=str, help="points to the organizers communication directory")
 
    parser.add_argument("-o", "--for", dest="org_job_name", default=None,
                      type=str, help="points to the organizer job name")

    parser.add_argument("-i", "--id", dest="unique_id", default=None,
                      type=str, help="unique id of the contributor")

    opts, _args = parser.parse_known_args(sys.argv[1:])
    
    if opts.org_dir is None:
        assert(opts.org_job_name is not None)
        # find the organizer by name
        qs = subprocess.check_output(["/opt/sge6/bin/lx24-amd64/qstat"], shell=True)
        uname = getpass.getuser()
        for line in qs.decode('utf-8').split("\n"):
            entry = re.split("\s+", line)
            # entries start at 1 because of a nifty '' field in the beginning. why is it there?
            if len(entry) > 1 and entry[3] == opts.org_job_name and entry[4] == uname:
                machine = entry[8].split("@")[-1]
                jobnum = entry[1]
                opts.org_dir = "/mnt/%s/%s%s/" % (machine,jobnum,uname)
                break
 
    assert(opts.org_dir is not None and opts.org_dir != "")
    
    mpi = MPIContainer(opts)
    if mpi.IsMaster(): ContributorMainLoop(opts)
