
import functools

def jit(nopython=True):
    def decorator(func):
        return func
    return decorator