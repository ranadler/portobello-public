
import numpy as np
from typing import List
from portobello.bus.Matrix import Matrix


class DIIS:
    """
        Implementation of the DIIS convergence acceleration algorithm.
        see https://aip.scitation.org/doi/pdf/10.1063/1.449880 for details
    """
    def __init__(self, initial_v, example_err, NHistory=10, eps=0.1, converged_eps=1.0e-6, admix=0.05, noop=False) -> None:
        self.SetOptions(eps=eps, converged_eps=converged_eps, admix=admix, noop=noop)        
        self.N = NHistory
        self.n = 0
        self.last_v = np.copy(initial_v)

        # "errors" are kept in N rows
        self.E = Matrix(np.zeros(shape=(self.N, example_err.shape[0]), dtype=np.complex128))
        # offsets
        self.V = Matrix(np.zeros(shape=(self.N, self.last_v.shape[0]), dtype=np.complex128))
        self.inDIIS = False

    def SetOptions(self, eps=0.1, converged_eps=1.0e-6, admix=0.05, noop=False):
        self.eps = eps
        self.admix = admix
        self.noop = noop
        self.converged_eps = converged_eps

    def Build_B_C(self):
        # B matrix is the overlaps between E's, extended with vectors of 1's and 0 at the corner
        self.B = Matrix(np.zeros(shape=(self.n+1, self.n+1), dtype=np.complex128))
        self.B[self.n,:] = 1.0
        self.B[:,self.n] = 1.0
        self.B[self.n,self.n] = 0.0

        # C matrix (vector): (0,0,0 .. ,1).T
        self.C = Matrix(np.zeros(shape=(self.n+1,1), dtype=np.complex128))
        self.C[self.n, 0] = 1.0

    def CalculateWeights(self, maxerr):
        self.Build_B_C()
        for i in range(self.n):
            for j in range(i, self.n):
                self.B[i,j] = np.vdot(self.E[i,:self.n], self.E[j,:self.n])
                if i!=j: self.B[j,i] = np.conj(self.B[i,j])
            ff = self.B[i,i]
            self.B[i,:] /= ff
        try:
            weights = np.real( self.B.I * self.C )[:self.n, :1]
        except np.linalg.LinAlgError:
            for i in range(self.n+1):
                self.B[i,i] += 1j * 1.0e-14
            self.step = "diis*"
            weights = np.real( self.B.I * self.C )[:self.n, :1]

        return weights.T / np.sum(weights)

    def UpdateLatest(self, err, v):
        self.E[self.n,:] = err[:]
        self.V[self.n,:] = v[:]

    def AdjustVector(self, next_v, err):
        # save last v
        prev_v = np.copy(self.last_v)
        maxerr = np.amax(np.abs(err))

        if maxerr < self.converged_eps:
            return "done", None, maxerr
        
        self.step = ""
        if not self.inDIIS and maxerr > self.eps or self.noop:
            # not yet ready to apply DIIS, error above threshold or not enough data yet
            self.last_v[:] = self.admix * next_v + (1.0-self.admix) * prev_v  
            self.step = "admix"
        else:
            self.inDIIS = True
  
        self.UpdateLatest(err, next_v) # very important it is next_v here and not self.next_v (which is already updated)
            
        if self.inDIIS:
            self.n += 1
            if self.n == self.N:
                self.inDIIS=False # stop DIIS
                self.n = 0

            if maxerr > 20.0: # some trouble
                self.n = 0
                # rollback
                self.last_v[:] =  prev_v  
                print("-- resetting DIIS due to high error")
                self.inDIIS = False
            if self.n < 2:
                self.last_v[:] = self.admix * next_v + (1.0-self.admix) * prev_v 
                self.step = "admix"
            else:
                self.step = "diis"
                weights = self.CalculateWeights(maxerr)
                #print(f"weights={weights}")
                self.last_v[:] = weights * Matrix(self.V[:self.n,:])
          

        return self.step, np.copy(self.last_v), maxerr #np.amax(np.abs(self.last_v - prev_v))

