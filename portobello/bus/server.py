'''
Created on Apr 23, 2018

@author: adler
'''

import os
import cherrypy
import cpcgiserver

bin_dir = '/home/adler/git/integrated-CM-code/bin/Debug/'

def main():
    print("my directory is:", bin_dir)
    config = {
        "global": {
            "server.socket_host": "0.0.0.0",
            "server.socket_port": 8080,
        },
        "/": {
            "tools.cgiserver.on": True,
            "tools.cgiserver.dir": bin_dir,
            "tools.cgiserver.base_url": "/",
            "tools.cgiserver.handlers": {"wannier-main": "/bin/cat"},
        }
    }
    app = cherrypy.Application(None, config = config)
    cherrypy.quickstart(app, config = config)

if __name__ == "__main__":
    main()