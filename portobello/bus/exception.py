'''
Created on May 4, 2018

@author: adler
'''
from portobello.bus.fortran import Fortran
from builtins import Exception, exit
from ctypes import *
import sys
import traceback
import inspect

class FortranException(Exception):
    pass

def ThrowExceptionInPython(s):
    #print "In Python --- throwing FortranException"
    #raise  FortranException(s)
    print("Exception raised by fortran:", s)
    #traceback.print_stack()
    stack =  inspect.stack()
    for s in stack:
        print(s)
    exit(1)
    raise FortranException(s)
    
     
callback = CFUNCTYPE(c_int, c_char_p)(ThrowExceptionInPython) 

registered = False

def RegisterInFortran(reg):
    if not reg:
        fortran = Fortran.Instance()
        fortran.invoke("exception.mp.register", callback)
        registered = True
        
RegisterInFortran(registered)
