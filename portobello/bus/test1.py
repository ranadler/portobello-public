'''
Created on May 18, 2018

@author: adler
'''

from pygen.example import Simple
from bus.persistence import Store

if __name__ == '__main__':
    s = Simple()
    s.m = 3
    s.n = 5
    s.allocate()
    
    s.d[0,:] = 1.0
    s.d[0,1] = 10.0
    s.d[1,:] = 2.0
    s.d[2,:] = 3.0
    
    print("MATRIX1 IN PYTHON")
    print(s.d[...])
    
    
    path = "/tmp/simple.hdf5:/simple"
    s.store(path)
    
    
    s.SimpleCall(path)
    
    #s = Simple()
    s.load(path)
    
    print("MATRIX2 IN PYTHON")
    print(s.d[...])
    
    Store.Singleton().Flush()