'''
Created on Apr 17, 2018

    
    Abstracted interface to Hdf5, which allows loading and saving _objects which are
    subclasses of Persistent

@author: adler
'''
from pathlib import Path
import h5py
from enum import Enum
import numpy as np
import json
import numbers
from datetime import datetime
from ctypes import CFUNCTYPE, c_char_p, c_uint, c_void_p, c_ulong
from portobello.bus.fortran import Fortran
from h5py._hl.files import File
from h5py._hl.group import Group
#from builtins import KeyError

base_module = "portobello.generated"
datetime_cformat = "%d %b %Y %H:%M:%S"

if h5py.__version__.split(".")[0] == '2':
    FIDTYPE = c_uint
else: # version 3 has long
    FIDTYPE = c_ulong


class Store:
    def __init__(self):
        self.file_ids = {}
    
    @staticmethod
    def Singleton():
        return singleton_store_

    # note: logical name is in ascii (should be same as in fortran), file_name is unicode
    def Associate(self, logical_name, file_name, create=False):
        fortran = Fortran.Instance()
        is_core = file_name.find('.core.') != -1
        args = {}
        if (is_core): 
            assert(create)
            args = {'driver': 'core', 'backing_store' : False}
        
        if create:
            #print "*** PY truncating or creating file", file_name
            fid = h5py.File(file_name, 'w', **args)
        else:
            try:
                #print "*** PY opening file", file_name
                fid = h5py.File(file_name, 'a')
            except OSError:
                try: 
                    # in some cases, if there is already a writer process, try just reading
                    fid = h5py.File(file_name, 'r')
                except:
                    print(f"h5py cannot open file {file_name}")
                    raise
                
        self.file_ids[logical_name] = fid
        fortran.invoke("persistence.mp.assignfrompython", FIDTYPE(fid.id.id),file_name)
        
    def Fetch(self, path_str):
        pp = PersistentPath(path_str, self)  
        return pp
     
    @staticmethod
    def FlushAll():
        Store.Singleton().Flush()
      
    def Flush(self):
        for fid in list(self.file_ids.values()):
            fid.flush()
            
    def GetFileId(self, file_name, create=False):
        logical_name = file_name.encode('ascii') # make sure it is the same as in fortran
        if logical_name not in self.file_ids:
            self.Associate(logical_name, file_name, create)
        fid =  self.file_ids[logical_name]
        try:
            # check validity of the file id - it may become invalid by claling Refresh()
            _root = fid["/"]
        except:
            #print "**** Python file-id for %s is invalid, re-obtaining it"%logical_name
            self.Associate(logical_name, file_name, create)
            fid =  self.file_ids[logical_name]
        return fid
            
    
    def FlushFile(self, file_name):
        logical = file_name.encode('ascii')
        fid = self.file_ids[logical]
        fid.flush()
        
    def CloseFile(self, file_name):
        file_name = file_name.split(":")[0]
        logical = file_name.encode('ascii')
        if logical in self.file_ids:
            #print(" ------------- deleting logical file ", logical)
            fid = self.file_ids[logical]
            # Note: Fortran still may have the file id in its file_ids data structure
            # which is why we check validity before using it
            del self.file_ids[logical]
            fid.close()
            del fid

    def ClearAll(self):
        logicals = [k for k in self.file_ids.keys()] # copy the keys
        for logical in logicals:
            fid = self.file_ids[logical]
            del self.file_ids[logical]
            fid.close()
            del fid
        self.file_ids = {}
        
    class HDF5FileOverride(File):
        def __init__(self, oid):
            self._swmr_mode = False
            Group.__init__(self,oid)
        
    # this static methods gets notified whenever fortran opens a new file
    @staticmethod
    def PythonCBForOpen(fid, logical_name):
        h5id = h5py.h5f.FileID(fid) # maybe fid needs to be converted to c int
        h5Obj = Store.HDF5FileOverride(h5id)  
        singleton_store_.file_ids[logical_name] = h5Obj
        #print("------- Python notified of open:", logical_name)
           
        
        
callback = CFUNCTYPE(c_void_p, FIDTYPE, c_char_p)(Store.PythonCBForOpen) 
        
registered = False
def RegisterInFortran(reg):
    if not reg:
        fortran = Fortran.Instance()
        fortran.invoke("persistence.mp.register_open_cb", callback)
        registered = True
        
RegisterInFortran(registered)

singleton_store_ = Store()
    
    
class PersistentPath:
    def __init__(self, path_str, store=Store.Singleton()):
        self.path_str = path_str
        self.store = store
    def GetSub(self, subpath):
        return PersistentPath(self.path_str + '/' + subpath, self.store)
    
    # idx is multi-dim index
    def GetSubIdx(self, subpath, idx, create=False):
        idx_str = str(idx[0])
        if len(idx) > 1:
            idx_str = str(idx)
        pp = PersistentPath(self.path_str + '/' + subpath, self.store)
        pp.FetchGroup(create) # possibly create it
        return pp.GetSub(idx_str)
    
    # note that the path need not exist at any level - it will be created
    #     and it could be muti-level 
    def FetchGroup(self, create=False, checkExists=False):
        s = self.path_str.split(':')
        if checkExists and not Path(s[0]).exists():
            raise Exception(f"file does {s[0]} not exist")
        fid = self.store.GetFileId(s[0], create)
        gname = s[1]
        if create:
            group = fid.require_group(gname)
        else: 
            if checkExists: 
                if gname not in fid:
                    raise KeyError(f"object {gname} does not exist")
            group = fid[gname]
        return group
    def Flush(self):
        s = self.path_str.split(':')
        self.store.FlushFile(s[0])
        
    def Disconnect(self):
        s = self.path_str.split(':')
        self.store.CloseFile(s[0])
        
    #def __del__(self):
    #    self.Disconnect()
            
        
        
class AttributeTypes(Enum):
    INT = 1,
    FLOAT = 2,
    COMPLEX = 3,
    DATE = 4,
    STRING = 5,
    BOOLEAN = 6
    
class Persistent():
    def __init__(self):
        self.__InitMetadata__(False)
        
    def __InitMetadata__(self, _superclass):
        self._attributes = {}
        self._objects = {}  # for each object name, its type name
        self._datasets = {}
        self._objars = {}
        self._ps = None
        self._group = None
        
        
    def connect(self, pp, create=False, checkExists=False):
        if self._ps:
            self.disconnect()
        self._group = pp.FetchGroup(create, checkExists=checkExists)
        self._ps = pp.store
    
    def disconnect(self):
        if self._ps:
            # h5py does not allow closing / flushing groups
            self._ps.Flush()
            self._ps = None
            self._group = None
            # TODO: also disonnect sub-objects
        
    def store(self, pp, flush=False):
        if isinstance(pp, str):
            pp = PersistentPath(pp)
        elif isinstance(pp, bytes):
            pp = PersistentPath(pp.decode('utf-8'))
        #print "*** storing ", pp.path_str
        self.connect(pp, create=True)
        for att in list(self._attributes.keys()):
            val = getattr(self, att)
            if self._attributes[att] == AttributeTypes.DATE:
                self._group.attrs[att] = val.strftime(datetime_cformat)
            elif self._attributes[att] == AttributeTypes.STRING:
                if isinstance(val, str):
                    val = val.encode('ascii')
            self._group.attrs[att] = val
        for dsname in list(self._datasets.keys()):
            val = getattr(self, dsname)
            assert(val is not None), "dataset is None: %s" % dsname
            val = np.transpose(val)
            #print "** storing", dsname, val.shape
            
            try:
                if not val is None:
                    ds = self._group.require_dataset(dsname, shape=val.shape, dtype=val.dtype)  #TODO: possibly check compatibility of type?
                    ds[...] = val[...]
            except TypeError as e:
                print("cannot store dataset %s, exception: %s" %(dsname, e))
            
        for objname in list(self._objects.keys()):
            val = getattr(self, objname)
            assert(val is not None), "object is None: %s" % objname
            val.store(pp.GetSub(objname))
        for objar in list(self._objars.keys()):
            val = getattr(self, objar)
            if not val is None:
                for idx,obj in np.ndenumerate(val): # we assume numpy object arrays
                    obj.store(pp.GetSubIdx(objar, idx, create=True))
        if flush:
            pp.Flush()
        return self
   
                
            
    # disconnect - so that the content is re-read if written by another process
    #     for now the file should not be shared in the same process with Fortran
    def load(self, pp, validate=False, disconnect=False, checkExists=False):
        if isinstance(pp, str):
            pp = PersistentPath(pp)
        elif isinstance(pp, bytes):
            pp = PersistentPath(pp.decode('utf-8'))
        self.connect(pp, checkExists=checkExists)
        self.init() # set defaults first
        for att in list(self._attributes.keys()):
            try:
                if self._attributes[att] == AttributeTypes.DATE:
                    setattr(self, att, datetime.strptime(self._group.attrs[att], datetime_cformat))
                elif self._attributes[att] == AttributeTypes.STRING:
                    val = self._group.attrs[att]
                    # read bytes that were written by python2 in previous versions
                    if isinstance(val, bytes):
                        val = val.decode('utf-8')
                    assert(isinstance(val,str))
                    setattr(self, att, val)
                else:
                    setattr(self, att, self._group.attrs[att])
            except Exception as e:
                # if not validating -it will have the previous value (which is the default, unless it was changed)
                if validate:
                    print("Cannot read attribute %s: %s" % (att,e))
                    raise
                
        for dsname in list(self._datasets.keys()):
            try:
                ds = self._group[dsname]
                #print "      *** loading", dsname, ds.shape
                setattr(self, dsname, np.transpose(ds))  #TODO: checktype?
            except:
                if validate:
                    print("dataset missing %s, skipping it"%dsname)
                    raise
                
        for objname in list(self._objects.keys()):
            clsname = self._objects[objname][1]
            namespace =  self._objects[objname][0]
            module =__import__(base_module + "." +namespace, globals(), locals(), [clsname])
            class_ = getattr(module, clsname)
            obj = class_()
            try:
                obj.load(pp.GetSub(objname), validate)
            except KeyError:
                if validate:
                    print(("sub object "+objname+ " doesn't exist or incomplete"))
                    raise
                # this will be an empty (default) object if not read from the disk - not None
            setattr(self, objname, obj)
            
        for objar, meta in self._objars.items():
            (namespace, clsname), shape_spec = meta 
            module =__import__(base_module + "." +namespace, globals(), locals(), [clsname])
            class_ = getattr(module, clsname)
            shape = eval(shape_spec)
            # for now we support only 1-dim arrays
            arr = np.full(shape, fill_value=None, dtype=object)
            # set the content of array to empty objects
            for idx,obj in np.ndenumerate(arr): # we assume numpy object arrays
                obj = class_()
                try:
                    obj.load(pp.GetSubIdx(objar, idx), validate=validate)
                except KeyError:
                    if validate:
                        print(("sub object "+objar+str(idx)+ " doesn't exist or incomplete"))
                        raise
                    # this will be an empty (default) object if not read from the disk - not None
                SetNpArray(arr, idx, obj)
            setattr(self, objar, arr)        
        if disconnect:
            pp.Disconnect() 
        return self
            
    @classmethod
    def Refresh(cls, pp):
        if int(h5py.__version__.split(".")[0]) > 2:
            return # no need for this in h5py 3
        if isinstance(pp, list):
            for pp0 in pp:
                cls.Refresh(pp0)
            return
        if isinstance(pp, str):
            pp = PersistentPath(pp)
        elif isinstance(pp, bytes):
            pp = PersistentPath(pp.decode('utf-8'))
        pp.Disconnect()
        
        
    def allocate(self, recursive=False, validate=False):
        for dsname, meta in self._datasets.items():
            dt, shape_spec = meta
            shape = eval(shape_spec)
            # TODO: throw exception if the shape is not ready (has 0)
            #print "shape of", dsname, ":", shape
            val =  np.zeros(shape, dt)
            setattr(self, dsname, val) 
        for objar, meta in self._objars.items():
            (namespace, clsname), shape_spec = meta 
            module =__import__(base_module + "." +namespace, globals(), locals(), [clsname])
            class_ = getattr(module, clsname)
            shape = eval(shape_spec)
            # TODO: throw exception if the shape is not ready (has 0)
            arr = np.full(shape, fill_value=None, dtype=object)
            # set the content of array to empty objects
            for idx,_ in np.ndenumerate(arr): # we assume numpy object arrays
                SetNpArray(arr, idx, class_())
            setattr(self, objar, arr)
        # Note: We don't want to do this recursively.
        # after calling this routine, the object should be ready to save
        # but it will be empty, and it's sub-objects may need allocation too.
         
    def fix(self):
        # try to fix an object to be consistent with its metadata
        None
                        
    def init(self):
        # create all attributes and initialize to 0
        # Create all objects recursively and initialize them
        # set all data sets, objars to NULL
        for att,scalar_type in self._attributes.items():
            if scalar_type ==  AttributeTypes.INT:
                setattr(self, att, np.int32(0))
            elif scalar_type ==  AttributeTypes.FLOAT:
                setattr(self, att, np.float64(0.0))
            elif scalar_type ==  AttributeTypes.COMPLEX:
                setattr(self, att, np.complex128(0.0,0.0))
            elif scalar_type ==  AttributeTypes.DATE:
                setattr(self, att, datetime.fromtimestamp(0))
            elif scalar_type ==  AttributeTypes.STRING:
                setattr(self, att, "")
            elif scalar_type ==  AttributeTypes.BOOLEAN:
                setattr(self, att, False)
        for objname in list(self._objects.keys()):
            clsname = self._objects[objname][1]
            namespace =  self._objects[objname][0]
            module =__import__(base_module + "." +namespace, globals(), locals(), [clsname])
            class_ = getattr(module, clsname)
            obj = class_()  # this calls init recursively
            setattr(self, objname, obj) 
        for dsname in list(self._datasets.keys()):
            setattr(self, dsname, None)  
        for objar, _ in self._objars.items():
            setattr(self, objar, None)

    # TODO: since we create all fields in init, we can't really implement this easily
    def updateStore(self, pp, validate=False):
        # this functionality does not exist in fortran
        # we update only the fields that were set in the object
        # validate - require that they all exist in the file
        None
        
    # write as json string
    def tojson(self):
        #return PersistentObjEncoder().encode(self)
        return json.dumps(self, cls=PersistentObjEncoder, indent=4)

    def FromOther(self, other):
        kwargs = other.__dict__
        for key in kwargs:
            if key not in ['_datasets','_attributes']:
                setattr(self, key, kwargs[key])
        return self
  
        
#TODO: is this the correct order of indexing ??
# mdidx is multi dimensional index
#note that this is slow, we don't expect object arrays to be too large
def SetNpArray(arr, mdidx, val):
    sarr = arr
    for i in range(len(mdidx)-1):
        sarr = sarr[mdidx[i]]
    sarr[mdidx[len(mdidx)-1]] = val
     
# mdidx is multi dimensional index
def GetNpArray(arr, mdidx, val):
    sarr = arr
    for i in range(len(mdidx)):
        sarr = sarr[mdidx[i]]
    return sarr
    
# deal with all the fields that we may meet in the encoding
class PersistentObjEncoder(json.JSONEncoder):
    def transKey(self, key):
        return key.replace("_", " ")
    def default(self, obj, debug=False):
        if isinstance(obj, Persistent):
            d = {}
            for att in obj._attributes:
                if debug: print("--> encoding att",att)
                try:
                    d[self.transKey(att)] = getattr(obj,att)
                except AttributeError:
                    None   # attributes may be missing
            for objname in list(obj._objects.keys()):
                if debug: print("--> encoding objname",objname)
                if hasattr(obj, objname):
                    d[self.transKey(objname)] = getattr(obj,objname) # persistent
            for dsname in list(obj._datasets.keys()):
                if debug: print("--> encoding dsname",dsname)
                arr =  getattr(obj,dsname)
                if not isinstance(arr, np.ndarray):
                    # create numpy array and fill it
                    nparr = np.ndarray(arr.shape, arr.dtype)
                    nparr[...]  = arr[...]
                    d[self.transKey(dsname)] = nparr.tolist() # numpy tolist()
                else:
                    d[self.transKey(dsname)] = arr.tolist()
            for objar, _ in obj._objars.items():
                d[self.transKey(objar)] = getattr(obj,objar).tolist() # numpy tolist()
            return d
        if isinstance(obj, np.complex) or isinstance(obj, numbers.Complex):
            return {'r': float(obj.real), 'i': float(obj.imag)}
        if isinstance(obj, datetime):
            return obj.strftime(datetime_cformat)
        if isinstance(obj, bytes): 
            return obj.decode('utf-8')
 
        return json.JSONEncoder.default(self, obj)
    
    
    
