'''
Created on Jul 26, 2018

@author: adler
'''
import sys
import subprocess
import os
import argparse
import numpy as np
from argparse import ArgumentDefaultsHelpFormatter, Namespace

from portobello.rhobusta.options import ArgumentParserThatStoresArgv

class MPIContainer():
    

    def __init__(self, opts : Namespace):
        """ constructure for the lazy (see use of InitializeMPI) MPI proxy

        Args:
            The following options should be set in the framework:
            num_workers (either number or Namepsace): can just specify the number of workers.
                        For extended parameterization, the usual options Namespace can be used
            worker (bool, optional): is this a worker thread. Defaults to False.
        """
        self.worker = opts.worker
        if hasattr(opts, 'mpi_workers'):
            self.num_workers = opts.mpi_workers
        else:
            self.num_workers=0
        if hasattr(opts, 'mpi_runner_params'):
            self.extra_params = opts.mpi_runner_params
        if hasattr(opts, 'worker'):
            self.worker = opts.worker
        self.mpi = None
        self.rank = 0
        self.initialized = False # lazy

        self.parent_argv = opts.argv

    def GetRank(self):
        if not self.worker:
            return 0
        self.InitializeMPI()
        return self.rank

    def InitializeMPI(self):
        if self.initialized:
            return
        if self.num_workers >0:
            from mpi4py import MPI
            self.rank = MPI.COMM_WORLD.Get_rank()
            self.comm = MPI.COMM_WORLD
            self.mpi = MPI

        else:
            self.rank = 0
            self.comm = None 

        self.initialized = True

    def Terminate(self):
        if self.mpi is None:
            self.InitializeMPI()
        if self.mpi is None:
            return
        comm = self.mpi.Comm
        if comm is None or self.MPSystemIsLayered():
            return
        try:
            parent = comm.Get_parent()
            parent.Disconnect()
        except self.mpi.Exception:
            # this happens if we are not actually connected to MPI
            pass
         
    def GetMPIInterface(self):
        self.InitializeMPI()
        return self.mpi

    # should be overriden
    def BeforeWorkers(self):
        return True  # continue
        
    def AfterWorkers(self):
        pass
    
    # should be overriden
    def MPIWorker(self):
        pass
        
    def StandaloneWorker(self):
        self.MPIWorker()
       
    def WorkerExecutable(self):
        return sys.executable
       
    def TruncateSysArgs(self):            
        return self.get_wrapped_parent_argv(truncated = True)

    def WorkerArgs(self):
        args =  self.get_wrapped_parent_argv()  + ["--worker"]
        #print "******* worker arguments:", args
        #sys.stdout.flush()
        return args

    def get_wrapped_parent_argv(self, truncated = False):
        """
        This wraps arguments if they has a special character 
            and they are not the program name
        
        The wrapping looks like:
            --argname='arg'
            -argname'arg'

        why wrap these arguments in quotes?
        1. probably the user has already wrapped these arguments in quotes,
            but python will have dropped them when accumulating in sys.argv
        2. they cause problems when submitted as arguments to mpi workers: 
            the shell will misinterpret them because of special characters
        """

        SPECIAL_CHARS="\[{()*+?^$| ><"

        if self.parent_argv[1] == '-m':
            starting_point = 3
        else:
            starting_point = 1

        wrapped_argv = []
        for i, arg in enumerate(self.parent_argv):
            if i<starting_point:
                if not truncated:
                    wrapped_argv.append(arg)
            else:

                special_char = False
                for c in arg:
                    if c in SPECIAL_CHARS:
                        special_char = True
                        break
                
                if special_char:
                    # split into arg name and arg value
                    if arg[:2] == "--":
                        split_args = arg.split("=")
                    else:
                        split_args = [arg[:2], arg[2:]]

                    # already wrapped
                    if len(split_args)==1 or split_args[1:][0][0] in ["\'", "\""]:
                        return arg

                    #wrap in single quotes
                    arg = split_args[0] + "\'" + "".join(split_args[1:]) + "\'"
                    
                wrapped_argv.append(arg)

        return wrapped_argv

    def MPSystemIsLayered(self):
        return 'OLCF_SPECTRUM_MPI_ROOT' in os.environ or 'SLURM_CLUSTER_NAME' in  os.environ or 'CRAY_MPICH_PREFIX' in os.environ
  
    def MPRunner(self):
        if 'OLCF_SPECTRUM_MPI_ROOT' in  os.environ:
            return 'jsrun'
        elif 'CRAY_MPICH_PREFIX' in os.environ:
            return 'aprun'
        return 'mpirun'


    def Run(self, wait=True):
        # supervisor 
        if not self.worker:
            if not self.BeforeWorkers():
                return
        if not self.worker and self.num_workers > 0:
            self.spn = None
            print("**** Spawning %d MPI workers ****" % self.num_workers)
            sys.stdout.flush()

            if not self.MPSystemIsLayered():
                self.InitializeMPI()
                comm = self.mpi.COMM_WORLD

                #mpi_info = MPI.Info.Create()
                #mpi_info.Set("hostfile", "machines")
                self.spn = comm.Spawn(
                    self.WorkerExecutable(),
                    args=self.WorkerArgs(),
                    #info=mpi_info,
                    maxprocs=self.num_workers)
                    
                if wait:
                    self.spn.Disconnect()  # this waits until they are done
            else:
                assert(wait) # in summit and similar systems there is a barrier between the spawned jobs and the parent
                             # we cannot communicate with the spawned job, only wait for it (as a unix process)
                command = f"{self.MPRunner()} {self.extra_params} -n {self.num_workers} {self.WorkerExecutable()} {' '.join(self.WorkerArgs())}"
                subprocess.run(command, shell=True, env=os.environ) # waits
            print("**** MPI workers done ****", flush=True)
        else:
            self.StandaloneWorker()
     
        if wait:
            self.AfterWorkers()
        
    def IsMaster(self):
        if not self.worker:
            return True
        return self.rank == 0

    def Barrier(self):
        if self.mpi is None:
            self.InitializeMPI()
        if self.mpi is None:
            return
        self.mpi.COMM_WORLD.Barrier()



def GetMPIProxyOptionsParser(add_help=False, private=False):
   
    parser = ArgumentParserThatStoresArgv('mpi', add_help=add_help ,
                            parents=[],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
           
    mp = parser.add_argument_group(title="mpi",
                                   description="mpi execution")
    
    mp.add_argument("--mpi-workers", dest="mpi_workers", default=0, type=int,
                    help="""[default:1 -> not parallel, forks 1 worker], is the number of parallel workers spawned.
                    There should be this number + 1 slots allocated in MPI for the workers and the supervisor.""")

    mp.add_argument("--worker", dest="worker",
                    action="store_true",
                    default=False,
                    help=argparse.SUPPRESS) #True if this is a worker MPI thread, and it should just do work 

    mp.add_argument("--runner-params", dest="mpi_runner_params",
                    default="",
                    type=str, 
                    help="extra parameter for the multiprocessing system")

    mp.add_argument("--outsource", dest="outsource",
                    default="",
                    type=str, 
                    help="names of methods whose work should be outsourced")
                 
    mp.add_argument("--orgdir", dest="orgdir",
                    default=".",
                    type=str, 
                    help="the organizer dir for outsourcing")
                   
    return parser
