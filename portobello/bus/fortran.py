'''
Created on Apr 23, 2018

@author: adler
'''
from ctypes import CDLL, c_char_p, c_int, c_float
#from builtins import Exception
import os

class Fortran(object):
    '''
    '''
    
    instance_ = None

    # shared objects should be a list of .so file - either full paths
    # or relative to a path on the LD_LIBRARY_PATH.
    # note that the transitive closure of libraries needs to be available 
    # on the external LD_LIBRARY_PATH (for example, hdf5, mpi, mkl etc).
    def __init__(self, shared_objects):
        '''
        Constructor
        '''
        self.shared_objects = shared_objects.split(":")
        self.dlls = {}
        
    # assumes no overloaded names - doesn't check for type matches
    def GetFunction(self, function_name, with_suffix=True, check_gfort=False):
        
        prefix=""
        if self.gfort and check_gfort:
            prefix = "__"
            with_suffix = False
            function_name = function_name.replace(".mp.",".MOD.")

        suffix = ""
        if with_suffix:
            suffix="_"

        # this actual name may depend on the compiler - check in object?
        actual_fn = prefix + function_name.replace(".","_") + suffix
        return self.GetCFunction(actual_fn)
        
    # assumes no overloaded names - doesn't check for type matches
    def GetCFunction(self, function_name):
        func = None
        # this actual name may depend on the compiler - check in object?
        actual_fn = function_name
        for dll in list(self.dlls.values()):
            try:
                func = getattr(dll, actual_fn)
            except AttributeError:
                None
        if func:
            return func
        for so in self.shared_objects:
            if so not in self.dlls:
                dll = CDLL(so)
                self.dlls[so] = dll
                try:
                    func = getattr(dll, actual_fn)
                except AttributeError:
                    None 
                if func:
                    break
        return func            

    def ConvertToAscii(self, s):
        if isinstance(s, str):
            return s.encode('ascii')
        return s
    
    def ConvertStringsToAscii(self, args0):
        args=[x for x in args0]
        for i, x in enumerate(args):
            if isinstance(x, str):
                args[i] = self.ConvertToAscii(x)
        return args
    
    # caller must take care to wrape with ctypes, see:
    # https://docs.python.org/2/library/ctypes.html#module-ctypes
    # also we are not handling allocatable return types - only scalar
    # to avoid ownership issues
    def invoke(self, function_name, *args):
        #print("calling fortran:", function_name, *args)
        f = self.GetFunction(function_name)
        if not f:
            raise Exception("cannot find function " + function_name)
        f(*self.ConvertStringsToAscii(args))
        
        
    # Note the naming conventions *Wrapper, *Imp in base_module, imp_module correspondingly.
    # This was done to enforce the association with the original base name
    def CallWrapper(self, base_module, imp_module, function_name,
                    obj_path, *args):
        prefix = ""
        mod = ".mp."
        suffix = True
        if self.gfort:
            prefix = "__"
            mod = "_MOD_"
            suffix = False
        function_name = function_name.lower()
        base_module = base_module.lower()
        imp_module = imp_module.lower()
        
        fn = base_module + ".mp." + function_name + "wrapper"
        wrapper = self.GetFunction(fn)
        if not wrapper:
            raise Exception("cannot find function wrapper " + fn)
        fn = prefix + imp_module + mod + function_name + "imp"
        imp = self.GetFunction(fn, with_suffix=suffix)
        if not imp:
            raise Exception("cannot find function implementation " + fn)
        dummy_type = c_char_p * 1
        ret = dummy_type()
        
        #print "before wrapper called"
        # wrapper arguments:the return value array, the implementation pointer,
        # object path ("self" path), and then the user defined arguments
        wrapper(ret, imp, self.ConvertToAscii(obj_path), *self.ConvertStringsToAscii(args))
        #print "after wrapper called"
        return ret[0]

    @classmethod
    def Instance(cls):
        if not cls.instance_:
            if "FORTRAN_INTEL_LD_PATH" in os.environ:
                cls.instance_ = Fortran(os.environ["FORTRAN_INTEL_LD_PATH"])
                cls.instance_.gfort = False
            elif "FORTRAN_GCC_LD_PATH" in os.environ:
                cls.instance_ = Fortran(os.environ["FORTRAN_GCC_LD_PATH"])   
                cls.instance_.gfort = True 
            else:
                print("LD_LIBRARY_PATH must include the path to portobello libraries and FORTRAN_GCC_LD_PATH or FORTRAN_INTEL_LD_PATH must be defined")
              
        if not cls.instance_:
            raise Exception("cannot load fortran code, aborting.")
        
        return cls.instance_;

if __name__ == '__main__':
    fort = Fortran.Instance() 
            
    ret = fort.CallWrapper("wannier", "wannier_module", "Compute", 
                           "/A/B/C",  c_float(-10.0), c_float(10.0),
                           c_int(110), "something-important")
    
    print(ret)
    
