import numpy as np
from portobello.bus.mpi import MPIContainer

class ShardedArray():
    def __init__(self, mpic : MPIContainer, full_size, *partial_dims):
        self.mpic = mpic
        self.mpi = mpic.GetMPIInterface()
        self.is_mpi = self.mpi is not None
        self.partial_dims = partial_dims
        self.partial = np.zeros(partial_dims, dtype=np.float64)
        self.chunk_dims = self.partial_dims[1:]
        self.chunk = np.prod(self.chunk_dims) # total size is all_k*chunk
        self.num_workers = self.mpi.COMM_WORLD.Get_size() if self.is_mpi else 1
        self.rank = mpic.GetRank()
        self.sizes = np.zeros((self.num_workers,))
        self.sizes[self.rank] = self.partial_dims[0]
        mpic.Barrier()
        self.sizes = self.mpic.comm.allreduce(self.sizes, op=self.mpi.SUM) if self.is_mpi else self.sizes
        assert(sum(self.sizes)==full_size), (self.sizes, full_size)
        self.sizes *= self.chunk
        self.full_size = full_size

    def getOffsets(self):
        offsets = np.zeros((self.num_workers,))
        for i in np.arange(1,self.num_workers):
            offsets[i] = offsets[i-1] + self.sizes[i-1]
        assert(offsets[-1]+self.sizes[-1] == self.full_size*self.chunk), (offsets[-1]+self.sizes[-1], self.full_size*self.chunk)
        return offsets

    def GatherOnMaster(self):
        if self.num_workers <=1:
            return self.partial
        self.mpic.Barrier()
        result = self.GatherV()

        if self.rank == 0:
            return result[0]
        else:
            return result

    def GatherV(self):
        if self.is_mpi:
            result = None
            if self.mpic.IsMaster():
                result = [np.zeros((self.full_size, *self.chunk_dims)), self.sizes, self.getOffsets(), self.mpi.DOUBLE]
            self.mpi.COMM_WORLD.Gatherv(self.partial, result, root=0)
        
            return result
        
        return [ self.partial ]

def GatherAndBroadcastAcrossKShards(mpic, basis, array_to_gather):
    sharded_array = ShardedArray(mpic, basis.num_k_irr, *list(array_to_gather.shape))
    sharded_array.partial[:] = array_to_gather
    sharded_array = sharded_array.GatherOnMaster()
    if mpic.mpi is not None:
        sharded_array = mpic.comm.bcast(sharded_array, root=0)
    return sharded_array

def GatherAcrossKShards(mpic, basis, array_to_gather):
    sharded_array = ShardedArray(mpic, basis.num_k_irr, *list(array_to_gather.shape))
    sharded_array.partial[:] = array_to_gather
    return sharded_array.GatherOnMaster()

def GatherAndBroadcastComplexAcrossKShards(mpic, basis, array_to_gather):
    return (GatherAndBroadcastAcrossKShards(mpic,basis,np.real(array_to_gather))
     + 1j * GatherAndBroadcastAcrossKShards(mpic,basis,np.imag(array_to_gather)))

def GatherComplexAcrossKShards(mpic, basis, array_to_gather):
    return (GatherAcrossKShards(mpic,basis,np.real(array_to_gather))
     + 1j * GatherAcrossKShards(mpic,basis,np.imag(array_to_gather)))