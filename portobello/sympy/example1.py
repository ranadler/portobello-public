'''
Created on Jan 25, 2019

@author: adler
'''

import sympy
from sympy import Matrix, printing
from sympy import sqrt, Q
from sympy.core.symbol import symbols
from sympy.physics.quantum.dagger import Dagger
from sympy.simplify.simplify import simplify
from sympy.assumptions.refine import refine
from sympy.core.numbers import Rational, oo
from sympy.interactive.printing import init_printing
from sympy.printing.pretty.pretty import pprint
from sympy.matrices.expressions import inverse
from sympy.matrices.dense import eye, zeros
from sympy.core.function import expand
from sympy.printing.mathml import print_mathml
from sympy.printing.latex import latex
from sympy.series.limits import limit


if __name__ == '__main__':
    
    init_printing()
    
    Ep, Ed, s, w = symbols("E_p E_d s iw")

    #Ep = 0
    #Ed = Ep *2
    #w = -1
    #s=10
    
    # setting Ep=Ed (completely degenerate) the difference will be 0
    #
    E = Matrix([
               [Ep, 0, 0, 0, 0, 0, 0, 0],
               [0, Ed, 0, 0, 0, 0, 0, 0],
               [0, 0, Ed, 0, 0, 0, 0, 0],
               [0, 0, 0, Ed, 0, 0, 0, 0],
               [0, 0, 0, 0, Ed, 0, 0, 0],
               [0, 0, 0, 0, 0, Ed, 0, 0],
               [0, 0, 0, 0, 0, 0, Ed, 0],
               [0, 0, 0, 0, 0, 0, 0, Ed]])
    
    Sorb = Matrix([
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, 0, 0, 0, 0, 0],
               [0, 0, 0, s, 0, 0, 0, 0],
               [0, 0, 0, 0, s, 0, 0, 0],
               [0, 0, 0, 0, 0, s, 0, 0],
               [0, 0, 0, 0, 0, 0, s, 0],
               [0, 0, 0, 0, 0, 0, 0, s]])
    
    c=Rational(1,2)
    
    U = Matrix([
               [sqrt(1-c), 0, 0, 0, 0, 0, 0, sqrt(c)],
               [0, 1, 0, 0, 0, 0, 0, 0],
               [0, 0, 1, 0, 0, 0, 0, 0],
               [0, 0, 0, 1, 0, 0, 0, 0],
               [0, 0, 0, 0, 1, 0, 0, 0],
               [0, 0, 0, 0, 0, 1, 0, 0],
               [0, 0, 0, 0, 0, 0, 1, 0],
               [sqrt(c), 0, 0, 0, 0, 0, 0, -sqrt(1-c)]])
    
    # the self energy in band basis
    SigEmbe = Dagger(U)*Sorb*U 
    # density in bands
    correctRho= (w*eye(8) - E -SigEmbe).inv() - (w*eye(8) - E).inv()
    
    # project energy to the 5x5  d space
    E5 = (Dagger(U)*E*U)[3:,3:]
    pprint(E5,use_unicode=True) 
    
    # embed the 5x5 rho into the bigger space
    otherRho = zeros(8,8)
    otherRho[3:, 3:] = (w*eye(5) - E5 - Sorb[3:,3:]).inv() - (w*eye(5) - E5).inv()
    otherRho = Dagger(U)* otherRho * U
    
    diff =  expand(simplify(otherRho - correctRho, ratio=0.8))
      
    
    pprint(diff,use_unicode=True)
    #print latex( diff[7,7])#, use_unicode=True)
    
    # limit of large s is proportional to the difference Ep-Ed
    l = limit(diff[7,7], s, oo)
    
    print(l)
    print(latex(l))
    
    
    #l = limit(diff[7,7], Ep, 00)
    #print latex(l)
    