#!/usr/bin/env python3

'''
Created on Aug 19, 2018

@author: adler
'''
from portobello.rhobusta.rhobusta import DFTPlusG,\
    RhobustaOptionsParser, DFT0
import sys
import os
from portobello.rhobusta.gutzwiller import Gutz, GutzOptionsParser
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.ProjectorEmbedder import DefineProjector, TestProjector,\
    ProjectorOptionsParser
from argparse import ArgumentDefaultsHelpFormatter
from portobello.rhobusta.options import SaveOptions, RefreshOptions
from time import sleep
from portobello.bus.persistence import Persistent
from portobello.bus.mpi import MPIContainer
from portobello.jobs import Organizer
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
    
from portobello.generated.MI import MultiImpuritySupervisor
from portobello.rhobusta.multi_impurity import MultiImpurityEmbedder
from portobello.rhobusta.wannier import MainWannier

@Organizer.with_organizer
def DFT__G(opts):
    DFTGMain([], {}, opts = opts, wait = False)

def DFTPlusGOptionsParser(prepArgs, add_help=False):
    parser = ArgumentParserThatStoresArgv('dft+g', add_help=add_help,
                            parents=[RhobustaOptionsParser(), 
                                     ProjectorOptionsParser(),
                                     GutzOptionsParser(prepArgs)],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
      
    #argcomplete.autocomplete(parser)
    parser.add_argument("-I", "--super-iters", dest="num_super_iterations", 
                     default=100, type=int,
                     help="number of iterations of the charge self consistency equations, negative numbers -> start DMFT first (even if dmft file exists)")
  
    parser.add_argument("-y", "--dft-plus-iters", dest="dft_plus_g_iters", 
                     default=80, type=int,
                     help="number of iterations of the charge self consistency equations in subsequent dft+dmft iterations")
  
    parser.add_argument("--force-gutz-first", dest="force_gutz_first", 
                        type=bool,
                        help="force gutz to run first even if the gutz file exists",
                        default=True)
    
    parser.add_argument("--reconstruct", dest="reconstruct", 
                      action="store_true",
                      help="run DFT from input with new parameters, re-define projector, and re-converge dft+g",
                      default=False)
    
    return parser

def DFTGMain(args, kwargs, opts = None, wait=True):

    if opts is None:
        parser = DFTPlusGOptionsParser(sys.argv[1:], add_help=True)
        opts = parser.parse_args(sys.argv[1:])
    
    curdir = os.getcwd()
    os.chdir(opts.dir)

    if opts.reconstruct:
        assert(os.path.exists(opts.input_file))
        assert(not os.path.exists("./ini.h5"))
        assert(not os.path.exists("./projector.h5"))
        assert(os.path.exists("./gutz.h5"))
       
    # do not call DFTPlugin.SetGw() - this is a no-matsubara-frequency code
    
    SaveOptions(opts)

    if opts.input_file is not None or opts.restart:
        DFT0(opts) # can override argument here if needed by adding args
        sleep(10) # for job control to realize that DMFT workers are done

    # after the first DFT, all further DFT+X are NOT spin polarized funcionals:
    opts.dft_spin_polarized = False   
    opts.input_file = None
            
    SaveOptions(opts)
    
    #mpi = MPIContainer(opts)
    #plugin = DFTPlugin.Instance(restart=True, mpi=mpi)
    
    if opts.wannier and not os.path.exists("./wannier.h5"):
        MainWannier([],{},opts)

    elif not os.path.exists("./projector.h5"): 
        DefineProjector(opts)
        p = TestProjector(ewin_str=opts.window)
        
    num_impurities=1
    if os.path.exists("./multi-impurity.h5"):
        mi = MultiImpuritySupervisor()
        mi.load("./multi-impurity.h5:/")
        num_impurities = mi.num_impurities 
                
    if not os.path.exists("./gutz.h5") and not os.path.exists("./gutz0.h5"):
        for i in range(num_impurities):
            print(f"Working on impurity {i}")
            opts.which_shell = i
            if not opts.force_gutz_first: opts.only_inputs = True
            Gutz(opts)
            opts.only_inputs = False
    
    if wait:
        Organizer.Wait()

    si = 0
    while True:
        sleep(5) # for job control to realize that DMFT workers are done
        RefreshOptions(opts)
        si += 1
        if si > opts.num_super_iterations: break
        
        DFTPlusG(opts)
        
        RefreshOptions(opts)
        if opts.reconstruct:
            break

        Persistent.Refresh(["./gutz.h5:/", "./history.h5:/"])  # reopen to make sure we read the latest
        if DFTPlugin.IsInitialized():
            DFTPlugin.Instance().Reload() # reload the memory image of this process' DFT

        for i in range(num_impurities):
            print(f"Working on impurity {i}")
            opts.which_shell = i
            if opts.wannier:
                MainWannier([],{},opts)
            Gutz(opts)

        if wait:
            Organizer.Wait()

    os.chdir(curdir)
    if wait:
        Organizer.Terminate()

    

if __name__ == '__main__':
    DFTGMain(sys.argv, {})
    
