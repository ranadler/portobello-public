#!/usr/bin/env python3

'''
Created on Jun 12, 2023

@author: adler
'''
            
import os
import pathlib
import argparse
import sys
import h5py
import numbers
from portobello.bus.mpi import GetMPIProxyOptionsParser
import numpy as np
from pathlib import Path
from numpy import linalg
import re
import subprocess
import datetime
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any

def BuildNamepsace(opts, output_files, ns, verify) -> bool:  
    outputs =  output_files.split(",")
    if opts.add_gutz:
        outputs +=["gutz.h5:/"]
    if opts.add_dmft:
        outputs +=["dmft.h5:/"]
    for loc in outputs:
        if opts.debug: print(f"opening file {loc}")
        file, objPath = loc.split(":")
        try:
            h5f = h5py.File(file,"r")[objPath] 
            if len(h5f.attrs) == 0 and verify: return False
            for key in h5f.attrs:
                if isinstance(h5f.attrs[key], numbers.Number):
                    ns[key] = h5f.attrs[key]
                if hasattr(h5f.attrs[key],"value"):
                    ns[key] = np.array(h5f.attrs[key].value)

            #Above keys miss, e.g., GState keys
            for key in h5f.keys():
                if isinstance(h5f[key], numbers.Number):
                    ns[key] = h5f[key]
                if hasattr(h5f[key],"value"):
                    ns[key] = np.array(h5f[key].value)
        except Exception as e: 
            print(f"cannot open {file} in {Path('.').absolute()}")
            if verify: return False
    return True
   
def num_from_dirname():
    cwd = Path(".").absolute()
    # finds the last numerical 
    return float(re.findall(r"[-+]?(?:\d*\.*\d+)",cwd.name)[0])
    

def EvalInDirectory(opts):
    obs = locals()
    readyToEvaluate = BuildNamepsace(opts, opts.outputs, obs, verify=True)
    BuildNamepsace(opts, opts.optional_outputs, obs, verify=False)
    if opts.debug: print(f"ready={readyToEvaluate}")
    if not readyToEvaluate:
        return None
    if opts.show_variables:
        print(obs)
    try:
        x = eval(opts.x)
        if opts.debug: print(f"x={x}")
        y = eval(opts.y)
        if opts.debug: print(f"y={y}")
        if isinstance(y,numbers.Number):
            y = [y]
        return (x,y)
    except Exception as e:
        print(f"problem evaluating x or y expressions in dir {Path.cwd()}",e)
        return None
    
def ScanDirectories(opts):
    result_dirs = []
    ignored_dirs = set(opts.ignore.split(","))
    for g in opts.glob:
        if g.find("*") != -1:
            dirs = Path().glob(g)
            for dir in dirs:
                if str(dir) in ignored_dirs:
                    continue
                if dir.is_dir():
                    result_dirs.append(dir)
        else:
            dir = Path(g)
            if str(dir) in ignored_dirs:
                continue
            if dir.is_dir():
                result_dirs.append(dir)
    return result_dirs

def SetOperations(argv, kwargs, opts = None):

    parser = argparse.ArgumentParser(add_help=True)
        
    parser.add_argument("-c", "--command", dest="command", 
                        default="",
                        help="""command to run""")

    parser.add_argument("glob", 
                        nargs="*",
                        default="*/*",
                        help="""directories to search""")
        
    parser.add_argument("-F", "--outputs", dest="outputs", 
                        default="ini.h5:/strct/",
                        help="""comma separated output file locations, to read the top levels attributers of""")
    
    parser.add_argument("-f", "--optional-outputs", dest="optional_outputs", 
                        default="energy.h5:/",
                        help="""comma separated output file locations, to read the top levels attributers of""")
    
    parser.add_argument("-G", "--gutz", dest="add_gutz", 
                          action="store_true",
                          default=False)

    parser.add_argument("-D", "--dmft", dest="add_dmft", 
                          action="store_true",
                          default=False)

    parser.add_argument("-y", dest="y", 
                        default="result",
                        help="""comma separated expressions to evalue for plotting""")

    parser.add_argument("--ignore", dest="ignore", 
                        default="",
                        help="""comma separated dir names to ignore""")

    parser.add_argument("-x", dest="x", 
                        default="num_from_dirname()",
                        help="""one expression to evalue for plotting""")
    
    parser.add_argument("--xl", dest="xlabel", 
                        default="x",
                        help="""label for the x axis""")

    parser.add_argument("-X", "--stop-on-errors", dest="stop_on_errors", 
                          action="store_true",
                          default=False)
    
    parser.add_argument("--debug", dest="debug", 
                          action="store_true",
                          default=False)
    
    parser.add_argument("--show",  dest="show_variables", 
                          action="store_true",
                          default=False)
            
    (opts, args) = parser.parse_known_args(argv)
    
    dirs = ScanDirectories(opts)
    ylabels =  opts.y.split(",")   
    cwd = Path.cwd().absolute()
    
    if opts.command == "":
        x = []
        y = [[] for ys in ylabels]
        for dir in dirs:
            os.chdir(cwd)
            if opts.debug: print(f"** reading dir {dir}")   
            os.chdir(dir) # relative dir
            vals = EvalInDirectory(opts)
            if vals is None:
                if opts.stop_on_errors:
                    return
                else:
                    continue # values from dir skipped
            
            x.append(vals[0])
            for i,yl in enumerate(ylabels): 
                y[i].append(vals[1][i])
            os.chdir(cwd)    
            
            if opts.show_variables:
                # show it in the first dir that has files
                return
            
        # sort by x now
        for i,yl in enumerate(ylabels): 
            _x, y[i] = zip(*sorted(zip(x, y[i])))
        x = sorted(x)
            
        import matplotlib.pyplot as plt
        fig, ax = plt.subplots(len(ylabels),1,sharex='all')
        if len(ylabels) == 1:
            ax = [ax] # stupid matplotlib convention is that it is not an array
        for i,yl in enumerate(ylabels):
            y[i] = np.squeeze(np.array(y[i]))
            if len(y[i].shape) > 1:

                if len(y[i].shape) != 4: raise NotImplementedError() #tensors or freqeuncy dependent matrices
                if y[i].shape[1] != y[i].shape[2]: raise NotImplementedError() #non-square matrices

                for si in range(y[i].shape[-1]):
                    for f in range(y[i].shape[1]):
                        ax[i].plot(x, y[i][:,f,f,si], label=f"{yl}|index{f}|spin{si}")
            else:
                ax[i].plot(x, y[i], label=yl)
                
            ax[i].legend()
            ax[i].set_xlabel(opts.xlabel)
        dt = datetime.datetime.now()
        fig.canvas.set_window_title(f"data: {str(cwd)}, {dt.ctime()}")
        plt.show()
    else:
        for dir in dirs:
            os.chdir(cwd)   
            os.chdir(dir) # relative dir
            # run in bash in order to get the module shortcuts that we use
            # RUN_MODULE_PURGE can be used in the .bashrc if it needs to avoid doing things like module purge (which has issues in some versions)
            result = subprocess.run(['bash', '-ilc', f"{opts.command};exit 0"], env={"RUN_MODULE_PURGE":"FALSE"})
            
    os.chdir(cwd)  
    
if __name__ == '__main__':
    SetOperations(sys.argv[1:], {})
