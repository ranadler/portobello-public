#!/usr/bin/env python3

'''
Created on Apr 29, 2018

@author: adler
'''
from portobello.bus.mpi import MPIContainer
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.symmetry.magmom import GroupAnalyzerWithMagmom
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.bus.persistence import Store
import re
from pymatgen.core.periodic_table import Element
from pymatgen.vis import structure_vtk
import sys
from portobello.symmetry.local import StructLocalSymmetryAnalyzer,\
    FindLocalNeighbors, Neighbors
from pymatgen.core.sites import Site
from scipy.spatial.qhull import ConvexHull
import numpy as np
import logging
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from pymatgen.vis.structure_vtk import StructureVis
from argparse import ArgumentDefaultsHelpFormatter
from portobello.rhobusta.ProjectorEmbedder import ProjectorOptionsParser
from portobello.rhobusta.InputBuilder import GetStructure
from pathlib import Path
import pyvista
import vtk
from pyvista.plotting.tools import create_axes_marker

# all measurements here are in bohr

def load_hydrogen_orbital(n=1, l=0, m=0, zoom_fac=1.0, Z=1, axis='z'):

    try:
        from sympy import lambdify
        from sympy.abc import phi, r, theta
        from sympy.physics.hydrogen import Psi_nlm
    except ImportError:  # pragma: no cover
        raise ImportError(
            '\n\nInstall sympy to run this example. Run:\n\n    pip install sympy\n'
        ) from None

    if n < 1:
        raise ValueError('`n` must be at least 1')
   
    if abs(m-int(m)) < 1.0e-6:
        psi = lambdify((r, phi, theta), Psi_nlm(n, l, int(m), r, phi, theta, Z), 'numpy')
    else:
        if  m>0 and abs(int(m) + 0.5 - m) < 1.0e-6:
            m1 = int(m)
            m2 = int(m) + 1
        else:
            assert(int(m) - 0.5 -m)  < 1.0e-6
            m1 = int(m)-1
            m2 = int(m)
            
        j = l-0.5
        assert(j >= 0.0)
        psi = lambdify((r, phi, theta), -np.sqrt((j+1-m)/(2*j+2))*Psi_nlm(n, l, m1, r, phi, theta, Z) +
                                        np.sqrt((j+1+m)/(2*j+2))*Psi_nlm(n, l, m2, r, phi, theta, Z), 
                        'numpy') 
        

    #if n == 1:
    #    org = 1.5 * n**2 + 1.0
    #else:
    #    org = 1.5 * n**2 + 10.0
    org = 9.0 # 2 bohr

    org /= zoom_fac 

    dim = 500
    sp = (org * 2) / (dim - 1)
    grid = pyvista.UniformGrid(
        dimensions=(dim, dim, dim),
        spacing=(sp, sp, sp),
        origin=(-org, -org, -org),
    )
    
    # allow simple rotation of the axis by permuting the coordinates right handedly
    axes = "xyz"
    zi = axes.find(axis)
    print(f"Atomic RSH axis is {axis}: {zi}, z={Z}")
    def grid_(i):
        letter = axes[(zi+i)%3]
        return getattr(grid, letter)
    r, theta, phi = pyvista.cartesian_to_spherical(grid_(1), grid_(2), grid_(0))
    wfc = psi(r, phi, theta).reshape(grid.dimensions)

    grid['real_wf'] = np.real(wfc.ravel())
    grid['wf'] = wfc.ravel()
    return grid



# return iatom, n, l where iatom is the first atom in the equivalence class of atoms
def DecodeOrbital(orbString, species):
    def FirstDistinctAtom(el, species):
        element = Element(el)
        z = element.Z
        return species.index(z)
    
    def Lof(orb_char):
        if orb_char == 's':
            return 0
        if orb_char == 'p':
            return 1
        if orb_char == 'd':
            return 2
        if orb_char == 'f':
            return 3
        if orb_char == 'e':
            return 4
        return -1
    
    Nel, Norb = orbString.split(":")
    n = int(Norb[:-1])
    l = Lof(Norb[-1])
    
    vals = re.split('(\D+)', Nel)
    if len(vals) == 3:
        iatomStr, el, _space = vals
        if iatomStr == '':
            iatom = FirstDistinctAtom(el, species)
            return iatom, n, l 
        return int(iatomStr), n, l
    else:
        iatom = int(vals[0])
        return iatom, n, l

class StructureVisSC(structure_vtk.StructureVis):
        
    def add_picker_fixed(self):
        None


def ShowVis(st, opts, strsym, el, L=2, r=0, N=1):
    vplotter = pyvista.Plotter()
    vis = StructureVis(show_bonds=True, show_polyhedron=False, show_unit_cell=True, poly_radii_tol_factor=1.0)
    if opts.supercell:
        vis.supercell = [[2, 0, 0], [0, 2, 0], [0, 0, 2]]
    vis.show_help = False
    vis.show_bonds = True 
 
    nn0, rN = strsym.neighbors
    vis.set_structure(st, reset_camera=True ,to_unit_cell=False)
    vis.title = f"{st.composition.formula} n={N} l={L} m_{opts.Ylm_axis}={opts.m}"
       
    el_str = str(el.specie) if el is not None else None
    for ls in strsym.distinct_symm:
        if ls is None:
            continue
        site = st.sites[ls.site_index]
            
        if r > 1.0e-4: # only if the r option was given
            for sts, _rsite in nn0:
                if sts.specie == site.specie:
                    vis.add_partial_sphere(sts.coords, color=(0.0,0.0,0.0), radius=0.3, opacity=0.3)
            
            # neighbors sphere    
            vis.add_partial_sphere(site.coords, color=(0.2,0.2,0.2), radius=rN, opacity=0.2)
            
        if el_str is None or not str(site.specie).startswith(el_str) :
            continue
        site_xyz = site.coords
        camera = vis.ren.GetActiveCamera()
        camera.SetFocalPoint(site_xyz)

        for i in range(3):
            axis = ls.annotated_reps_l[L].rot[:,i]
            vis.add_line(site_xyz,site_xyz + axis * 3.0, width=4, color=(0.1, 0.0, 0.0))
            vis.add_text(site_xyz + axis * 3.0, "xyz"[i])
            
        vis.add_line([0,0,0],[4,0,0], width=3, color=(0.0, 1.0, 0.0))
        vis.add_text([4,0,0], "x")
        vis.add_line([0,0,0],[0,4,0], width=3, color=(0.0, 1.0, 0.0))
        vis.add_text([0,4,0], "y")
        vis.add_line([0,0,0],[0,0,4], width=3, color=(0.0, 1.0, 0.0))
        vis.add_text([0,0,4], "z")
            
        neighbors = []
        nns, _rad = FindLocalNeighbors(st, site, min_number=7)
        for ps, r in nns:  # ps is a PeriodicSite, r a real
            if r < 0.00001: # do not include the center
                continue
            neighbor = Site(ps.species, ps.coords)
            vis.add_site(neighbor)
            neighbors.append(neighbor)
        points = np.zeros((len(neighbors), 3), np.float32)
        for i, n in enumerate(neighbors):
            points[i,:] = n.coords
        hull = ConvexHull(points)
        color = vis.el_color_mapping[site.specie.symbol]
        simps = hull.simplices
        for face in range(simps.shape[0]):
            simplex = simps[face, :]
            vis.add_faces([[points[i,:] for i in simplex]], opacity=0.2, color=(0.0,1.0,0.0))
        neighbors1 = [site.coords] + [n.coords for n in neighbors]
        try:
            voro = VoronoiTess(neighbors1)
            all_vertices = voro.vertices
            for nn, vind in list(voro.ridges.items()):
                if 0 in nn:
                    neighbor = neighbors[nn[1]-1]
                    vis.add_site(neighbor)
                    facet = [all_vertices[i] for i in vind]   
                    color = vis.el_color_mapping[site.specie.symbol]             
                    vis.add_bonds(center=site,neighbors=[neighbors[nn[1]-1]], radius=0.03, opacity=0.5, 
                                  color=color)
                    vis.add_faces([facet], color=(0.0,0.0,1.0), opacity=0.5)
        except:
            None
      
    grid = load_hydrogen_orbital(n=N, l=L, m=opts.m, zoom_fac=1.0, Z=opts.N0, axis=opts.Ylm_axis) #Element(el_str).Z
    grid.origin += site_xyz
        
    pvactor = vplotter.add_volume(grid,  opacity=[1, 0, 1], cmap='magma')
    vis.ren.AddVolume(pvactor)
    #add_axes_at_origin(vis.ren)
    vis.ren_win.Render()
    vis.show()
    
    
def add_axes_at_origin(
        ren,
        x_color=None,
        y_color=None,
        z_color=None,
        xlabel='X',
        ylabel='Y',
        zlabel='Z',
        line_width=2,
        labels_off=False):

        _marker_actor = create_axes_marker(
            line_width=line_width,
            x_color=x_color,
            y_color=y_color,
            z_color=z_color,
            xlabel=xlabel,
            ylabel=ylabel,
            zlabel=zlabel,
            labels_off=labels_off,
        )
        ren.AddActor(_marker_actor)
       

if __name__ == '__main__':
    
    parser = ArgumentParserThatStoresArgv('symm', add_help=True ,
                            parents=[ProjectorOptionsParser(private=True)],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
   
    parser.add_argument("-R", "--fully-relativistic", dest="fully_relativistic", 
                      action="store_true",
                      default=False)
   
    parser.add_argument("--supercell", dest="supercell", 
                      action="store_true",
                      default=False)
    
    parser.add_argument("--axis", dest="Ylm_axis", default="z")

    parser.add_argument("-m", dest="m", type=float, default=0)
    parser.add_argument("-N", "--N0", dest="N0", 
                        type=float,
                        default=5)
    
    opts, _args = parser.parse_known_args(sys.argv[1:])
        
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)
        
    if not Path("./flapw_image.h5").exists():
        st = DFTPlugin.GetStructureFromIni()
    else:     
        mpi = MPIContainer(opts)
        st = DFTPlugin.Instance(restart=True,mpi=mpi).GetStructure()
      
    species = [site.specie.Z for site in st.sites]  
    afm = GroupAnalyzerWithMagmom.RecoverMagneticMoments(st)
    
    original = st
    
    sga = None  # it will be reconstructed in the local symmetry analyzer
    if afm is not None:
        mga = GroupAnalyzerWithMagmom(st)
        sga = mga.get_sga()
        st = mga.get_primitive_standard_structure()
    else:
        sga = SpacegroupAnalyzer(st, symprec=0.001, angle_tolerance=opts.spacegroup_angle_tolerance)
        st = sga.get_symmetrized_structure()

    nrel = 1
    num_si = 2
    if opts.fully_relativistic:
        nrel = 2
        num_si = 1
    
    mag = None
            
    requestedSubshells = [DecodeOrbital(orb,species) for orb in opts.orbitals.split(",")]     
       
    print(" - input:", requestedSubshells)        
             
    iatom, n, l = requestedSubshells[0]
             
    strsym = StructLocalSymmetryAnalyzer(st, r=opts.r, 
                                         l=l, num_si=num_si, nrel=nrel, 
                                         species="", sga=sga, basis=opts.basis, 
                                         moment=opts.moment, symmetry_breaking=opts.symmetry_breaking,
                                         opts=opts)
   
    ShowVis(original, opts, strsym, L=l, el=original[iatom], r=opts.r, N=n)
    
    Store.FlushAll()

    
