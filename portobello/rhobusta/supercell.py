#!/usr/bin/env python3

        
from optparse import OptionParser
import sys
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer

from portobello.rhobusta.InputBuilder import GetStructure
#from portobello.rhobusta.magnetic_supercell import CifWriter
from pymatgen.io.cif import CifWriter


def main():
    
    parser = OptionParser()    
    parser.add_option("-m", "--multiplier", dest="mult", default="2x1x1")
    (opts, args) = parser.parse_args(sys.argv[1:])
    

    struct = GetStructure(args[0])
    sga = SpacegroupAnalyzer(struct, symprec=0.001, angle_tolerance=5)
    struct = sga.get_primitive_standard_structure()

    mult = [int(a) for a in opts.mult.split("x")]
    struct.make_supercell(mult)
    sga = SpacegroupAnalyzer(struct, symprec=0.001, angle_tolerance=5)

    base, _suffix = args[0].split('.')
    cw = CifWriter(struct, symprec=0.0001, refine_struct=False)
    cw.write_file(base+ f"-{opts.mult}.cif")


if __name__ == '__main__':
    main()