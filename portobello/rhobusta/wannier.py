#!/usr/bin/env python3

"""
NOTE: If you are bringing in a new wannier90 
-- you must search and replace #ifdef MPI -> #ifdef WMPI
    as wannier90 in library mode does not work with a parallel compilation
"""

'''
Created on Apr 29, 2018

@author: adler@physics.rutgers.edu
'''


from argparse import ArgumentDefaultsHelpFormatter, Namespace
from itertools import combinations, product

from portobello.bus.Matrix import Matrix, ZeroComplexMatrix
from portobello.bus.mpi import  MPIContainer
from portobello.bus.shardedArray import GatherAndBroadcastAcrossKShards, GatherAndBroadcastComplexAcrossKShards
from portobello.generated.FlapwMBPT import KGrid
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.generated.base_types import Window
from portobello.generated.FlapwMBPT_basis import InterstitialBasis
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.ProjectorEmbedder import CorrelatedSubspaceProjector, Projector, ProjectorOptionsParser, CalculateBandBoundsWithinWindow
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.bandstructure_utilities import CreateKPath, getHighSymmKpath
from portobello.rhobusta.orbitals import OrbitalsDefinition, PrintMatrix
from portobello.generated.FlapwMBPT_interface import DFT
from portobello.bus.persistence import Store
from portobello.generated import LAPW, wannier
from portobello.rhobusta.basis import BohrToAng, OrbitalEvaluatorContainer, xyz_to_spherical
from portobello.rhobusta.orbital_analysis import MainOrbitalAnalysis

from scipy.spatial.transform import Rotation as sciRot
import scipy.interpolate as scinterp
import scipy.integrate as scint
from scipy.linalg import sqrtm
import sys
import numpy as np

from portobello.symmetry.local import GetCGMatrix, GetRealSHMatrix

Ry2eV = 27.2107*0.5  # from Kutepov, careful when changing
BohrToAng = 1./1.88973

def WannierParser(add_help = True):

    parser = ArgumentParserThatStoresArgv('wann', add_help=add_help,
                            parents=[ProjectorOptionsParser(private=True)],
                            formatter_class=ArgumentDefaultsHelpFormatter)      

    parser.add_argument("-i", "--inputs", dest="input_only",
                      action="store_true",
                      default=False,
                      help="only generate inputs and exit.") 

    parser.add_argument("-K", "--kpoint-density",
                    dest="kdensity",
                    default=50,
                    type=int,
                    help = "Density of kpoints along the lines between high-symmetry points")

    parser.add_argument("--bands",dest="plot_bands", 
                        default=False, action="store_true",
                        help = "Plot the wannier band structure")

    parser.add_argument("--visualize",dest="visualize", 
                        default=False, action="store_true",
                        help = "Visualize the wannier orbitals")

    parser.add_argument("--nr",dest="num_realspace_points", 
                        default=25, type=int,
                        help = "number of realspace points (for visualization)")

    parser.add_argument("--spinor",dest="spinor",  
                        default=0, type=int,
                        help = "component of the relativistic wavefunction to visualize (0 or 1)")

    parser.add_argument("--symmetry-adapted",dest="symmetry_adapted", 
                        default=False, action = "store_true",
                        help = "Construct symmetry adapted wannier functions")

    parser.add_argument("--path", dest="path", default="", 
                        help="""evaluate a portion of the path as defined by pairs of labels,
                                each entry in pair separated by a comma, each pair seperated by a semicolon
                                e.g., \Gamma,X;X,M;M,\Gamma;\Gamma,Z\n
                                one can add offsets from these lines, e.g., \Gamma,Z+0.1*kx-0.1*ky\n
                                """)

    parser.add_argument("--dis-window", dest="dis_window", default="-10:10",
                        help="""Extend the frozen window (-w) by this many eV to generate the disintanglement window""")

    parser.add_argument("--adjust-apw", dest="adjust_apw", default="gaussian", choices = ["none", "gaussian", "hydrogen", "smooth", "maximize"],
                        help="""Instead of projecting onto the APW orbital of s,p, or non-correlated d, 
                        project onto an orbital with a different radial function (for the initial guess).
                        """)

    return parser

class UNKP:
    def __init__(self, st, lapw : LAPW.Basis, dft : DFT, proj : Projector, opts : Namespace):
        self.nr = opts.num_realspace_points 
        self.st = st
        self.dft = dft
        self.lapw = lapw
        self.opts = opts

        def rmesh_along(i):
            return np.linspace(-0, st.lattice.matrix[i,:]*BohrToAng, self.nr, endpoint = False)
        
        self.rmesh = np.zeros((self.nr**3,3), dtype = float)
        ir = 0
        for x,y,z in product(rmesh_along(0), rmesh_along(1), rmesh_along(2)):
            self.rmesh[ir,:] = x[:]+y[:]+z[:]
            ir += 1

        self.map_rotated_group_to_place_in_rmesh = []
        for op in range(dft.sg.num_ops):
            rot = Matrix(dft.sg.rots[:,:,op])
            rot2 = np.copy(rot)
            rot[:,:] = rot2[[2,0,1], :]
            rot[:,:] = rot[:, [2,0,1]]
            shift = Matrix(dft.sg.shifts[:,op])*BohrToAng

            inv_rot = rot.I
            inv_shift = (-inv_rot * shift.T).flatten()

            r = sciRot.from_matrix(inv_rot)
            new_mesh = r.apply(self.rmesh) + inv_shift
            new_mesh = Matrix(new_mesh) * Matrix(st.lattice.reciprocal_lattice.matrix) / BohrToAng / (2*np.pi)

            if op == 0:
                scale = new_mesh[-1,:].flatten()/(self.nr-1)

            integer_coords = np.array(np.around(new_mesh/scale), dtype=int)
            integer_coords_in_unit_cell = np.mod(integer_coords, self.nr)

            indices = np.tensordot(integer_coords_in_unit_cell[:,:] , [self.nr**2, self.nr, 1], axes=(1,0))
            if op == 0:
                assert(all(indices==range(len(indices)))), indices[:100]

            self.map_rotated_group_to_place_in_rmesh.append(indices)
            
            
        self.num_k_all = dft.num_k_all
        self.num_k = proj.num_k
        self.num_si = proj.num_si
        self.min_band = proj.min_band
        self.max_band = proj.max_band

    def compute_interstitial(self):

        print("Computing wave function in the interstitial")
        #array4<complex> A{max_num_pw, num_expanded, num_k, num_si};

        loc = "./flapw_basis.core.h5:/"
        basis = InterstitialBasis()
        basis.GetInterstitialBasis(loc, self.min_band, self.max_band)
        basis.load(loc)

        self.int_in_bands = np.zeros((self.num_k_all, self.dft.nrel, self.num_si, basis.num_expanded, self.nr**3), dtype=np.complex)

        return
        c = 274.074
        c2 = c*c

        for k in range(self.num_k_all):
            k_irr = self.dft.k_irr[k] - 1 - self.dft.shard_offset
            if k_irr < 0 or k_irr >= self.num_k:
                continue
                
            if k < self.dft.num_k_irr:
                for i in range(basis.num_vectors_at_k[k]*basis.nrel):
                    bi = basis.vector_index[i%basis.num_vectors_at_k[k],k]-1
                    G = basis.reciprical_lattice_vectors[:,bi] 
                    kpg = G + self.lapw.KPoints[k,:] # bohr

                    factor = 1
                    if self.dft.nrel == 2:
                        kpg2 = np.dot(kpg,kpg)
                        c2_p_ekpg = c2 + 0.5*(-c2 + c * np.sqrt(c2 + 4*kpg2))
                        factor = (c2 + c2_p_ekpg)/np.sqrt(c2_p_ekpg**2+c2*kpg2)

                    psi = np.exp( 1j * 2*np.pi * np.tensordot(kpg / BohrToAng, self.rmesh[:,:], axes=(0,1)) ) * factor / np.sqrt(self.st.lattice.volume)
                
                    for irel in range(self.dft.nrel):

                        spinor_factor = 1 if irel == 0 else c*kpg[2]/(c2+c2_p_ekpg)

                        for si in range(self.num_si):
                            for n in range(basis.num_expanded):    
                                self.int_in_bands[k,irel,si,n,:] += basis.A[i,n,k_irr,si] * psi[:] * spinor_factor
                
        for k in range(self.num_k_all):
            k_irr = self.dft.k_irr[k] - 1 - self.dft.shard_offset
            if k_irr < 0 or k_irr >= self.num_k:
                continue
            ig = self.dft.ig[k] - 1  

            if k >= self.dft.num_k_irr:
                for irel in range(self.dft.nrel):
                    for si in range(self.num_si):
                        for n in range(basis.num_expanded):
                            self.int_in_bands[k,irel,si,n,:] = self.int_in_bands[k_irr,irel,si,n,self.map_rotated_group_to_place_in_rmesh[ig]]

        for k in range(self.num_k_all):
            k_irr = self.dft.k_irr[k] - 1 - self.dft.shard_offset
            if k_irr < 0 or k_irr >= self.num_k:
                continue

            exp_fac = np.exp( -1j * 2 * np.pi *np.tensordot(self.lapw.KPoints[k,:] / BohrToAng, self.rmesh, axes=(0,1)) )
            self.int_in_bands[k,:,:,:,:] *= exp_fac

         
    def compute_MT(self, KS : LAPW.BandStructure):
        self.num_expanded = KS.num_expanded
        self.mt_in_bands = np.zeros((self.num_k_all, self.dft.nrel, self.num_si, self.num_expanded, self.nr**3), dtype=np.complex)
        
        lat_vecs = [self.st.lattice.matrix[i,:]*BohrToAng for i in range(3)] + [-self.st.lattice.matrix[i,:]*BohrToAng for i in range(3)]
        lat_vecs.append(np.zeros(3))
        
        # find periodic image in point closest to atom and center the mesh on atom for spherical coordinates
        spherical_coords_centered_at_atom = []
        periodic_coords = []
        print("Computing periodic images")
        for atom in self.st.sites:
            centered = self.rmesh - atom.coords*BohrToAng
            for i, xyz in enumerate(centered):
                min_dist = np.linalg.norm(xyz)
                for v1,v2,v3 in combinations(lat_vecs, r=3):
                    new_min_dist = np.linalg.norm(xyz-v1-v2-v3)
                    if new_min_dist < min_dist:
                        min_dist = new_min_dist
                        centered[i] = xyz - v1 - v2 - v3
                    
            periodic_coords.append(centered + atom.coords*BohrToAng)
            spherical_coords_centered_at_atom.append( xyz_to_spherical(centered) )

        print("Computing wavefunction from Muffin Tins")
        orbital_evaluators = OrbitalEvaluatorContainer(self.dft, self.lapw)
        self.in_mt = {}
                    
        for iorb in range(self.lapw.num_muffin_orbs):

            iatom = self.lapw.atom_number[iorb] - 1

            orbital_evaluators.emplace(iorb, spherical_coords_centered_at_atom[iatom])
            mt = orbital_evaluators.get_wavefunction()

            if iatom not in self.in_mt.keys():
                r = orbital_evaluators.get_MT_radius()
                self.in_mt[iatom] = spherical_coords_centered_at_atom[iatom][:,2] < r 

            for k in range(self.num_k_all):
                k_irr = self.dft.k_irr[k] - 1 - self.dft.shard_offset
                if k_irr < 0 or k_irr >= self.num_k:
                    continue
                ig = self.dft.ig[k] - 1  

                exp_fac = np.exp( -1j * 2*np.pi * np.tensordot(self.lapw.KPoints[k,:] / BohrToAng, periodic_coords[iatom][:,:], axes=(0,1)) )

                for irel in range(self.dft.nrel):
                    psi = exp_fac * mt[irel, self.map_rotated_group_to_place_in_rmesh[ig]]
                    if (self.dft.nrel > 1):
                        psi += exp_fac * mt[irel+2, self.map_rotated_group_to_place_in_rmesh[ig]]

                    for si in range(self.num_si):
                        for n in range(self.num_expanded):
                            self.mt_in_bands[k, irel, si,  n, :] += psi * KS.Z[iorb, n, k_irr, si]

    def carve_spheres_from_interstitial(self):
        return
        outside_MT = not self.in_mt[0]
        inside_MT = self.in_mt[0]
        for key in self.in_mt.keys():
            if key:
                inside_MT += self.in_mt[key]
                outside_MT += not self.in_mt[key]

        assert(np.sum(np.abs(self.mt_in_bands[0, 0, 0,  0, outside_MT])) == 0)
                
        #inside_MT = np.abs(self.mt_in_bands[0, 0, 0,  0, :]) > 0
        self.int_in_bands[:,:,:,:,inside_MT] = 0
        self.mt_in_bands *= 0

    def write_dmn(self, wan_proj):
        with open("_wannier.dmn", "w") as f:
            f.write("Symmetry adapted overlaps (from portobello)\n")
            f.write(f"{self.num_expanded} {self.dft.sg.num_ops} {self.dft.num_k_irr} {self.dft.num_k_all}\n\n")

            for ik in range(self.num_k_all):
                f.write(f"{self.dft.k_irr[ik]} ") 
                if (ik+1)%10 == 0: f.write("\n")
            f.write("\n")

            for ik in range(self.dft.num_k_irr):
                f.write(f"{ik} ") 
                if (ik+1)%10 == 0: f.write("\n")
            f.write("\n")

            for ik in range(self.dft.num_k_irr):
                for op in range(self.dft.sg.num_ops):
                    r = sciRot.from_matrix(self.dft.sg.rots[:,:,op])
                    k_rot = r.apply(self.lapw.KPoints[ik,:])
                    m = np.argwhere([np.allclose(k_rot,k) for k in self.lapw.KPoints])
                    f.write(f"{m}")
                    if (op+1)%10 == 0: f.write("\n")
                f.write("\n")

            dmn = wan_proj.compute_dmn()
            for ik in range(self.dft.num_k_irr):
                for op in range(self.dft.sg.num_ops):
                    for m,n in product(range(wan_proj.num_bands), repeat=2):
                        f.write(f"{dmn[ik,op,m,n]}\n")
                    f.write("\n")

                
    def write(self):
        print("Writing result")
        for k in range(self.dft.num_k_all):
            k_irr = self.dft.k_irr[k] - 1 - self.dft.shard_offset
            if k_irr < 0 or k_irr >= self.num_k:
                continue

            k_str = '{:0>5}'.format(k+1)

            if self.dft.nrel == 2:
                irel = self.opts.spinor
            else:
                irel = 0

            if self.dft.num_si == 2:
                file_name = f"UNK{k_str}.NC" #untested
            else:
                file_name = f"UNK{k_str}.{1}" #pretend the second spinor is "down" rather than use spinors

            with open(file_name,"w") as f:
                f.write(f"{self.nr} {self.nr} {self.nr} {k+1} {self.num_expanded}\n")

                for n in range(self.num_expanded):
                    for i in range(self.nr**3):

                        si = 0
                        el = self.mt_in_bands[k,irel,si,n,i] + self.mt_in_bands[k,irel,si,n,i]
                        f.write(f"{el.real} {el.imag}\n")

                        if self.dft.num_si == 2:
                            si = 1
                            el = self.mt_in_bands[k,irel,si,n,i] + self.int_in_bands[k, irel, si, n, i]
                            f.write(f"{el.real} {el.imag}\n")          

            
class WannierProjectorGenerator(Projector, wannier.WannierInput):
    
    def GetNumberOfKPoints(self):
        return self.dft.num_k_all

    def CalculateDmn(self):
        orbital_evaluator_container = OrbitalEvaluatorContainer(self.dft, self.lapw, prepare_for_spherical_coordinates = False)
        for iorb in range(self.lapw.num_muffin_orbs):
            orbital_evaluator_container.emplace(iorb, None)

        intBasis = InterstitialBasis()
        loc = "./flapw_basis.core.h5:/"
        intBasis.GetInterstitialBasis(loc, self.min_band, self.max_band)
        intBasis.load(loc)

        #...

    def adjust_radial_functions(self, orbital_evaluator_container):
        #Now we need to generate our new radial functions
        for shell in self.shells:
            iatom = shell.atom_number
            isort = self.dft.st.distinct_number[iatom] - 1
            iorb = shell.lapw_indices[0]

            tin = orbital_evaluator_container.basis.tins[isort]
            ibasis = tin.lapw2self.data[iorb]
            radial_func = tin.radial_functions[ibasis]

            shell.radial_function = radial_func

            shell.include_interstitial = False
            if radial_func[-1] > 1e-8:

                if self.apw_adjust == "hydrogen": #1s orbital radial function
                    alpha = tin.radial_mesh[-1]*1.5
                    shell.radial_function = 2*alpha**1.5*np.exp(-alpha*tin.radial_mesh)

                elif self.apw_adjust == "gaussian": #gaussian
                    sigma = tin.radial_mesh[-1]/3
                    shell.radial_function = 1./np.sqrt(2*np.pi*sigma)*np.exp(-tin.radial_mesh**2 / (2*sigma**2) )

                elif self.apw_adjust == "smooth": #turn APW->LOC
                    deriv_radial = np.gradient(radial_func, tin.radial_mesh)
                    sign_changes = np.where(np.diff(np.sign(deriv_radial)) != 0)[0]
                    final_sign_change = sign_changes[-1]
                    final_sign_change = (9*tin.nrad)//10
                    ntail=2
                    if len(sign_changes) and tin.nrad - final_sign_change > ntail:
                        tail = tin.radial_mesh[-ntail:]
                        head = tin.radial_mesh[:final_sign_change]
                        x = np.concatenate((head,tail))
                        y = np.concatenate((radial_func[:final_sign_change], np.zeros(ntail)))
                        f = scinterp.interp1d(x,y,kind='cubic')
                        shell.radial_function = f(tin.radial_mesh)
                    else:
                        raise NotImplementedError

    def symm_z(self, k, si, Z_irr, orbital_evaluator_container, atoms = "all", ls = range(3), bases = None):
        #Rotates into a new k point AND into a new basis (per desired l and atom), 
        #returns rotated Z and indices of Z which were rotated (where atom and l are)

        k_irr = self.dft.k_irr[k] - 1 - self.dft.shard_offset
        #if k_irr == k and bases is None: return Z

        if not hasattr(self, "st") and atoms == "all":
            self.st = self.plugin.GetStructure()
            atoms = range(self.st.num_sites)

        ig = self.dft.ig[k] - 1 #python index, the rotation from irr_k to k
        ip = orbital_evaluator_container.basis.atom_to_equiv_under_op - 1
        
        nkind = 3

        Z = np.zeros_like(Z_irr[:,:,k_irr,si])
        rotated_indices = []

        for il,l in enumerate(ls):
            wigs = getattr(self.dft.sg, f"WigD{l}" if self.dft.nrel<2 else f"WigD{l}J")
            rotation = Matrix(wigs[:,:,ig])
            if bases is not None:
                rotation = rotation * bases[il]

            for iatom in atoms:
                jatom = ip[iatom,ig]
                jsort = self.dft.st.distinct_number[jatom] - 1

                for n in range(orbital_evaluator_container.max_n[jsort]):
                        for kind in range(nkind): # there are not necessarily all three kinds in this n,l

                            keys = [f"{jatom+1}.{n}.{kind}.{l}"]
                            if self.dft.nrel > 1:
                                keys.append([f"{jatom}.{n}.{kind}.{-l-1}"])

                            for key in keys:
                                if key in orbital_evaluator_container.orbital_evaluators.keys():
                                    
                                    basis_shell = orbital_evaluator_container.orbital_evaluators[key]

                                    Z[basis_shell.lapw_indices,:] = rotation * Matrix(Z_irr[basis_shell.lapw_indices, :, k_irr, si])

                                    rotated_indices += basis_shell.lapw_indices


        return Z, rotated_indices

    """
    From Sang's ComWannier, roughly


    For a given atom type and angular momentum (l,isort)
    construct a radial function from the space of LAPW basis functions matching that (l, isort)
    which maximizes the overlap in the window

    Two steps: 
        1. get radial coefficients for each basis function which describe this new radial function
        2. construct the projection onto these radial functions

    Done if --apw-adjust=maximze (although it is not only done to APW's!)
    """
    def get_radial_coefficients(self, KS, orbital_evaluator_container):
        """
        Step 1. 
        for each atom, l
            P_cd = 1/N_k sum_ab,kn Z_an Z*_bn sqrt(O_ad) sqrt(O_bc)

            radial_coefficients = eigenvector of P with largest eigenvalue
        """
        
        for ishell, shell in enumerate(self.shells):

            proj = None

            for k, si in product(list(range(self.num_k)), list(range(self.num_si))):

                Z, rotated_indices = self.symm_z(k, si, KS.Z, orbital_evaluator_container, atoms = [shell.atom_number], ls = [shell.l])
                #rotate Z from irreducible zone into actual k
                #rotated_indices are the lapw indices of Z that we bothered to rotate (the rest were at another l and atom number)

                if proj is None:
                    n_per_rad = (2*shell.l+1)*self.dft.nrel
                    nrad = len(rotated_indices)//n_per_rad
                    proj = np.zeros((nrad,nrad), dtype=complex)
                    overlaps = self.lapw.mo_overlap[np.ix_(rotated_indices[::n_per_rad], rotated_indices[::n_per_rad])]
                    s = Matrix(sqrtm(overlaps))
            
                for a,b in product(range(nrad*n_per_rad), repeat=2):
                    for c,d in product(range(nrad), repeat=2):
                        proj[c,d] += np.sum(Z[rotated_indices[a],:] * np.conj(Z[rotated_indices[b],:]) * s[a//n_per_rad,d] * s[b//n_per_rad,c])

            proj /= float(self.num_k)
                        
            e, v = np.linalg.eigh(proj)
            self.shells[ishell].radial_coefficients = (s*Matrix(v[-1,:]).T)
            self.shells[ishell].n_per_rad = n_per_rad
            self.shells[ishell].nrad = nrad

    def poorman_wan(self, KS, orbital_evaluator_container):
        """
        Step 2.
        P[bands,shell] = sum over basis @ l,isort of shell of radial_coefficent[kind,n] * Z_shell^dag[bands, (kind, n)->(lapw indices)]
           (rotated into basis of the shell)
        """

        for ishell, shell in enumerate(self.shells):
            num_equiv = (shell.num_equivalents + shell.num_anti_equivalents)
            num_per_equiv = int((self.shell_offsets[ishell][-1] - self.shell_offsets[ishell][0])/num_equiv)
            start = self.shell_offsets[ishell][0]

            #wigs = getattr(self.dft.sg, f"WigD{shell.l}" if self.dft.nrel<2 else f"WigD{shell.l}J")
            #B = Matrix(shell.basis)

            for iequiv in range(num_equiv):
                rotation = Matrix(shell.rotations[iequiv])
                    
                end = start+num_per_equiv
                shell_slice = slice(start, end)
                start = end

                for k, si in product(list(range(self.num_k)), list(range(self.num_si))):
                    Z, rotated_indices = self.symm_z(k, si, KS.Z, orbital_evaluator_container, atoms = [shell.atom_number+iequiv], ls = [shell.l])
                    Zh = Matrix(Z).H

                    #ig = self.dft.ig[k] - 1 #python index, the rotation from irr_k to k
                    #wigD = wigs[:,:,ig]

                    for i in range(shell.nrad):
                        radial_slice = slice(i*shell.n_per_rad, (i+1)*shell.n_per_rad)

                        self.P[:,shell_slice,k,si] += shell.radial_coefficients[i][0,0] * Zh[:,rotated_indices[radial_slice]] * rotation * shell.subM


    def typical_proj(self, KS, orbital_evaluator_container):

        """
        In contrast to the above, we project onto a LAPW basis function
        OR, we project onto a new radial function, e.g., a gaussian
        """        
        ip = orbital_evaluator_container.basis.atom_to_equiv_under_op - 1

        for ishell, shell in enumerate(self.shells):
            num_equiv = (shell.num_equivalents + shell.num_anti_equivalents)
            num_per_equiv = int((self.shell_offsets[ishell][-1] - self.shell_offsets[ishell][0])/num_equiv)
            start = self.shell_offsets[ishell][0]
            l = shell.l

            for iequiv in range(num_equiv):
                rotation = Matrix(shell.rotations[iequiv])

                B = Matrix(shell.basis)

                end = start+num_per_equiv
                shell_slice = slice(start, end)
                start = end
                
                for basis_shell_key in orbital_evaluator_container.orbital_evaluators.keys():
                    basis_shell = orbital_evaluator_container.orbital_evaluators[basis_shell_key]
                    basis_qns = basis_shell.quantum_numbers
                    iatom, n_, kind_, qn_ = basis_shell_key.split(".")
                    iatom = int(iatom) - 1

                    #all other integrals vanish due to orthonormality of Ylms / disjointedness of MT 
                    # -- we will search for the right shell in the list of equivalents later
                    # wastefull -- precompute the integrals?
                    if basis_qns.l == l and iatom in shell.equiv_atoms: 

                        wigs = getattr(self.dft.sg, f"WigD{basis_qns.l}" if self.dft.nrel<2 else f"WigD{basis_qns.l}J")
                        mesh = basis_shell.radial_mesh / BohrToAng
                        radial_integral = scint.simps(basis_shell.radial_function*shell.radial_function * mesh**2, mesh, even='last')
                        norm = scint.simps(shell.radial_function ** 2 * mesh**2,  mesh, even='last')
                        radial_integral /= np.sqrt(norm) 
                        
                        for k, si in product(list(range(self.num_k)), list(range(self.num_si))):
                            k_irr = self.dft.k_irr[k] - 1 - self.dft.shard_offset
                            if k_irr < 0 or k_irr >= self.num_k:
                                continue

                            #make sure we have the right atom
                            ig = self.dft.ig[k] - 1 #python index, the rotation from irr_k to k
                            
                            if ip[iatom,ig] != shell.equiv_atoms[iequiv]:
                                continue
                            
                            Zk =  Matrix(KS.Z[basis_shell.lapw_indices, :, k_irr, si]).H * rotation * shell.subM
                            if k >= self.dft.num_k_irr:
                                wigD = Matrix(wigs[:,:,ig])
                                Zk = Zk * B.H * wigD * B

                            self.P[:,shell_slice,k,si] += radial_integral*Zk

    def CalculateP(self):
        KS = self.GetBandStructureInWindow()
        is_sharded = KS.is_sharded
        self.num_k = self.dft.num_k_all
        self.shard_offset = KS.shard_offset
        self.num_band = self.max_band-self.min_band+1
        
        #This will load all of the radial functions of the MT basis and figure out quantum numbers
        orbital_evaluator_container = OrbitalEvaluatorContainer(self.dft, self.lapw, prepare_for_spherical_coordinates = False)
        for iorb in range(self.lapw.num_muffin_orbs):
            orbital_evaluator_container.emplace(iorb, None)

        #Construct radial functions which for each l and sort which maximize overlap out of basis of LAPW radial functions (at that l and sort)
        if self.apw_adjust == "maximize":
            self.get_radial_coefficients(KS, orbital_evaluator_container)
            self.poorman_wan(KS, orbital_evaluator_container)

        else:
            if self.apw_adjust != "none":
                self.adjust_radial_functions(orbital_evaluator_container)
            self.typical_proj(KS, orbital_evaluator_container)
                       
        del KS

        if self.normalize or self.ReportCoverage():
            NN = ZeroComplexMatrix(self.num_orbs)  
            for k, si in product(list(range(self.num_k)), list(range(self.num_si))):
                Pk = Matrix(self.P[:,:,k,si])
                tmp = Pk.H * Pk
            
                # just for reporting coverage
                if self.ReportCoverage(): NN += tmp / self.num_k / self.num_si

                if self.normalize:
                    ww, v = np.linalg.eigh(tmp)  # The matrix v satisfies v * w * v.H = Pk.H*Pk 
                    V = Matrix(v)
                    
                    # normalize Pk
                    Pk = Pk * V * np.diag([np.sqrt(1.0/w) for w in ww]) * V.H

                    self.P[:,:, k, si] = Pk[:,:]

                    try:
                        np.linalg.svd(Pk)
                    except:
                        #If we can't do a SVD wannier will fail -- seems to happen when coverage is extremely low?
                        PrintMatrix(f"{k}", Pk)
                        PrintMatrix(f"{k}", abs(tmp))
                        print(ww)
                        quit()
        
        if is_sharded and self.ReportCoverage():
            MPI = self.plugin.GetMPIInterface()
            NN = MPI.COMM_WORLD.allreduce(NN, op=MPI.SUM)
        
        if self.ReportCoverage() and self.plugin.IsMaster():
            PrintMatrix(" - projector coverage (%): ", np.diag(NN).real * 100.0) 

        self.coverage = np.diag(NN).real
    #"""

    def ShouldNormalizeK(self):
        return False

    def __init__(self, orbsDef, energyWin : Window,  dft : DFT, mpic : MPIContainer, bands_hint=None, apw_adjust="none"):
        self.apw_adjust=apw_adjust

        if isinstance(orbsDef, str):
            odef = SelectedOrbitals()
            odef.load(orbsDef)
            orbsDef = odef
            self.orbsDef_str = orbsDef
        else:
            assert(isinstance(orbsDef, SelectedOrbitals))

        self.shells = orbsDef.shells
        
        for ishell, shell in enumerate(self.shells):
            if ishell == 0:
                self.shell_offsets=[[0,shell.dim*(shell.num_equivalents + shell.num_anti_equivalents)]]
            else:
                last_offset = self.shell_offsets[ishell-1][1]
                self.shell_offsets.append( [last_offset, last_offset + shell.dim*(shell.num_equivalents + shell.num_anti_equivalents)] )
        
        Projector.__init__(self, orbsDef, energyWin, bands_hint=bands_hint)
        assert(self.shell_offsets[-1][-1] == self.num_orbs)

        self.num_k_full = self.dft.num_k_all
        self.dis_window = energyWin
        self.A = np.zeros((self.max_band-self.min_band+1, self.num_orbs, self.num_k_full), dtype=np.complex128)
        self.FillAMatrix()
        self.location_of_partial_bs_all_k = "./wannier.core.h5:/bs_partial_bands_all_k"        
        self.isMaster = mpic.IsMaster()

    def FillAMatrix(self):
        self.A[:,:,:] = self.P[:,:,:,0]
    
    def GetAndWriteEig(self, mpic, basis, dft):
        #As above, we need the solution on the full mesh
        self.KS = self.GetBandStructureInWindow()

        energy = GatherAndBroadcastAcrossKShards(mpic, basis, np.swapaxes(self.KS.energy, 0, 1) )
        energy =  np.swapaxes(energy, 0, 1)
        Z = GatherAndBroadcastComplexAcrossKShards(mpic, basis, np.swapaxes(self.KS.Z, 0, 2) )
        Z = np.swapaxes(Z, 0, 2)

        full_KS = LAPW.BandStructure(num_bands = self.KS.num_bands, num_expanded = self.KS.num_expanded, 
                                    num_muffin_orbs = self.KS.num_muffin_orbs, max_num_pw = self.KS.max_num_pw,
                                    num_k = dft.num_k_irr, chemical_potential = self.KS.chemical_potential) 

        full_KS.allocate()
        full_KS.Z = Z
        full_KS.energy = energy
        full_KS.store(self.location_of_partial_bs_all_k)

        del full_KS

    def CalculateAndWriteRealSpaceBloch(self, st, dft: DFT, lapw : LAPW.Basis, opts : Namespace):
        
        unkp = UNKP(st,lapw,dft,self,opts)
        unkp.compute_interstitial()
        unkp.compute_MT(self.KS)
        unkp.carve_spheres_from_interstitial()
        unkp.write()

    def store(self, loc : str):
        wannier.WannierInput.store(self, loc)

class WannierProjector(CorrelatedSubspaceProjector):

    def __init__(self, orbsDef, energyWin : Window, dft : DFT, opts : Namespace, which_shell : int=0, bands_hint=None):
        self.opts = opts
        self.num_k = dft.num_k
        self.is_sharded=dft.is_sharded
        
        if isinstance(orbsDef, str):
            odef = SelectedOrbitals()
            odef.load(orbsDef)
            orbsDef = odef
            self.orbsDef_str = orbsDef
        else:
            assert(isinstance(orbsDef, SelectedOrbitals))

        def total_dim(shell):
            return (shell.num_equivalents + shell.num_anti_equivalents) * shell.dim

        index_total = 0
        slices_total = []
        index = 0
        slices = []
        for shell in orbsDef.shells:
            slices_total.append(slice(index_total, index_total+total_dim(shell)))
            slices.append(slice(index_total, index_total+shell.dim))
            index += shell.dim
            index_total += total_dim(shell)

        if hasattr(self.opts, "concatenate_shells") and self.opts.concatenate_shells:
            self.concatenate_shells = True
            self.total_dim = index_total
            self.dim = index
            self.slice_total = slice(0,index_total)
            self.slice = slice(0,index)
        else:
            self.concatenate_shells = False
            shell = orbsDef.shells[self.opts.which_shell]
            self.total_dim = total_dim(shell)
            self.slice_total = slices_total[self.opts.which_shell]
            self.slice = slices[self.opts.which_shell]

        CorrelatedSubspaceProjector.__init__(self, orbsDef, energyWin, which_shell, bands_hint)
        self.shell.dim = self.dim
    
    def ShouldNormalizeK(self):
        return False

    def OneAtomProjectorOnFullBZ(self, k, si):
        return Matrix(self.A[:,self.slice, k, si])
        
    def GetActualDim(self):
        return self.total_dim

    def GetNumberOfKPoints(self):
        return self.num_k

    def CalculateP(self):
        #MainWannier([],{},self.opts)
        wout = wannier.WannierOutput()
        wout.load("./wannier.h5:/output")

        NN = ZeroComplexMatrix(self.num_orbs)
        for k, si in product(list(range(self.num_k_all)), list(range(self.num_si))):
            Pk = Matrix(wout.P[:, self.slice_total, k, si])
            tmp = Pk.H * Pk
                
            # just for reporting coverage
            if self.ReportCoverage(): NN += tmp / self.num_si
            
            if k < self.num_k:
                self.P[:,:, k, si] = Pk[:,:]
        
        if self.ReportCoverage() and self.plugin.IsMaster():
            PrintMatrix(" - projector coverage (%): ", np.diag(NN).real)

class WannierRunner(MPIContainer):

    input_location = "./wannier.h5:/input"
    output_location = "./wannier.h5:/output"
    orbitals_location = "./wannier.h5:/proj"

    def __init__(self, opts : Namespace):
        self.opts = opts
        MPIContainer.__init__(self,opts)

    def MPIWorker(self):

        self.InitializeMPI()
        self.plugin = DFTPlugin.Instance(restart=True, mpi=self)
        # dft, apw are non-persistent, needed in the construction of persistent fields
        self.dft = self.plugin.GetDFTStuff()
        self.lapw = self.plugin.GetLAPWBasis()

        self.frozen_window = [float(f) for f in self.opts.window.split(":")]
        self.frozen_window = Window(low=self.frozen_window[0], high = self.frozen_window[1])
        self.dis_window = [float(f) for f in self.opts.dis_window.split(":")]
        self.dis_window = Window(low=self.dis_window[0], high = self.dis_window[1])

        if hasattr(self.opts, "orbitals"):

            if self.opts.orbitals == "search":

                shells_to_keep = MainOrbitalAnalysis(self.opts.argv + ["--orb-search","-C0.15",f"-w{self.frozen_window.low}:{self.frozen_window.high}"], {})
                
                self.opts.orbitals = shells_to_keep

                wan_odef = OrbitalsDefinition(self.opts)
                wan_odef.store(self.orbitals_location)

            else:
                wan_odef = OrbitalsDefinition(self.opts)
                wan_odef.store(self.orbitals_location)
        else:
            wan_odef = SelectedOrbitals()
            wan_odef.load(self.orbitals_location)

        if self.dft.num_si == 2:
            raise NotImplementedError()
        
        wan_proj = WannierProjectorGenerator(wan_odef, Window(low = self.dis_window.low, high = self.dis_window.high), self.dft, self, apw_adjust = self.opts.adjust_apw)

        #Make sure there is at least one orbital per band in the frozen window
        while True: 
            try:
                frozen_min_band, frozen_max_band = CalculateBandBoundsWithinWindow(self.plugin, self.frozen_window.low, self.frozen_window.high)
            except:
                frozen_max_band = 0
                frozen_min_band = 0
            if frozen_max_band - frozen_min_band + 1 > wan_proj.num_orbs:
                span = self.frozen_window.high - self.frozen_window.low
                self.frozen_window.low += span*0.1
                self.frozen_window.high -= span*0.1
            else:
                break

        #Make sure there is at least one band per orbital in the disentanglement window
        changed = False
        while True: 
            dis_min_band,dis_max_band = CalculateBandBoundsWithinWindow(self.plugin, self.dis_window.low, self.dis_window.high)
            if dis_max_band - dis_min_band + 1 < wan_proj.num_orbs:
                changed = True
                span = self.dis_window.high - self.dis_window.low
                self.dis_window.low -= span*0.1
                self.dis_window.high += span*0.1
            else:
                break
        if changed:
            WannierProjectorGenerator(wan_odef, Window(low = self.dis_window.low, high = self.dis_window.high), self.dft, self, apw_adjust = self.opts.adjust_apw)


        print (f"{frozen_max_band - frozen_min_band + 1} bands and {wan_proj.num_orbs} orbitals in window {self.frozen_window.low}:{self.frozen_window.high}")
        print (f"{wan_proj.max_band - wan_proj.min_band + 1} bands and {wan_proj.num_orbs} orbitals in window {self.dis_window.low}:{self.dis_window.high}")
        
        wan_proj.store(self.input_location)
        wan_proj.GetAndWriteEig(self, self.plugin.GetLAPWBasis(), self.dft)
        
        self.WriteInput(wan_proj)

        if self.opts.visualize:
            wan_proj.CalculateAndWriteRealSpaceBloch(self.plugin.GetStructureFromIni(), self.dft, self.plugin.GetLAPWBasis(), self.opts)

        if self.opts.input_only:
            "Generated input projector (orbitals)."
            return

        Store.FlushAll()

        self.Barrier()
        wan_proj.Compute(self.input_location, self.output_location)
        
        st = self.plugin.GetStructure()
        print(st.sites)

        Store.FlushAll()
        
        if self.IsMaster():
            print("Done with Wannier90 calculation")

        sys.stdout.flush()

        self.Terminate()
        
    def WriteControl(self, wan_proj, f):
        f.write(f"num_wann = {wan_proj.num_orbs}\n")
        f.write(f"num_iter = 1000\n")
        f.write(f"conv_tol = 0.000001\n")
        f.write(f"conv_window = 10\n")
        f.write(f"num_bands = {wan_proj.max_band - wan_proj.min_band + 1}\n")
        f.write(f"fermi_energy = 0.0\n")
        f.write(f"dis_win_max = {self.dis_window.high}\n")
        f.write(f"dis_win_min = {self.dis_window.low}\n")
        f.write(f"dis_froz_max = {self.frozen_window.high}\n")
        f.write(f"dis_froz_min = {self.frozen_window.low}\n")
        f.write(f"dis_num_iter = 1000\n")
        f.write(f"iprint = 3\n")
        f.write(f"num_cg_steps = 5\n")
        f.write(f"berry = false\n")
        f.write(f"search_shells = 20\n\n")

    def WriteNonLibraryInputs(self,wan_proj,f):
        basis = self.plugin.GetLAPWBasis()
        st = self.plugin.GetStructureFromIni()

        kg = KGrid()
        kg.load("./ini.h5:/kg/")
        prec=8

        f.write("begin unit_cell_cart\n")
        f.write("ang\n")
        for vec in st.lattice.matrix:
            f.write(" ".join(str(e*BohrToAng) for e in np.round(vec,prec)))
            f.write("\n")
        f.write("end unit_cell_cart\n\n")

        f.write("begin atoms_frac\n")
        for (at, vec) in zip(st.species, st.frac_coords):
            f.write(f"{at} ")
            f.write(" ".join(str(e) for e in np.round(vec,prec)))
            f.write("\n")
        f.write("end atoms_frac\n\n")

        #f.write("guiding_centres = .true.\n")
        f.write("begin projections\n")
        st = self.plugin.GetStructure()
        for shell in wan_proj.shells:
            el = str(st.sites[shell.atom_number].specie)
            el_clean = ""
            for c in el:
                try:
                    int(c)
                except:
                    el_clean += c
            if len(el_clean) == 1:
                el_clean = "_" + el_clean

            f.write(f"{el_clean}:l={shell.l}\n")
        f.write("end projections\n\n")

        f.write(f"mp_grid : {kg.ndiv[0]} {kg.ndiv[1]} {kg.ndiv[2]}\n\n")

        f.write("begin kpoints\n")
        for k in basis.KPoints:
            k_full = np.round(Matrix(st.lattice.matrix) * Matrix(k).T, prec)
            f.write(" ".join(str(x[0]) for x in k_full))
            f.write("\n")
        f.write("end kpoints\n\n")

    def WriteBandPlotInput(self,f):
        f.write("bands_plot : true\n")
        f.write(f"bands_num_points : {self.opts.kdensity}\n")
        f.write("bands_plot_format : gnuplot\n\n")

        f.write("begin kpoint_path\n")
        
        st, hskp = getHighSymmKpath(self.plugin, self.opts)
        kpath, labels = CreateKPath(hskp, self.opts, cartesian=False, strip_repeated=False)

        i = 0
        for ik, label in enumerate(labels):
            if label != '':

                f.write(label+" ")
                f.write(" ".join(str(e) for e in np.round(kpath.k[:,ik],5)))
                if i%2:
                    f.write("\n")
                else:
                    f.write(" ")
                i+=1

        f.write("end kpoint_path\n\n")

    def WriteVisualizationInput(self,f):
        f.write("wvfn_formatted = .true.\n")
        f.write("wannier_plot = .true.\n\n")
        if self.dft.num_si == 2: 
            f.write("spinor = .true.\n")
            f.write("wanier_plot_spinor = .true.\n")

    def WriteInput(self, wan_proj):

        with open("_wannier.win", "w") as f:
            self.WriteControl(wan_proj,f)

            #if self.opts.input_only:
            self.WriteNonLibraryInputs(wan_proj, f)

            if self.opts.plot_bands:
                self.WriteBandPlotInput(f)

            if self.opts.visualize:
                self.WriteVisualizationInput(f)

            if self.opts.symmetry_adapted:
                f.write("site_symmetry = .true.\n\n")

        if self.opts.symmetry_adapted:
            self.write_dmn(wan_proj)
            
    
def MainWannier(args, kwargs, opts=None):

    parser = WannierParser()
    
    if opts is None:
        opts = parser.parse_args(args)
        opts.which_shell = 0

    else:
        default_opts = parser.parse_args([])
        defaults = {key: parser.get_default(key) for key in vars(default_opts)}
        for key in defaults.keys():
            if not hasattr(opts, key):
                setattr(opts, key, defaults[key])

    for key in kwargs:
        setattr(opts, key, kwargs[key])
        
    runner = WannierRunner(opts)
    runner.Run()

    wout = wannier.WannierOutput()
    wout.load("./wannier.h5:/output")

    for i in range(wout.centers.shape[-1]):
        print(wout.centers[:,i])

    print(wout.nearest_atom)

if __name__ == '__main__':
    MainWannier(sys.argv[1:],{})
    
