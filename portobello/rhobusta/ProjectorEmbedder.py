#!/usr/bin/env python3

'''
Created on May 16, 2019

@author: adler
'''

import os
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.multi_impurity import MultiImpurityEmbedder
from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer
import sys
from portobello.generated.base_types import Window
from portobello.bus.Matrix import Matrix, ZeroComplexMatrix
import numpy as np
from itertools import combinations, product
from cmath import sqrt
from portobello.generated.atomic_orbitals import AtomicShell, OrbitalsBandProjector,\
    SelectedOrbitals
from portobello.rhobusta.orbitals import PrintMatrix, OrbitalsDefinition
from portobello.rhobusta.DFTPlugin import DFTPlugin
from argparse import ArgumentDefaultsHelpFormatter
from pathlib import Path
from portobello.rhobusta.observables import GetAllObservables, GetSubspace
from portobello.generated.LAPW import KPath
from scipy.linalg.special_matrices import block_diag
import portobello.symmetry.WignerDJ as wigdj# make sure we load the wigner J matrices
from portobello.symmetry.magmom import GroupAnalyzerWithMagmom


# report the range of all bands that *intersect* the window [dis_low, dis_high] (in eV)
# this may include bands that oscillate through this window (that is, continue above and below it), but with a large enough window, 
# this should be avoided.
def CalculateBandBoundsWithinWindow(plugin, dis_low, dis_high, debug=False):           
    dft = plugin.GetDFTStuff()
    # minBand, maxBand determine the non-excluded band window around FS 
    minBand = sys.maxsize
    maxBand = -1
    minNumBands = sys.maxsize
    if debug:
        print("porjector: num_si= %d, num_k_irr= %d in the projector:"%( dft.num_si, dft.num_k_irr))
        print("porjector: looking in window", dis_low, dis_high)
    
    for k in range(dft.num_k):
        KS = plugin.GetAllBandsForKpoint(k + dft.shard_offset) # fortran convention
        for si in range(dft.num_si):
            num_bands = KS.num_bands[0, si]
            minNumBands = min(num_bands, minNumBands)
            if debug and k==1: 
                print("porjector: mu", KS.chemical_potential)
                print("porjector: num bands for si=%d : %d"%(si, num_bands))
                print("porjector: bands: ", KS.Ek[0:num_bands,0,si])
            
            # energies, shifted by the chemical potential, in eV
            # The band energies reside in the first num_bands array entries, sorted from low to high
            Ek = (KS.energy[0:num_bands,0,si] - KS.chemical_potential)* Projector.Ry2eV  # relative to \mu, in eV
            # form the filter of bands in the window
            bandsInWindow = np.logical_and(Ek <= dis_high, Ek >= dis_low)
            numActiveBands =  np.sum(bandsInWindow)
            if numActiveBands == 0:
                raise Exception(f"number of bands in window is 0 for k={k} chemical potential={KS.chemical_potential}")
            
            bandsInMarginWindow = np.logical_and(Ek <= dis_high, Ek >= dis_low)
            
            windowIndices = [x[0] for x in np.argwhere(bandsInMarginWindow)]
            #print(f"window indices: {windowIndices}")
            maxBand = max(maxBand, np.max(windowIndices))
            minBand  = min(minBand, np.min(windowIndices))
                
            del Ek
            del bandsInMarginWindow
        del KS  # release some memory

    if dft.is_sharded:
        MPI = plugin.GetMPIInterface()
        MPI.COMM_WORLD.Barrier()
        maxBand = MPI.COMM_WORLD.allreduce(maxBand, op=MPI.MAX)
        minBand = MPI.COMM_WORLD.allreduce(minBand, op=MPI.MIN)
        minNumBands = MPI.COMM_WORLD.allreduce(minNumBands, op=MPI.MIN)
    maxBand = min(maxBand, minNumBands-1) # make sure the subspace for all k is of the same size
    return minBand,maxBand # shifted from 0 for python

class Projector(OrbitalsBandProjector):
    '''
    This is a projector of a window of bands into a set of orthogonal orbitals, which may contain one or more shells of atoms.
    The atoms may be equivalent or not, but they should be different.
    This projector could be used for various puposes. For example:
    1. Build tight-binding models using the orbitals
    2. Build local green functions and then embed them back in the band model
    etc.
    '''
    
    Ry2eV = 27.2107*0.5  # from Kutepov, don't change unless it changes there

    # The following can be overriden - for example to modify the behavior for a projector along a path
    
    def GetNumberOfKPoints(self):
        return self.dft.num_k

    def ReportCoverage(self):
        return True
    
    def GetBandBoundsWithinWindow(self):
        return CalculateBandBoundsWithinWindow(self.plugin, self.energyWin.low, self.energyWin.high)
    
    # Note that now the projector owns a band structure object for the given range of bands
    def GetBandStructureInWindow(self):
        if self.KS is None:
            self.KS = self.plugin.GetBandsForAllKpoints(self.min_band-1, self.max_band-1)
        return self.KS

    def GetBandStructureAndGradientInWindow(self):
        return self.plugin.GetBandsAndGradientsForAllKpoints(self.min_band-1, self.max_band-1)
    
    def ShouldNormalizeK(self):
        return True
    
    # note that this doesn't change the projector, only the bands
    #def RefreshBands(self):
    #    self.KS = None #deferred
    
    def __init__(self, orbsDef : SelectedOrbitals, energyWin : Window, bands_hint=None):
        '''
        Constructor
        '''
        self.plugin = DFTPlugin.Instance()
        self.dft = self.plugin.GetDFTStuff()
        self.lapw = self.plugin.GetLAPWBasis()

        self.orbsDef = orbsDef
        self.energyWin = energyWin # transient
        self.KS = None
        
        OrbitalsBandProjector.__init__(self, 
            min_band = -1,
            max_band = -1,
            num_k_all = self.GetNumberOfKPoints(),
            num_orbs =  self.GetActualDim(),
            num_si =    self.dft.num_si)      
        
        if bands_hint is not None and bands_hint.window_min_band > -1 and bands_hint.window_max_band > 0:
            self.min_band = bands_hint.window_min_band
            self.max_band = bands_hint.window_max_band
        else:
            if self.plugin.IsMaster(): print("- recalculating window bands")
            self.min_band, self.max_band = self.GetBandBoundsWithinWindow()
        self.min_band += 1  # for fortran
        self.max_band += 1  # for fortran
        
        self.normalize = self.ShouldNormalizeK()
        
        self.allocate()
        self.CalculateP()
    
    def GetActualDim(self):
        return self.orbsDef.total_dim
        
   
    def GetActualV(self):
        return self.orbsDef.V
    
             
    def CalculateP(self):
        KS = self.GetBandStructureInWindow()
        is_sharded = KS.is_sharded
        self.num_k = KS.num_k
        self.shard_offset = KS.shard_offset

        mtO = Matrix(self.lapw.mo_overlap)
        
        # we need P*E(T) = T for all O and P(I) = I. 
        # This is critical for the DMFT equation to be meaningful.
        # Since the band basis vector for k
        # are complete in the unit cell, this is almost true for Ek=Pk (Pk.H*Pk ~ I), 
        # but since we are looking at a smaller window, we have to normalize Pk a bit.
        # 
        
        NN = ZeroComplexMatrix(self.num_orbs)
        
        # this is the input: note only one k-point and one spin index needed to represent this matrix
        MTOrbitals = mtO.H * self.GetActualV()
       
        # calculate the Wannier initial A matrix - this goes directly to Wannier input
        for k, si in product(list(range(self.num_k)), list(range(self.num_si))):
            Zk = Matrix(KS.Z[:,:,k, si])
            # normalize the orbital first - some are not usually normalized (APW1 for exmaple)
                            
            # TODO: also add the interstitial part (for example, for wannier orbitals)
            Pk = Zk.H * MTOrbitals
            tmp = Pk.H * Pk
                
            # just for reporting coverage
            if self.ReportCoverage(): NN += tmp * self.lapw.weight[k] / self.num_si
            
            if self.normalize:
                ww, v = np.linalg.eigh(tmp)  # The matrix v satisfies v * w * v.H = Pk.H*Pk 
                V = Matrix(v)
                 
                # normalize Pk
                Pk = Pk * V * np.diag([sqrt(1.0/w) for w in ww]) * V.H

            self.P[:,:, k, si] = Pk[:,:]
        del KS

        if is_sharded and self.ReportCoverage():
            MPI = self.plugin.GetMPIInterface()
            NN = MPI.COMM_WORLD.allreduce(NN, op=MPI.SUM)
        
        if self.ReportCoverage() and self.plugin.IsMaster():
            PrintMatrix(" - projector coverage (%): ", np.diag(NN).real * 100.0) 

        self.coverage = np.diag(NN).real

    def GetEigenstateProjections(self):
        def abs2(z):
            return z.real*z.real + z.imag*z.imag
        eps = []
        eps.extend(abs2(self.P[:,:,:,:].flatten()))
        return eps
        
def GetSuffix(orbsDef : SelectedOrbitals, which_shell : int):
    if which_shell < 0:
        return "" # no suffix
    assert(which_shell < orbsDef.num_shells)
    shell = orbsDef.shells[which_shell]
    # TODO: get access to nspin here, to make this rigorous
    # first case - nspin=1 and total dim equal shell*dim with all equivalents
    # second case - nspin=2 and total dim can have AFM, and spin
    # in multi-impurity - always have index
    if not os.path.exists("./multi-impurity.h5") and (
        orbsDef.total_dim == shell.dim * shell.num_equivalents or \
        orbsDef.total_dim == 2*shell.dim * (shell.num_equivalents+shell.num_anti_equivalents)):
        return "" # no suffix
    return str(which_shell)
        
# note that this projector possibly holds the definition of multiple inequivalent shells (some may be of different type
# such as s,p,d,f), like its superclass, but also has one designated shell. Projection is actually restricted to this shell.
# - basically self.shell designates the relevant correlated subspace.


class CorrelatedSubspaceProjector(Projector):
    
    # orbsDef is of type SelectedOrbitals (persistent)
    def __init__(self, orbsDef, energyWin : Window, which_shell : int=0, bands_hint=None):
        if isinstance(orbsDef, str):
            odef = SelectedOrbitals()
            odef.load(orbsDef)
            orbsDef = odef
            self.orbsDef_str = orbsDef
        else:
            assert(isinstance(orbsDef, SelectedOrbitals))
        subspace_expr = orbsDef.subspace_expr
        assert(which_shell < orbsDef.num_shells and which_shell >=0)
        #assert(orbsDef.shells[which_shell].l >=2)
        self.l = orbsDef.shells[which_shell].l 
        # note we have to set this before construction of hte projector, because a virtual method
        # is used to get the dimension from here
        self.shell =  orbsDef.shells[which_shell]
        self.which_shell = which_shell

        if subspace_expr != "":
            obs = GetAllObservables(self.shell, printout=False)
            
            self.subspace = GetSubspace(self.shell, obs, subspace_expr)
            
            self.shell.dim = np.count_nonzero(self.subspace)
            
            self.shell.rep = self.shell.rep[self.subspace, :][:, self.subspace]
            self.shell.raw_rep = self.shell.raw_rep[self.subspace, :][:, self.subspace] #not exactly raw -- but raw in the subspace
            self.shell.basis = self.shell.basis[:, self.subspace]

            self.subspace = np.tile(self.subspace, self.shell.num_equivalents + self.shell.num_anti_equivalents)

        else:
            self.subspace = [True] * self.GetActualDim()
        
        Projector.__init__(self, orbsDef, energyWin, bands_hint=bands_hint)
        self.orbsDef.total_dim = (self.shell.num_equivalents + self.shell.num_anti_equivalents) * self.shell.dim
        
    def EmbeddedIntoKBandWindow(self, M, k, si):
        if self.shell.num_equivalents+self.shell.num_anti_equivalents > 1:  #optimization
            # add all equivalent blocks
            blocks = [M[:,:,si] for _i in range(self.shell.num_equivalents)]
            # add all anti-equivalent blocks (in the end)
            blocks.extend([M[:,:,1-si] for _i in range(self.shell.num_anti_equivalents)])
            Mfull = Matrix(block_diag(*blocks))
        else:
            Mfull = Matrix(M[:,:,si])
        #self.PrintMatrix(M)
        Pk = Matrix(self.P[:,:,k,si])
        return Pk * Mfull * Pk.H
  
    def EmbeddedDiagonalTensorIntoKBandWindow(self, T, kind, k1, k2, si1, si2): 
        assert(kind == 0 or kind == 1)
        
        Pk1 = Matrix(self.P[:,:,k1,si1])
        Pk2 = Matrix(self.P[:,:,k2,si2])

        if not hasattr(self,f"diagonal_einsum_path"):
            
            self.diagonal_einsum_path_str = ["ia,ja,ac,ck,cl->ijkl", "ia,jc,ac,ck,al->ijkl"]
            self.diagonal_einsum_path = [np.einsum_path(path_str, Pk1, Pk2, T, Pk1.H, Pk2.H, optimize = "optimal" ) for path_str in self.diagonal_einsum_path_str]

        return np.einsum(self.diagonal_einsum_path_str[kind], Pk1, Pk2, T, Pk1.H, Pk2.H, optimize = self.diagonal_einsum_path[kind][0])

    def GetActualDim(self):
        return self.shell.dim *(self.shell.num_equivalents + self.shell.num_anti_equivalents)
    
    # define P's to be only on the correlated subspace (specific equivalence class typically)
    def GetActualV(self):
        if self.shell.end_index != 0:
            return (self.orbsDef.V[:,self.shell.start_index : self.shell.end_index])[:, self.subspace]
        else:
            return (self.orbsDef.V[:,self.shell.start_index : ])[:, self.subspace]
            
    def GetSuffix(self):
        return GetSuffix(self.orbsDef, self.which_shell)
 
    def OneAtomProjector(self, k, si):
        return Matrix(self.P[:,:self.shell.dim, k, si])

             
class KPathProjector(CorrelatedSubspaceProjector):
    
    # just override a few functions
    
    def __init__(self, BZProjector : CorrelatedSubspaceProjector, kpath : KPath , which_shell : int = 0, orbsDef = None):
        self.bzp = BZProjector
        self.kpath = kpath

        if orbsDef is None:
            CorrelatedSubspaceProjector.__init__(self, "./projector.h5:/def/", self.bzp.energyWin, which_shell)
        else:
            CorrelatedSubspaceProjector.__init__(self, orbsDef, self.bzp.energyWin, which_shell)
        
    def GetNumberOfKPoints(self):
        return self.kpath.num_k
    
    def GetBandBoundsWithinWindow(self):
        return self.bzp.min_band-1, self.bzp.max_band-1  #python convention
    
    def GetBandStructureInWindow(self):
        if self.KS is None:
            self.KS = self.plugin.GetBandsForKPath(self.kpath, self.min_band-1, self.max_band-1)
        return self.KS
      
    def ReportCoverage(self):
        return False

    def ShouldNormalizeK(self):
        return True
    
# only FULL equivalence classes of atoms are added (e.g., never one atom out of multiple equivalents)
# basis - the default is 'FC'. It is RSH (real spherical harmonics) with a modification for f-shells
#         which makes it diagonalize cubic fields, which seems like the "best" choice overall.
def DefineProjector(opts):         
    mpi = MPIContainer(opts)
    plugin = DFTPlugin.Instance(restart=True, mpi=mpi)
    
    orbsDef = OrbitalsDefinition(opts)
    orbsDef.store(opts.projector, flush=True)

    mag = GroupAnalyzerWithMagmom.RecoverMagneticMoments(plugin.GetStructure())
    if mag is not None and not opts.force_mi:
            return # for now, we don't create MI for AFM runs. Figure out in future how these things interwork in SDW, multi-AFM, etc.

    MultiImpurityEmbedder.InitializeFrom(orbsDef)


def TestProjector(ewin_str, storeP=False):
               
    ewin = [float(f) for f in ewin_str.split(":")]
    energyWindow = Window(low=ewin[0], high=ewin[1])
        
    ob = SelectedOrbitals()
    ob.load("./projector.h5:/def/")
    p = Projector(ob, energyWindow)
    if storeP:
        p.store("./projector-bands.h5:/", flush=True)
    sys.stdout.flush()

    return p
    
def ProjectorOptionsParser(add_help=False, private=False):
    parents = []
    if private: parents = [GetMPIProxyOptionsParser()]

    parser = ArgumentParserThatStoresArgv('proj', add_help=add_help,
                            parents=parents,
                            formatter_class=ArgumentDefaultsHelpFormatter)    

    pp = parser.add_argument_group(title="Projector arguments",
                              description="argument for defining the projector and the local basis (for correlated calculations)")
   
    pp.add_argument("-s", "--orbital-subshells", dest="orbitals", default=None,
                        required=not Path("./projector.h5").exists(),
                      help=""""List of comma-separated orbitals in the form N(el):n[spdfe] where:
                            N is the number of the atom in the unit cell (starting at 0), useful if there are multiple
                            equivalence classes of the same kind of atom.
                            N can be dropped, in which case the first distinct element (el) will be used.\n
                            el is the element (which can be dropped, but then N is needed)\n
                            for example 1Fe:2d,Se:1s,2:3d""")
                            
    pp.add_argument("-w", "--energy-window", dest="window",
                      help="energy window for the bands (in eV)",
                      default='-10.0:10.0')
                        
    # note that RSH is also the native basis in Kutepov's code for non-relativistic but J is the native basis for relativistic DFT.
    pp.add_argument("-B", "--basis", dest="basis", 
                      help="""Name basis to use:
                            "native": nonrelativistic = RSH labeld by L,L_z, for relativistic - RSH labeled by J=L+S,J_z 
                            "FC": basis for f that diagonalizes H,G in cubic field, real-spherical harmonics for l>3
                                  this is more useful than RSH because it diagonalizes the rep. for a lot of f crystal fields
                                  whereas RSH doesnt (default) 
                            "RSH": real spherical harmonics,
                            "adapt" adapt the default basis to the symmetry, labels are not necessarily preserved, also the quantization axis is not rotated,
                            "SH": spherical harmonics""", 
                      default="FC")

    parser.add_argument("--apw-projector", dest="loc_projector", 
                          action="store_false",
                          default=True)
        
    parser.add_argument("-F", "--projector-subspace", dest="proj_expr",
                    default="",
                    help="""
                        restrict the projector using np.diag(proj_expr) where proj_expr 
                        is the given expression. proj_expr should be a condition, posssibly formulated using the observables algebra.
                        For example, "j < 3" on an f-shell will restrict to the 5/2 subspace. Asimilar result
                        would be the condition "jlow > 0". Typically diagonal operators should be used, because
                        we end up taking only the diagonal boolean values, but there is no such restriction.
                        Note that the equality condition should be used carefully, since numerical errors
                        mean that equality should be evaluated with a small non-zero tolerance. 
                    """) 
    
    pp.add_argument("-X", "--approx-rep", dest="approx_rep", 
                  help="""kind of approximation to use for the representation:
                        "diag": keep only the diagonal of the representation (even if there are nonzero offdiagonals),
                        "none": keep all off-diagonals, and number them,
                        "full-diag": number the diagonal from 1 to its dimension,
                        "0CF": |J^2 j_m> basis with only magnetic splitting, no crystal field,
                        "jj": J^2 - 2 blocks in the J basis, each for the j value""", 
                  default="diag")

    pp.add_argument("--break-local-symmetry", dest="moment", default="", help="if vector (3 numbers separated by ','),"+
                    " it describes the full magnetic moment, if a number, it is the size of the moment along the z axis.")
    

    pp.add_argument("--symmetry-breaking", dest="symmetry_breaking", default="M", 
                    help="""if 'moment', breaks along the moment. Otherwise, uses the string
                         as an observable name to break along O \dot moment, where the moment is defined
                         by the --break-local-symmetry option.""")
    
    pp.add_argument("--pre-adapt-eig", dest="pre_adapt_eig", default="", 
                    help="""an observable expression (e.g Mx) which should be diagonalized to find 
                    the initial eigenfunctions, which are used as the baseline for symmetry adaptation.
                    The effective choices if none is given, are Lz for non-rel, and Jz for rel.
                    If a list of observables is given, they have to commute, and then they are co-diagonalize to define
                    the initial basis (possibly before adaptation), which conserves their quantum numbers. For example
                    the argument 'J2,Jx' will adapt for quantization in Dirac basis along x.""")
    
    parser.add_argument("--radius-local", dest="r", type=float,
                      default=0.0, help="use this radius (do not calculate)")
    
    pp.add_argument("--projector", dest="projector", type=str, default="./projector.h5:/def/",
                    help="address to which to save - or read the projector from")

    pp.add_argument("--force-mi", dest="force_mi", 
                    default=False,                           
                    action="store_true",
                    help="force an mcif run to be multi-impority for the different magnetic lables (not AFM)")
    
    pp.add_argument("--cluster", dest="cluster", 
                    default=False,                           
                    action="store_true",
                    help="Project onto all equivalent atoms in the structure to perform a cluster-correlated simulation")
                    
    
    if private:
        parser.add_argument("--store", dest="store_proj", 
                          action="store_true",
                          default=False)
        
        parser.add_argument("--overlap-histogram ", dest="show_overlaps", 
                          action="store_true",
                          default=False)
            
        parser.add_argument("--which-shell", dest="which_shell", default=0, 
                        help="which shell (index) to project to, in order to build the correlated problem")
                        
        parser.add_argument("--spacegroup-angle-tolerance", 
                            dest='spacegroup_angle_tolerance',
                            type=float,
                            default=5.0,
                            help="angle tolerance for spacegroup diagnosis")

        parser.add_argument("--directory", dest="dir", 
                            default=".",
                            help="""Directory in which to run. [for organized runs only]""")

    
    return parser

def PROJ(opts):

    curdir = os.getcwd()
    if hasattr(opts,"dir"):
        try:
            os.chdir(opts.dir)
        except:
            pass

    mpi = MPIContainer(opts)

    plugin = DFTPlugin.Instance(restart=True, mpi=mpi)
    
    if not opts.show_overlaps:
        DefineProjector(opts)
    
    p = TestProjector(ewin_str=opts.window, storeP=opts.store_proj)

    if opts.show_overlaps:
        import matplotlib.pyplot as plt
        csp = CorrelatedSubspaceProjector(opts.projector,
                                          Window(low=-10,high=10), False)
        ovls = csp.GetEigenstateProjections()
        print(f"#overlaps: {len(ovls)}")
        plt.hist(ovls)
        plt.show()
        
    os.chdir(curdir)


if __name__ == '__main__':
    parser = ProjectorOptionsParser(add_help=True, private=True)
    opts, argv_ = parser.parse_known_args(sys.argv[1:])
    PROJ(opts)
      
    

        
