#!/usr/bin/python3

'''
Created on Apr 29, 2018

@author: adler
'''
import sys
from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer
from portobello.generated import  wannier
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.loader import AnalysisLoader

class WXSFRunner(MPIContainer):

    def MPIWorker(self):

        wan_output_location = "./wannier.h5:/output"
        self.InitializeMPI()
        plugin = DFTPlugin.Instance(restart=True, mpi=self)
        ret = wannier.WannierOutput.WriteXSF(wan_output_location)

if __name__ == '__main__':


    parser = GetMPIProxyOptionsParser()
    opts = parser.parse_args(sys.argv[1:])
    
    runner = WXSFRunner(opts)
    runner.Run()

