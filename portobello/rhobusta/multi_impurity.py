'''
Created on Jul 31, 2021

@author: adler
'''

from argparse import Namespace
from pathlib import Path
from portobello.bus.persistence import Store
from portobello.rhobusta.orbitals import OrbitalsDefinition
from portobello.generated.MI import MultiImpuritySupervisor
import importlib
import gc
from portobello.bus import Matrix

MULTI_IMPURITY_SINGLETON="./multi-impurity.h5:/"

class MultiImpurityEmbedder(MultiImpuritySupervisor):


    # this should be called from the (main) projector, to create the mutli-impurity file
    @staticmethod
    def InitializeFrom(orbsDef : OrbitalsDefinition):
        if orbsDef.num_shells < 2:
            return
        mi = MultiImpuritySupervisor(num_impurities=orbsDef.num_shells)
        mi.allocate()
        mi.cls = ""
        for i in range(mi.num_impurities):
            mi.impurity_problem[i].file_name = ""
        mi.store(MULTI_IMPURITY_SINGLETON, flush=True)
        Store.Singleton().CloseFile("./multi-impurity.h5") # so that we will overwrite it next time

    # this should be called from the solver, to report its impurity solver
    @staticmethod
    def StoreImpurityDetails(problem, opts):
        if not  Path(MULTI_IMPURITY_SINGLETON.split(":")[0]).exists():
            return 
        mi = MultiImpuritySupervisor()
        mi.load(MULTI_IMPURITY_SINGLETON)
        if problem.__class__.__name__ == "Gutzwiller":
            mod = "portobello.rhobusta.gutzwiller"
        elif problem.__class__.__name__ == "DMFTOrchastrator" or problem.__class__.__name__ == "RerunOrchastrator":
            mod = "portobello.rhobusta.dmft"

        cls = problem.__class__.__name__ 
        if mi.solver_class != "":
            assert(cls == mi.solver_class) # make sure all processes have the same class
            assert(mod == mi.solver_module)
        mi.solver_class = cls
        mi.solver_module = mod
        mi.ready = True
        mi.impurity_problem[opts.which_shell].file_name = problem.self_path

        mi.store(MULTI_IMPURITY_SINGLETON, flush=True)
        Store.Singleton().CloseFile("./multi-impurity.h5:/") # so that we will overwrite it next time


    # the rest is called from dft+X
    # ------------------------------------

    def __init__(self, opts : Namespace):
        MultiImpuritySupervisor.__init__(self)
        self.load(MULTI_IMPURITY_SINGLETON)
        assert(self.ready)

        mod = importlib.import_module(self.solver_module)
        self.method = getattr(mod, self.solver_class)
        opts.which_shell=0
        # construct the first impurity
        self.problem0 =  self.method(opts, for_dft=True)
        self.opts = opts
        # this process lives in a shard, and it needs to add up sigma / rho from all
        # the impurity problems, into this shard.
        # We assume that the bands range that was projected for all of them was the same,
        # but of course, the correlated bands that they were projected on depend were diffent (inequivalent)
        # (but we don't care about that, it is hidden by the individual problem solvers)
        
        self.problem0.StartMIEmbed()
        for i in range(1, self.num_impurities):
            # construct ith impurity
            self.opts.which_shell=i
            problem = self.method(self.opts, for_dft=True)
            # combine the ith impurity solution
            if self.problem0.AddMI(problem):
                del problem        
                gc.collect()
        self.problem0.FinalizeEmbed()

    def LoadSigRealOmega(self, Nw=800, bound=0.6, interp_w=False):
        pass

    def EmbedSigma(self, sigma_info, ignore_state=False, retain_result=False):

        # Note that we are writing to a memory file
        # so this should be available to Fortran in the same process
        self.problem0.WriteMIEmbed()
    
    def GetInterfaceForAnalysis(self):
        return self.problem0