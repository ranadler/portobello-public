#!/usr/bin/env python3
import sys, os
import numpy as np
import scipy.interpolate as scinterp
import scipy.integrate as scintegrate
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from argparse import ArgumentDefaultsHelpFormatter, SUPPRESS, Namespace

from portobello.generated.dmft import DMFTState, ImpurityProblem, DMFTFreeEnergy

from portobello.rhobusta.PEScfEngine import PEScfEngine
from portobello.rhobusta.dmft import DMFTOptionsParser, DMFTOrchastrator
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.ctqmc import GetMeasurement
from portobello.rhobusta.DFTPlugin import DFTPlugin

from portobello.bus.mpi import MPIContainer
from portobello.bus.Matrix import Matrix

from portobello.jobs import Organizer
from portobello.jobs.Organizer import with_organizer

KB = 6.333328e-6 *27.2107* 0.5

INFO_HEADER = f"-------------------DMFT Energy Orchestrator-------------------"

energyLocation = "./dmft_free_energy.h5:/"

def PlotFit(func, type = 0):
    x = func.omega
    
    tail_x = x[int(len(x)/2):]
    tail_ix = 1j*tail_x

    fig,ax = plt.subplots(2,1)

    colors=['r','b', 'g', 'k']
    for i in range(func.num_orbs):
        if type == 1:
            fit = func.moments[0,i,i,0]/(tail_ix - func.moments[1,i,i,0])/(tail_ix - func.moments[2,i,i,0])
        elif type == 2:
            fit = tail_ix*func.moments[0,i,i,0]/(tail_ix - func.moments[1,i,i,0])/(tail_ix - func.moments[2,i,i,0])
        elif type == 0:
            fit = func.moments[0,i,i,0]/(tail_ix - func.moments[1,i,i,0])
        
        ax[0].plot(x, np.imag(func.M[:,i,i,0]), color = colors[i])
        ax[0].plot(tail_x, np.imag(fit), color = colors[i], linestyle="--")

        ax[1].plot(x, np.real(func.M[:,i,i,0]), color = colors[i])
        ax[1].plot(tail_x, np.real(fit), linestyle = "--", color = colors[i])

    plt.show()

class EffortResetter:
    """
    opts doesn't seem to want to deep copy, so the effort scaling needs to be reset between 
    DMFTEnergyOrchestrators
    """
    def __init__(self,opts):
        self.time_thermalization = opts.time_thermalization
        self.time_measurement = opts.time_measurement
        self.mpi_workers = opts.mpi_workers

    def reset(self,opts):
        opts.time_thermalization = self.time_thermalization
        opts.time_measurement = self.time_measurement
        opts.mpi_workers = self.mpi_workers

class DMFTEnergyOrchestrator(DMFTOrchastrator):

    """
    -Much like DMFTOrchastrator except we define it at a new temperature, T, at the reference hybridization
    -T is expected in K
    -is_reference_run lets us create an energy orchastrator that wont run CTQMC and will load the impurity measurement
        from the already existing, refence DMFT run
    """
    def __init__(self, ref : DMFTOrchastrator, opts : Namespace, T : float, is_reference_run = False):
        self.opts = opts
        self.is_reference_run = is_reference_run 

        if not opts.info:
            DMFTState.__init__(self)

            is_new = PEScfEngine.__init__(self, opts)

        self.dry_run = True #Don't save results outside of the impurity measurement -- i.e., don't overwrite reference dmft.h5

        self.ref_beta = ref.beta
        self.beta = 1 / T / KB
        
        self.temperature = T

        self.opts.beta = self.beta
        self.opts.temperature = self.temperature

        if self.opts.scale_times:
            self.opts.time_thermalization = self.scale_effort(self.opts.time_thermalization)
            self.opts.time_measurement = self.scale_effort(self.opts.time_measurement)
            
        if self.opts.scale_workers:
                self.opts.mpi_workers =  self.scale_effort(self.opts.mpi_workers)

        #for accounting
        if self.is_reference_run:
            assert(abs(self.beta - ref.beta) < 1e-3 ) 
            self.opts.time_thermalization = 0
            self.opts.time_measurement = 0
            self.opts.mpi_workers = 0

        if not opts.info:
            assert (not is_new) 
             
            if not hasattr(ref,"Elocal"):
                ref.InitOneBody()
            self.Elocal = ref.Elocal
   
            self.num_freqs =  max(1,int(opts.energy_limit * self.beta / (2*np.pi)))
            self.sig = self.NewFreqDepLocalEnergyMatrix()
            self.GImp = self.NewFreqDepLocalEnergyMatrix()
            self.wdhyb = self.NewFreqDepLocalEnergyMatrix()
            self.hyb = self.InterpolatedFreqDepLocalEnergyMatrix(ref.sig.omega, ref.hyb.M, derivative = self.wdhyb,
                    derivative_weights= self.sig.omega)

            if not self.is_reference_run:
                print(INFO_HEADER)
                print(f"Running simulation at temperatures: {self.temperature}K")
                print(f"CTQMC will take {self.opts.time_thermalization + self.opts.time_measurement} min")
                print(f"CTQMC will use {self.opts.mpi_workers} MPI processes",  flush = True)
                print(INFO_HEADER)

    def dispatch(self):
        if not self.is_reference_run:
            self.StoreMyself()
            self.PrepareAndRunCTQMC()

    def PrepareAndRunCTQMC(self):

        if False in self.orbBandProj.subspace:
            explcitU = self.GetUMatrix().flatten()
            Udim = len(explcitU)
        else:
            Udim = 0

        imp = ImpurityProblem(isReal=False,
                              U=self.U,
                              J=self.J,
                              beta=self.beta,
                              subshell=self.corrSubshell,
                              num_si=self.num_si,
                              nrel=self.dft.nrel,
                              num_freqs=self.num_freqs,
                              max_sampling_energy=0.75*self.num_freqs *2*np.pi /self.beta, # in eV. Make this an option?
                              hamiltonian_approximation='none' if self.opts.full_interaction else 'ising',
                              mu=0.0,
                              dim=self.corrSubshell.dim,
                              self_path=self.GetImpurityLocation(),
                              os_dir = f"./Impurity{self.GetSuffixOfImpurity()}{self.GetAdditionalDescriptors()}/",
                              two_body_size=Udim)
        imp.allocate()

        if Udim:
            imp.two_body = explcitU

        imp.hyb = self.hyb
        for si in range(self.num_si):
            # this includes off-diagonals
            imp.E[:,:,si] = self.Elocal[:,:,si] - self.DC[:,:,si]

        self.RunCTQMC(imp)

    def GetImpurityMeasurement(self):

        imp = ImpurityProblem()
        if self.is_reference_run:
            loc = DMFTOrchastrator.GetImpurityLocation(self)
        else:
            loc = self.GetImpurityLocation()
        imp.load(loc)
        return imp, GetMeasurement(imp) 

    def ComputeHighTempFreeEnergy(self):

        imp, IM = self.GetImpurityMeasurement() 
        p0 = IM.expansion_histogram[0]
        if p0 < 1e-5:
            raise Exception("Temperature is not high enough to get a good measurement of the entropy") 
        
        F = -1/self.beta * (IM.lnZ - np.log(p0))

        if self.num_si == 1 and self.nrel == 1:
            F *= 2

        return F

    def ComputeEntropy(self):

        F = self.ComputeHighTempFreeEnergy()

        return (self.internal_energy - F) * self.beta

    def ComputeInternalEnergy(self):
        self.GetHybridizationMoment()

        imp, IM = self.GetImpurityMeasurement() 
        self.GImp = IM.GImp
        self.sig = IM.Sigma
        self.ResizeAll(self.GImp.num_omega*100)
        
        wdhyb = self.NewFreqDepLocalEnergyMatrix()
        self.InterpolatedFreqDepLocalEnergyMatrix(self.sig.omega, self.hyb.M, derivative = wdhyb,
                derivative_weights = self.sig.omega)
        
        #IM.GImp = self.GetHybridizationLikeMoment(IM.GImp)

        #DeltaGimp
        if self.temperature < 300:
            deltaG =  self.FreqMatrix_x_FreqMatrix(self.hyb, self.GImp)
            for si in range(self.num_si):
                deltaG.moments[0,:,:,si] = self.hyb.moments[0,:,:,si]*self.GImp.moments[0,:,:,si]
                deltaG.moments[1,:,:,si] = self.hyb.moments[1,:,:,si]
                deltaG.moments[2,:,:,si] = 0
            dg = self.MatsubaraSum(deltaG, 2)
        else:
            dg = -IM.k/self.beta

        #EimpGImp
        eg=0
        for si in range(self.num_si):
            eg += np.trace(imp.E[:,:,si]*self.RhoImp[:,:,si])
        
        #omega_n dHyb/domega G_imp
        wdhybGimp = self.FreqMatrix_x_FreqMatrix(wdhyb, self.GImp)
        for si in range(self.num_si):
            wdhybGimp.moments[0,:,:,si] = -self.hyb.moments[0,:,:,si] * self.GImp.moments[0,:,:,si]
            wdhybGimp.moments[1:,:,:,si] = self.hyb.moments[1,:,:,si]
            wdhybGimp.moments[2:,:,:,si] = self.hyb.moments[1,:,:,si]
        wd = self.MatsubaraSum(wdhybGimp, 1) 
        #1/2 Tr[Sigma Gimp]
        pot = IM.E - eg
        self.internal_energy = np.real(dg+eg-wd+pot)

        #print(dg, -wd, IM.E, self.beta)

        if self.num_si == 1 and self.nrel == 1:
            self.internal_energy *= 2

        return self.internal_energy

    def Matrix_x_FreqMatrix(self, A, B):
        f = self.NewFreqDepLocalEnergyMatrix()
        A = Matrix(A)
        for si in range(self.num_si):
            for iomega in range(self.hyb.num_omega):
                Bmat = Matrix(B.M[iomega, :,:, si])
                f.M[iomega, :, :, si] = A * Bmat
        return f

    def FreqMatrix_x_FreqMatrix(self, A, B, hermetian = False):
        f = self.NewFreqDepLocalEnergyMatrix()
        for si in range(self.num_si):
            for iomega in range(A.num_omega):
                Amat = Matrix(A.M[iomega, :, :, si])
                Bmat = Matrix(B.M[iomega, :, :, si])
                if hermetian:
                    Amat = Amat.H
                    Bmat = Bmat.H
                f.M[iomega, :, :, si] = Amat * Bmat 
        return f

    def MatsubaraSum(self, func, npoles):
        iomega = 1j * func.omega

        def mferm(m):
            shape = m.shape
            f = np.array( self.ferm(m.flatten()) )
            return f.reshape(shape)

        sum = 0
        for si in range(self.num_si):

            moment = func.moments[0,:,:,si]
            eps1 = func.moments[1,:,:,si]
            eps2 = func.moments[2,:,:,si]
                
            if npoles == 1:
                tail_contribution = moment * self.beta * mferm(eps1) * (-1+mferm(eps1)) * self.beta 
        
            elif npoles == 2:
                tail_contribution = moment*(mferm(eps1) - mferm(eps2))/(eps1 - eps2) * self.beta 
            
            tail_fit_in_window = np.real(np.array([moment / ((iw - eps1) * (iw - eps2)) for iw in iomega]))

            low_freq = np.real(func.M[:,:,:,si])
            
            sum += 2*np.sum(low_freq, axis=0) + tail_contribution - 2*np.sum(tail_fit_in_window, axis=0)
        
        if self.nrel == 1 and self.num_si == 1:
            sum *= 2

        return np.trace(sum)/self.beta

    def StoreStateInHistory(self):
        pass

    def GetAdditionalDescriptors(self):
        return f"_at_{int(self.temperature)}K"

    #e.g., results are saved in dmft.h5:/impurity_at_300K"
    def GetImpurityLocation(self):

        loc = self.GetTopLevelMethodName()
        index = loc.find(".h5")
        loc = loc[:index] + self.GetAdditionalDescriptors() + loc[index:]

        return loc+f"impurity{self.GetAdditionalDescriptors()}/"
        
    def scale_effort(self, ref_effort, dtype = int):
        if ref_effort > 0:
            return max(1, dtype(ref_effort * self.beta / self.ref_beta ))
        else:
            return ref_effort

    #derivative is a FreqDepLocalEnergyMatrix (or NoneType) which stores the derivate of f_to_interp at each matsubara point 
    #derivative_weights allows one to calculate df_x * weight_x
    def InterpolatedFreqDepLocalEnergyMatrix(self, omega, f_to_interp, kind = 'cubic', derivative = None, 
                                                deriv_om_offset = 0.001, derivative_weights = None):
        f = self.NewFreqDepLocalEnergyMatrix()
        if derivative is not None:
            om_m = self.sig.omega - deriv_om_offset
            om_p = self.sig.omega + deriv_om_offset
            dom = 2*deriv_om_offset
            if derivative_weights is None:
                derivative_weights = self.sig.omega*0 + 1
                
        for si in range(self.num_si):
            for (i,j), element in np.ndenumerate(self.rep):
                if abs(element) >= 1e-14:
                    interp = scinterp.interp1d(omega, f_to_interp[:,i,j,si], kind = kind, fill_value = "extrapolate", bounds_error = False)
                    f.M[:,i,j,si] = interp(self.sig.omega)

                    if derivative is not None:
                        for iomega, om in enumerate(self.sig.omega):
                            d = interp([om_m[iomega], om_p[iomega]])
                            derivative.M[iomega,i,j,si] = (d[1] - d[0])/dom * derivative_weights[iomega]
                        
        return f


def DMFTEnergyOptionsParser(prepArgs, add_help=False, private=False):
        
    parser = ArgumentParserThatStoresArgv('dmft energy', add_help=add_help ,
                            parents=[DMFTOptionsParser(prepArgs, private=private)],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
 
    parser.add_argument("--scale-times", dest="scale_times", 
                     default=False, action="store_true",
                     help="Scale the provided measurement and thermalization times by T_low/T" )

    parser.add_argument("--scale-workers", dest="scale_workers", 
                     default=False, action="store_true",
                     help="Scale the number of workers allocated to each temperature" )

    parser.add_argument("--scaling-factor", dest="temperature_scaling_factor", 
                     default=0.5, type = float,
                     help="Scale the reference temperature up by exp(factor*i) until we reach 2U" )
    
    parser.add_argument("--info", dest="info", 
                     default=False, action = "store_true",
                     help="Output information about the energy runs to be conducted (but don't conduct them)" )

    parser.add_argument("--directory", dest="dir", 
                     default=".",
                     help="Directory to run in" )


  
    return parser

@with_organizer
def DMFT__ENERGY(opts):
    DMFTEnergyMain([], {}, opts = opts, wait = False)


def DMFTEnergyMain(args, kwargs, opts=None, wait = True):
    
    if opts is None:
        parser = DMFTEnergyOptionsParser(args, add_help=True, private=True)    
        opts, _args = parser.parse_known_args(args)
    
    for key in kwargs:
        setattr(opts, key, kwargs[key])

    curdir = os.getcwd()
    if hasattr(opts,"dir"):
        os.chdir(opts.dir)
    
    mpi = MPIContainer(opts)
    _plugin = DFTPlugin.Instance(restart=True, mpi=mpi)
    
    effort = EffortResetter(opts)
    ref = DMFTOrchastrator(opts)

    temps = [1. / ref.beta]
    n=0
    while True:
        n+=1
        T = temps[0] * np.exp(opts.temperature_scaling_factor * n) #in eV
        if T > max(0.25*ref.U, 5) and n>0:
            break
        temps.append( T )
        
    temps = np.array(temps) / KB
    
    if not opts.analysis_only:
        dmft_free_energy = DMFTFreeEnergy()
        dmft_free_energy.num_temperatures = len(temps)
        dmft_free_energy.allocate()

    if opts.info:
        print(INFO_HEADER)
        print(f"- There are {len(temps)-1} CTQMC computations to conduct")
        total_time = 0
        total_workers = 0
        
    orcs = []
    for i, T in enumerate(temps):

        orc = DMFTEnergyOrchestrator(ref, opts, T, is_reference_run = i == 0)

        if opts.info and not orc.is_reference_run:
            print(f"-- Run {i}:")
            print(f"---- T: {orc.temperature}K")
            print(f"---- time: {orc.opts.time_thermalization+orc.opts.time_measurement} min")
            print(f"---- CPUs: {orc.opts.mpi_workers}")

            total_time += orc.opts.time_thermalization+orc.opts.time_measurement
            total_workers += orc.opts.mpi_workers

        elif not opts.analysis_only:
            dmft_free_energy.temperatures[i] = orc.temperature
            orc.dispatch()

        effort.reset(opts)
        
        orcs.append(orc)

    if wait:
        Organizer.Wait()
    
    if opts.info:
        print(f"- Total run-time (fully serial): {total_time}")
        print(f"- Total number of workers (fully parallel): {total_workers}")
        return
    elif not opts.analysis_only:
        dmft_free_energy.ready = True
        dmft_free_energy.store(energyLocation)
        print(INFO_HEADER)
        print("-- Ready to compute internal energies")
    else:
        dmft_free_energy = DMFTFreeEnergy()
        dmft_free_energy.load(energyLocation)
        assert(dmft_free_energy.ready)


    for i, orc in enumerate(orcs):
        dmft_free_energy.internal_energies[i] = orc.ComputeInternalEnergy()

    # find first high temp run with a good ctqmc estimate of S -- we will integrate from there as it's more accurate
    for i, orc in enumerate(orcs):
        try:
            Shigh = orcs[i].ComputeEntropy()
            break
        except:
            pass

    print(f"Integration starting from T = {dmft_free_energy.temperatures[i]}")

    entropy = (Shigh 
            - dmft_free_energy.internal_energies[i]*orcs[i].beta + dmft_free_energy.internal_energies[0]*orcs[0].beta
            + scintegrate.simps(dmft_free_energy.internal_energies[:i+1], x = 1 / dmft_free_energy.temperatures[:i+1] / KB)) #sign change because of ordering


    dmft_free_energy.entropy = entropy
    if dmft_free_energy.entropy < 0: dmft_free_energy.entropy = 0
    dmft_free_energy.result = np.real(dmft_free_energy.internal_energies[0] - 1/ref.beta * dmft_free_energy.entropy)
    
    print(f"DMFT Internal energy = {dmft_free_energy.internal_energies[0]} eV")
    print(f"DMFT entropy = {dmft_free_energy.entropy}")
    print(f"DMFT Free energy = {dmft_free_energy.result} eV")

    dmft_free_energy.store(energyLocation)

    if False:
        import matplotlib
        matplotlib.use("TkAgg")
        import matplotlib.pyplot as plt
        from matplotlib import cm
        fig,ax = plt.subplots(1,1)
        ax.plot(dmft_free_energy.temperatures[:],dmft_free_energy.internal_energies[:])
        plt.show()

        while len(orcs)>2:
            try:
                Shigh = orcs[-1].ComputeEntropy()

                print(len(orcs))
                for i, orc in enumerate(orcs):
                    s2 = (Shigh 
                        - dmft_free_energy.internal_energies[-1]*orcs[-1].beta + dmft_free_energy.internal_energies[i]*orcs[i].beta
                        + scintegrate.simps(dmft_free_energy.internal_energies[i:], x = 1 / dmft_free_energy.temperatures[i:] / KB)) #sign change because of ordering
                    try:
                        s = orc.ComputeEntropy()

                        print(f"T:{dmft_free_energy.temperatures[i]}  S:{s}, S from integration:{s2}")
                    except:
                        print(f"T:{dmft_free_energy.temperatures[i]}  S from integration:{s2}")

                orcs.pop()
                dmft_free_energy.internal_energies = dmft_free_energy.internal_energies[:-1]
                dmft_free_energy.temperatures = dmft_free_energy.temperatures[:-1]
            except:
                break


    os.chdir(curdir)

    if wait:
        Organizer.Terminate()

if __name__ == '__main__':
    DMFTEnergyMain(sys.argv[1:], {})
