#!/usr/bin/env python3

'''
Created on Apr 20, 2022

@author: adler
'''

from argparse import ArgumentDefaultsHelpFormatter
from fractions import Fraction
import sys

from matplotlib.collections import PathCollection

from portobello.bus.Matrix import Matrix
from portobello.bus.mpi import GetMPIProxyOptionsParser
from portobello.rhobusta.loader import AnalysisLoader
from portobello.rhobusta.observables import AddObservables, CoupledBasisObs, GetAllObservables, ProductBasisRSHObs, ProductBasisSHObs, RhobustaMatrixToPatrickMatrix
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
import numpy as np
from numpy.linalg.linalg import norm
from portobello.rhobusta.plotters import BaseBuilderOnIBZ
from tabulate import tabulate
import pickle
        
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("TkAgg")

def Frac(f):
    return Fraction.from_float(float(f.real)).limit_denominator(16) 

class CrystalFieldSplittingPlot:

    def __init__(self, so_split : list, idx : list, ops : Matrix, uM : Matrix, runner, l : int , ax = None, fig = None):

        self.ops = ops
        self.obs = runner.obs
        self.svs = runner.svs
        self.rho = runner.ρ
        self.dim = len(idx)
        self.idx = idx
        self.uM = uM

        self.labels = []
        for i in range(self.dim):
            self.labels.append(";".join([f"{Frac(op[i,i])}" for op in ops]))

        self.fig, self.ax, self.data, self.annotations = self.generate_frame_and_labels(so_split, idx, ops, l, runner.Eloc, runner.ρ, ax, fig)

    def show(self):
        self.fig.show()
        plt.show()

    def place_mb_state(self, conf : int, color):

        if hasattr(self,"state_artists"):
            for artist in self.state_artists:
                artist.remove()

        getbinary = lambda x, n: format(x, 'b').zfill(n)
        binstring = getbinary(conf, self.dim)
        state = np.diag(np.array([int(bit) for bit in binstring][::-1]))
        rho = self.uM.H*Matrix(state)*self.uM
        
        self.state_artists = []
        for i, j in enumerate(self.idx):
            val = rho[j,j]
            if i==0:
                artist = self.ax.scatter(self.data[i][0]-1+i/self.dim, self.data[i][1], s = (10*val)**2, color=color, label = rf"$\langle j | \rho_i | j \rangle$")
            artist = self.ax.scatter(self.data[i][0]-1+i/self.dim, self.data[i][1], s = (10*val)**2, color=color)

            self.state_artists.append(artist)

    def generate_frame_and_labels(self, so_split, idx, ops, l, Eloc, rho, ax, fig):

        if ax is None:
            fig, ax = plt.subplots(1,1)

        x = np.array([0.,1.])
        for i, en in enumerate(so_split):
            y=[en,en]
            ax.plot(x,y,color='black')

            if i==0:
                va = 'bottom'
            else:
                va = 'top'

            if len(so_split)==1:
                label = f"l={l}"
            else:
                label = f"j={Frac(l+(-0.5+i))}"

            ax.annotate(label, (0.5,en), ha='center', va=va )

        deltax = 1.2
        xc = [x[1],x[0]+deltax]
        x += deltax

        energies = np.diag(Eloc.real)
        annotation_range = np.max(energies) - np.min(energies)
        annotations_at = []
        data_at = []
        annotation_tol = 0.05

        for j, i in enumerate(idx):
            closest_so = so_split[np.argmin(np.abs(so_split - Eloc[i,i]))]
            yc = [closest_so, Eloc[i,i].real]
            ax.plot(xc,yc,color='black', linestyle=":")

            val = Eloc[i,i].real
            y=[val, val]

            ax.plot(x,y,color='black')

            if j==0:
                ax.scatter(x[0]+(x[1]-x[0])*j/len(idx),val, s= (20*rho[i,i].real)**2, color="gray", marker='s', label = r"$\rho$")
            else:
                ax.scatter(x[0]+(x[1]-x[0])*j/len(idx),val, s= (20*rho[i,i].real)**2, color="gray", marker='s')
            
            xy = (x[1],y[0])
            if len(annotations_at) == 0 or np.abs(annotations_at[-1][1] - val)/annotation_range > 2*annotation_tol:
                annotations_at.append(xy)
            else:
                last = np.copy(annotations_at[-1])
                annotations_at[-1] = (last[0]+annotation_tol*3*deltax, last[1]-annotation_tol*annotation_range)
                annotations_at.append((last[0]+annotation_tol*3*deltax, last[1]+annotation_tol*annotation_range))
                
            data_at.append(xy)

        for i, j in enumerate(idx):
            label = "|"+ ";".join([f"{Frac(op[j,j])}" for op in ops]) +">"
            if data_at[i] == annotations_at[i]:
                ax.annotate(label, data_at[i], xytext=annotations_at[i] )
            else:

                ax.annotate(label, data_at[i], xytext=annotations_at[i],
                    arrowprops=dict(arrowstyle="->",
                                connectionstyle="arc3")
                                )
            
        ax.set_xlim(0,x[-1]+deltax)
        ax.set_xticks([])
        ax.set_xticklabels([])
        ylim = ax.get_ylim()
        ax.set_ylim((ylim[0]-annotation_tol*annotation_range,ylim[1]))
        
        ax.set(xlabel="", ylabel="Energy (eV)")

        return fig, ax, data_at, annotations_at
        
# calculate the projected Hqp on the local atom, so we could look at QP crystal field splittings
class LocalQPHBUilder(BaseBuilderOnIBZ):
    def __init__(self,svs,opts):
        opts.broadening=0.05 # not used here
        opts.num_high_energy_bands = 0
        opts.num_low_energy_bands = 0
        BaseBuilderOnIBZ.__init__(self, svs,opts,"")
        self.Hqp_loc = np.zeros(shape=(self.svs.dim, self.svs.dim), dtype=np.complex)
    
    def VisitKPoint(self, k, si, Hqp,
            lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
            weight, num_atoms):
        Pk2 = self.proj.OneAtomProjector(k, si)
        self.Hqp_loc  +=  Pk2.H * Hqp * Pk2 * weight

    def End(self):
        MPI = self.svs.plugin.mpi.GetMPIInterface()
        if self.proj.GetBandStructureInWindow().is_sharded:
            self.Hqp_loc= MPI.COMM_WORLD.allreduce(self.Hqp_loc, op=MPI.SUM)
            MPI.COMM_WORLD.Barrier()

class CFRunner(AnalysisLoader):
    def __init__(self, opts, ax = None, fig = None):
        AnalysisLoader.__init__(self,opts)
        self.ax = ax
        self.fig = fig


    def Splitting(self, symm_rep):
        rep = np.array(symm_rep) 
        rmax = np.amax(rep)
        split = np.zeros((rmax,), dtype=np.float64)
        weight = np.zeros((rmax,), dtype=np.float64)
        for i in range(rmax):
            for j in range(len(rep)):
                if rep[j] == i+1:
                    split[i] += self.Eloc[j,j]*self.ρ[j,j]
                    weight[i] += self.ρ[j,j]
        for i in range(rmax):
            split[i] /= weight[i]
        return split

    def GetSplitting(self, split2, split1, rep2, rep1):
        r1 = 0
        r2 = 0
        vec = []
        for i in range(len(rep1)):
            if rep1[i] != r1 or rep2[i] != r2:
                r1 = rep1[i]
                r2 = rep2[i]
                vec.append(split2[r2-1] - split1[r1-1])
        return vec

    def MPIWorker(self):
        self.LoadCalculation(loadRealSigma=self.opts.from_real_axis)
        self.svs.InitOneBody()

        self.qn = ""
        if self.svs.nrel > 1:
            self.qn = "j,Jm"
        else:
            self.qn = "Lm,Sm"
        if self.opts.qn != "":
            self.qn = self.opts.qn

        lqpH = None
        if not self.opts.eimp_not_eqp:
            lqpH = LocalQPHBUilder(self.svs, self.opts)
            self.svs.VisitBandStructure(lqpH)

        if self.IsMaster():
            self.PrintCrystalFields(lqpH)

    def PrintCrystalFields(self, lqpH):
        shell = self.svs.corrSubshell
        
        self.obs = GetAllObservables(shell)
        self.opts.Eimp = True # add Eimp
        AddObservables(self.obs, self.svs, self.opts)

        qn = self.qn.split(",")
        uM = self.obs.CoDiagonalize(self.qn)
        self.obs.Transform(uM) # now obs is in the basis has the quantum numbers

        # get the transfomed ops
        ops = [ self.obs.evaluate(op) for op in qn] 
        #for i, op in enumerate(qn):
        #    print(op)
        #    print(Matrix(ops[i]))
        self.ρ = self.obs.Get("ρ")
        l = shell.l

        if self.opts.eimp_not_eqp:
            self.Eloc = self.obs.Get("Eimp")
            if self.isDMFT and self.opts.from_real_axis:
                self.Eloc += RhobustaMatrixToPatrickMatrix(self.svs.sig.M[np.argmax(self.svs.sig.omega > -1e-10),:,:,:].real, self.svs)
        else:
            Hqloc = lqpH.Hqp_loc
            if self.svs.num_si == 2 and self.svs.nrel < 2:
                Hqloc = Matrix(np.kron(np.eye(2), Hqloc))
            self.Eloc = uM.H * Hqloc * uM
            
        so_split=[0]
        if len(qn) == 2 and qn[0] in ["j","J2"] and qn[1] in ["Jx","Jy","Jz","Jm"]:
            nonRelSpherSymRep = [1]*(4*l+2)
            spherSymRep = [1]*(2*l) + [2]*(2*l+2)
            magSymRep   = [1]*l + [2]*l + [3]*(l+1) + [4]*(l+1)
            fullRep = [i+1 for i in range(4*l+2)]

            nonRelSpher = self.Splitting(nonRelSpherSymRep)
            so = self.Splitting(spherSymRep)
            mag = self.Splitting(magSymRep)

            # average energy levels in low, high
            so_split = self.GetSplitting(so, nonRelSpher, spherSymRep, nonRelSpherSymRep)
            print(f"Spin-orbit splitting: {so_split[1]-so_split[0]:3.3f} eV")
            print(f"    between {so_split[0]*1000:3f} meV, {so_split[1]*1000:3f} meV")

            # magnetic splitting:
            # self.GetSplitting(mag, so, magSymRep, spherSymRep)

            #print("Crystal field splitting:")
            #diag = np.array([self.Eloc[i,i] for i in range(4*l+2)])
            #self.GetSplitting(diag, mag, fullRep, magSymRep)


        idx = np.ndarray((self.Eloc.shape[0]), np.int16)
        idx[:] = np.diag(self.Eloc).argsort()[:]
        E0 = self.Eloc[idx[0],idx[0]].real


        print("Eloc in transformed basis (with sorted diagonal) [meV]")
        for i in idx:
            self.Eloc[i,i] -= E0
        ElocPermuted = Matrix(self.Eloc[:,idx][idx,:])
        print(ElocPermuted)


        table = []
        Z = None
        if self.obs.Has("Z"):
            Z = self.obs.Get("Z")
        for i in idx:
            line = [f"{self.Eloc[i,i].real:3.1f}", 
                    "|"+ ";".join([f"{Frac(op[i,i])}" for op in ops]) +">",
                    f"{self.ρ[i,i].real:1.3f}"]
            if Z is not None:
                line.append(f"{Z[i,i].real:1.3f}")
            table.append(line)
        headers = ["Energy [meV]", ";".join(qn), "ρ"]
        if Z is not None:
            headers.append("Z")
        tt = tabulate(table, headers=headers)
        print(tt)
        

        print("Diagonalized density")
        # u * diag *u.H = rho
        ρ_diag, u = self.svs.DiagonalizeHamiltonianSort(self.ρ, complex=False)
        uu = Matrix(u)
        #print(f"diagonal ro: {ρ_diag}")
        E_diag = [ -np.log(rr)/self.svs.beta for rr in reversed(ρ_diag)]
        print(f"beta={self.svs.beta}")
        print(E_diag)
        t2 = [f"{1000.0*(E - E_diag[0]) :4.1f}" for E in E_diag]
        print(f"F=-log(ρ) [meV]:", ", ".join(t2))
        print("Eimp In diagonalized ρ basis:")
        EE = self.obs.Get("Eimp") # in the given QN
        EEnd = EE-EE.DiagonalAsMatrix()
        print(f"diagonal:", np.diag(np.real(EE)))
        odm = np.max(np.abs(EEnd))
        badness2 = 0.0
        for i in idx:
            for j in idx:
                if i != j:
                    if abs(self.Eloc[i,j]) < 1.0e-6: continue
                    badness2 = max(badness2, abs(self.Eloc[i,j])/max(abs(self.Eloc[i,i]), abs(self.Eloc[j,j])))
        print(f"offdiagonal max is {odm}, baddness in {self.opts.qn}: {odm/np.max(np.abs(EE))*100:3.3}%  badness2: {badness2*100.0:3.3}%")
        #printMatrix(np.abs(EEnd))
        

        print("Diagonalized Eimp")
        Ed, uE = self.svs.DiagonalizeHamiltonianSort(ElocPermuted, complex=True)
        print(",".join([f"{Ed[ii]-Ed[0] : 3.1f}" for ii in range(Ed.shape[0])]))
        print(Matrix(uE))

        #self.obs.Transform(uE)
        #PrintMatrix(self.obs.Get("Jx")) # problem -this is not permuted

        import code
        code.interact(local=locals())


        # now print the local matrix, diagonalized by the same U that diagonalizes rho

        if self.opts.plot:
            self.plot = CrystalFieldSplittingPlot(so_split,idx,ops, uM, self,shell.l, self.ax, self.fig)
            



if __name__ == '__main__':
    parser = ArgumentParserThatStoresArgv('crystal-field', add_help=True,
                            parents=[GetMPIProxyOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--which-shell", dest="which_shell", default=0, type=int,
                    help="which shell (index) to project to, in order to build the correlated problem")

    parser.add_argument("-Q", "--quantum-numbers", dest="qn", default="",
                      help="Quantum numbers to use. By default, 'j,Jm' is used on rel., 'Lm,Sm' in non-rel, but any observables can be used ")
                      
    parser.add_argument("--eimp",
                        dest="eimp_not_eqp",
                        default=False,
                        action="store_true",
                        help = "use Eimp, not Eqp_loc to calculate the splitting")

    parser.add_argument("--mb-index", dest="mb_index", default=0, type=int,
                    help="When plotting: Index in the sorted list of many body states (sorted from most to least probable)")

    parser.add_argument("--from-real-axis", dest="from_real_axis",
                    action="store_true",
                    default=False,
                    help = "When constructing a quasiparticle hamiltonian, use Z's computed from the derivative of the real-axis self energy (dmft)")

    # for the loaded real self-energy
    parser.add_argument("-W", "--window",
                    dest="omega_window",
                    default=10.0,
                    type=float,
                    help = "omega energy window (-w to w) in eV") 
     
    parser.add_argument(
                    "--nw",
                    dest="Nw",
                    default=2, # need just the middle w=0 points actually
                    type=int,
                    help = "number of frequency points") 

    parser.add_argument("--plot",
                        dest="plot",
                        default=False,
                        action="store_true",
                        help = "Plot splittings")

    opts, _args = parser.parse_known_args(sys.argv[1:])

    runner = CFRunner(opts)
    runner.Run(wait=True)
    
    if opts.plot:
        ii = list(reversed(np.argsort(runner.svs.hist.p)))
        runner.plot.place_mb_state(ii[opts.mb_index],color='orange')
        runner.plot.ax.legend(loc="upper left")
        runner.plot.show()
