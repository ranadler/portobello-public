#!/usr/bin/python3


'''
Created on Sep 15, 2020

@author: melnick
'''

import numpy as np

class DiscontinuitySegment:
    
    def __init__(self, start, end, err_start, err_end):
    
        self.start = start
        self.end = end + 1
        
        self.m = (err_end - err_start) / (end - start)
        self.b = err_start

def discontinuities(errors, epsk, criterion):
    nk = len(errors)
    
    p, pairs=[], []
    for k in range(1,nk-1):
        
        if abs(errors[k]) > criterion and abs(errors[k]) > abs(errors[k-1]) and abs(errors[k]) > abs(errors[k+1]):
            p.append(k)
        
        if len(p)==2:
            
            expected = [0,0]
            offset = [0,0]
            
            ik = p[0]
            expected[0] = 2 * epsk[ik - 1] - epsk[ik - 2]
            offset[0] = epsk[ik] - expected[0]
            
            ik = p[1] - 1
            expected[1] = 2 * epsk[ik + 1] - epsk[ik + 2]
            offset[1] = epsk[ik] - expected[1]
        
            pairs.append(DiscontinuitySegment(p[0], p[1]-1, offset[0], offset[1]))
            p = []
            
    print(f"There are {len(pairs)} pairs with {len(p)} remaining discontinuities")
    
    return pairs


def add_test_discontinuties(epsk, ndisc, shift):
    
    nk = len(epsk)
    
    ndisc = ndisc + 1 if ndisc % 2 else ndisc
    
    loc_disc = np.linspace( nk/ndisc, nk * (1 - 1./ndisc), ndisc)
    loc_disc = [int(x) for x in loc_disc]
    
    pairs=[]
    for i in range(0, ndisc, 2):
        r = np.random.rand(2)
        
        errs = [(shift if r[i] > 0.5 else - shift) for i in range(2)]
        
        pairs.append(DiscontinuitySegment(loc_disc[i], loc_disc[i+1], errs[0], errs[1]))
        
    for pair in pairs:
        for k in range(pair.start, pair.end):
            epsk[k] += pair.m * (k - pair.start) + pair.b
        
    return epsk

def correct_band(epsk, criterion):
    epsk_corrected = np.copy(epsk)
    #epsk is a single band from full set of eps_ik
  
    nk = len(epsk)
    differences = np.zeros(nk)
    for k in range(1,nk):
        differences[k] = epsk[k] - epsk[k-1]

    errors = np.zeros(nk)
    for k  in range(1,nk-1):
        errors[k] =  abs(2*differences[k] - differences[k-1] - differences[k+1])
    
    pairs = discontinuities(errors, epsk, criterion)
        
    for pair in pairs:
        for k in range(pair.start, pair.end):
            epsk_corrected[k] -= pair.m * (k - pair.start) + pair.b
    
    return epsk_corrected


