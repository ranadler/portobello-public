#!/usr/bin/python3

'''
Created on Dec 28, 2019

@author: adler
'''

from portobello.bus.mpi import GetMPIProxyOptionsParser
import sys
import os
import importlib

from portobello.rhobusta.options import ArgumentParserThatStoresArgv
      
if __name__ == '__main__':  
    
    parser = ArgumentParserThatStoresArgv(parents=[GetMPIProxyOptionsParser()])    
    
    parser.add_argument("-x", "--function", dest="function_name",
                      help="The server path for a script to execute",
                      default="")

    
    (opts, args) = parser.parse_known_args(sys.argv[1:])
    
    comps = opts.function_name.split(".")
    
    mod_name, func_name = ".".join(comps[:-1]), comps[-1]
    
    mod = importlib.import_module(mod_name)
    method = getattr(mod, func_name)
    method()
    