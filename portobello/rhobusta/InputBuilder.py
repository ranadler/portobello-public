'''
Created on Jun 6, 2020

@author: adler

'''
import json
from portobello.generated.FlapwMBPT import Input
import numpy as np
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from portobello.symmetry.magmom import GroupAnalyzerWithMagmom
from matdelab.dmft.w2k_struct import W2KStruct
from pymatgen.core.structure import Structure
import os
import operator
from portobello.symmetry.general import PrintMatrix
from numpy.linalg.linalg import norm
from pymatgen.core.sites import PeriodicSite
from cmath import pi
from math import ceil
import math
from pymatgen.core.periodic_table import Element
from portobello.symmetry.spacegroup import BuildSpaceGroupGens

class OO():
    def __init__(self, atocc, ptnl, corr, idmd):
        self.atocc = float(atocc)
        self.ptnl = ptnl
        self.corr = corr
        self.idmd = idmd

    def Fill(self, ad, l, isApw):
        i = ad.ntle[l]
        ad.atoc[i,l] = self.atocc
        ad.ptnl[i,l] = self.ptnl
        ad.correlated[i,l] = self.corr
        ad.idmd[i,l] = self.idmd
        ad.augm[i,l] = isApw

        ad.lim_pb_mt[l]     += 1
        ad.ntle[l]          += 1

        aorl = 'LOC'
        if isApw: aorl = 'APW'
        corr = 'N'
        if self.corr: corr='L'

        print(f" {l}   {aorl}  {float(self.atocc):4.3}  {self.ptnl:4.3}     {corr}    {self.idmd}")

#
# Angstrom to Bohr conversion factor
#
#    "CODATA recommended values of the fundamental physical constants: 2014", 
#    P.J. Mohr, D. B. Newell, B. N. Taylor,
#    the Committee on Data for Science and Technology (CODATA),
#    August, 2015, <http://dx.doi.org/10.5281/zenodo.22826>
#
# See also:
#
#    "Fundamental Physical Constants", NIST
#    (<http://physics.nist.gov/cgi-bin/cuu/Value?bohrrada0>)
#
angstrom2bohr = 1.8897261254535

"""
    From Kutepov's documentation:
        * =1 for simple cubic;
        * =2 for body-centered cubic;
        * =3 for face-centered cubic;
        
        * =4 for hexagonal;
        
        * =5 for simple tetragonal;
        * =6 for body-centered tetragonal (C<A);
        * =7 for body-centered tetragonal (C>A);
        
        * =8 for simple orthorhombic;
        * =9 for body-centered orthorhombic;
        * =10 for face-centered orthorhombic;
        * =11 for base-centered orthorhombic;
        
        * =12 for simple monoclinic.
        * =13 for base-centered monoclinic;
        
        * =14 for rombohedral;
        
        * =15 for triclinic;
        
        * =16 for hexagonal two-dimensional;
"""
def GetIstruc(st, sga):
  
    #   pymatgen get_crystal_system:
    #     {"triclinic": (1, 2), "monoclinic": (3, 15),
    #      "orthorhombic": (16, 74), "tetragonal": (75, 142),
    #      "trigonal": (143, 167), "hexagonal": (168, 194),
    #      "cubic": (195, 230)}
    #
    #   pymatgen get_lattice_type:
    #
    #    if n in [146, 148, 155, 160, 161, 166, 167]:
    #        return "rhombohedral"
    #    elif crystal-system == "trigonal":
    #        return "hexagonal"
    #    else:
    #        return crystal-system



    lattice_system    = sga.get_lattice_type()
    hall       = sga.get_hall()
    print("- space group: %s, lattice: %s, Hall: %s" % (sga.get_space_group_number(),
                                                      lattice_system, hall))  
    
    if lattice_system == 'cubic':
        if "F" in hall:
            return 3
        elif "I" in hall:
            return 2
        return 1
    elif lattice_system == 'hexagonal':
        return 4 # or 16
    elif lattice_system == 'tetragonal':
        if 'I' in hall:
            if st.lattice.a > st.lattice.c:
                return 6 # - this doesn't seem to work well
            else:
                return 7 #- this doesn't seem to work well
        return 5
    elif lattice_system == 'orthorhombic':
        if 'I' in hall:
            return 9
        elif 'F' in hall:
            return 10
        elif 'C' in hall:  
            return 11
        elif 'A' in hall or 'B' in hall:
            # raise Exception("we are not prepared for this - should we fall back to 8 or rotate?")
            # TODO: is our crystal standardized? Does can it be centered on A or B faces instead?
            return 8
        return 8
    elif lattice_system == 'monoclinic':
        if 'C' in hall:
            return 13
        elif 'A' in hall or 'B' in hall:
            raise Exception("we are not prepared for this - should we fall back to 12 or rotate?")
            # TODO: is our crystal standardized? Does can it be centered on A or B faces instead?
        return 12 
    elif lattice_system == 'rhombohedral':
        return 14
    else: # crystal_system == 'triclinic':
        return 15

class AtomDisplacement:
    def __init__(self, opts):
        self.atom_to_displace = opts.atom_to_displace
        self.displacement = opts.atom_displacement * np.array(opts.direction_to_displace.split(","), dtype=float)

def GetStructure(filename):
    if filename.lower().endswith(".struct"):
        st = W2KStruct.from_file(filename)
    elif filename.upper().endswith("POSCAR") or filename.upper().startswith("POSCAR"):
        st = Structure.from_str(open(filename).read(), primitive=False,fmt='poscar')
    else:
        st = Structure.from_file(filename, primitive=False)      
    return st

def GetRefinedStructureWithSGAAndOps(input_file, opts, volume_scale=1, lattice_scalings="", atomDisplacement : AtomDisplacement = None):
    struct_to_refine = GetStructure(input_file)  
    if abs(volume_scale-1.0) > 1.0e-10:
        struct_to_refine.scale_lattice(volume_scale**3 * struct_to_refine.volume)

    if lattice_scalings!="":
        scalings = np.array([float(s) for s in lattice_scalings.split(",")])
        lattice = struct_to_refine.lattice.matrix * scalings
        struct_to_refine = Structure(lattice, struct_to_refine.species, struct_to_refine.frac_coords)

    """
    Find atom with given name and all symmetry equivalent atoms
    Displace each of them symmetrically
    """
    if atomDisplacement is not None:
        sga = SpacegroupAnalyzer(struct_to_refine, symprec=0.001, angle_tolerance=opts.spacegroup_angle_tolerance)

        if not opts.use_given_cell:
            struct_to_refine = sga.get_primitive_standard_structure() # the standard primitive .get_primitive_standard_structure() has issues with SG 70 
            sga = SpacegroupAnalyzer(struct_to_refine, symprec=0.001, angle_tolerance=opts.spacegroup_angle_tolerance)

        ops = sga.get_symmetry_operations(cartesian=True)
        struct_to_refine = sga.get_symmetrized_structure()

        equiv_sites = None
        other_sites = []
        for site in struct_to_refine.sites:
            if site.species_string == atomDisplacement.atom_to_displace:
                if not opts.dft_no_symm:
                    equiv_sites = struct_to_refine.find_equivalent_sites(site)
                else:
                    equiv_sites = [site] # no symmetry
            else:
                other_sites.append(site)
        assert equiv_sites is not None, "Could not find requested atom to displace"
        
        #attach a symmetry op to each atom in the list of equivalent sites
        for i, site in enumerate(equiv_sites):
            equiv_sites[i].found = False
            for op in ops:
                coords = op.operate(site.coords)
                for j in range(3):
                    r = struct_to_refine.lattice.matrix[j,:]
                    if np.allclose(r+coords, equiv_sites[0].coords, rtol=1e-4, atol=1e-3):
                        equiv_sites[i].op = op
                        equiv_sites[i].found = True
                        break
        coords = []
        species = []
        for i, site in enumerate(equiv_sites):
            assert site.found, f"did not find symmetry operation for site: {site}"
            coords.append(site.coords + site.op.operate(atomDisplacement.displacement))

            species.append(site.species)

        for site in other_sites:
            coords.append(site.coords)
            species.append(site.species)

        struct_to_refine = Structure(struct_to_refine.lattice, 
                    species, coords, coords_are_cartesian=True)

    structHasMagmom = False
    for site in struct_to_refine.sites:
        if 'magmom' in site.properties and site.properties['magmom'] != 0.0:
            structHasMagmom = True
            break     

    extra = None

    # if it doesn't have magnetic moment, we just proceed to take the primitive cell directly
    # and extract the symmetry group.
    if not structHasMagmom:
        print("- structure does not contain magnetic information")
        # standardize it, and get a primitive structure
        sga = SpacegroupAnalyzer(struct_to_refine, symprec=0.001, angle_tolerance=opts.spacegroup_angle_tolerance)
        
        if not opts.use_given_cell:
            struct_to_refine = sga.get_primitive_standard_structure() # the standard primitive .get_primitive_standard_structure() has issues with SG 70 
            sga = SpacegroupAnalyzer(struct_to_refine, symprec=0.001, angle_tolerance=opts.spacegroup_angle_tolerance)

        ops = sga.get_symmetry_operations(cartesian=True)
        struct_to_refine = sga.get_symmetrized_structure()
        
        #print struct_to_refine .lattice
    else:
        # here we proceed more carfully, because pymatgen ignores magmom when analyzing symmetry.
        # also we need to identify the symmetry between AFM sites if there are such.
        print("- structure has magnetic information")
        # we replace all magnetic atoms with substitutes, and then undo it.
        print(struct_to_refine)
        if not opts.spin_polarized:
            if not opts.fully_relativistic:
                opts.spin_polarized = True
                print("** Computation has to be spin-polarized for structures with magnetic order **")
                print("   added an effective -S to the command line")
            
        msga = GroupAnalyzerWithMagmom(struct_to_refine, angle_tolerance=opts.spacegroup_angle_tolerance)
        struct_to_refine = msga.get_primitive_standard_structure()
        ops = msga.get_symmetry_operations()     
        sga = msga.get_sga()   
        if opts.run:
            extra = msga.GetAFMSymmetry()

    print(struct_to_refine)
    return struct_to_refine, sga, ops, extra
        
    # TODO: remove this experiment
    #sga = SpacegroupAnalyzer(st, symprec=0.001, angle_tolerance=5)
    #if sga.get_crystal_system() == 'trigonal' and sga.get_space_group_number() == 166 or True:
    #    print "***** improving triagonal unit cell to select natural x,y ******"
    #    
    #    a = st.lattice.a / sqrt(6)
    #    st = Structure([[2*a,a,a],[a,2*a,a],[a,a,2*a]], st.species,
    #                    [s.frac_coords for s in st.sites],
    #                    site_properties=st.site_properties,
    #                    coords_are_cartesian=False)                
    

def RKRadius1(z):
    #print "RKRadius:", z
    if z == 1:
        return 3.0 # H
    if z == 3:
        return 4.5 # Li
    if z in (4,5,14):
        return 5.0 # Be, B, Si
    if z in (6,15):
        return 5.5 # C,P
    if z in (7,16):
        return 6.0 # N, S
    if z in (8,17,11,12,13,19,20,37,38,55,56):
        return 6.5 # O, Cl, Na,K,Rb,Cs, Mg, Ca, Sr, Ba, Al
    if z == 9:
        return 7.0 # F
    if z in range(21,25) or z in range(39,43) or z in range(31,36):
        return 7.5 # Sc-Cr, Ga-Br, Y-Mo
    if z in range(25,31) or z in range(44,49) or z in range(72,76) or z in (57,58) or z in range(49,54):
        return 8.0 # Mn-Zn, Ru-Cd, In-I, La, Ce, Hf-Re
    if z in range(76,86) or z in range(59,72) or z in range(87,104):
        return 8.5 # Os-At, Pr-Lu, Ac-Lr
    raise Exception("element not in list of binding elements,", z)

class DFTInputBuilder(Input):

    def OptimizeRadii(self, RKRadii, distmat, opts, addWeight=True): 
        from quadprog import solve_qp
        ns = self.strct.nsort
        G = np.eye(ns+1,ns+1) * 2.0 
        #G[ns,ns] = 0.0
        a= np.zeros((ns+1,), np.float64)

        a[:ns] = 2*np.array(RKRadii)
        
        if addWeight:
            # put a weight of 4 in front of the quadratic expressions of correlated atoms, so that their closeness to the
            # optimal l (>=8) is more important
            fac_corr = 2.0
            f1 = [ (int(a[i]>=16)*fac_corr+1)  for i in range(ns+1)]
            G= np.diag(f1) * 2.0
            a = a*f1
                
        ns2 = ns*ns+ns  
        C = np.zeros((ns2,ns+1), np.float64) 
        h = np.zeros((ns2,), np.float64)
         
        ij = 0 
        for i in range(ns): 
            for j in range(ns): 
                C[ij,j] -= 1.0 
                C[ij,i] -= 1.0 
                C[ij, ns]  = distmat[i,j]
                ij += 1 
            C[ns*ns+i,i] = 1.0 
            
        x = solve_qp(G, a, C.T, h.T, 0)[0]
        x[:ns] /= x[ns]
        return x

    def __init__(self, input_file, opts, **kwargs):
        Input.__init__(self, **kwargs)   # note: this sets named fields

        ad = AtomDisplacement(opts) if opts.atom_to_displace != "" else None
        struct, self.sga, self.ops, self.extra = GetRefinedStructureWithSGAAndOps(input_file, opts, opts.a, opts.lattice_scaling, ad)

        if not opts.run:
            print(f"- number of atoms in (primitive) cell: {len(struct.sites)}")
            
        self.mag.allocate()
        self.mag.b_ext[:] = [0.0,0.0,1.0]
        
        self.num_si = 1 # transient
        self.irel = 1   # non-relativistic
        
        if opts.fully_relativistic:
            self.mag.b_ext[:] = 0.0
            self.num_si = 1
            self.ctrl.irel = 2
            self.ctrl.rel_interst = opts.interst_rel
        elif opts.spin_polarized:
            self.mag.iter_h_ext = 10000
            self.num_si = 2
            
        if opts.B_spec != "":
            v = opts.B_spec.split(",")
            if len(v) == 1:
                self.mag.b_extval = float(v[0])
            else:
                self.mag.b_ext = [float(vi) for vi in v]
                self.mag.b_extval = 1.0

                        
        if not opts.dft_no_symm:
            equivalent_indices = struct.equivalent_indices
        else:
            equivalent_indices = [[si] for si in range(len(struct.sites))] # no equivalences
        equivalent_sites = [[struct.sites[i] for i in eqC] for eqC in equivalent_indices]
        print("- equivalent sites in final cell (considering spin pol. as well):", equivalent_indices)
        
        #TODO: throw exception if too big 
        # this is really not the space group generators, it is the entire space group. 
        # it used to be the generators, but since Andrey code has issues with generators, we just generate everything in Python
        self.sgg = BuildSpaceGroupGens(self.ops, struct, opts)
        print(f"- #symmetry ops (in final cell): {len(self.ops)}, #generators: {self.sgg.num_generators}")

        #ShowCrystal(struct)
        
        self.text = "DFT setup for FLAPWMBPT, original cif file is at: %s"% os.path.abspath(input_file)
        self.allfile = "_" + "".join(struct.composition.formula.split(" "))
        print("- Generating hdf5 input for", self.allfile)         
        # set general parameters for DFT
        
        
        self.ctrl.temperature = opts.Temperature
        self.ctrl.admix = opts.admix
        
        if opts.dft_functional == "gga":
            self.ctrl.iexch = 205 # GGA  # the default w2k
        else:
            self.ctrl.iexch = 5 #for LDA 
            
        self.ctrl.iter_gw = 0
        self.ctrl.iter_dft = 80
        
        self.ctrl.restart_end = 0
        self.ctrl.restart_begin = 0
                
        # Set the structure 
        self.strct.istruc = -GetIstruc(struct, self.sga) # see afteread.F -> being <0 means fractional coords
        self.strct.nsort = len(equivalent_indices)
        self.strct.natom = len(struct.sites)
        
        self.strct.par = 1.0   # we write all coords in angstrom
        self.strct.b_a = 1.0
        self.strct.c_a = 1.0
        self.strct.allocate()
        self.strct.a[:] = struct.lattice.matrix[0,:] * angstrom2bohr
        self.strct.b[:] = struct.lattice.matrix[1,:] * angstrom2bohr
        self.strct.c[:] = struct.lattice.matrix[2,:] * angstrom2bohr
        iatom = 0 
        species = []
        for isort in range(self.strct.nsort):
            for site in equivalent_sites[isort]:
                species.append(site.specie)
                self.strct.tau[:,iatom] = struct.lattice.get_fractional_coords(site.coords)
                self.strct.isA[iatom] = isort+1
                iatom +=1
                      
        print("----- NOTE: PyMatGen lengths below are in Bohr---------------")
        print(struct)
        print("----- Lattice:")
        print(f"a={self.strct.a[:]}")
        print(f"b={self.strct.b[:]}")
        print(f"c={self.strct.c[:]}")
        print("------------------------- full fractional atomic positions-----------------")
        for iatom in range(self.strct.natom):
            print(f"{species[iatom]} {self.strct.tau[:,iatom]}")
        print("----------------------------------------------------------------------------")
    
        # set meshes, zones, kgrid, magnetism
        
        self.dos.emindos = -1.5  # in Ry
        self.dos.emaxdos = 1.5   # in Ry
        self.dos.ndos = 1000
        
        self.zns.allocate()
        self.zns.nbndf = 0  # all bands are used in GF
        self.zns.nbndf_bnd[:] = [0,0]  # 0 bands around the FE corrected by vertex band correction
        
        self.bp.n_k_div = 100
                  
        # find distance matrix between species
        distmat = np.zeros((self.strct.nsort, self.strct.nsort), np.float64)
        distmat[:,:] = np.Infinity

        # A reference structure can be built so that the MT Radii are identical to some baseline volume
        # as required in, e.g., energy-volume curves
        if abs(opts.ref_a-1.0) > 1e-10 or opts.ref_lattice_scaling!="" or ad is not None:
            ref_struct, sga_, ops_, extra_ = GetRefinedStructureWithSGAAndOps(input_file, opts, opts.ref_a, opts.ref_lattice_scaling)
        else:
            ref_struct = struct
            
        ref_equivalent_sites = [[ref_struct.sites[i] for i in eqC] for eqC in equivalent_indices]

        for i in range(self.strct.nsort):
            sitei = ref_equivalent_sites[i][0]
            siteiP = PeriodicSite(sitei.species, sitei.coords, ref_struct.lattice, coords_are_cartesian=True)
            for j in range(i,self.strct.nsort):
                if i == j:
                    # the closest non-identical atom of this sort is either a lattice image, or equivalent
                    for eq in ref_equivalent_sites[i][1:]:  # don't include the site itself
                        d = eq.distance(siteiP)
                        distmat[i,i] = min(distmat[i,i], d)
                    distmat[i,i] = min(distmat[i,i],ref_struct.lattice.a)
                    distmat[i,i] = min(distmat[i,i],ref_struct.lattice.b)
                    distmat[i,i] = min(distmat[i,i],ref_struct.lattice.c)
                             
                if i < j:
                    for eq in ref_equivalent_sites[j]:
                        sitejP = PeriodicSite(eq.species, eq.coords, ref_struct.lattice, coords_are_cartesian=True)
                        d, _ = siteiP.distance_and_image(sitejP)
                        distmat[i,j] = min(d, distmat[i,j])
                        distmat[j,i] = distmat[i,j] 
                        
        print("distance matrix (Å):") 
        PrintMatrix(distmat, 4, 400)
                
        distmat *= angstrom2bohr
        Kmax = 0
        
        RKRadii = [RKRadius1(es[0].specie.Z)  for es in ref_equivalent_sites]
        maxRK = max(RKRadii)

        if opts.goalRK is None:
            opts.goalRK = maxRK # this means no reduction in RK
                
        def RKRadius(z):
            return RKRadius1(z) * opts.goalRK * 1.0 / maxRK
        
        if opts.mtRadii == 'optimized':

            sol = self.OptimizeRadii([RKRadius(es[0].specie.Z)  for es in ref_equivalent_sites], distmat, opts)
            
            print("finished radius optimization")
            
            if opts.reduction_per_atom != "":
                reductions = [float(r) for r in opts.reduction_per_atom.split(",")]
                for isort in range(self.strct.nsort):
                    sol[isort] *= reductions[isort]
                    
            for isort in range(self.strct.nsort):
                sol[isort] *= opts.reduction

            #print "RADIUS optimal solution:", sol
            # note this solution also determines the optimal Kmax (as well as the optimal radii):
            #Kmax = sol[-1]
            
            Kmax = max([RKRadius(site.specie.Z) / sol[isort]  for i in range(self.strct.nsort)])
            
            for isort in range(self.strct.nsort):
                ad = self.strct.ad[isort]
                ad.smt = sol[isort]
                site = ref_equivalent_sites[isort][0]  
                el = site.specie
                
                print("   optimal MT radius of %s: %fÅ for RK: %3.3f [goal %f]" % (str(el), ad.smt/angstrom2bohr, ad.smt*Kmax, RKRadius(el.Z)))

        else:
            # equal radii
            
            RK = opts.goalRK
            
            # Set the orbital info per distinct atom
            for isort in range(self.strct.nsort):
                ad = self.strct.ad[isort]
                site = ref_equivalent_sites[isort][0]    
                # Huristic for radius: should be <= 0.85 of distance ratio with all other atoms (weighed
                # by its radius)
                el = site.specie
                ad.smt = np.Infinity
                for jsort in range(0, self.strct.nsort):
                    if opts.mtRadii == "equal-ratios":
                        ad.smt = min(distmat[isort,jsort] * 0.5, ad.smt)
                    
                    else:
                        try:
                            r = float(opts.mtRadii)
                            ad.smt = r*angstrom2bohr
                        except:
                            print("Did not understand option --MT-radii, which can be a number (sets the muffin-tin radius to this many Angstroms), \"equal-ratios\", or \"optimized\" ")
                            quit()
         
                
            Kmax = max([RK / self.strct.ad[i].smt  for i in range(self.strct.nsort)])
            
            for isort in range(self.strct.nsort):
                site = ref_equivalent_sites[isort][0]    
                # Huristic for radius: should be <= 0.85 of distance ratio with all other atoms (weighed
                # by its radius)
                el = site.specie
                ad = self.strct.ad[isort]
                print("    MT radius of %s: %f (actual RK: %3.3f)" % (el, ad.smt, ad.smt*Kmax))
        
        
        
        for isort in range(self.strct.nsort):
            ad = self.strct.ad[isort]
            site = equivalent_sites[isort][0]    
            # Huristic for radius: should be <= 0.85 of distance ratio with all other atoms (weighed
            # by its radius)
            el = site.specie
            els = str(el)
            els = els[:min(2,len(els))]
            #print "**** els:", els
            if 'magmom' in site.properties:
                ad.magn_shift = float(site.properties['magmom'])
                print(" - set magn_shift=%f for %s #%d" % (ad.magn_shift, els, isort+1))
      
        def EvenCeilUnlessOne(f):
            ret = int( ceil(f))
            if ret != 1:
                ret = int( 2*ceil(f/2.0))
            return ret

        def EvenCeil(f):
            ret = int( 2*ceil(f/2.0))
            return ret
            
        print("Kmax is %3.3f"%Kmax)
        
        latticeNorms = np.array([norm(ref_struct.lattice.a), norm(ref_struct.lattice.b), norm(ref_struct.lattice.c)]) * angstrom2bohr
                            
        # this one controls the size of the k-mesh, it's the size of the smallest kpoint in the BZ
        # that one gets by dividing the reciprocal lattic by ndiv[:]
        #   TODO: the number was picked empirically, why does it work?
        #   Note- this affects only kmesh -> ndiv
        Ksmall = 0.11
        try:
            kmeshFactor = [float(opts.kMeshFactor) for f in latticeNorms]
        except:
            kmeshFactor = [float(f) for f in opts.kMeshFactor.split(",")]
        
        kmesh =  [EvenCeilUnlessOne(kmeshFactor[i] *2*pi/Ksmall/f) for i, f in enumerate(latticeNorms)]
        print("ndiv (k-mesh for Band basis) is %s, size: %d" % (kmesh, kmesh[0]*kmesh[1]*kmesh[2]))
                
        # TODO: how to automate this cutoff selection? This doesn't seem to matter (there is division and multiplication in Fort.)
        grid = Kmax  / self.bas.cut_lapw_ratio / 0.6 * latticeNorms / (2*np.pi) # divide by lengths of reciprocal lattice vectors

        kgrid = [EvenCeil(f) for f in grid]
        print("nrdiv (mesh for unit cell / PW basis) is %s, size: %d" % (str(kgrid), kgrid[0]*kgrid[1]*kgrid[2]))
        
        huristic_factor = opts.potential_grid_factor 
        potential_kgrid = [EvenCeil(huristic_factor*f) for f in grid]  # for DMFT we need large grid?
        print("mdiv (mesh for potential) is %s, size: %d" % (potential_kgrid, potential_kgrid[0]*potential_kgrid[1]*potential_kgrid[2]))
        
        
    
        self.rsm.allocate()
        #self.rsm.mdiv[:] =  [18,18,18]  # self.rsm.nrdiv[:] * 4/3
        self.rsm.mdiv[:] =  potential_kgrid 
        #self.rsm.nrdiv[:] = [10,10,10]  
        self.rsm.nrdiv[:] = kgrid # determines the grid for r-space as well as the plane wave basis
        self.rsm.nrdiv_red[:] = [4,4,4]
        
        
        self.kg.allocate()
        self.kg.ndiv[:] = kmesh
        self.kg.ndiv_c[:] = [2,2,2]
      
        # finally fill atomic data
        for isort in range(self.strct.nsort):
            ad = self.strct.ad[isort]
            site = equivalent_sites[isort][0]    
            self.FillAtomicData(ad, site, opts, maxL=ceil(ad.smt*Kmax)),
                             
    ############## end __init___ #####################################  
   
        
        
    def FilterSymmetryGroup(self, ops):
        nops = []
        for op in ops:
            # operations can translate also, but we are only interested in the direction
            pnt = op.apply_rotation_only(self.mag.b_ext)
            if all(abs(pnt - self.mag.b_ext) < 1.0e-5):
                nops.append(op)
        print("- Smaller group which preserves the magnetic field %s has %d elements" % (str(self.mag.b_ext), len(nops)))
        return nops
            
        
        
    @staticmethod
    def NandL(orb_str):
        Ltol = {"s":0, "p":1, "d":2, "f":3 }
        lstr = orb_str[-1]
        return [int(orb_str[:-1]), Ltol[lstr] ]

    def GetAtomicOrbitals(self, el, opts):        
        if el.Z == 94: # plutonium is not in the list

            rn = Element.from_Z(92)  # uranium
            Pu = rn.atomic_orbitals.copy()
            #Pu['6d'] = -3.9
            Pu['6p'] = -2.0 
            Pu['7p'] = -0.1
            return Pu
            
        return el.atomic_orbitals

    @staticmethod
    def toSuper(cs):
        SUP = u"⁰¹²³⁴⁵⁶⁷⁸⁹"
        ret = u""
        for c in cs:
            if c == ".":
                return ret
            else:
                ret += SUP[int(c)]
        return ret
   
    @staticmethod
    def spdf(l):
        return u"spdfghijklmnopqrstuvwxyz----"[l]
        
    @staticmethod
    def ValenceOrbsStr(orbs):
        st = u""
        for n, l, occ, _energy in orbs:
            st += u"%d%s%s "% (n, DFTInputBuilder.spdf(l), DFTInputBuilder.toSuper(str(occ)))
            
        return st
        
    # returns valenceOrbs
    def FindValenceOrbs(self, el, opts, maxL):
        occs = {}
        maxN = -1
        for n, l, o in el.full_electronic_structure:
            occs[str(n)+l] = o

            maxN = max(n,maxN)
        #print el.atomic_orbitals
        atomic_orbitals = self.GetAtomicOrbitals(el,opts)
        sortedOrbitals = sorted(iter(atomic_orbitals.items()), key=operator.itemgetter(1))
        
        occ_tweak = {}
        if opts.occupancies != "":
            for spec in opts.occupancies.split(" "):
                tel, eloccs = spec.split(":")
                if tel == el.symbol:
                    for occ_str in eloccs.split(","):
                        occ_tweak[occ_str[:2]] = float(occ_str[2:])
            print(f"- tweaking occupancies: {occ_tweak}")

        valenceOrbs = []
        for orbi, (orb_str,energy) in enumerate(sortedOrbitals):
            n, l = DFTInputBuilder.NandL(orb_str)
            if orb_str in occs.keys():
                occ =  occs[orb_str]
            else:
                continue
            if orb_str in occ_tweak:
                occ = occ_tweak[orb_str]
            valenceOrbs.append([n, l, occ, energy])

        valenceMaxN = valenceOrbs[-1][0]
        valenceMaxL = max([valenceOrbs[i][1] for i in range(len(valenceOrbs))])
        
        print(f"{el}[z={el.Z}] {self.ValenceOrbsStr(valenceOrbs)}")

        return valenceOrbs, valenceMaxN, valenceMaxL


    def FillAtomicData(self, ad, site, opts, maxL):
        el = site.specie
        ad.z = el.Z
        ad.txtel = el.symbol.strip()

        #Add some virtual doping
        if opts.doping != "":
            doping_inp = opts.doping.split(",")
            doping = {}
            for d in doping_inp:
                key_val = d.split(":")
                doping[key_val[0]] = float(key_val[1])
        
            if ad.txtel in doping.keys():
                ad.z_dop = doping[ad.txtel]
                print(f"adding {ad.z_dop} electrons to all {ad.txtel} elements")

        # pad one letter elements with _ on left, as Kutepov does - this only affects file names
        if len(ad.txtel) == 1:
            ad.txtel = '_' + ad.txtel

        # full representation of the valence
        valenceOrbs, _valenceMaxN, valenceMaxL = self.FindValenceOrbs(el, opts, maxL)
        MaxLoc = min(maxL, valenceMaxL+opts.locs_above_valence)

        semiCore = [None for l in range(valenceMaxL+1)]
        basis    = [None for l in range(maxL+1)]
        loc      = [None for l in range(MaxLoc+1)]

        # IDMD menu (Kutepov): 
        #    0 => at center of band (this seems to depend on the index in the list of orbs being before or after the APW)
        #    4 => at the fermi energy, 
        #    1 => at the ptnl energy,     (with given ptnl)
        #    2 => bound (but deriv. != 0) (with given ptnl)
        #    3 => completely localized,
        # for LOC, 0,4,3 are relevant and they always have 0 on the boundary (they are in fact "LO")

        for l in range(valenceMaxL+1, maxL+1):
            basis[l] = OO(atocc=0,ptnl=l+1+0.8,corr=False,idmd=0)

        # add the occupied states to the basis or to semicore
        for _idx, (n, l, occ, E) in enumerate(reversed(valenceOrbs)):
            stop = False
            if basis[l] is None:
                basis[l] = OO(atocc=occ,ptnl=n+0.8,corr=False,idmd=0) 
            elif semiCore[l] is None: 
                # this is our normal stopping condition - the last orbital to create
                semiCore[l] = OO(atocc=occ,ptnl=n+0.95,corr=False,idmd=0)
                if  basis[l].atocc == 4*l+2:
                    stop = True
            else:
                if occ <4*l+2:
                    raise Exception(f"atomic data issue: cannot allocate APW for a partially occupied orbital {(n, l, occ, E)}")
                stop = True
            # stop but only if all valence basis elements have been set
            if stop and all([basis[ll] is not None for ll in range(valenceMaxL+1)]):
                print(f"- {el} cutoff at n={n}, l={l} E={E}")
                break
        

        # Drop APW (basis) orbitals to semicore if they are full and semicore is not taken 
        for l in range(valenceMaxL+1):
            assert(basis[l] is not None), l
            if semiCore[l] is None and basis[l].atocc == 4*l+2: 
                semiCore[l] = basis[l]
                basis[l] = None
                semiCore[l].ptnl = int(semiCore[l].ptnl) + 0.95

        # localize semicore orbitals as requested by user
        for l in range(opts.localize_semicore_upto_l):
            if semiCore[l] is not None and semiCore[l].atocc ==4*l+2: 
                semiCore[l].idmd = 3

        # add a shell atom at the highest partially unoccupied APW
        for l in reversed(range(2,valenceMaxL+1)):
            if basis[l] is None:
                continue 
            if basis[l].atocc > 0 and basis[l].atocc < 4*l+2:
                if semiCore[l] is not None:
                    print(f"warning: correlated orbital for {l} bumps another ptnl={semiCore[l].ptnl}")
                # add correlated orbital
                bl = basis[l]
                idmd = 4 # at fermi
                if opts.correlated_bound and l==3:
                    idmd = 3 # bound
                elif opts.correlated_off_EF and l==2 or l==3:
                    idmd = 0 # in semicore level for this l
                occ2 = bl.atocc
                if opts.corr_occ is not None:
                    occ2 = opts.corr_occ
                semiCore[l] =  OO(atocc=occ2,ptnl=int(bl.ptnl)+0.85,corr=True,idmd=idmd) 
                bl.atocc = bl.atocc - semiCore[l].atocc

        # Complete the APW basis if functions are missing
        for l in range(valenceMaxL+1):
            if basis[l] is None:
                if semiCore[l] is None:
                    basis[l] = OO(atocc=0,ptnl=l+1+0.8,corr=False,idmd=0) 
                else:
                    sc = semiCore[l]
                    basis[l] = OO(atocc=0,ptnl=int(sc.ptnl)+1+0.8,corr=False,idmd=0) 

        # Add APW for levels higher than valence
        for l in range(valenceMaxL+1, maxL+1):
            basis[l] = OO(atocc=0,ptnl=l+1+0.8,corr=False,idmd=0) 
        
        # add augmentations (not for s)
        for l in range(1, MaxLoc+1):
            # do not add two LO for one L if the semicore is correlated
            if l<=valenceMaxL and semiCore[l] is not None and semiCore[l].corr:
                if l+1<=valenceMaxL and semiCore[l+1] is not None and semiCore[l+1].corr:
                    # if the next l is also correlated, then this augmentation should be create
                    None
                else:
                    # don't create this augmentation - it is the top correlated orbitla
                    continue
            bo = basis[l]
            if bo is not None:
                loc[l] = OO(atocc=0.0,ptnl=bo.ptnl+1,corr=False,idmd=1) 
            else:
                break # no more LOC's than APWs

        #----------------------------------------------------------------------
        # write into ad
        ad.lmb = maxL
        ad.lmpb = maxL
        count = 0
        for l in range(maxL+1):
            if l < valenceMaxL+1:
                count += (semiCore[l] is not None)
            count += (basis[l] is not None)
            if l < MaxLoc+1:
                count += (loc[l] is not None)
        ad.maxntle = count
        ad.allocate()
        ad.ntle[:] = 0
        ad.lim_pb_mt[:] = 0
        ad.lim_pb_mt_red[:] = 0

        for l in range(maxL+1):
            if l < valenceMaxL+1 and semiCore[l] is not None:
                semiCore[l].Fill(ad, l, isApw=False)
            if basis[l] is not None:
                basis[l].Fill(ad, l, isApw=True)
            if l <  MaxLoc+1 and loc[l] is not None:
                loc[l].Fill(ad, l, isApw=False)
