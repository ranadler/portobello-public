#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

'''
Created on Jun 13, 2018

@author: adler
'''

import sys
import os
import gc
#from argparse import Namespace
from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer
from portobello.bus.persistence import Store
from portobello.generated.PEScf import SolverState
from portobello.rhobusta.PEScfEngine import PEScfEngine
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.dft_plus_sigma import ContinueWithLocalSelfEnergy
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.generated.FlapwMBPT import Input,RhobustaExtra
import numpy as np
from pymatgen.core.periodic_table import Element

from portobello.bus.fortran import Fortran
from ctypes import CFUNCTYPE, c_void_p
import portobello.symmetry.WignerDJ as wigdj# make sure we load the wigner J matrices 
from argparse import ArgumentDefaultsHelpFormatter
import argparse #, argcomplete
from pathlib import Path
from portobello.rhobusta.options import SaveOptions, RefreshOptions
from portobello.rhobusta.InputBuilder import DFTInputBuilder
from portobello.jobs.Organizer import with_organizer

def strcutrue_from_W2K(struct_filename):
    from matdelab.dmft.w2k_struct import W2KStruct
    return W2KStruct.from_file(struct_filename).PMG_struct
   

def ShowCrystal(st):
    from pymatgen.vis import structure_vtk

    vis = structure_vtk.StructureVis(show_bonds=True, show_polyhedron=False, show_unit_cell=True)
    vis.show_help = True
    vis.show_bonds = True 
 
    vis.set_structure(st, reset_camera=True ,to_unit_cell=True)
    vis.show()
    
def PrintAllValenceOrbitals():
    for z in range(1,90):
        el = Element.from_Z(z)
        print(el)
        builder = DFTInputBuilder() 
        builder.FindValenceOrbs(el, None)
        

Ry2eV = 27.2107*0.5
        

def DFT(args, opts=None, **kwargs):
    
    curdir = os.getcwd()
    if hasattr(opts,"dir"): #DFT0 called from higher level programs -- might already be in the directory, which causes problems if it's a relative path
        try:
            os.chdir(opts.dir)
        except:
            pass
    supervisor = RhobustaRunner(args, kwargs, opts=opts)
    supervisor.Run()
    #isWorker = supervisor.opts.worker
    del supervisor

    os.chdir(curdir)
 
@with_organizer
def DFT0(opts):
    DFT([], opts)

    
@with_organizer
def DFTPlusSigma(opts):
    opts.plus_sigma = True
    supervisor = RhobustaRunner([], [], opts=opts)
    supervisor.Run()
    del supervisor
           
@with_organizer
def DFTPlusG(opts):
    opts.gutz = True
    supervisor = RhobustaRunner([], [], opts=opts)
    supervisor.Run()
    del supervisor
    
def RhobustaOptionsParser(add_help=False, private=False):
        parents = []
        if private: parents = [GetMPIProxyOptionsParser()]

        parser = ArgumentParserThatStoresArgv('rhobusta', 
                                parents=parents,
                                add_help=add_help ,formatter_class=ArgumentDefaultsHelpFormatter)
        has_file = Path("./ini.h5").exists()
        
        # one positional optional argument
        parser.add_argument(dest="input_file", nargs="?",
                            help="required if there is no ini.h5 file in this directory",
                            type=str)
        
        parser.add_argument("--info", dest="run", #just show info
                          action="store_false",
                          default=True,
                          help="Prepare input, don't run Rhobusta, only print information.") 
        
        if private:
                        
            tg = parser.add_mutually_exclusive_group(required=True)
            
            tg.add_argument("-b", "--beta", dest="beta", type=float, default=None)
        
            tg.add_argument("-T", "--Temperature", dest="Temperature", default=None,
                            type=int, help="Temperature in Kelvin")

            parser.add_argument("--recalc-window-bands", dest="recalc_window_bands", 
                            default=False,                           
                            action="store_true",
                            help="force recalculation of window band numbers in dft+, even if this is not the 1st iteration in the super-iter")
            
   
        parser.add_argument("-i", "--iterations", dest="num_charge_iterations", 
                         default=10, type=int,
                         help="number of iterations of the charge self consistency equations")
        
        parser.add_argument("--functional", dest="dft_functional", 
                         default="gga",
                         help="dft functional")
     
        
        parser.add_argument("-R", "--fully-relativistic", dest="fully_relativistic", 
                            help="run DFT in relativistic mode, which includes a relativistic muffin-tin and a relativistic interstitial",
                            action="store_true",
                            default=False)
         
        parser.add_argument("--non-rel-interst", dest="interst_rel", 
                            help="keep interstitial non-relativistic",
                            action="store_false",
                            default=True)
        
        parser.add_argument("-a", "--crystal-constant", dest="a", type=float, default=1.0,
                          help="crystal constant multiplier - 1.0 means the sizes from the cif will be preserved.")

        parser.add_argument("--lattice-scaling", dest="lattice_scaling", default="",
                          help="Instead of scaling the volume, as in -a/--crystal-constant, scale each lattice vector by some value (comma delimited floats).")

        li = parser.add_argument_group(title="LAPW inputs", description="parameters that control the generated LAPW basis and meshes")
              
        li.add_argument("-K", "--kmesh-factor", dest="kMeshFactor",
                          help="size multiplier for the k-mesh used for integration",
                          default=1.0)
        
        li.add_argument("-Q", "--RK", dest="goalRK",
                          help="sets the Lmax=RKmax - all other RK factors are scaled proportionally",
                          type=float,
                          default=None)
       
        li.add_argument("--MT-radii", dest="mtRadii", 
                          help="""Name of strategy to generate MT radii: 
                                "equal-ratios": divide the distance equally between neighbors
                                "optimized": radii to optimize basis""", 
                          default="optimized")

        li.add_argument("--reference-crystal-constant", dest="ref_a", type=float, default=1.0,
                          help="MT Radii generated for a crystal with the lattice vectors scaled by this value (see -a).")

        li.add_argument("--reference-lattice-scaling", dest="ref_lattice_scaling", default="",
                          help="MT Radii generated for a crystal with the lattice vectors scaled by these values (see --lattice-scaling).")
        
        li.add_argument("-f", "--radius-reduction-factor", dest="reduction", type=float, default=1.0,
                          help="1.0 means the spheres will possibly touch (no reduction)")

        li.add_argument("--radius-reduction-factor-per-atom", dest="reduction_per_atom", default="",
                          help="comma seperated list for each sort of atom in order")
    
        li.add_argument("--potential-grid-factor", dest="potential_grid_factor", type=float, 
                          default=1.2, #4/3.0 was used earlier, 2.5 works better, 1.2 more like examples
                          help="factor that goes into the potential grid multiplier")
    
        li.add_argument("--correlated-bound", dest="correlated_bound", 
                         default=False,
                         help="treat correlated orbital as bound rather than solving at Fermi energy",
                         action="store_true")

        li.add_argument("--correlated-on-EF", dest="correlated_off_EF", 
                         default=True,
                         help="solve the radial equation for the correlated orbs. at E_F",
                         action="store_false")

        li.add_argument("--initial-occ", dest="occupancies", 
                         default="",
                         help="override initial occupancies, defined per elment. For example: 'Pu:3d8,4f6 Fe:3d2' ")

        li.add_argument("--correlated-occ", dest="corr_occ", 
                         type=int,
                         default=None,
                         help="if not None, defines the initial occupancy of the correlated orbital")
               
        li.add_argument("--locs-above-valence", dest="locs_above_valence", 
                        type=int,
                         default=2,
                         help="number of l's above max occupied valence to add LOC orbitals - can go up or down (to negative).")
               
        li.add_argument("--localize-semicore-upto-l", dest="localize_semicore_upto_l", 
                        type=int,
                         default=-1, # not localizing (solving at semi-level) any loc orbitals
                         help="localize the deep l semi core orbitals, upto level l (not including)")
               
        parser.add_argument("--restart", dest="restart",
                          action="store_true",
                          default=False,
                          help="restart from syncpoint - do not create input, and read sync point") 
       
        parser.add_argument("-S", "--spin-polarized", dest="spin_polarized", 
                          action="store_true",
                          default=False,
                          help="Run with 2 spins instead of 1 per state")
        
        
        parser.add_argument("--SDFT", dest="dft_spin_polarized", 
                          action="store_true",
                          default=False,
                          help="break spin symmetry in DFT. Repeat this flag for restarts.")

        parser.add_argument("-M", "--magnetic-field", dest="B_spec", default="", help="if vector (3 numbers separated by ','),"+
                          " it describes the full magnetic field, if a number, it is the size of B along the z axis.")
        
        parser.add_argument("--dft-no-symm", dest="dft_no_symm", 
                          action="store_true",
                          default=False,
                          help="set the dft crystal space group to {I} - note that this increases the problem size considerably")

    
        cp = parser.add_argument_group(title="correlations", description="DFT+X parameters")
              
        
        cg = cp.add_mutually_exclusive_group()
        
        cg.add_argument("--plus-sigma", dest="plus_sigma",
                          action="store_true",
                          default=False,
                          help="run DFT+Sigma, not just DFT. If self-energy is not present, it is assumed 0") 
          
        cg.add_argument("-G", "--with-gutz-density", dest="gutz", 
                          action="store_true",
                          help="run DFT+G, that is, DFT with a present Gutzwiller solution (consisting of R and \lambda)",
                          default=False)
            
        cp.add_argument("--use-z-in-mu-search", dest="use_z_in_mu_search",
                            default=False,
                            type=bool,
                            help="use z in seraching mu (multiply density by z) - applies only in qp mode, --mu-search-variant=qp")
    
        cp.add_argument("--mu-search-variant", 
                          dest='mu_search_variant',
                          type=str,
                          default="default",
                          help="""use this to force the mu search algorithm to be either dft or qp 
                                  (note that qp is enough, no need for the right fortran spacing).
                                  Note that qp search uses the qp lifetime if available, and Z if --use-z-in-mu-search """)
        
        # this is a technical flag, which is on when running a full-greens function case
        cp.add_argument("--with-full-green",
                        dest="with_full_green",
                        action="store_true", default=False, help=argparse.SUPPRESS)
        
        # this is not exactly the same thing as in DMFT cutoff, but it is the cutoff for exact
        # frequencies in a similar way, but for the summation of Gn in the DFT+G code,
        # and therefore it should be around the same value
        cp.add_argument("--matsubara-exact-energy-limit", dest="matsubara_exact_energy_limit", default=10.0, type=float,
                          help="energy limit for exact self-energy frequency for the summation algorithm [eV]")

        # this implement the stitching point to the Gutzwiller and DMFT loops
        cp.add_argument("--density-callback", dest="density_callback", default=None, help=argparse.SUPPRESS)
        
        
        parser.add_argument("-x", "--admix", dest="admix", type=float, default=0.1,
                            help="should be a floating point number between 0.0 and 1.0 - it is the admix used on every iteration")

        parser.add_argument("--fix-mu", 
                          dest='search_mu',
                          action="store_false",
                          help="do not re-calculate mu any more - use the current value (this can be runed on and off)",
                          default=True)

        # this is to work around issues of FLAPW with complex primtiive cells, which to get the k-tetrahedron algorithm
        # into trouble. In these cases the easiest think to do is to work with some symmetric non-primitive cell.
        parser.add_argument("--use-given-cell", dest="use_given_cell", 
                          action="store_true",
                          help="use the unit cell defined in the input file, do not try to reduce it (mostly usefull if there is a bug in the reduction)",
                          default=False)        
        
        parser.add_argument("-C", "--charge-conversion-goal", 
                          dest='charge_conversion_goal',
                          type=float,
                          default=1.0e-7,
                          help="""goal of charge conversion. Computation will end when it is hit (it may end before because of maxed out iterations)""")
        
        parser.add_argument("-P", "--plots", dest="plots", 
                         default=None,
                         help="number of ticks in one band-plot segment. If 0, it is calculated. (default: None, no plotting).")
               
        parser.add_argument("--dft-debug", dest="dft_debug", 
                         default=False,
                         help="debug dft",
                         action="store_true")

        parser.add_argument("--spacegroup-angle-tolerance", 
                          dest='spacegroup_angle_tolerance',
                          type=float,
                          default=5.0,
                          help="angle tolerance for spacegroup diagnosis")

        parser.add_argument("--finalize", dest="finalize", 
                         default=False,
                         help="finalize dft+ when done",
                         action="store_true")

        parser.add_argument("--spin-parity-representation", dest="spin_parity_representation", 
                         default=1.0,
                         help="see spins.py, if set to 'old' this will be 1j")

        parser.add_argument("--doping", dest="doping", 
                         default="",
                         help="""Add some virtual doping, initial concentrated on the given ion. 
                         For example, Fe:0.1,Li:0.2, adds 0.1 electrons to all iron atoms and 0.2 electrons to all Lithium ions.""")

        parser.add_argument("--directory", dest="dir", 
                         default=".",
                         help="""Directory in which to run. [for organized runs only]""")

        ad = parser.add_argument_group(title="atomic displacements", description="adjusting positions from input structure file")

        ad.add_argument("--atom-to-displace", dest="atom_to_displace",
                         default="",
                         help="Name of atom to displace")

        ad.add_argument("--direction-to-displace", dest="direction_to_displace",
                         default="0,0,1",
                         help="Name of atom to displace")

        ad.add_argument("--atom-displacement", dest="atom_displacement",
                         default=0.01,
                         type=float,
                         help="Displacement of atom in bohr")

        return parser
    
    
    
   
class RhobustaRunner(MPIContainer):
    def __init__(self, args, kwargs, opts=None):
        if "--info" in args or "--restart" in args or "-P" in args or "-G" in args or "--plus-sigma" in args:
            # These cases don't need a temperature -- they take it from elsewhere for consistency
            # This makes it so the user doesn't have to supply a temperature that does nothing (in these cases)
            args = args + ['-T1'] 
                
#        if "-TEST" in args:
#            opts = Namespace()
#            RefreshOptions(opts)
#            opts.worker = True
#            opts.input_file = "./ini.h5"
          
        if opts is None:
            parser = RhobustaOptionsParser(add_help=True, private=True)
            #argcomplete.autocomplete(parser)
            self.opts, self.args = parser.parse_known_args(args)
             
        else:
            self.opts = opts
        
        for key in kwargs:
            setattr(self.opts, key, kwargs[key])

        MPIContainer.__init__(self, self.opts)
                      
        self.dft_callback = None
                                    
        if self.opts.plus_sigma or self.opts.gutz or self.opts.plots is not None:
            if os.path.exists("./ini.h5"):
                self.opts.restart = True
        
        if  hasattr(self.opts,'dft_spin_polarized') and self.opts.dft_spin_polarized:
            self.opts.spin_polarized = True
        
        if self.opts.input_file is not None:
            self.opts.restart = False
                
        if self.opts.restart:
            input_objects_path = "./ini.h5:/"
            inp = Input()
            inp.load(input_objects_path)
            self.opts.Temperature = inp.ctrl.temperature
            self.opts.beta = None # see later

        if self.opts.plus_sigma or self.opts.gutz:
            state = SolverState()
            suffix = PEScfEngine.GetSuffixOfFirstImpurity()
            loc = f'./dmft{suffix}.h5:/' if self.opts.plus_sigma else f"./gutz{suffix}.h5:/"
            state.load(loc)
            self.opts.beta = state.beta
            self.opts.Temperature = None
        
        
    def SetPerRunOptions(self, input_objects_path="./ini.h5:/", inp = None):
        todel = None
        if inp is None:
            inp = Input()
            inp.load(input_objects_path)
            todel = inp
        extra = RhobustaExtra()
        try:
            extra.load(input_objects_path+"rhobusta/")
        except:
            # it will contain the defaults, extra does not exist
            None
            
        if  hasattr(self.opts,'dft_spin_polarized'):
            extra.is_dft_spin_polarized = self.opts.dft_spin_polarized
        else:        
            extra.is_dft_spin_polarized = False
        
        inp.ctrl.admix = self.opts.admix
        inp.ctrl.temperature = self.opts.Temperature
        inp.ctrl.iter_dft = self.opts.num_charge_iterations
        inp.ctrl.iter_qp = 0
        
        inp.tmesh.n_tau = 4        # need high accuracy value at beta (0)
        if hasattr(self.opts,'plus_sigma') and self.opts.plus_sigma:
            energy_limit = self.opts.matsubara_exact_energy_limit
            inp.omesh.n_omega_exa =  int(energy_limit * self.opts.beta / (2*np.pi))
            # the geometric part is up to 20 (not in this code)
            inp.omesh.n_omega_asy = 20
            inp.omesh.omega_max = 20.0 # Ry
        
            # TODO: change this when we start to run GW
            inp.tmesh.exp_tau_gw = 10 # see mesh_tau_spl.f90
            if self.IsMaster():
                print(f" - n_omega_exact={inp.omesh.n_omega_exa}, n_tau={inp.tmesh.n_tau}")
        else:
            inp.omesh.n_omega_exa = 1
            inp.omesh.n_omega_asy = 2  # not 1, to prevent crash in an ini routine
            inp.omesh.omega_max = 1.2 # Ry
            inp.tmesh.exp_tau_gw = 1

        inp.nmesh.n_nu_exa = 1
        inp.nmesh.n_nu_asy = 1
        #inp.bas.cut_pb_ratio = 0.01 # not using pb in portobello at the moment
        
        #plotting options
        inp.dos.emindos = -1.5  # in Ry
        inp.dos.emaxdos = 1.5   # in Ry
        inp.dos.ndos = 1000
        
        if hasattr(self.opts,'plots') and self.opts.plots is not None:
            if self.opts.plots == '' or int(self.opts.plots) <= 0:
                inp.bp.n_k_div  = max(int(min(inp.kg.ndiv[:])*0.75), 2)
            else:
                inp.bp.n_k_div = int(self.opts.plots)
            print(" - plotting with %d ticks"% inp.bp.n_k_div) 
    
        if self.IsMaster():
            inp.store(input_objects_path, flush=True)
            extra.store(input_objects_path+"rhobusta/", flush=True)
        if todel:
            del todel
        
    # if returns False, the program ends
    # The purpose of this code is either to create or update the ini.h5 file
    # so that it will contain the correct crystal info, as well as the correct operational
    # parameters, most importantly the correct parralelization info for this run - which
    # has to be updated before the fortran part is started.
    def BeforeWorkers(self):
        
        KB = 6.333328e-6 *27.2107* 0.5 # collected from Kutepov's code, to match perfectly

        if self.opts.Temperature is not None and self.opts.beta is None:
            self.opts.beta = 1/(self.opts.Temperature * KB)
        if self.opts.Temperature is None:
            self.opts.Temperature = 1/self.opts.beta / KB
        
        self.opts.beta = 1/(self.opts.Temperature * KB)
            
        assert(self.opts.Temperature is not None)

        # for now don't use qp
        self.opts.mu_search_variant = 'dft'

        SaveOptions(self.opts)
                            
        input_file = "./ini.h5"
        input_objects_path = input_file + ":/"

        inp = None
        input_struct_file = None
        if self.opts.input_file is not None:
            input_struct_file = self.opts.input_file
            if input_struct_file.endswith("ini.h5"):
                inp = Input()
                inp.load(input_struct_file+":/")
                self.opts.restart = False
                print("starting from scratch with existing ini.h5 file")
        else:
            if not self.opts.restart:
                print("need input file name (cif, struct, or POSCAR) ")
                #TODO: find the input - either cif, or struct or POSCAR
                return False # do not continue
                
        if not self.opts.restart:
            if inp is None:
                inp = DFTInputBuilder(input_struct_file, self.opts)
                print(f"**** writing input file {input_objects_path} *****")
                inp.store(input_objects_path, flush=True)
                if inp.extra is not None:
                    inp.extra.store(input_objects_path+"extra/", flush=True)
        else:
            print(" - restarting from existing input " + input_objects_path)
            inp = Input()
            inp.load(input_objects_path)
               
        sys.stdout.flush()
        if self.opts.run:
            if self.IsMaster: print(" - setting run options in ini file")
            self.SetPerRunOptions(input_objects_path, inp)
            
        if self.opts.run and (self.opts.num_charge_iterations > 0 or self.opts.plots is not None):
            return True
        else:
            return False
          
    def WorkerArgs(self):
        args =  "-m portobello.rhobusta.rhobusta".split() +\
                self.TruncateSysArgs()+ ["--worker"] 
        if self.opts.gutz:
            args += ['-G']
            #if self.opts.mu_search_variant == 'default':
                #args += ['--mu-search-variant', 'qp']
        elif self.opts.plus_sigma:
            args += ['--plus-sigma']
        # for now only dft search
        args += ['--mu-search-variant', 'dft']

        return args
        
    def AfterWorkers(self):
        if self.opts.finalize:
            from portobello.bus.persistence import Store
            Store.Singleton().ClearAll()

            # TODO: call memory_close; call ending (in Fortran)
    
             
    def RegisterCallbackInFortran(self, python_callback):
        if self.dft_callback is None:
            # this is done to keep the callback function object around, so that it is not garbage collected, and when Fortran
            # calls it, it will exist.
            self.dft_callback = CFUNCTYPE(c_void_p)(python_callback) 
            fortran = Fortran.Instance()
            fortran.invoke("dftplussigma.mp.register_prepare_sigma_or_rho_cb", self.dft_callback)
            
    def MPIWorker(self):
        
        self.InitializeMPI()

        if self.opts.gutz:
            from portobello.rhobusta.dft_plus_sigma import EmbedSigma
            self.opts.density_callback = EmbedSigma  
            self.opts.with_full_green = False
        elif self.opts.plus_sigma:
            from portobello.rhobusta.dft_plus_sigma import EmbedSigma
            self.opts.density_callback = EmbedSigma
            self.opts.with_full_green = True
                       
        if self.opts.density_callback:
            python_callback = None
            if isinstance(self.opts.density_callback, str):
                modn, funcn = self.opts.density_callback.rsplit('.', 1)
                if modn not in sys.modules:
                    __import__(modn)
                python_callback = getattr(sys.modules[modn], funcn)
            else:
                python_callback = self.opts.density_callback
                
            if python_callback is not None:
                self.RegisterCallbackInFortran(python_callback)
                              
        if self.opts.density_callback is not None:
            self.opts.density_callback = python_callback.__name__
                               
        # this happens usually on the 1st run, so that it creates the green's function in the image
        if self.opts.with_full_green:
            DFTPlugin.SetGw()
             
        if self.opts.plus_sigma :
            ContinueWithLocalSelfEnergy(self, restart=True, hasSelfEnergy=True) # sigma_location is the default, which is not ""
        elif self.opts.gutz:
            ContinueWithLocalSelfEnergy(self,restart=True, hasDensity=True)
        else:
            ContinueWithLocalSelfEnergy(self,restart=self.opts.restart)

        RefreshOptions(self.opts)

        if self.opts.plots is not None:
            plugin = DFTPlugin.Instance()
            plugin.CreatePlots()

        # MUST call this in the end
        self.Barrier()
        self.Terminate()
        gc.collect()

        if self.opts.finalize:
            from portobello.bus.persistence import Store
            Store.Singleton().ClearAll()

            # TODO: call memory_close; call ending (in Fortran)
    
    
if __name__ == '__main__':
    DFT(sys.argv[1:])

    
