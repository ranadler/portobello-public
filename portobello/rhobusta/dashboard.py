#!/usr/bin/env python3

import os
import matplotlib
import glob
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.loader import AnalysisLoader, LightWeightLoader
from portobello.bus.mpi import GetMPIProxyOptionsParser
import sys
from subprocess import call, run
import subprocess
import re
import h5py
from portobello.generated.gutz import GState
from portobello.generated.PEScf import SolverState
import pymatgen

import matplotlib.pyplot as plt
from portobello.generated.FlapwMBPT import Input
from portobello.generated.dmft import DMFTState
import numpy as np
from pathlib import Path
from portobello.rhobusta.observables import AddObservables, GetAllObservables, GetDenmat
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.rhobusta.multi_impurity import MultiImpuritySupervisor
import time
from portobello.bus.persistence import Store

def press(event):
    #print('press', event.key)
    #sys.stdout.flush()
    if event.key == 'R':
        #visible = xl.get_visible()
        #xl.set_visible(not visible)
        #fig.canvas.draw()
        print("*** R pressed ***")

def CommunicateWithNode(opts):
    method = ""
    abspath = os.path.abspath(".")
    dnames = abspath.split("/")[-3:]
    p = subprocess.Popen(["ssh", "rupc02", "ssh -2 rupc-caip-01 '. /etc/profile;/opt/sge6/bin/lx24-amd64/qstat'"], stdout=subprocess.PIPE)
    qs, _stderr = p.communicate()
    for line in qs.decode('utf-8').split("\n"):
        entry = re.split("\s+", line)
        
        if len(entry) > 1 and entry[3] in dnames:
            if len(entry) > 4:
                idx = abspath.find(entry[3])
                assert(idx != -1)
                wdir = f"/scratch/{entry[1]}{entry[4]}/{abspath[idx+len(entry[3]):]}"
                #print("wdir is", wdir)
            machine = entry[8].split("@")[-1]
            
            if opts.login:
                # open interactive shell with the node
                ret = call(["ssh", machine, "-t", "cd %s/;bash"%wdir])
                return None
            if opts.track_log:
                ret = call(["ssh", machine, "-t", f"cd {wdir}/;tail -f all.out"])
                return None

            if opts.update:
                ret = call(["scp", *opts.files_to_upload, f"{machine}:{wdir}/"])
                return None
            
            if opts.vi_opts_json:
                ret = call(["ssh", machine, "-t", f"cd {wdir}/;vi opts.json"])
                return None

            
            if opts.abort_job:
                raw = input('add note:')
                f = open('./TERMINATED', 'w+')
                f.write(raw + "\n")
                f.close()
                if not opts.discard:
                    print("Deleteing temporary files *tmp* and core files")
                    ret = call(["ssh", machine,  f"rm -f {wdir}/*tmp* {wdir}/core*"])
                else:
                    print("Discarding all temporary / large h5 files")
                    ret = call(["ssh", machine,  f"rm -f  {wdir}/*tmp* {wdir}/core* {wdir}/*/*tmp*"])
                    ret = call(["ssh", machine,  f"rm -f {wdir}/flapw_image.h5"])
                    ret = call(["ssh", machine,  f"date > {wdir}/DISCARDED"])
                ret = call(["ssh", machine,  "echo ' terminated:' `date +'%%Y-%%m-%%d %%T:%%N'` >> %s/README"%wdir])
                ret = call(["scp", "./TERMINATED", "%s:%s/"%(machine, wdir)])
                ret = call(["ssh", machine,  "echo  -n ' reason: '      >> %s/README"%wdir])
                ret = call(["ssh", machine,  "cat %s/TERMINATED >> %s/README"%(wdir, wdir)])
    
                print("Killing job %s" % entry[1])
                ret = call(["ssh", "%s@rupc02"%entry[4], ". /etc/profile;/opt/sge6/bin/lx24-amd64/qdel %s"% entry[1]])
                return None
             
            if not opts.recursive:
                lsCommand = f"ls  {wdir}/ini.h5 {wdir}/organizer.log {wdir}/README {wdir}/proj.h5 {wdir}/opts.json {wdir}/projector.h5 {wdir}/muti-impurity.h5 {wdir}/dmft*.h5 {wdir}/gutz*.h5 {wdir}/history.h5"
                 
                if opts.download_all:
                    lsCommand += f" {wdir}/flapw_image.h5 {wdir}/all.out"
                if opts.copy_overlaps:
                    lsCommand += f" {wdir}/overlaps.tmp.h5"
                    
                separator = "+-01"*20
                lsCommand += f"; echo '{separator}'; tail -30 {wdir}/all.out"
                    
                exclusion = ["gutz-impurity"]
                file_search = subprocess.check_output(["ssh", machine, lsCommand],stderr=subprocess.DEVNULL)
                if file_search == "":
                    print("Cannot communicate with the node, exiting")
                    exit(0)
                found_files, all_out = file_search.decode(encoding='utf-8').split(separator)
                 
                print("-"*180)
                print(all_out)
                print("-"*180)
            
                file_str = str(found_files)
                found_files = str(file_str).split()
                files = [f"{machine}:/{f}" for f in found_files]
                
                if all(f"ini.h5" not in f for f in found_files):
                    print("Cannot find ini.h5 file, quitting")
                    exit(0)
                                
                for exc in exclusion:
                    files = [ff for ff in files if exc not in ff]
                
                ret = call(["rsync", "--progress", "-h", "-e", "ssh", *files, "."])
                
                if all(f"history.h5" not in f for f in found_files):
                    print("Cannot find history.h5 file, quitting")
                    exit(0)
                    
            else:
                print("recursively copying all subtree")
                ret = call(["rsync", "-r", "--progress", "-h", "-e", "ssh",  f"{machine}:{wdir}/", "."])
                
            if ret != 0:
                print("Cannot communicate with the node using rsync, check rsync in the PATH, or network status. exiting")
                exit(0)
                
            return method
    return ""



def DoPlots(opts, loader, stuffToPlot, obs, plotMu, plotSign, plotNlatt, plotEnergyImp, plotEnergyBands, plotNN, plotChargeCvg, plotQ, *plotObs):
    
    if loader.isDMFT:
        sig = []
        dsig = []
        plotSig = plotObs[0]
        plotDSig = plotObs[1]

    allMu = []
    allNimp = []
    allNlatt = []
    allEnergyImp = []
    allNN = []
    allEnergyBands = []
    signs = []
    Q = []
    allObs = { o : [] for o in obs }
    
     
    dftIndices = []
    corrIndices = []
    
    cc = []

    ishell = stuffToPlot.ishell
    shell = stuffToPlot.shell
    labels = stuffToPlot.labels
    xticks = stuffToPlot.xticks
    SolverHistory = stuffToPlot.SolverHistory
  
    proj_location = "./projector.h5:/def/"
    proj = SelectedOrbitals()
    proj.load(proj_location)
      
    observables = GetAllObservables(proj.shells[ishell], subspace_expr = proj.subspace_expr)

    index = 0
    obsM = {} # matrix for each observer
    for src in SolverHistory:
        if 1:
            if loader.isDMFT:
                svs = SolverState()
            else:
                svs = GState()
            svs.load(src)

            allNimp.append(svs.Nimp)
            allNlatt.append(svs.Nlatt)
            allEnergyImp.append(svs.energyImp)
            allEnergyBands.append(svs.energyBands)

            AddObservables(observables, svs, opts)
            

            dm0 = GetDenmat(svs)
            for o in obs:
                dm = dm0
                name = o

                if o.find("@") != -1:
                    name, repnum = o.split("@")
                    repnum = int(repnum)
                    dm = np.zeros_like(dm)
                    dm = 1.0*(observables.Get("rep") == repnum)
                    dm /= np.trace(dm * dm)[0,0] # this is sum of the ones, so that we get average 
                # if variable starts with +, we report the observable for the highest prob.
                # multiplet in the histogram
                elif o.find("#") != -1:
                    name, num = o.split("#")
                    if num =="": 
                        num=0
                    else:
                        num=int(num)
                    conf =  list(reversed(np.argsort(svs.hist.p)))[num]
                    bits=conf
                    dm = np.zeros_like(dm)
                    n=0
                    while bits != 0:
                        b = bits & 0x1
                        if b == 1: 
                            dm[n,n] = 1.0
                        bits = bits >> 1
                        n+=1
                if o not in obsM:
                    obsM[o] = observables.evaluate(name)

                # evaluate the observable with the density matrix
                val = np.real(np.trace(dm * obsM[o])[0,0])
                #print(o, val)
                allObs[o].append(val)
        
            
            if loader.isDMFT:
                if svs.state != 'dft+sigma':
                    svs = DMFTState()
                    svs.load(src)
                    signs.append(svs.sign)
                    avg_sig = np.imag(np.sum(svs.sig.M, axis=(1,2,3)))/svs.sig.num_omega/svs.dim # keeping omega axis
                    
                    if len(sig) > 0:
                        sig.append( np.sum(np.abs(last_sig - avg_sig)) ) 
                    else:
                        sig.append(0)
                    last_sig = avg_sig

                    if len(avg_sig) > 1:
                        dsig.append( avg_sig[1] - avg_sig[0] / svs.sig.omega[0] - svs.sig.omega[1])
                    else:
                        dsig.append (avg_sig[0]/svs.sig.omega[0])
                else:
                    # no sign reported here - 
                    signs.append(signs[-1] if len(signs)>0 else 0.0)
                    dsig.append(dsig[-1] if len(dsig)>0 else 0.0)
                    sig.append(sig[-1] if len(sig)>0 else 0.0)
            else:
                signs.append(float(svs.success))
            
            Q.append(svs.quality)  
                
            allNN.append(svs.NN)
            allMu.append(svs.mu)
            
            cc.append(svs.cvg.charge_convergence)
            #print svs.state
            if svs.state.startswith('dft+'):
                dftIndices.append(index)
                
            else:
                corrIndices.append(index)
            
                
            index += 1
            
            
    def plotDFTandCorrMarkers(plot, y, log = False):
        ynp = np.array(y)
        plot.margins(0.0)
        plot.scatter(labels[dftIndices], ynp[dftIndices], marker='+')
        plot.scatter(labels[corrIndices], ynp[corrIndices], marker='o', color='grey')
        plt.xticks(labels,xticks,fontsize=8, rotation=(90))
        #plot.minorticks_on()
        if len(ynp[ynp > 0]) > 0:
            if log:
                plot.set_yscale('log')
                min_gt0 = np.min(ynp[ynp > 0])
                plot.set_ylim(min_gt0, np.max(ynp)) 
        plot.grid(which='major', linestyle='-', linewidth='0.1', color='brown')
        #plot.grid(which='minor', linestyle=':', linewidth='0.1', color='grey')
            
    labels = np.array(range(len(allMu)))  # last one may be missing
            
    
    plotMu.set_ylabel("$\mu$",fontsize=12)
    plotMu.plot(labels, allMu)
    plotDFTandCorrMarkers(plotMu, allMu)

    if loader.isDMFT:
        plotSign.set_ylabel("sign",fontsize=12)
    else:
        plotSign.set_ylabel("solver conv.", fontsize=12)
    plotDFTandCorrMarkers(plotSign, signs)  


    if loader.isDMFT:
        plotSig.set_ylabel(r"$\langle\Delta\Sigma\rangle$",fontsize=12)
        plotDSig.set_ylabel(r"$\langle\Sigma'\rangle$",fontsize=12)

        plotDFTandCorrMarkers(plotSig, sig, log = True)      
        plotDFTandCorrMarkers(plotDSig, dsig)   
    
    
    plotNlatt.set_ylabel("$N$",fontsize=12)
    plotNlatt.plot(labels, allNlatt, label="$N_{lattice}$")
    plotNlatt.plot(labels, allNimp,label="$N_{imp}$")
    plotDFTandCorrMarkers(plotNlatt, allNlatt)
    plotDFTandCorrMarkers(plotNlatt, allNimp)
    plotNlatt.legend(loc='lower left')#bbox_to_anchor=(0.05, 1.2))
    
    
    plotEnergyImp.set_ylabel("$E_{imp}$",fontsize=12)
    plotEnergyImp.plot(labels, allEnergyImp)
    plotDFTandCorrMarkers(plotEnergyImp, allEnergyImp)
    
    plotEnergyBands.set_ylabel("$E_{loc}$",fontsize=12)
    plotEnergyBands.plot(labels, allEnergyBands)
    plotDFTandCorrMarkers(plotEnergyBands, allEnergyBands)
       
    plotNN.set_ylabel("N susc.",fontsize=12)
    plotNN.plot(labels, allNN)
    plotDFTandCorrMarkers(plotNN, allNN)
    
    plotChargeCvg.set_ylabel("charge conv.", fontsize=12)
    plotDFTandCorrMarkers(plotChargeCvg, cc)
    plotChargeCvg.set_ylim(0,0.1)
    
    plotQ.set_ylabel("quality", fontsize=12)
    plotDFTandCorrMarkers(plotQ, Q)
    
    
    
    plt.subplots_adjust(hspace = 0.4)
            
    lastPlot = plotChargeCvg
            
    for i,O in enumerate(obs):

        offset = 0
        if loader.isDMFT:
            offset = 2

        plot = plotObs[offset+i]
        plot.set_ylabel(O, fontsize=12)
        plotDFTandCorrMarkers(plot, allObs[O])
        lastPlot = plot
            
    #figManager = plt.get_current_fig_manager()
    #figManager.window.showMaximized()
    lastPlot.set_xlabel("iter/sub-iter")

class DashboardLoader(LightWeightLoader):

    class StuffToPlot:
        def __init__(self, ishell : int, shell : str, labels, xticks, SolverHistory):
            self.ishell = ishell
            self.shell = shell
            self.labels = labels
            self.xticks = xticks
            self.SolverHistory = SolverHistory

    def __init__(self, opts):
        
        all_shells = True if (opts.which_shell < 0 or not os.path.exists("./multi-impurity.h5")) else False

        if all_shells:
            opts.which_shell = 0
        which_shell = opts.which_shell

        AnalysisLoader.__init__(self, opts)

        self.LoadCalculation(loadRealSigma=False)

        self.SelectDMFTorGutz()

        self.shells = self.GetShells(all_shells, which_shell)

        self.histories = self.GetHistories()

    def GetStuffToPlot(self):

        toPlot = []
        for i, history in enumerate(self.histories):
            self.svs.which_shell = self.shells[i]
            shell = self.svs.GetSuffixOfImpurity()

            ht = dict()
            ticks = dict()
            
            for key in list(history.keys()):
                try:
                    if history[key] is None or len(history[key]) == 0:
                        break
                except:
                    break
                for subkey in history[key]:
                    
                    if self.isNewFormat:
                        src = f"./history.h5:/impurity{shell}/{key}/{subkey}/"
                    else:
                        src = f"./history.h5:/{key}/{subkey}/"

                    asfloat = float(key) + float(subkey)/1000.0  # assuming maximum of 1000 sub iterations
                    ht[asfloat] = src
                    ticks[asfloat] = float("%s.%s"% (key, subkey))

            
            def GetTick(lbl: float):
                ff = lbl - int(lbl)
                ticks = [0,1,5,15,25,35,45,55]
                for t in ticks:
                    if abs(ff - t*1.0/1000.0)<1.0e-10:
                        return str(int(lbl)) + "." + str(round(ff*1000.0))
                return ""
        

            labels = [x[1] for x in sorted(ticks.items())]
            xticks = [GetTick(x[0]) for x in sorted(ticks.items())]
            SolverHistory = [x[1] for x in sorted(ht.items())]
            
            toPlot.append(self.StuffToPlot(self.shells[i], shell, labels, xticks, SolverHistory))

        return toPlot


    def GetHistories(self):

        self.f = h5py.File("./history.h5",'r')

        if f"impurity{self.svs.GetSuffixOfImpurity()}" in self.f.keys():
            self.isNewFormat = True
        else:
            self.isNewFormat = False

        histories = []
        try:
            for s in self.shells:
                if self.isNewFormat:
                    self.which_shell = s
                    histories.append(self.f[f'impurity{self.svs.GetSuffixOfImpurity()}'])
                else:
                    histories.append(self.f["/"])
        except: 
            print("--> %s does not contain results (yet)"%method)
            exit(0) 
        
        return histories

    def Terminate(self):
        self.f.close()

    def GetShells(self, all_shells, which_shell):
        shells = [0]
        if os.path.exists("./multi-impurity.h5"):
            mi = MultiImpuritySupervisor()
            mi.load("./multi-impurity.h5:/")
                
            if all_shells:
                for i in range(1,mi.num_impurities):
                    shells.append(i)
            else:
                shells = [which_shell]
        return shells

    def SelectDMFTorGutz(self):
        if os.path.exists("./dmft.h5") or os.path.exists("./dmft0.h5"):
            self.isDMFT = True
            self.name = "DMFT"
        
        elif os.path.exists("./gutz.h5") or os.path.exists("./gutz0.h5"):
            self.isDMFT = False
            self.name = "Gutzwiller"

        else:
            print("No files to display - done")
            exit(0)

def SetupPlots(obs, loader, num_cols):

    num_plots = 8 + len(obs)
    if loader.isDMFT:
        num_plots += 2
    
    fig, axes = plt.subplots(num_plots, num_cols, sharex='col',
                             sharey='row', figsize=(8, 14))
    if num_cols == 1:
        axes = [ [a] for a in axes ]
    
    fig.canvas.mpl_connect('key_press_event', press)
    
    DCStr = "$V_{DC}$ varying"
    if loader.svs.double_counting_method == "fixed":
        DCStr = "$V_{nom. DC}$=%2.2feV" % loader.svs.nominal_dc
    
    KB = 6.333328e-6 *27.2107* 0.5 # collected from Kutepov's code, to match perfectly        
    if loader.svs.beta != None and loader.svs.beta > 0:
        T = round(1/loader.svs.beta / KB)
    else:
        # TODO: where do we get this form in this case?
        T = -1.0
    
    if os.path.exists("./README"):
        with open("README") as freadme:
            for line in freadme.readlines():
                if line.startswith('Q '):
                    runInfo = line.replace("Q ", "")
                elif line.startswith(' notes:'):
                    notes = line.replace(' notes:','')
    else:
        notes = "README file missing"
        runInfo = ""


    if Path("./TERMINATED").exists():
        notes += "\n" + open("./TERMINATED").read() + '\n'
    

    toLatex = None
    if hasattr(pymatgen, '__version__') and pymatgen.__version__.split(".")[0]<="2020":
        from pymatgen.util.string import latexify
        toLatex=latexify
    else:
        from pymatgen.util.string import Stringify
        class Latexify(str, Stringify):
            None
        def toLatex(s:str):
            return Latexify(s).to_latex_string


    ini = Input()
    ini.load("./ini.h5:/")
    fig.suptitle(f"%s %s\n%s   $\\beta$=%2.2f T=%2.2f   U=%2.2feV   J=%2.2feV   %s\n\n%s\n{notes}"% (  
                    loader.name,
                    os.path.abspath(".").split("/")[-1],
                    toLatex(ini.allfile[1:]), loader.svs.beta, T, 
                    loader.svs.U, loader.svs.J, DCStr,
                    runInfo),
                 #fontweight='bold',
                 fontsize=10)


    return fig, axes, num_plots

def MainFunction():
    parser = ArgumentParserThatStoresArgv('dashboard',add_help=True, parents=[GetMPIProxyOptionsParser()])    

    parser.add_argument("-j", "--job-name", dest="job",
                      help="readable job name that can be looked up in qstat",
                      default=None)
        
    parser.add_argument("-A", "--all", dest="download_all",
                      action="store_true",
                      help="download all relevant files",
                      default=False)
        
    parser.add_argument("-S", "--skip-download", dest="skip_download",
                      action="store_true",
                      help="do not download any files from the node",
                      default=False)
    
    parser.add_argument("--stop", dest="abort_job",
                      action="store_true",
                      help="qdel the job, after deleting temporary files",
                      default=False)
    
    parser.add_argument("--discard", dest="discard",
                      action="store_true",
                      help="when deleting the job, also discard history and the lapw image",
                      default=False)
    
    parser.add_argument("--no-history", dest="history",
                      action="store_false",
                      help="do not download history.h5",
                      default=True)
    
    parser.add_argument("-l", "--login", dest="login",
                      action="store_true",
                      help="login to the node and jump to the directory",
                      default=False)
    
    parser.add_argument("-L", "---", "--track-log", dest="track_log",
                      action="store_true",
                      help="track the log file",
                      default=False)
    
    parser.add_argument("-v", "--vi-opts", dest="vi_opts_json",
                      action="store_true",
                      help="vi the options file",
                      default=False)
    
    parser.add_argument("--upload", dest="update",
                      action="store_true",
                      help="upload files to the node (see files_to_upload)",
                      default=False)
    
    parser.add_argument("-f", dest="files_to_upload", nargs="*",
                        help='''list of files to upload to the node. 
                                relevant only if in update mode, default is just the options.json file''',
                        default=["opts.json"],
                        type=str)
        
    parser.add_argument("-n", "--num-iters", dest="num_iters", 
                     default=100, type=int,
                     help="number of last iterations to show, 0 - show all")

    parser.add_argument("--observables", dest="observables",
                      default="Mz",
                      help="""if empty, no observables are measured after CTQMC. 
                              otherwise, a comma-separated list of observables.
                              The set of observables are: 
                                 "Lx","Ly","Lz","L2",
                                 "Sx","Sy","Sz","S2",
                                 "Jx","Jy","Jz","J2", 
                                 "Mx","My","Mz","M2"
                           """) 
 
    parser.add_argument("-R", "--recursive", dest="recursive",
                      action="store_true",
                      help="copy directories recursively (for sets)",
                      default=False)
    
    parser.add_argument("-O",
                        dest="copy_overlaps",
                        default=False,
                        action="store_true",
                        help = "copy overlaps file from the server")
    
    parser.add_argument("--monitor-seconds", dest="monitor_seconds", 
                     default=60.0, type=float,
                     help="number seconds to wait in the monitoring")


    # positional parameter for the directories to be dashboarded. "." by default
    parser.add_argument("dirs", nargs="*", default=".", help="subdirectories to look at")
                                        
    parser.add_argument("--which-shell", dest="which_shell", default=-1, type=int,
                    help="which shell (index) to project to, in order to build the correlated problem")

    matplotlib.use("TkAgg")
    opts = parser.parse_args(sys.argv[1:])
    
    current_dir = os.path.abspath(".")
    if current_dir.find("/snapshot") ==-1 and not opts.skip_download:
        print("can only run D in a snapshot (sub) directory")
        exit(1)

    method = ""
    if not opts.skip_download:
        method = CommunicateWithNode(opts)
        if method is None or opts.download_all:
            exit(0) 
    
    
    #if opts.abort_job or not Path("./ini.h5").exists():
    #    exit(0)

    loader = DashboardLoader(opts)
    cwd = Path.cwd().absolute()
    
    dirs = []
    for g in opts.dirs:
        if g.find('*') != -1:
            dirs += Path().glob(g)
        else:
            dirs.append(Path(g))
        
    obs = opts.observables.split(",")
    if len(obs)==1 and obs[0] == "":
        obs = [] 
        
    fig, axes, num_plots = SetupPlots(obs, loader, len(dirs))
    loader.Terminate()
             
    def generate_plot(dir, idir, plot = True): 
        if Path("./ini.h5").exists():
            if plot:
                loader = DashboardLoader(opts)                 
                axl = [axes[iplot][idir] for iplot in range(num_plots)]
                for stuffToPlot in loader.GetStuffToPlot():   
                    DoPlots(opts, loader, stuffToPlot, obs, *axl)
            
                loader.Terminate()

        else:
            print(f"No ini in directory {os.getcwd()}")

    DoDownload = False
    closed = False
    Continue = False
        
    def handle_key(event):
        nonlocal Continue, closed
        event.key = str(event.key)
        if event.key in "Uu":
            Continue = True
            return False
        if event.key in "Dd":
            opts.download_all = True
            opts.copy_overlaps = True
            Continue = True
            print("flapw_image and overlaps will be downloaded next time")
            return False
        if event.key in "+":
            opts.monitor_seconds *= 1.2
            Continue = True
            return False
        if event.key in "-":
            opts.monitor_seconds *= 0.7
            Continue = True
            return False
     
    def handle_close(event):
        nonlocal Continue, closed
        Continue = True
        closed = True
        return False
            
    fig.canvas.mpl_connect("key_press_event", handle_key)
    fig.canvas.mpl_connect("close_event", handle_close)
    
    def BlockUntilTimeoutOrContinueKey():
        nonlocal Continue, closed
        Continue = False
        while True:
            ret = plt.waitforbuttonpress(opts.monitor_seconds)
            if ret: # process the event if it is a key press
                fig.canvas.flush_events()
            if ret is None or ret and Continue: # timeout, or u/d key pressed
                break
        
    while opts.monitor_seconds > 0:
        for idir, dir in enumerate(dirs):
            generate_plot(dir, idir)
            
        opts.download_all = False
        opts.copy_overlaps = False
            
        plt.plot(block=False)
        
        #plt.pause(opts.monitor_seconds)

        BlockUntilTimeoutOrContinueKey()
    
        if closed:
            break

        Store.Singleton().ClearAll()
        CommunicateWithNode(opts)
        
        for iplot in range(num_plots):
            axes[iplot][idir].cla()
    
    
    

if __name__ == '__main__':
    MainFunction()