'''
Created on Aug 18, 2018

@author: adler
'''
from os.path import exists
import sys
import argparse
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.options import RefreshOptions
from argparse import Namespace
from portobello.bus.persistence import Persistent
from argparse import ArgumentDefaultsHelpFormatter
from portobello.bus.mpi import GetMPIProxyOptionsParser
import gc


Ry2eV = 27.2107*0.5  # from Kutepov, don't change unless it changes there


def SelfEnergyLocation(mpi):
    return f"./self-energy-{mpi.GetRank()}.core.h5:/info/" 

def FindCorrelatedMethod(opts, mpi):
    if exists('./multi-impurity.h5'):
        if mpi.IsMaster(): print(" - aggregating multiple impurities")
        from portobello.rhobusta.multi_impurity import MultiImpurityEmbedder
        return MultiImpurityEmbedder(opts=opts)
    elif opts.plus_sigma and exists('./dmft.h5'):
        # this prevents a cyclic dependency on loading this file from Rhobusta
        if mpi.IsMaster(): print(" - self energy from DMFT")
        from portobello.rhobusta.dmft import DMFTOrchastrator
        
        # opts are None, since this object already has to exist with known options
        return DMFTOrchastrator(opts=opts)
    elif opts.gutz and exists('./gutz.h5'):
        if mpi.IsMaster(): print(" - self energy: lambda, R from Gutzwiller")
        from portobello.rhobusta.gutzwiller import Gutzwiller
        return Gutzwiller(opts, for_dft=True)
    else:
        print("Error: cannot find correlated method file, using plain DFT.")
        # this just continues to regular DFT (Sigma = 0)
        return None
    
# this is the subset of options of rhobusta which  control dft+g 
# This hardcoded list is hard to implement abstractly - there is not interface
# to introspect an arg parser to create a subset
def DFTPlusSigmaWorkerOptionsParser(add_help=False, private=False):
        parents = []
        if private: parents = [GetMPIProxyOptionsParser()]

        parser = ArgumentParserThatStoresArgv('rhobusta', 
                                parents=parents,
                                add_help=add_help ,formatter_class=ArgumentDefaultsHelpFormatter)
           
        cp = parser.add_argument_group(title="correlations", description="DFT+X parameters")
              
        cg = cp.add_mutually_exclusive_group()
        
        cg.add_argument("--plus-sigma", dest="plus_sigma",
                          action="store_true",
                          default=False,
                          help="run DFT+Sigma, not just DFT. If self-energy is not present, it is assumed 0") 
          
        cg.add_argument("-G", "--with-gutz-density", dest="gutz", 
                          action="store_true",
                          help="run DFT+G, that is, DFT with a present Gutzwiller solution (consisting of R and \lambda)",
                          default=False)
                 
        # this is a technical flag, which is on when running a full-greens function case
        cp.add_argument("--with-full-green",
                        dest="with_full_green",
                        action="store_true", default=False, help=argparse.SUPPRESS)
        
        # this is not exactly the same thing as in DMFT cutoff, but it is the cutoff for exact
        # frequencies in a similar way, but for the summation of Gn in the DFT+G code,
        # and therefore it should be around the same value
        cp.add_argument("--matsubara-exact-energy-limit", dest="matsubara_exact_energy_limit", default=10.0, type=float,
                          help="energy limit for exact self-energy frequency for the summation algorithm [eV]")


        cg.add_argument("--which-shell", dest="which_shell", default=-1, 
                    help="which shell (index) to project to, in order to build the correlated problem")

        return parser


def EmbedSigma():    
    opts,_args = DFTPlusSigmaWorkerOptionsParser(private=True).parse_known_args(sys.argv[1:])
    RefreshOptions(opts,False) # this is a workaround - fix it. Should not be necessary, but we use it to fill Temperature
    plugin = DFTPlugin.Instance()
    cm = FindCorrelatedMethod(opts, plugin.mpi)
    if cm is not None:
        # update the DFT convergence informationi if available
        cvg = plugin.GetConvergence()
        if cvg is not None: cm.cvg = cvg
        cm.EmbedSigma(SelfEnergyLocation(plugin.mpi), ignore_state=True)
        del cm
        gc.collect()
    else: 
        if plugin.mpi.IsMaster(): print(" - No correlated method found - no embedding will be done (check existence of dmft.h5 or gutz.h5)")

   
def ContinueWithLocalSelfEnergy(rhobusta, restart, hasSelfEnergy=False, hasDensity=False):
    if hasSelfEnergy:
        DFTPlugin.SetGw()

    i = 0
    opts : Namespace = rhobusta.opts
        
    plugin = DFTPlugin.Instance(restart=restart, mpi=rhobusta)
        
    while True:
        RefreshOptions(opts,verbose=rhobusta.IsMaster())
        num_iters=10
        if hasattr(opts, 'num_charge_iterations'):
            num_iters = opts.num_charge_iterations
        if hasSelfEnergy and hasattr(opts, 'dft_plus_sigma_iters'):
            num_iters = opts.dft_plus_sigma_iters
        elif hasDensity and hasattr(opts, 'dft_plus_g_iters'):
            num_iters = opts.dft_plus_g_iters
                        
        if i>= num_iters: 
            last = 1
            break
        else:
            # we don't save correlated mu other than in the DMFT object
            last = int(i==(num_iters-1))

        cvg = plugin.GetConvergence()
 
        admix = getattr(opts, "admix", 0.1)
        search_mu = int(getattr(opts, "search_mu", True))
        
        charge_conversion_goal = getattr(opts, "charge_conversion_goal", 1.0e-7)
            
        # on the first iteration we don't have the correct charge conversion numbers
        if i > 0 and abs(cvg.charge_convergence) < charge_conversion_goal:
            if rhobusta.IsMaster():
                print("charge conversion goal of %5.12e reached, stopping DFT." % charge_conversion_goal)
            last = 1

        #print(f"last is: {last}, rank={get_comm_world().Get_rank()}")
                    
        update_lapw =  1
        # also make sure the last one is not updated - that would be a bug
        if last==1: 
            update_lapw = 0
        
        sel = ""
        if hasSelfEnergy:
            sel = SelfEnergyLocation(rhobusta)

        plugin.dft.DFTPlusSigmaOneIteration(
            "", 
            sel,
            checkpoint=last,
            search_mu=search_mu,
            update_lapw=update_lapw,    
            admix=admix, 
            save_potentials=0,
            mu_search_variant=opts.mu_search_variant)  # Never save potentials (it has side effects?)

        sys.stderr.flush()
        sys.stdout.flush()
        if last==1:
            break
        
        i+=1
    

if __name__ == '__main__':
    pass
