#!/usr/bin/env python3
'''
Created on Apr 26, 2021

@author: adler

This program is an example of the contributor-organizer framework.
The idea is to run DFT with multiple lattice constants and find the optimal one (with lowest energy.)

'''

from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.rhobusta import RhobustaOptionsParser, DFT
from argparse import ArgumentDefaultsHelpFormatter
import sys
from pathlib import Path
from portobello.jobs.Organizer import Organizer

parser = ArgumentParserThatStoresArgv('lattice constant', add_help=True,
                        parents=[RhobustaOptionsParser(private=True)],
                        formatter_class=ArgumentDefaultsHelpFormatter)    
    
#argcomplete.autocomplete(parser)
parser.add_argument("--max-change", dest="max_change", 
                    default=5.0, type=float,
                    help="size of maximal volume change in percent")

parser.add_argument("--num-ticks", dest="num_ticks", 
                    default=10, type=int,
                    help="number of ticks for change")

opts = parser.parse_args(sys.argv[1:])
opts.Temperature = 50
    
# start listening immediately
org = Organizer(shareddir=str(Path.cwd()))

input_file = opts.input_file

for tick in range(-opts.num_ticks, opts.num_ticks+1): 
    change = tick/ opts.num_ticks * opts.max_change / 100.0
    idx = tick + opts.num_ticks
    run_dir = Path(f'./run-{idx}')

    opts.input_file=f"../{input_file}"
    opts.a = 1.0 + change
    opts.finalize=True
    
    print(f"**** Added work: {change} tick={tick}")
    org.AddWork(
            workdir=str(run_dir),
            module="portobello.rhobusta.rhobusta", 
            function="DFT",  
            args={ 'args':[],
                  'opts':opts})

org.Wait()

print("------------- ALL DFT work finished ---------------")

# now we analyze the results
