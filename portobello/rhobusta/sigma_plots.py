#!/usr/bin/env python3

'''
Created on Aug 21, 2018

@author: adler
'''

from argparse import ArgumentDefaultsHelpFormatter, Namespace
import os
import warnings

import json

from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.rhobusta.ctqmc import CTQMCOptionsParser, MakeGreensFunctionForImp, ResultsParser, Serializer
from portobello.rhobusta.observables import GetLabelsForPlotting
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
warnings.filterwarnings("ignore")

import matplotlib.pyplot as plt
from matplotlib import cm
from portobello.generated.dmft import DMFTState, ImpurityProblem
from portobello.rhobusta.PEScfEngine import PEScfEngine
from portobello.rhobusta.models.PEScfEngine import PEScfEngineForModels
from optparse import OptionParser
import sys
import numpy as np

class SigmaPlots(DMFTState):
    '''
    classdocs
    '''
    def __init__(self, version, opts):
        '''
        Constructor
        '''
        DMFTState.__init__(self)
        
        if version != '':
            nums = version.split(".")
            maj = int(nums[0])
            sub = int(nums[1])
            try:
                self.load(f"./dmft.h5:/history/{maj}/{sub}/")
            except:
                self.load(f"./history.h5:/impurity/{maj}/{sub}/")
                
        else:
            self.load("./dmft.h5:/")
        
        
        self.orbs = PEScfEngine.GetRepOrbitals(self.rep) # orbital indices
       
        if self.num_si == 2:
            self.orbs += [o+self.dim for o in self.orbs]

        odef = SelectedOrbitals()
        odef.load("./projector.h5:/def/")

        if opts.model:
            vals, orbs = PEScfEngineForModels.GetLabelsForPlotting(self)
        else:
            self.corrSubshell = odef.shells[opts.which_shell]
            self.orbBandProj = Namespace()
            self.orbBandProj.orbsDef = odef

            vals, orbs = PEScfEngine.GetLabelsForPlotting(self)
        
        self.orb_names = [f"${orb}$" for orb in vals[0]]
        
        imp = ImpurityProblem()
        imp.load("./dmft.h5:/impurity/")
        err_path = imp.os_dir+"params.err.json"
        self.error_exists=os.path.exists(err_path)
        if(self.error_exists):
            with open(err_path) as f:
                self.err = json.load(f)["partition"]

            ser = Serializer(imp)
            rp = ResultsParser(imp, ser, opts)

            actualErrLen = len( rp.err[opts.self_energy][ser.orbitalLabel[(0,0)]]["function"]["real"])
            error = MakeGreensFunctionForImp(imp, actualErrLen, imp.beta)
            errorParser = rp.FunctionAndSource(error, self.err, opts.self_energy)
            self.error = rp.ParseFunction(errorParser, ser, imp, opts)

    def PlotCurrentSig(self, axr, axi ,sig, num_freqs, legend=False, add_errorbars = False):
        
        extra = True
                                            
        if num_freqs >= sig.num_omega:
            num_freqs = sig.num_omega
            extra = False
        
        if self.error_exists and add_errorbars:
            num_freqs = min(num_freqs,self.error.function.num_omega)
            
        colors = cm.rainbow(np.linspace(0, 1, len(self.orbs)))
        for i, (iorb, jorb) in enumerate(self.orbs):
            
            dash = "-" if iorb == jorb else ":"
            label = f"{self.orb_names[jorb]}-{self.orb_names[iorb]}" if iorb != jorb else self.orb_names[iorb]
            
            x = sig.omega[:num_freqs]
            y = sig.M[:num_freqs,iorb%self.dim,jorb%self.dim, iorb//self.dim]
            if self.error_exists and add_errorbars:
                yerr = self.error.function.M[:num_freqs,iorb%self.dim,jorb%self.dim, iorb//self.dim]

                RE = np.abs(yerr)/np.abs(y)
                if (RE > 10).all():
                    axr.errorbar(x,np.real(y),yerr=np.real(yerr),
                        linestyle=dash, color = colors[i], 
                        label=label)
                    axi.errorbar(x,np.imag(y),yerr=np.imag(yerr),
                        linestyle=dash, color = colors[i], 
                        label=label)

            else:
                axr.plot(x,np.real(y),
                    linestyle=dash, color = colors[i],
                    label=label)
                axi.plot(x,np.imag(y),
                    linestyle=dash, color = colors[i],
                    label=label)


            if legend:
                axr.legend(bbox_to_anchor=(1.0, 1.2))
            axr.set(xlabel='i$\omega$ [eV]', ylabel="real")
                  


            axi.set(xlabel='i$\omega$ [eV]', ylabel="imag")


        if extra:
            None

    
        axr.label_outer()
        axi.label_outer()
        
        
        
        
        
def main():
    
    parser = ArgumentParserThatStoresArgv('ctqmc', add_help=True , parents=[CTQMCOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)   
        
    parser.add_argument("-v", "--versions", dest="versions", default='',
                      type=None, help="version list, such as 1.1, 2.4 etc. separated by ,")
 
    parser.add_argument("-F", "--num_freqs_factor", dest="F", type=float, default=1.0)
    
    parser.add_argument("--which-shell", dest="which_shell", default=0, type=int,
                    help="which shell (index) to project to, in order to build the correlated problem")
    
    parser.add_argument("--model", dest="model", default=False, action="store_true",
                    help="model, not material")

    opts = parser.parse_args(sys.argv[1:])
    
    versions = opts.versions.split(",")

    _f, axis = plt.subplots(6, len(versions), sharex='col', sharey='row',  figsize=(12, 12))

    for i, version in enumerate(versions):
       
        if len(versions) == 1:
            axr = axis[0]
            axi = axis[1]
            bxr = axis[2]
            bxi = axis[3]
            cxr = axis[4]
            cxi = axis[5]
        else:
            axr = axis[0,i]
            axi = axis[1,i] 
            bxr = axis[2,i]
            bxi = axis[3,i] 
            cxr = axis[4,i]
            cxi = axis[5,i] 
        
        if version != '':
            axr.set_title("$\Sigma_{local} [\#{%s}]$"%(version),fontsize=20)
            bxr.set_title("$\Delta [\#{%s}]$"%(version),fontsize=20)
            cxr.set_title("$G_{imp} [\#{%s}]$"%(version),fontsize=20)
        else:
            axr.set_title("$\Sigma_{local} [{current}]$",fontsize=20)
            bxr.set_title("$\Delta [{current}]$",fontsize=20)
            cxr.set_title("$G_{imp} [{current}]$",fontsize=20)
            
        assert(opts.F > 0), "frequency factor should be positive"
        
        sp = SigmaPlots(version=version, opts = opts)
        num_freqs = int(opts.F* sp.num_freqs)
        sp.PlotCurrentSig(axr, axi, sp.sig, num_freqs, legend=(i==len(versions)-1), add_errorbars = True)
        sp.PlotCurrentSig(bxr, bxi, sp.hyb, num_freqs)
        sp.PlotCurrentSig(cxr, cxi, sp.GImp, num_freqs)
        
    plt.subplots_adjust(hspace = 0.4)
    plt.show()
        
if __name__ == '__main__':
    main()
    
        

