#!/usr/bin/env python3

'''
Created on Jul 12, 2018

@author: adler@physics.rutgers.edu
'''
from genericpath import exists
from optparse import SUPPRESS_HELP
from numpy.core.fromnumeric import partition
from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer

from portobello.bus.fortran import Fortran
import sys
import logging
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.generated.dmft import ImpurityProblem, ImpurityMeasurement,\
    DMFTState
import numpy as np
import os
from portobello.bus.persistence import PersistentObjEncoder
import json
from portobello.generated.self_energies import LocalOmegaFunction
from portobello.generated.CTQMC_internal import ComplexArray, Parameters, BasisStruct, Hyb, ComplexSquareMatrix,\
    CTQMCObservable
from portobello.generated.PEScf import Histogram
from itertools import product
from portobello.generated import FlapwMBPT_interface
from argparse import ArgumentDefaultsHelpFormatter, Namespace
from portobello.rhobusta.PEScfEngine import PEScfEngine
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.observables import GetAllObservables
from builtins import enumerate
from math import ceil
from pathlib import Path

from portobello.jobs.Organizer import with_organizer
from portobello.rhobusta.g.utils import U_J_to_radial_integrals
import time

PARAMS_ASCII = "params".encode('ascii')

def ToJsonComplexMatrix(matrix):
    tm = ComplexSquareMatrix(dim=matrix.shape[0])
    tm.allocate()
    tm.real = np.array(np.real(matrix), dtype=np.float64)
    tm.imag = np.array(np.imag(matrix), dtype=np.float64)
    del tm.dim
    return tm
                
class Serializer(object):

    def __init__(self, imp: ImpurityProblem):
        self.imp = imp
        self.rep = self.imp.subshell.rep
     
        self.spinful_dim = 2*imp.dim
        if imp.nrel == 2:
            # relativistic mode has full dimension
            self.spinful_dim = imp.dim
        
        self.HM =  np.chararray((self.spinful_dim,self.spinful_dim), itemsize=5, unicode=True) # make it possible to have up to 1000 orbs + "_" + "c"
        # filter is used to nullify the entries that are supposed to be 0
        # so that we don't have very small non-zero values where rep is zero.
        self.filter = np.zeros_like(self.rep)
        self.hasOffDiag = False
        
        uniqueOrbitals = {}
        self.orbitals = []
        self.orbitalLabel = {}
        for i,j in product(range(imp.dim),repeat=2):
            if self.rep[i,j] == 0: continue # identically zero
            label = str(imp.subshell.rep[i,j])
            if i > j: self.hasOffDiag = True
            
            self.orbitalLabel[(i,j)] = label
            self.HM[i,j] = label
            if not imp.nrel == 2:
                if imp.num_si == 2:
                    self.HM[imp.dim+i, imp.dim+j] = label + "_"
                else:
                    self.HM[imp.dim+i, imp.dim+j] = label
            self.filter[i,j] = 1.0 
            # note that the down labels are handled as a special case (not in the list of orbitals)
            if label not in uniqueOrbitals:
                uniqueOrbitals[label] = label
                self.orbitals.append(((i,j), label))
                
        self.num_orbs = len(uniqueOrbitals.keys())
        
    def dim(self):
        return self.spinful_dim
    
    # build a python object for Hyb, which will be serialized as jason
    # this is described by Hyb (where the schema matches the json convention)
    def GetHyb(self):
        orbhyb = {}
        for (i,j), orb in self.orbitals:
            hyb = Hyb(beta=self.imp.beta, num=self.imp.hyb.num_omega)
            hyb.allocate()
            hyb.real[:] = self.imp.hyb.M[:, i, j, 0].real
            hyb.imag[:] = self.imp.hyb.M[:, i, j, 0].imag
            del hyb.num   # this one is not understood by CTQMC
            orbhyb[orb] = hyb
            # if has 2 spins, create hyb's for the spin downs
            if self.imp.num_si == 2:
                hyb = Hyb(beta=self.imp.beta, num=self.imp.hyb.num_omega)
                hyb.allocate()
                hyb.real[:] = self.imp.hyb.M[:, i, j, 1].real
                hyb.imag[:] = self.imp.hyb.M[:, i, j, 1].imag
                del hyb.num   # this one is not understood by CTQMC
                # add the corresponding down orbital
                orbhyb[orb+"_"] = hyb
                
        return orbhyb
    
    def GetHlocMatrix(self):
        # multiply by filter to sanitze values. This is important
        # so that the irrep analyzer in DMFT finds the correct invariant spaces.
        sanE = np.multiply(self.filter, self.imp.E[:,:,0]) # sanitized E matrix
        if self.imp.nrel == 2:
            return sanE # don't have to add anything
        newMat = np.zeros((self.spinful_dim,self.spinful_dim), dtype=np.complex128)
        dim = self.imp.dim
        newMat[:dim:, :dim] = sanE
        if self.imp.num_si < 2:
            newMat[dim:, dim:]= sanE
        else:
            newMat[dim:, dim:] = np.multiply(self.filter, self.imp.E[:,:,1])
        return newMat
    
    def GetHloc(self):
        return ToJsonComplexMatrix(self.GetHlocMatrix())

    def GetHybMatrix(self):
        return self.HM
    
    def GetFirstOrbitalLabel(self):
        return next(iter(self.orbitalLabel.values()))
    

def CTQMCOnInputFile(mpi_proxy : MPIContainer):
    sys.stdout.flush()
    fortran = Fortran.Instance()
    MPI = mpi_proxy.GetMPIInterface() # initialize the mpi for ctqmc too
    matdelab_ctqmc = fortran.GetCFunction("CTQMCDriverStart")
    matdelab_ctqmc(PARAMS_ASCII)
    if MPI is not None:
        mpi_proxy.Terminate()

def EvalSim(mpi_proxy : MPIContainer):
    sys.stdout.flush()
    fortran = Fortran.Instance()
    MPI = mpi_proxy.GetMPIInterface() # initialize the mpi for ctqmc too
    evalsim = fortran.GetCFunction("EvalSim_Main")
    evalsim(PARAMS_ASCII)
    if MPI is not None:
        MPI.COMM_WORLD.Barrier()
        mpi_proxy.Terminate()


class CTQMCSupervisor(MPIContainer):
    def __init__(self, opts):
        MPIContainer.__init__(self, opts)
        if opts.worker:
            self.InitializeMPI()

    def WorkerArgs(self):
        return ["-m", "portobello.rhobusta.ctqmc", "-f", "--worker", "--eval-sim", "--mpi-workers", f"{self.num_workers}"] 

def StartComCTQMC(opts):
    if not opts.analysis_only:
        sup = CTQMCSupervisor(opts)
        sup.Run()

def GetMeasurement(imp):
    IM = ImpurityMeasurement()
    IM.load(imp.self_path + "/measurement/")
    return IM
    
def MakeGreensFunctionForImp(imp, num_freqs, beta):
    gf = LocalOmegaFunction(num_omega=num_freqs, 
                            num_si=imp.num_si,
                            num_orbs=imp.dim)
    gf.allocate()
    gf.omega[:] = [(2*i+1)*np.pi/beta for i in range(0,num_freqs)]
    return gf

#MatrixFunction because we will need Tensor functions at some point
#this is for Chi_iijj measured in partition space
def MakeSusceptibilityMatrixFunctionForImp(spinful_dim, num_freqs, beta):
    gf = LocalOmegaFunction(num_omega=num_freqs, 
                            num_si=1, #we do not have a block diagonal matrix for susceptibilities
                            num_orbs=spinful_dim)
    gf.allocate()
    gf.omega[:] = [2*i*np.pi/beta for i in range(0,num_freqs)]
    return gf
 
def FillHistogram(obs, spinful_dim):
    hist = Histogram(num_configs = 2**spinful_dim)
    hist.allocate()
    ptot=0.0
    totConfigs = 0
    
    sectors = {}
    
    for N,E,*_rest,sectorid,p in obs["probabilities"]["quantum numbers"]:
        sectors[sectorid] = (N, E, p)
    
    for *bits, sectorid, p in obs["probabilities"]["occupation numbers"]:
        sector = sectors[sectorid]
        
        # compute the config number from the bit representation
        config  = 0
        bb = 1
        lb = len(bits)
        for j in range(lb):
            assert(bits[j] in [0,1]), bits[j]
            config += bb * bits[j]
            bb *= 2
            
        hist.N[config] = sector[0]
        hist.p[config] = float(p[0])
        ptot += float(p[0])
        hist.energy[config] = float(sector[1])
        totConfigs += 1

    print("- read %d values from the histogram with total p=%f" %(totConfigs,ptot))
        
    return hist

def GetRunTimes(imp      : ImpurityProblem, 
                ser      : Serializer, 
                opts     : Namespace, 
                sign     : float, 
                nworkers : int,
                cvg      : FlapwMBPT_interface.Convergence):
    orbitals_time_factor = 1.0 #len(uniqueOrbitals.keys()) * imp.num_si / 2.0
    if imp.nrel == 2:
        orbitals_time_factor = 1.6
    T_factor = 1.0
    if imp.beta > 5.0:
        T_factor = (imp.beta - 5.0) / 8.0
    OffDiagFactor = 1.0 
    if ser.hasOffDiag:
        OffDiagFactor = 2.0
    thermalizationShortening = 1.0
    signFactor = 1.0
    if opts.adjust_time_to_sign: 
        if abs(sign) < 1.0e-18:
            signFactor = 1.0 # it is zero artificially, start of program
        else:
            signFactor = min(10.0, 1.0/sign) # at most 10 times
    
    
    if cvg is not None and cvg.charge_convergence !=0.0 and os.path.exists("./Impurity/params.meas.json"): # it is not first iteration
        # this shortens thermalization by a factor of cvg.charge_convergence*1000.0,
        # which becomes 0  as the charge converges
        thermalizationShortening = min(cvg.charge_convergence*1000.0,1.0)
    
    thermalisation_time = max(1.0, 
        int(5.0 * orbitals_time_factor * T_factor) * \
        OffDiagFactor * thermalizationShortening) 
    measurement_time = int(max(20.0 / nworkers, 4) * orbitals_time_factor * T_factor) * \
        OffDiagFactor  * \
        signFactor
        
    if opts.time_measurement > 0:
        measurement_time = ceil(opts.time_measurement)
    if opts.time_thermalization > 0:
        thermalisation_time = ceil(opts.time_thermalization)
    return ceil(thermalisation_time), ceil(measurement_time)

class ParameterOrchestrator(Parameters):

    def __init__(self, imp : ImpurityProblem, opts : Namespace):
        Parameters.__init__(self, beta=imp.beta, mu=imp.mu, complex = True)

        print("- G measurement cutoff: %d [eV]" %(imp.max_sampling_energy))
                
        self.sim_per_device = opts.sim_per_device
        print("Running "+str(opts.sim_per_device)+" simulations per GPU")

        self.SetWormOptions(imp, opts)
        
        self.partition.occupation_susceptibility = opts.occupation_susceptibility
        self.partition.occupation_susceptibility_bulla = opts.occupation_susceptibility


    def FinishAndWrite(self, imp : ImpurityProblem, 
                            opts : Namespace, 
                            mpi_workers, 
                            cvg,
                            sign,
                            ser : Serializer):

        self.hloc.one_body = ser.GetHloc()
        

        if imp.two_body_size > 0:
            self.hloc.two_body = {
                                "imag" : list(np.imag(imp.two_body)),
                                "real" : list(np.real(imp.two_body)),
                                }
            del self.basis
        else:
            self.hloc.two_body.F0 = imp.F0
            self.hloc.two_body.F2 = imp.F2
            self.hloc.two_body.F4 = imp.F4
            self.hloc.two_body.F6 = imp.F6
                    
            self.hloc.two_body.approximation = imp.hamiltonian_approximation 
            
            # this sets up a problem with product basis
            subshell = 'd'
            if imp.subshell.l == 3:
                subshell = 'f'
            self.basis = BasisStruct(dim=ser.dim(), orbitals=subshell)
            self.basis.allocate()
        
            def CreateTransformation():
                """ take the transformation matrix from the shell. Usually it should be identity
                    unless we are not using the native basis as above 
                """
                if imp.nrel < 2:
                    ttype = 'product'
                    matrix = np.block([[np.transpose(imp.subshell.basis), np.zeros((imp.dim,imp.dim))],
                                    [np.zeros((imp.dim,imp.dim)), np.transpose(imp.subshell.basis)]])
                else:
                    ttype = 'coupled'
                    matrix = np.transpose(imp.subshell.basis)
                    
                return ttype, ToJsonComplexMatrix(matrix)
                            
            self.basis.type, self.basis.transformation = CreateTransformation() 
            
        self.hybridisation.dim = ser.dim()
        self.hybridisation.allocate()
        self.hybridisation.matrix = ser.GetHybMatrix()
        
        if (opts.sim_per_device):
            self.partition.density_matrix_precise = False #GPU cannot handle density_matrix_precise=true
        else:
            self.partition.density_matrix_precise =  opts.density_matrix_precise
        self.partition.green_bulla = opts.green_bulla
        self.partition.green_matsubara_cutoff = imp.max_sampling_energy
        self.partition.susceptibility_cutoff = imp.max_sampling_energy
         
        self.partition.observables = {}
        #self.partition.observables["J2"] = {}
        self.partition.probabilities = ["N", "energy", "label"]
            
        requestedOL = opts.observables.split(",")
        allOL = "all" in requestedOL
            
        try: #for non-model calculations | TODO: deal with this better
            proj_location = "./projector.h5:/def/"
            proj = SelectedOrbitals()
            proj.load(proj_location)

            # calculate the quantum numbers in this basis / transformation
            OL = GetAllObservables(imp.subshell, subspace_expr=proj.subspace_expr)
            labels = OL.GetLabels()
            for i,op in enumerate(OL.GetOperators()):
                label = labels[i]
                if not allOL and label not in requestedOL:
                    continue
                O = CTQMCObservable()
                O.one_body = ToJsonComplexMatrix(op)
                self.partition.observables[label] = O
                self.partition.probabilities.append(label)

            if opts.ctqmc_qns != "":
                self.partition.quantum_numbers = {}
                requested_qns = opts.ctqmc_qns.split(",")
                for i,op in enumerate(OL.GetOperators()):
                    label = labels[i]
                    if label not in requested_qns:
                        continue
                    
                    self.partition.quantum_numbers[label] = np.array(np.round( np.real( np.diag(op) ), 4), dtype=float).tolist()[0]
        except:
            self.partition.quantum_numbers = {}


        self.partition.quantum_number_susceptibility = opts.qn_susceptibility
                    
        orbhyb = ser.GetHyb() 
        
        nworkers = mpi_workers
        if mpi_workers == 0:
            nworkers = 1
        
        if mpi_workers == 1:
            self.error = "serial"
        self.all_errors=opts.all_errors            

        if opts.measurement_steps > 0 or opts.thermalization_steps >= 0:
            self.measurement_steps = max(500,opts.measurement_steps) #500 is minimum to get measurements with defaults
            self.thermalisation_steps = max(0,opts.thermalization_steps)
            del self.thermalisation_time
            del self.measurement_time
            print("- Thermalization steps: %g steps, measurement steps: %g steps" % (self.thermalisation_steps, self.measurement_steps)) 
        else:
            self.thermalisation_time, self.measurement_time = GetRunTimes(imp, ser, opts, sign, nworkers, cvg) 
            del self.thermalisation_steps
            del self.measurement_steps
            print("- Thermalization time: %f min., measurement time: %f min." % (self.thermalisation_time, self.measurement_time)) 
        
        self.DeleteFields(opts)
        
        if opts.interaction_truncation > 1.0e-8:
            self.interaction_truncation = opts.interaction_truncation
        else:
            del self.interaction_truncation
            
        if not os.path.exists(imp.os_dir):
            os.makedirs(imp.os_dir)
        
        def WriteToFile(prefix, content):
            fp = open(os.path.join(imp.os_dir,prefix+".json"), "w")
            fp.write(content)
            fp.flush()
            fp.close()

        WriteToFile("params", self.tojson())
        WriteToFile("Hyb", json.dumps(orbhyb, cls=PersistentObjEncoder, indent=4))
        sys.stdout.flush()

    def SetWormOptions(self, imp : ImpurityProblem, opts : Namespace):
        meas=""
        if (opts.green_bulla): #If you don't want partition space bulla, you probably don't want worm space bulla
            meas="imprsum"

        patrick_cutoff = max(int(imp.beta * imp.max_sampling_energy / (2 * np.pi)), 1)
        self.green.cutoff = patrick_cutoff

        possible_worms = ["green", "susc ph", "susc pp", "hedin ph", "hedin pp", "vertex"]
        if not opts.worms == "all":
            worms = opts.worms.split(",")
            
            for worm in possible_worms:
                worm_ = worm.replace(" ", "_")
                if worm in worms:
                    w = getattr(self,worm_)
                    w.diagonal = opts.diagonal_worms
                    w.meas = [meas]
                
                else:
                    delattr(self,worm_)

    def DeleteFields(self, opts):
        # delete some fields that CTQMC doesn't know 
        del self.hybridisation.dim
        
        Nrange = PEScfEngine.GetNRange(opts.N0)
        if Nrange is None:
            del self.trunc_dim
        else:
            # the lower bound is not used, only the highest one
            self.trunc_dim = Nrange[-1]



class ResultsParser:

    class FunctionAndSource:
        """
        Source is the json dictionary from which function grabs
        sl is the slice or index of parsed source
        """
        def __init__(self, function : LocalOmegaFunction, source : dict, name : str, moments = 0, sl = None, partition_source = None):
            self.function = function
            self.source = source[name]
            self.name = name
            self.moments = moments

            self.is_omega_function = isinstance(function,LocalOmegaFunction)
            
            if sl is None:
                if self.is_omega_function:
                    sl = slice(0,function.num_omega)
                else:
                    sl = 0

            self.slice = sl

            if partition_source is not None:
                self.partition_source = partition_source[name]
            else:
                self.partition_source = partition_source

    EXP_HIST_LEN = 400

    def __init__(self, imp : ImpurityProblem, ser : Serializer, opts: Namespace):
        self.label = ser.GetFirstOrbitalLabel()
        
        ImpurityDir = Path(imp.os_dir)
        i=0
        while not (ImpurityDir / "params.obs.json").exists():
            if opts.post_process:
                break
            
            import time
            time.sleep(10)
            i+=1
            if i>24:
                raise Exception("params.obs.json was not produced")
                
        with open(ImpurityDir / "params.obs.json") as f:
            self.obs_all = json.load(f) # observables 
        self.obs = self.obs_all["partition"]

        if "green imprsum" in self.obs_all.keys():
            self.green_obs = self.obs_all["green imprsum"]
        else:
            self.green_obs = self.obs
   
        scalars = self.obs["scalar"]
        
        err_path = ImpurityDir / "params.err.json"
        error_exists=err_path.exists()
        if(error_exists):
            with open(err_path) as f:
                self.err = json.load(f)["partition"] # observable error

        covar_path = ImpurityDir / "params.covar.json"
        self.covar_exists=covar_path.exists()
        if(self.covar_exists):
            with open(covar_path) as f:
                self.covar = json.load(f)
            

        #We have a collection of functions, each with its own number of frequencies
        #We make them all before building our important objects -- the functions we need to parse (in a standard way)
        #And the Impurity measurement we will be storing all of these in
        self.num_freqs = self.CountFrequenciesInOutput()

        Sigma = MakeGreensFunctionForImp(imp, self.num_freqs, imp.beta)

        lastSamplingFreqIdx = len(list(filter(lambda a: a[1] < imp.max_sampling_energy, 
                                            enumerate(Sigma.omega))))
        self.num_x = lastSamplingFreqIdx

        GImp = MakeGreensFunctionForImp(imp, self.num_freqs, imp.beta)
        GAux =  MakeGreensFunctionForImp(imp, self.num_x, imp.beta)

        # make the errSig flexible enough - it is just what the length in the CTQMC output is,
        # we don't assume that it is equal to lastSamplingFreqIdx (although it should be)
        if error_exists:
            actualErrLen = len( self.err[opts.self_energy][ser.orbitalLabel[(0,0)]]["function"]["real"])
            errSig = MakeGreensFunctionForImp(imp, actualErrLen, imp.beta)
        else:
            errSig = MakeGreensFunctionForImp(imp, self.num_freqs, imp.beta)
            self.err = self.green_obs

        rhoImp = np.zeros((imp.dim, imp.dim, imp.num_si), np.complex128)


        self.functionsToParse = { "Sigma" : self.FunctionAndSource(Sigma, self.green_obs, opts.self_energy, moments = 2, partition_source=self.obs),
                            "errSig" : self.FunctionAndSource(errSig, self.err, opts.self_energy),
                            "GImp" : self.FunctionAndSource(GImp, self.green_obs, "green", moments = 3, partition_source=self.obs),
                            "GAux" : self.FunctionAndSource(GAux, self.obs, "aux green matsubara"),
                            "rhoImp" : self.FunctionAndSource(rhoImp, self.obs, "occupation")
                            }


        self.IM = ImpurityMeasurement(
            num_samples = self.EXP_HIST_LEN,
            dim=imp.dim,
            num_si=imp.num_si,
            nrel=imp.nrel,
            num_freqs = self.num_freqs,
            N=float(scalars["N"][0]),
            k=float(scalars["k"][0]),
            E=float(scalars["energy"][0]),
            NN=float(scalars["NN"][0]),
            lnZ=float(scalars["ln(atomic Z)"][0]),
            sign=float(self.obs['sign'][0]),
            num_orbs = ser.num_orbs,
            num_x = self.num_x
        )
        self.IM.allocate()


    def Parse(self, imp: ImpurityProblem, ser : Serializer, opts : Namespace, params : Parameters):
        # Grab histograms
        self.IM.hist = FillHistogram(self.obs, ser.dim())
        self.histogram = self.ReadRealFunction(self.obs["expansion histogram"])

        # Grab the covariance of the auxilliary green's function
        if self.covar_exists:
            for orb in range(ser.num_orbs):
                
                def SetCovar(l, si):
                    mout = self.ReadComplexFunction(self.covar["aux green matsubara"][l]["function"]) 
                    mdim = int(np.sqrt(mout.shape[0]))
                    assert(mdim >= self.num_x)

                    mout = np.reshape(mout, (mdim, mdim))[:self.num_x, :self.num_x]
                    self.IM.CoVariance[:,:,orb,si] = mout[:,:]
                
                (i_,j_), label = ser.orbitals[orb]
                SetCovar(label, 0)

                if imp.num_si == 2:
                    up = label + "_"
                    SetCovar(up, 1)

        # Grab all of the LocalOmegaFunctions
        for key in self.functionsToParse.keys():
            self.functionsToParse[key] = self.ParseFunction(self.functionsToParse[key], ser, imp, opts)

        Sigma = self.functionsToParse["Sigma"].function
        errSig = self.functionsToParse["errSig"].function

        # Grab the error
        RE = 0
        for (i,j), label in ser.orbitalLabel.items():

            numIndices = min(len(errSig.M[:,0,0,0]),self.num_x)
            relative_error = np.abs(errSig.M[:numIndices, i, j, 0])/np.abs(Sigma.M[:numIndices, i, j, 0])
            if opts.truncate_offdiags == 0 or i==j or (relative_error < opts.truncate_offdiags).all():

                RE = self.UpdateRE(RE,np.imag(errSig.M[:, i, j, 0]), np.imag(Sigma.M[:, i, j, 0]))
                if imp.num_si == 2:
                    RE = self.UpdateRE(RE,np.imag(errSig.M[:, i, j, 1]), np.imag(Sigma.M[:, i, j, 1]))

            else:
                self.functionsToParse["Sigma"].function.M[:,i,j,:] = 0
                print(f"Off-diagonal {i,j} is not sufficiently converged, discarding it")

        self.RE = RE

        #Grab the Occupation Susceptibility
        self.OccSusc = None
        if opts.occupation_susceptibility or hasattr(params,"susc_ph"):
            
            self.OccSusc = MakeSusceptibilityMatrixFunctionForImp(ser.spinful_dim, self.num_freqs, imp.beta)
            
            for i,j in product(range(ser.spinful_dim),repeat=2):
                
                occSuscObs, label, OccSuscJson = None, None, None
                if opts.occupation_susceptibility:
                    occSuscObs = self.obs["occupation-susceptibility-bulla"]
                    label = f"{i}_{j}"
                    OccSuscJson = occSuscObs[label]["function"]
                    
                else:
                    occSuscObs = self.obs_all["susc ph"]["susceptibility"]
                    label = f"{2*i+1}_{2*i}_{2*j+1}_{2*j}"
                    OccSuscJson = occSuscObs[label]["function"]["real"]
                
                if label in occSuscObs:
                    n = min(self.OccSusc.num_omega,50)
                    self.OccSusc.M[:n, i, j, 0] = self.ReadRealFunction(OccSuscJson)[:n]
                    self.OccSusc.moments[0, i, j, 0] = (self.ReadMoment(label,self.obs["occupation-susceptibility-bulla"]) 
                                            if opts.occupation_susceptibility else 0.)

    def StoreIM(self, imp : ImpurityProblem, ser : Serializer):

        #Fill out Impurity measurement
        def AssignLocalFunction(dst, src, num_omega):
            assert(num_omega <= src.num_omega)
            dst.M = np.copy(src.M[:num_omega, ...])
            dst.moments = src.moments
            dst.omega = src.omega
            dst.num_omega = num_omega
            
        print(f" - relative error of CTQMC is {self.RE}")
        self.IM.relative_error = self.RE
                
        AssignLocalFunction(self.IM.GImp, self.functionsToParse["GImp"].function, imp.num_freqs)
        AssignLocalFunction(self.IM.Sigma, self.functionsToParse["Sigma"].function, imp.num_freqs)
        AssignLocalFunction(self.IM.GAux, self.functionsToParse["GAux"].function, self.num_x)

        if self.IM.OccSusc.omega is None:
            self.OccSusc = MakeSusceptibilityMatrixFunctionForImp(ser.spinful_dim, imp.num_freqs, imp.beta)
        if self.OccSusc is not None:
            AssignLocalFunction(self.IM.OccSusc, self.OccSusc, imp.num_freqs)
        
        self.IM.RhoImp = self.functionsToParse["rhoImp"].function

        hl = min(self.EXP_HIST_LEN,len(self.histogram))
        self.IM.expansion_histogram[:hl] = self.histogram[:hl]
    
        print("- trying to save impurity result at ", imp.self_path+ "/measurement/")
                
        self.IM.store(imp.self_path + "measurement/", flush=True)

        #rename some files so we are sure we're loading the latest when we re-run ctqmc
        #keep them around for run recovery
        ImpurityDir = Path(imp.os_dir)
        for file in ["params.obs.json", "params.err.json", "params.covar.json"]:
            if os.path.exists(ImpurityDir / file): #error/covar files wont always exist
                os.rename(ImpurityDir / file, ImpurityDir / file.replace(".json", ".last.json"))
    
    def ParseFunction(self, fs : FunctionAndSource, ser : Serializer, imp : ImpurityProblem, opts : Namespace):
        for (i,j), label in ser.orbitalLabel.items():

            if fs.partition_source is not None and label in opts.forced_partition_meas_list.split(","):
                source = fs.partition_source
            else:
                source = fs.source

            #The omega functions will have function then imag / real then 
            #The first case here catches non-omega functions like rhoImp which otherwise fit in the paradigm
            if not fs.is_omega_function:
                functionJson = source[label]
            else:
                functionJson = source[label]["function"]

            if fs.is_omega_function:
                fs.function.M[:,i,j,0] = self.ReadComplexFunction(functionJson)[fs.slice]

                if fs.moments > 1:
                    fs.function.moments[0:fs.moments, i, j, 0] = self.ReadMoments(label, source)
                elif fs.moments == 1:
                    fs.function.moments[0:fs.moments, i, j, 0] = self.ReadMoment(label, source)
                
            else:
                fs.function[i,j,0] = self.ReadRealFromUnknownType(functionJson)[fs.slice]

            if imp.num_si == 2:
                # the down spin component - we just add the number of orbits
                up = label + "_" # and iorb stays the same (simple (valid?) symmetry)
                
                if not fs.is_omega_function:
                    functionJson = source[up]
                else:
                    functionJson = source[up]["function"]
                
                if fs.is_omega_function:
                    fs.function.M[:, i, j, 1] = self.ReadComplexFunction(functionJson)[fs.slice]
                    
                    if fs.moments > 1:
                        fs.function.moments[0:fs.moments, i, j, 1] = self.ReadMoments(up, source)
                    elif fs.moments == 1:
                        fs.function.moments[0:fs.moments, i, j, 1] = self.ReadMoment(up, source)

                else:
                    fs.function[i, j, 1] = self.ReadRealFromUnknownType(functionJson)[fs.slice]

        return fs

    def ReadRealFunction(self, address):
        num = len(address)
        fa = np.ndarray((num,), dtype=np.float64)
        for i, f in enumerate(address):
            fa[i]=f
        return fa

    def ReadComplexFunction(self, address):
        num = len(address["real"])
        fa = np.ndarray((num,), dtype=np.complex128)
        for i, f in enumerate(address["real"]):
            fa[i]=f + 1j*address["imag"][i]
        return fa
    
    def CountFrequenciesInOutput(self):
        # count them in the first green function
        return len(self.ReadRealFunction(self.obs["green"][self.label]["function"]["real"]))

    def ReadRealFromUnknownType(self, value):
        #if the CTQMC has complex = false
        if isinstance(value,list):
            return [float(f) for f in value ]
        
        #if the CTQMC has complex = true
        return [float(f) for f in value["real"] ]
        
    def ReadMoments(self, orb : int, obs : dict):
        return self.ReadRealFromUnknownType(obs[orb]["moments"])
        
    def ReadMoment(self, orb : int, obs : dict):
        return self.ReadRealFromUnknownType(obs[orb]["moment"])[0]

    def UpdateRE(self, RE, errSig, sig):
        # look at the 10% tail of the self energy (but at least 1 value) 
        # and take ratio of error to the actual value
        # we don't look at the fitted part of the self-energy, as the error there should be lower
        #  -- we look at the last freq. of the sampled part
        numIndices = min(len(errSig),self.num_x) 
        num = max(int(numIndices*0.1), 1) 
        #print("last sampling freq idx:", lastSamplingFreqIdx,num)
                 
        MR = np.max(abs(errSig[numIndices-num:numIndices]) / 
                    abs(sig[numIndices-num:numIndices])) # max ratio
        return max(RE, np.max(MR))

# imp is of type ImpurityProblem
# sign - the previous run's sign
# return value is of type ImpurityMeasurement
def RunCTQMC(imp : ImpurityProblem, 
             opts : Namespace, 
             mpi_workers=0, 
             cvg : FlapwMBPT_interface.Convergence = None,
             sign = 1.0):   

    def AddFsToImpurityProblem():
        
        F = U_J_to_radial_integrals(imp.subshell.l, imp.U, imp.J)
        
        imp.F0 = F[0]
        imp.F2 = F[1] if imp.subshell.l>=1 else 0
        imp.F4 = F[2] if imp.subshell.l>=2 else 0
        imp.F6 = F[3] if imp.subshell.l==3 else 0

    AddFsToImpurityProblem()

    #Basic parameterization needed to understand results
    params = ParameterOrchestrator(imp, opts)

    #Serializer which produces ComCTQMC readable formats
    ser = Serializer(imp)
    
    if not opts.post_process:

        #Complete parameterization and write input file out for ComCTQMC
        params.FinishAndWrite(imp, opts, mpi_workers, cvg, sign, ser)
        
        if opts.dry_run or opts.pre_process:
            return 
        
        #Run ComCTQMC
        currentDir = os.getcwd()
        os.chdir(imp.os_dir)
        StartComCTQMC(opts)
        os.chdir(currentDir)
    
    #Load Results and prepare to parse them into an ImpurityMeasurement -- then parse and store
    parser = ResultsParser(imp, ser, opts)
    parser.Parse(imp, ser, opts, params)
    parser.StoreIM(imp, ser)

    # returns the impurity measurement
    return parser.IM
                
def CTQMCOptionsParser(add_help=False, private=False):
    parser = ArgumentParserThatStoresArgv('ctqmc', add_help=add_help , parents=[],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
 
    cp = parser.add_argument_group(title="ctqmc parameters",
                                   description="modify ctqmc computation")
    
    
    cp.add_argument("--observables", dest="observables",
                      default="",
                      help="""if empty, no observables are measured after CTQMC. 
                              Otherwise, if "all" - add all the observables from the list
                              otherwise, a comma-separated list of observables.
                              The set of all possible (automatic) observables are: 
                                 "Lx","Ly","Lz","L2",
                                 "Sx","Sy","Sz","S2",
                                 "Jx","Jy","Jz","J2", 
                                 "Mx","My","Mz","M2"
                           """) 
    cp.add_argument("--time-thermalization", dest="time_thermalization",
                    type=int,
                    default=-1.0,
                    help="if >0, it is the time for thermalization in minutes. otherwise - it is computed") 
       
    cp.add_argument("--time-measurement", dest="time_measurement",
                    type=int,
                    default=-1.0,
                    help="if >0, it is the time for measurement in minutes. otherwise - it is computed") 

    cp.add_argument("--thermalization-steps", dest="thermalization_steps",
                    type=int,
                    default=-1.0,
                    help="if >=0, have the CTQMC perform a set number of steps during thermalization") 
       
    cp.add_argument("--measurement-steps", dest="measurement_steps",
                    type=int,
                    default=-1.0,
                    help="if >0, have the CTQMC perform a set number of steps during measurement (positive values below 1000 become 1000)") 
       
    cp.add_argument("--adjust-time-to-sign", dest="adjust_time_to_sign",
                      action="store_true",
                      help="if measurement time is <0 (default), calculate it adaptively using 1/sign dependence",
                      default=False) 
       
    cp.add_argument("--no-green-bulla", dest="green_bulla",
                      action="store_false",
                      help="do not approximate the greens function using Bulla's method",
                      default=True) 
         
    cp.add_argument("--not-precise-density-matrix", dest="density_matrix_precise",
                      action="store_false",
                      help="do not use the precise density matrix calculation",
                      default=True) 
      
    cp.add_argument("--analysis-only", dest="analysis_only",
                      action="store_true",
                      default=False,
                      help="only rewrite the params / observables, and then re-evaluate them") 

         
    cp.add_argument("--simulations-per-GPU", dest="sim_per_device",
                         type=int,
                         default=0,
                         help="Specify the number of simulations (or markov chains) to run on each GPU. 0 specifies a CPU only run. Will adaptively reduce if GPU runs out of memory.")
                         
    
    cp.add_argument("--worms", dest="worms",
                        default="",
                        help="""if empty, no worm spaces are visited in CTQMC.
                                    Otherwise, if "all" - add all the worm spaces from the list
                                    otherwise, a comma-separated list of worm spaces.
                                    The set of all possible worm spaces are:
                                        "green",
                                        "susc_ph","susc_pp",
                                        "hedin_ph","hedin_pp",
                                        "vertex"
                                """)
    
    cp.add_argument("--diagonal-worms", dest="diagonal_worms",
                    default=False,
                    action = "store_true",
                    help=""" Instead of measuring the full tensors chi_ijkl asociated with a worm, only measure the diagonal entries chi_iijj """)
                  
    cp.add_argument("--self-energy", dest="self_energy",
                    default="self-energy",
                    help="the json component to read self-energy from: either self-energy-dyson (exists only if bulla=True), or self-energy")

    cp.add_argument("--pre-process", dest="pre_process",
                      action="store_true",
                      default=False,
                      help="Generate input files associated with the CTQMC run but do not run CTQMC or EVALSIM") 

    cp.add_argument("--post-process", dest="post_process",
                      action="store_true",
                      default=False,
                      help="Do only post processing AFTER a run (assumes results exist).") 
           
    cp.add_argument("--occupation-susceptibility", dest="occupation_susceptibility",
                        action = "store_true",
                        default = False,
                        help = "measure the susceptibilities \chi_aabb(i\omega_n), where a and b are orbital indices" )

    cp.add_argument("--qn-susceptibility", dest="qn_susceptibility",
                        action = "store_true",
                        default = False,
                        help = "measure the susceptibility of quantum numbers \chi_QQ(i\omega_n), where Q is a quantum number of the Hilbert space" )
                    
    cp.add_argument("--ctqmc-qns", dest="ctqmc_qns",
                        default = "",
                        help = "Quantum numbers for which to compute observables <Q> (and <Q(tau)Q(0)> if --qn-susceptibility is turned on)" )
                       
    cp.add_argument("--all-errors", dest="all_errors",
                        action = "store_true",
                        default = False,
                        help = "Compute error of CTQMC for all observables rather than a limited, easier to compute set")

    cp.add_argument("--force-partition-measurement-for", dest="forced_partition_meas_list",
                        default = "",
                        help = """If the worm-space self-energy is measured, by default it is used. 
                        The list of elements in the rep given here will instead use the partition measurement. 
                        This is useful when a few elements of the rep dominate the partition space 
                        (such that the remaining elements are poorly measured).""")

    cp.add_argument("--dry-run", dest="dry_run",
                    action="store_true",
                    default = False)


    cp.add_argument("--truncate-offdiags", dest="truncate_offdiags",
                    default = 0, type=float,
                    help = """Get rid of offdiagonal self energies if every element of 
                            the measured self energy has a relative error greater than this value""")

    cp.add_argument("--file-refresh-time", dest="file_refresh_time",
                    default = 0, type=float, 
                    help = SUPPRESS_HELP)
                        
    # TODO: Add more options here for CTQMC - worms, quad insertions etc.
    
    return parser

@with_organizer
def CTQMC(opts):
    main([],{},opts = opts)

def main(args, kwargs, opts = None):
    
    parser = ArgumentParserThatStoresArgv("ctqmc", add_help=True,
                            parents=[CTQMCOptionsParser(private=True), GetMPIProxyOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)
        
    parser.add_argument("-I", "--impurity-problem", dest="impurity", default="./dmft.h5:/impurity/",
                      help="designates an object of type OrbitalsBandProjector")
    
    parser.add_argument("-f", "--on-files", dest="files",
                      action="store_true",
                      default=False,
                      help="Directly run CTQMC and analysis on files in current dir") 
 
    parser.add_argument("--eval-sim", dest="eval_sim",
                      action="store_true",
                      default=False,
                      help="Directly run EvalSim in current dir") 

    parser.add_argument("--interaction-truncation", dest="interaction_truncation",
                    default=0.0,
                    type=float, help="values of U below this in [eV] will be truncated to 0.0. Usually useful only for --full-interaction.")

    # prepare files for CTQMC
    if opts is None:
        opts, _args = parser.parse_known_args(args)
    
    for key in kwargs:
        setattr(opts, key, kwargs[key])
        
    logging.basicConfig()
    #logging.getLogger().setLevel(logging.INFO)
    
    logging.info("running in directory "+ os.path.abspath("."))
    
    mpi_proxy = MPIContainer(opts)
    if opts.files:
        CTQMCOnInputFile(mpi_proxy)
        mpi_proxy.Barrier()
        if not opts.eval_sim:
            mpi_proxy.Terminate()
            return

    if opts.eval_sim:
        EvalSim(mpi_proxy)
        mpi_proxy.Barrier()
        mpi_proxy.Terminate()
        return
        
    imp = ImpurityProblem()
    imp.load(opts.impurity)

    sys.stdout.flush()

    IM = RunCTQMC(imp, 
                  opts, 
                  mpi_workers=opts.mpi_workers)

    sys.stdout.flush()
    
    if opts.post_process:
        dmft = DMFTState()
        dmft.load("./dmft.h5:/")
        dmft.hist = IM.hist
        dmft.store("./dmft.h5:/", flush=True)
  
if __name__ == '__main__':
    main(sys.argv[1:],{})
    
