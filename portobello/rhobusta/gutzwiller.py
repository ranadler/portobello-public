#!/usr/bin/env python3

'''
Created on Nov 12, 2018

@author: adler@physics.rutgers.edu
'''
from portobello.rhobusta.ProjectorEmbedder import CorrelatedSubspaceProjector
from portobello.rhobusta.multi_impurity import MultiImpurityEmbedder
from portobello.rhobusta.plotters import BaseBuilderOnIBZ
from portobello.symmetry.general import PrintMatrix
from portobello.jobs.Organizer import with_organizer
import sys
from argparse import ArgumentDefaultsHelpFormatter
import numpy as np
from scipy.integrate import simps as SimpsonIntegration
from portobello.bus.Matrix import Matrix, ZeroComplexMatrix
from portobello.generated import Rho
from portobello.generated.gutz import GState, GSolution
from portobello.generated.Rho import RhoInBands
from portobello.generated.self_energies import LocalOmegaFunction
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.GutzEquations import FlatMPIEquationSolver, GetGutzEquationsOptionParser, FileNameForShell
from portobello.rhobusta.MatrixParameterization import MatrixParameterizer
from portobello.rhobusta.interaction import U_matrix_slater, get_average_uj
from portobello.rhobusta.options import ReadOptions, SaveOptions
from portobello.rhobusta.PEScfEngine import GetPEScfOptionsParser, PEScfEngine, Ry2eV
from portobello.rhobusta.options import ArgumentParser, ArgumentParserThatStoresArgv
from portobello.symmetry.local import GetCGMatrix, GetRealSHMatrix
from portobello.bus.mpi import MPIContainer
from portobello.bus.persistence import Persistent, Store
from itertools import product

EPSILON = 1.0e-10
gLocation = "./gutz{0}.h5:/"


class Gutzwiller(GState, PEScfEngine):
    '''
    This code calculates the Ek and Eloc matrices, for the use of the Gutzwiller code.
    The other numbers that this code provides are:
    
    self.num_si             - number of spin indices
    self.num_k_all          - the number of k points in the FULL Brillouin zone
    self.full_dim  - the non-correlated dimension
    self.dim                - the dimension of the correlated subspace (without spin polarization)
    
    Then we calculate:
    self.Hk[num_k, full_dim, full_dim,num_si]
    self.Hloc[dim,dim,num_si]
    
    '''
    def __init__(self, opts, for_dft=False):
        GState.__init__(self)
        is_new = PEScfEngine.__init__(self, opts)

        self.InitOneBody()    
        # if requested initial rho from lattice (not uniform)
        if self.super_iter == 1 and hasattr(opts, "initial_rho") and opts.initial_rho == "from-lattice":
            self.RhoImp[...] = self.occ[...]
            self.SymmetrizeCorrelated(self.RhoImp, hermitian=True, raw=True)
          
        # to allocate the arrays and use later
        self.num_k = self.dft.num_k
        self.num_k_irr = self.dft.num_k_irr
        self.num_bands = self.num_corr_bands
        
        # we don't want to call alloc() here, since InitOneBody() already allocated and initialized some arrays
        self.Hk = np.zeros((self.num_k, self.num_bands, self.num_bands, self.num_si), np.complex128)
        # this one is transient
        self.HkBands = np.zeros((self.dft.num_k, self.num_bands, self.num_bands, self.num_si), np.complex128)
        
        if self.state == 'start':
            # normal initialization 
            self.state = 'initialized'
            self.R[:,:,:] = self.ID[:,:,:]*opts.r_initial # to avoid probing R>1 in the solver
            self.Lambda[:,:,:] = self.Elocal[:,:,:]

        if hasattr(opts,'renormalize_hloc'):
            renormalize_hloc = opts.renormalize_hloc
        else:
            opts2 = ReadOptions()
            if hasattr(opts2,'renormalize_hloc'):
                renormalize_hloc = opts2.renormalize_hloc
            else:
                renormalize_hloc = False

        if renormalize_hloc:
            for si in range(self.num_si):
                self.RenormalizeElocal(si)

        if for_dft:
            if self.state != 'initialized' and self.state != "dft+sigma":
                self.super_iter += 1
                self.sub_iter = 0
            else:
                self.sub_iter += 1
            self.state = 'dft+sigma'

        # TODO: remove this alias
        self.full_dim = self.num_bands
        # this is the dimension of the non-correlated hamiltonian that GA uses
       
        # first set Hk and Hloc
        for k, si in self.shard_k_si_pairs():
            self.Hk[k,:,:, si] = np.diag(self.epsAllK[:,k, si])
        
        self.Hloc  = np.copy(self.Elocal)
            
        # occupation number
        self.occupancy = np.trace(self.occ).real
            
                    
        # substract Hloc from Hk
        for k, si in self.shard_k_si_pairs():
            self.Hk[k,:,:, si] -= self.EmbeddedIntoKBandWindow(self.Hloc, k, si)
                     
        # return a numbered Identity matrix        
        def MakeNumberedID():
            nid = np.zeros((self.dim, self.dim, self.num_si), dtype=np.complex128)
            for si in range(self.num_si):
                for i in range(self.dim):
                    nid[i,i, si] = i+1  # non zero
            return nid
            
        nid = MakeNumberedID()
          
        # Rotating Hk so that ONE copy of the correlated space is in the bottom right of the matrix
        for k, si in self.shard_k_si_pairs():
            
            # save the non-rotated, band matrix
            self.HkBands[k,:,:, si] = self.Hk[k, :,:, si]
            
            indices = np.array([-self.dim + i for i in range(self.dim)])
            
            em = self.EmbeddedIntoKBandWindow(nid, k, si)
            w, v = np.linalg.eigh(em)
              
            # The matrix v satisfies v.H * w * v = Pk*Pk.H (replicated over all equivalent atoms)
            assert(np.all(w[:-self.dim*(self.num_equivalents+self.num_anti_equivalents)] < EPSILON)) 
            tester = np.repeat(list(range(1,self.dim+1)),self.num_equivalents+self.num_anti_equivalents) 
            assert(np.all(abs(w[-self.dim*(self.num_equivalents+self.num_anti_equivalents):]-tester) < 0.01)), w # embedding can be off by <10% due to normalization
            
            if not np.all(abs(Matrix(v).H * v - self.ID1F) < EPSILON):  # assert u.H = v
                print("**** matrices V violate assumption:")
                self.PrintMatrix(Matrix(v).H * v)
                assert(False), "assumption violated"
            
            # copy the first basis vectors corresponding to non-projected subspace.
            # the last vectors will be replaced
            V = Matrix(np.copy(v))
            
            
            # TODO: can this be skipped if num_equivalents=1?
            
            assert(self.num_equivalents >=0)
            ###### Note: the equivalents are handled first, but appear LAST! 
            ######       the anti-equivalents appear in the start of the bigger matrix
            for eqv in range(self.num_equivalents+self.num_anti_equivalents):
                # the last eigenvalues are 0, which means they belong to the non-correlated subspace
                # this is the subspace that we are interested in when calculating the Ek matrix

                em = self.EmbeddedIntoKBandWindowOneAtom(nid[:,:,si], k, si, eqv)
                w, v = np.linalg.eigh(em)
            
                # verify that the first num_bands-dim values are 0, and last dim  values are 1:
                # embedding is on the last self.dim*num_equivalents eigenvectors
                    
                # The matrix v satisfies v.H * w * v = Pk*Pk.H (replicated over all equivalent atoms)
                assert(np.all(w[:-self.dim] < EPSILON)) 
                assert(np.all(abs(w[-self.dim:]-list(range(1,self.dim+1))) < 0.01)), w # embedding can be off by <10% due to normalization
                
                if not np.all(abs(Matrix(v).H * v - self.ID1F) < EPSILON):  # assert u.H = v
                    print("**** matrices V violate assumption:")
                    self.PrintMatrix(Matrix(v).H * v)
                    assert(False), "assumption violated"
            
                V[:, indices] = v[:, -self.dim:]
                indices[:] -= self.dim
            
            if not np.all(abs(V.H*V- self.ID1F) < 0.01):  # make sure the new v matrix is also unitary
                print("**** V matrix is not unitary:")
                self.PrintMatrix(V.H*V)
                assert(False), "assumption violated"
                
            self.Hk[k,:,:, si] = V.H * self.Hk[k,:,:, si] * V
            
        
        if not for_dft:
            self.UpdateDoubleCounting(is_new=is_new)
        self.Vdc = self.DC

        if not for_dft:           
            self.UTensor = self.GetUMatrix()

        self.gutz_name = FileNameForShell(self.opts.which_shell, template="./gutz-impurity{shell}.tmp.h5:/" )

        sys.stdout.flush()
        self.StoreMyself()

    def IsQpHamiltonianNonHermitian(self):
        if hasattr(self.opts, 'gutz_with_lifetime') and self.opts.gutz_with_lifetime:
            return True
        return False
    
    def DebugPrint(self, s : str, M):
        if not hasattr(self.opts, 'verbose'): return
        if self.plugin.IsMaster() and self.opts.verbose > 2: PrintMatrix(s,M)

    def RenormalizeElocal(self, si):
        R = Matrix(self.R[:,:,si])
        # first set Elocal to the QP projected Elocal
        self.Elocal[:,:,si] = R * Matrix(self.Elocal[:,:,si]) * R.H + Matrix(self.Lambda[:,:,si])
        # Now solve the equation P(Hqp) = Hloc for Hloc - which basically renormalizes Elocal
        HlocMP = MatrixParameterizer(self, attr="Elocal", si=si, isHermitian=True, obj=self, isReal=False)
        # set up equation Q*R = B, B is packed complex Hloc
        B = HlocMP.PackComplex()
        Q = np.zeros((HlocMP.repMax,HlocMP.repMax),dtype=np.complex128)
        # calculate Q matrix
        for r1 in range(HlocMP.repMax):
            Q[r1,r1] = 1.0
            m,n = HlocMP.indices[r1][0] # representative indices
            for r2 in range(HlocMP.repMax):
                k,l = HlocMP.indices[r2][0]
                F = R[m,k] * np.conj(R[n,l])
                if abs(F) > 1.0e-8:
                    Q[r1,r2] += F
        self.DebugPrint("- Q matrix:", Q)
        # solve the equation
        HlocNewPacked = Matrix(Q).I * Matrix(B).T
        # finally, renormalize Eloc
        HlocMP.UnpackFromComplexVector(HlocNewPacked)

    def GetMethodName(self):
        return "gutzwiller"

    def GetMethodLocation(self):
        return gLocation
           
    # here M does not have a spin index - this is only used to decompose the orbital space (in GEngine)
    def EmbeddedIntoKBandWindowOneAtom(self, M, k, si, eqv):
        Mfull = ZeroComplexMatrix(self.dim*(self.num_equivalents + self.num_anti_equivalents))
        Mfull[eqv*self.dim:(eqv+1)*self.dim, eqv*self.dim:(eqv+1)*self.dim] = M[:,:]
        Pk = Matrix(self.orbBandProj.P[:,:,k,si])
        return Pk * Mfull * Pk.H
        
    # this is the callback
    def EmbedSigma(self, location, ignore_state=False, retain_result=False):
        return self.WriteDensityMatrix(retain_result=retain_result)

    def WriteDensityMatrix(self, retain_result=False, MI=False):
        if self.plugin.IsMaster(): print(" - portobello writing ρ(λ,R)")
        
        rho = RhoInBands(num_k=self.dft.num_k,
                         min_band=self.orbBandProj.min_band,
                         max_band=self.orbBandProj.max_band,
                         num_si=self.num_si)
        #print "num k irr: ", rho.num_k
        rho.allocate()

        # we also record the orbital correlated number matrix
         
        if self.plugin.IsMaster(): print(" - μ: %f eV" % self.mu)
        
        # calculate rho_k = Rf(RHkR+L)R  and the projected value of rho_k
        Ncorr = self.ZeroLocalMatrix()
        I = np.eye(self.num_bands, self.num_bands, dtype=np.complex128)

        if MI:
            # R_MI = Embed_i(R_i-I)
            # L_MI = Embed_i(L_i)
            self.R_MI = np.zeros((self.num_bands, self.num_bands, self.dft.num_k, self.dft.num_si), dtype=np.complex128)
            self.L_MI = np.zeros_like(self.R_MI)
            
        for k,si in self.shard_k_si_pairs():            
            # we make all calculations in band basis, which is why we use HkBands, and embed R,L into bands
            HkB = Matrix(self.HkBands[k, :,:, si])

            R = Matrix(np.copy(I))
            ERminI = self.EmbeddedIntoKBandWindow(self.R - self.ID, k, si) 
            if MI: self.R_MI[:,:,k,si] = ERminI[:,:]
            R +=  ERminI[:,:]
            EL =  Matrix(self.EmbeddedIntoKBandWindow(self.Lambda, k, si))
            if MI: self.L_MI[:,:,k,si] = EL[:,:]
            L = EL[:,:]
            # Quasi particle hamiltonian
            Hqp = R * HkB * R.H + L - self.mu *I
            
            Eqp, u = self.DiagonalizeHamiltonianNoSort(Hqp)
            uM = Matrix(u)          
            # assert(np.all(abs(uM *np.diag(Eqp)* uM.H - Hqp) < 1.0e-8))
 
            if hasattr(self.opts, 'use_z_in_mu_search') and self.opts.use_z_in_mu_search:
                z = self.Z - self.ID
                embZ = self.EmbeddedIntoKBandWindow(z, k, si) + I
                embZ = uM * embZ.H * uM.H
            else:
                embZ = I

            rho.Z[:,k,si] = np.real(np.diag(embZ))
            
            # calculate the fermi function of the Eqp matrix
            UR = uM.H * R
            rhoBands = UR.H *  np.diag(self.ferm(Eqp)) * UR
            if MI:
                rhoBands *= 0.0
            rho.rho[:, :, k, si] = rhoBands[:,:]            
            rho.Hqp[:,k, si] = (Eqp[:] +  self.mu) / Ry2eV
            
            # calculate the projected rho (using Rf()R)
            Pk = self.OneAtomProjector(k, si) # importantly, this is the 1st dim dimensions, not last (last may be AFM)
            Ncorr[:,:,si] +=  Pk.H * rhoBands * Pk  * self.lapw.weight[k] # this is why we loop over ALL k: in order to project we need values at all k
             
        if self.dft.is_sharded:
            MPI = self.plugin.GetMPIInterface()
            MPI.COMM_WORLD.Barrier()
            Ncorr = MPI.COMM_WORLD.allreduce(Ncorr, op=MPI.SUM)

        self.SymmetrizeCorrelated(Ncorr, hermitian=True, raw=True)
        #if self.plugin.IsMaster(): PrintMatrix("RhoImp", self.RhoImp[:,:,0])

        # then we correct the local correlated part of this rho_k 
        for k,si in self.shard_k_si_pairs():
            rho.rho[:,:,k, si] -= self.EmbeddedIntoKBandWindow(Ncorr - self.RhoImp, k, si)

        # note the indirection of this write - the location is dependent on the number of bands
        # this is because arrays are fixed sized in storage, and therefore we need to include the number of 
        # bands (which is the varying size) in the output name
        ri = Rho.RhoInfo()
        ri.location = "./rho.core.h5:/bands-%d"% (rho.max_band-rho.min_band+1)
        rho.my_location = ri.location # transient 
        ri.store("./rho.core.h5:/info", flush=True)
        # write it on the bus so that Fortran code can find it
        rho.store(ri.location, flush=True)
        
        if not retain_result: 
            del rho
            rho = None
        
        # this one should be close to Nimp
        sys.stdout.flush()
        
        # Note: the values here will be valid only if set_g was called before in FlapwMBPT,
        #       which IS the case when we embed sigma
        if self.sub_iter >= 1:
            self.SetLatticeOccupation()
            self.StoreMyself()
        return rho
          
    # we are the main embed (#0 shell)
    def StartMIEmbed(self):
        # HkBands is ready.
        # now prepare my rho
        self.MIRho = self.WriteDensityMatrix(retain_result=True, MI=True)
        self.allImpurities = [self]

    def AddMI(self, imp : PEScfEngine):
        rhoOther = imp.WriteDensityMatrix(retain_result=True, MI=True)
        self.MIRho.rho[...] += rhoOther.rho[...]
        self.allImpurities.append(imp)
        self.R_MI[...] += imp.R_MI[...]  # add up all the embedded_i(R_i-I)
        self.L_MI[...] += imp.L_MI[...]  # add up all the embedded_i(L_i)
        return False # this algorithm uses the full set of impurities to loop for a second time to compute E(P(f(Hqp))) to subtract

    def FinalizeEmbed(self):
        # rename the totals
        I = np.eye(self.num_bands, self.num_bands, dtype=np.complex128)
        for k, si in self.shard_k_si_pairs():
            self.R_MI[:,:,k,si] += I[:,:]
        # self.HkBands already arranged

    def WriteMIEmbed(self):
        Ncorr = [self.ZeroLocalMatrix() for imp in self.allImpurities]
        # first calculate Hqp eigenvalues (used in calculation of chemical potential)
        I = np.eye(self.num_bands, self.num_bands, dtype=np.complex128)
        for k, si in self.shard_k_si_pairs():
            HkB = Matrix(self.HkBands[k,:,:,si])
            R = Matrix(self.R_MI[:,:,k,si])
            L = Matrix(self.L_MI[:,:,k,si])
            # Quasi particle hamiltonian
            Hqp = R * HkB * R.H + L - self.mu*I
            Eqp, u = self.DiagonalizeHamiltonianNoSort(Hqp)
            # set the total quasi particle hamiltonian
            # TODO: this will not work with gutz_with_lifetime, for now it is as good as we will do.
            #       to finish this option, we will need to sum the weight on the left side of gaussians to determine mu
            self.MIRho.Hqp[:,k, si] = (np.sort(np.real(Eqp))[:] +  self.mu) / Ry2eV
            uM = Matrix(u)          
            # add the total density of the combined quasi-particle hamiltonian (since in MI=True mode we don't add it before)
            UR = uM.H * R
            rhoBands =  UR.H * np.diag(self.ferm(Eqp[:])) * UR
            self.MIRho.rho[:, :, k, si] += rhoBands[:,:]

            # NOW subtract Ncorr for the impurity (aka Ncorr in WriteDensityMatrix terms)
            # first we calculate Ncorr
            for i, imp in enumerate(self.allImpurities):
                Pk = imp.OneAtomProjector(k, si) # importantly, this is the 1st dim dimensions, not last (last may be AFM)
                Ncorr[i][:,:,si] +=  Pk.H * rhoBands * Pk  * self.lapw.weight[k] # this is why we loop over ALL k: in order to project we need values at all k
             
        if self.dft.is_sharded:
            MPI = self.plugin.GetMPIInterface()
            MPI.COMM_WORLD.Barrier()
            for i, imp in enumerate(self.allImpurities):
                Ncorr[i] = MPI.COMM_WORLD.allreduce(Ncorr[i], op=MPI.SUM)
                
        for i, imp in enumerate(self.allImpurities):
            self.SymmetrizeCorrelated(Ncorr[i], hermitian=True, raw=True)

        # then we correct the local correlated part of this rho_k 
        for k,si in self.shard_k_si_pairs():
            for i, imp in enumerate(self.allImpurities):
                self.MIRho.rho[:,:,k, si] -= imp.EmbeddedIntoKBandWindow(Ncorr[i], k, si)
        self.MIRho.store(self.MIRho.my_location, flush=True)
          
    def LoadMyself(self):
        is_new = False
        name = self.GetTopLevelMethodName()
        try:
            GState.load(self, name, checkExists=True)
        except Exception as e:
            is_new = True
        return is_new
        
    def StoreMyself(self):
        self.self_path = self.GetTopLevelMethodName()
        index = self.self_path.find(".h5")
        self.self_path = self.self_path[:index] + self.GetAdditionalDescriptors() + self.self_path[index:]
        
        name = self.GetTopLevelMethodName()
        if self.plugin.IsMaster(): 
            self.store(self.self_path, flush=True)
            MultiImpurityEmbedder.StoreImpurityDetails(self, self.opts)
        else:
            Persistent.Refresh(self.self_path)
        
    def StoreStateInHistory(self):
        if not self.plugin.IsMaster(): 
            return
        self.PreserveHistory()
        GState.store(self, f"./history.h5:/impurity{self.GetSuffixOfImpurity()}/{self.super_iter}/{self.sub_iter}/", flush=True)
                     
    # mu is *not* subtracted from local and eps_k
    def MuChoice(self):
        return 0.0

    def SetSigImag(self, sig):
        for si in range(self.num_si):
            R = Matrix(self.R[:,:,si])
            μI = (1.0 - self.MuChoice()) * self.mu * self.ID1
            sig0 = R.I * (self.Lambda[:,:,si]-μI) * R.H.I - self.Hloc[:,:,si] +  μI 
            sig1 = self.ID1 - (R.H*R).I
                            
                            
            for iomega in range(sig.num_omega):
                w = sig.omega[iomega]
                
                # valid only within the energy window / 2.0 (is this even too much ?)
                sig.M[iomega, :,:,si] =  sig0 + 1j*w*sig1
                # otherwise it is 0
             
    def LoadSigRealOmega(self, bound=25.0, Nw = 100, interp_w=False):
        sig = LocalOmegaFunction(is_real=True,
                                 num_orbs=self.dim,
                                 num_si=self.num_si,
                                 num_omega = 2*Nw+1)
        sig.allocate()
        
        self.DC[:,:,:] = 0.0 # we avoid adding and substracting it again in the plotting, since the self energy is not defined over the entire range
        
        for si in range(self.num_si):
            R = Matrix(self.R[:,:,si])
            μI = (1.0 - self.MuChoice()) * self.mu * self.ID1
            sig0 = R.I * (self.Lambda[:,:,si] - μI)* R.H.I - self.Hloc[:,:,si] + μI# no DC
            # note that we add mu in sigma, which appears with - sign in the G function,
            # and therefore cancels the mu added in that function in the QP orbital space, as
            # it appears in the formula for QP greens function

            sig1 = self.ID1 - (R.H*R).I
                            
            for iomega in range(-Nw, Nw+1):
                w = iomega * bound / max(Nw,1) 
                
                sig.omega[iomega+Nw] = w
                # valid only within the energy window / 2.0 (is this even too much ?)
                #if w < self.proj_energy_window.high / 2.0 and w > self.proj_energy_window.low / 2.0:
                if w < self.proj_energy_window.high and w > self.proj_energy_window.low:
                    sig.M[iomega+Nw, :,:,si] =  sig0 + w*sig1
                # otherwise it is 0
        self.sig = sig

    def GetUMatrix(self):
        return PEScfEngine.GetUMatrix(self, factor = 0.5 if self.opts.solver == 'ied' else 1)

    def SetSolution(self):
        gsol = GSolution()
        gsol.load(self.gutz_name+"result/")
        Store.Singleton().CloseFile(self.gutz_name) # so that we will overwrite it next time - in case this is one process
        
        self.R = gsol.R
        self.Lambda = gsol.Lambda * self.opts.lambda_admix + (1.0-self.opts.lambda_admix)*self.Lambda
        self.Z = gsol.Z
        self.RhoImp = gsol.RhoImp
        self.Z = gsol.Z
        self.hist = gsol.hist
        self.quality = gsol.quality
        self.Nimp = gsol.N
        self.success = gsol.success
        self.energyImp = gsol.energyImp

        if self.state != 'initialized':
            self.super_iter += 1
            self.sub_iter = 0
        self.state = "g-solved"
        
        self.StoreMyself()
        self.StoreStateInHistory()

    def ConstructQuasiparticleHamiltonian(self, proj, epsAllK, I, k, si):

        Hk = np.diag(epsAllK[:,k, si]) - proj.EmbeddedIntoKBandWindow(self.Hloc, k, si)

        R = Matrix(np.copy(I))
        ERminI = proj.EmbeddedIntoKBandWindow(self.R - self.ID, k, si)
        R +=  Matrix(ERminI[:,:])
        L =  Matrix(proj.EmbeddedIntoKBandWindow(self.Lambda, k, si))

        return R * Hk * R.H + L - self.mu * (1.0-self.MuChoice()) * I


    def ConstructZ(self, proj, uM, I, k, si):

        z = self.Z - self.ID
        embZ = proj.EmbeddedIntoKBandWindow(z, k, si) + I
        embZ = uM.H * embZ.H * uM

        return embZ

    def GetQuasiparticleWeights(self, proj, uM, I, k, si):
        return np.diag(self.ConstructZ(proj, uM, I, k, si))

    def GetQuasiparticleLifetimes(self, proj, uM, I, k, si):

        l = np.imag(self.Lambda)
        l = proj.EmbeddedIntoKBandWindow(l, k, si)
        l = uM.H * l.H * uM
        
        return 1./np.imag(np.diag(l))


def GutzOptionsParser(prepArgs, add_help=False, private=False):
   
    parser = ArgumentParserThatStoresArgv('gutz', add_help=add_help ,
                            parents=[GetPEScfOptionsParser('gutz', prepArgs), GetGutzEquationsOptionParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)    

    if private:
        parser.add_argument("-w", "--energy-window", dest="window",
                            help="energy window for the cubic_harmonicsands (in eV)",
                            default='-10.0:10.0')

    parser.add_argument("--inputs", dest="only_inputs",
                        action="store_true",
                        default=False,
                        help="Only create inputs R=I Lambda=Eloc+Vdc")

    parser.add_argument("--solver", dest="solver",
                        type=str,
                        default='ied',
                        help="ED solvers. This code has only one solver possible: do not change")
    
    parser.add_argument("--is-real", dest="is_real",
                        default=False,
                        action="store_true",
                        help="Make the gutzwiller λ variables real")
   
    parser.add_argument("--r-initial",
                    dest="r_initial",
                    default=1.0,
                    type=float,
                    help = "initial R value for the gutzwiller calculation") 
                
    parser.add_argument("--lambda-admix",
                    dest="lambda_admix",
                    default=0.95,
                    type=float,
                    help = "admix for lambda at the end of each gutz. iteration") 
                
    parser.add_argument("--renormalize-hloc", dest="renormalize_hloc",
                        default=False,
                        action="store_true",
                        help="experimental")
    
    parser.add_argument("--initial-rho", dest="initial_rho",
                        choices=["from-lattice", "uniform"],
                        default="from-lattice",
                        help="take initial density from the lattice calculation")
       
    return parser
  
def GutzwillerMain(args, kwargs, opts=None):
    
    if opts is None:
        parser = GutzOptionsParser(args, add_help=True, private=True)    
        opts, _args = parser.parse_known_args(args)
        SaveOptions(opts)
    
    for key in kwargs:
        setattr(opts, key, kwargs[key])
    
    mpi = MPIContainer(opts)
    _plugin = DFTPlugin.Instance(restart=True, mpi=mpi)
    gutz = Gutzwiller(opts)
    
    if opts.only_inputs:
        gutz.which_shell = opts.which_shell
        gutz.StoreMyself()
        return
    
    #ges = GutzEquationsSolver(gutz, isReal=opts.is_real, opts=opts)
    #ges.Solve()

    feq = FlatMPIEquationSolver(opts)
    feq.SetGutz(gutz)
    feq.Run()

    gutz.SetSolution()
    

@with_organizer
def Gutz(opts):
    GutzwillerMain([], {}, opts=opts)


if __name__ == '__main__':
    GutzwillerMain(sys.argv[1:], {})
