#!/usr/bin/python

'''
Created on Feb 28, 2019

@author: adler
'''
from pymatgen.symmetry.maggroups import MagneticSpaceGroup, MAGSYMM_DATA
import sqlite3
from pymatgen.analysis.magnetism.analyzer import CollinearMagneticStructureAnalyzer

if __name__ == '__main__':
    #mg = MagneticSpaceGroup(1078)
    #mg.get_orbit((0,0,0), (0,0,1))
    #print mg
    #print(mg.symmetry_ops)
    #print(len(mg.symmetry_ops))
    
    i = "225"
    db = sqlite3.connect(MAGSYMM_DATA)
    c = db.cursor()
    c.execute("SELECT OG1,OG2,OG3 FROM space_groups WHERE OG1 LIKE '%s%%';" % i)
    
    all_ids =  c.fetchall()
    
    for og in all_ids:
        #print "-------------------------------------------\n",og
        mg = MagneticSpaceGroup(og[2])
        
        for op in mg.symmetry_ops:
            if op.time_reversal == -1.0:
                print("group %s has reversal"%og[2], len(mg))
                break
        #orbit = mg.get_orbit((0,0,0), (1,0,0))
        #print len(orbit[0])
        #print orbit
        
    CollinearMagneticStructureAnalyzer
        
    #op = mg[1]
    #print "operation is:"
    #print op.apply_rotation_only((0,0,0))
    #print "new magmom:"
    #print op.operate_magmom((0,0,1))
    
    