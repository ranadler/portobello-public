'''
Created on Nov 19, 2020 by C. Melnick
'''

import numpy as np

def computeQuasiMomemtumConservation(arpes, photons, electrons, photoelectrons):
    print("Computing momentum \"conservation\" in perpendicular direction")
    
    kpath = electrons.regularKpath
    
    delta = np.zeros(( kpath.nk_per_irregular_point, arpes.n_theta_k, arpes.nbins), dtype=np.complex)
    
    for th in range(arpes.n_theta_k):
        for b in range(arpes.nbins):
            
            i = 0
            for ik in kpath.map_irregular_to_regularized(th,b):
            
                k = 1j * ( kpath.at(ik).perp
                        - photoelectrons.k_at(th,b).perp
                        + photons.kperp) \
                        + electrons.invDecay
                    
                    
                delta[i,th,b] = 1./k
                
                i += 1
        
    return delta
            
def computePolarizationPart(arpes, photons, electrons, photoelectrons):
    
    polarizationPart = np.zeros((arpes.n_theta_k, arpes.nbins), dtype=np.complex)
        
    for th in range(arpes.n_theta_k):
        for b in range(arpes.nbins):
            
            polarizationPart[th,b] = np.dot(photons.polarization + 1e-14,
                                            1j * photoelectrons.k_at(th,b).full - electrons.invDecay * arpes.normal
                                            )
        
    return polarizationPart
