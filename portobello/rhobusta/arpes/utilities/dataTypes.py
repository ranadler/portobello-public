'''
  dataTypes.py
  ComARPES

  Created by Corey Melnick on 11/19/20.
'''
import numpy as np

def realArrayToComplexArray(rawArray):
    n = int(len(rawArray)/2)
    complexArray = np.zeros(n, dtype = np.complex)
    complexArray.real = rawArray[::2]
    complexArray.imag = rawArray[1::2]
    return complexArray
