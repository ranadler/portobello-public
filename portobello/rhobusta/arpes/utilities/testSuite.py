#!/usr/bin/env python
'''
Created on Nov 9, 2020 by C. Melnick
'''

import numpy as np


class TestSuite:
    
    def __init__(self, opts):
        pass
    
    @classmethod
    def getGreensFunctions(cls, arpes, kpath):
        
        t = -0.125
        
        greensFunctions = np.zeros((kpath.nk, arpes.nbins), dtype=np.complex)
        
        for ik in range(kpath.nk):
            
            ek = 0
            for x in kpath.at(ik).k:
                ek += t * np.cos(2.*np.pi*x / (1.413 * 2))
            
            for b in range(arpes.nbins):
                greensFunctions[ik,b] =  1.0 / (ek - arpes.ebins[b]  + 1j * 0.01 )
                
            
            
        return greensFunctions
    
    @classmethod
    def getCs(cls, arpes, structure, kpath):
        
        c_ntk = np.zeros((structure.natom, kpath.nk), dtype=np.complex)
        
        decay = 0.1
                
        for tau in range(structure.natom):
            for ik in range(kpath.nk):
                for r in structure.lat_vec_mesh:
                        
                    kdr = np.dot(kpath.at(ik).k, r)
                    c_ntk[tau,ik] += np.exp( -1j * kdr ) * np.exp( -np.linalg.norm(r) / decay )
        
        return c_ntk
        
    @classmethod
    def getOverlaps(cls, arpes, structure, electrons, photon, photoelectrons):
        
        overlaps = np.zeros((structure.natom, arpes.n_theta_k, arpes.nbins), dtype=np.complex)
        
        for tau in range(structure.natom):
            for th in range(arpes.n_theta_k):
                for b in range(arpes.nbins):
                    overlaps[tau,th,b] = np.exp(-1j * np.dot(
                        photoelectrons.kf[th,b] + arpes.normal * electrons.imagInvDecay,
                        structure.coordinates[tau])
                    )
                    
        return overlaps
        
        
