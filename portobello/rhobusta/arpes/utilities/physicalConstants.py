#!/usr/bin/env python
'''
Created on Nov 9, 2020 by C. Melnick
'''

class PhysicalConstants:
    
    hbar = 6.582119569E-16 #ev * s
    c = 299792458 #m/s
    kb = 8.617333262145E-5 # ev/K
    m = 9.1093837015E-31 #eV/c^2
    ec = 1.602176634E-19 #C
    
    angstrom = 1E-10 # m
    angstrom2bohr = 1.8897261254535
    Rydberg = 13.605698066

    c_atomic = 274.074
    
