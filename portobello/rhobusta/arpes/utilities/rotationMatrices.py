#!/usr/bin/env python
'''
Created on Nov 9, 2020 by C. Melnick
'''

import numpy as np


def Rx(theta):
    c, s = np.cos(theta), np.sin(theta)
    return np.array((
            ( 1,  0,  0),
            ( 0,  c, -s),
            ( 0,  -s,  c)
        ))

def Ry(theta):
    c, s = np.cos(theta), np.sin(theta)
    return np.array((
            ( c,  0, s),
            ( 0,  1, 0),
            (-s,  0, c)
        ))


def Rz(theta):
    c, s = np.cos(theta), np.sin(theta)
    return np.array((
            ( c,  s, 0),
            (-s,  c, 0),
            ( 0,  0, 1)
        ))

def R(theta):
    c, s = np.cos(theta), np.sin(theta)
    return np.array((
            ( c,  s),
            (-s,  c)
        ))
