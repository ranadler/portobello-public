#!/usr/bin/env python
'''
Created on Nov 12, 2020 by C. Melnick
'''

import numpy as np

def get_x_from_k(k):
    sk = np.sum(np.sign(k) + 1e-14)
    return np.linalg.norm(k) * (sk/abs(sk))

def createLabels(arpes, crystal, opts, kpath, ax):

    print("Generating labels")
    labels = None
    if opts.plane:
        #The default plane labels
        labels = [r"$k_\parallel (1/\AA)$", r"$k_\perp (1/\AA)$"]
    else:
        #The default path labels
        labels = [r"$k_\parallel (1/\AA)$", r"$\omega (eV)$"]
        
    #see if we can come up with something better
    xyz = [[1,0,0], [0,1,0], [0,0,1], [1,1,0], [0,1,1], [1,0,1] ]
    xyz_l = [r"$k_x$",r"$k_y$",r"$k_z$",r"$k_{xy}$",r"$k_{yz}$",r"$k_{zx}$"]
        
    for i in range(len(xyz)):
        xyz[i] = np.array(xyz[i]) / np.linalg.norm(xyz[i])
            
    ik = 0
    labels_to_find = [kpath.points[0].par - kpath.points[-1].par]
    if opts.plane:
        labels_to_find.append(arpes.normal)
    
    for ik, k in enumerate(labels_to_find):
        for i, x in enumerate(xyz):
            if np.abs( np.abs(np.dot(np.abs(k),x)) - np.linalg.norm(k) ) < 1e-14:
                labels[ik] = xyz_l[i]
                break
                
    ax.set_xlabel(labels[0])
    ax.set_ylabel(labels[1])
    
def placeHSKP_path(arpes, crystal, opts, kpath, extent, ax):
    k1 = kpath.points[-1].full
    k2 = kpath.points[0].full

    ak1 = get_x_from_k(k1)
    ak2 = get_x_from_k(k2)
 
    if ak1 > ak2:
        k1 = k2
        k2 = kpath.points[-1].full

    hskp = crystal.findHSKPbetween(k1, k2)

    axis = k2 - k1
    length = np.linalg.norm(axis)
    
    for label, point in hskp.items():
        f = np.linalg.norm(point - k1)/length
        x = extent[0] + f*(extent[1] - extent[0])
        dx = 0.02*(extent[1] - extent[0])
        y = extent[2] + 0.9*(extent[3] - extent[2])

        ax.axvline(x, color = "gray", linestyle = ":")    
        ax.text(x + dx, y, f"${label}$", color = "gray")
        

def placeHSKP_plane(arpes, crystal, opts, kpath, extent, ax):
    print("Placing HSKP")    

    k1_par = kpath.points[-1].par
    k2_par = kpath.points[0].par
    k1_perp = kpath.points[-1].perp * arpes.normal
    k2_perp = kpath.points[0].perp * arpes.normal

    ak1_par = get_x_from_k(k1_par)
    ak2_par = get_x_from_k(k2_par)
    ak1_perp = get_x_from_k(k1_perp)
    ak2_perp = get_x_from_k(k2_perp)
    
    if ak1_par > ak2_par:
        k1_par, k2_par = k2_par, k1_par
    
    if ak1_perp > ak2_perp:
        k1_perp, k2_perp = k2_perp, k1_perp

    hskp = crystal.findHSKPwithin(k1_par, k2_par, k1_perp, k2_perp, arpes.normal)

    x_axis = k2_par - k1_par
    x_length = np.linalg.norm(x_axis)
    
    y_axis = k2_perp - k1_perp
    y_length = np.linalg.norm(y_axis)
    
    for label, point in hskp.items():
        f = np.linalg.norm(point['par'] - k1_par)/x_length
        x = extent[0] + f*(extent[1] - extent[0])
        dx = 0.02*(extent[1] - extent[0])
        
        f = np.linalg.norm(point['perp'] - k1_perp)/y_length
        y = extent[2] + f*(extent[3] - extent[2])
        dy = 0.02*(extent[1] - extent[0])

        ax.scatter(x, y, color = "gray")
        ax.text(x + dx, y + dy, f"${label}$", color = "gray")
        
