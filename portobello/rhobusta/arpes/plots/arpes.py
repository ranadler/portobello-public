#!/usr/bin/env python
'''
Created on Nov 12, 2020 by C. Melnick
'''

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors
from scipy.interpolate import griddata
import pickle

import  portobello.rhobusta.arpes.plots.utilities as putilities

def plot(arpes, crystal, electrons, polarization, intensity, opts):
    
    if opts.plane:
        plotIrregularOnPlane(arpes, crystal, electrons, intensity, opts)
    else:
        if opts.plotIrregular:
            plotIrregularOnPath(arpes, crystal, electrons, intensity, opts)
        else:
            plotRegularOnPath(arpes, crystal, electrons, polarization, intensity, opts)

def plotRegularOnPath(arpes, crystal, electrons, polarization, intensity, opts):
    print("Plotting results")

    nrel = intensity.shape[0]
    nk = electrons.regularKpath.nk
    nbins = arpes.nbins
    
    points, values = np.zeros((nk*nbins,2)), np.zeros((nk*nbins))
    i=0
    for ik in range(nk):
        for b in range(nbins):
        
            k = electrons.regularKpath.at(ik).par
            
            points[i,0] = putilities.get_x_from_k(k)
            points[i,1] = arpes.ebins[b]

            if nrel == 1:
                values[i] = intensity[0,ik,b]
                if opts.logplot:
                    values[i] = np.log(values[i])
            else:
                if opts.subtract_spins:
                    values[i] = intensity[0,ik,b] - intensity[1,ik,b]
                else:
                    values[i] = intensity[0,ik,b] + intensity[1,ik,b]
                    #values[i] = intensity[1,ik,b]

            i+=1
                
    N = 501j
    extent = (np.min(points[:,0]), np.max(points[:,0]), arpes.ebins[0], arpes.ebins[-1])
    xs,ys = np.mgrid[extent[0]:extent[1]:N, extent[2]:extent[3]:N]
    
    fig = plt.figure(constrained_layout=True)
    ax = fig.add_subplot()
        
    resampled = griddata(points, values, (xs,ys), method = 'cubic')

    if opts.subtract_spins and nrel>1:
        divnorm=colors.TwoSlopeNorm(vmin=np.min(resampled), vcenter=0, vmax=np.max(resampled))
    else:
        divnorm = None

    cs = ax.imshow(resampled.T, extent=extent,  origin='lower', aspect = 'equal', cmap = opts.cmap, norm = divnorm)
    ax.set_aspect( (np.max(points[:,0]) - np.min(points[:,0])) / (np.max(points[:,1]) - np.min(points[:,1])) )
    #fig.colorbar(cs, ax=ax)
    putilities.createLabels(arpes, crystal, opts, electrons.regularKpath, ax)
    putilities.placeHSKP_path(arpes, crystal, opts, electrons.regularKpath, extent, ax)
    
    if opts.spectral_only:
        pol_str = "non-pol"
    else:
        pol_str = ",".join([str(round(e.real,3)) for e in polarization])
    with open(f'ARPES.{pol_str}.path.pickle','wb') as f:
        pickle.dump(fig,f)

def plotIrregularOnPath(arpes, crystal, electrons, intensity, opts):
    print("Plotting results")
    
    ntheta = arpes.n_theta_k
    nbins = arpes.nbins
    
    points, values = np.zeros((ntheta*nbins,2)), np.zeros((ntheta*nbins))
    i=0
    for th in range(ntheta):
        for b in range(nbins):
        
            k = electrons.irregularKpath.at(th,b).par
            
            points[i,0] = putilities.get_x_from_k(k)
            points[i,1] = arpes.ebins[b]

            values[i] = intensity[th,b,0]
            if opts.logplot:
                values[i] = np.log(values[i])

            i+=1
            
    N = 501j
    extent = (np.min(points[:,0]), np.max(points[:,0]), arpes.ebins[0], arpes.ebins[-1])
    xs,ys = np.mgrid[extent[0]:extent[1]:N, extent[2]:extent[3]:N]
    
    fig = plt.figure(constrained_layout=True)
    ax = fig.add_subplot()
        
    resampled = griddata(points, values, (xs,ys), method = 'cubic')
    ax.imshow(resampled.T, extent=extent,  origin='lower', aspect = 'equal', cmap = opts.cmap)
    ax.set_aspect( (np.max(points[:,0]) - np.min(points[:,0])) / (np.max(points[:,1]) - np.min(points[:,1])) )
    
    putilities.createLabels(arpes, crystal, opts, electrons.regularKpath, ax)
    
    putilities.placeHSKP_path(arpes, crystal, opts, electrons.regularKpath, extent, ax)
    
    with open('ARPES.path.pickle','wb') as f:
        pickle.dump(fig,f)     

   

def plotIrregularOnPlane(arpes, crystal, electrons, intensity, opts):
    print("Plotting results")

    ntheta = arpes.n_theta_k
    nbins = arpes.nbins
    nkperp = electrons.regularKpath.nk_per_irregular_point
    n = ntheta*nkperp
    
    points, values = np.zeros((n,2)), np.zeros((n))
    i,b=0,0
    for th in range(ntheta):
        indx_k = 0
        for ik in electrons.regularKpath.map_irregular_to_regularized(th,b):
                
            kx = electrons.irregularKpath.at(th,b).par
            ky = electrons.regularKpath.at(ik).perp

            points[i,0] = putilities.get_x_from_k(kx)
            points[i,1] = ky
            
            values[i] = intensity[th,b,indx_k]
                 
            indx_k +=1
            i+=1
    
    N = 501j
    extent = (np.min(points[:,0]), np.max(points[:,0]),np.min(points[:,1]), np.max(points[:,1]))
    xs,ys = np.mgrid[extent[0]:extent[1]:N, extent[2]:extent[3]:N]
    
    fig = plt.figure(constrained_layout=True)
    ax = fig.add_subplot()
        
    resampled = griddata(points, values, (xs,ys), method = 'cubic')
   
    ax.imshow(resampled.T, extent=extent,  origin='lower', aspect = 'equal', cmap = opts.cmap)
    ax.set_aspect( (np.max(points[:,0]) - np.min(points[:,0])) / (np.max(points[:,1]) - np.min(points[:,1])) )
    
    putilities.createLabels(arpes, crystal, opts, electrons.regularKpath, ax)
    
    putilities.placeHSKP_plane(arpes, crystal, opts, electrons.regularKpath, extent, ax)
    
    with open('ARPES.plane.pickle','wb') as f:
        pickle.dump(fig,f)



