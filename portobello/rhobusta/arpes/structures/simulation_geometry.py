'''
Created on Nov 9, 2020 by C. Melnick
'''

import numpy as np
from argparse import ArgumentDefaultsHelpFormatter
from portobello.rhobusta.options import ArgumentParserThatStoresArgv

import portobello.rhobusta.arpes.utilities.rotationMatrices as RotationMatrices
     
class ArpesGeometry: #See fig. 3 of Moser 2017 for nomenclature
    
    @classmethod
    def arguments(cls, add_help=False):
        
        parser = ArgumentParserThatStoresArgv("Arpes Geometry", add_help=add_help,
                                formatter_class=ArgumentDefaultsHelpFormatter)
                                    
        
        group = parser.add_argument_group("ARPES experiment [enter angles in degrees by appending 'deg' to the end of the argument]")
    
        group.add_argument("--alpha", dest="alpha",
                     default=np.pi/2., type=float,
                     help="Angle between beam and analyzer [radians]")
                                
        group.add_argument("--xi", dest="xi",
                     default=0., type=float,
                     help="Polarization ratio between principal components [radians]")
                     
        group.add_argument("--delta", dest="delta",
                     default=0, type=float,
                     help="Relative phase between principal components of polarization [radians]")
                          
        group.add_argument("--theta", dest="theta",
                     default=0, type=float,
                     help="Rotation of the material surface about the x-axis (applied first) [radians]")
                        
        group.add_argument("--beta", dest="beta",
                     default=0, type=float,
                     help="Rotation of material surface about the y-axis (applied second) [radians]")
                     
        group.add_argument("--phi", dest="phi",
                     default=0, type=float,
                     help="Rotation of material surface about the z-axis (applied third) [radians]")
                    
        #some helper options
        group.add_argument("--right-circular-polarized", dest="is_right_circular_polarized",
                    action="store_true",
                    default = False,
                    help="Set cicularly polarized light (right-handed) [sets --xi=pi/4 and --delta=pi/2]")
                    
        group.add_argument("--left-circular-polarized", dest="is_left_circular_polarized",
                    action="store_true",
                    default = False,
                    help="Set cicularly polarized light (left-handed) [sets --xi=-pi/4 and --delta=pi/2]")
                    
        group.add_argument("--sigma_polarized", dest="is_sigma_polarized",
                    action="store_true",
                    default = False,
                    help="Set cicularly polarized light [sets --xi=-0 and --delta=0]")
                            
        group.add_argument("--pi_polarized", dest="is_pi_polarized",
                    action="store_true",
                    default = False,
                    help="Set cicularly polarized light [sets --xi=-pi/2 and --delta=0]")
                    
        group.add_argument("--explicit-polarization", dest="polarization",
                    default = None,
                    help="Set polarization (rather than the various angles and phases)"
                    )
                                             
        group.add_argument("--dipole", dest="dipole",
                    action="store_true",
                    default = False,
                    help="Use the dipole approximation \epsilon \dot r, instead of \epsilon \dot \nabla [untested]")
     
                          
        return parser
    
    def debug(self):
        print (f"theta, beta: {self.theta}, {self.beta}")
        print (f"for normal vector {self.normal}")
        
    def orient(self,to_rotate):
        return np.dot(to_rotate,self.rotation_mat)
        
    def __init__(self, svs, opts):
                
        #the structure has as attributes all of its options
        defaults = ArpesGeometry.arguments().parse_args([])
        for k in defaults.__dict__:
            setattr(self, k, opts.__dict__[k])
            
        assert self.is_right_circular_polarized + self.is_left_circular_polarized + self.is_sigma_polarized + self.is_pi_polarized <= 1
        
        if self.is_right_circular_polarized:
            self.xi = np.pi/4
            self.delta = np.pi/2
                        
        elif self.is_left_circular_polarized:
            self.xi = -np.pi/4
            self.delta = np.pi/2
                        
        elif self.is_sigma_polarized:
            self.xi = 0
            self.delta = 0
                        
        elif self.is_pi_polarized:
            self.xi = np.pi/2
            self.delta = 0
                    
        if opts.plane:
            self.nbins = 1
            self.window = opts.window
            self.ebins = np.array([float(self.window)]) #not sure why this float(x) is necessary
            
        else:
            omegas = svs.sig.omega
            def GetOmegasInWindow():
            
                indx_m = np.argmax(omegas>=opts.window_minus)
                indx_p = np.argmax(omegas>opts.window_plus)
                if indx_p < indx_m: indx_p = len(omegas)
            
                return omegas[indx_m:indx_p]

            self.ebins = GetOmegasInWindow()
            assert omegas[0] == self.ebins[0], f"{omegas[0]} != {self.ebins[0]}"
            
            self.nbins = len(self.ebins)
            self.window = self.ebins[0]
            self.window_plus = self.ebins[-1]
        
        self.rotation_mat = np.dot( np.dot( RotationMatrices.Rx(self.theta), RotationMatrices.Ry(self.beta)), RotationMatrices.Rz(self.phi))
        
        self.normal = self.orient([0,0,1])
        
