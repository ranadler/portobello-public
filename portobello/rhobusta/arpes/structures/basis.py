'''
Created on Nov 9, 2020 by C. Melnick
'''
import numpy as np
from scipy import integrate
import itertools

class Basis:

    def __init__(self, nf, nrdiv, origin, span, centers, data, center_mesh = False):
        self.nf = nf
        self.nrdiv = nrdiv
        
        self.origin = origin
        self.span = span
        
        self.centers = centers
        
        self.data = data

        
        self.mesh = self.generate_realspace_mesh(center_mesh)
    
    def kdr_centered_on_func_i(self, ks, i):
    
        self.mesh -= self.centers[i]
        kdr =  np.tensordot(ks, self.mesh, axes=(1,3))
        self.mesh += self.centers[i]
        
        return kdr
        
    def adjust_by_dipole(self, polarization):
        
        r = np.copy(self.data)
    
        for i in range(self.nf):
            self.mesh -= self.centers[i]
            edr = np.tensordot(polarization, self.mesh, axes=(0,3))
            self.mesh += self.centers[i]
            
            r[i] = edr * self.data[i]
        
        return r
    
    def compute_overlap_integrals(self, crystal, n=2):
        '''
        compute overlap between basis function and itself centered at 0 and R_i
        n = number of lattice vectors (x,y,z / + and -) to compute integrals for, i.e., compute N = (2*n+1)**3 integrals
        returns associated list of overlaps and R's
        '''
        nLatVecInSpan = np.array( [self.span[i,:] / (crystal.structure.lattice.matrix[i,:] + 1e-14) for i in range(3)])

        nrdiv = np.zeros((3,3),dtype=np.int)
        for i in range(3):
            nrdiv[i,i] = self.nrdiv[i]
        
        offset_basis = np.zeros((3,3), dtype=np.int)
        offset_basis = nrdiv / np.diag(nLatVecInSpan) + 0.5
        
        N = (2*n+1)**3
        offsets = np.zeros((N, 3), dtype = int)
        rs = np.zeros((N, 3), dtype = float)
        x = range(-n,n+1)
        
        for idx, (i, j, k) in enumerate(itertools.product(x,repeat=3)):
            offsets[idx] = i*offset_basis[0] + j*offset_basis[1] + k*offset_basis[2]
            rs[idx] = i*crystal.structure.lattice.matrix[i] + j*crystal.structure.lattice.matrix[j] + k*crystal.structure.lattice.matrix[k]
        
        overlap = np.zeros((self.nf, N), dtype=np.complex)
        for idx, r in enumerate(offsets):
            
            rf = np.abs(r)
            rb = self.nrdiv - rf
            overlap_integral = (np.conj(self.data[:, rf[0]:,rf[1]:,rf[2]:])
                                      * self.data[:, :rb[0],:rb[1],:rb[2]])
                    
            for dim in range(3):
                overlap_integral = integrate.simps(overlap_integral)
            
            overlap[:,idx] = overlap_integral
        
        return overlap, rs
        
        
    def generate_realspace_mesh(self, center = False):
        dirs = np.array([ self.span[i] / np.linalg.norm(self.span[i]) for i in range(3)])
        xs = [dirs[i] * np.delete(np.linspace( self.origin[i], self.origin[i] + self.span[i], self.nrdiv[i] + 1, endpoint = True ), 0, axis=0) for i in range(3)]
        
        if center:
            for i in range(3):
                xs[i] -= xs[i][int(nrdiv[i]/2)-1]
            
        rs = np.zeros( (self.nrdiv[0],self.nrdiv[1],self.nrdiv[2],3) )
            
        for iz,z in enumerate(xs[2]):
            for iy,y in enumerate(xs[1]):
                for ix,x in enumerate(xs[0]):
                    rs[ix,iy,iz,:] = x + y + z
                        
        return rs
