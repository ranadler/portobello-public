'''
Created on Nov 9, 2020 by C. Melnick
'''
import numpy as np

from portobello.rhobusta.arpes.utilities.physicalConstants import PhysicalConstants
import portobello.rhobusta.arpes.utilities.rotationMatrices as RotationMatrices

class Photons:
        
    def __init__(self, arpes, electrons, opts):
        
        def polarization(alpha, delta, xi):
            
            return np.array([
                    np.cos(xi),
                    np.exp(1j*delta)*np.cos(alpha)*np.sin(xi),
                    np.exp(1j*delta)*np.sin(alpha)*np.sin(xi)
                ])
                
        self.omega = opts.omega
        
        if opts.polarization is not None:
            self.unrotated_polarization = np.array([float(s) for s in opts.polarization.split(",")])
            self.unrotated_polarization = self.unrotated_polarization / np.linalg.norm(self.unrotated_polarization)
        else:
            self.unrotated_polarization = polarization(arpes.alpha, arpes.delta, arpes.xi)

        self.polarization = np.zeros(electrons.kpath.shape, dtype=np.complex)

        nk = len(electrons.kpath)
        for ik, k in enumerate(electrons.kpath):
            offset=0

            k = electrons.kpath[(ik+offset) % nk]
            kperp = np.dot(k, arpes.normal) * arpes.normal
            kpar = k - kperp

            x = arpes.orient(np.array((1,0,0)))
            y = arpes.orient(np.array((0,1,0)))

            kx = np.dot(x,kpar)
            ky = np.dot(y,kpar)

            phi = np.arctan2(kx,ky)

            self.polarization[ik,:] = np.dot(self.unrotated_polarization, RotationMatrices.Rz(phi) )
            self.polarization[ik,:] /= np.linalg.norm(self.polarization[ik,:])


        #kmag = self.omega * 2 / PhysicalConstants.Rydberg 
        
        #self.k = kmag * arpes.orient( np.array((0,0,-1)) )
        #self.k[:2] = np.dot(self.k[:2], RotationMatrices.R(arpes.phi_k))
        
        #self.kperp = np.dot(self.k, arpes.normal)
        #self.kpar = self.k - self.kperp * arpes.normal
        
        
        
