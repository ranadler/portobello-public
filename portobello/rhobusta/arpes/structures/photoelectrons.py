'''
Created on Nov 9, 2020 by C. Melnick
'''
import numpy as np

from portobello.rhobusta.arpes.utilities.physicalConstants import PhysicalConstants

import portobello.rhobusta.arpes.structures.kpath as kpath
    
class Photoelectrons:

    #compute all photoelectrons at theta_k[i] emitted from energy bins ebins
    def __init__(self, arpes, electrons, photons):
        self.ekin = photons.omega - electrons.wf - np.array(arpes.ebins)
        kmag = np.sqrt((np.average(self.ekin)) * 2 / PhysicalConstants.Rydberg)

        self.kpath = np.zeros_like(electrons.kpath)
        
        #bound electron k's
        for ik, k in enumerate(electrons.kpath):
            kperp = np.dot(k, arpes.normal) * arpes.normal
            kpar = k - kperp
            
            #photoelectron k's
            kperp = np.sqrt(kmag**2 - np.linalg.norm(kpar)**2) * arpes.normal
            self.kpath[ik] = kperp - kpar


