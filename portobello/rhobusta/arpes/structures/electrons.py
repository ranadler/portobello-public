'''
Created on Nov 9, 2020 by C. Melnick
'''

import numpy as np

class Electrons:
    
    def __init__(self, kpath, opts):
        
        self.wf = opts.wf
        #self.V0 = opts.V0
        self.decay = opts.decay
        
        self.imagInvDecay = 1j / self.decay
        self.invDecay = 1. / self.decay

        self.kpath = np.zeros((kpath.num_k, 3), dtype=float)
        self.num_k = kpath.num_k

        self.kpath[:,:] = np.transpose(kpath.k) * 2*np.pi 

        #Give a small direction to get appropariate matrix elements / polarization's at Gamma
        for ik, k in enumerate(self.kpath):
            if np.linalg.norm(k) < 1e-8:
                if ik == 0:
                    self.kpath[ik,:] = self.kpath[ik+1,:]*0.001
                    
                elif ik == self.num_k - 1:
                    self.kpath[ik,:] = self.kpath[ik-1,:]*0.001

                else:
                    self.kpath[ik,:] = (self.kpath[ik-1,:] + self.kpath[ik+1,:]) * 0.001
    
        

