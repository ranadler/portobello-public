'''
Created on Nov 9, 2020 by C. Melnick
'''

#DEPRECATED

import numpy as np

from pymatgen.symmetry.bandstructure import HighSymmKpath
from portobello.rhobusta.arpes.utilities.physicalConstants import PhysicalConstants

class Point:
    
    def __init__(self, full, perp, par):
        self.full = full #full k-vector
        self.perp = perp #magnitude in the normal direction
        self.par = par #vector parallel to the surface

        
class Path: #Also plane
    
    def __init__(self, points, map = None):
        '''
        points = list of points in the path / plane (list of points)
        map = th,b -> list of ik which conserve momentum (nested list of ints)
        '''
        self.nk = len(points)
        
        self.points = points
        self.map = map
        
        if map is not None:
            self.nk_per_irregular_point = len(self.map[0,0]) #map is not (necessarily) 1-to-1
        
        '''
        irr_points = list of points in the path / plane (list of points) which are irreducible
        irr_map = ik -> ik_irr
        initialize these with generateIrreduciblePath(self,crystal)
        '''
        self.irr_nk = None
        self.irr_points = None
        self.irr_map = None
    
    def toFile(self, structure, filename = "kpath.dat", write_fractional = False, write_irreducible = False):
        print("Writing kpath to file")
        
        points = self.irr_points if write_irreducible else self.points
        
        if points == None and write_irreducible:
            raise Exception("Must generate irreducible path before attempting to write it out")
        
        with open(filename, "w") as f:
                
            f.write(f"{len(points)}\n")
            
            for k_cart in points:
               
                k = k_cart.full
                
                if write_fractional:
                    k = structure.lattice.reciprocal_lattice.get_fractional_coords(k)
                                
                f.write(f"{k[0]} {k[1]} {k[2]}\n")
    
    def at(self, ik):
        return self.points[ik]
    
    def irr_at(self, ik):
        return self.irr_points[self.irr_map[ik]]
            
    def map_irregular_to_regularized(self,th,b):
        return self.map[th,b]
    
    def generateIrreduciblePath(self, crystal):
        
        irr_points, irr_map = [], []
        for point in self.points:
            symm_points = [op.operate(point.full) for op in crystal.symmOps]
            
            equiv_to_irr_point = [ any( [np.allclose(symm_point, p.full) for symm_point in symm_points ]) for p in irr_points]
                
            if not any(equiv_to_irr_point):
                irr_points.append(point)
                irr_map.append(len(irr_points) - 1)
            else:
                irr_map.append(np.argwhere(equiv_to_irr_point))
        
        self.irr_nk = len(irr_points)
        self.irr_points = irr_points
        self.irr_map = irr_map
                
    @classmethod
    def fromIrregularPath(cls, irregularPath, arpes, crystal, kfactor = 1, is_plane = False, debug = False):
        '''
        This regularizes a mesh -- placing it into a linearly spaced bins, generating a Path w/ map
        irregularPath -- theta,b -> point; see below
        kfactor -- number
            
        plane: create a square mesh of momenta to look at (one axis along arpes.normal, one along parallel component of irregularPath)
        path: follow the irregular path provided
        '''
            
        assert irregularPath.ntheta == arpes.n_theta_k and irregularPath.nbins == arpes.nbins, print(irregularPath.nbins, arpes.nbins, irregularPath.ntheta, arpes.n_theta_k)
        
        nk = int(irregularPath.ntheta * kfactor) - 1

        ks = np.zeros((nk,3))
        for th in range(arpes.n_theta_k - 1):
            start = int(th * kfactor)
            end = start + kfactor
            ks[start:end] = np.linspace(irregularPath.at(th,0).full, irregularPath.at(th+1,0).full, kfactor, endpoint = False)
            
        ks[-1] = irregularPath.at(-1,0).full
        
        map = np.empty((arpes.n_theta_k, arpes.nbins), dtype=object)
        
        for th in range(arpes.n_theta_k):
            for b in range(arpes.nbins):
            
                condition = [np.linalg.norm(ks[i] - irregularPath.at(th,b).full) for i in range(nk)]
            
                ik = np.argmin(condition)
                
                if is_plane:
                    map[th,b] = [ik + jk*nk for jk in range(nk)]
                else:
                    map[th,b] = [ik]
        
        kperp = None
        kpar = None
        mkperp = None
        if is_plane:
        
            mkpar = [np.linalg.norm(ks[i]) for i in range(nk)]
            
            mkperp_max = np.max( np.abs( np.tensordot( crystal.hskp_path, arpes.normal, axes=(1,0) ) ) )
            
            mperp = np.max(mkpar)
            #dk = mperp / nk
            
            mperp = min( mperp, mkperp_max )
            #nk = int(mperp / dk + 0.5)
            #TODO: if I want to keep nk consant like this, I neeed to change map
            
            ksperp = np.linspace(- mperp * arpes.normal, mperp * arpes.normal, nk, endpoint = True)
            mksperp = np.linspace(- mperp, mperp, nk, endpoint = True)
       
            kpar, kperp, mkperp = np.zeros((nk*nk,3)), np.zeros((nk*nk,3)), np.zeros(nk*nk)
            
            i=0
            for jk in range(nk):
                for ik in range(nk):
                    kpar[i] = ks[ik]
                    kperp[i] = ksperp[jk]
                    mkperp[i] = mksperp[jk]
                    
                    i += 1
            
        else:
        
            mkperp = [np.dot(ks[ik], arpes.normal) for ik in range(nk)]
            kperp = [mkperp[ik] * arpes.normal for ik in range(nk)]
            kpar = ks - kperp
                
        return cls.fromVectors( kperp + kpar, mkperp, kpar, map = map)
    
    @classmethod
    def fromVectors(cls, vfull, vperp, vpar, map = None):
        nk = len(vfull)
        points = [Point(vfull[i], vperp[i], vpar[i]) for i in range(nk)]
        return cls(points, map = map)

class IrregularPath:
    
    def __init__(self, points, arrayOfFull):
        '''
        points = list of list of points in the mesh
        full-mesh = ks in numpy form -- np.array(nkx,nky,3)
        '''
        self.ntheta = len(points)
        self.nbins = len(points[0])
        self.points = points
        self.arrayOfFull = arrayOfFull
        
    @classmethod
    def fromArray(cls, mfull, mperp, mpar):
        ntheta = len(mfull)
        nbins = len(mfull[0])
        points = [[Point(mfull[th,b], mperp[th,b], mpar[th,b]) for b in range(nbins)] for th in range(ntheta)]
        return cls(points, mfull)
            
    def at(self, th, b):
        return self.points[th][b]
            
        
