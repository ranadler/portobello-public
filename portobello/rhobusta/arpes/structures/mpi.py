'''
Created on Nov 9, 2020 by C. Melnick
'''

from mpi4py import MPI
        
class Parallel:
    
    def __init__(self):
        
        self.comm = MPI.COMM_WORLD
        self.num_procs = self.comm.Get_size()  # Stores the number of processes in size.
        self.rank = self.comm.Get_rank()
        self.master = self.rank == 0
    
    def print(self,*args):
        if self.master:
            args = [f"{a}" for a in args]
            out = ' '.join(args)
            print(out, flush = True)
      
