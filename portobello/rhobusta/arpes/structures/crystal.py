'''
Created on Nov 9, 2020 by C. Melnick
'''

import numpy as np
import pymatgen

from portobello.rhobusta.arpes.utilities.physicalConstants import PhysicalConstants


class Crystal:
    
    def __init__(self, structure):
        
        self.structure = structure
        self.coordinates =  [structure.lattice.get_cartesian_coords(c) for c in structure.cart_coords] #cartesian
        self.symm = pymatgen.symmetry.analyzer.SpacegroupAnalyzer(structure, )
        self.symmOps = self.symm.get_point_group_operations(cartesian=True)
        self.hskp = pymatgen.symmetry.bandstructure.HighSymmKpath(structure)
        self.hskp_path, self.hskp_labels = self.hskp.get_kpoints(line_density=1, coords_are_cartesian=True)

        def stripNonSymmPoints(points, labels):
            i = 0
            entries=[]
            for label in labels:
                if label == '':
                    entries.append(i)
                i += 1
                
            for i in reversed(entries):
                points.pop(i)
                labels.pop(i)
            
            return points, labels
                
        self.hskp_path, self.hskp_labels = stripNonSymmPoints(self.hskp_path,self.hskp_labels)
        self.uhskp_path, indx = np.unique(np.array(self.hskp_path).round(decimals=10),axis=0,return_index=True)
        self.uhskp_labels = [self.hskp_labels[i] for i in indx]

    # Construct lists of R and G
    def constructLatticeMeshes(self,n):
        factors = list(range(-n, n + 1))
        factors.remove(0)
        
        self.recip_lat_vec_mesh = [np.array((0,0,0))]
        self.lat_vec_mesh = [np.array((0,0,0))]
        for f in factors:
            for r in self.structure.lattice.reciprocal_lattice.matrix:
                self.recip_lat_vec_mesh.append( f*r )
            for r in self.structure.lattice.matrix:
                self.lat_vec_mesh.append( f*r )
                
    
    # Compute parallel part of reciprocal lattice mesh
    def computeParallelComponents(self,arpes,n=1):
        
        if not hasattr(self,"recip_lat_vec_mesh"):
            self.constructLatticeMeshes(n)
        
        self.Gpar = []
        
        for g in self.recip_lat_vec_mesh:
            gperp = np.dot(arpes.normal, g)*arpes.normal
            self.Gpar.append( g - gperp )
        
        def unique(array_list, rtol=1e-7, atol=1e-8):
            uniques = []
            for arr in array_list:
                if not any(np.allclose(arr, unique_arr, rtol=rtol, atol=atol) for unique_arr in uniques):
                    uniques.append(arr)
            return uniques
        
        self.Gpar = unique(self.Gpar)
        
    #Is this rhobust?
    def intoBZ(self, vec, parallel_only = False):
       
        g_list = self.Gpar if parallel_only else self.recip_lat_vec_mesh
        
        v2 = np.dot(vec,vec)
        
        gsave = [0,0,0]
        for g in g_list:
            
            vmg = vec-g
                
            if v2 > np.dot(vmg,vmg) + 1E-14:
                vec = vmg
                v2 = np.dot(vec,vec)
                
        return vec
        
    def getSegmentFromLabelPair(self, label):
        return self.highSymmLineSegments.getSegmentFromLabelPair(label)
    
    def findHSKPbetween(self,k1,k2):
        segment = LineSegment("","",k1,k2)
   
        points = {}
        for ik, k in enumerate(self.uhskp_path):
            repeated_check=[]
            for op in self.symmOps:
                symmk_ = op.operate(k)
                for symmk in [symmk_,-symmk_]:
                    check = [np.allclose(symmk, x, rtol = 1e-10) for x in repeated_check]
                    if segment.isWithinSegment(symmk) and not any(check):
                        key = self.uhskp_labels[ik]
                        while key in points.keys(): key += '\''
                        points[key] = symmk
                        repeated_check.append(symmk)
                    
            
        return points
        
    def findHSKPwithin(self,k1_par,k2_par,k1_perp,k2_perp, normal):
    
        segment_par = LineSegment("","",k1_par,k2_par)
        segment_perp = LineSegment("","",k1_perp,k2_perp)
   
        points = {}
        for ik, k in enumerate(self.uhskp_path):
            repeated_check=[]
            for op in self.symmOps:
                symmk_ = op.operate(k)
                for symmk in [symmk_,-symmk_]:
                    symmk_perp = np.dot(symmk, normal)*normal
                    symmk_par = symmk - symmk_perp
                    check = [np.allclose(symmk, x, rtol = 1e-10) for x in repeated_check]
                    if segment_perp.isWithinSegment(symmk_perp) and segment_par.isWithinSegment(symmk_par) and not any(check):
                        key = self.uhskp_labels[ik]
                        while key in points.keys(): key += '\''
                        points[key] = {'par':symmk_par, 'perp':symmk_perp}
                        repeated_check.append(symmk)
        
            
        return points
            
class LineSegment:
            
    eps = 1E-8
        
    def __init__(self, aLabel, bLabel, a, b):
        self.aLabel = aLabel
        self.bLabel = bLabel
        self.a = a
        self.b = b
            
        self.dif = b - a
        self.length2 = np.dot(self.dif, self.dif)
        self.length = np.sqrt(self.length2)
                
    def isCollinear(self, c):
        return np.linalg.norm(np.cross(c - self.a, self.dif)) < self.eps
                
    def isBetween(self, c):
        dp = np.dot(c - self.a, self.dif)
        return dp > 0 and dp < self.length2
            
    def isWithinSegment(self, c):
        return self.isCollinear(c) and self.isBetween(c)
                    
    def operateOn(self,op):
        self.a = op.operate(self.a)
        self.b = op.operate(self.b)
            
    def projectOnto(self,vec):
        return np.dot(vec, self.dif) / np.sqrt(self.length) * self.dif

        
