'''
Created on Nov 19, 2020 by C. Melnick
'''
from itertools import product
import sys
import numpy as np

from scipy.special import jv, sph_harm, spherical_jn
from scipy.integrate import simps
from sympy.physics.wigner import gaunt
from sympy.physics.quantum.cg import CG
from portobello.bus.Matrix import Matrix
from portobello.generated.FlapwMBPT_basis import InterstitialBasis, MuffinTinBasis
from portobello.generated.FlapwMBPT_interface import DFT
from portobello.rhobusta.PEScfEngine import Ry2eV
from portobello.rhobusta.arpes.utilities.physicalConstants import PhysicalConstants
from portobello.rhobusta.basis import OrbitalEvaluator, OrbitalEvaluatorContainer
from portobello.rhobusta.observables import PrintMatrix

from portobello.symmetry.local import GetRealSHMatrix, GetCGMatrix
        
        
# The purpose of this class is to help iterate through the basis
# It keeps track of the current quantum number
#    (i.e., those associated with a atom, n, kind, and l or j)
# Organizing an integrator for each unique combination
# It provides the results for the correct integration when asked
class Organizer(OrbitalEvaluatorContainer):
    
    def __init__(self, dft, lapw, electrons, photoelectrons, proj, opts):
        self.is_dipole = opts.dipole
        self.photoelectrons = photoelectrons
        
        if self.is_dipole:
            self.pw_expansion = PW_expansion(opts)
        
        OrbitalEvaluatorContainer.__init__(self, dft, lapw, prepare_for_spherical_coordinates=False)

        if self.is_relativistic:
            pass
        else:
            #self.interstitialIntegrator = NonRelInterstitialIntegrator(electrons, photoelectrons, dft, self.basis.tins, proj.min_band, proj.max_band)
            pass
        
    def emplace(self, iorb):
        
        is_new = OrbitalEvaluatorContainer.emplace(self, iorb, None)

        if 1 or is_new:
            if self.is_relativistic:
            
                if self.is_dipole:
                    #integrator = RelDipoleIntegrator(quantum_numbers, photoelectrons, shell.radial_mesh, g_radial_function, f_radial_function, self.pw_expansion)
                    raise NotImplementedError()
                else:
                    integrator = RelGradientIntegrator(self.current_evaluator.quantum_numbers, self.photoelectrons, 
                        self.current_evaluator.radial_mesh, self.current_evaluator.radial_function, self.current_evaluator.f_radial_function)
                    
            else:
                radial_function = self.current_evaluator.radial_function
                
                if self.is_dipole:
                    integrator = NonRelDipoleIntegrator(self.current_evaluator.quantum_numbers, self.photoelectrons, self.current_evaluator.radial_mesh, radial_function, self.pw_expansion)
        
                else:
                    integrator = NonRelGradientIntegrator(self.current_evaluator.quantum_numbers, self.photoelectrons, self.current_evaluator.radial_mesh, radial_function)
        
        else:
            integrator = self.orbital_evaluators[self.current_description].integrator

        self.current_evaluator.integrator = integrator
        self.current_integrator = integrator
        
    def conduct_interstitial(self, up_down_or_total = -1):
        if self.is_dipole:
            return 0
        else:
            if up_down_or_total < 0:
                return np.sum(self.interstitialIntegrator.overlap[up_down_or_total,:,:], axis=0)
            else:
                return self.interstitialIntegrator.overlap[up_down_or_total,:,:]
        
    def conduct(self, ik, iorb, Z, phase_factor, reordered_polarization, up_down_or_total = -1):
            
        if self.is_dipole:

            for ipw, (l_pw, m_pw) in enumerate(self.pw_expansion.lm_list):
                for idir, pol in enumerate(reordered_polarization):
                    res = self.current_integrator.radial(ik, l_pw) * self.current_integrator.spherical(ik, ipw, idir, self.current_qn.indices()) * phase_factor * pol
                    return Z[iorb,:,ik,0] * res
                        
        else:
            res = self.current_integrator.at(ik,self.current_qn.indices(), up_down_or_total=up_down_or_total) * phase_factor
            return Z[iorb,:,ik,0] * res
            

    def k_at(self,ik):
        return self.current_integrator.kmesh[ik]   
            
class InterstitialIntegrator:

    class TinInfo:
        def __init__(self, radius, position):
            self.radius = radius
            self.position = position
            self.volume = 4/3*np.pi*radius**3


    def __init__(self, electrons, photoelectrons, dft : DFT, tins : MuffinTinBasis, min_band, max_band):

        self.k_minus_kf_mesh = electrons.kpath - photoelectrons.kpath
        
        self.nk = len(self.k_minus_kf_mesh)
        self.num_si = dft.num_si
        self.nrel = dft.nrel

        self.tins = self.get_radii(dft,tins)
        
        loc = "./basis.tmp.h5:/"
        self.basis = InterstitialBasis()
        #self.basis.GetInterstitialBasis(loc, min_band, max_band) TODO: This gets on IBZ, not kpath
        self.basis.load(loc)

        self.volume = abs(np.dot(np.cross(dft.st.a, dft.st.b), dft.st.c))
        self.overlap = self.volume * np.ones((dft.nrel+dft.num_si, self.nk, self.basis.num_expanded), dtype=np.complex) #integral over the whole cell
        self.conduct()

    def conduct(self):
        return self.overlap # the result if we have no muffin tins

    def get_radii(self, dft : DFT, tins : MuffinTinBasis):
        tin_dict = {}
        for at in range(dft.st.num_atoms):
            kind = dft.st.distinct_number[at]
            tin_dict[at] = InterstitialIntegrator.TinInfo( tins[kind-1].radial_mesh[-1], dft.st.xyz[:,at])
        return tin_dict

class NonRelInterstitialIntegrator(InterstitialIntegrator):

    def conduct(self):
        for ik in range(self.nk):
            for ig in range(self.basis.num_vectors_at_k[ik]):
                ig0 = self.basis.vector_index[ig,ik]-1
                g = self.basis.reciprical_lattice_vectors[:,ig0] 
                kpg = self.k_minus_kf_mesh[ik] + g
                norm = np.linalg.norm(kpg)

                for key in self.tins.keys():
                    if (norm<1e-8):
                        for si in range(self.num_si):
                            self.overlap[si,ik,:] -= tin.volume * self.basis.A[ig,:,ik,si] 

                    else:
                        tin = self.tins[key]
                        x = tin.radius * norm
                        sx = np.sin(x)
                        xcx = x*np.cos(x)
                        x3 = x**3

                        phase = np.exp(1j*np.dot(kpg, tin.position))
                        
                        for si in range(self.num_si):
                            self.overlap[si,ik,:] -= phase* self.basis.A[ig0,:,ik,si] * 3*tin.volume*(sx-xcx)/x3 

        return self.overlap


class Integrator:
    
    def __init__(self, quantum_numbers, photoelectrons, radial_mesh, radial_function, dradial_function = None):
    
        self.rmesh = radial_mesh * PhysicalConstants.angstrom2bohr
        self.nr = len(self.rmesh)
        
        self.kmesh = -photoelectrons.kpath
        self.kmagn = np.array([np.linalg.norm(k) for k in self.kmesh])
        
        self.nk = len(self.kmesh)
        
        self.radial_function = radial_function
        
        if quantum_numbers.is_relativistic:
            self.quantum_numbers = quantum_numbers
        
        else:
            self.l = quantum_numbers.l
            
            self.nm = 2*self.l + 1
            self.basis_ms = list(range(-self.l,self.l+1))
            self.dir_ms = [-1,0,1]        
        
class NonRelGradientIntegrator(Integrator):
    
    def __init__(self, quantum_numbers, photoelectrons, radial_mesh, radial_function):

        Integrator.__init__(self, quantum_numbers, photoelectrons, radial_mesh, radial_function)
        self.get_radial_coefficients()
        self.get_spherical_harmonic_coefficients()
        
    def radial(self, ik):
        return self.radial_coefficients[ik]
        
    def spherical(self, ik, im):
        return self.sph_harm_coefficients[ik,im]
        
    def at(self,ik,im, up_down_or_total = -1):
        return self.radial(ik) * self.spherical(ik,im)
        
    #internal
    def get_radial_coefficients(self):
    
        jkrre = np.zeros((self.nk, self.nr ), dtype=float)
        for ir, r in enumerate(self.rmesh):
            jkrre[:, ir] = spherical_jn(self.l, r * self.kmagn) * r**2
                
        self.radial_coefficients = np.zeros((self.nk), dtype = np.complex)
        
        for ik in range(self.nk):
            self.radial_coefficients[ik] = (2*self.l+1)*(1j)**self.l * simps( jkrre[ik,:] * self.radial_function, x = self.rmesh)
            
    def get_spherical_harmonic_coefficients(self):
        self.sph_harm_coefficients = np.zeros((self.nk, self.nm), dtype = np.complex)
            
        azimuth = np.arctan2(self.kmesh[:,1], self.kmesh[:,0])
        polar = np.arctan2(self.kmagn, self.kmesh[:,2])
        
        rshmat = GetRealSHMatrix(self.l)
        
        for im, m in enumerate(self.basis_ms):
            for imu, mu in enumerate(self.basis_ms):
                self.sph_harm_coefficients[:, im] += rshmat[im,imu] * (sph_harm(mu, self.l, azimuth, polar))
            

class RelGradientIntegrator(Integrator):
    
    def __init__(self, quantum_numbers, photoelectrons, radial_mesh, radial_function, dradial_function):

        Integrator.__init__(self, quantum_numbers, photoelectrons, radial_mesh, radial_function, dradial_function)
        self.dradial_function = dradial_function/PhysicalConstants.c_atomic
        self.get_radial_coefficients()
        self.get_spherical_harmonic_coefficients()
        self.get_plane_wave_components()
        
    def at(self, ik, imu, up_down_or_total = -1, verbose = False):
        g = self.nk_factor[ik] * self.G[ik] * self.sph_harmonics_bispinor[:2, ik, imu]
        f = self.nk_factor[ik] * self.F[ik] * Matrix(self.sigma_k[:,:,ik]).H * Matrix(self.sph_harmonics_bispinor[2:, ik, imu]).T #.H because this is a bra - plane-wave
        res = g+f[0,:]
        
        if up_down_or_total < 0:
            return np.sum(res) #up + dn
        else:
            return res[0,up_down_or_total] #0,0 = dn, 0,1 = up
                
    #internal
    def get_plane_wave_components(self):
        self.sigma_k = np.zeros((2,2,self.nk), dtype=np.complex) #\sigma \cdot \bm{k}_f
        self.nk_factor = np.zeros((self.nk), dtype=np.complex) #N_k
        
        c = PhysicalConstants.c_atomic
        k = self.kmesh
        kx = k[:,0]
        ky = k[:,1]
        kz = k[:,2]
        
        c2 = c ** 2
        k2 = kx*kx + ky*ky + kz*kz

        ek = 0.5*(-c2 + c * np.sqrt(c2 + 4*k2))
        factor = (c2 + ek)/np.sqrt(ek**2 + c2*k2)
        factor_f = c/(c2+ek)
        
        pauli_x = np.array([[0,   1], [1,  0]])
        pauli_y = np.array([[0, -1j], [1j, 0]])
        pauli_z = np.array([[1,   0], [0, -1]])

        self.nk_factor[:] = factor
        for ik in range(self.nk):
            self.sigma_k[:,:,ik] = factor_f[ik] * (pauli_x*kx[ik] + pauli_y*ky[ik] + pauli_z*kz[ik])

    def get_radial_coefficients(self):
        lg,m1,m2 = self.quantum_numbers.get_lmm(is_f = False)
        lf,m1,m2 = self.quantum_numbers.get_lmm(is_f = True)
    
        jkrre = np.zeros((2, self.nk, self.nr), dtype=float)
        for ir, r in enumerate(self.rmesh):
            kr = r*self.kmagn[:] 
            jkrre[0, :, ir] = spherical_jn( lg, kr[:]) * r**2
            jkrre[1, :, ir] = spherical_jn( lf, kr[:]) * r**2

        self.G = np.zeros((self.nk), dtype = np.complex)
        self.F = np.zeros((self.nk), dtype = np.complex)
        
        for ik in range(self.nk):
            self.G[ik] =  (2*lg+1) *        (1j)**lg * simps( jkrre[0,ik,:] * self.radial_function,  x = self.rmesh)
            self.F[ik] =  (2*lf+1) * (1j) * (1j)**lf * simps( jkrre[1,ik,:] * self.dradial_function, x = self.rmesh)

        if 0 and self.quantum_numbers.index == 0:
            import matplotlib.pyplot as plt
            import matplotlib
            matplotlib.use("TkAgg")
            plt.plot(self.rmesh,  jkrre[1,0,:] * self.dradial_function)
            plt.plot(self.rmesh,  jkrre[0,0,:] * self.radial_function)
            plt.show()
        
    def get_spherical_harmonic_coefficients(self):
    
        azimuth = np.arctan2(self.kmesh[:,1], self.kmesh[:,0])
        polar = np.arctan2(self.kmagn, self.kmesh[:,2])

        dim = 2*(2*self.quantum_numbers.l+1)
        self.sph_harmonics_bispinor = np.zeros((4, self.nk, dim), dtype = np.complex)
        tmp_quantum_numbers = OrbitalEvaluator.get_quantum_number(self.quantum_numbers.l, is_relativistic = True)

        for i in range(dim):

            j = tmp_quantum_numbers.j
            mu = tmp_quantum_numbers.mu
            k = tmp_quantum_numbers.kappa
                
            if k < 0:
                cg_up = np.sqrt(j+mu)/np.sqrt(2*j)
                cg_dn = np.sqrt(j-mu)/np.sqrt(2*j)
            elif k > 0:
                cg_up = -np.sqrt(j+1-mu)/np.sqrt(2*j+1)
                cg_dn =  np.sqrt(j+1+mu)/np.sqrt(2*j+1)
            else:
                cg_dn = 1
                cg_up = 1

            l,m1,m2 = tmp_quantum_numbers.get_lmm(is_f = False)

            if abs(m1) <= l:
                self.sph_harmonics_bispinor[0,:,i] = cg_dn * (sph_harm(m1, l, azimuth[:], polar[:])) #G_dn
            if abs(m2) <= l:
                self.sph_harmonics_bispinor[1,:,i] = cg_up * (sph_harm(m2, l, azimuth[:], polar[:])) #G_up


            j = tmp_quantum_numbers.f_numbers.j
            mu = tmp_quantum_numbers.f_numbers.mu

            l,m1,m2 = tmp_quantum_numbers.get_lmm(is_f = True)
            if l==0:
                cg_dn = 1
                cg_up = 1
            elif l < tmp_quantum_numbers.l:
                j = l - 0.5
                cg_up = -np.sqrt(j+1-mu)/np.sqrt(2*j+1)
                cg_dn =  np.sqrt(j+1+mu)/np.sqrt(2*j+1)
                
            else:
                j = l + 0.5
                cg_up = np.sqrt(j+mu)/np.sqrt(2*j)
                cg_dn = np.sqrt(j-mu)/np.sqrt(2*j)

            if abs(m1) <= l:
                self.sph_harmonics_bispinor[2,:,i] = cg_dn * (sph_harm(m1, l, azimuth[:], polar[:])) #F_dn
            if abs(m2) <= l:
                self.sph_harmonics_bispinor[3,:,i] = cg_up * (sph_harm(m2, l, azimuth[:], polar[:])) #F_up

            if i+1 < dim:
                tmp_quantum_numbers.increment()

        
class NonRelDipoleIntegrator(Integrator):
    
    def __init__(self, quantum_numbers, photoelectrons, radial_mesh, radial_function, pw_expansion):
        
        Integrator.__init__(self, quantum_numbers, photoelectrons, radial_mesh, radial_function)
        
        self.get_radial_coefficients(pw_expansion)
        self.get_spherical_harmonic_coefficients(pw_expansion)
        self.get_gaunt_coefficients(pw_expansion)
        
        
    def radial(self, ik, l):
        return self.radial_coefficients[ik,l]
        
    def spherical(self, ik, ipw, idir, im):
        return self.gaunt_coefficients[ipw,idir,im] * self.sph_harm_coefficients[ik,ipw]
        
    #internal
    def get_radial_coefficients(self,pw_expansion):
        jkrr3 = np.zeros((self.nk, pw_expansion.nl, self.nr ), dtype=float)
        for l in pw_expansion.ls:
            for ir, r in enumerate(self.rmesh):
                jkrr3[:, l, ir] = spherical_jn(l, r * self.kmagn) * r**3
                
        self.radial_coefficients = np.zeros((self.nk, pw_expansion.nl), dtype = np.complex)
                
        for ik in range(self.nk):
            for l in pw_expansion.ls:
                self.radial_coefficients[ik,l] = (-1j)**l * simps( jkrr3[ik,l,:] * self.radial_function, x = self.rmesh)


    def get_spherical_harmonic_coefficients(self, pw_expansion):
        self.sph_harm_coefficients = np.zeros((self.nk, pw_expansion.nlm), dtype = np.complex)
            
        azimuth = np.arctan2(self.kmesh[:,1], self.kmesh[:,0])
        polar = np.arctan2(self.kmagn, self.kmesh[:,2])
        
        for ipw, (l, m) in enumerate(pw_expansion.lm_list):
            
            self.sph_harm_coefficients[:, ipw] = (sph_harm(m, l, azimuth, polar))


    def get_gaunt_coefficients(self, pw_expansion):

        rshmat_basis = GetRealSHMatrix(self.l)
        rshmat_op = GetRealSHMatrix(1)

        self.gaunt_coefficients = np.zeros((pw_expansion.nlm, len(self.dir_ms), len(self.basis_ms) ), dtype = np.complex)
        for ipw, (l1, mu1) in enumerate(pw_expansion.lm_list):
            for idir, mu2 in enumerate(self.dir_ms):
                for imu3, mu3 in enumerate(self.basis_ms):   
                    for im2, m2 in enumerate(self.dir_ms):
                        for im3, m3 in enumerate(self.basis_ms):
                            self.gaunt_coefficients[ipw,idir,imu3] += rshmat_op[idir,im2] * rshmat_basis[imu3,im3] * gaunt(l1, 1, self.l, mu1, m2, m3)

#Just an enumeration of the free plane-wave expansion's l's and m's
class PW_expansion:
    def __init__(self,opts):
        
        self.nl = opts.pw_expansion_maxl+1
        self.ls = list(range(self.nl))
        
        self.lm_list = []
        for l in range(self.nl):
            for m in range(-l, l+1):
                self.lm_list.append( (l,m) )
        
        self.nlm = len(self.lm_list)
