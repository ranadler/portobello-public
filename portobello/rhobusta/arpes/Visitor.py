'''
Created on Nov 19, 2020 by C. Melnick
'''
import numpy as np
from mpi4py import MPI

from portobello.rhobusta.ProjectorEmbedder import Projector, KPathProjector
from portobello.rhobusta.plotters import ShardedArray
from portobello.rhobusta.PEScfEngine import GreensFunctionVisitor, PEScfEngine
        
class ComARPESBuilder(GreensFunctionVisitor):
    def __init__(self, svs : PEScfEngine, kpath, broadening):
    
        self.svs = svs
        self.kpath = kpath
        self.broadening = broadening
        
        self.isMaster = MPI.COMM_WORLD.Get_rank() == 0
        if self.isMaster: print(f" - creating a kpath projector, path length: {kpath.num_k}")
        self.proj =  KPathProjector(BZProjector=svs.orbBandProj, kpath=kpath)
        
        # in general for MPI, KPath band structure is sharded
        KS = self.proj.GetBandStructureInWindow() # this is cached in the projector
        self.num_k = KS.num_k
        self.shard_offset = KS.shard_offset
        self.nf = svs.num_corr_bands
        self.is_sharded = KS.is_sharded
        # the sharded image

        self.spectral = ShardedArray(self.svs.plugin.mpi,self.kpath.num_k,
                                  self.num_k,
                                  self.svs.sig.num_omega,
                                  self.nf, self.nf)  
        self.gradient_r = ShardedArray(self.svs.plugin.mpi,self.kpath.num_k,
                                  self.num_k,
                                  self.nf, self.nf, 3)
        self.gradient_i = ShardedArray(self.svs.plugin.mpi,self.kpath.num_k,
                                  self.num_k,
                                  self.nf, self.nf, 3)
  
    def passWeight(self) -> bool:
        return False

    def GetProjector(self) -> Projector:
        return self.proj

    def GetGlocProjector(self) -> Projector:
        return self.proj

    def GetLowBands(self):
        return self.svs.plugin.GetBandsForKPath(self.kpath, 0,  self.svs.orbBandProj.min_band - 1 - 1)
    def GetMidBands(self,proj):
        return self.svs.plugin.GetBandsAndGradientForKPath(self.kpath, self.svs.orbBandProj.min_band - 1, self.svs.orbBandProj.max_band - 1)
    def GetHighBands(self):
        return self.svs.plugin.GetBandsForKPath(self.kpath, self.svs.orbBandProj.max_band + 1 - 1, self.svs.dft.num_bands)

    def NonCorrelatedWidening(self) -> float:
        return self.broadening
    def CorrelatedWidening(self) -> float:
        return self.broadening
        
    def GetLabel(self):
        return self.svs.GetMethodName()

    def StartKPointsPerOmega(self,iomega, w):
        if iomega % 100 == 0:
            print("%d/%d %3.3f" % (iomega, self.svs.sig.num_omega, w))
        
    def VisitKOmegaPoint(self, k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
            negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
            weight, num_atoms):
        self.spectral.partial[k,iomega,:,:] = np.imag(Gk)
        self.gradient_r.partial[k,:,:,:] = np.real(gradients)
        self.gradient_i.partial[k,:,:,:] = np.imag(gradients)


    def End(self):
        #Note that these are not ShardedArrays anymore!
        self.spectral = -1/np.pi * self.spectral.partial
        self.gradient = self.gradient_r.partial + 1j*self.gradient_i.partial


