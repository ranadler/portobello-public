#!/usr/bin/env python3
'''
Created on Nov 9, 2020 by C. Melnick
'''
import sys
import matplotlib.pyplot as plt
import pickle as pl
import numpy as np
from argparse import ArgumentDefaultsHelpFormatter

from portobello.rhobusta.options import ArgumentParserThatStoresArgv

def arguments(add_help=False):
    
    parser = ArgumentParserThatStoresArgv('Display', add_help=add_help,
                            formatter_class=ArgumentDefaultsHelpFormatter)
                       
    group = parser.add_argument_group("Plotting options")
                    
    group.add_argument("--plane", dest="plane",
                    action="store_true",
                    default = False,
                    help="Load and plot ARPES.plane rather than ARPES.path")
        
    group.add_argument("--xlim", dest="xlim",
                    default = "",
                    help="Domain of plot. Default shows full domain")
                    
    group.add_argument("--ylim", dest="ylim",
                    default = "",
                    help="Range of plot. Default shows full range")
                    
    return parser
    
def main(opts = None):
    
    if opts == None:
        parser = arguments(add_help=True)
        opts = parser.parse_args(sys.argv[1:])
    
    if opts.plane:
        fname = "ARPES.plane.pickle"
    else:
        fname = "ARPES.path.pickle"
    
    fig_handle = pl.load(open(fname,'rb'))
    
    def get_lim(lim):
        return np.array(lim.split(":"), dtype = np.float)
    
    ax = fig_handle.axes[0]
    if opts.xlim != "":
        xlim = get_lim(opts.xlim)
        ax.set_xlim(xlim)
        
    if opts.ylim != "":
        ylim = get_lim(opts.ylim)
        ax.set_ylim(ylim)
        
    fig_handle.show()
    plt.show()
    
if __name__ == '__main__':
    main()
    
