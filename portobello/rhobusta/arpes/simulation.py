#!/usr/bin/env python3
'''
Created on Nov 9, 2020 by C. Melnick
'''
import sys
import numpy as np
from argparse import ArgumentDefaultsHelpFormatter
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.arpes.structures.simulation_geometry import ArpesGeometry
from portobello.rhobusta.arpes.ArpesRunner import ARPESRunnerWithMatrixElements

def deg2rad(deg):
    return deg*np.pi/180.

def arpesArguments(add_help=False):

    parser = ArgumentParserThatStoresArgv('ComARPES', add_help=add_help,
                            parents=[ ARPESRunnerWithMatrixElements.arguments(), ArpesGeometry.arguments()],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
    
    group = parser.add_argument_group("Simulation control")

    group.add_argument("--hw", dest="omega",
                default = 20, type=float,
                help="Energy of the light source's photons.")
    
    group.add_argument("-w", "--work-function", dest="wf",
                default=10, type=float,
                help="Work function of material. (photoelectron energy = [--hw] - [-w])")

    group.add_argument("-W", "--window", dest="window",
                default = "-0.5:0.1",
                help="If not --plane, defines the energy window of the ARPES.\n" +
                        "If --plane, defines energy level of ARPES. \n" +
                        "Given in eV")
                    
    group.add_argument("--spectral-only", dest="spectral_only",
                action="store_true",
                default = False,
                help="Do not use the light-element matrix elements (plot the trace of the spectral matrix only")

    group.add_argument("-T", "--temperature", dest="temperature",
                default = -1,
                type = float,
                help="Temperature of the experiment -- used to simulate loss of data near and above chemical potential")
    
    group.add_argument("--subtract-spins", dest="subtract_spins",
                default=False,action="store_true",
                help="Instead of combining ARPES instensity of both spins (in relativistic or spinful calculation), subtract them")
         
    group.add_argument("-l", "--lambda", dest="decay",
                    default = 5, type = float,
                    help="Mean free path of photoelectron in Angstroms")           
                    
    return parser

def convertCmdLineDegToRad(list):
    
    i=0
    for i, el in enumerate(list):
        args = el.split("=")
        for ia,arg in enumerate(args):
            sub_args = arg.split(",")
            for isa,sub_arg in enumerate(sub_args):
                if sub_arg[-3:] == "deg":
                    sub_args[isa] = str (deg2rad( float(sub_arg[:-3]) ))
            args[ia] = ",".join(sub_args)
        list[i] = "=".join(args)
        
def completeOptions(opts):
        
    if opts.plane:
        try:
            opts.window = float(opts.window)
            opts.window_minus = opts.window
            opts.window_plus = opts.window
        except:
            opts.window = opts.window.split(':')[0]
            opts.window_minus = float(opts.window)
            opts.window_plus = float(opts.window)
        
        opts.plotIrregular = True
        opts.kfactor = 1

    else:
        opts.window = opts.window.split(':')
        opts.window_minus = float(opts.window[0])
        opts.window_plus = float(opts.window[1])
    
def main():

    parser = arpesArguments(add_help=True)
    
    rawOptions = sys.argv[1:]
    convertCmdLineDegToRad(rawOptions)
    opts = parser.parse_args(rawOptions)
    completeOptions(opts)
    
    runner = ARPESRunnerWithMatrixElements(opts)
    runner.Run()
    
                       
if __name__ == '__main__':
    main()
    
