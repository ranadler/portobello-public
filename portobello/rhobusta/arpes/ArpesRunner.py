'''
Created on Nov 19, 2020 by C. Melnick
'''
import pickle
import numpy as np
from argparse import ArgumentDefaultsHelpFormatter
from portobello.bus.Matrix import Matrix
from portobello.bus.shardedArray import ShardedArray
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.arpes.Integrators import Organizer
from portobello.rhobusta.arpes.structures.simulation_geometry import ArpesGeometry
from portobello.rhobusta.arpes.structures.photons import Photons
from portobello.rhobusta.arpes.structures.electrons import Electrons
from portobello.rhobusta.arpes.structures.photoelectrons import Photoelectrons
from portobello.rhobusta.arpes.Visitor import ComARPESBuilder


from portobello.generated.LAPW import KPath
from portobello.rhobusta.bandstructure import ARPESRunner, ARPESRunnerOnPlane, BandstructureParser
from portobello.rhobusta.spectral_plotting import plot_spectral_f


class ARPESRunnerWithMatrixElements(ARPESRunnerOnPlane):
    
    @classmethod
    def arguments(cls,add_help=False):

        parser = ArgumentParserThatStoresArgv('ARPES Runner', add_help=add_help,
                                parents = [BandstructureParser(private = False)],
                                formatter_class=ArgumentDefaultsHelpFormatter)
          
        group = parser.add_argument_group("ARPES matrix-element integration arguments")
                    
        return parser
    
    def __init__(self, opts):
        
        opts.omega_window = np.max(np.abs(np.array(opts.window, dtype = float)))
        if opts.omega_window < 0.001: opts.omega_window = 0.1
        
        if opts.plane:
            self.super = ARPESRunnerOnPlane
        else:
            self.super = ARPESRunner

        self.super.__init__(self,opts)

    def GetSigma(self):
        return self.super.GetSigma(self)

    def Prepare(self):
        if self.IsMaster(): print (f"Constructing a description of the experiment")

        self.structure = self.dftplugin.GetStructure()
        self.arpes = ArpesGeometry(self.svs,self.opts)
        self.electrons = Electrons(self.kpath, self.opts)
        self.photons = Photons(self.arpes, self.electrons, self.opts)
        self.photoelectrons = Photoelectrons(self.arpes, self.electrons, self.photons)
        
        if self.IsMaster(): 
            print (f" - photon polarization is {self.photons.unrotated_polarization}")
            print (f" - surface normal is {self.arpes.normal}")

        return True

    def GetKPathAndLabels(self):
        return self.super.GetKPathAndLabels(self)

    def GetVisitor(self):
        return ComARPESBuilder(self.svs, self.kpath, self.opts.broadening)

    def Work(self, visitor):
        
        if self.IsMaster(): print (f"Computing Green's functions")

        self.svs.VisitGreensFunction(visitor)
        
        self.nf = visitor.nf
        self.g = visitor.spectral
        self.P = visitor.proj
        self.v = visitor.gradient
        self.Z = visitor.proj.GetBandStructureInWindow().Z

        self.shard_num_k = visitor.num_k
        self.shard_offset = visitor.shard_offset

        def my_shard(a):
            try:
                return a[self.shard_offset:self.shard_offset+self.shard_num_k]
            except:
                return None #empty shard

        self.photoelectrons.kpath = my_shard(self.photoelectrons.kpath)
        self.electrons.kpath = my_shard(self.photoelectrons.kpath)

        if self.IsMaster(): print (f"Integrating matrix elements")
        self.computeMatrixElements() #TODO: parallelize this
    
        if self.IsMaster(): print (f"Computing intensities")
        intensity = self.computeArpesIntensity()
        
        if self.IsMaster(): 
            print (f"Plotting result")
            if self.opts.subtract_spins and self.nrel == 2:
                intensity = intensity[:,0,:] - intensity[:,1,:]
            else:
                intensity = np.sum(intensity, axis=1)

            hskpp = None
            if self.pws is not None:
                hskpp = self.pws.hskp_placer
                intensity = self.pws.unfold(intensity)

            fig, ax = plot_spectral_f(self.labels, self.arpes.ebins, intensity, self.opts, hskpp = hskpp)

            if self.opts.spectral_only:
                pol_str = "non-pol"
            else:
                pol_str = ",".join([str(round(e.real,3)) for e in self.photons.unrotated_polarization])
                
            if self.opts.plane:
                path_str = f"plane{self.opts.path}"
            else:
                path_str = self.opts.path
                if path_str == "":
                    path_str = "full"

            pickle.dump(fig, open(f'ARPES-pol:{pol_str}-path:{path_str}.pickle','wb'))
                
    def initOmegaMesh(self):
        return self.svs.sig.omega[:]
        
    def computeMatrixElements(self):
        lapw = self.svs.plugin.GetLAPWBasis()
        dft = self.svs.plugin.GetDFTStuff()
        self.nrel = dft.nrel

        if self.opts.spectral_only: return
        if self.IsMaster():
            print("Computing the matrix elements")

        integrationOrganizer = Organizer(dft, lapw, self.electrons, self.photoelectrons, self.P, self.opts)

        reordered_polarization = [self.photons.polarization[1], self.photons.polarization[2], self.photons.polarization[0] ]

        #nabla_on_regularized_mesh = np.zeros((dft.nrel, self.kpath.num_k, self.nf), dtype=np.complex)
        overlaps =  np.zeros((self.shard_num_k, self.nrel, self.nf),dtype=complex)
        for iorb in range(lapw.num_muffin_orbs):
            iatom = lapw.atom_number[iorb] - 1
            
            integrationOrganizer.emplace(iorb)
            
            atom_position = self.structure.sites[iatom].coords

            for my_ik in range(self.shard_num_k):
                ik = my_ik + self.shard_offset
                phase_factor = np.exp(-1j * np.dot(integrationOrganizer.k_at(my_ik), atom_position))
                
                for irel in range(dft.nrel):
                    overlaps[my_ik, irel, :] += integrationOrganizer.conduct(my_ik, iorb, self.Z, phase_factor, reordered_polarization, up_down_or_total=irel)
                    
        #interstitial contribution -- not fully implemented
        #for irel in range(dft.nrel): 
        #    nabla_on_regularized_mesh[irel, :, :] += integrationOrganizer.conduct_interstitial(up_down_or_total=irel)

        self.nabla_on_regularized_mesh =  np.zeros((self.shard_num_k, self.nrel, self.nf),dtype=complex) 
        for my_ik in range(self.shard_num_k):
            ik = my_ik + self.shard_offset
            if self.opts.dipole:
                for irel in dft.nrel:
                    self.nabla_on_regularized_mesh[my_ik, irel, :] += overlaps[my_ik, irel, :]
            else:                    
                v = self.v[my_ik,:,:,:]
                v = Matrix(np.tensordot(v, self.photons.polarization[ik],axes=(2,0)))
                
                for irel in range(dft.nrel):
                    self.nabla_on_regularized_mesh[my_ik, irel, :] = overlaps[my_ik, irel, :] * v

    def computeArpesIntensity(self):
    
        arpesIntensity_i = ShardedArray(self.svs.plugin.mpi, self.kpath.num_k, self.shard_num_k, self.nrel, self.arpes.nbins)
        arpesIntensity_r = ShardedArray(self.svs.plugin.mpi, self.kpath.num_k, self.shard_num_k, self.nrel, self.arpes.nbins)
        
        for my_ik in range(self.shard_num_k):
            for irel in range(self.nrel):
                for b in range(self.arpes.nbins):
                
                    if self.opts.spectral_only:
                        i = np.trace(self.g[my_ik,b,:,:])
                    
                    else:
                        m = Matrix(self.nabla_on_regularized_mesh[my_ik,irel,:]).T
                        i =  m.H * Matrix(self.g[my_ik,b,:,:]) * m

                    arpesIntensity_r.partial[my_ik,irel,b] = i.real
                    arpesIntensity_i.partial[my_ik,irel,b] = i.imag

        arpesIntensity_r = arpesIntensity_r.GatherOnMaster()
        arpesIntensity_i = arpesIntensity_i.GatherOnMaster()

        if self.IsMaster():
            arpesIntensity = arpesIntensity_r + 1j*arpesIntensity_i

            if self.opts.temperature >= 0:
                        
                kbt = self.opts.temperature * 8.617E-5
                
                def fermi_dirac(energy):
                    if kbt > 0:
                        return 1/(np.exp(energy/kbt) + 1)
                    else:
                        return 1 if energy < 0 else 0
                        
                n = [fermi_dirac(e) for e in self.arpes.ebins]
                
                for b in range(self.arpes.nbins):
                    arpesIntensity[:,:,b] *= n[b]
            

            return np.abs(arpesIntensity)
        
        return None
