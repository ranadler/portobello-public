'''
Created on Nov 19, 2020 by C. Melnick
'''

import numpy as np
from scipy.special import sph_harm
from structures.basis import Basis

def basis_generator(atomic_numbers, generator_dictionary):
    '''
    atomic_numbers: list of atomic numbers for which to create a basis
    generator_dictionary: associate each atomic number with list of quantum numbers n and l.
        The n and l paired according to their ordering in these lists.
        All valid m can be compued from the constructed basis.
    '''

    basis = []
    for Z in atomic_numbers:
        ns = generator_dictionary[str(Z)]["n"]
        ls = generator_dictionary[str(Z)]["l"]
        for i in range(len(ns)):
            basis.append(sto(Z, ns[i], ls[i]))

    return basis

        
def constructSTO(crystal, opts):
    print("Constructing slater-type orbitals")
        
    span = 4*crystal.structure.lattice.matrix
    origin = -2*crystal.structure.lattice.matrix
    nrdiv = np.array([51, 51, 51])
    
    generators = basis_generator(opts.sto_basis_Z, opts.sto_basis_nl)
    
    b = Basis(None, nrdiv, origin, span, None, None)
       
    data = []
    for generator in generators:
        for m in range(-generator.l,generator.l+1):
            data.append(generator.compute_on_mesh(b.mesh, m))
        
    b.nf  = len(data)
    b.centers = np.zeros((b.nf,3))
    b.data = np.array(data, dtype = np.complex)
    return b

class sto:

    def __init__(self, atomic_number, n, l):
        
        self.n = n
        self.l = l
        self.zeta = slaters_rules(atomic_number, n, l)/n
        
        self.N = (2*self.zeta)**n * np.sqrt( 2*self.zeta / np.math.factorial(2*n) )
    
    def compute_on_mesh(self, r_mesh, m):
    
        r = np.linalg.norm(r_mesh, axis = 3)
        rho = np.linalg.norm(r_mesh[:,:,:,:2], axis = 3)
        
        azimuth = np.arctan2(r_mesh[:,:,:,1], r_mesh[:,:,:,0])
        polar = np.arctan2(rho, r_mesh[:,:,:,2])
        
        s = None
        if m < 0:
            s = 1./np.sqrt(2) * np.imag(sph_harm(m, self.l, azimuth, polar))
        elif m == 0:
            s = np.real(sph_harm(m, self.l, azimuth, polar))
        else:
            s = 1./np.sqrt(2) * np.real(sph_harm(m, self.l, azimuth, polar))
        
        return s*self.radial_function(r)
   

    def radial_function(self, r):
        return self.N * r**(self.n-1) * np.exp(-self.zeta*r)

def check_atomic_number(atomic_number):
    assert atomic_number < 119 and atomic_number > 0

def slaters_rules(atomic_number, N, L):
    check_atomic_number(atomic_number)
    
    Z = atomic_number
    
    num_electrons = 0
    for n in range(1, N+2):
        
        for l in range(0,n):
            for m in range(-l,l+1):
                for si in range(2):
                    s = slaters_rules_for_orb(N, L, n, l)
                    Z -= s
                    
                    if num_electrons == atomic_number:
                        return Z
                
                    num_electrons += 1
    
    return Z

def slaters_rules_for_orb(N, L, n, l):
    if (n > N):
        return 0
    
    if (N==n and (L == l or (L < 2 and l < 2)) ):
        if (n==1 and l==0):
            return 0.30
        else:
            return 0.35
    
    if (l < 2):
        if (n == N-1):
            return 0.85
        else:
            return 1.
            
    else:
        return 1.
