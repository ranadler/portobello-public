import os
import numpy as np
from portobello.bus.shardedArray import GatherAndBroadcastAcrossKShards
from portobello.generated.base_types import Window
from portobello.generated.susceptibility import SusceptibilityCalculation
from portobello.rhobusta.lattice_susceptibility.kqmap import K_Q_Mesh
from portobello.rhobusta.lattice_susceptibility.utilities import  SusceptibilityLocation
from portobello.rhobusta.loader import AnalysisLoader

from portobello.rhobusta.ProjectorEmbedder import CorrelatedSubspaceProjector, DefineProjector

class BaseRunner(AnalysisLoader):                

    def GatherProjector(self):
        basis = self.dftplugin.GetLAPWBasis()
        self.proj.P = np.swapaxes(self.proj.P,0,2)
        self.proj.P = GatherAndBroadcastAcrossKShards(self, basis, self.proj.P)
        self.proj.P = np.swapaxes(self.proj.P,0,2)

    def Report_Progress(self, i):
        current = int(10.*i/self.kqmesh.num_irr_k)
        if self.opts.debug and self.IsMaster() and not current == self.last:
            self.last = current
            print(f"Worker {self.GetRank()}: working on {i}/{self.kqmesh.num_irr_k} ... ")
            return True
        else:
            return False
            
    def GetKPairs(self):
        self.last = 0
        return self.kqmesh.ik_pairs_at(self.calculation.current_q)

    def SaveMyCalculationState(self):
        self.calculation.store(self.mySusceptibilityLocation)

    def LoadMyCalculationState(self):
        if not hasattr(self, "calculation"): 
            self.calculation = SusceptibilityCalculation()
        self.calculation.load(self.mySusceptibilityLocation)

    def RefreshCalculationState(self):
        if not hasattr(self, "calculation"): 
            self.calculation = SusceptibilityCalculation()
        self.LoadMyCalculationState()
        
        if not self.post_processing:
            self.calculation.num_q = self.kqmesh.num_q 
            
        self.GenerateTodoList()

        self.SaveMyCalculationState()

    def LoadCalculation(self):
        self.InitializeMPI()

        if not hasattr(self, "calculation"): #finishing initialization
            #if self.mpi is not None:
            self.mySusceptibilityLocation = SusceptibilityLocation.split(".h5")[0] + f".tmp.{self.GetRank()}.h5:/"
            #else:
            #   self.mySusceptibilityLocation = SusceptibilityLocation.split(":")[0]

            if self.opts.post_processing:
                self.post_processing = True
            else:
                self.post_processing = False
            
            self.method = AnalysisLoader.LoadCalculation(self, loadRealSigma=False) # if self.post_processing, we just load the light version

            if not self.post_processing:
                self.proj = self.svs.orbBandProj 
                self.kqmesh = K_Q_Mesh(self, self.dftplugin, self.svs, self.opts)

            self.RefreshCalculationState()

        else:
            self.LoadMyCalculationState()

        return self.method

    def getProjector(self):
        #This lets us project onto a smaller subspace -- memory requirements are steep! O(num_correlated_bands^4)
        self.opts.projector = SusceptibilityLocation + "/proj/"
        ewin = [float(f) for f in self.opts.window.split(":")]
        energyWindow = Window(low=ewin[0], high=ewin[1])
        if not os.path.exists(SusceptibilityLocation.split(":")[0]):
            if self.IsMaster():
                print("Computing new projector")
            DefineProjector(self.opts)

        proj = CorrelatedSubspaceProjector(self.opts.projector, energyWindow, which_shell=self.opts.which_shell)

        return proj


    def GenerateTodoList(self):
        size = self.calculation.num_q * self.calculation.num_nu
        chunk = (size + max(self.num_workers,1) - 1)/max(self.num_workers,1)
        start = int(chunk*self.GetRank())
        end = int(min(chunk*(self.GetRank() + 1), size))
        
        self.todo_list = []
        for qnu in range(start,end):
            q = int(qnu / self.calculation.num_nu)
            nu = qnu % self.calculation.num_nu
            if q >= self.calculation.current_q and nu >= self.calculation.current_nu:
                self.todo_list.append( [q, nu] )
                
        if len(self.todo_list):
            self.calculation.current_nu = self.todo_list[0][1]
            self.calculation.current_q = self.todo_list[0][0]
        else:
            self.calculation.current_nu = self.calculation.num_nu
            self.calculation.current_q = self.calculation.num_q