#!/usr/bin/python3

import pickle
from re import S, T
import sys
from argparse import ArgumentDefaultsHelpFormatter, Namespace

import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg
from itertools import product

import gc
import tracemalloc

from portobello.bus.mpi import GetMPIProxyOptionsParser
from portobello.rhobusta.ProjectorEmbedder import ProjectorOptionsParser
from portobello.rhobusta.lattice_susceptibility.analytical_continuation import AnalyticalContinuationRunner
from portobello.rhobusta.lattice_susceptibility.susceptibility_runner import SusceptibilityRunner
from portobello.rhobusta.loader import AnalysisLoader, PEScfLight
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.lattice_susceptibility.bubble import BubbleRunner
from portobello.rhobusta.lattice_susceptibility.greens_functions import GreensFunctionRunner
from portobello.rhobusta.lattice_susceptibility.vertex import VertexRunner
from portobello.rhobusta.lattice_susceptibility.plots import LinePlot, ImagePlot, ContinuedLinePlot, ContinuedImagePlot

#this causes quite a few problem -- e.g., can't rely on self.name; but it also solves quite a few -- e.g., only need to load svs / proj once
class SusceptibilityOrchastrator(GreensFunctionRunner, BubbleRunner, VertexRunner, SusceptibilityRunner, AnalyticalContinuationRunner): 

    def LoadMethod(self, method, *args):
        if self.post_processing:
            self.svs = PEScfLight(method, self.opts)
            self.svs.load(method+":/")
        else:
            AnalysisLoader.LoadMethod(self,method,False)


    def SubRunner(self, subrunner, next_todo, save = True):
            if self.IsMaster():
                print(f"Worker {self.GetRank()}: {subrunner.name} at nu {self.calculation.current_nu+1}/{self.calculation.num_nu} and q {self.calculation.current_q+1}/{self.calculation.num_q}", flush = True)

            subrunner.MPIWorker(self)
            if save: subrunner.Save(self)
            gc.collect()
            
            self.opts.mpi_workers = self.mpi_workers

            self.calculation.next_todo = next_todo
            self.SaveMyCalculationState()

    def MPIWorker(self):
        self.InitializeMPI()

        if self.opts.debug:
            tracemalloc.start()

        self.mpi_workers = self.opts.mpi_workers

        self.LoadCalculation()
        if self.calculation.next_todo == "":
            self.SubRunner(GreensFunctionRunner, BubbleRunner.name if self.opts.no_vertex else VertexRunner.name, save = False)
            print(" -- Green's functions calculated")

        self.Barrier()
        self.RefreshCalculationState()

        if not self.post_processing:
            self.GatherProjector()

        restart = False
        if self.opts.current_nu != -1:
            self.calculation.current_nu = self.opts.current_nu
            restart = True

        if self.opts.current_q != -1:
            self.calculation.current_q = self.opts.current_q
            restart = True

        if restart:
            print(f"restarting run from nu:{self.calculation.current_nu}, q:{self.calculation.current_q}")
            self.calculation.next_todo = BubbleRunner.name if self.opts.no_vertex else VertexRunner.name

        self.SaveMyCalculationState()
        self.Barrier()
        self.RefreshCalculationState()

        while True:
            self.LoadMyCalculationState()
            if self.calculation.current_nu == self.calculation.num_nu and self.calculation.current_q == self.calculation.num_q:
                break
            
            if self.calculation.next_todo == VertexRunner.name:
                self.SubRunner(VertexRunner,  BubbleRunner.name)

            if self.calculation.next_todo == BubbleRunner.name:
                self.SubRunner(BubbleRunner, SusceptibilityRunner.name)

            if self.calculation.next_todo == SusceptibilityRunner.name:
                self.SubRunner(SusceptibilityRunner, "update work list")

            if self.calculation.next_todo == "update work list":
                self.update_todo()

            if self.opts.debug:
                snap = tracemalloc.take_snapshot()
                top = snap.statistics('lineno')
                for s in top[:1]:
                    print(s)

        if 1 or self.calculation.next_todo == AnalyticalContinuationRunner.name:
            self.Barrier()
            self.SubRunner(AnalyticalContinuationRunner, "plot")

        self.Barrier()
        if self.calculation.next_todo == "plot":
            chi = AnalyticalContinuationRunner.LoadAll(self)
            if self.IsMaster():
                print("Producing plots", flush = True)
                if self.calculation.num_q == 1 or self.calculation.num_nu==1:
                    LinePlot(self.calculation, chi, self.opts)
                else:
                    ImagePlot(self.calculation, chi, self.kqmesh, self.opts)

        self.Terminate()
    
    def update_todo(self):
        last_q = self.calculation.current_q
        last_nu = self.calculation.current_nu

        self.todo_list.pop(0)
        if len(self.todo_list):
            self.calculation.current_nu = self.todo_list[0][1]
            self.calculation.current_q = self.todo_list[0][0]
        else:
            self.calculation.current_nu = self.calculation.num_nu
            self.calculation.current_q = self.calculation.num_q
            self.calculation.next_todo = AnalyticalContinuationRunner.name

        if last_nu == self.calculation.current_nu or self.opts.no_vertex:
            self.calculation.next_todo = BubbleRunner.name
        else:
            self.calculation.next_todo = VertexRunner.name
        
        self.SaveMyCalculationState()




def SusceptibilityArgumentParser(prepArgs, add_help=False, private=False):

    parser = ArgumentParserThatStoresArgv('lattice susceptibility', add_help=add_help,
                            parents=[GetMPIProxyOptionsParser(), ProjectorOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--which-shell", dest="which_shell", default=0, 
                        help="which shell (index) to project to, in order to build the correlated problem")

    parser.add_argument("--post-processing", dest="post_processing", default=False, action="store_true",
                        help="Only post processing is to be done (speeds up loading)")

    parser.add_argument("--no-vertex", dest="no_vertex", default=False, action="store_true",
                        help="Only use the unconnected bubble diagram when computing the lattice susceptibility")

    parser.add_argument("--boson-cutoff", dest="cutoff", default=10, type = float,
                        help="Compute the susceptibility up to this cutoff (in eV) or the highest frequency vertex which has been pre-computed, whichever is smaller.")

    parser.add_argument("--frequency-factor", dest="frequency_factor", default=1, type = float,
                        help="Expand the matsubara fermionic basis by this factor")

    parser.add_argument("--restart-at-bosonic-index", dest="current_nu", default = -1, type = int, 
                        help="Instead of continuing the run from wherever it is -- restart from the specified bosonic frequency index")
    
    parser.add_argument("--restart-at-q-index", dest="current_q", default = -1, type = int, 
                        help="Instead of continuing the run from wherever it is -- restart from the specified momentum (q) index")
    
    parser.add_argument("-q", "--q-direction", dest="q_dir", default="",
                        help="Direction of the q vector, e.g., 1,0,0. By default only q=0 is computed.")

    parser.add_argument("-k", "--k-regions", dest="k_regions", default="",
                        help="""Dictionary of labels and equations which define regions of k-space which should be grouped together.
                        For example: -k\Gamma:kx^2+ky^2<0.5,M:(kx-0.5)^2+(ky-0.5)^2<0.5,X:(kx-0.5)^2+ky^2<0.5""")

    parser.add_argument("--no-coarse-graining", dest="no_coarse_graining", default=False, action="store_true",
                        help="""Instead of averaging over k-points within a region, treat all k-points individually""")

    parser.add_argument("--coarse-grain-along-dir", dest="coarse_grain_along_dir", default="",
                        help="""group all k-points on the same line in the given direction""")

    parser.add_argument("--kernel-boson-cutoff", dest="kernel_boson_cutoff", default=10, 
                        help="Use the vertex kernels up to a boson energy of this cutoff in eV.")

    parser.add_argument("--kernel-fermion-cutoff", dest="kernel_fermion_cutoff", default=10, 
                        help="Use the vertex kernels up to a fermion energy of this cutoff in eV.")
    
    parser.add_argument("--debug", dest="debug", default=False, action="store_true",
                        help="Print additional information.")

    pg = parser.add_argument_group("Analytical Continuation")

    pg.add_argument("-e", "--energy-limit", dest="energy_limit", 
                        default=20.0, type=float,
                        help="energy limit for self-energy frequency (on positive side)")

    pg.add_argument("-O", "--nomega", dest="nomega", 
                        type=int,
                        default=400,
                        help="number of points in the frequency (omega) mesh")

    pg.add_argument("-d", "--tan-omega-mesh-min", dest="tan_omega_mesh_min", 
                        type=float,
                        default=0.005,
                        help="the min interval (eV) in a tan()-like mesh around 0 (if not set - it is linear)")

    pg.add_argument("--flat-model", dest="flat_model",
                      action="store_true",
                      default=False,
                      help="True if model is flat and not normal") 
    
    pg.add_argument("--gaussian-width", dest="gaussian_width", 
                        type=float,
                        default=10,
                        help="width of the gaussian model")
       
    pg.add_argument("-E", "--error", dest="assumed_error", 
                    type=float,
                    default=0.01,
                    help="Susceptibility variance due to error in CTQMC.")
    
    pg.add_argument("--alpha0", dest="alpha0", 
                        type=float,
                        default=1000.0,
                        help="initial alpha. The algorithm searches for the optimal value of alpha.")

    pg.add_argument("-i", "--iterations", dest="num_iters", 
                        type=int,
                        default=100,
                        help="number outer (alpha) loop iterations")
    
    pg.add_argument("-A", "--annealing-iterations", dest="num_annealing", 
                        type=int,
                        default=4000,
                        help="number of annealing iterations")

    pg.add_argument("--min-ratio", dest="min_ratio", 
                        type=float,
                        default=0.001,
                        help="stopping condition")
                            
    pg.add_argument("--smoothing-iterations", dest="num_smooth_iters", 
                    type=int,
                    default=0,
                    help="number of smoothing iterations") 
    
    pg.add_argument("--smoothing-width", dest="smoothing_width", 
                    type=float,
                    default=0.004,
                    help="width of lorenzians for smoothing")
    
    pg.add_argument("--final-smooth-A", dest="final_smooth", 
                    action="store_true",
                    default=False,
                    help="smooth A before saving it and running KK, run just once (unlike the smoothing iterations)")

    return parser

def SusceptibilityMain(args, kwargs, opts=None, ):

    if opts is None:
        parser = SusceptibilityArgumentParser(args, add_help=True, private=True)   
        opts = parser.parse_args(sys.argv[1:])

    runner = SusceptibilityOrchastrator(opts)
    runner.Run(wait=True)
    
if __name__ == '__main__':
    SusceptibilityMain(sys.argv, {}, opts = None)