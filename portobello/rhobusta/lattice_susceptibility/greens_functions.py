import numpy as np
from portobello.bus.Matrix import Matrix

from portobello.bus.persistence import Store
from portobello.generated.susceptibility import LatticeGreensFunctions
from portobello.rhobusta.lattice_susceptibility.base_runner import BaseRunner
from portobello.rhobusta.lattice_susceptibility.utilities import SusceptibilityLocation
from portobello.rhobusta.observables import PrintMatrix
from portobello.rhobusta.plotters import GreensFunctionBuilder

class GreensFunctionRunner(BaseRunner):
    name = "Gk"
    gimp_name = "GImp"

    def MPIWorker(self):    
        self.InitializeMPI()

        self.LoadCalculation()

        self.num_bands = self.proj.max_band - self.proj.min_band + 1

        self.svs.ResizeAll(int(self.svs.sig.num_omega*self.opts.frequency_factor))

        visitor = GreensFunctionBuilder(self.svs, self.opts, proj = self.proj)
        
        self.svs.VisitGreensFunction(visitor)
        self.result = visitor.result
        self.Save()

    def Save(self):
        if self.IsMaster():
            Gk = LatticeGreensFunctions()
            Gk.num_k = self.result.shape[0]
            Gk.num_si = self.result.shape[1]
            Gk.num_omega = self.result.shape[2]
            Gk.num_bands = self.result.shape[3]

            Gk.allocate()

            Gk.M = self.result

            Gk.omega = self.svs.sig.omega
            Gk.store(SusceptibilityLocation + GreensFunctionRunner.name)
            self.svs.GImp.store(SusceptibilityLocation + GreensFunctionRunner.gimp_name)
        
            #Base calculation descritpion
            self.calculation.current_q=0
            self.calculation.current_nu=0

            self.calculation.num_nu = max(1, int(self.opts.cutoff*self.svs.beta/(2*np.pi) + 0.5))
            self.calculation.num_omega = 2*(Gk.num_omega - self.calculation.num_nu - 1)
            self.calculation.num_bands = self.svs.dim

            self.calculation.store(SusceptibilityLocation, flush = True)

        #Grab all of the base calculation descritpion which only the master has available 
        self.Barrier()
        if not self.IsMaster():
            Store.Singleton().CloseFile(SusceptibilityLocation)
            self.calculation.load(SusceptibilityLocation)
            
        #And store a record in my own calculations tate
        self.SaveMyCalculationState()

        del self.result

    @staticmethod
    def Load(base_filename : str = SusceptibilityLocation):
        Store.Singleton().CloseFile(SusceptibilityLocation)
        Gk = LatticeGreensFunctions()
        Gk.load(base_filename + GreensFunctionRunner.name)
        return Gk