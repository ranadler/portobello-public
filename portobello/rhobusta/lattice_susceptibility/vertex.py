
import pickle
from matplotlib import pyplot
import numpy as np
from itertools import product
from portobello.bus.Matrix import Matrix
from portobello.bus.mpi import MPIContainer
from portobello.bus.persistence import Store

from portobello.generated.self_energies import LocalOmegaFunction
from portobello.generated.susceptibility import IrreducibleVertex
from portobello.rhobusta.ctqmc import CTQMC
from portobello.rhobusta.lattice_susceptibility.greens_functions import GreensFunctionRunner
from portobello.rhobusta.lattice_susceptibility.kernels import FullVertexKernels
from portobello.rhobusta.lattice_susceptibility.utilities import CTQMC_Interpreter, SusceptibilityLocation

class VertexRunner(MPIContainer):
     
    name = "vertex"
    symmetries = {"pp" : ["singlet", "triplet"], "ph" : ["spin", "charge"]}

    def MPIWorker(self):    
        self.InitializeMPI()

        self.LoadCalculation()
    
        if not hasattr(self,"kernels"):
            self.kernels = FullVertexKernels(self.svs, self.proj, self.calculation, self.opts)
        
        vertex_ph = self.kernels.ConstructFullVertex(self.calculation.current_nu, self.calculation.num_omega, channel='ph')
        vertex_pp = self.kernels.ConstructFullVertex(self.calculation.current_nu, self.calculation.num_omega, channel='pp')

        green = LocalOmegaFunction()
        green.load(SusceptibilityLocation + GreensFunctionRunner.gimp_name)
        self.irr_vertex = self.ComputeIrreducibleVertices(vertex_ph, vertex_pp, green)
        del green

    def Save(self):

        for channel in self.symmetries.keys():
            for symm in self.symmetries[channel]:
                self.irr_vertex[channel][symm].store(self.mySusceptibilityLocation+VertexRunner.name+"/"+channel+"/"+symm+"/")

        del self.irr_vertex

    def InitializeIrreducibleVertex(self):
        vertex = IrreducibleVertex()
        vertex.num_kind = CTQMC_Interpreter.num_orbital_kind
        vertex.num_bands = self.svs.dim
        vertex.num_omega = self.calculation.num_omega
        vertex.allocate()
        return vertex

    @staticmethod
    def Load(base_filename):
        Store.Singleton().CloseFile(base_filename)
        gamma={}
        for channel in VertexRunner.symmetries.keys():
            gamma_in_channel={}
            for symm in VertexRunner.symmetries[channel]:
                try:
                    gamma_in_channel[symm] = IrreducibleVertex()
                    gamma_in_channel[symm].load(base_filename+VertexRunner.name+"/"+channel+"/" + symm + "/")
                except:
                    print("Did not find vertex in channel/symm ", channel, symm)
                    
            if len(gamma_in_channel.keys()):
                gamma[channel] = gamma_in_channel

        if not len(gamma.keys()):
            print("No saved vertex found!")
            quit()

        return gamma

    def ComputeIrreducibleVertices(self, vertex_ph, vertex_pp, green):

        self.num_kind = CTQMC_Interpreter.num_orbital_kind #kind allows us to store the sparse tensor more compactly
        nf = int(self.calculation.num_omega/2)
        
        irr_vertex={}
        for channel in self.symmetries.keys():
            irr_vertex[channel] = {}
            for symm in self.symmetries[channel]:
                irr_vertex[channel][symm] = self.InitializeIrreducibleVertex()

        nu=self.calculation.current_nu

        def compute_bubble(g1, g2, channel):
            mid = int(g1.shape[0]/2)
            if channel=="ph":
                return Matrix(np.diag( -self.svs.beta*g1[mid-nf:mid+nf] * g2[mid-nf+nu:mid+nf+nu] )) #g(omega)g(omega+nu)
            else:
                return Matrix(np.diag( -self.svs.beta*g1[mid-nf:mid+nf] * g2[mid+nf+nu-1:mid-nf+nu-1:-1] ))#g(omega)g(-omega-nu)

        for i,k in product(range(self.svs.dim), repeat=2):

            def add_negative_axis(g):
                g_ = np.flip(g)
                g_ = np.conj(g_)
                return np.concatenate([g_, g])

            def get_bubbles(channel, i, k):
                
                i,j,k,l = CTQMC_Interpreter.ijkl(channel,orb_kind,i,k)

                g1 = add_negative_axis(green.M[:,i,i,0])
                g2 = add_negative_axis(green.M[:,j,j,0])
                g3 = add_negative_axis(green.M[:,k,k,0])
                g4 = add_negative_axis(green.M[:,l,l,0])

                bubble1 = compute_bubble(g1,g2,channel)
                bubble2 = compute_bubble(g3,g4,channel)

                return bubble1, bubble2

            #Particle hole channels
            channel = "ph"
            for orb_kind in range(self.num_kind):
                if orb_kind == CTQMC_Interpreter.ijij:
                    continue # no ph vertex in ijij
                
                bubble1, bubble2 = get_bubbles(channel, i, k)

                F = 0.5*Matrix(vertex_ph[:,:, orb_kind, CTQMC_Interpreter.iiii, i,k] + vertex_ph[:,:, orb_kind, CTQMC_Interpreter.iikk, i,k])
                chi = bubble1 * F * bubble2
                irr_vertex["ph"]["charge"].M[:,:, orb_kind, i, k] = chi

                F = Matrix(vertex_ph[:,:, orb_kind, CTQMC_Interpreter.iiii, i,k] - vertex_ph[:,:, orb_kind, CTQMC_Interpreter.iikk, i,k])
                chi = bubble1 * F * bubble2
                irr_vertex["ph"]["spin"].M[:,:, orb_kind, i, k] = chi


            #TODO: Something is wrong with the pp bubble / vertex

            #Particle particle channels
            channel = "pp"
            for orb_kind in range(self.num_kind):
                if orb_kind == CTQMC_Interpreter.iiii:
                    continue # no pp vertex in iiii

                bubble1, bubble2 = get_bubbles(channel, i, k)

                F = Matrix(vertex_pp[:,:, orb_kind, CTQMC_Interpreter.iikk, i,k] - vertex_pp[:,:, orb_kind, CTQMC_Interpreter.ikki, i,k])
                chi = bubble1 * F * bubble2
                irr_vertex["pp"]["singlet"].M[:,:, orb_kind, i, k] = chi

                F = 0.5*Matrix(vertex_pp[:,:, orb_kind, CTQMC_Interpreter.iikk, i,k] + vertex_pp[:,:, orb_kind, CTQMC_Interpreter.ikki, i,k])
                chi = bubble1 * F * bubble2
                irr_vertex["pp"]["triplet"].M[:,:, orb_kind, i, k] = chi


        def get_inv_bubble(channel, kind, i, k):
            
            i,j,k,l = CTQMC_Interpreter.ijkl(channel,orb_kind,i,k)

            g1 = add_negative_axis(green.M[:,i,j,0])
            g2 = add_negative_axis(green.M[:,k,l,0])

            bubble = compute_bubble(g1,g2,channel)

            try:
                inv_bubble = bubble.I
            except:
                inv_bubble = bubble*0

            return inv_bubble

        for i,k in product(range(self.svs.dim), repeat=2):
            for channel in self.symmetries.keys():
                for orb_kind in range(self.num_kind):

                    inv_bubble = get_inv_bubble(channel, orb_kind, i, k)

                    for symm in self.symmetries[channel]:
                        chi = Matrix(irr_vertex[channel][symm].M[:,:, orb_kind, i, k])
                        try:
                            irr_vertex[channel][symm].M[:,:, orb_kind, i, k] = inv_bubble - chi.I
                        except:
                            irr_vertex[channel][symm].M[:,:, orb_kind, i, k] = 0 #some combinations of kind and i,k are empty, e.g., iiii and i != k or iikk and i==k

        if self.opts.debug and 0:
            eps = 1e-8
            for i,k in product(range(self.svs.dim), repeat=2):
                for orb_kind in range(self.num_kind):
                    for channel in self.symmetries.keys():

                        inv_bubble = get_inv_bubble(channel, orb_kind, i, k)

                        for symm in self.symmetries[channel]:
                            
                            G = Matrix(irr_vertex[channel][symm].M[:,:, orb_kind, i, k])

                            if np.max(np.abs(G)) > eps:
                                chi = (inv_bubble - G).I

                                mid2 = int(chi.shape[0]/2)
                                nf2 = min(40,mid2)

                                fig,ax = pyplot.subplots(2,2)
                                ax[0,0].imshow(np.real(chi.T[mid2-nf2:mid2+nf2, mid2-nf2:mid2+nf2]))
                                ax[1,0].imshow(np.imag(chi.T[mid2-nf2:mid2+nf2, mid2-nf2:mid2+nf2]))

                                ax[0,1].imshow(np.real(G[mid2-nf2:mid2+nf2, mid2-nf2:mid2+nf2]))
                                ax[1,1].imshow(np.imag(G[mid2-nf2:mid2+nf2, mid2-nf2:mid2+nf2]))

                                with open(f'chi.loc.{channel}.{symm}.{orb_kind}.{i}.{k}.pickle','wb') as f:
                                    pickle.dump(fig,f)
        
        #quit()

        return irr_vertex