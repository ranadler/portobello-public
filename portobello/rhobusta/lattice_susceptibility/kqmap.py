
import pickle
import numpy as np
from numpy.linalg.linalg import norm
import matplotlib.pyplot as plt

from argparse import Namespace
from portobello.bus.Matrix import Matrix
from portobello.bus.mpi import MPIContainer
from portobello.bus.shardedArray import GatherAndBroadcastAcrossKShards
from portobello.generated.symmetry import Bootstrap
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.PEScfEngine import PEScfEngine


class LineSegment:
    eps = 1E-9
        
    def __init__(self, aLabel, bLabel, a, b):
        self.aLabel = aLabel
        self.bLabel = bLabel
        self.a = a
        self.b = b
            
        self.dif = b - a
        self.length2 = np.dot(self.dif, self.dif)
        self.length = np.sqrt(self.length2)
                
    def isCollinear(self, c):
        return np.linalg.norm(np.cross(c - self.a, self.dif)) < self.eps
                
    def isBetween(self, c):
        dp = np.dot(c - self.a, self.dif)
        return dp > 0 and dp < self.length2
            
    def isWithinSegment(self, c):
        return self.isCollinear(c) and self.isBetween(c)
                    
    def operateOn(self,op):
        self.a = op.operate(self.a)
        self.b = op.operate(self.b)
            
    def projectOnto(self,vec):
        return np.dot(vec, self.dif) / np.sqrt(self.length) * self.dif

class K_Q_Mesh:
    eps = 1e-9

    def __init__(self, mpic : MPIContainer, dftplugin : DFTPlugin, svs : PEScfEngine, opts : Namespace):
        
        label = opts.q_dir
        self.dftplugin = dftplugin
        basis = self.dftplugin.GetLAPWBasis()
        self.weights = GatherAndBroadcastAcrossKShards(mpic, basis, svs.lapw.weight)

        self.kpoints = self.generate_full_mesh(basis)
        if label != "":
            self.num_irr_k = len(self.kpoints)
        else:
            self.num_irr_k = basis.num_k_irr

        if label != "":
            q_dir = np.array(opts.q_dir.split(","), dtype=float)
            self.q_dir = LineSegment("origin", label, [0,0,0], q_dir)
            self.qmap = self.generate_qmap()
        else:
            self.qmap = self.generate_trivial_qmap()

        self.k_to_neg_k_map = self.generate_k_to_neg_k_map()

        self.ik_to_region_map, self.num_k = self.map_regions(opts)
        
        self.keys = np.sort( list(self.qmap.keys()) )
        self.num_q = len(self.keys)
        assert(self.num_q > 0)

    def region_index(self, ik):
        return self.ik_to_region_map[ik].index

    def region_label(self, ik):
        return self.ik_to_region_map[ik].label

    def key_at(self, iq):
        return self.keys[iq]

    def ik_pairs_at(self, iq):
        return self.qmap[self.key_at(iq)]
    
    def neg_k_at(self,ik):
        return self.k_to_neg_k_map[ik]

    def weight_at(self, ik, ikpq):
        return self.weights[ik]

    def q_at(self,iq):
        return self.key_at(iq) * self.scale

    def qs(self):
        return np.array(self.keys)*self.scale


    class K_region:
        def __init__(self, index, label, eqn):
            self.index = index
            self.label = label
            self.eqn = eqn

    def trivial_map(self):
        k_region_map = {}
        for ik, irr_k_orbit in enumerate(self.kpoints):
            k_region_map[ik] = self.K_region(ik,"Individual","")

        return k_region_map, len(self.kpoints)

    def map_along_dir(self, dir):

        x=0
        y=1
        z=2
        idir = eval(dir)
        dirs = []
        for i in range(3):
            if i != idir:
                dirs.append(i)

        k_region_map = {}
        regions = {}
        for ik, irr_k_orbit in enumerate(self.kpoints):
            k = np.around(irr_k_orbit[0], decimals=6)
            key = f"{k[dirs[0]], k[dirs[1]]}"
            if key not in regions.keys():
                ir = len(regions.keys())
                regions[key] = self.K_region(ir, key, dir)

            k_region_map[ik] = regions[key]

        return k_region_map, len(regions.keys())

    def map_regions(self, opts):

        if opts.no_coarse_graining:
            return self.trivial_map()

        elif opts.coarse_grain_along_dir != "":
            return self.map_along_dir(opts.coarse_grain_along_dir)

        k_region_leftovers = [self.K_region(0, "All Remaining", "True")]

        st = self.dftplugin.GetStructure()
        recip = st.lattice.reciprocal_lattice_crystallographic
        bx = norm(recip.matrix[0,:])
        by = norm(recip.matrix[1,:])
        bz = norm(recip.matrix[2,:])

        def evaluate(regions, verbose = False):
            k_region_map = {}
            for ik, irr_k_orbit in enumerate(self.kpoints):
                def check_orbit():
                    for k in irr_k_orbit:

                        kx = k[0]/bx
                        ky = k[1]/by
                        kz = k[2]/bz

                        for region in regions:
                            if (eval(region.eqn)):
                                k_region_map[ik] = region

                                if region.index > 0:  #note that we keep the first region if the user provides overlapping regions
                                    return
                check_orbit()

            return k_region_map

        if opts.k_regions != "":
            k_regions = [s.split(":") for s in opts.k_regions.split(",")]
            k_regions = k_region_leftovers + [self.K_region(ir+1, region[0], region[1].replace('^','** ')) for ir, region in enumerate(k_regions)]
        else:
            k_regions = k_region_leftovers

        k_region_map = evaluate(k_regions, verbose = True)

        to_delete = []
        for ir, region in enumerate(k_regions):
            if region not in k_region_map.values():
                print(f"Warning: No k-points found in region {region.label}. Removing it...")
                to_delete.append(ir)
 
        to_delete.reverse()
        for i in to_delete:
            k_regions.pop(i)

        #re-index after deleting
        if len(to_delete):
            for ir, region in enumerate(k_regions):
                region.index = ir

            k_region_map = evaluate(k_regions)
        
        colors = ['red','blue','green','k','orange','purple']
        if len(k_regions) <= len(colors):
            fig = plt.figure()
            ax = fig.add_subplot(projection='3d')

            kx = [[] for ir in k_regions]
            ky = [[] for ir in k_regions]
            kz = [[] for ir in k_regions]

            for ik, irr_k_orbit in enumerate(self.kpoints):
                region = k_region_map[ik]
                for k in irr_k_orbit:
                    kx[region.index].append(k[0])
                    ky[region.index].append(k[1])
                    kz[region.index].append(k[2])

            for region in k_regions:
                ax.scatter(kx[region.index],ky[region.index],kz[region.index], color = colors[region.index], label = region.label, depthshade=False)
            ax.legend()

            with open(f'k_regions.3d.pickle','wb') as f:
                pickle.dump(fig,f)

            kx = [[] for ir in k_regions]
            ky = [[] for ir in k_regions]
            for ik, irr_k_orbit in enumerate(self.kpoints):
                region = k_region_map[ik]
                for k in irr_k_orbit:
                    if norm(k[2]) < self.eps:
                        kx[region.index].append(k[0])
                        ky[region.index].append(k[1])

            fig = plt.figure()
            ax = fig.add_subplot()
            for region in k_regions:
                ax.scatter(kx[region.index],ky[region.index], color = colors[region.index], label = region.label)
            ax.legend()

            with open(f'k_regions.2d.pickle','wb') as f:
                pickle.dump(fig,f)

        return k_region_map, len(k_regions)

    def generate_k_to_neg_k_map(self):

        negk_map = {}
        for ik1, irr_k1_orbit in enumerate(self.kpoints):

            def add_neg_k():
                for ik2, irr_k2_orbit in enumerate(self.kpoints):
                    for k1 in irr_k1_orbit:
                        for k2 in irr_k2_orbit:
                            dif = k2+k1
                            if norm(dif) < self.eps:
                                negk_map[ik1] = ik2
                                return
            
            add_neg_k()

        return negk_map

    def generate_trivial_qmap(self):

        self.scale = 1
        self.Gamma_key = 0
        qmap = {self.Gamma_key:[]}
        for ik in range(self.num_irr_k):
            qmap[self.Gamma_key].append([ik,ik])

        return qmap

    def generate_qmap(self):

        qmap_trivial = self.generate_trivial_qmap()

        qs = []
        for ik1, irr_k1_orbit in enumerate(self.kpoints):
            for ik2, irr_k2_orbit in enumerate(self.kpoints):
                if ik1 != ik2:
                    for k1 in irr_k1_orbit:
                        for k2 in irr_k2_orbit:
                            dif = k2-k1
                            sign = -1 if norm(k2) < norm(k1) else 1
                            if self.q_dir.isCollinear(dif):
                                qs.append([norm(dif), sign, ik1, ik2])

        self.scale = np.min(np.array(qs),axis=0)[0]

        qmap = {}
        qmap[self.Gamma_key] = qmap_trivial[self.Gamma_key]
        
        for q in qs:
            key = int(q[0]/self.scale+0.5) #q[1]*int(q[0]/self.scale+0.5) ignore the sign for now??
            pair = q[2:]
            if key in qmap:
                if not pair in qmap[key]:
                    qmap[key].append(pair)
            else:
                qmap[key] = [pair]

        return qmap

    def generate_full_mesh(self, basis):

        bs = Bootstrap()
        bs.load("./ini.h5:/sgg/")

        bs.num_ops = bs.rots.shape[2] #not saved in the ini

        kpoints = []
        for k in range(basis.num_k_irr):
            kp = np.array(basis.KPoints[k,:])
            orbit = [kp]
                        
            for g in range(bs.num_ops):
                rot = Matrix(bs.rots[:,:,g])
                kp2 = rot * Matrix(kp).T
                found = False
                for k3 in orbit:
                    if norm(k3 - kp2) < self.eps:
                        found = True
                        break
                if not found:
                    arrk = np.zeros(3,dtype=float)
                    arrk[:] = kp2.flatten()
                    orbit.append(arrk) #otherwise this is a matrix and it behaves differently than k, an array

            kpoints.append(orbit)
        
        return kpoints