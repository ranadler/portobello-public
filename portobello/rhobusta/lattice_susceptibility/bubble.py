import numpy as np
from portobello.bus.Matrix import Matrix
from portobello.bus.persistence import Store
from portobello.generated.susceptibility import LatticeBubble_at_qnu

from portobello.rhobusta.lattice_susceptibility.base_runner import BaseRunner
from portobello.rhobusta.lattice_susceptibility.greens_functions import GreensFunctionRunner
from portobello.rhobusta.lattice_susceptibility.utilities import generate_frequencies


class BubbleRunner(BaseRunner):
    name = "bubble"
    channels = ["ph","pp"]

    def MPIWorker(self):
        self.InitializeMPI()

        self.LoadCalculation()
        Gk = GreensFunctionRunner.Load()

        self.Chi0_ph = LatticeBubble_at_qnu()
        self.Chi0_pp = LatticeBubble_at_qnu()

        self.Chi0_ph.num_si = Gk.num_si
        self.Chi0_ph.num_bands = self.calculation.num_bands
        self.Chi0_ph.num_omega = self.calculation.num_omega # || factor of two for negative axis; Gamma.num_omega is another choice
        self.Chi0_ph.num_k = self.kqmesh.num_k 

        self.Chi0_pp.num_si = self.Chi0_ph.num_si
        self.Chi0_pp.num_bands = self.Chi0_ph.num_bands
        self.Chi0_pp.num_omega = self.Chi0_ph.num_omega
        self.Chi0_pp.num_k = self.Chi0_ph.num_k

        self.Chi0_ph.allocate()
        self.Chi0_pp.allocate()

        self.Chi0_ph.nu = generate_frequencies(self.Chi0_ph.num_nu, self.svs.beta, is_boson = True)
        self.Chi0_pp.nu = generate_frequencies(self.Chi0_ph.num_nu, self.svs.beta, is_boson = True)
        self.Chi0_ph.omega = generate_frequencies(self.Chi0_ph.num_nu, self.svs.beta, is_symmetric = True)
        self.Chi0_pp.omega = generate_frequencies(self.Chi0_ph.num_nu, self.svs.beta, is_symmetric = True)

        mid = int(self.Chi0_ph.num_omega/2) 
        mid_g = Gk.num_omega


        assert(mid_g >= mid + self.Chi0_ph.num_nu - 1)
        nu = self.calculation.current_nu

        g_slice = slice(mid_g-mid, mid_g+mid)
        g_slice_plus_nu = slice(mid_g-mid+nu, mid_g+mid+nu) #for the particle hole bubble
        g_slice_minus_nu = slice(mid_g+mid+nu-1, mid_g-mid+nu-1, -1) #for the particle-particle bubble TODO: check this

        for ik, ikpq in self.GetKPairs():

            self.Report_Progress(ik)

            n_ikpq = self.kqmesh.neg_k_at(ikpq) #-k-q

            ir = self.kqmesh.region_index(ik)

            for si in range(self.svs.num_si):

                Pk = self.proj.OneAtomProjector(ik,si)
                Pkpq = self.proj.OneAtomProjector(ikpq,si)
                Pn_kpq = self.proj.OneAtomProjector(n_ikpq,si)

                #Add the -i\omega data
                def G_at(k):
                    G = Gk.M[ik,si,:,:,:]
                    Gdagg = np.conj(np.transpose(G, axes=(0,2,1)))
                    return np.concatenate( (np.flip(Gdagg, axis=0),  G) )

                Gil = G_at(ik)[g_slice]
                Gjk_ph = G_at(ikpq)[g_slice_plus_nu]
                Gjk_pp = G_at(n_ikpq)[g_slice_minus_nu]
                                
                if not hasattr(self,f"einsum_path"):
                    self.einsum_path_str = "ai,bj,il,jk,kc,ld->abcd"
                    self.einsum_path = np.einsum_path(self.einsum_path_str, Pk.H, Pkpq.H, Gil[0], Gjk_ph[0], Pkpq, Pk, optimize = "optimal")

                for om in range(self.Chi0_ph.num_omega):
                    #chi_ph_band_basis = np.einsum("il,jk->ijkl", Pk.H, Pkpq.H, Gil[om], Gjk_ph[om], Pkpq, Pk) * self.kqmesh.weight_at(ik,ikpq)

                    self.Chi0_ph.M[ir, om, :, :, :, :] -= np.einsum(self.einsum_path_str, Pk.H, Pkpq.H, Gil[om], Gjk_ph[om], Pkpq, Pk, optimize = self.einsum_path[0]) * self.kqmesh.weight_at(ik,ikpq)
                    self.Chi0_pp.M[ir, om, :, :, :, :] -= np.einsum(self.einsum_path_str, Pk.H, Pn_kpq.H, Gil[om], Gjk_pp[om], Pn_kpq, Pk, optimize = self.einsum_path[0]) * self.kqmesh.weight_at(ik,ikpq)

        self.Chi0_ph.M *= self.svs.beta
        self.Chi0_pp.M *= self.svs.beta

    def Save(self):

        self.Chi0_ph.store(self.mySusceptibilityLocation+BubbleRunner.name+"/ph/")
        self.Chi0_pp.store(self.mySusceptibilityLocation+BubbleRunner.name+"/pp/")

        self.SaveMyCalculationState()

        del self.Chi0_ph, self.Chi0_pp

    
    @staticmethod
    def Load(base_filename):
        Store.Singleton().CloseFile(base_filename)
        Chi0 = {}
        for channel in BubbleRunner.channels:
            Chi0[channel] = LatticeBubble_at_qnu()
            Chi0[channel].load(base_filename + BubbleRunner.name+"/"+channel+"/")
        return Chi0
