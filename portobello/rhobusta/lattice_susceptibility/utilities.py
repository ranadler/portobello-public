import numpy as np
from itertools import product
from portobello.bus.Matrix import Matrix
from portobello.rhobusta.plotters import ShardedArray

SusceptibilityLocation = "./susceptibility_calculation.h5:/"

def generate_frequencies(number, beta, is_boson = False, is_symmetric = False):
    offset = 0 if is_boson else 1
    r = range(-number+is_boson,number) if is_symmetric else range(number)
    return np.array([2*np.pi*(i+offset)/beta for i in r])

def CollapseTensorIntoMatrix(T):
    n = T.shape[0]
    return T.reshape(n*n,n*n)



class CTQMC_Interpreter:

    num_orbital_kind = 4
    num_spin_kind = 3

    #these are the definitions for the various kinds
    #PH:
    # i_         _l
    #    \_____/
    #    |     |
    #    |     |
    #    |_____|
    # j_/       \_k

    #PP
    # i_         _l
    #    \_____/
    #    |     |
    #    |     |
    #    |_____|
    # k___\____/  
    #      \______j

    iiii = 0 
    iikk = 1 
    ikki = 2 
    ijij = 3 

    @classmethod
    def ijkl(cls, channel, kind, i,j):

        if channel == "ph":
            if kind == cls.ikki:
                return i,j,j,i
            elif kind == cls.ijij:
                return i,j,i,j
            else: #iiii, iikk
                return i,i,j,j

        elif channel == "pp":
            if kind == cls.ikki:
                return i,j,j,i
            elif kind == cls.iikk:
                return i,j,i,j
            else: #ijij -- iiii has no vertex in pp
                return i,i,j,j

        else:
            print (channel)
            raise NotImplementedError()

    class entry_info:
        def __init__(self, i, j, k, l, orbital_kind, spin_kind):
            self.i = int(i)
            self.j = int(j)
            self.k = int(k)
            self.l = int(l)
            self.orbital_kind = orbital_kind
            self.spin_kind = spin_kind

            self.first_idx = i
            self.second_idx = k if orbital_kind < 3 else j

    @classmethod
    def parse_entry(cls, entry, dim):
        #CTQMC multiplies spin-orbit index by two to keep track of creation / annihilation operators
        indices = [int(int(i)/2) for i in entry.split("_")] 

        #CTQMC has block of up followed by block of down
        i = indices[0]
        j = indices[1]
        k = indices[2]
        l = indices[3]

        i_orb = i % dim
        j_orb = j % dim
        k_orb = k % dim
        l_orb = l % dim
        
        i_spin = int(i >= dim)
        j_spin = int(j >= dim)
        k_spin = int(k >= dim)
        l_spin = int(l >= dim)


        if (i_orb == j_orb and j_orb==k_orb and k_orb == l_orb):
            orbital_kind=cls.iiii

        elif (i_orb == j_orb and k_orb == l_orb): 
            orbital_kind=cls.iikk 

        elif(i_orb == l_orb and k_orb == j_orb):
            orbital_kind=cls.ikki

        elif(i_orb == k_orb and j_orb == l_orb):
            orbital_kind=cls.ijij

        else:
            #There are other vertices, but they don't matter if we have a chi_ijkl ~ delta_ij delta_kl ... do we?
            orbital_kind = cls.num_orbital_kind
        
        # as above but for spins
        if (i_spin == j_spin and j_spin == k_spin and k_spin == l_spin): 
            spin_kind=cls.iiii

        elif(i_spin == j_spin and k_spin == l_spin):
            spin_kind=cls.iikk

        elif(i_spin == l_spin and k_spin == j_spin):
            spin_kind=cls.ikki 

        else:
            print(i_spin,k_spin,j_spin,l_spin, "Haven't implemented bubble for this spin combination!")
            spin_kind = 4
        
        return cls.entry_info(i_orb, j_orb, k_orb, l_orb, orbital_kind, spin_kind)

    @classmethod
    def parse_function(cls, json_function):
        try:
            return np.array(json_function["real"][:]) + 1j*np.array(json_function["imag"][:])
        except:
            return np.array(json_function["function"]["real"][:]) + 1j*np.array(json_function["function"]["imag"][:])

