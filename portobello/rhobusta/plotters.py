'''
Created on Sep 10, 2020

@author: adler
'''
from matplotlib import cm
import scipy
from portobello.bus.mpi import MPIContainer
import sys
import numpy as np
from itertools import product
from scipy.linalg import block_diag
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.generated.base_types import Window
from portobello.bus.Matrix import ZeroComplexMatrix, Matrix
from portobello.bus.shardedArray import ShardedArray
from portobello.rhobusta.observables import AddObservables
from portobello.rhobusta.ProjectorEmbedder import Projector, CorrelatedSubspaceProjector, KPathProjector, ProjectorOptionsParser
from portobello.generated.fermi_surface import LowEnergyHamiltonian
from portobello.generated.LAPW import BandStructure, KPath
import matplotlib
from portobello.rhobusta.orbitals import OrbitalsDefinition

from portobello.symmetry import local
matplotlib.use('PS')
import matplotlib.pyplot as plt
import pickle
from scipy import integrate
from argparse import Namespace
from portobello.rhobusta.PEScfEngine import GreensFunctionVisitor, PEScfEngine
from portobello.rhobusta.observables import GetAllObservables, ObservableDecomposition
import matplotlib.gridspec as gridspec
from portobello.symmetry.characters import PrintMatrix
from portobello.rhobusta.spectral_plotting import Character, plot_spectral_f, plot_bands
from distinctipy import distinctipy

from portobello.generated.ARPES import PlaneARPES, PathARPES, HSKP


class Projected_collector:

    """
    Projected_collector constructs and holds a collection of visitors -- either band or ARPES style visitors
    The purpose is to manage projection onto an observable (or collection of observables), 
        each of which corresponds to a set of values (those on the diagonal of the operator in the correlated subspace)
    Visitors should not handle projection themselves, but call Projected_collector.VisitK[Omega]Point(...), which projects at that point
        across all observables / values
    """

    correlated_shell_name = "Cor. Imp."

    class ObservableDescription:
            def __init__(self, iobs, idir, iatom, ival, value, scale):
                self.iobs = iobs
                self.idir = idir
                self.iatom = iatom
                self.ival = ival
                self.value = value
                self.scale = scale

    class ShellDescription:
        def __init__(self, projector : CorrelatedSubspaceProjector, observables, dim : int):
            self.proj = projector
            self.observables = observables
            self.dim = dim

    # As opposed to is_band -- see contstruct_visitors
    def is_ARPES(self): 
        return False

    def make_label(self,shell_name,key):
        return f"{shell_name}:{key}"

    def decipher_label(self,label):
        return label.split(":")

    def __init__(self, svs : PEScfEngine, labels, kpath : KPath, omegas, num_equivalents, opts : Namespace, pws = None):
        
        self.shells={}
        for impurity in svs.impurity_delegator.GetImpurityProblems(svs):

            if opts.model:
                proj = svs.corrSubshell
                obs = []
                for rep in opts.obs.split(","):
                    rep = int(rep)
                    obs.append(np.zeros((proj.dim,proj.dim), dtype=float))
                    for i in range(proj.dim):
                        if rep==svs.rep[i,i]:
                            obs[-1][i,i] += 1

                observables = []
                for i, M in enumerate(obs):
                    uM = Matrix(M)
                    diag = np.sum(uM)*np.ones(proj.dim)
                    observables.append(ObservableDecomposition([uM], [diag], f"rep{i+1}"))
                    
            else:
                obs = GetAllObservables(impurity.corrSubshell)
                try:
                    AddObservables(obs, impurity, opts)
                except:
                    pass #DFTLoader
                observables = obs.evaluateObsList(opts.obs)

            self.num_obs = len(observables)

            suffix = impurity.GetSuffixOfImpurity()
            name = f"{self.correlated_shell_name} {suffix}" if suffix != "" else self.correlated_shell_name
            self.shells[name] =  self.ShellDescription(impurity.orbBandProj, observables, impurity.dim)

        if hasattr(opts, "shells") and opts.shells != "":

            #bands may have shifted into or out of the original window and we need to do some hunting to find them.
            energyWin = impurity.orbBandProj.energyWin
            max_band = impurity.orbBandProj.max_band
            min_band = impurity.orbBandProj.min_band
            
            def adjust_window(current_en, band_dif):
                if band_dif == 0:
                    return current_en
                elif band_dif*current_en > 0:
                    return current_en*0.95
                else:
                    return current_en*1.05

            shells = opts.shells.split(",")
            proj_parser = ProjectorOptionsParser(private=True)
            for shell in shells:

                #TODO:allow users to choose which symmetry inequivalent shell in the specified type?
                #TODO:or sum across all of them?
                which_shell=0

                #guard against some naming foibles
                assert(shell != "def") 
                shell_name = shell.replace(":","-")

                #define projector on the fly
                proj_loc = f"./projector.tmp.h5:/{shell_name}/" #not actually used -- just some protection against overwriting the real thing
                proj_opts, argv_ = proj_parser.parse_known_args([f"-s{shell}", f"--projector={proj_loc}", "-Bnative"])
                orbsDef = OrbitalsDefinition(proj_opts)
                    
                #Window hunting described above
                energyWinTry = energyWin
                while True:
                    shell_proj = CorrelatedSubspaceProjector(orbsDef, energyWinTry, which_shell)
                    if shell_proj.max_band == max_band and shell_proj.min_band == min_band:
                        break

                    energyWinTry.low = adjust_window(energyWinTry.low, shell_proj.min_band-min_band)
                    energyWinTry.high = adjust_window(energyWinTry.high, shell_proj.max_band-max_band)
                energyWin = energyWinTry

                if kpath is not None:
                    shell_proj = KPathProjector(shell_proj, kpath, which_shell, orbsDef = orbsDef)
                        

                shell_obs = GetAllObservables(shell_proj.shell)
                observables = shell_obs.evaluateObsList(opts.obs)

                self.shells[shell_name] = self.ShellDescription(shell_proj, observables, shell_proj.shell.dim)
                
        self.svs = svs
        self.labels = labels
        self.kpath = kpath
        self.omegas = omegas
        self.opts = opts
        self.broadening = 1j*opts.broadening

        self.I = Matrix(np.eye(self.svs.num_corr_bands, dtype=np.complex))
        
        self.values_per_key = {}
        self.unique_map = {}
        for shell_name in self.shells.keys():
            self.unique_map[shell_name] = {}
            self.values_per_key[shell_name] = {}

            for iobs, obs in enumerate(self.shells[shell_name].observables):
                for idir, direction in enumerate(obs.directions):

                    if opts.sum_across_equivalents:
                        equivalents_todo = [0]
                        scale = num_equivalents
                    else:
                        equivalents_todo = range(num_equivalents)
                        scale = 1

                    for i_equivalents in equivalents_todo:

                        if opts.sum_across_equivalents:
                            key = f"{obs.expr}{direction}"
                        else:
                            key = f"{i_equivalents}.{obs.expr}{direction}"
                        
                        if key not in self.values_per_key[shell_name].keys():
                            self.values_per_key[shell_name][key] = 0

                        for dv in obs.eigenvalues[idir]:
                            
                            val = np.round(dv,5)
                            if abs(val) < 1e-13:
                                val = 0 # dealing with things which round to -0.0

                            key_and_value = f"{key}={val}" #e.g., keys will be Sz=0.5 or Sz=-0.5

                            if key_and_value not in self.unique_map[shell_name].keys():
                                self.unique_map[shell_name][key_and_value] = self.ObservableDescription(
                                    iobs,idir,i_equivalents,self.values_per_key[shell_name][key],round(dv,5), scale)
                                self.values_per_key[shell_name][key] += 1
        
        if kpath is not None:
            self.nk = kpath.num_k
            if hasattr(opts,"plane") and opts.plane:
                self.hskp = HSKP(npoints = 1)
                self.hskp.allocate()
                self.hskp.labels[0].text = labels[0]

            else:
                self.hskp = HSKP(npoints = len(labels))
                self.hskp.allocate()
                for i, l in enumerate(labels):
                    self.hskp.labels[i].text = l

        else: #non-kpath users will try to access these things

            self.nk = next(iter(self.shells.values())).proj.num_k #just grab any of the shells projectors
            self.kpath = KPath()
            self.kpath.num_k = self.nk
            self.hskp = None

        self.construct_visitors(pws)

    def construct_visitors(self, pws):
        
        self.total_visitors = 0

        self.visitors = {}
        for ishell, shell_name in enumerate(self.shells.keys()):
            self.visitors[shell_name] = []

            proj = self.shells[shell_name].proj
            basis = Matrix(proj.shell.basis)
            umap = self.unique_map[shell_name]
            print(f"- {shell_name} shell: unique observable values are:", list(umap.keys()))

            dim = self.shells[shell_name].dim

            num_colors = min(self.opts.max_colors, len(umap.keys()))
            
            self.colors = distinctipy.get_colors(num_colors)
            linestyles = self.opts.linestyles.split(",")
            #linestyles = list(lines.lineStyles.keys())
            markers =self.opts.markers.split(":")
            #markers = list(Line2D.markers.keys())

            for ikey,key in enumerate(umap.keys()):
                iobs = umap[key].iobs
                idir = umap[key].idir
                value = umap[key].value
                iatom = umap[key].iatom

                uM = self.shells[shell_name].observables[iobs].eigenspace[idir]
                diag = self.shells[shell_name].observables[iobs].eigenvalues[idir]
                basisProjector=1.0*(abs(diag[:]-value)<=1.0e-5)
                rot = basis.H * proj.shell.rotations[iatom][:,:] * basis
                if self.svs.nrel == 1 and not self.opts.model:
                    rot = Matrix(block_diag(rot,rot))

                uM2 = rot * uM
                resolver = uM2 * Matrix(np.diag(basisProjector[:])) * uM2.H
                if self.svs.num_si == 1 and not self.opts.model:
                    if self.svs.nrel < 2:
                        resolver = [Matrix(resolver[:dim, :dim])]
                    else:
                        resolver = [resolver]
                else:
                    resolver = [Matrix(resolver[:dim, :dim]), Matrix(resolver[dim:, dim:])]

                if self.is_ARPES():
                    self.visitors[shell_name].append(ResolvedARPESBuilder(self.svs, self.kpath, resolver = resolver,
                                                    name = self.make_label(shell_name, key), 
                                                    labels=self.labels, opts=self.opts, proj=proj, pws = pws,
                                                    collector = self,
                                                    value=value, 
                                                    color=self.colors[ikey % num_colors], 
                                                    linestyle=linestyles[ishell % len(linestyles)], 
                                                    marker=markers[(ishell//len(linestyles)) % len(markers)]))
                else:
                    self.visitors[shell_name].append(ResolvedBandBuilder(self.svs, self.kpath, resolver = resolver,
                                                    name = self.make_label(shell_name, key), 
                                                    labels=self.labels, opts=self.opts, proj=proj, 
                                                    collector = self))

                self.total_visitors += 1

            self.visitors[shell_name][-1].obs_value = value

    def End(self, plot = True):
        for shell_name in self.shells.keys():
            for v in self.visitors[shell_name]:
                v.End(plot)        


class Projected_band_collector(Projected_collector):

    def VisitKPoint(self, k, si, Hqp,
            lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
            weight, num_atoms):

        for shell_name in self.shells.keys():
            proj = self.shells[shell_name].proj
            dim = self.shells[shell_name].dim

            if self.svs.GetMethodName() == "gutzwiller" and self.correlated_shell_name in shell_name:
                R = self.svs.R

                ERminI = proj.EmbeddedIntoKBandWindow(R - self.svs.ID, k, si)
                embR =  self.I + Matrix(ERminI[:,:])
            else:
                embR = self.I
                
            Pk = proj.OneAtomProjector(k, si) #* Matrix(R)
            rho = np.zeros((self.svs.num_corr_bands, dim, dim), dtype=np.complex)
            for ib, e_qp in enumerate(midEpsK):
                ginv = Matrix(self.I * (e_qp + self.broadening) - Hqp)
                rho[ib,:,:] = -1/np.pi * np.imag( Pk.H * embR.H * ginv.I * embR * Pk )

            for v in self.visitors[shell_name]: v.VisitKPoint(k, si, rho, lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
                weight, num_atoms)

    def __init__(self, svs : PEScfEngine, num_equivalents, opts : Namespace, labels = None, kpath : KPath = None):
        Projected_collector.__init__(self, svs, labels, kpath, None, num_equivalents, opts,)

        colors =  distinctipy.get_colors(self.total_visitors)
        self.band_characters = {}
        c=0
        for shell_name in self.shells.keys():
            self.band_characters[shell_name] = {}
            for key in self.unique_map[shell_name].keys():
                self.band_characters[shell_name][key] = Character(label = self.make_label(shell_name, key), 
                                                                    value = self.unique_map[shell_name][key].value,
                                                                    color = colors[c])
                c+=1
                
        self.is_band = True

    def add(self, shell_name, key_and_value : str, band_character):
        self.band_characters[shell_name][key_and_value].add(band_character) 

    def get_characters(self):
        return self.band_characters

class Projected_ARPES_collector(Projected_collector):

    def VisitKOmegaPoint(self, k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
                        negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
                        weight, num_atoms):

        rhok = 0.5*1j/np.pi * (Gk - Gk.H)
        for shell_name in self.shells.keys():
            proj = self.shells[shell_name].proj
            Pk2 = proj.OneAtomProjector(k, si)
            rho = Pk2.H *rhok * Pk2 
            # call the partial plot
            for v in self.visitors[shell_name]: v.VisitKOmegaPoint(k, iomega, w, si, rho, gradients, lowEnergyGkDos, negativeOmegaDos, 
                                                        lowEnergyGkDos, positiveOmegaDos, weight, num_atoms)

    def is_ARPES(self):
        return True

    def __init__(self, svs : PEScfEngine, omegas, num_equivalents, opts : Namespace, labels = None, kpath : KPath = None, pws = None):
        Projected_collector.__init__(self, svs, labels, kpath, omegas, num_equivalents, opts, pws = pws)

        self.ARPES = []
        for shell_name in self.shells.keys():
            for obs in self.shells[shell_name].observables:
                ndir = len(obs.directions)
                
                #just need one of the keys assocaited with this observable
                # so that we can get num_values
                direction = obs.directions[0] 
                if opts.sum_across_equivalents:
                    key = f"{obs.expr}{direction}"
                else:
                    key = f"0.{obs.expr}{direction}"
                num_values = self.values_per_key[shell_name][key]
                title=self.make_label(shell_name, obs.expr)

                if hasattr(opts, "plane") and opts.plane:
                    self.ARPES.append(PlaneARPES(title = title,  nx = self.nk, ny = 1, 
                                                num_directions = ndir, num_equivalents = num_equivalents, num_values = num_values,
                                                width = opts.width, xlabel = labels[1], ylabel = labels[2])
                                                )
                else:
                    self.ARPES.append(PathARPES(title = title, nx = self.nk, ny = len(omegas),
                                                num_directions = ndir, num_equivalents = num_equivalents, num_values = num_values)
                                                )

                self.ARPES[-1].allocate()
                self.ARPES[-1].hskp = self.hskp
            
    def add(self, shell_name, key_and_value : str, pARPES : np.array):

        iobs = self.unique_map[shell_name][key_and_value].iobs
        idir = self.unique_map[shell_name][key_and_value].idir
        iatom = self.unique_map[shell_name][key_and_value].iatom
        ival = self.unique_map[shell_name][key_and_value].ival
        val = self.unique_map[shell_name][key_and_value].value
        scale =self.unique_map[shell_name][key_and_value].scale
       
        self.ARPES[iobs].Z[iatom, idir, ival, :, :] = pARPES[:,:] * scale
        self.ARPES[iobs].values[ival] = val
        
    def store(self):
        for ARPES in self.ARPES:
            ARPES.store(f"./pARPES.{ARPES.title}.h5:/")


class BaseBuilderOnIBZ(GreensFunctionVisitor):
    def __init__(self,svs,opts,label : str, GlocProj : str = None):
        self.svs = svs
        self.opts = opts
        self.label = label
        self.proj = svs.orbBandProj
        
        if GlocProj is not None:
            self.glocProj = CorrelatedSubspaceProjector(GlocProj, svs.proj_energy_window) # no subspace in plotting
        else:
            self.glocProj = self.proj

        self.isMaster = self.svs.plugin.IsMaster()
        self.broadening = self.opts.broadening

        if self.opts.num_high_energy_bands < 0:
            self.num_high_bands = self.svs.dft.num_bands
        else:
            self.num_high_bands = min(self.GetGlocProjector().max_band + self.opts.num_high_energy_bands, self.svs.dft.num_bands)
       
        if self.opts.num_low_energy_bands < 0:
            self.num_low_bands = 0
        else:
            self.num_low_bands = max(self.GetGlocProjector().min_band - 2 -  self.opts.num_low_energy_bands, 0)       
        
    def GetLowBands(self):
        return self.svs.plugin.GetBandsForAllKpoints(self.num_low_bands,  self.GetGlocProjector().min_band -1 -1)
    def GetHighBands(self):
        return self.svs.plugin.GetBandsForAllKpoints(self.GetGlocProjector().max_band + 1 - 1, self.num_high_bands)

    def GetGlocProjector(self) -> Projector:
        return self.glocProj

    def GetLabel(self):
        self.label

    def GetProjector(self) -> Projector:
        return self.proj

    def NonCorrelatedWidening(self) -> float:
        return self.broadening

    def CorrelatedWidening(self) -> float:
        return self.broadening
    
    def StartKPointsPerOmega(self,iomega, w):
        if self.isMaster and iomega % 100 == 0:
            print("%d/%d %3.3f" % (iomega, self.svs.sig.num_omega, w))

class DOSPlotBuilder(BaseBuilderOnIBZ):
    def __init__(self, svs, opts, label : str, GlocProj : str = None):
        BaseBuilderOnIBZ.__init__(self,svs,opts,label,GlocProj)
  
        # these are the plots
        self.dft =      np.zeros((self.svs.sig.num_omega,))
        self.full_dos = np.zeros((self.svs.sig.num_omega,))
        self.corr_dos = np.zeros((self.svs.sig.num_omega,))

        if opts.obs != "":
            self.collector = Projected_ARPES_collector(svs, svs.sig.omega, self.proj.shell.num_equivalents, opts)

    def VisitKOmegaPoint(self, k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
                        negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
                        weight, num_atoms):
        ww = weight / num_atoms
        Pk2 = self.glocProj.OneAtomProjector(k, si)
        wGloc = -1/np.pi * Pk2.H * Gk * Pk2 * ww * self.svs.num_equivalents
        
        # add up all contribution from the same kind of orbital
        self.corr_dos[iomega] += np.trace(wGloc).imag 

        self.full_dos[iomega] +=  (negativeOmegaDos + positiveOmegaDos + lowEnergyGkDos)*ww 
        self.dft[iomega] +=  (negativeOmegaDos + lowOmegaDos + positiveOmegaDos)*ww

        if self.opts.obs != "":
            self.collector.VisitKOmegaPoint(k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
                        negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
                        weight, num_atoms)
    
    def End(self):
        fig_handle = plt.figure()

        MPI = self.svs.plugin.mpi.GetMPIInterface()
        if self.proj.GetBandStructureInWindow().is_sharded:
            MPI.COMM_WORLD.Barrier()
            self.dft= MPI.COMM_WORLD.allreduce(self.dft, op=MPI.SUM)
            MPI.COMM_WORLD.Barrier()
            self.full_dos= MPI.COMM_WORLD.allreduce(self.full_dos, op=MPI.SUM)
            MPI.COMM_WORLD.Barrier()
            self.corr_dos= MPI.COMM_WORLD.allreduce(self.corr_dos, op=MPI.SUM)

            for orb in self.orbs:
                MPI.COMM_WORLD.Barrier()
                self.corb[orb]= MPI.COMM_WORLD.allreduce(self.corb[orb], op=MPI.SUM)

            if self.opts.obs != "":
                for shell_name in self.collector.shells.keys():
                    for v in self.collector.visitors[shell_name]: 
                        v.dos = MPI.COMM_WORLD.allreduce(v.dos, op=MPI.SUM)

        if self.isMaster:
            noncorr = self.full_dos[:] - self.corr_dos[:]
            

            plt.plot(self.svs.sig.omega[:], self.corr_dos, color = "gray", label="corr.")
            plt.plot(self.svs.sig.omega[:], self.full_dos, color='black', label=self.label)
            plt.plot(self.svs.sig.omega[:], self.full_dos-self.corr_dos, "+", color='black', label="non-corr.")
            #plt.plot(self.svs.sig.omega[:], noncorr, label="non-corr.")
            #plt.plot(self.svs.sig.omega[:], self.dft, "-", label="DFT")
            
            if self.opts.obs != "":
                for shell_name in self.collector.shells.keys():
                    for v in self.collector.visitors[shell_name]:
                        plt.plot(self.svs.sig.omega[:], v.dos, marker=v.marker, linestyle=v.linestyle, color = v.color, label = v.name)

            plt.legend()      

            pickle.dump(fig_handle,open(f'DOS-{self.svs.super_iter}.{self.svs.sub_iter}.pickle','wb'))
            
            def ferm(x):
                if x < -50.0:
                    return 1.0
                if x > 50:
                    return 0.0
                return 1/(1+np.exp(x))
            
            for i,w in enumerate(self.svs.sig.omega[:]):
                self.corr_dos[i] *= ferm(w * self.svs.beta)
            N = integrate.trapz(y=self.corr_dos, x=self.svs.sig.omega)
            if self.svs.num_si == 1:
                N *= 2
            print(f"Integrated N={N}")                
                

class FSBandBuilder(BaseBuilderOnIBZ):
    def __init__(self, svs, label : str, opts:Namespace, GlocProj : str = None):
        BaseBuilderOnIBZ.__init__(self,svs,opts,label,GlocProj)
        
        KS : BandStructure = self.proj.GetBandStructureInWindow() # this is cached in the projector
        self.is_sharded = KS.is_sharded
        
        self.num_si = self.svs.dft.num_si
        assert(self.svs.dft.num_k == KS.num_k)

        mpic = self.svs.plugin.mpi
        self.epsMaxIntensity = ShardedArray(mpic, self.svs.dft.num_k_irr, 
                                            self.proj.num_k, self.svs.num_corr_bands, self.num_si)


        if self.opts.lifetime:
            self.lifetime = ShardedArray(mpic, self.svs.dft.num_k_irr,
                                          self.proj.num_k, self.svs.num_corr_bands, self.num_si)
                
        if self.opts.qp_weight:
            self.qp_weight = ShardedArray(mpic, self.svs.dft.num_k_irr,
                                          self.proj.num_k, self.svs.num_corr_bands, self.num_si)

        if opts.num_correlated_bands < 0:
            self.correlated_bands_end = self.svs.num_corr_bands
            self.correlated_bands_start = 0
        else:
            midpoint = int(self.svs.num_corr_bands/2)
            self.correlated_bands_end = min(midpoint + int(opts.num_correlated_bands/2), self.svs.num_corr_bands)
            self.correlated_bands_start = max(midpoint - int(opts.num_correlated_bands/2), 0)

        if opts.obs != "":
            self.collector = Projected_band_collector(svs, self.proj.shell.num_equivalents, opts)
            
    
    def VisitKPoint(self, k, si, Hqp,
            lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
            weight, num_atoms):

        self.epsMaxIntensity.partial[k, :, si] = midEpsK[:]

        if self.opts.lifetime:
            self.lifetime.partial[k,:,si] = lifetimes[:]

        if self.opts.qp_weight:
            self.qp_weight.partial[k,:,si] = np.abs(qp_weights[:])

        if self.opts.obs != "":
            self.collector.VisitKPoint(k, si, Hqp,
                lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
                weight, num_atoms)


    def End(self):
        result = self.epsMaxIntensity.GatherOnMaster()
        intensity = np.zeros_like(result)
        lifetime = np.zeros_like(result)
        qp_weight = np.zeros_like(result)
        if self.opts.lifetime:
            lifetime = self.lifetime.GatherOnMaster()
            
        if self.opts.qp_weight:
            qp_weight = self.qp_weight.GatherOnMaster()

        if self.opts.obs != "":
            self.collector.End()

        if self.isMaster:

            if self.opts.obs != "":
                characters = self.collector.get_characters()
                for shell_name in self.collector.shells.keys():
                    for c in characters[shell_name].values():
                        intensity += c.weight * c.value
                    
            self.LEH = LowEnergyHamiltonian(num_k_irr=self.svs.dft.num_k_irr, 
                            num_si=self.num_si, 
                            name=self.label,
                            min_band = self.correlated_bands_start+1, 
                            max_band = self.correlated_bands_end)
            self.LEH.allocate()
            self.LEH.intensity = np.zeros_like(self.LEH.scalar)
            self.LEH.lifetime = np.zeros_like(self.LEH.scalar)
            self.LEH.qp_weight = np.zeros_like(self.LEH.scalar)
            for si in range(self.num_si):
                for k in range(self.svs.dft.num_k_irr):
                    self.LEH.eps[:,k, si] = result[k,self.correlated_bands_start:self.correlated_bands_end,si]
                    for i in range( self.LEH.eps.shape[0] ):
                        if not self.opts.lifetime and not self.opts.qp_weight and self.opts.obs == "":
                            self.LEH.scalar[i ,k, si] = i/ self.LEH.eps.shape[0]
                        else:
                            self.LEH.scalar[i, k, si] = intensity[k,i,si] + lifetime[k,i,si] + qp_weight[k,i,si]
                        
                        self.LEH.intensity[i, k, si] = intensity[k,i,si]
                        self.LEH.lifetime[i, k, si] = lifetime[k,i,si]
                        self.LEH.qp_weight[i, k, si] = qp_weight[k,i,si]

class FSBuilder(FSBandBuilder):
    def __init__(self, svs, label : str, opts:Namespace, GlocProj : str = None):
        FSBandBuilder.__init__(self,svs,label, opts, GlocProj)
        
        self.maxIntensity = np.zeros((self.proj.num_k, self.svs.num_corr_bands, self.num_si))

        mpic = self.svs.plugin.mpi
        if self.opts.lifetime and self.opts.lifetime_distribution:
            self.weights = ShardedArray(mpic,self.svs.dft.num_k_irr, self.proj.num_k)
        
        if opts.obs != "":
            self.collector = Projected_ARPES_collector(svs, self.proj.shell.num_equivalents, opts)


    def VisitKOmegaPoint(self, k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
                        negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
                        weight, num_atoms):
        if self.opts.obs != "":
            self.collector.VisitKOmegaPoint(k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
                        negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
                        weight, num_atoms)
        rhok = 0.5*1j/np.pi * (Gk - Gk.H)

        for ib in range(self.svs.num_corr_bands):
            gki =rhok[ib,ib]
            if gki > self.maxIntensity[k, ib, si]:
                self.maxIntensity[k, ib, si] = gki
                self.epsMaxIntensity.partial[k, ib, si] = w
                if self.opts.lifetime: self.intensity.partial[k, ib, si] = 1.0/np.imag(1/Gk[ib,ib])
        
        if self.opts.lifetime and self.opts.lifetime_distribution:
            self.weights.partial[k] = weight

    def End(self):
        del self.maxIntensity
        result = self.epsMaxIntensity.GatherOnMaster()
        if self.opts.lifetime: intensity = self.intensity.GatherOnMaster()
        if self.opts.lifetime and self.opts.lifetime_distribution: weights = self.weights.GatherOnMaster()
        if self.opts.obs != "": self.collector.End(plot=False)

        if self.isMaster:
            
            #bands can become un-sorted which confuses fermisurfer
            for si in range(self.num_si):
                for k in range(self.svs.dft.num_k_irr):
                    result[k,:,si] = np.sort(result[k,:,si])

            if self.opts.obs != "": 
                intensity = np.zeros_like(result)
                for si in range(self.num_si):
                    for k in range(self.svs.dft.num_k_irr):
                        for i in range(self.correlated_bands_start,self.correlated_bands_end):
                            iomega = np.argwhere(self.svs.sig.omega > result[k, i, si])
                            for v in self.collector.visitors:
                                intensity[k,i+self.correlated_bands_start,si] = v.result[k,iomega,si] * v.obs_value

            self.LEH = LowEnergyHamiltonian(num_k_irr=self.svs.dft.num_k_irr, 
                            num_si=self.num_si, 
                            name=self.label,
                            min_band = self.correlated_bands_start+1, 
                            max_band = self.correlated_bands_end)
            self.LEH.allocate()
            for si in range(self.num_si):
                for k in range(self.svs.dft.num_k_irr):
                    self.LEH.eps[:,k, si] = result[k,self.correlated_bands_start:self.correlated_bands_end,si]
                    for i in range( self.LEH.eps.shape[0] ):
                        if not self.opts.lifetime and not self.opts.qp_weight and self.opts.obs == "":
                            self.LEH.scalar[i ,k, si] = i/ self.LEH.eps.shape[0]
                        else:
                            self.LEH.scalar[i ,k, si] = intensity[k,i+self.correlated_bands_start,si]

            if self.opts.lifetime and self.opts.lifetime_distribution:
                
                kbt = 1/self.svs.beta
                fs_indices = np.argwhere(np.abs(result) < 2*kbt)

                fs_lifetimes = [lifetime[k,i,si] for k,i,si in fs_indices]
                fs_weights = [weights[k] for k,i,si in fs_indices]

                fig = plt.figure(constrained_layout=True)
                ax = fig.add_subplot()
                ax.hist(fs_lifetimes, self.opts.lifetime_distribution_nbin, weights = fs_weights, density = True)

                ax.set_xlabel("Lifetime near FS")
                ax.set_ylabel("Probability")

                pickle.dump(fig, open(f'FSTauDist-{self.svs.super_iter}.{self.svs.sub_iter}.pickle','wb'))

                 
class ARPESBuilder(GreensFunctionVisitor):
    def __init__(self, svs : PEScfEngine, kpath, labels, opts, proj=None, pws = None):
        self.svs = svs
        self.kpath = kpath
        self.labels = labels
        self.opts = opts
        self.isMaster = self.svs.plugin.IsMaster()
        if proj is None:
            if self.isMaster: print(f" - creating a kpath projector, path length: {kpath.num_k}")
            self.proj = self.svs.impurity_delegator.GenerateKPathProjectors(self.svs,kpath)
        else:
            self.proj = proj
        # in general for MPI, KPath band structure is sharded 
        self.pws = pws

        KS : BandStructure = self.proj.GetBandStructureInWindow() # this is cached in the projector
        self.num_k = KS.num_k
        self.is_sharded = KS.is_sharded
        # the sharded image
       
        self.image = ShardedArray(self.svs.plugin.mpi, self.kpath.num_k, 
                                  self.num_k, self.svs.sig.num_omega)
      
        self.broadening = opts.broadening

    def passWeight(self) -> bool:
        return False

    def GetProjector(self) -> Projector:
        return self.proj

    def GetGlocProjector(self) -> Projector:
        return self.proj

    def NonCorrelatedWidening(self) -> float:
        return self.broadening*5

    def CorrelatedWidening(self) -> float:
        return self.broadening

    def GetLabel(self):
        return self.svs.GetMethodName()

    def GetLowBands(self):
        return self.svs.plugin.GetBandsForKPath(self.kpath, 0,  self.svs.orbBandProj.min_band -1 -1)
        
    def GetHighBands(self):
        return self.svs.plugin.GetBandsForKPath(self.kpath, self.svs.orbBandProj.max_band + 1 - 1, self.svs.dft.num_bands)

    def StartKPointsPerOmega(self,iomega, w):
        None

    def VisitKOmegaPoint(self, k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
            negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
            weight, num_atoms):
            
        self.image.partial[k,iomega] += lowEnergyGkDos + negativeOmegaDos + positiveOmegaDos

    def EndKPointsPerOmega(self,iomega, w):
        if self.isMaster and iomega % 100 == 0:
            print("%d/%d %3.3f" % (iomega, self.svs.sig.num_omega, sum(self.image.partial[:,iomega])))

    def End(self):
        # collect the image from all the shards
        self.result = self.image.GatherOnMaster()
        if (self.isMaster):
            hskp_placer = None
            if self.pws is not None:
                hskp_placer = self.pws.hskp_placer
                self.result = self.pws.unfold(self.result)
                    
            fig, ax = plot_spectral_f(self.labels, self.svs.sig.omega[:], self.result, self.opts, hskpp = hskp_placer)
            pickle.dump(fig, open(f'ARPES-{self.svs.super_iter}.{self.svs.sub_iter}.pickle','wb'))
            plt.close()

# delegates to multiple visitors with 
class ResolvedARPESBuilder(ARPESBuilder):
    def __init__(self, svs : PEScfEngine, kpath, resolver, name : str, labels, opts, proj, pws, collector, value, color, linestyle, marker):
        ARPESBuilder.__init__(self, svs,kpath, labels, opts, proj=proj, pws = pws)
        
        self.name = name
        self.value = value
        self.collector = collector
        self.resolver = resolver
        self.color = color
        self.linestyle = linestyle
        self.marker = marker
        #print("partial observable:")
        #PrintMatrix(self.resolver)

        self.dos = np.zeros(self.svs.sig.num_omega) #this is no longer only used by kpath users / but also IBZ users
    
    def VisitKOmegaPoint(self, k, iomega, w, si, rho, gradients, lowEnergyGkDos,
            negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
            weight, num_atoms):
        # rho passed here is a projected local density
        
        partialDos = np.trace(self.resolver[si] * rho)[0,0] * self.svs.num_equivalents
        self.image.partial[k,iomega] += np.real(partialDos) / num_atoms
        self.dos[iomega] += np.real(partialDos) / num_atoms * weight

    def End(self, plot):
        # collect the image from all the shards
        self.result = self.image.GatherOnMaster()
        if (self.isMaster):
            shell_name, key = self.collector.decipher_label(self.name)
            self.collector.add(shell_name, key, self.result)

            hskp_placer = None
            if self.pws is not None:
                hskp_placer = self.pws.hskp_placer
                self.result = self.pws.unfold(self.result)

            if plot:
                fig, ax = plot_spectral_f(self.labels, self.svs.sig.omega[:], self.result, self.opts, hskpp = hskp_placer)
                pickle.dump(fig, open(f'pARPES.{self.name}.pickle','wb'))
                plt.close()

            """
            with open("p-arpes.json", "w") as f:
                import json

                output = {"x" : self.labels, "y": self.svs.sig.omega.tolist(), "z" : self.result.tolist()}
                json.dump(output, f, indent=4)
            """



class MultiARPESBuilder(ARPESBuilder):
    def __init__(self, svs : PEScfEngine, kpath, labels, opts, pws = None):
        ARPESBuilder.__init__(self, svs,kpath, labels, opts, pws = pws)
        self.collector = Projected_ARPES_collector(svs, self.svs.sig.omega[:], self.proj.shell.num_equivalents, opts, labels = labels, kpath = kpath, pws = pws)

    def VisitKOmegaPoint(self, k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
            negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
            weight, num_atoms):

        #projected plots
        self.collector.VisitKOmegaPoint(k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
            negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
            weight, num_atoms)

        # this is the total plot
        ARPESBuilder.VisitKOmegaPoint(self, k, iomega, w, si, Gk, gradients, lowEnergyGkDos, negativeOmegaDos, 
                                                    lowEnergyGkDos, positiveOmegaDos, weight, num_atoms)

    def EndKPointsPerOmega(self,iomega, w):
        if self.isMaster and iomega % 100 == 0:
            print("%d/%d %3.3f" % (iomega, self.svs.sig.num_omega, sum(self.image.partial[:,iomega])))

    def End(self):
        ARPESBuilder.End(self)
        self.collector.End()
        self.collector.store()

class BandBuilder(GreensFunctionVisitor):
    def __init__(self, svs : PEScfEngine, kpath, labels, opts, proj=None):
        self.svs = svs
        self.kpath = kpath
        self.labels = labels
        self.opts = opts
        self.isMaster = self.svs.plugin.IsMaster()
        
        if proj is None:
            if self.isMaster: print(f" - creating a kpath projector, path length: {kpath.num_k}")
            self.proj = self.svs.impurity_delegator.GenerateKPathProjectors(self.svs, kpath)
        else:
            self.proj = proj

        # in general for MPI, KPath band structure is sharded 
        KS : BandStructure = self.proj.GetBandStructureInWindow() # this is cached in the projector
        self.num_k = KS.num_k
        self.num_bands = self.svs.dft.num_bands
        self.is_sharded = KS.is_sharded
        # the sharded image

        self.num_low_bands = self.proj.min_band - 1
        self.num_mid_bands = self.proj.max_band - self.proj.min_band + 1
        self.num_high_bands = self.svs.dft.num_bands - self.num_mid_bands - self.num_low_bands + 1
        
        mpic = self.svs.plugin.mpi
        
        self.correlated = ShardedArray(mpic, self.kpath.num_k, 
                                  self.num_k, self.num_mid_bands, self.svs.num_si)

        self.uncorrelated = ShardedArray(mpic, self.kpath.num_k, 
                                  self.num_k, self.num_low_bands + self.num_high_bands, self.svs.num_si)

        self.uncorrelated.partial[:,self.num_low_bands:] = sys.float_info.max  

    def passWeight(self) -> bool:
        return False

    def GetProjector(self) -> Projector:
        return self.proj

    def GetGlocProjector(self) -> Projector:
        return self.proj

    def GetLabel(self):
        return self.svs.GetMethodName()
    def GetLowBands(self):
        return self.svs.plugin.GetBandsForKPath(self.kpath, 0,  self.svs.orbBandProj.min_band -1 -1)
    def GetHighBands(self):
        return self.svs.plugin.GetBandsForKPath(self.kpath, self.svs.orbBandProj.max_band + 1 - 1, self.svs.dft.num_bands)

    def VisitKPoint(self, k, si, Hqp,
            lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
            weight, num_atoms):

        self.correlated.partial[k,:,si] += midEpsK

        nhigh = len(highEpsK)
        self.uncorrelated.partial[k,:self.num_low_bands,si] += lowEpsK
        self.uncorrelated.partial[k,self.num_low_bands:self.num_low_bands+nhigh,si] += highEpsK
       
    def End(self):
        # collect the image from all the shards
        self.correlated_result = self.correlated.GatherOnMaster()
        self.uncorrelated_result = self.uncorrelated.GatherOnMaster()
        if (self.isMaster):
            fig, ax = plot_bands(self.labels, self.correlated_result,  self.uncorrelated_result, self.opts,
                 no_spin = self.svs.nrel == 1 and self.svs.num_si == 1)
            pickle.dump(fig, open(f'BS-{self.svs.super_iter}.{self.svs.sub_iter}.pickle','wb'))
            plt.close()


# delegates to multiple visitors with 
class ResolvedBandBuilder(BandBuilder):
    def __init__(self, svs : PEScfEngine, kpath, resolver, name : str, labels, opts, proj, collector):
        BandBuilder.__init__(self, svs, kpath, labels, opts, proj=proj)

        self.name = name
        self.collector = collector
        self.resolver = resolver
        #print("partial observable:")
        #PrintMatrix(self.resolver)
    
    def VisitKPoint(self, k, si, rho,
            lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
            weight, num_atoms):

        for ib, e_qp in enumerate(midEpsK):
            g = Matrix(rho[ib,:,:]) * self.resolver[si]
            self.correlated.partial[k,ib,si] += np.real(np.trace(g))[0,0]

    def End(self, plot_):
        # collect the image from all the shards
        self.result = self.correlated.GatherOnMaster()
        if (self.isMaster):
            shell_name, key = self.collector.decipher_label(self.name)
            self.collector.add(shell_name, key, self.result)

class MultiBandBuilder(BandBuilder):
    def __init__(self, svs : PEScfEngine, kpath, labels, opts):
        BandBuilder.__init__(self, svs, kpath, labels, opts)

        num_equivalents = 1 # self.proj.shell.num_equivalents
        self.collector = Projected_band_collector(svs, num_equivalents, opts, labels = labels, kpath = kpath)

        self.broadening = 1j*0.001

    def VisitKPoint(self, k, si, Hqp,
            lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
            weight, num_atoms):
        
        #projected bands
        self.collector.VisitKPoint(k, si, Hqp,
                lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
                weight, num_atoms)

        # this is the total plot
        BandBuilder.VisitKPoint(self, k, si, Hqp, lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights, weight, num_atoms)

    def End(self):
        BandBuilder.End(self)
        self.collector.End()
        if self.isMaster:
            fig, ax = plot_bands(self.labels, self.correlated_result, self.uncorrelated_result, self.opts, characters=self.collector.get_characters(), 
                no_spin = self.svs.nrel == 1 and self.svs.num_si == 1)
            pickle.dump(fig, open(f'pBS-{self.svs.super_iter}.{self.svs.sub_iter}.pickle','wb'))
            plt.close()
            
class GreensFunctionBuilder(GreensFunctionVisitor):
    def __init__(self, svs, opts, proj = None):

        self.svs = svs
        self.opts = opts
        self.isMaster = self.svs.plugin.IsMaster()
        
        if proj is None:
            self.proj = svs.orbBandProj
        else:
            self.proj = proj
        # in general for MPI, KPath band structure is sharded 
        
        basis = svs.plugin.GetLAPWBasis()

        KS : BandStructure = self.proj.GetBandStructureInWindow() # this is cached in the projector
        self.num_k = KS.num_k
        self.is_sharded = KS.is_sharded
        # the sharded image
        self.num_correlated_bands = self.GetGlocProjector().max_band - self.GetGlocProjector().min_band + 1
        
        self.re_image = ShardedArray(self.svs.plugin.mpi, basis.num_k_irr, 
                                  self.num_k, self.svs.num_si, self.svs.sig.num_omega, 
                                  self.num_correlated_bands, self.num_correlated_bands)

        
        self.im_image = ShardedArray(self.svs.plugin.mpi, basis.num_k_irr, 
                                  self.num_k, self.svs.num_si, self.svs.sig.num_omega, 
                                  self.num_correlated_bands, self.num_correlated_bands)
      
        self.broadening = 0

    def passWeight(self) -> bool:
        return False

    def GetProjector(self) -> Projector:
        return self.proj

    def GetGlocProjector(self) -> Projector:
        return self.proj

    def NonCorrelatedWidening(self) -> float:
        return self.broadening*0.25
    def CorrelatedWidening(self) -> float:
        return self.broadening
    def GetLabel(self):
        return self.svs.GetMethodName()

    def StartKPointsPerOmega(self, iomega, w):
        None

    def GetLowBands(self):
        return self.svs.plugin.GetBandsForAllKpoints(0,  self.GetGlocProjector().min_band -1 -1)
    def GetHighBands(self):
        return self.svs.plugin.GetBandsForAllKpoints(self.GetGlocProjector().max_band + 1 - 1, self.svs.dft.num_bands)


    def EndKPointsPerOmega(self,iomega, w):
        if self.isMaster and iomega % 100 == 0:
            print("%d/%d" % (iomega, self.svs.sig.num_omega))
    
    def VisitKOmegaPoint(self, k, iomega, w, si, Gk, gradients, lowEnergyGkDos,
            negativeOmegaDos, lowOmegaDos, positiveOmegaDos,
            weight, num_atoms):
        self.re_image.partial[k, si, iomega] += np.real(Gk)
        self.im_image.partial[k, si, iomega] += np.imag(Gk)

    def End(self):
        r = self.re_image.GatherOnMaster()
        i = self.im_image.GatherOnMaster()
        if self.isMaster:
            self.result = r + 1j*i
        else:
            self.result = None
        self.ready = True
         

if __name__ == '__main__':
    pass
