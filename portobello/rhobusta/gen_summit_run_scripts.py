import os, sys, shutil
from argparse import ArgumentDefaultsHelpFormatter
import numpy as np

from portobello.rhobusta.options import ArgumentParserThatStoresArgv

def suffix(i : int, no_suffix = False):
    if i < 0 or no_suffix:
        return ""
    else:
        return str(i)

class bsub_template:
    
    workers_per_node = 42

    def __init__(self, account, nnodes, walltime, name, alpha_minus, alpha_plus, temp):

        if account == "INCITE":
            account = "MAT229"
        elif account == "DD":
            account = "CPH128"

        assert (account == "CPH128" or account == "MAT229")

        self.header = f'''#!/bin/bash
#BSUB -P {account}
#BSUB -W {walltime}
#BSUB -nnodes {nnodes}
#BSUB -J {name}
#BSUB -o out.%J.txt
#BSUB -e err.%J.txt
#BSUB -alloc_flags "smt1 nvme"
        
source ~/.bashrc
        
'''

        self.nnodes = nnodes

        self.set_dc(alpha_minus, alpha_plus)
        self.T = temp

    def set_dc(self,alpha_minus, alpha_plus):
        self.dc = f"\"-jlow*{alpha_minus}+jhigh*{alpha_minus}+jlow*{alpha_plus}+jhigh*{alpha_plus}\""


    def write(self, fname, run, run_opts, impurities = [-1]):
        with open(fname,"w") as f:

            f.write(self.header)

            launch_cmd = run.program 
            if not run.cd_imp_dir:
                launch_cmd += f" {run_opts} --dc-term={self.dc} -T{int(self.T)}"
            
            for impurity in impurities:

                outfile = fname.split("/")[1][:-3]
                f.write(launch_cmd)
                f.write("\n\n")

            f.write(f"{run.subsequent} \n\n")


   
def OptionParser(add_help=False):
    
    parser = ArgumentParserThatStoresArgv('summit job generator', add_help=add_help,
                            formatter_class=ArgumentDefaultsHelpFormatter)    
   
    parser.add_argument("--nnodes-dft", dest="nnodes_dft",
                     default=4, type=int,
                     help="Number of nodes requested for dft part")

    parser.add_argument("-N", "--nnodes", dest="nnodes", 
                     default=50, type=int,
                     help="Number of nodes requested")
    
    parser.add_argument("-W", "--walltime", dest="walltime", 
                     default="02:00",
                     help="Amount of time requested [hr:mn:sc]")

    parser.add_argument("-J", "--run-description", dest="run_description", 
                     default="delta",
                     help="Prepend this description onto the job name")

    parser.add_argument("-A", "--account", dest="account", 
                     default="INCITE",
                     help="account code (or INCITE / DD) to charge job to")

    parser.add_argument("-R", "--run-options", dest="run_options", 
                     default="--admix=0.025 -R --RK=9 -K1.5 -U4.5 -J0.512 -N5.0  -w-10:10 -sPu:5f -Xdiag -Badapt -D1 --time-thermalization=15 --time-measurement=45 --simulations-per-GPU=5 --full-interaction  -i100 -y20",
                     help="Set of options to pass to portobello")

    parser.add_argument("--alpha-minus", dest="alpha_minus", 
                     default="0.1:0.1:1",
                     help="alpha minus range -- start:stop:number")

    parser.add_argument("--alpha-plus", dest="alpha_plus", 
                     default="1.5:1.5:1",
                     help="alpha minus range -- start:stop:number")

    parser.add_argument("-T","--temperatures", dest="temperatures",
                     default="500:800:4",
                     help="alpha minus range -- start:stop:number")

    parser.add_argument("--lattice-constants", dest="lattice_constants",
            default="1.05:1.08:1.1:1.12:1.15",
                     help="number of lattice parameters to test")

    parser.add_argument("--workers-per-node", dest="workers_per_node", 
                     default=42, type=int,
                     help="number of workers to spawn per node")

    parser.add_argument("--num-impurities", dest="num_impurities", 
                     default=8, type=int,
                     help="number of impurities")

    parser.add_argument("--cif", dest="cif_file", 
                     default = "ICSD_CollCode44768.cif",
                     help="cif file of run")

    return parser

class Run:
    def __init__(self, program, batch_script_file, nnodes = 1, walltime = "00:30:00", subsequent = "", add_cif_arg = False, cd_imp_dir = False, num_impurities = 0, max_contributors = 0):
        self.program = program
        self.batch_script_file = batch_script_file
        self.nnodes = nnodes
        self.walltime = walltime
        self.subsequent = subsequent
        self.add_cif_arg = add_cif_arg
        self.cd_imp_dir = cd_imp_dir

        if num_impurities > 0:
            self.impurities = []
            last_job = -1
            while last_job <= num_impurities:
                self.impurities.append(list(range(last_job+1, min(num_impurities,last_job+1+max_contributors))))
                last_job += max_contributors
            self.impurities.pop()
            self.no_suffix = num_impurities == max_contributors
        else:
            self.no_suffix = True
            self.impurities = [[-1]]

def main():
    parser = OptionParser(add_help=True) 
    opts = parser.parse_args(sys.argv[1:])
    
    with open("log.txt","w") as f:
        f.write(' '.join(sys.argv))

    python_command = "~/.conda/envs/portobello-conda-openmpi/bin/python"

    step0 = Run(f"{python_command} -m portobello.rhobusta.dft__dmft {opts.cif_file} -I0 --mpi-workers={int(opts.nnodes_dft*42)}",
                "step0.sh",
                nnodes = opts.nnodes_dft,
                walltime = "2:00")

    cont = Run(f"{python_command} -m portobello.rhobusta.dft__dmft --runner-params=\"-a6 -c6 -g6\" -I4",
                "step0.sh",
                walltime = "6:00")

    step1 = Run(f"{python_command} -m portobello.rhobusta.rhobusta --plus-sigma --mpi-workers={int(opts.nnodes_dft*42)}",
                "dft-plus.sh",
                nnodes = opts.nnodes_dft,
                walltime = "2:00",
                subsequent = "bsub dmft.sh")

    step2 = Run(f"{python_command} -m portobello.rhobusta.dmft --runner-params=\"-a6 -c6 -g6\"",
                "dmft.sh",
                walltime = "2:00",
                num_impurities = 1,
                max_contributors = 1)

    
    energy = Run(f"{python_command} -m portobello.rhobusta.dmft_energy --runner-params=\"-a6 -c6 -g6\" --scale-times --scaling-factor=0.75",
                "energy.sh",
                walltime = "2:00",
                num_impurities = 1,
                max_contributors = 1)

    runs = [step0, cont, step1, step2, energy]

    lattice_scalings = np.array(opts.lattice_constants.split(":"), dtype=float)

    p = np.array(opts.alpha_minus.split(":"), dtype=float)
    alpha_minus_range = np.round(np.linspace(p[0], p[1], int(p[2]), endpoint = True),2)

    p = np.array(opts.alpha_plus.split(":"), dtype=float)
    alpha_plus_range = np.round(np.linspace(p[0], p[1], int(p[2]), endpoint = True), 3)

    p = np.array(opts.temperatures.split(":"), dtype=float)
    temperatures = np.linspace(p[0], p[1], int(p[2]), endpoint = True)

    run_scripts = [[] for r in runs]
    for alpha_minus in alpha_minus_range:
        for alpha_plus in alpha_plus_range:
            for temp in temperatures:
                for lattice_scale in lattice_scalings:
                    f = 1./lattice_scale#lattice_scalings[0]/lattice_scale
            
                    folder = f"{opts.run_description}-{alpha_minus}-{alpha_plus}-{temp}"

                    if not os.path.exists(folder):
                        os.makedirs(folder)

                    folder += f"/{lattice_scale}"
                    if not os.path.exists(folder):
                        os.makedirs(folder)

                    if opts.cif_file:
                        shutil.copyfile(opts.cif_file, folder + "/" + opts.cif_file)

                    for i, run in enumerate(runs):
                        
                        run_options = opts.run_options + f" -a{lattice_scale} -f{f}"
                        if "--mpi-workers=" not in run.program:
                            run.nnodes = int(opts.nnodes)#int(opts.nnodes * 650/temp)
                            run_options += f" --mpi-workers={run.nnodes}"

                        job_script = bsub_template(opts.account, run.nnodes, run.walltime, f"{folder}-{run.batch_script_file[:-3]}", alpha_minus, alpha_plus, temp)

                        run_suffix = ".sh"
                        job_script.write(folder + "/" + run.batch_script_file[:-3] + run_suffix, run, run_options)
                        run_scripts[i].append(folder + "/" + run.batch_script_file[:-3] + run_suffix)



    for i, run in enumerate(runs):
        with open (f"{opts.run_description}.{run.batch_script_file[:-3]}.bash","w") as f:
            f.write("#!/bin/bash\n")
            for script in run_scripts[i]:
                s = script.split("/")
                jdir = "/".join(s[:-1])
                cwd = os.getcwd()
                f.write(f"cd {jdir}\n")
                f.write(f"bsub {s[-1]}\n")
                f.write(f"cd {cwd}\n")

            f.write(f"\n")

if __name__ == '__main__':
    main()
