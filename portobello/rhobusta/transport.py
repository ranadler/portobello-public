#!/usr/bin/env python3

'''
Created on Jul 9, 2019

@author: adler
'''
from argparse import ArgumentDefaultsHelpFormatter
import sys
from portobello.generated import TransportOptics, FlapwMBPT, LAPW
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.rhobusta import Ry2eV
from os.path import exists
from portobello.rhobusta.DFTPlugin import DFTPlugin
import gc
import numpy as np
from portobello.bus.mpi import GetMPIProxyOptionsParser
from portobello.rhobusta.loader import AnalysisLoader

K2eV = 8.621738e-5  
KB   = 6.333328e-6 *27.2107* 0.5 # collected from Kutepov's code, to match perfectly

def Print(ini,out : TransportOptics.Output ,opts):

    # print out a few outputs
    print(ini.allfile[1:])
    print("T=", ini.ctrl.temperature)
    
    print("σ [(Ω*cm)⁻¹]: ",out.A[0,:,0])
    print("ρ [μΩ*cm]:    ", 1/out.A[0,:,0] *1.0e6)

    print("<σ> [(Ω*cm)⁻¹]: ", sum(out.A[0,:,0])/3.0)
        
    # - out.chemical_potential ?
    seebeck = -out.A[0,:,1] / out.A[0,:,0] * Ry2eV / ini.ctrl.temperature * 1.0e6 
    
    print("S [μV/K]:     ", seebeck[:])  
        
    if out.num_omega > 1:
        print("Plotting optical conductivity")
        import matplotlib
        import matplotlib.pyplot as plt
        
        matplotlib.use("TkAgg")
        for d in range(out.num_dirs):
            # plot the optical conductivity A (no prefactor needed)
            plt.plot(out.omega[:], out.A[:,d,0], alpha=0.5, label="xyz"[d])
            plt.xlim(0.0, max(out.omega[:]))
            plt.ylabel("σ [(Ωcm)⁻¹]")
            plt.xlabel("$\omega$ [eV]")
        plt.plot(out.omega[:], 1/3.0*np.sum(out.A[:,:,0],axis=1), alpha=0.5, label="average")
            
        plt.legend()
        plt.show(block=True)
        
    
    if opts.plots > 1:
        
        ifd = TransportOptics.IntegrandForDebug()
        ifd.load("./transport_integrand.h5:/")
        
        if out.num_omega <= 1:
            import matplotlib.pyplot as plt
        
        plt.plot(ifd.omega[:], -1/np.pi *np.imag(ifd.G[:]), alpha=0.5) 
        #plt.plot(ifd.omega[:], np.real(ifd.integrand[:]), alpha=0.5)
        plt.show()
    
    
   
class TransportRunner(AnalysisLoader):
    def __init__(self, args, kwargs):
        
        parser = ArgumentParserThatStoresArgv(parents=[GetMPIProxyOptionsParser()],
                                              formatter_class=ArgumentDefaultsHelpFormatter)    
            
        # opts are all in eV units, later translated to input file (inp) which is is Ry units
            
        parser.add_argument("-T", "--Temperature", dest="Temperature", default=None,
                          type=int, help="Temperature in Kelvin")
     
        parser.add_argument("-b", "--beta", dest="beta", type=float, default=None)
        
        parser.add_argument("-e", "--energy-limit", dest="energy_limit", default=1.0, type=float,
                         help="""energy limit for self-energy frequency: this can be set to 0.0 safely (to reduce memory).
                                The integration mesh is set in the window num_omega*delta + max(4kT, energy_limit)""")
    
        parser.add_argument("-g", "--gamma", dest="gamma", default=0.001, type=float,
                         help="broadening")
        parser.add_argument("-G", "--gamma-corr", dest="gamma_corr", default=0.001, type=float,
                         help="broadening")
        parser.add_argument("-J", "--gamma-hyb", dest="gamma_hyb", default=0.0001, type=float,
                         help="broadening")
   
        parser.add_argument("-D", "--delta", dest="delta", default=0.0005*Ry2eV, type=float,
                         help="energy spacing between sample points of G (in eV)")
   
        parser.add_argument("-w", "--energy-window", dest="window",
                          help="energy window for the low energy hamiltonian (in eV)",
                          default=None)
           
        parser.add_argument('-O', "--optics", dest="num_omega", 
                         default=0, type=int,
                         help="number omega points of the optics plot. The optics range is [0 .. num_omega*delta], as given above.")
    
        parser.add_argument("-P", "--plots", dest="plots", 
                        help="print summary (-P1) or print and plot some graphs (-P2)",
                        default=0, type=int)
        
        parser.add_argument("--momentum-k", dest="momentum_k",
                          action="store_true",
                          default=False,
                          help="use k as the momentum") 
        
        parser.add_argument("--irr-bz", dest="symmetrize_bz",
                          action="store_false",
                          default=True,
                          help="set if not to symmetrize bz (in almost all cases one should not set this)")
        
        parser.add_argument("--test-velocity", dest="test_velocity",
                          action="store_true",
                          default=False,
                          help="test velocity matrix to be hermitian") 
            
        parser.add_argument("--which-shell", dest="which_shell", default=-1, type=int,
                        help="""which shell (index) to project to, in order to build the correlated problem. Note that -1 does not mean that this is 
                        a single-impurity - it merely makes it possible to say that "all" impurities should be considered. The file suffix of gutz or dmft
                        will be determined based on comparing the dim of its correlated subspace to the full dimension of the projector. Note that for 
                        the gutzwiller of dmft computation, -1 is meaningless, so it is set to 0.
                        """)

        
        opts = parser.parse_args(args)
        AnalysisLoader.__init__(self, opts)

        for key in kwargs:
            setattr(self.opts, key, kwargs[key])

        self.input_location = "./transport.h5:/input/"
        self.output_location = './transport.h5:/output'
        self.input_objects_path = './ini.h5:/'
        
        assert(exists("./ini.h5"))
             
                
    def BeforeWorkers(self):
        self.ini = FlapwMBPT.Input()
        self.ini.load(self.input_objects_path)
                
        self.inp = TransportOptics.Input()
        self.inp.allocate()
        self.inp.dirs[0,0,0] = 1.0 
        self.inp.dirs[1,1,1] = 1.0 
        self.inp.dirs[2,2,2] = 1.0 
        self.inp.test_velocity = self.opts.test_velocity
        
        if self.opts.beta is None:
            if self.opts.Temperature is None:
                self.opts.Temperature = self.ini.ctrl.temperature
            #TODO: take the temperature from the ini  file
            self.opts.beta = 1/(self.opts.Temperature * KB)
        else:
            assert(self.opts.beta is not None)
            self.opts.Temperature = 1/(self.opts.beta * KB)
            
        self.inp.Temperature = self.opts.Temperature * K2eV / Ry2eV
        self.inp.delta = self.opts.delta / Ry2eV
        self.inp.num_omega = self.opts.num_omega
        
        # omega max is the range used for integration, where sigma is available        
        # we integrate at least in 4K_B*T interavl around the omega point, with at least energy_limit as well.
        self.inp.active_omega_points =  max(self.opts.energy_limit/ Ry2eV, 4*self.inp.Temperature) / self.inp.delta
        self.inp.omega_max = self.inp.delta * (self.inp.active_omega_points + self.inp.num_omega)
        self.inp.max_omega_mesh = int(self.inp.omega_max / self.inp.delta)
 

        self.inp.gamm = self.opts.gamma / Ry2eV
        self.inp.gammc = self.opts.gamma_corr / Ry2eV 
        self.inp.symmetrize_bz = self.opts.symmetrize_bz
            

        self.momentum_kind = 'gradient'
        if self.opts.momentum_k:
            self.inp.momentum_kind = 'k'

        if self.opts.plots > 0 and self.opts.plots < 3:        
            out = TransportOptics.Output()
            out.load(self.output_location)
            Print(self.ini, out, self.opts)
            return
        
        if self.opts.plots <3 and self.opts.plots>0: 
            return False
      
        self.inp.store(self.input_location, flush=True)
                
        if self.opts.plots >0: 
            return False

        return True
                
    
    def MPIWorker(self):
        MPI = self.GetMPIInterface()

        self.LoadCalculation(loadRealSigma = False) #real sigma loaded in self.svs.EmbedInterpolatedOnRealOmega

        if self.IsMaster():
            self.inp = TransportOptics.Input()
            self.inp.load(self.input_location)

            if self.svs is not None:
                self.inp.min_band = self.svs.orbBandProj.min_band 
                self.inp.max_band = self.svs.orbBandProj.max_band 
            else:
                self.inp.min_band = 1
                self.inp.max_band = 0
            
            if self.opts.window is not None:
                (low,high) = [int(x) for x in self.opts.window.split(":")]
            else:
                if self.svs is not None:
                    (low,high) = (self.svs.proj_energy_window.low, self.svs.proj_energy_window.high)
                else:    
                    low,high = 0.0,0.0
            
            self.inp.energy_window = max(abs(low),abs(high)) / Ry2eV

            # store the input min-max bands and energy bounds
            self.inp.store(self.input_location, flush=True)
            self.Barrier() # wait until the other workers start reading (all threads need to have the barrier)
        else:
            self.Barrier() # wait for the input to be written by the master
            self.inp = TransportOptics.Input()
            self.inp.load(self.input_location)
                    
        if self.svs is not None:
            ese = self.svs.EmbedInterpolatedOnRealOmega(self.inp, debug=self.opts.plots == 3)
            ese.store(f"./shard-{self.rank}.core.h5:/", flush=True)
            del ese
                
        TransportOptics.Input.ComputeFromPortobello(self.input_location)
        
        # MUST call this in the end
        self.Barrier()
        self.Terminate()
        gc.collect()
        
    def AfterWorkers(self):
        if False:
            out = TransportOptics.Output()
            out.load(self.output_location)
        
if __name__ == '__main__':  
    supervisor = TransportRunner(sys.argv[1:], {})
    supervisor.Run()
    
    del supervisor