
#!/usr/bin/env python3

'''
Created on June 3, 2021

@author: adler@physics.rutgers.edu

Calculate the Gutzwiller susceptibility matrix M, following code and paper by Tsung Han.

'''
