'''
Created on Aug 19, 2018

@author: adler
'''
from itertools import combinations
from pymatgen.core.periodic_table import Element
import re
from portobello.generated.FlapwMBPT_basis import MuffinTinBasis
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.generated.FlapwMBPT import Input
from portobello.bus.Matrix import Matrix
from collections import OrderedDict
import numpy as np
from scipy.special import sph_harm
from portobello.symmetry.local import GetRealSHMatrix, StructLocalSymmetryAnalyzer,\
    GetWignerDForRots
from cmath import sqrt
from portobello.generated.atomic_orbitals import SelectedOrbitals, AtomicShell
from numpy.linalg.linalg import det
from portobello.symmetry.magmom import GroupAnalyzerWithMagmom

class ShellAnalyzer:

    def __init__(self, species, distinct_number, spec):
        self.species = species # Z number of each element in the full list of atoms
        self.requested = spec.split(",") # user input
        # map to 1st index in distinct list
        eqclass = {}
        self.first_in_eqclass = {}
        for i, dist in enumerate(distinct_number):
            if dist in eqclass:
                self.first_in_eqclass[i] = eqclass[dist]
            else:
                eqclass[dist] = i
                self.first_in_eqclass[i] = i

    def Lof(self, orb_char):
        if orb_char == 's':
            return 0
        if orb_char == 'p':
            return 1
        if orb_char == 'd':
            return 2
        if orb_char == 'f':
            return 3
        if orb_char == 'e':
            return 4
        return -1

    def MatchUserInputToShells(self):
        shells = []
        for reqorb in self.requested:
            Nel, Norb = reqorb.split(":")
            n = int(Norb[:-1])
            l = self.Lof(Norb[-1])
            vals = re.split('(\D+)', Nel)
            if len(vals) != 3:
                # specific atomic number was specified
                iatom = self.first_in_eqclass(int(vals[0])-1)
                shells += [(iatom, n, l)]
            else:
                iatomStr, el, _space = vals
                if iatomStr != '':
                    shells += [(int(iatomStr), n, l)]
                else:
                    element = Element(el)
                    Z = element.Z
                    for i in self.first_in_eqclass.values():
                        if self.species[i] == Z:
                            shells += [(i, n, l)]
        return shells

def PrintMatrix(s, M, max_line_width=180):
    print(s+"\n\t"+np.array_str(M, precision=5, max_line_width=max_line_width, suppress_small=True).replace('\n','\n\t'))
       

class LAPWRec:
    def __init__(self, iorb, atom_number, n, l, m, kind, correlated):
        self.iorb = iorb
        self.atom_number = atom_number # starts from 0
        self.n = n
        self.l  = l 
        self.m = m 
        self.kind = kind
        self.correlated = correlated

    def __str__(self):
        return "a=%d,n=%d,l=%d,m=%2.2f,kind=%d corr=%d"% (self.atom_number, 
                                                  self.n, self.l, 
                                                  (1.0)*self.m, self.kind, self.correlated)
    
    def key(self):
        return (self.atom_number, self.n, self.l, self.kind)


class SymmetrizedShell:
    def __init__(self, plugin, orb):
        self.lapw_indices = []
        self.kind = orb.kind
        self.distinct_lapw_indices = []
        self.equiv_atoms = [orb.atom_number]
        self.n = orb.n
        self.l = orb.l
        self.distinct_index = plugin.dft.st.distinct_number[orb.atom_number]-1
        self.orig_index = orb.atom_number # use the first one as the distinct
        self.dim = plugin.dft.nrel *(2*self.l + 1)
        self.rotation = [Matrix(np.eye(self.dim, self.dim, dtype=np.float64))]
        self.rot3     = [Matrix(np.eye(3, 3, dtype=np.float64))]  # for debug
        self.translation = [np.zeros((3), dtype=np.float64)]
        self.g = plugin.GetEquivalentAtomSymmetries() # an array - a rotation in L for each atom to its firt equivalent
        self.num_anti_equivalents = 0
        self.pendingRotations = []
        self.dft = plugin.dft
        
           
    def GetDMatrix(self, op):
        if self.dft.nrel == 1:
            return Matrix(getattr(self.dft.sg,"WigD"+str(self.l))[:,:,op])
        else:
            return Matrix(getattr(self.dft.sg,"WigD"+str(self.l)+"J")[:,:,op])

    def GetTranslation(self,op):
        return self.dft.sg.shifts[:,op]        
    
    def GetRot3(self,op):
        return Matrix(self.dft.sg.rots[:,:,op])
           
    def GetSymmetryOp(self, atom_num):
        return self.g[atom_num]
           
    def AddEquivalentAtom(self, orb):
        #print (orb.atom_number, orb.n, orb.l, orb.m, orb.kind, orb.iorb)
        self.lapw_indices.append(orb.iorb)
        if orb.atom_number not in self.equiv_atoms:
            self.equiv_atoms.append(orb.atom_number)
            op = self.GetSymmetryOp(orb.atom_number)
            
            # these are the equivalence transformation, once in 2l+1 dims, once in 3 dims
            self.rotation.append(self.GetDMatrix( op))
            
            self.rot3.append(self.GetRot3(op))

            self.translation.append(self.GetTranslation(op))
        if self.orig_index == orb.atom_number:
            self.distinct_lapw_indices.append(orb.iorb)
            assert(len(self.distinct_lapw_indices) <= self.dim)
            
    def AddAntiEquivalentAtom(self,orb, afms):
        assert(self.orig_index != orb.atom_number)
        self.lapw_indices.append(orb.iorb)
        # add the atom only once, as above in equivalent atoms
        if orb.atom_number not in self.equiv_atoms:
            self.equiv_atoms.append(orb.atom_number)
            
            #find the opposite sign atom and multiply the rotation with the rotation in AFMs 
            op = self.GetSymmetryOp(afms.opposite_site)
            #print "**** symmetry op to %d from its equivalent *********" %(afms.opposite_site)
            #print op
            #print "afms op:"
            #print afms.op
            afmM = Matrix(afms.op[0:3,0:3]).T
            self.pendingRotations.append([afmM, self.GetDMatrix(op)])
            
            # Now append our own symmetry in 2l+1 dimensions, given by afms.op 
            # Note: we ignore the translation in the operation, it contributes only a phase to the entire matrix
            
            tt = self.GetTranslation(op)
            self.rot3.append(afmM*self.GetRot3(op))
            tt2 = np.zeros_like(tt)
            tt2[:] = tt*afmM
            self.translation.append(tt2)
            
            self.num_anti_equivalents += 1
        

    # this is designed so that we make ONE call to fortran (GetWignerDForRots) to get all the relevant 
    # AFM rotation matrices
    # The picking of the actual D matrices needs to happen in the end, only when we know the actual l 
    # for the AFM shells.
    def FinishPendingRotations(self):
        if len(self.pendingRotations) == 0:
            return # nothing to do
        rots = []
        for rot, _D in self.pendingRotations:
            rots.append(rot)
        _group, Dl = GetWignerDForRots(rots, self.dft.nrel, "AFM_rots")
        for i, (rot, DMat) in enumerate(self.pendingRotations):
            # the rotation matrix is the result of composing the AFM rotation on top of the
            # rotation from the original (class rep) atom to the AFM anti-equivalent atom (Dmat)
            
            # TODO: test this correct expression
            self.rotation.append(Matrix(Dl[self.l][:,:,i]) * DMat)
        self.pendingRotations = []
        
    # st_symm : StructLocalSymmetry
    def SetRotMatrixAndRep(self, st_sym, approx_rep, cluster_size):
        print(" - subshell: atom=%d, n=%d l=%d, 1st atom: %s all: %s" %(self.distinct_index, self.n, self.l, str(self.distinct_lapw_indices), str(self.lapw_indices)))
        
        annotatedRep = st_sym.distinct_symm[self.distinct_index].annotated_reps_l[self.l]
        print("   basis:", [l.short_label for l in annotatedRep.labels])
        self.labels = annotatedRep.labels
        assert(annotatedRep.basisChangeD.shape == (self.dim*cluster_size, self.dim*cluster_size)), (annotatedRep.basisChangeD.shape, self.dim)
        self.basisChangeD = annotatedRep.basisChangeD

        self.subM = Matrix(annotatedRep.rotD) * Matrix(annotatedRep.basisChangeD)
        PrintMatrix("   axis rotation * basis change:", np.real(self.subM))
        
        self.rep =  annotatedRep.ApproximateRepresentation(approx_rep, self.subM) # a matrix
        self.raw_rep = annotatedRep.HStructure()
        PrintMatrix("   representation:", self.rep)
    
    
            
class OrbitalsDefinition(SelectedOrbitals):
    
    def EnumerateLAPWOrbitals(self):
        description = []
        curr = None
        byAtomAndN = OrderedDict()
        Js = {}
        for iorb in range(self.lapw.num_muffin_orbs):
            # here m=0 is temporary, atom numbers in the cell start at 0
            curr = LAPWRec(iorb, self.lapw.atom_number[iorb]-1, self.lapw.n[iorb], 
                           self.lapw.l[iorb], m=0, kind=self.lapw.kind[iorb],
                           correlated=self.lapw.correlated[iorb])
            
            # kind:
            #   0 - is the LOC orbitals
            #   1 - is the normal orbital
            #   2 - is the energy derivative
      
            if self.nrel == 1:
                if curr.key() not in byAtomAndN:
                    byAtomAndN[curr.key()] =  -self.lapw.l[iorb]
                else:
                    byAtomAndN[curr.key()] +=1
            else:
                # relativistic case
                if curr.key() not in byAtomAndN:
                    byAtomAndN[curr.key()] =  -self.lapw.l[iorb]+0.5
                elif byAtomAndN[curr.key()] == self.lapw.l[iorb]-0.5 and curr.key() not in Js:
                    Js[curr.key()] = True
                    byAtomAndN[curr.key()] =  -self.lapw.l[iorb]-0.5
                else:
                    byAtomAndN[curr.key()] +=1.0
                    
            # this will show the correct m label for each orbital in the basis:         
            #  print iorb, byAtomAndN[curr.key()]
            
            curr.m = byAtomAndN[curr.key()]  
            description.append(curr)
        return description
            
        
    def __init__(self, opts):
        SelectedOrbitals.__init__(self)
        self.opts = opts
        orbitals = opts.orbitals
        basis = opts.basis
        approx_rep = opts.approx_rep
        moment=opts.moment
        symmetry_breaking=opts.symmetry_breaking

        self.subspace_expr = opts.proj_expr
                           
        self.plugin = DFTPlugin.Instance()
        self.dft = self.plugin.GetDFTStuff()
        self.nrel = self.dft.nrel
        self.lapw = self.plugin.GetLAPWBasis()
    
        self.species = [int(self.dft.st.Z[idx-1]) for idx in self.dft.st.distinct_number] # for each atom, its Z
        # obtain a pymatgen structure from a Kutepov structure
        st = self.plugin.GetStructure()
        #TODO: we need to add magnetic data if it exists    
        afm = None
        afm = GroupAnalyzerWithMagmom.RecoverMagneticMoments(st)
        
        siteAFM = {}  
        sga = None  # it will be reconstructed in the local symmetry analyzer
        if afm is not None:
            mga = GroupAnalyzerWithMagmom(st, angle_tolerance=opts.spacegroup_angle_tolerance)
            sga = mga.get_sga()
            st = mga.get_primitive_standard_structure()
            
            for i in range(afm.num_afm):
                afms = afm.afm_symm[i]
                siteAFM[afms.site] = afms
        print(" - read the following structure from ini.h5:")
        print(st)
            
            
        sa = ShellAnalyzer(self.species, self.dft.st.distinct_number, orbitals)    
        def FirstInEqClass(atom_number): # this atom number is relative to 0
            return sa.first_in_eqclass[atom_number]

        requestedSubshells = sa.MatchUserInputToShells()
        print(" - input:", requestedSubshells)        
        
        # this is the full list of the orbitals in DFT
        lapwOrbs = self.EnumerateLAPWOrbitals()
        #print [str(o) for o in lapwOrbs]
        
        self.num_muffin_orbs = self.lapw.num_muffin_orbs
        
        def IsIgnored(orb):     # for higher l's (d,f and on) we require the orbital is marked correlated
            # we never project on the derivative function
            if orb.kind == 2:
                return True
            if orb.l >=2 and orb.correlated == 0:
                return opts.loc_projector
            else:
                return not opts.loc_projector
            
            
        
        # this is a map of each requested subshell to the LAPW orbital indices (ordered from m=-l to l)
        self.uniqueShells = {}
        self.total_dim = 0
        self.num_shells = 0
        for orb in lapwOrbs:
            if IsIgnored(orb):
                continue
            
            orbKey = (FirstInEqClass(orb.atom_number), orb.n, orb.l)
            # if this is equivalent to a shell that is requested 
            if orbKey in requestedSubshells:
                print(" \torbital found ", orb)
                if orbKey in self.uniqueShells:
                    shell = self.uniqueShells[orbKey]
                else:
                    shell = SymmetrizedShell(self.plugin, orb)
                    self.uniqueShells[orbKey] = shell
                    
                shell.AddEquivalentAtom(orb)
                self.total_dim += 1
            
 
        # now add all the AFM (anti-equivalent) atoms
        for orb in lapwOrbs:
            if IsIgnored(orb):
                continue
                        # if this is an AFM site
            if orb.atom_number in siteAFM and not self.opts.force_mi:
                afms = siteAFM[orb.atom_number]
                
                orbKey = (FirstInEqClass(afms.opposite_site), orb.n, orb.l)
                # if the opposite shell was already added
                if orbKey in self.uniqueShells: 
                    # Add AFM (anti-equivalent) orbital in this symmetrized shell
                    shell = self.uniqueShells[orbKey]
                    print(" \tAFM orbital found ", orb)
                    shell.AddAntiEquivalentAtom(orb, afms)
                    self.total_dim += 1

        # now define the orbital matrix  
        self.num_shells = len(list(self.uniqueShells.keys()))

        self.allocate()
        print(" - total dimension of the image: %d" % self.total_dim)
        
        st_sym = [None,None,None,None] # for l=0,1,2,3 only
        
        # Now add up the AtomicShell() objects of the output
        index = 0
        shell = 0
        for _key, orbs in self.uniqueShells.items():
            cluster_size = len(orbs.equiv_atoms) if opts.cluster else 1

            orbs.FinishPendingRotations()  # only here the actual l for AFM is known
                
            assert(orbs.l<4)
            if st_sym[orbs.l] is None:
                print(" - Analyzing local symmetry")
                st_sym[orbs.l] = StructLocalSymmetryAnalyzer(st, l=orbs.l, num_si=self.dft.num_si,
                                    nrel=self.dft.nrel, species="",
                                    moment=moment,
                                    symmetry_breaking=symmetry_breaking,
                                    r=opts.r,
                                    basis=basis, sga=sga, opts=opts)
                self.neighbors = st_sym[orbs.l].neighbors
                self.sites = st_sym[orbs.l].sites
            
            orbs.SetRotMatrixAndRep(st_sym[orbs.l], approx_rep, cluster_size)  # this is the rotation matrix to the symmetry adapted basis
        
        for _key, orbs in self.uniqueShells.items():
            # this loop handles both equivalent and anti-equivalent (AFM) atoms
            # the AFM atoms are always LAST. We count them, so we can substract the number from the total.

            if opts.cluster:
                start_index = index
                d = orbs.dim*cluster_size
                mo = np.average([self.lapw.mo_overlap[i0,i0] for i0 in orbs.lapw_indices])
                self.V[orbs.lapw_indices, index:index+d] = orbs.subM / sqrt(mo)
                #testing...
                a = np.real(orbs.subM)
                if orbs.dim == 5:
                    t2gi = np.array([0,1,3])
                    egi = np.array([2,4])
                    for j in range(20):
                        t2g = 0
                        eg = 0
                        for i in range(4):
                            t2g += np.sum(np.abs(a[j,i*5+t2gi]))
                            eg += np.sum(np.abs(a[j,i*5+egi]))
                        print(f"basis function {j}| t2g: {t2g}, eg:{eg}")
                index += d

            else:
                start_index = index
                for j, _atom in enumerate(orbs.equiv_atoms):
                    d = orbs.dim
                    # orbs.rotation[j] is the rotation of the basis for the equivalent atom (using g)
                    afm = ""
                    if j >= (len(orbs.equiv_atoms) - orbs.num_anti_equivalents): afm = "AFM"
                    PrintMatrix("  %s equivalence rotation (3D) %d:"%(afm,j), orbs.rot3[j])
                    PrintMatrix("  %s equivalence rotation rep %d:"%(afm,j), orbs.rotation[j])
                    i0 = orbs.lapw_indices[j*d]
                    # note orbs.subM, set by SetRotMatrixAndRep also changes to the preferred basis (all orb.rotation matrices are in native basis)
                    # this is the only place in the code where it should be used, other than in the computation of the 
                    # representation
                    self.V[orbs.lapw_indices[j*d:(j+1)*d], index:index+d] = \
                        orbs.rotation[j] * orbs.subM / sqrt(self.lapw.mo_overlap[i0,i0])
                    index += d

            
            if opts.cluster:
                dim = orbs.dim*cluster_size
                num_equivalents = 1
            else:
                dim = orbs.dim
                num_equivalents = len(orbs.equiv_atoms)-orbs.num_anti_equivalents

            ashell = AtomicShell(
                        atom_number = orbs.equiv_atoms[0], 
                        l=orbs.l,
                        dim=dim,
                        nrel=self.dft.nrel,
                        lapw_kind=orbs.kind, 
                        num_equivalents=num_equivalents,
                        num_anti_equivalents=orbs.num_anti_equivalents,
                        start_index=start_index,
                        end_index=index,
                        cluster_size = cluster_size,
                        is_correlated=False)  # TODO: fill is_correlated
            ashell.allocate()
            #print "*** ashell.l=", ashell.l, ashell.rep.shape
            ashell.rep[:,:] = orbs.rep[:,:]
            ashell.raw_rep[:,:] = orbs.raw_rep[:,:]
            ashell.lapw_indices[:] = orbs.lapw_indices[:]
            ashell.det_signs = [det(rot)  for rot in  orbs.rot3]
            #ashell.dim = orbs.dim
            ashell.basis[:,:] = orbs.basisChangeD[:,:]
            ashell.labels = orbs.labels
            for i in range(ashell.num_equivalents+ashell.num_anti_equivalents):
                ashell.rotations[i,:,:] = orbs.rotation[i][:,:]
            ashell.translations = orbs.translation
            self.shells[shell] = ashell

            ashell.subM = orbs.subM
            ashell.equiv_atoms = orbs.equiv_atoms

            shell += 1
            #self.VisualizeOrbitals(orbs.subM, shell)

    def VisualizeOrbitals(self,subM, shell_index : int, n = 101, iso = 0.1):

        shell = self.shells[shell_index]
        l = shell.l
        dim = 2*l+1

        loc = "./flapw_basis.tmp.h5:/"
        basis = MuffinTinBasis()
        basis.GetMtBasis(loc)
        basis.load(loc)
        isort = self.dft.st.distinct_number[shell.atom_number] -1
        tin = basis.tins[isort]
        ibasis = tin.lapw2self.data[shell.lapw_indices[0]]
        def interpolate_radial_function(rs):
            return np.interp(rs, tin.radial_mesh*0.529177, tin.radial_functions[ibasis, :], right=0, left=0)
        
        coords = [site.coords for site in self.sites]
        center = np.average( np.array(coords), axis = 0 )
        coords = np.array([(c-center) for c in coords])

        s=2
        xyz = [np.linspace(np.min(coords[:,i]) - s, np.max(coords[:,i])+s, n, endpoint = True) for i in range(3)]
        x, y, z = np.meshgrid(xyz[0], xyz[1], xyz[2])

        rshmat = GetRealSHMatrix(l)
        def get_orb(x,y,z):
            r = np.sqrt(x**2+y**2+z**2)
            azimuth = np.arctan2(y, x)
            polar = np.arctan2(r, z)
        
            ylm = np.array([sph_harm(m,l,azimuth,polar) for m in range(-l,l+1,1)])
            rsh = np.tensordot(rshmat,ylm,axes=(1,0))
            return interpolate_radial_function(r)*rsh

        from skimage import measure #conda install sci-kitimage

        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D

        for i in range(shell.dim):

            fig = plt.figure()
            ax = fig.add_subplot(111,projection='3d')
            summed_orb = np.zeros(x.shape, dtype=np.complex)
            for iat, c in enumerate(coords):
                orb = get_orb(x-c[0], y-c[1], z-c[2])
                for m in range(dim):
                    summed_orb += orb[m] * shell.basis[i,m+dim*iat]

            
            verts, faces, _, _ = measure.marching_cubes(np.real(summed_orb), iso, spacing=(0.1, 0.1, 0.1))
            ax.plot_trisurf(verts[:, 0], verts[:,1], faces, verts[:, 2],
                color='red', lw=1, alpha=0.3)

            verts, faces, _, _ = measure.marching_cubes(np.real(summed_orb), -iso, spacing=(0.1, 0.1, 0.1))
            ax.plot_trisurf(verts[:, 0], verts[:,1], faces, verts[:, 2],
                color='blue', lw=1, alpha=0.3)

            ax.set_title(f"func {i}")
    
            plt.show()
    
