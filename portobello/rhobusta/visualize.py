#!/usr/bin/env python3

'''
Created on Sep 8, 2019

@author: adler
'''

from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer
import sys
from pymatgen.vis.structure_vtk import StructureVis
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.options import ArgumentParserThatStoresArgv

if __name__ == '__main__':
    
    
    parser = ArgumentParserThatStoresArgv(parents=[GetMPIProxyOptionsParser()])
    parser.add_argument("-P", dest="start_from_primitive",
                      action="store_true",
                      default=False,
                      help="Add -N if the primitive cell should not be the starting point (stick to the input)") 
            
    opts = parser.parse_args(sys.argv[1:])

    vis = StructureVis(show_bonds=False, show_polyhedron=False, show_unit_cell=True)
      
    
    mpi=MPIContainer(opts)
    st = DFTPlugin.GetStructureFromIni()
    
    
    sga = SpacegroupAnalyzer(st, symprec=0.001, angle_tolerance=5)
    if opts.start_from_primitive:
        #struct = sga.get_primitive_standard_structure()
        #struct = sga.find_primitive()
        #struct = sga.get_conventional_standard_structure()
        st = sga.get_refined_structure()
    
    vis.set_structure(st, True)
    vis.show()