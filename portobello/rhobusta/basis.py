#!/usr/bin/env python3

from random import random
import numpy as np
import scipy.interpolate as interp
from scipy.special import sph_harm

from portobello.generated.FlapwMBPT_basis import MuffinTinBasis, MuffinTin, InterstitialBasis
from portobello.symmetry.local import GetCGMatrix, GetRealSHMatrix

Ry2eV = 27.2107*0.5  # from Kutepov, careful when changing
BohrToAng = 1./1.88973

def xyz_to_spherical(xyz):
    ptsnew = np.empty(xyz.shape)
    xy = xyz[:,0]**2 + xyz[:,1]**2
    ptsnew[:,0] = np.sqrt(xy + xyz[:,2]**2)
    ptsnew[:,1] = np.arctan2(xyz[:,1], xyz[:,0])
    ptsnew[:,2] = np.arctan2(np.sqrt(xy), xyz[:,2]) # for elevation angle defined from Z-axis down
    #ptsnew[:,2] = np.arctan2(xyz[:,2], np.sqrt(xy)) # for elevation angle defined from XY-plane up
    return ptsnew

class OrbitalEvaluatorContainer:
    """
    The purpose of this class is to facilitate the evaluation of quantities in atomic shells within the MT
    """
    def __init__(self, dft, lapw, prepare_for_spherical_coordinates = True):
        self.dft = dft
        self.lapw = lapw
        self.prepare_for_spherical_coordinates = prepare_for_spherical_coordinates
        
        loc = "./flapw_basis.core.h5:/"
        self.basis = MuffinTinBasis()
        self.basis.GetMtBasis(loc)
        self.basis.load(loc)

        self.is_relativistic = dft.nrel == 2

        self.orbital_evaluators = {}
        self.last_iorb = -1

        self.max_n = np.zeros(self.basis.nsort, dtype=int) - 1

    def emplace(self, iorb, spherical_coords):
        assert(self.last_iorb < iorb)
        self.last_iorb = iorb

        iatom = self.lapw.atom_number[iorb] - 1
        isort = self.dft.st.distinct_number[iatom] - 1
        shell = self.basis.tins[isort]

        # We don't know what the (non-l) quantum numbers are for a given orbital, but we do know the pattern they follow within a shell
        # Here we increment the orbitals' quantum number until we get to the next one -- then we are set to use the orbital
        quantum_numbers = OrbitalEvaluator.get_quantum_number(self.lapw.l[iorb], self.is_relativistic)
        descr = f'{self.lapw.atom_number[iorb]}.{self.lapw.n[iorb]}.{self.lapw.kind[iorb]}.{quantum_numbers.major_descriptor()}'
        
        if descr in self.orbital_evaluators.keys():
            self.orbital_evaluators[descr].quantum_numbers.increment()
            self.orbital_evaluators[descr].lapw_indices.append(iorb)
            is_new = False
        else:
            self.orbital_evaluators[descr] = OrbitalEvaluator(shell, iorb, self.lapw.l[iorb], self.is_relativistic, self.prepare_for_spherical_coordinates)
            self.orbital_evaluators[descr].begin_realspace_evaluation(spherical_coords)
            is_new = True

            self.max_n[isort]=max(self.max_n[isort], self.lapw.n[iorb])

        self.current_description = descr
        self.current_evaluator = self.orbital_evaluators[descr]
        self.current_qn = self.orbital_evaluators[descr].quantum_numbers
        return is_new

    def shells(self):
        return self.orbital_evaluators.keys()

    def get_current_description(self):
        return self.current_description

    def get_wavefunction(self):
        return self.current_evaluator.finish_realspace_evaluation()

    def get_MT_radius(self):
        return self.current_evaluator.radial_mesh[-1]

class OrbitalEvaluator:
    @classmethod
    def get_quantum_number(cls, l : int, is_relativistic : bool):
        return cls.RelativisticNumbers(l) if is_relativistic else cls.NonRelativisticNumbers(l)

    def __init__(self, shell : MuffinTin, iorb : int, l : int, is_relativistic : bool, prepare_for_spherical_coordinates = True):
        
        self.is_relativistic = is_relativistic
        self.eval_at_spherical_coordinates = prepare_for_spherical_coordinates
        self.lapw_indices = [iorb]

        ibasis = shell.lapw2self.data[iorb]

        self.quantum_numbers = self.get_quantum_number(l, is_relativistic)

        self.radial_mesh = shell.radial_mesh * BohrToAng

        self.radial_function = self.radial_interpolator(shell.radial_functions[ibasis,:]) if prepare_for_spherical_coordinates else shell.radial_functions[ibasis,:]
        if is_relativistic:
            self.f_radial_function = self.radial_interpolator(shell.radial_functions2[ibasis,:]) if prepare_for_spherical_coordinates else shell.radial_functions2[ibasis,:]

        self.nrel = 2 if is_relativistic else 1

        self.begun_realspace_evaluation = False

        
    def begin_realspace_evaluation(self, sph_coord : np.array):
        if self.eval_at_spherical_coordinates:
            self.evaluate_rad(sph_coord)
            self.evaluate_sph(sph_coord)
        self.begun_realspace_evaluation = True

    def evaluate_rad(self, sph_coord):
        self.radial_function = np.array(self.radial_function(sph_coord[:,0]))
        if self.is_relativistic:
            self.f_radial_function = 1j*np.array(self.f_radial_function(sph_coord[:,0]))/274.074

    def evaluate_sph(self, sph_coord):
        if self.quantum_numbers.is_relativistic:
            self.basis = GetCGMatrix(self.quantum_numbers.l)
        else:
            self.basis = GetRealSHMatrix(self.quantum_numbers.l)

        dim = self.basis.shape[0]

        tmp_quantum_numbers = self.get_quantum_number(self.quantum_numbers.l, self.quantum_numbers.is_relativistic)

        if self.quantum_numbers.is_relativistic:
            self.sph_harmonics_bispinor=np.zeros((4,dim,sph_coord.shape[0]), dtype=np.complex )
        else:
            self.sph_harmonics=np.zeros((dim,sph_coord.shape[0]), dtype=np.complex )

        for i in range(dim):
            if self.quantum_numbers.is_relativistic:

                j = tmp_quantum_numbers.j
                mu = tmp_quantum_numbers.mu
                k = tmp_quantum_numbers.kappa
                    
                if k < 0:
                    cg_up = np.sqrt(j+mu)/np.sqrt(2*j)
                    cg_dn = np.sqrt(j-mu)/np.sqrt(2*j)
                elif k > 0:
                    cg_up = -np.sqrt(j+1-mu)/np.sqrt(2*j+1)
                    cg_dn =  np.sqrt(j+1+mu)/np.sqrt(2*j+1)
                else:
                    cg_dn = 1
                    cg_up = 1

                l,m1,m2 = self.quantum_numbers.get_lmm(is_f = False)
                if abs(m1) <= l:
                    self.sph_harmonics_bispinor[0,i,:] = cg_dn*(sph_harm(m1, l,  sph_coord[:,1],  sph_coord[:,2])) #G_dn
                if abs(m2) <= l:
                    self.sph_harmonics_bispinor[1,i,:] = cg_up*(sph_harm(m2, l,  sph_coord[:,1],  sph_coord[:,2])) #G_up

                l,m1,m2 = self.quantum_numbers.get_lmm(is_f = True)
                if abs(m1) < l:
                    self.sph_harmonics_bispinor[2,i,:] = cg_dn*(sph_harm(m1, l, sph_coord[:,1],  sph_coord[:,2])) #F_dn
                if abs(m2) < l:
                    self.sph_harmonics_bispinor[3,i,:] = cg_up*(sph_harm(m2, l, sph_coord[:,1],  sph_coord[:,2])) #F_up
                
            else:
                self.sph_harmonics[i,:] = sph_harm(tmp_quantum_numbers.m, tmp_quantum_numbers.l, sph_coord[:,1], sph_coord[:,2])

            if i+1 < dim: # don't increment on the last iteration
                tmp_quantum_numbers.increment()

    def finish_realspace_evaluation(self):
        assert(self.begun_realspace_evaluation) # Must call begin_realspace_evaluation() first

        ang = np.zeros((self.nrel ** 2,self.radial_function.shape[0]), dtype=np.complex)
        if self.quantum_numbers.is_relativistic:

            ang[0,:] = self.radial_function * self.sph_harmonics_bispinor[0,self.quantum_numbers.index,:]
            ang[1,:] = self.radial_function * self.sph_harmonics_bispinor[1,self.quantum_numbers.index,:]

            ang[2,:] = self.f_radial_function * self.sph_harmonics_bispinor[2,self.quantum_numbers.index,:]
            ang[3,:] = self.f_radial_function * self.sph_harmonics_bispinor[3,self.quantum_numbers.index,:]
            
            return ang

        else:
            basis_vector = np.array(self.basis)[self.quantum_numbers.index,:]
            ang[0,:] = np.tensordot( basis_vector, self.sph_harmonics[:,:], axes=(0,0) )
            return ang*self.radial_function

    #internal
    def radial_interpolator(self, radial_function):
        return interp.interp1d(self.radial_mesh, radial_function, kind = "cubic", bounds_error = False, fill_value = 0)
    
    class NonRelativisticNumbers:
        is_relativistic = False
        
        def __init__(self, l):
            self.l = l
            self.m = -l
            
            self.index = 0
            
        def increment(self):
            self.m += 1
            assert (self.m <= self.l)
            
            self.index += 1
            return self
            
        def major_descriptor(self):
            return self.l
            
        def indices(self):
            return self.index
          
    class RelativisticNumbers:
        is_relativistic = True
             
        class F_component:
            def __init__(self, l, kappa):
                self.kappa = -kappa
                self.l = l

                if kappa < 0:
                    self.lbar = l+1
                    self.j = self.lbar + 0.5
                else:
                    self.lbar = l-1
                    self.j = self.lbar - 0.5

                if self.lbar == 0:
                    self.j = l + 0.5

                self.mu = -self.j

                self.name = random()
            
            def increment(self):
                self.mu += 1
                if self.mu > self.j:
                    assert(self.kappa < 0)
                    self.kappa = self.l
                    self.lbar = self.l + 1

                    self.j = self.lbar + 0.5
                    self.mu = -self.j
            
                return self

        def __init__(self, l):
            self.l = l

            if l == 0:
                self.j = l + 0.5
                self.kappa = -self.l-1
            else:
                self.j = l - 0.5
                self.kappa = self.l

            self.mu = -self.j
            
            self.index = 0

            self.f_numbers = self.F_component(self.l, self.kappa)

        def increment(self):
            self.index += 1
            self.mu += 1

            if self.mu > self.j:
                assert(self.kappa > 0)
                self.kappa = -self.l - 1
                self.j = self.l + 0.5
                self.mu = -self.j
            
            self.f_numbers.increment()

            return self
            
        def major_descriptor(self):
            return self.kappa
            
        def indices(self):
            return  self.index
            
        # l, spin dwn m, spin up m
        def get_lmm(self, is_f = False):
            if not is_f:
                return self.l, np.round(self.mu+0.5), np.round(self.mu-0.5)
            else:
                return self.f_numbers.lbar, np.round(self.f_numbers.mu+0.5), np.round(self.f_numbers.mu-0.5)

