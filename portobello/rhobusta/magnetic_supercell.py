#!/usr/bin/env python3


'''
Created on Aug 21, 2019

@author: adler
'''

# remove benign warnings from numpy and pymatgen
from collections import OrderedDict
from pymatgen.io.cif import CifBlock, CifFile
from monty.io import zopen
from pymatgen.core.structure import Structure
from pymatgen.vis.structure_vtk import StructureVis

from pymatgen.electronic_structure.core import Magmom
from portobello.rhobusta.InputBuilder import GetStructure
from optparse import OptionParser
import sys

from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from pymatgen.core import periodic_table
import numpy as np
    
    
    
class CifWriter(object):
    def __init__(self, struct, sga, write_magmoms=False):
        """
        A wrapper around CifFile to write CIF files from pymatgen structures.

        Args:
            struct (Structure): structure to write
            symprec (float): If not none, finds the symmetry of the structure
                and writes the cif with symmetry information. Passes symprec
                to the SpacegroupAnalyzer
            write_magmoms (bool): If True, will write magCIF file. Incompatible
                with symprec
        """

        format_str = "{:.8f}"

        block = OrderedDict()
        loops = []
    
        #struct = sga.get_refined_structure()
                          
        latt = struct.lattice
        comp = struct.composition
        no_oxi_comp = comp.element_composition
        block["_symmetry_space_group_name_H-M"] = sga.get_space_group_symbol()
        for cell_attr in ['a', 'b', 'c']:
            block["_cell_length_" + cell_attr] = format_str.format(
                getattr(latt, cell_attr))
        for cell_attr in ['alpha', 'beta', 'gamma']:
            block["_cell_angle_" + cell_attr] = format_str.format(
                getattr(latt, cell_attr))
        block["_symmetry_Int_Tables_number"] = sga.get_space_group_number()
        block["_chemical_formula_structural"] = no_oxi_comp.reduced_formula
        block["_chemical_formula_sum"] = no_oxi_comp.formula
        block["_cell_volume"] = "%.8f" % latt.volume

        _reduced_comp, fu = no_oxi_comp.get_reduced_composition_and_factor()
        block["_cell_formula_units_Z"] = str(int(fu))

        #sf = sga

        symmops = sga.get_symmetry_operations()
        #print symmops
        #for op in sf.get_symmetry_operations():
        #    v = op.translation_vector
        #    symmops.append(SymmOp.from_rotation_and_translation(
        #        op.rotation_matrix, v))

        ops = [op.as_xyz_string() for op in symmops]
        block["_symmetry_equiv_pos_site_id"] = \
            ["%d" % i for i in range(1, len(ops) + 1)]
        block["_symmetry_equiv_pos_as_xyz"] = ops

        loops.append(["_symmetry_equiv_pos_site_id",
                      "_symmetry_equiv_pos_as_xyz"])

        try:
            symbol_to_oxinum = OrderedDict([
                (el.__str__(),
                 float(el.oxi_state))
                for el in sorted(comp.elements)])
            block["_atom_type_symbol"] = list(symbol_to_oxinum.keys())
            block["_atom_type_oxidation_number"] = list(symbol_to_oxinum.values())
            loops.append(["_atom_type_symbol", "_atom_type_oxidation_number"])
        except (TypeError, AttributeError):
            symbol_to_oxinum = OrderedDict([(el.symbol, 0) for el in
                                            sorted(comp.elements)])

        atom_site_type_symbol = []
        atom_site_symmetry_multiplicity = []
        atom_site_fract_x = []
        atom_site_fract_y = []
        atom_site_fract_z = []
        atom_site_label = []
        atom_site_occupancy = []
        atom_site_moment_label = []
        atom_site_moment_crystalaxis_x = []
        atom_site_moment_crystalaxis_y = []
        atom_site_moment_crystalaxis_z = []
        count = 1
        for site in struct:
            for sp, occu in sorted(site.species.items()):
                atom_site_type_symbol.append(sp.__str__())
                atom_site_symmetry_multiplicity.append("1")
                atom_site_fract_x.append("{0:f}".format(site.a))
                atom_site_fract_y.append("{0:f}".format(site.b))
                atom_site_fract_z.append("{0:f}".format(site.c))
                atom_site_label.append("{}{}".format(sp.symbol, count))
                atom_site_occupancy.append(occu.__str__())

                magmom = Magmom(
                    site.properties.get('magmom', getattr(sp, 'spin', 0)))
                if write_magmoms and abs(magmom) > 0:
                    moment = Magmom.get_moment_relative_to_crystal_axes(
                        magmom, latt)
                    atom_site_moment_label.append(
                        "{}{}".format(sp.symbol, count))
                    atom_site_moment_crystalaxis_x.append("%.5f" % moment[0])
                    atom_site_moment_crystalaxis_y.append("%.5f" % moment[1])
                    atom_site_moment_crystalaxis_z.append("%.5f" % moment[2])

                count += 1

        block["_atom_site_type_symbol"] = atom_site_type_symbol
        block["_atom_site_label"] = atom_site_label
        block["_atom_site_symmetry_multiplicity"] = \
            atom_site_symmetry_multiplicity
        block["_atom_site_fract_x"] = atom_site_fract_x
        block["_atom_site_fract_y"] = atom_site_fract_y
        block["_atom_site_fract_z"] = atom_site_fract_z
        block["_atom_site_occupancy"] = atom_site_occupancy
        loops.append(["_atom_site_type_symbol",
                      "_atom_site_label",
                      "_atom_site_symmetry_multiplicity",
                      "_atom_site_fract_x",
                      "_atom_site_fract_y",
                      "_atom_site_fract_z",
                      "_atom_site_occupancy"])
        if write_magmoms:
            block["_atom_site_moment_label"] = atom_site_moment_label
            block[
                "_atom_site_moment_crystalaxis_x"] = atom_site_moment_crystalaxis_x
            block[
                "_atom_site_moment_crystalaxis_y"] = atom_site_moment_crystalaxis_y
            block[
                "_atom_site_moment_crystalaxis_z"] = atom_site_moment_crystalaxis_z
            loops.append(["_atom_site_moment_label",
                          "_atom_site_moment_crystalaxis_x",
                          "_atom_site_moment_crystalaxis_y",
                          "_atom_site_moment_crystalaxis_z"])
        d = OrderedDict()
        d[comp.reduced_formula] = CifBlock(block, loops, comp.reduced_formula)
        self._cf = CifFile(d)

    def __str__(self):
        """
        Returns the cif as a string.
        """
        return self._cf.__str__()

    def write_file(self, filename):
        """
        Write the cif file.
        """
        with zopen(filename, "wt") as f:
            f.write(self.__str__())

    
    
# this is a simple supercell procedure, to make the given structure a magnetic
# structure with checkered AFM magnetic moments (the simplest possible AFM extension)
# where moments are set on the sublattice defined by element el
def ATypeAFM(st, el, miller, mult, vis, pol0=0.5):
    elements = set()
    
    for i, site in enumerate(st.sites):
        elements.add(site.specie)
    
    # we temporarily substitute the spin down elements with an element that is not in the 
    # crystal because of a bug in pymatgen: it thinks that different magmom's are equivalent
    # in terms of symmetry
    # TODO: this is true 2019 for python2, check what happens when we migrate to python3
    SUBEL = None
    for i in range(4, 80):
        el2 = periodic_table.Element.from_Z(i)
        if el2 not in elements:
            SUBEL = el2
            break
        
    assert(SUBEL is not None)  # crystal had better not have tens of elements 
    
    print("Substitute:", SUBEL)
    
    sl = []
    actualSpecie = None
    for i, site in enumerate(st.sites):
            specie = site.specie
            if site.species_string.startswith(el):
                actualSpecie = specie
                fc = site.frac_coords
                Y =  mult*np.dot(miller, fc)
                print(Y)
                if round(Y) % 2 == 0:
                    specie = SUBEL
                    print("- making site %i at height %f <0." % (i,Y))
                else:
                    print("- making site %i at height %f >0." % (i,Y))
            sl.append(specie)
            
    #print len(sl)
    #print len(st.frac_coords)
    #print sl

    st2 = Structure(st.lattice, sl, st.frac_coords, coords_are_cartesian=False)            
    sga = SpacegroupAnalyzer(st2, symprec=0.001, angle_tolerance=5)
    
    #vis.set_structure(st2, True)
    #vis.show()
   
    # find the primitive structure of this AFM
    st2 = sga.get_primitive_standard_structure()
    print(st2)
    
    # obtain the group before we substitute back
    sga = SpacegroupAnalyzer(st2, symprec=0.001, angle_tolerance=5)
    
    # substitute back, and add a magnetic moment hint for the projector
    for i, site in enumerate(st2.sites):
        if site.specie == SUBEL:
            st2.replace(i, actualSpecie, site.frac_coords, 
                       coords_are_cartesian=False, 
                       # these are also the flipped magnetic moments
                       properties={'magmom': Magmom(-1.0*pol0)})
        elif site.specie ==  actualSpecie:
            st2.replace(i, actualSpecie, site.frac_coords, 
                       coords_are_cartesian=False, 
                       # these are also the flipped magnetic moments
                       properties={'magmom': Magmom(pol0)})
    
    #also return the operation that maps the AFM atoms to each other
    
    return st2, sga

        
def main():
    
    parser = OptionParser()    
    
    # example uses

    #MnO
    #miller = (1,1,-1) 
    #m = 2.0
    
    #V2O3:
    #miller = (1, 1,0)
    #m = 6.0
    

    parser.add_option("-A", "--Atype", dest="atype", 
                      help="""Calculate A-type AFM ordering. 
                              The parameter is a miller index of the plane of A-type spin polarization.
                             """,
                      default=None)
    
    parser.add_option("-m", "--multiplier", dest="mult",
                      type=int, default=2)
    
    parser.add_option("-s", "--species", dest="specie", 
                      default=None)  
    

    parser.add_option("-N", dest="start_from_primitive",
                      action="store_false",
                      default=True,
                      help="Add -N if the primitive cell should not be the starting point (stick to the input)") 
            

    (opts, args) = parser.parse_args(sys.argv[1:])
    
    if opts.specie is None:
        print("use -s to specifie an element")
        exit(1)

    #vis = StructureVis(show_bonds=False, show_polyhedron=False, show_unit_cell=True)
      
    struct = GetStructure(args[0])
    sga = SpacegroupAnalyzer(struct, symprec=0.001, angle_tolerance=5)
    if opts.start_from_primitive:
        struct = sga.get_primitive_standard_structure()
    
   
    #vis.set_structure(struct, True)
    #vis.show()
   
    if opts.atype is not None:
        miller = [int(x) for x in opts.atype.split(",")]
        
        
        st, sga = ATypeAFM(struct*2, opts.specie, miller, opts.mult, None)
        base, *_suffix = args[0].split('.')
        
        print(st)
        print("- writing file "+base + "-AFM.mcif")
        cw = CifWriter(st, sga=sga, write_magmoms=True)
        cw.write_file(base+ "-AFM.mcif")
    
        #TODO: Test that the structure makes sense:
        #1. it has equal number of up/down
        #2.  that when dropping magnetic structure, it is isomorphic to the original structure
    
    
if __name__ == '__main__':
    main()
    

