
#!/usr/bin/env python3

'''
Created on March 15, 2021

@author: adler@physics.rutgers.edu
'''

from pathlib import Path
from typing import Set
from portobello.bus.DIIS import DIIS
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.rhobusta.multi_impurity import MultiImpurityEmbedder
import sys
from portobello.generated.gutz import GSolution, GState, GProblem
from portobello.generated.edx import EDSolverState
from portobello.rhobusta.PEScfEngine import PEScfEngine
from scipy import optimize
import numpy as np
from portobello.rhobusta.options import RefreshOptions
from portobello.rhobusta.orbitals import PrintMatrix
from portobello.bus.Matrix import Matrix
from scipy.linalg import block_diag
from numba import jit
from portobello.generated.PEScf import Histogram
from ed.ed_service import Diagonalize
from portobello.rhobusta.MatrixParameterization import MatrixParameterizer, Parameterizer
from itertools import product
from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer
from portobello.bus.fortran import Fortran
from portobello.bus.persistence import Persistent
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from argparse import ArgumentDefaultsHelpFormatter, Namespace
from portobello.bus.persistence import Store

@jit(nopython=True)
def power2(a):
    return 1 << a

@jit(nopython=True)
def countBits(a):
    ret = 0
    while a != 0:
        ret += a & 0x1
        a = a >> 1
    return ret

@jit(nopython=True)
def FockStatesNoSpinSymmetryWithRange(n, nrange):
    result = []
    for x in range(power2(n)):
        cx = countBits(x)
        if not cx in nrange:
            continue    
        for x2 in range(power2(n)):
            if countBits(x2) == n - cx:
                result.append((x2 << n) | x)
    result.sort()
    return result
    
#  
@jit(nopython=True)
def FockStatesNoSpinSymmetry(n : int):
    """
        only preserving: num in orbitals = num bath.
    """
    result = []
    for x in range(power2(n)):
        cx = countBits(x)  
        for x2 in range(power2(n)):
            if countBits(x2) == n - cx:
                result.append((x2 << n) | x)
    result.sort()
    return result
  
#  
@jit(nopython=True)
def FockStatesWithSpinSymmetry(n : int):
    """
        preserving: num in orbitals = num bath and total spin is 0
    """
    result = []

    bound = power2(n)
    odds = (bound-1)*4//3 + 1
    odds = odds & (bound-1)
    evens = ~odds & (bound-1)

    for x in range(bound):
        cx = countBits(x)  
        cxOdds = countBits(x & odds) 
        cxEvens = countBits(x & evens)
        for x2 in range(bound):
            if countBits(x2) == n - cx and countBits(x2 & evens) + cxEvens == countBits(x2 & odds) + cxOdds:
                result.append((x2 << n) | x)
    result.sort()
    return result


ex_entry_point = None

def DiagonalizerWorker(opts):
    mpi = MPIContainer(opts)
    from mpi4py import MPI # the fortran code actually requires MPI to be initialized
    command = MPI.COMM_WORLD.bcast("not-done", root=0)
    if command == "done":
        return False
    fortran = Fortran.Instance()
    global ex_entry_point
    if ex_entry_point is None:
        ex_entry_point = fortran.GetFunction("ed_rhobusta_driver.mp.run", check_gfort = True)
    Persistent.Refresh("./edx.tmp.h5:/input/") #refresh this file, it may have been written outside
    # note that at this point we can only refresh in Python (which does the work for the fortran code as well)
    ex_entry_point()
    mpi.Barrier()
    return True

# this is a transient adapter object used for convenience, the list of fields
# in __atributes and __datasets is inconsistent
class GutzEquationsInterface(GState, GProblem, PEScfEngine):
    def __init__(self, gs:GState, gp:GProblem, opts:Namespace) -> None:
        # merge all the fields from both
        kwargs = gs.__dict__
        for key in kwargs:
            if key in ['_datasets','_attributes', '_objects']:
                continue
            setattr(self, key, kwargs[key])
        kwargs = gp.__dict__
        for key in kwargs:
            if key in ['_datasets','_attributes', '_objects']:
                continue
            setattr(self, key, kwargs[key])
        self.Vdc = self.DC # just an alias
        orbsDef = SelectedOrbitals()
        orbsDef.load("./projector.h5:/def/")
        self.corrSubshell = orbsDef.shells[opts.which_shell] # for symmetrization
        PEScfEngine.InitSymmetrization(self)
        
    @property
    def ID1F(self):
        I = np.eye( self.num_bands, dtype=np.complex128)
        return I
             
    # create zero complex local matrix, which has spin as well
    def ZeroLocalMatrix(self, isComplex=True):
        t = np.float64
        if isComplex:
            t = np.complex128
        return np.zeros((self.dim, self.dim,  self.num_si), t)
        
    @property
    def ID1(self):
        I = np.eye(self.dim, dtype=np.complex128)
        return I
      
    def StoreSolution(self, hist):
        gsol = GSolution(dim=self.dim, num_si=self.num_si)
        gsol.allocate()
        for si in range(self.num_si):
            self.Z[:,:,si] = Matrix(self.R[:,:,si]).H*self.R[:,:,si]

        print("Gutz matrices:")
        print("--------------")
        print("R:")
        print(Matrix(self.R[:,:,0]))
        gsol.R[...] = self.R
        print("λ:")
        print(Matrix(self.Lambda[:,:,0]))
        gsol.Lambda[...] = self.Lambda
        print("Z:")
        print(Matrix(self.Z[:,:,0]))
        gsol.Z[...] = self.Z
        print("ρ:")
        print(Matrix(self.RhoImp[:,:,0]))
        gsol.RhoImp[...] = self.RhoImp
        print(f"Nf: {self.Nimp}")
        gsol.N = self.Nimp
        gsol.energyImp = self.energyImp
 
        gsol.hist=self.CompleteHistogramData(hist)
        gsol.success = self.success
        gsol.quality = self.quality
        gsol.store(self.gutz_name+"result/", flush=True)
        Store.Singleton().CloseFile(self.gutz_name) # so that we will overwrite it next time - in case this is one process
        

    
class GutzEquationsSolver:
    def __init__(self, gutz: GutzEquationsInterface, isReal : bool, opts):
        self.gutz = gutz
        self.opts = opts
        self.params = []
        
        self.λtmp = np.zeros_like(gutz.R)
        self.λMP = MatrixParameterizer(gutz, attr="λtmp", si=0, isHermitian=False, obj=self, isReal=isReal)
        
        self.RλParams = Parameterizer(gutz)
        self.RλParams.AddSpinfulMatrix(gutz, attr="R",      isHermitian=False)
        self.RλParams.AddSpinfulMatrix(gutz, attr="Lambda", isHermitian=not opts.gutz_with_lifetime, isReal=isReal)
        
        self.results = []
        self.F1 = np.zeros_like(gutz.R)
        self.F2 = np.zeros_like(gutz.Lambda)
        
        self.F1F2Params = Parameterizer(gutz)
        self.F1F2Params.AddSpinfulMatrix(self, attr="F1", isHermitian=False,)
        self.F1F2Params.AddSpinfulMatrix(self, attr="F2", isHermitian=not opts.gutz_with_lifetime, isReal=isReal)
        
        self.min = 1.0e10
        self.xMin = None
        self.num_calls = 0
        self.xfile = FileNameForShell(self.opts.which_shell, template="./x{shell}.ED.tmp.h5")

        self.mottCloseness = set()

    def BoundsVector(self, vmin : float, vmax : float, vmin_c : float, vmax_c : float):
        bounds = []
        for pz in self.params:
            if pz.attr == "R":
                bounds.extend(pz.BoundsVector(False, vmin,vmax,vmin_c,vmax_c))
            else:
                bounds.extend(pz.BoundsVector(True, -10.0, 10.0, -0.1, 0.1))
        return bounds

    def Fermi(self, M):
        try:
            diag, u = self.gutz.DiagonalizeHamiltonianNoSort(M)
        except np.linalg.LinAlgError as e:
            PrintMatrix("failed diagonalizing matrix:", M)
            PrintMatrix("Lambda:",self.gutz.Lambda[:,:,0])
            PrintMatrix("R:", self.gutz.R[:,:,0])
            raise
        uM = Matrix(u)
        return uM *  np.diag(self.gutz.ferm(diag)) * uM.H

    def MatrixFunction(self, M, function):
        try:
            dd, u = self.gutz.DiagonalizeHamiltonianNoSort(M)
        except np.linalg.LinAlgError:
            PrintMatrix("bad matrix",M)
            raise
            
        uM = Matrix(u)
        #M2 =  uM *  np.diag(dd) * uM.H
        #assert(np.allclose(M,M2))
        self.DebugPrint("uM", uM)
        self.DebugPrint("diagonal", np.diag(dd))
        self.DebugPrint("function on diagonal", np.diag(function(dd)))
        return uM *  np.diag(function(dd)) * uM.H

    def ProjectAndSymmetrize(self, MM):
        M =  Matrix(MM[-self.gutz.dim:, -self.gutz.dim:])
        self.gutz.SymmetrizeCorrelated(M, hermitian=True)
        return M

    def dΔ_dDs(self, Δ, D, R):
        diag, u = self.gutz.DiagonalizeHamiltonianNoSort(Δ)
        V = Matrix(u)  # V.H * Δ * V=diag
        VH = V.H
        S0 = (Matrix(R)*Matrix(D)).T
        S = VH*(S0 + np.conjugate(S0))*V
        self.DebugPrint("S matrix:",S)
        A = [np.sqrt(d*(1-d)) for d in diag]
        DRn = range(self.gutz.dim)
        for a,b in product(DRn, DRn):
            S[a,b] /= (A[a]+A[b])
        self.λtmp[...] = 0.0 # initialize work space for self.λMP
        x = []
        for s in range(self.λMP.repMax):
            Is = self.λMP.DerivativeByComplexParam(s+1)
            Ts = VH * (Is-Δ*Is-Is*Δ)*V
            x.append(np.trace(Ts*S) / np.sum(Is))
        M = Matrix(self.λMP.UnpackFromComplexVector(x)[:,:,0])
        self.DebugPrint("derivative expression:",M)
        return M

    def SolveEmbeddingWithEdX(self, D_ws, λc_ws, E1e_ws, Utensor, verbose, opts):   
        """ Note this is a 32-bit version of EDX, because all of the ints have been compiled as int32.
             This is enough for gutzwiller f-shell (28 bits needed, 31 are the max because of the sign bit)
        Args:
            D_ws (Matrix): D matrix with spin index
            E1e_ws (Matrix): E1e matrix with spin index
            Utensor (tensor): The interaction tensor
            verbose (int): verbosity level
            opts (Namespace): all options

        Returns:
            F1F2: vector of errors
        """
        gutz = self.gutz

        def ToEdxSpinFormat(M_ws):
            if gutz.nrel < 2 and gutz.num_si == 1:
                # nonrel spin symmetry
                zeros =np.zeros_like(M_ws[:,:,0])
                return np.block([[M_ws[:,:,0], zeros], 
                                [zeros, M_ws[:,:,0]]])
            elif gutz.num_si == 1:
                return M_ws[:,:,0] # Dirac case
            else:
                zeros =np.zeros_like(M_ws[:,:,0])
                return np.block([[M_ws[:,:,0], zeros], 
                                [zeros, M_ws[:,:,1]]])

        def FromEdxSpinToRhobusta(M):
            dim = M.shape[0] // 2
            assert(dim*2 == M.shape[0])
            if gutz.nrel < 2 and gutz.num_si == 1:
                M_ws = np.zeros((dim,dim,1), M.dtype)
                M_ws[:,:,0] = M[:dim,:dim] # here we seem to lose the information on the 2nd spin
                                           # - but it is symmetrical so we don't
            elif gutz.num_si == 1:
                M_ws = np.zeros((dim*2,dim*2,1), M.dtype)
                M_ws[:,:,0] = M # Dirac case
            else:
                M_ws = np.zeros((dim,dim,2), M.dtype)
                M_ws[:,:,0] = M[:dim,:dim] 
                M_ws[:,:,1] = M[dim:,dim:] 
                # note that the off-diagonal blocks are lost here
            return M_ws
            
        D = Matrix(ToEdxSpinFormat(D_ws))
        λc = ToEdxSpinFormat(λc_ws)
        E1e = ToEdxSpinFormat(E1e_ws)
        # initialize the hopping rep
        rep = gutz.CorrectRep()
        if self.gutz.nrel == 2:
            rep1 = rep
        else:     
            rep1 = np.block([[rep,   rep*0], 
                            [rep*0,  rep]])
        rep = np.block([[rep1, rep1], 
                        [rep1, rep1]])

        # The 1 body hamiltonian:
        # ------------------------------
        H1e = np.block([[E1e,   D],
                        [D.H,  -λc]])
        # ------------------------------

        def MakeFockStates():
            # this is quite inefficient, but it happens only once
            nrange = None
            if self.gutz.NRange.low <= self.gutz.Nimp and self.gutz.Nimp <= self.gutz.NRange.high:
                nrange = list(np.arange(self.gutz.NRange.low, self.gutz.NRange.high+1))
            n = D.shape[0]
            if nrange is None:
                if gutz.nrel < 2 and gutz.num_si ==1:
                    fock_states = FockStatesNoSpinSymmetry(n) # is the interlacing code compatible with the ed??
                else:
                    # only N conserved
                    fock_states=FockStatesNoSpinSymmetry(n)
            else: 
                print("- Building fock space for l=%d with range %s"%(n, nrange))
                fock_states=FockStatesNoSpinSymmetryWithRange(n, nrange=nrange)
            return fock_states

        out = Diagonalize(DiagonalizerWorker, H1e, Utensor, MakeFockStates, rep, opts=opts)

        def ToBits(self, a):
            ret = ""
            while a != 0:
                if a & 0x1:
                    ret = "1"+ret 
                else:
                    ret = "0"+ret 
                a = a >> 1
            ret = "0" *(self.dim - len(ret)) + ret
            return ret

        self.num_calls +=1

        rho =  out.denmat[:,:,0] # TODO: complete the energy, occupation
        dim0 = rho.shape[0] //2

        if out.neval > 1:
            if abs(out.E[0]-out.E[1]) < 1.0e-4:
                print(f"** warning: lowest ED eigenvalues are degenerate, {out.E}, solution may be incorrect")

        return (out.E[0],
                out.histogram, 
                FromEdxSpinToRhobusta(rho[:dim0,:dim0]),   #Cc
                FromEdxSpinToRhobusta(rho[dim0:,dim0:]),   #Ff 
                FromEdxSpinToRhobusta(rho[:dim0,dim0:]))   #Cf        

    def BuildHistogram(self, num_configs):
        his = Histogram(num_configs = num_configs)
        his.allocate()
        total = 0.0
        print ()
        for i in range(num_configs):
            his.p[i] = self.histogram[i]
            his.N[i] = countBits(i)
            total += self.histogram[i]
        his.p[:] = his.p[:] / total
        return his

    def DebugPrint(self, s : str, M):
        if self.opts.verbose > 2:
            PrintMatrix(s,M)

    def ClipMatrixWhenSingular(self, Δp, M, si, eps=1.0e-5):
        found = set()
        Δpsq = Matrix(Δp[:,:,si]) * Matrix((self.gutz.ID-Δp)[:,:,si])
        self.DebugPrint("Δ(1-Δ)",Δpsq)
        self.DebugPrint("D",M)
        for i in range(Δp.shape[0]):
            for j in range(Δp.shape[1]):
                if abs(Δpsq[i,j]) < eps:
                    if np.isnan(M[i,j,si]) or abs(M[i,j,si]) > 1.0e-3: # nonzero
                        M[i,j,si] =1.0e-3
                        if i==j and abs(Δp[i,j,si]-1.0)<eps:
                            found.add("1") # just to report to users
                        elif abs(Δp[i,j,si])<eps:
                            found.add("0") # just to report to users
        
        self.mottCloseness = self.mottCloseness.union(found)

    def CalcΔpDM(self, Δ = None, onlyΔ=False, avoid_mott=False):
        gutz = self.gutz # shortcut
        # this is what we calculate
        Δp =  np.zeros_like(gutz.R)
        D  = np.zeros_like(gutz.R)
        M = np.zeros_like(gutz.R)

        for si in range(gutz.num_si):
            R = gutz.R[:,:,si]
            L = gutz.Lambda[:,:,si]
            self.DebugPrint("R:", R)
            self.DebugPrint("λ:", L)
            if gutz.num_si > 1:
                Rafm   = gutz.R[:,:,1-si]
                Lafm   = gutz.Lambda[:,:,1-si]
            # Gdim is the full dimension of the subspace that is multiplied by copies of R
            Gdim = gutz.dim * (gutz.num_equivalents + gutz.num_anti_equivalents) 
            RR = Matrix(np.eye(gutz.num_bands,dtype=np.complex128))
            LL = Matrix(np.zeros_like(RR))
            RR[-Gdim:,-Gdim:] = block_diag(*([Rafm for _i in range(gutz.num_anti_equivalents)] +
                                             [R for _i in range(gutz.num_equivalents)]))
            LL[-Gdim:,-Gdim:] = block_diag(*([Lafm for _i in range(gutz.num_anti_equivalents)] +
                                             [L for _i in range(gutz.num_equivalents)]))

            shapeWithK = (gutz.num_bands, gutz.num_bands, gutz.num_k_irr)
            rhoK = np.zeros(shapeWithK, dtype=np.complex128)
            # Equation 1: calculate Δp 
            ΔpBands = np.zeros_like(RR)
            for k in range(self.gutz.num_k_irr):
                rhoK[:,:,k] = self.Fermi(RR * Matrix(gutz.Hk[k,:,:,si])* RR.H + LL -gutz.mu*gutz.ID1F)
                if Δ is None: ΔpBands += rhoK[:,:,k] * gutz.weight[k]
            if Δ is None: 
                Δp[:,:,si] = self.ProjectAndSymmetrize(ΔpBands).T
                self.DebugPrint("Δp:", Δp[:,:,si])
            else:
                Δp[:,:,si] = Δ[:,:,si] 
                        
            if onlyΔ: continue

            # Equation 2: calculate D
            K = np.zeros_like(RR)
            for k in range(self.gutz.num_k_irr):
                # adler: is this  the correct order below?
                K += Matrix(gutz.Hk[k,:,:,si]) * RR.H * rhoK[:,:,k] *gutz.weight[k]  
            K = Matrix(self.ProjectAndSymmetrize(K))
            #K -= Matrix(R).H.I
            def function1(x):
                f1 = []
                for v in x:
                    if avoid_mott and abs(v * (1.0 -v)) < 1.0e-7:
                        f1.append(0.0)
                    else:
                        f1.append(1.0/(np.sqrt(v * (1.0  - v))+self.opts.solver_eta*1j))
                return f1
            D[:,:,si] = K * (self.MatrixFunction(Δp[:,:,si], function1)).T
            
            self.DebugPrint("D:", D[:,:,si]) # note D is not necessarily hermitian but satisfies the rep.
            
        if onlyΔ: return Δp

        # D should satisfy the rep
        self.gutz.SymmetrizeCorrelated(D,hermitian=False)
        self.DebugPrint("D matrix after symmetrization", D)
        for si in range(gutz.num_si):
            if avoid_mott:
                self.ClipMatrixWhenSingular(Δp, D, si)
                self.DebugPrint("D matrix after clip", D)
                
        for si in range(gutz.num_si):
            M[:,:,si] = self.dΔ_dDs(Δp[:,:,si],D[:,:,si], gutz.R[:,:,si])

        return Δp, D, M

    def PrettyPrintX(self, x):
        xstr = "current:  " + ",".join([f"{xi:+3.4f}" for xi in x])
        if self.x0prev is not None:
            xdiff = x - self.x0prev
            num_diff = np.sum(abs(xdiff) > 1.0e-12)
            if num_diff == 1 and xdiff[0] > 1.0e-12:
                xstr = f"gradient: (1/{len(xdiff)})"
            elif num_diff == 2:
                for i,xx in enumerate(xdiff):
                    if xx > 0.0 and i > 0 and xdiff[i-1]<0:
                        xstr = f"gradient: ({i+1}/{len(xdiff)})"
                        break
        print(f"{xstr}")
        self.x0prev = np.copy(x)

    def calcGutzF1F2(self, x, final=False):
        self.PrettyPrintX(x)
        self.RλParams.Unpack(x)
        gutz = self.gutz # shortcut

        # Equation 3: calculate λc (embedding bath)
        Δp, D, M = self.CalcΔpDM(avoid_mott=self.opts.solver_avoid_mott)
        λc =  -gutz.Lambda - M
        self.DebugPrint("λc:", λc[:,:,0])

        #Notice: no mu here: it is absent in D, λc as well
        E1e = gutz.Hloc - gutz.Vdc

        # symmetrize - although it is almost symmetrized (confined to the rep.) in DerivativeExpr3
        # this way, the results of the ED will be in the representation (unless the ground state is degenerate)
        self.gutz.SymmetrizeCorrelated(λc, hermitian=not self.opts.gutz_with_lifetime)

        # Equation 4: solve the EV problem       
        self.E, self.histogram, self.ρCc,self.ρFf,ρCf  = self.SolveEmbeddingWithEdX(D, λc, E1e, 
                                                                    gutz.UTensor, self.opts.verbose, self.opts)
        self.DebugPrint("ρ:",self.ρCc[:,:,0])
        self.gutz.SymmetrizeCorrelated(self.ρCc, hermitian=True)

        if final:
            gutz.RhoImp[...] = self.ρCc[...]
            gutz.Nimp = 0
            for si in range(gutz.num_si):
                gutz.Nimp += np.trace(gutz.RhoImp[:,:,si]).real 
            if gutz.num_si == 1 and gutz.nrel < 2: gutz.Nimp *= 2
        
        # Equations 5 (F1) and 6 (F2)
        for si in range(gutz.num_si):
            R = Matrix(gutz.R[:,:,si])
            def function3(x):
                f1 = []
                for v in x:
                    if v >= 1.0 or v <= 0:
                        f1.append(1.0e10) #penalty
                    else:
                        f1.append(np.sqrt(v * (1.0  - v)))
                return f1
            self.F1[:,:,si] =  ρCf[:,:,si] - R.T * self.MatrixFunction(Δp[:,:,si], function3)
        self.F2[:,:,:] = gutz.ID- self.ρFf - Δp

        if self.opts.gutz_with_lifetime:
            for si in range(gutz.num_si):
                for m in range(self.F2.shape[0]):
                    self.F2[m,m,si] = abs(self.F2[m,m,si]) + max(np.imag(self.gutz.Lambda[m,m,si]), 0)
            
        F1F2 = self.F1F2Params.Pack()

        maxval =  max(abs(F1F2))
        if maxval < self.min:
            # store for recovery
            ess = EDSolverState(num_x=x.shape[0], x=x)
            ess.store(self.xfile+":/", flush=True)
            
            self.min = maxval
            self.xMin = np.copy(x)

        if final:
            gutz.quality = maxval 
        print(f"MAXERR={maxval}")
        sys.stdout.flush()
        if self.opts.verbose>2:
            print(f"F1F2: {F1F2}")
        if maxval < self.opts.solver_stop_eps:
            F1F2[:] = 0.0
            print(f"stopping equation solver because maxerr is lower than {self.opts.solver_stop_eps}")
        return F1F2

    def Solve(self):
        x0 = self.RλParams.Pack()
        # options for root solver
        options = {'xtol': 1.0e-7,  # tolerance for convergence
                   'eps': self.opts.solver_stop_eps,  # step size
                   'disp': True,
                   'maxfev' : self.opts.solver_num_calls*len(x0), # maximum number of calls
                   'factor': 0.1  # initial factor of the step size (use small to avoid overshooting in the first).
                   }
        self.gutz.success = 0
        # root solver

        
        self.x0prev = None

        def F1F2(x, *args):
            return args[0].calcGutzF1F2(x)

        xf = Path(self.xfile)
        if xf.exists():
            print("- restarting from existing ED x point")
            Persistent.Refresh(self.xfile+":/")
            eds = EDSolverState()
            eds.load(self.xfile+":/")
            x0[:] = eds.x[:]
            # now remove the file
                        
        solution = optimize.root(F1F2, x0, 
                                 tol=1.0e-7,
                                 args=(self), method=self.opts.variant_solver, 
                                 options=options)

        xf = Path(self.xfile)
        if xf.exists(): xf.unlink()

        print("success=", solution.success)
        print(f"number of diagonalizations: {self.num_calls}")
        print(solution.message)
        if solution.success:
            print("converged iteration")
            print("===================")
            self.gutz.success = 1
            self.calcGutzF1F2(solution.x, final=True)
            self.gutz.energyImp = self.CalculateImpuirtyEnergy()
        else:
            if self.min < self.opts.graceful_fail_solver: 
                # graceful fail: use the x for which the minimum value was found (if that value is not too large)
                print(f"Not converged - but setting the input variables where |F1F2|={self.min}")
                self.calcGutzF1F2(self.xMin, final=True)
                self.gutz.energyImp = self.CalculateImpuirtyEnergy()
            else:
                # undo the calculation
                print(f"Not converged - resetting variables")
                self.RλParams.Unpack(x0)
        self.gutz.StoreSolution(self.BuildHistogram(num_configs=2**self.gutz.norb2))

    def CalculateImpuirtyEnergy(self):
        gutz = self.gutz
        # Equation 3: calculate λc (embedding bath)
        Δp, D, M = self.CalcΔpDM(avoid_mott=self.opts.solver_avoid_mott)
        # note that λc + λ = -M, we calculate the term from the field equation
        λc =  -M-gutz.Lambda

        def function3(x):
            f1 = []
            for v in x:
                f1.append(np.sqrt(v * (1.0  - v)))
            return f1

        spf =  2.0/(gutz.num_si * gutz.nrel)
        total = self.E
        for si in range(gutz.num_si):
            # importantly note that we have to add sum(λc) to get the actual eigenvalue, since the problem 
            # that was formulated for eigensolution does not include a constant of λc (because of the form f*F vs. F*f)
            total += np.trace(λc[:,:,si])*spf 
        
        for si in range(gutz.num_si):
            # Note that we do not drop the saddle point term λΔp, although it does not have an explanation as cancelling the bath part of the impurity
            total += spf*np.trace(Matrix(M[:,:,si]) * Matrix(Δp[:,:,si]).T)[0,0]
            mf = self.MatrixFunction(Δp[:,:,si], function3) * Matrix(D[:,:,si])
            tp =  spf*np.trace(Matrix(gutz.R[:,:,si]).T * mf)[0,0]
            total -= tp + np.conj(tp)
        if self.gutz.num_anti_equivalents == 1:
            total *=2
        return np.real(total)


class IterativeSolver(GutzEquationsSolver):
    def __init__(self, gutz: GutzEquationsInterface, isReal: bool, opts):
        super().__init__(gutz, isReal, opts)
        self.D = np.zeros_like(gutz.R)
        self.λc = np.zeros_like(gutz.Lambda)
        self.Δp = np.zeros_like(gutz.Lambda)
        self.DλcParams = Parameterizer(gutz)
        self.DλcParams.AddSpinfulMatrix(self, "D", isHermitian=False, isReal=isReal)
        self.DλcParams.AddSpinfulMatrix(self, "λc", isHermitian=not opts.gutz_with_lifetime, isReal=isReal)

        self.λParams = Parameterizer(gutz)
        self.λParams.AddSpinfulMatrix(gutz, "Lambda", isHermitian=not opts.gutz_with_lifetime, isReal=isReal)
        self.F2Params = Parameterizer(gutz)
        self.F2Params.AddSpinfulMatrix(self, "F2", isHermitian=not opts.gutz_with_lifetime, isReal=isReal)
        self.ΔpParams = Parameterizer(gutz)
        self.ΔpParams.AddSpinfulMatrix(self, "Δp", isHermitian=True, isReal=isReal)
        self.λdiff = 0.0

    def From_Rλ_To_Dλc(self):
        gutz = self.gutz # shortcut

        # Equation 3: calculate λc (embedding bath)
        Δp2, D, M = self.CalcΔpDM(avoid_mott=self.opts.solver_avoid_mott)
        self.λc[...] =  -gutz.Lambda[...] - M[...]
        self.DebugPrint("λc:", self.λc[:,:,0])

        self.D[...] = D[...]
        self.Δp[...] = Δp2[...] # just for comparison

        # symmetrize - although it is almost symmetrized (confined to the rep.) in DerivativeExpr3
        # this way, the results of the ED will be in the representation (unless the ground state is degenerate)
        self.DλcParams.Symmetrize()

    # solve equation 1 for lambda, fixing all other parameters
    def LambdaSolver(self):
        λ0 = self.λParams.Pack()
        options = {'xtol': 1.0e-10,  # tolerance for convergence
                   'eps': self.opts.eps_solver,  # step size
                   'disp': True,
                   'maxfev' : 10000, # maximum number of calls
                   'factor': 0.1  # initial factor of the step size (use small to avoid overshooting in the first).
                   }

        x = np.copy(λ0)

        def ErrorInΔpGivenλ(λ, *args):
            self.λParams.Unpack(λ)
            Δp2 = self.CalcΔpDM(onlyΔ=True, avoid_mott=False) # solve exactly
            self.F2[...] = Δp2 - self.Δp
            return self.F2Params.Pack()

        solution = optimize.root(ErrorInΔpGivenλ, x, 
                                 tol=1.0e-7,
                                 args=(self), method=self.opts.variant_solver, 
                                 options=options)
        if not solution.success:
            if np.max(np.abs(self.F2)) > 1.0e-2:
                print(f"** trouble solving equation (1) for λ, maxerr is {np.max(np.abs(self.F2))} **")
                self.λParams.Unpack(λ0)
                return
        self.λParams.Unpack(solution.x)

    def From_Dλc_To_Rλ(self, invert_lambda=True):
        gutz = self.gutz # shortcut

        #Notice: no mu here: it is absent in D, λc as well
        E1e = gutz.Hloc - gutz.Vdc

        # Equation 4: solve the EV problem       
        self.E, self.histogram, self.ρCc,self.ρFf,ρCf  = self.SolveEmbeddingWithEdX(self.D, self.λc, E1e, 
                                                                    gutz.UTensor, self.opts.verbose, self.opts)
        self.DebugPrint("ρ:",self.ρCc[:,:,0])
        gutz.SymmetrizeCorrelated(self.ρCc,hermitian=True)
        
        # Equations 6
        self.Δp[...] = gutz.ID- self.ρFf

        # Equation 5
        for si in range(gutz.num_si):
            def function4(x):
                return 1.0/(np.sqrt(x * (1.0  - x))+self.opts.solver_eta*1j)
            R = ρCf[:,:,si] * self.MatrixFunction(self.Δp[:,:,si], function4)
            gutz.R[:,:,si] = R.T

        M = np.zeros_like(gutz.R)
        for si in range(gutz.num_si):
            M[:,:,si] = self.dΔ_dDs(self.Δp[:,:,si], self.D[:,:,si], gutz.R[:,:,si])
        # Equation 3: calculate λc (embedding bath)
        gutz.Lambda[...] = -self.λc[...] - M[...]
        self.RλParams.Symmetrize() 
        # solve equation (1) for Lambda with fixed Δp
        lambda0 = np.copy(gutz.Lambda)
        if invert_lambda:
            self.LambdaSolver()
            self.RλParams.Symmetrize()
        self.λdiff = np.max(np.abs(lambda0-gutz.Lambda))

    # Iterative step. We start with Rλ and go back to it, but  also update Dλc again.
    def AyralSolver(self):
        self.From_Dλc_To_Rλ()
        Rλ1 = self.RλParams.Pack()
        self.From_Rλ_To_Dλc()
        return Rλ1

    def Solve(self):
        gutz = self.gutz # shortcut
        converged = False
        self.iter = 1
        self.From_Rλ_To_Dλc()
        Rλ1 = self.RλParams.Pack()
        Rλ0 = np.copy(Rλ1)
        Dλc1 = self.DλcParams.Pack()
        Δp1 = self.ΔpParams.Pack()

        diis = DIIS(initial_v=Dλc1,
                    example_err=Dλc1,
                    NHistory=self.opts.solver_diis_nmax, 
                    eps=self.opts.solver_diis_start_epsilon, 
                    noop=not self.opts.solver_with_diis,
                    admix=self.opts.gutz_admix,
                    converged_eps=self.opts.eps_solver)

        gutz.success = 0
        min_maxerr = 10.0e6
        minDλc = np.copy(Dλc1)
        
        self.DebugPrint("λ initial:", gutz.Lambda[:,:,0])
        if hasattr(self,'E'):
            prevE = np.real(self.E)
        else:
            prevE = 0.0

        while not converged and self.iter < self.opts.solver_num_calls*Rλ0.shape[0]:
            self.mottCloseness = set()  # nullify the closeness
            try:
                Rλ2 = self.AyralSolver()
            except Exception as e:
                print(f"gutzwiller computation crashed! Not saving result.")
                min_maxerr = 1.0e10 # so that this is not recorded as success
                break

            # capture up to date vectors, right after the solver iteration.
            Dλc2 = self.DλcParams.Pack() 
            Δp2 = self.ΔpParams.Pack()
  
            step, Dλc, maxerr = diis.AdjustVector(Dλc2,Dλc2-Dλc1)
            
            asterik = ""
            if maxerr < min_maxerr:
                minDλc[:] = Dλc1[:]
                min_maxerr = maxerr
                asterik = "*"

            if Dλc is None: 
                minDλc[:] = Dλc1[:]
                converged = True
                print(f"converged with maxerr={maxerr}")
                break # we are converged
            
            #if maxerr > 100.0:
            #    print(f" -- stopping iterative solver, maxerr={maxerr}")
            #    break

            self.DebugPrint("λ:", gutz.Lambda[:,:,0])
            
            mott = ""
            if len(self.mottCloseness) > 0:
                mott = " !!! ρ" + ",".join(self.mottCloseness)

            maxλ = np.max(abs(gutz.Lambda))

            print(f"{step} #{self.iter}: E0={np.real(self.E):6.6f} "+
                  f"max(λ)={maxλ:3.3f} 𝛿(λi)={self.λdiff:10.8f} "+
                  f"𝛿(Δp)={np.max(np.abs(Δp1-Δp2)):10.8f} "+
                  f"𝛿(R,λ)={np.max(np.abs(Rλ1-Rλ2)):10.8f} "+ 
                  f"𝛿(λc,D)={maxerr:10.8f}{mott}{asterik} ",
                  #f"proposed admix: {min(1/(abs(np.real(prevE - self.E)) * maxerr), 0.8):3.3f}", 
                  flush=True)
            prevE = self.E

            if maxλ > 1000.0:
                print("Maximum λ diverging - failing gutzwiller.")
                min_maxerr = 1.0e10 # so that this is not recorded as success
                break 
                
            autopilot_admix=self.opts.gutz_admix
            no_verbose=[] 
            if self.opts.solver_auto_pilot:
                # don't report changes in this variable in auto pilot
                no_verbose = ["gutz_admix"]
            RefreshOptions(self.opts,verbose=True, no_verbose=no_verbose)
            # in auto pilot mode, don't override the admix
            if self.opts.solver_auto_pilot:
                self.opts.gutz_admix=autopilot_admix

            if step == "admix" and self.opts.solver_auto_pilot and self.iter % self.opts.solver_n_admix_batch == 0:
                #print(f"Dλc: {Dλc[:]}")
                #print(f"minDλc: {minDλc[:]}")
                #Dλc[:] = minDλc[:]
                new_admix = min(1.0/maxerr, 0.8)
                if abs(new_admix-self.opts.gutz_admix) > 1.0e-7:
                    print(f"      autopilot admix change: {self.opts.gutz_admix:3.3f} → {new_admix:3.3f}")
                    self.opts.gutz_admix = new_admix
                
            diis.SetOptions(eps=self.opts.solver_diis_start_epsilon, 
                            noop=not self.opts.solver_with_diis,
                            admix=self.opts.gutz_admix,
                            converged_eps=self.opts.eps_solver)

            converged = maxerr < self.opts.eps_solver
            gutz.quality = maxerr

            self.iter +=1
            Rλ1[:] = Rλ2[:]
            Dλc1[:] = Dλc[:]
            Δp1[:] = self.ΔpParams.Pack()
            self.DλcParams.Unpack(Dλc)

        # after loop
        # even if failed, if err < graceful fail threshold, we still set the solution
        if min_maxerr < self.opts.graceful_fail_solver:
            self.DλcParams.Unpack(minDλc)
            self.From_Dλc_To_Rλ(invert_lambda=False)
            # set solution fields
            gutz.RhoImp[...] = self.ρCc[...]
            gutz.Nimp = 0
            gutz.energyImp = self.CalculateImpuirtyEnergy()
            for si in range(gutz.num_si):
                gutz.Nimp += np.trace(gutz.RhoImp[:,:,si]).real 
            if gutz.num_si == 1 and gutz.nrel < 2: gutz.Nimp *= 2

        else:
            # rollback to previous R, Gamma - we are stuck until some parameters are changed manually
            self.RλParams.Unpack(Rλ0)
            # other components of the solution are unchanged. 

        gutz.success = int(converged)
        gutz.quality = min_maxerr
        gutz.StoreSolution(self.BuildHistogram(num_configs=2**gutz.norb2))


def FileNameForShell(shell, template):
    if shell < 0: # no shell selected - this is only valid in the one shell case
        shell = ""
    return template.format(shell=shell)

class FlatMPIEquationSolver(MPIContainer):

    def __init__(self, opts: Namespace):
        self.opts = opts
        MPIContainer.__init__(self, opts)
        self.gutz_name =  FileNameForShell(opts.which_shell, template="./gutz-impurity{shell}.tmp.h5:/" )

    # this happens in the caller, of course gutz will not be available as is in the workers
    def SetGutz(self, gutz):
        self.gutz = gutz

    def WorkerArgs(self):
        args = f"-m portobello.rhobusta.GutzEquations --worker --which-shell={self.opts.which_shell}"
        return args.split() + self.get_wrapped_parent_argv(truncated = True)

    def BeforeWorkers(self):
        # Write Gutz and the problem
        gp = GProblem().FromOther(self.gutz)
        gp.weight = self.gutz.lapw.weight # assign the weights
        gp.E = gp.Hloc # complete Null field before saving (this is a legacy field)

        gp.self_path = self.gutz.GetTopLevelMethodName()
        self.gutz.self_path = gp.self_path # communicate to multi_impurity
        gp.store(self.gutz.gutz_name, flush=True)
        Store.Singleton().CloseFile(self.gutz.gutz_name) # so that we will overwrite it next time
        return True
        
    def MPIWorker(self):
        self.InitializeMPI()

        gp = GProblem()
        gp.load(self.gutz_name) # load the impurity problem
        Store.Singleton().CloseFile(self.gutz_name) # so that we will overwrite it next time - in case this is one process
        
        # recover Gutz description and the problem - note that we do not open connection to flapw.
        gs = GState()
        gs.load(gp.self_path) # this may be a non-default impurity

        assert(self.gutz_name == gp.gutz_name)
        
        gei = GutzEquationsInterface(gs, gp, self.opts)

        # master drives the solution, the workers only participate in ED
        if self.IsMaster():
            if self.opts.iterative_solver:
                ges = IterativeSolver(gei,isReal=self.opts.is_real, opts=self.opts)
            else:
                ges = GutzEquationsSolver(gei,isReal=self.opts.is_real, opts=self.opts)
            ges.Solve()
            if self.mpi is not None: self.mpi.COMM_WORLD.bcast("done", root=0)
        else:
            keepGoing = True
            while keepGoing:
                keepGoing = DiagonalizerWorker(self.opts)
        self.Terminate()
          


def GetGutzEquationsOptionParser(add_help=False, private=False):
    if private:
        parents = [GetMPIProxyOptionsParser()]
    else:
        parents = []
    parser = ArgumentParserThatStoresArgv('gutz-equations', add_help=add_help ,
                            parents=parents,
                            formatter_class=ArgumentDefaultsHelpFormatter)
 
    parser.add_argument("--gutz-debug", dest="verbose",
                        type=int,
                        default=2,
                        help="verbosity for the GA iterations. 1: print all intermediate. 2: Print only the maxerror. 0: No verbosity.")
        
    sol = parser.add_argument_group(title="Solver options", description="parameters that control the solver works")
              
    sol.add_argument("--gutz-with-lifetime", dest="gutz_with_lifetime",
                        default=False,
                        action="store_true",
                        help="Add lifetime to lambda (make it complex)")

    sol.add_argument("--solver-non-iterative", dest="iterative_solver",
                        default=True,
                        action="store_false",
                        help="Solve using the Ayral et. al. 2017 iterative idea")

    sol.add_argument("--solver-variant", dest="variant_solver",
                        type=str,
                        default="hybr",
                        help="""variant of the solver (non iterative only). Other options are listed in 
                                https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.root.html#scipy.optimize.root
                                including lm and Krylov which are the more reasonable ones""")

    sol.add_argument("--solver-num-calls", dest="solver_num_calls",
                        type=int,
                        default=50,
                        help="number of calls per variable (irreps of R and lambda in reals)")
                        
    sol.add_argument("--solver-admix", dest="gutz_admix",
                        type=float,
                        default=0.2,
                        help="used only in the iterative solution to throttle the change in the potential (lambda)")
                        
    sol.add_argument("--solver-graceful-fail-threshold", dest="graceful_fail_solver",
                        type=float,
                        default=0.1,
                        help="if non-iterative solver 'fails', but reaches quality < this number, then the x that achieves this quality is used as a solution")
                        
    sol.add_argument("--solver-num-evs", dest="solver_num_evs",
                        type=int,
                        default=1,
                        help="change this to n>1 to see the lowest n ev's in the solution. it does cost in time.")
                        
    sol.add_argument("--solver-eps", dest="eps_solver",
                        type=float,
                        default=1.0e-6,
                        help="non-iterative solvers: eps step for the solver")
      
    sol.add_argument("--solver-eta", dest="solver_eta",
                        type=float,
                        default=1.0e-9,
                        help="imaginary number to circumvent 1/Delta issues (fake lifetime)")
      
    sol.add_argument("--solver-ed-tolerance", dest="ed_tolerance",
                        type=float,
                        default=1.0e-7,
                        help="""tolerance for the ED algorithm""")
    
    sol.add_argument("--solver-ed-ncv", dest="ncv",
                        type=int,
                        default=30,
                        help=""""ncv parameter for ARPACK""")
    
    sol.add_argument("--solver-stop-eps", dest="solver_stop_eps",
                        type=float,
                        default=1.0e-8,
                        help="stop if  reached result below this value")
    
    sol.add_argument("--solver-tmp-dir", type=str, dest="solver_tmp_dir", default=".", help="directory where large files are created in the diagonalization server")
    
    
    sol.add_argument("--solver-diis-start-epsilon", dest="solver_diis_start_epsilon",
                        type=float,
                        default=0.01,
                        help="the small difference in the SCF loop at which to start applying diis acceleration")
    
    sol.add_argument("--solver-diis-n-max", dest="solver_diis_nmax",
                        type=int,
                        default=10,
                        help="maximum size of the DIIS matrix. if not enough, diis mode will be stopped (and possibly restart from 1)")
    
    sol.add_argument("--solver-with-diis", dest="solver_with_diis",
                        default=False,
                        action="store_true",
                        help="use diis accelerator in the iterative solver")

    sol.add_argument("--solver-auto-pilot", dest="solver_auto_pilot",
                        default=False,
                        action="store_true",
                        help="adjust admix automatically")

    sol.add_argument("--solver-n-admix-batch", dest="solver_n_admix_batch",
                        type=int,
                        default=4,
                        help="number of steps between admix changes in auto pilot mode)")
    
    sol.add_argument("--solver-avoid-mott", dest="solver_avoid_mott",
                        default=True,
                        type=bool,
                        help="avoid getting into integer occupied mott states on a way to a solution that sohuld not be")

    return parser



if __name__ == '__main__':
    parser = ArgumentParserThatStoresArgv("gutz-equations-solver", add_help=True,
                            parents=[GetGutzEquationsOptionParser(private=True)],
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("--is-real", dest="is_real",
                        default=False,
                        action="store_true",
                        help="Make the gutzwiller λ variables real")
    
    parser.add_argument("--interaction-truncation", dest="interaction_truncation", 
                    default=0.0, 
                    type=float, help="values of U below this in [eV] will be truncated to 0.0. Usually useful only for --full-interaction.")
           
    parser.add_argument("-N", "--N0", dest="N0", 
                    required=True,
                    type=str, help="""Either a number (float, int), or 3 numbers separated by colon (:).
                      If number, it is just the occupation used for double counting.
                      Otherwise, the 1st and last number designate a range of occupations which sohuld
                      be used for the fock space, and the middle number should be the double counting occupation as above.
                      For example 1:3:5 means the double counting N0=3, and the range 1,2,3,4,5 will be used for construction
                      of the fock space.""")
    
    # we use this to form the file name of the interaction tensor, so that there is no overwrite
    parser.add_argument("--which-shell", dest="which_shell", default=-1, type=int,
                    help="which shell (index) to project to, in order to build the correlated problem")


    opts, _args = parser.parse_known_args(sys.argv[1:])
    fqs = FlatMPIEquationSolver(opts)
    fqs.Run()

    
