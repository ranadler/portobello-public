# -*- coding: utf-8 -*-
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go

app = dash.Dash()

app.layout = html.Div([
    html.Label('Dropdown'),
    dcc.Dropdown(
        options=[
            {'label': 'New York City', 'value': 'NYC'},
            {'label': 'Montréal', 'value': 'MTL'},
            {'label': 'San Francisco', 'value': 'SF'}
        ],
        value='MTL'
    ),
    

    html.Label('Multi-Select Dropdown'),
    dcc.Dropdown(
        options=[
            {'label': 'New York City', 'value': 'NYC'},
            {'label': 'Montréal', 'value': 'MTL'},
            {'label': 'San Francisco', 'value': 'SF'}
        ],
        value=['MTL', 'SF'],
        multi=True
    ),

    html.Label('Radio Items'),
    dcc.RadioItems(
        options=[
            {'label': 'New York City', 'value': 'NYC'},
            {'label': 'Montréal', 'value': 'MTL'},
            {'label': 'San Francisco', 'value': 'SF'}
        ],
        value='MTL'
    ),

    html.Label('Checkboxes'),
    dcc.Checklist(
        options=[
            {'label': 'New York City', 'value': 'NYC'},
            {'label': 'Montréal', 'value': 'MTL'},
            {'label': 'San Francisco', 'value': 'SF'}
        ],
        values=['MTL', 'SF']
    ),

    html.Label('Text Input'),
    dcc.Input(value='MTL', type='text'),

    html.Label('Slider'),
    dcc.Slider(
        min=0,
        max=9,
        marks={i: 'Label {}'.format(i) if i == 1 else str(i) for i in range(1, 6)},
        value=5,
    ),
    

    dcc.Graph(
        id='scattercarpet',
        figure={
            'data': [
                go.Carpet(
                    a = [0.1,0.2,0.3],
                    b = [1,2,3],
                    y = [[1,2.2,3],[1.5,2.7,3.5],[1.7,2.9,3.7]],
                    cheaterslope = 1,
                    aaxis = dict(
                        title = "a",
                        tickmode = "linear",
                        dtick = 0.05
                    ),
                    baxis = dict(
                        title = "b",
                        tickmode = "linear",
                        dtick = 0.05
                    )
                ),
                go.Scattercarpet(
                    name = "a = 0.2",
                    a = [0.2, 0.2, 0.2, 0.2],
                    b = [0.5, 1.5, 2.5, 3.5],
                    line = dict(
                        smoothing = 1,
                        shape = "spline"
                    ),
                      marker = dict(
                        size = [10, 20, 30, 40],
                        color = ["#000", "#f00", "#ff0", "#fff"]
                      )
                )]    
            }),
        
  
    dcc.Graph(
        id='life-exp-vs-gdp',
        figure={
            'data': [
                 go.Scatter(
                        x=[1, 2, 3, 4],
                        y=[1, 4, 9, 16],
                        name='$\\alpha_{1c} = 352 \\pm 11 \\text{ km s}^{-1}$'
                    )
            ],
            'layout': go.Layout(
                hovermode='closest',
   
                xaxis=dict(
                    title='$\\sqrt{(n_\\text{c}(t|{T_\\text{early}}))}$', type='log'),
                yaxis=dict(
                    title='YAXIS')
                )
        }
    ),
    
], style={'columnCount': 1})

if __name__ == '__main__':
    app.run_server(debug=True)