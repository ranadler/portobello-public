#!/usr/bin/env python3


import contextlib
import os, sys, shutil
from pathlib import Path
from argparse import ArgumentDefaultsHelpFormatter
import numpy as np
import pickle
from portobello.bus.mpi import GetMPIProxyOptionsParser

from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.generated.dmft import DMFTFreeEnergy, DMFTState

from portobello.jobs import Organizer
from portobello.rhobusta.InputBuilder import DFTInputBuilder
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.dft__dmft import DFT__DMFT, DFTPlusDMFTOptionsParser
from portobello.rhobusta.dft__g import DFT__G, DFTPlusGOptionsParser
from portobello.rhobusta.dmft_energy import DMFT__ENERGY, DMFTEnergyOptionsParser
from portobello.rhobusta.energy import ENERGY, EnergyOptionParser
from portobello.rhobusta.rhobusta import DFT0, RhobustaOptionsParser
from portobello.rhobusta.options import SaveOptions, RefreshOptions
from portobello.rhobusta.ProjectorEmbedder import ProjectorOptionsParser, PROJ
from portobello.rhobusta.PEScfEngine import Ry2eV
from portobello.rhobusta.loader import LightWeightLoader
from portobello.rhobusta.DFTPlugin import DFTPlugin

from portobello.generated.energy import FreeEnergy
   
def OptionParser(add_help=False):
    
    parser = ArgumentParserThatStoresArgv('summit job generator', add_help=add_help,
                            parents = [GetMPIProxyOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
   
    parser.add_argument("-J", dest="name",
                     default="run",
                     help="Name of run")

    parser.add_argument("-R", "--run-options", dest="run_options", 
                     default="",
                     help="Set of options to pass to portobello")

    parser.add_argument("-T","--temperatures", dest="temperatures",
                     default="",
                     help="temperature range -- start:stop:number")

    parser.add_argument("-V", "--volumes", dest="volumes",
            default="", help="scaling factors for volume -- start:stop:number")

    parser.add_argument("-L", "--lattice-scalings", dest="lattice_scalings",
            default="", help="scaling factors for each lattice vector -- start:stop:number,start:stop:number,start:stop:number")

    parser.add_argument("-M", "--atom-displacements", dest="atom_displacements",
            default="", help="displace atom by fraction -- start:stop:number,start:stop:number,start:stop:number")

    parser.add_argument("-A", "--atom-to-displace", dest="atom_to_displace",
            default="", help="atom to displace")

    parser.add_argument("-D", "--direction-to-displace", dest="direction_to_displace",
            default="0,0,1", help="direction to displace atom")

    parser.add_argument("--cif", dest="cif_file", 
                     default = "",
                     help="cif file of run")

    parser.add_argument("--interpolation", dest="interpolation", 
                     default = "linear", choices=["cubic", "linear", "quadratic"],
                     help="interpolation to use when plotting results")

    parser.add_argument("--continue", dest="continue_run", 
                     default = False, action="store_true",
                     help="If there is already a DFT solution in all folders in the run, this may be set to avoid re-doing the DFT")

    parser.add_argument("--program", dest="program", choices=["dft", "proj", "dft+dmft", "dmft-energy", "dft+g", "energy", "all dft", "all dmft", "all gutz"],
                     default = "dft+dmft",
                     help="Program to run actoss all temperatures, double countings, etc.")

    parser.add_argument("--collect",
                     default = False, action="store_true",
                     help="Collect the results and plot them")

    parser.add_argument("--recover",
                     default = False, action="store_true",
                     help="Search for runs with problems and re-run them")

    parser.add_argument("--no-organizer",
                     default = False, action="store_true",
                     help="Instead of running with organizer / contributor model, just run with this master process")

    parser.add_argument("--debug",
                     default = False, action="store_true",
                     help="Plot additional energy components")
                    

    return parser

class DummyFile(object):
    def write(self, x): pass

@contextlib.contextmanager
def nostdout():
    save_stdout = sys.stdout
    sys.stdout = DummyFile()
    yield
    sys.stdout = save_stdout


class EnergyCurve:

    class Run:

        class Result:
            def __init__(self, energy, scale, volume):
                self.energy = energy
                if isinstance(scale,str):
                    self.scale = [float(s) for s in scale.split(",")]
                else:
                    self.scale = scale
                self.volume = volume

        def __init__(self, opts):
            self.opts = opts
            
            cif_option = "" if opts.continue_run else f"{opts.cif_file} "
            self.options = cif_option + f"--mpi-workers={int(opts.mpi_workers)} {opts.run_options}"

            self.outsource = not opts.no_organizer

            self.results=[]
            self.correlated_results=[]
            self.entropy=[]
            if self.opts.debug:
                self.vks_terms = []
                self.Fx_plus_muN = []
                self.results2 = []

            if self.opts.program ==  "all dmft":
                self.programs_to_run = ["dft+dmft", "dmft-energy", "energy"]
            elif self.opts.program == "all gutz":
                self.programs_to_run = ["dft+g", "energy"]
            elif self.opts.program == "all dft":
                self.programs_to_run = ["dft", "proj", "energy"]
            else:
                self.programs_to_run = [self.opts.program]
            
            self.program = self.programs_to_run[0]

        def getOptions(self, additional_options, dir):
            str_opts = self.options + " " + additional_options + f" --directory={dir} "
            if self.outsource:
                program = self.program.replace('-','__').replace('+','__').upper()
                if program == "DFT":
                    program += "0"
                str_opts += f" --outsource={program} "
            
            str_opts = str_opts.split(" ")

            while '' in str_opts:
                str_opts.remove('')

            if self.opts.mpi_runner_params != "": #because this can have spaces we treat it separately
                str_opts.append(f"--runner-params={self.opts.mpi_runner_params}")

            return str_opts
        
        def collect(self, dir, scaling):
            FE = FreeEnergy()
            FE.load(dir+"/energy.h5:/")

            st = DFTPlugin.GetStructureFromIni(dir)
            volume = st.lattice.volume

            if os.path.exists(dir+"/dmft_free_energy.h5"):
                DFE = DMFTFreeEnergy()
                DFE.load(dir+"/dmft_free_energy.h5:/")
                DMFT = DMFTState()
                DMFT.load(dir+"/dmft.h5:/")
                self.entropy.append(self.Result(-DFE.entropy/DMFT.beta/Ry2eV, scaling, volume))
            else:
                self.entropy.append(self.Result(0, scaling, volume))

            self.results.append(self.Result(FE.result, scaling, volume))
            self.correlated_results.append(self.Result(FE.corr_energy_term/Ry2eV, scaling, volume))
            

            if self.opts.debug:
                self.energy_breakdown_names = "Fx Fc z_vnucl ro_vh_new exch_dft e_coul tr_vxc_valence tr_vh_valence e_h_core e_xc_core core_energy muN correlated_term_ry".split(" ")
                for attr in self.energy_breakdown_names:
                    if not hasattr(self, attr):
                        setattr(self, attr, [])
                    current_list = getattr(self, attr)
                    current_list.append(self.Result(getattr(FE, attr), scaling, volume))
                    setattr(self, attr, current_list)

                self.Fx_plus_muN.append(self.Result(FE.Fx+FE.muN, scaling, volume))

                self.vks_terms.append(self.Result(
                    (FE.exch_dft - (FE.tr_vxc_valence+FE.e_xc_core))
                + FE.e_coul - 0.5*(FE.ro_vh_new),
                scaling, volume))

                self.results2.append(
                    self.Result(
                        + self.Fx_plus_muN[-1].energy 
                        + self.vks_terms[-1].energy
                        + self.core_energy[-1].energy,
                scaling, volume))


        def check_run_needs_recovery(self, opts, dir, update_super_iterations = False):
            currentDir = os.getcwd()
            os.chdir(dir)

            needs_recovery = False
            if not os.path.exists("./ini.h5") or not os.path.exists("./flapw_image.h5"):
                needs_recovery = True

            elif self.program != "dft":

                try:
                    odef = SelectedOrbitals()
                    odef.load("./projector.h5:/def/")
                except:
                    if os.path.exists("./projector.h5"):
                        os.unlink("./projector.h5")
                    needs_recovery = True

                try:
                    lwl = LightWeightLoader(opts)
                    lwl.LoadCalculation(loadRealSigma=False)

                    if update_super_iterations:
                        dif_iter = abs(lwl.svs.super_iter - opts.num_super_iterations)
                        opts.num_super_iterations = dif_iter

                except:

                    if os.path.exists("./dmft.h5"):
                        os.unlink("./dmft.h5")

                    elif os.path.exists("./gutz.h5"):
                        os.unlink("./gutz.h5")

                    needs_recovery = True
            
            os.chdir(currentDir)

            return needs_recovery

        def organize(self, additional_options = "", dir = "."):

            str_opts = self.getOptions(additional_options, dir)

            if self.program == "dft+dmft":
                parser = DFTPlusDMFTOptionsParser(str_opts)
                opts = parser.parse_known_args(str_opts)[0]
                opts.argv = [self.program] + str_opts
                prog = DFT__DMFT

            elif self.program == "dft+g":
                parser = DFTPlusGOptionsParser(str_opts)
                opts = parser.parse_known_args(str_opts)[0]
                opts.argv = [self.program] + str_opts
                prog = DFT__G
                
            elif self.program == "dft":
                parser = RhobustaOptionsParser(private=True)
                opts = parser.parse_known_args(str_opts)[0]
                opts.argv = [self.program] + str_opts
                prog = DFT0

            elif self.program == "proj":
                parser = ProjectorOptionsParser(private=True)
                opts = parser.parse_known_args(str_opts)[0]
                opts.argv = [self.program] + str_opts
                prog = PROJ
                
            elif self.program == "dmft-energy":
                parser = DMFTEnergyOptionsParser(str_opts, private=True)
                opts = parser.parse_known_args(str_opts)[0]
                opts.argv = [self.program] + str_opts
                prog = DMFT__ENERGY

            elif self.program == "energy":
                parser = EnergyOptionParser()
                opts = parser.parse_known_args(str_opts)[0]
                #Energy does not overload WorkerArgs, so we need to actually prepend the options with a real module command
                opts.argv = ["-m"] + [ENERGY.__module__] + str_opts
                prog = ENERGY

            else:
                print("Do not understand program name -- quiting")
                Organizer.Terminate()
                quit()

            #Have the organizer clean up any old organizers in the run directory and wait forever
            #for contributors to check back in (because dft+g / dft+dmft may take a very long time)
            org_opts = ["-n", "-e-1"]

            #clean the organizer directory and wait a very long time to decide things are done
            if self.opts.recover:
                needs_recovery = self.check_run_needs_recovery(opts, dir)
                if needs_recovery:
                    prog(opts, organizer_opts = org_opts)

            else:
                if self.outsource:
                    prog(opts, organizer_opts = org_opts)
                else:
                    mod = prog.__module__
                    if opts.argv[0] == "-m":
                        opts.argv = opts.argv[2:]
                    else:
                        opts.argv = opts.argv[1:]


                    cmd = f"python3 -m {mod} " + " ".join(opts.argv)

                    banner = "-"*int(1.1*len(cmd)+6)
                    print(f"{banner}")
                    print(f" --- Running command {cmd} ---")
                    print(f"{banner}")
                    os.system(cmd)

    def __init__(self, opts):
        self.opts = opts
        self.scalings = self.split_options(self.GetScalingOption())
        self.temperatures = self.split_options(self.opts.temperatures)
        
        self.scaling_str = ""
        if opts.collect or opts.continue_run or "dft" not in opts.program:
            self.no_cif = True
        else:
            self.no_cif = False
            assert opts.cif_file != "", "Must provide a cif file when starting a new run"
            if not "--reference-c" in opts.run_options and not "--reference-l" in opts.run_options:
                self.scaling_str = self.GetReferenceScalings()

        self.run = self.Run(opts)

    def organize(self):
        for program in self.run.programs_to_run:
            self.run.program = program

            if not self.opts.collect:
                if not self.opts.no_organizer:
                    print(f"Organizing {self.run.program}")
                
            for temp in self.temperatures:

                for i, scale in enumerate(self.scalings):
                    folder = f"./{self.opts.name}-{temp}"

                    if not os.path.exists(folder):
                        os.makedirs(folder)

                    folder += f"/{scale}"
                    if not os.path.exists(folder):
                        os.makedirs(folder)

                    if not self.no_cif:
                        shutil.copyfile(self.opts.cif_file, folder + "/" + self.opts.cif_file)
                
                    if self.opts.collect or self.run.program == "collect":
                        self.run.collect(folder, scale)
                    
                    else:
                        additional_options =  f" -T{int(temp)} {self.scaling_str} {self.GetAdditionalOptions(scale)}"
                        self.run.organize(additional_options = additional_options, dir = folder)
            
            if self.opts.collect:
                break

            Organizer.Wait()
        Organizer.Terminate()

        if self.opts.collect:
            import matplotlib.pyplot as plt
            import matplotlib
            matplotlib.use("TkAgg")

            if self.IsVolumeLike():

                fig, ax = plt.subplots(1,1)
                ax2, ax2l_ = self.GetSecondX(self.run.results)
                if ax2 is not None:
                    ax_twin = ax.twiny()
                else:
                    ax_twin = None
                self.plot_curve(fig,ax,ax_twin,self.run.results, color = 'red', label = "Total", note_minimum=True)
                self.plot_curve(fig,ax,ax_twin,self.run.correlated_results, color = 'blue', label = "Impurity")
                self.plot_curve(fig,ax,ax_twin,self.run.entropy, color = 'green', label = "Entropy")

                if self.opts.debug:
                    from matplotlib import cm
                    colors = cm.rainbow(np.linspace(0, 1, len(self.run.energy_breakdown_names)))
                    """
                    fig2, ax2 = plt.subplots(1,1)
                    ax2_twin = ax2.twiny()

                

                    for i, attr in enumerate(run.energy_breakdown_names):
                        res = getattr(run,attr)
                        plot_curve(fig2,ax2,ax2_twin,res, color = colors[i], label = attr)
                    """
                    fig3, ax3 = plt.subplots(2,1)
                    ax3_twin = ax3[0].twiny()
                    ax4_twin = ax3[1].twiny()

                    self.plot_curve(fig3,ax3[0],ax3_twin,self.run.Fx_plus_muN, color = colors[0], label = "Fx+muN")
                    self.plot_curve(fig3,ax3[0],ax3_twin,self.run.vks_terms, color = colors[2], label = "vks")
                    self.plot_curve(fig3,ax3[0],ax3_twin,self.run.core_energy, color = colors[4], label = "core")
                    self.plot_curve(fig3,ax3[0],ax3_twin,self.run.correlated_results, color = colors[6], label = "correlated")
                    self.plot_curve(fig3,ax3[1],ax4_twin,self.run.results2, color = colors[0], label = "sum dft", note_minimum=True)
                    self.plot_curve(fig3,ax3[1],ax4_twin,self.run.results, color = colors[-1], label = "sum dft + correlated", note_minimum=True)

            else:
                if self.IsSurfaceLike():
                    fig, ax = plt.subplots(2,1)
                    fig.subplots_adjust()
                    self.plot_surface(fig, ax[0], self.run.results, True, "Total Energy")
                    self.plot_surface(fig, ax[1], self.run.correlated_results, False, "Impurity Energy")
                    
                else:
                    self.report_min_of_3d(self.run.results)
                    self.report_min_of_3d(self.run.correlated_results)

            pickle.dump(fig, open(f"EVC.pickle","wb"))
            plt.show()

    def GetScalingOption(self):
        raise NotImplementedError

    def GetReferenceScalings(self):
        raise NotImplementedError

    def GetAdditionalOptions(self, scale):
        raise NotImplementedError
    
    def GetX(self,result):
        raise NotImplementedError

    def GetSecondX(self,result):
        return None, None

    def IsVolumeLike(self):
        return False

    def IsSurfaceLike(self):
        return False

    def split_options(self, opt, dtype=float, decimals = 3):
        sopt = opt.split(":")
        if len(sopt)==3:
            p = np.array(sopt, dtype=dtype)
            return np.round(np.linspace(p[0], p[1], int(p[2]), endpoint = True), decimals)

        res = []
        for o in opt:
            res.append(self.split_options(o))
        return res

    def plot_curve(self, fig, ax, ax_twin, results, color = 'black', label = "", set_max_to_zero = True, note_minimum = False):
        from scipy.interpolate import interp1d

        x, xlabel = self.GetX(results)
        y = np.squeeze(np.array([result.energy for result in results])) #squeeze protects against energies mistakely stored as rank 0 matrices

        if set_max_to_zero:
            y -= np.max(y)
        interpolator = interp1d(x,y,kind=self.opts.interpolation)
        
        xs = np.linspace(x[0], x[-1], 10*len(x), endpoint=True)
        ys = interpolator(xs)
        m = np.argmin(ys)
        ax.scatter(x, y, color=color, label = label)
        ax.plot(xs,ys, color=color)
        if note_minimum:
            ax.vlines(xs[m], min(ys), 0, linestyle=':', label=f"minimum at a={np.round(xs[m],4)}", color='black')
        ax.legend()
        ax.set_ylabel("Free energy (Ry relative to maximum value)")
        ax.set_xlabel(xlabel)

        x2, x2label = self.GetSecondX(results)

        if x2 is not None:
            ax_twin.set_xlabel(x2label)
            x2 = [result.scale for result in results]
            ax_twin.set_xlim(ax.get_xlim())
            ax_twin.set_xticks(x)
            ax_twin.set_xticklabels(x2)

    def plot_surface(self,fig, ax, results, add_title, label):
        import matplotlib.pyplot as plt
        from mpl_toolkits.axes_grid1 import make_axes_locatable
        from scipy.interpolate import griddata

        xy = np.array([[result.scale[0],result.scale[2]] for result in results])
        z = np.array([result.energy for result in results])
        z -= np.max(z)

        xs = np.linspace(np.min(xy[:,0]), np.max(xy[:,0]), 100, endpoint=True)
        ys = np.linspace(np.min(xy[:,1]), np.max(xy[:,1]), 100, endpoint=True)
        grid_x, grid_y = np.meshgrid(xs,ys)

        grid = griddata(xy, z, (grid_x, grid_y), method = 'cubic')

        mx, my = np.unravel_index(np.argmin(grid), grid.shape)

        cs = ax.imshow(grid.T, extent=(xs[0], xs[-1], ys[0],ys[-1]), origin='lower')
        ax.scatter(xs[mx],ys[my], marker='o', color='white', label = f"minimum at a={np.round(xs[mx],4)},{np.round(ys[my],4)}")
        ax.set_ylabel("scaling of paramter c")
        ax.set_xlabel("scaling of paramter a")
        ax.legend()

        if add_title:
            ax.set_title("Free energy for variations in lattice parameters")

        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.05)

        bar = plt.colorbar(cs, cax=cax)
        bar.set_label(f"{label} (Ry)")


    def report_min_of_3d(self, results):
        from scipy.interpolate import interpn
        xyz = np.array([[result.scale[0], result.scale[1], result.scale[2]] for result in results])
        v = [result.energy for result in results]

        xs = np.linspace(np.min(xyz[:,0]), np.max(xyz[:,0]), 100, endpoint=True)
        ys = np.linspace(np.min(xyz[:,1]), np.max(xyz[:,1]), 100, endpoint=True)
        zs = np.linspace(np.min(xyz[:,2]), np.max(xyz[:,2]), 100, endpoint=True)

        grid_x, grid_y, grid_z = np.meshgrid(xs,ys,zs) 
        
        interpolated = interpn(xyz,v, (grid_x,grid_y,grid_z))

        m = np.argmin(interpolated)

        print(f"minimum energy occurs at scaling {grid_x[m]},{grid_y[m]},{grid_z[m]}")
    
#Displace Atom
class EnergyDisplacementCurve(EnergyCurve):

    def __init__(self,opts):
        EnergyCurve.__init__(self,opts)
        self.atom_to_displace = self.opts.atom_to_displace
        self.direction_to_displace = self.opts.direction_to_displace

        assert self.atom_to_displace != "", "Must specify atom to displace"
        assert self.direction_to_displace != "", "Must specify direction to displace atom"

    def GetScalingOption(self):
        return self.opts.atom_displacements

    def GetReferenceScalings(self):
        return f"--reference-crystal-constant={0.95}"

    def GetAdditionalOptions(self, scale):
        return f" --atom-to-displace={self.atom_to_displace} --direction-to-displace={self.direction_to_displace} --atom-displacement={scale}"

    def GetX(self, results):
        return [result.scale for result in results], f"displacement of {self.atom_to_displace} in {self.direction_to_displace} (bohr rel. to exp.)"

    def IsVolumeLike(self):
        return True

#Stretch unit cell along a,b, and or c
class EnergyDistortionCurve(EnergyCurve):

    def GetScalingOption(self):
        return self.opts.lattice_scalings.split(",")

    def GetReferenceScalings(self):
        return f"--reference-lattice_scalings={self.scalings[0]}"

    def GetAdditionalOptions(self, scale):
        return f" --lattice-scaling={scale}"

    def IsSurfaceLike(self):
        return True

#expand/compress volume uniformly
class EnergyVolumeCurve(EnergyCurve):

    def GetScalingOption(self):
        return self.opts.volumes
        
    def GetReferenceScalings(self):
        return f"--reference-crystal-constant={self.scalings[0]}"

    def GetAdditionalOptions(self, scale):
        return f" -a{scale}"

    def GetX(self, results):
        return [result.volume for result in results], "volume"

    def GetSecondX(self, results):
        return [result.scale for result in results], "scale"

    def IsVolumeLike(self):
        return True

def EVCmain():
    parser = OptionParser(add_help=True) 
    opts = parser.parse_args(sys.argv[1:])

    if opts.continue_run or opts.recover or opts.collect or "dft" not in opts.program:
        fill_in_opts = parser.parse_args(sys.argv[1:])
        RefreshOptions(fill_in_opts)
        if fill_in_opts.lattice_scalings != "":
            opts.lattice_scalings = fill_in_opts.lattice_scalings
        if fill_in_opts.volumes != "":
            opts.volumes = fill_in_opts.volumes
        if fill_in_opts.temperatures != "":
            opts.temperatures = fill_in_opts.temperatures
        if fill_in_opts.atom_to_displace != "":
            opts.atom_to_displace = fill_in_opts.atom_to_displace
        if fill_in_opts.atom_displacements != "":
            opts.atom_displacements = fill_in_opts.atom_displacements
        #Code below causes problem if there are special characters in the options -- really want to figure out how to deal with these better
        #if opts.run_options == "":
        #    opts.run_options = f"{fill_in_opts.run_options}"
    else: 
        assert opts.temperatures != "", "Must supply temperature range"
    SaveOptions(opts)

    if opts.volumes != "":
        EC = EnergyVolumeCurve(opts)
    elif opts.lattice_scalings != "":
        EC = EnergyDistortionCurve(opts)
    elif opts.atom_displacements != "":
        EC = EnergyDisplacementCurve(opts)
    else:
        raise RuntimeError("Must specify some type of scaling (volume / scaling / displacement)")

    EC.organize()


if __name__ == '__main__':
    EVCmain()
