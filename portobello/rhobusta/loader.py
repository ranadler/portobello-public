from argparse import ArgumentDefaultsHelpFormatter
import numpy as np
from portobello.bus.Matrix import Matrix
from portobello.generated.PEScf import SolverState
from portobello.generated.self_energies import LocalOmegaFunction
from portobello.rhobusta.ProjectorEmbedder import KPathProjector
from portobello.rhobusta.multi_impurity import MULTI_IMPURITY_SINGLETON
from portobello.rhobusta.DFTPlugin import DFTPlugin
from pathlib import Path
from portobello.rhobusta.dmft import DMFTOrchastrator
from portobello.rhobusta.gutzwiller import Gutzwiller
from portobello.rhobusta.PEScfEngine import PEScfEngine, Ry2eV, InterpolatedSelfEnergy
from portobello.rhobusta.models.dmft import DMFTForModelsOrchastrator
from portobello.generated.MI import MultiImpuritySupervisor
from portobello.bus.mpi import MPIContainer
import importlib
from portobello.rhobusta.options import ArgumentParserThatStoresArgv, RefreshOptions
from portobello.generated import TransportOptics

class DFTLoader(PEScfEngine):
    def __init__(self,opts):
        RefreshOptions(opts)
        opts = self.FakeCorrelatedCalcIfNecessary(opts)
        PEScfEngine.__init__(self,opts)
    
    def GetMethodName(self):
        return "DFT"

    def LoadMyself(self):
        return True
        
    def StoreMyself(self):
        return False

    def FakeCorrelatedCalcIfNecessary(self, opts):
        if not hasattr(opts,'window'):
            w = input("Input window: (default = -10:10)")
            if w=="":
                w = "-10:10"
            opts.window=w
        if not hasattr(opts,'which_shell'):
            opts.which_shell=0
        if not hasattr(opts,'U'):
            opts.U=0
        if not hasattr(opts,'J'):
            opts.J=0
        if not hasattr(opts,'N0'):
            opts.N0=0
        if not hasattr(opts,'ising'):
            opts.ising = False
        if not hasattr(opts,'kanamori'):
            opts.kanamori = False
        return opts

    def ConstructQuasiparticleHamiltonian(self, proj, epsAllK, I, k, si):
        return np.diag(epsAllK[:,k,si] - self.mu * (1.0-self.MuChoice()))

    def GetQuasiparticleWeights(self, proj, uM, I, k, si):
        return np.diag(I)

    def GetQuasiparticleLifetimes(self, proj, uM, I, k, si):
        return np.diag(I)

    def LoadSigRealOmega(self, Nw=500, bound=0.6, interp_w=False):
        sig = LocalOmegaFunction(is_real=True,
                                 num_orbs=self.dim,
                                 num_si=self.num_si,
                                 num_omega = 2*Nw+1)
        sig.allocate()

        if hasattr(self.opts,"broadening"):
            broadening = self.opts.broadening
        else:
            broadening = 0.2
        
        self.DC[:,:,:] = 0.0 
        for si in range(self.num_si):
            for iomega in range(-Nw, Nw+1):
                w = iomega * bound / max(Nw,1) 
                
                sig.omega[iomega+Nw] = w
                if w < self.proj_energy_window.high and w > self.proj_energy_window.low:
                    sig.M[iomega+Nw, :,:,si] = -1j*broadening
        self.sig = sig

class AnalysisLoader(MPIContainer):
    def __init__(self, opts):
        self.opts = opts
        MPIContainer.__init__(self, self.opts)

    def LoadCalculation(self, loadRealSigma : bool = True):
        #if not self.opts.model or not Path("./ini.h5").exists():
        #    print("this program requires an ini.h5 file")
        #    exit(0)
            
        self.opts.mpi_workers = max(self.opts.mpi_workers,1)
        
        method = ""
        paths = ["./multi-impurity.h5", "./gutz.h5", "./dmft.h5"]
        for path in paths: 
            if Path(path).exists():
                method = path 
                break
        return self.LoadMethod(method, loadRealSigma)

    def LoadMethod(self, method, loadRealSigma):
        self.svs : PEScfEngine = None
        self.isDMFT = False
        if method == "./multi-impurity.h5":
            self.dftplugin = DFTPlugin.Instance(restart=True, mpi=self)
            mis = MultiImpuritySupervisor()
            mis.load(MULTI_IMPURITY_SINGLETON)
            if mis.ready:
                self.svs = GetSpecializedAnalyzer(mis, self.opts)
                if not loadRealSigma:
                    self.svs.impurity_delegator.LoadImpurityProblems(self.svs)
            methodName = "multi-impurity"
        elif method == "./dmft.h5":
            if hasattr(self.opts, "model") and self.opts.model:
                mpi = MPIContainer(self.opts)
                self.svs = DMFTForModelsOrchastrator(self.opts, mpi)
                self.dftplugin = self.svs.GetPlugin()
            else:
                DFTPlugin.SetGw()
                self.dftplugin = DFTPlugin.Instance(restart=True, mpi=self)
                self.svs = DMFTOrchastrator(self.opts)
            methodName = "dmft"
            self.isDMFT = True
        elif method == "./gutz.h5":
            self.dftplugin = DFTPlugin.Instance(restart=True, mpi=self)
            self.svs = Gutzwiller(self.opts, for_dft=True)
            methodName = "gutz"
        else:
            self.dftplugin = DFTPlugin.Instance(restart=True, mpi=self)
            self.svs = DFTLoader(self.opts)
            methodName = "dft"

        if loadRealSigma and self.svs is None:
            print("this program requires a valid computation (dmft or gutz.)")
            exit(0)
        
        # get the real self energy
        if loadRealSigma and self.svs is not None:
            self.svs.impurity_delegator.LoadSigRealOmega(self.svs, bound=self.opts.omega_window, interp_w=True, Nw=self.opts.Nw)

        return methodName

class PEScfLight(PEScfEngine, SolverState):
    def __init__(self, method, opts):
        SolverState.__init__(self)
        self.opts = opts
        self.which_shell = 0
        if self.opts.which_shell > 0: self.which_shell = self.opts.which_shell

class LightWeightLoader(AnalysisLoader):
    def LoadMethod(self, method, *args):
        self.svs = PEScfLight(method, self.opts)
        self.svs.load(method+":/")
        # in case of MI, load the requested shell run


def GetSpecializedAnalyzer(mis, opts):
    if mis.solver_class == "DMFTOrchastrator":
        return DMFT_MIA(opts)
    elif mis.solver_class == "Gutzwiller":
        return Gutz_MIA(opts)
    else:
        return DFTLoader(opts)

class MultiImpurityDelegator(MultiImpuritySupervisor):

    def __init__(self, opts):
        MultiImpuritySupervisor.__init__(self)
        self.load(MULTI_IMPURITY_SINGLETON)
        assert(self.ready)
        self.opts = opts

        mod = importlib.import_module(self.solver_module)
        self.method = getattr(mod, self.solver_class)

    def GetImpurityProblems(self, svs):
        return self.impurity_problems

    def GenerateKPathProjectors(self, svs, kpath):
        for i in range(self.num_impurities):
            self.impurity_problems[i].orbBandProj = KPathProjector(BZProjector=self.impurity_problems[i].orbBandProj, kpath=kpath, which_shell = i) 
        return self.impurity_problems[0].orbBandProj

    def LoadImpurityProblems(self, svs):
        self.impurity_problems = []
        for i in range(self.num_impurities):
            self.opts.which_shell=i
            problem = self.method(self.opts, for_dft=True)
            self.impurity_problems.append(problem)
            
    def LoadSigRealOmega(self, svs, Nw=250, bound=0.6, interp_w=False):

        self.impurity_problems = []
        for i in range(self.num_impurities):
            self.opts.which_shell=i
            problem = self.method(self.opts, for_dft=True)
            problem.LoadSigRealOmega(Nw=Nw, bound=bound, interp_w=interp_w)
    
            self.impurity_problems.append(problem)
            
        svs.sig = self.impurity_problems[0].sig # this is referenced for sizes but it should not actually used


    def SigmaMinusDC(self, svs, iomega):
        return [s.sig.M[iomega, :, :, :] - s.DC[:,:, :] for s in self.impurity_problems]

    def ComputeInverseGreensFunction(self, svs, w, epsAllK, muChoice, epsic, proj, MM, k, si):
        non_interacting = np.diag(w - epsAllK[:,k, si] + (1.0-muChoice)*svs.mu + epsic)
        embedded_sigma = np.zeros_like(non_interacting)
        for i, s in enumerate(self.impurity_problems):
            embedded_sigma += s.orbBandProj.EmbeddedIntoKBandWindow(MM[i], k, si)
        
        return non_interacting - embedded_sigma

    def ComputeMidEpsK(self, w, Hk, mu, epsi, k, si):
        # this is just the dft Gf inverse
        return w -  Hk[:,k,si] + mu + epsi
    
    
class DMFT_MIA(DMFTOrchastrator):

    def __init__(self, opts):
        DMFTOrchastrator.__init__(self,opts, for_dft=True)

    def GetImpurityDelegator(self):
        return MultiImpurityDelegator(self.opts)

    def StoreMyself(self):
        pass        
    
    def EmbedInterpolatedOnRealOmega(self, inp:TransportOptics.Input, debug):
        omegaListRy =[i*inp.delta  for i in range(-inp.max_omega_mesh, inp.max_omega_mesh+1)]
        
        self.impurity_delegator.LoadSigRealOmega(self, bound=inp.max_omega_mesh*inp.delta*Ry2eV+1.0)
        ese = TransportOptics.EmbeddedSelfEnergyOnRealOmega(
            min_band=self.orbBandProj.min_band,
            max_band=self.orbBandProj.max_band,
            shard_start = self.dft.shard_offset + 1, 
            shard_end = self.dft.shard_offset + self.dft.num_k,
            num_si=self.num_si,
            max_omega=inp.max_omega_mesh)
        ese.allocate()

        ese.omegaRy[:] = omegaListRy[:]
        
        for i, s in enumerate(self.impurity_delegator.impurity_problems):
            ise = InterpolatedSelfEnergy(s, mesh=ese.omegaRy[:]*Ry2eV)
            for iomega in range(2*inp.max_omega_mesh+1):
                MM = ise.GetMatrix(iomega) 
                for si in range(self.num_si):
                    for kidx in range(0, self.dft.num_k):
                        ese.M[:,:,iomega, kidx, si] += s.EmbeddedIntoKBandWindow(MM , kidx, si)
            for kidx in range(0, self.dft.num_k):
                ese.actual_k[kidx] = self.dft.shard_offset + kidx + 1 #1-based for fortran. This is for debug
        return ese

class Gutz_MIA(Gutzwiller):
    def __init__(self, opts):
        Gutzwiller.__init__(self, opts, for_dft=True)

    def GetImpurityDelegator(self):
        return MultiImpurityDelegator(self.opts)

    def StoreMyself(self):
        pass        
        
    def ConstructQuasiparticleHamiltonian(self, proj, epsAllK, I, k, si):
        Hk = np.diag(epsAllK[:,k, si]).astype(np.complex)
        
        for i, s in enumerate(self.impurity_delegator.impurity_problems):
            Hk -= s.orbBandProj.EmbeddedIntoKBandWindow(s.Hloc, k, si)

            if i == 0:
                ERminI = s.orbBandProj.EmbeddedIntoKBandWindow(s.R - self.ID, k, si)
                L =  Matrix(s.orbBandProj.EmbeddedIntoKBandWindow(self.Lambda, k, si))
            else:
                ERminI += s.orbBandProj.EmbeddedIntoKBandWindow(s.R - self.ID, k, si)
                L +=  Matrix(s.orbBandProj.EmbeddedIntoKBandWindow(self.Lambda, k, si))
                

        I = np.eye(self.num_bands, self.num_bands, dtype=np.complex128)
        R = Matrix(np.copy(I))
        R += ERminI

        return R * Hk * R.H + L - self.mu * (1.0-self.MuChoice()) * I
        
    def StoreMyself(self):
        return False

    def EmbedInterpolatedOnRealOmega(self, inp:TransportOptics.Input, debug):
        raise NotImplementedError
        
def AnalysisOptionsParser(add_help = False, private = True):
    parser = ArgumentParserThatStoresArgv('Common Analysis Options', add_help=add_help,
                            parents=[],
                            formatter_class=ArgumentDefaultsHelpFormatter)

    if private:
        parser.add_argument("-W", "--window",
                            dest="omega_window",
                            default=10.0,
                            type=float,
                            help = "omega energy window (-w to w) in eV") 

    parser.add_argument("-B", "--broadening",
                    dest="broadening",
                    default=0.05,
                    type=float,
                    help = "Broadening of spectral functions")

    parser.add_argument("--interpolation",
                    dest="interpolation",
                    default="bilinear",
                    help = "Type of interpolation used by matplotlib.imshow")
                    
    parser.add_argument("--color-map",
                    dest="cmap",
                    default="viridis",
                    help = "Color map used to plot the spectral function (see matplotlib for options)")

    parser.add_argument("--max-colors",
                    dest="max_colors",
                    default=20,
                    type=int,
                    help = "maximum number of colors to use. Set to lower number to group plots by color, and identify them by linestyle / marker")

    parser.add_argument("--linestyles", dest="linestyles", default="solid,dashed,dotted", help="linestyles to rotate through")
    parser.add_argument("--markers", dest="markers", default=",:o:^:<:v:>", help="markers to rotate through, : used as separator since comma means pixel")

    parser.add_argument("--observable", dest="obs", default="", 
                        help="""evaluate the observable, and use its unique eigenvalues to label observable-resolved plots
                                for example, --obs=Ylm will resolve along cubic-harmonic functions (all values of m), --obs=basis will plot the
                                the densities corresponding to the basis vectors (for example eg, t2g in cubic systems)""")

    parser.add_argument("--spacegroup-angle-tolerance", 
                          dest='spacegroup_angle_tolerance',
                          type=float,
                          default=5.0,
                          help="angle tolerance for spacegroup diagnosis")

    parser.add_argument("--which-shell", dest="which_shell", default=0, type=int,
                    help="which shell (index) to project to, in order to build the correlated problem")

    parser.add_argument("--from-real-axis", dest="from_real_axis",
                    action="store_true",
                    default=False,
                    help = "When constructing a quasiparticle hamiltonian, use Z's computed from the derivative of the real-axis self energy (dmft-only)")


    parser.add_argument(
                    "--num-high-energy-bands",
                    dest="num_high_energy_bands",
                    default=-1,
                    type=int,
                    help = "Number of bands above the correlated window to include")

    parser.add_argument(
                    "--nw",
                    dest="Nw",
                    default=250,
                    type=int,
                    help = "number of frequency points")

    parser.add_argument(
                    "--num-low-energy-bands",
                    dest="num_low_energy_bands",
                    default=-1,
                    type=int,
                    help = "Number of bands below the correlated window to include")
 

    parser.add_argument(
                    "--num-correlated-bands",
                    dest="num_correlated_bands",
                    default=-1,
                    type=int,
                    help = "Number of bands below in correlated window to include")

    parser.add_argument("-s", "--shells", dest="shells", default="",
                      help="list of comma separated shells given as in projector, e.g., Pu:5f,Pu:7s")


    parser.add_argument("--dont-sum-equivalents", dest="sum_across_equivalents", default=True, action="store_false",
                      help="don't sum observables across symmetry equivalent atoms")
    

    parser.add_argument("--wannier", dest="wannier", 
                    default=False,                           
                    action="store_true",
                    help="Use wannier orbitals rather than atomic orbitals as projectors")

    parser.add_argument("--model", dest="model", 
                    default=False,                           
                    action="store_true",
                    help="Analyze a model run rather than a DFT+X run")

    return parser