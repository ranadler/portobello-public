#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

'''
Created on Aug 21, 2018

@author: adler
'''


import matplotlib.pyplot as plt
from optparse import OptionParser
import sys
from portobello.generated.PEScf import SolverState
import os
import numpy as np
import pandas
from portobello.rhobusta.postprocess import GutzEnergyPlot, DrawNHistogramDMFT
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.rhobusta.observables import GetLabelsForPlotting
from mpl_toolkits.mplot3d import Axes3D
from portobello.rhobusta.simple_ed.basis import countBits

class HistogramPlots(SolverState):
    '''
    classdocs
    '''


    def __init__(self, version):
        '''
        Constructor
        '''
        SolverState.__init__(self)
            
        proj_location = "./projector.h5:/def/"
        proj = SelectedOrbitals()
        proj.load(proj_location)
    
        self.f = ""
        if os.path.exists("gutz.h5"):
            self.f = "gutz"
        elif os.path.exists("dmft.h5"):
            self.f = "dmft"
        else:
            raise Exception("no dmft.h5 or gutz.h5 file around")
            
        
        if version != '':
            nums = version.split(".")
            maj = int(nums[0])
            sub = int(nums[1])
            try:
                self.load(f"./{self.f}.h5:/history/{maj}/{sub}/")
            except:
                self.load(f"./history.h5:/{maj}/{sub}/")
        else:
            self.load(f"./{self.f}.h5:/")
                    
             
        if self.nrel > 1:
            obsv = "j"
        else:
            obsv = 'Ylm'
            
        labels, orbs = GetLabelsForPlotting(self, shell=proj.shells[0], opn=obsv, subspace_expr = proj.subspace_expr)
        labels=labels[0]
            
        def GetBitMask(label, labels):
            bm = 0  
            for i,label2 in enumerate(labels):
                if label == label2:
                    if self.nrel == 1 and self.num_si == 1:
      	                bm = (1 << i + len(labels)) | (1 << i) # up and down
                    else:
                        bm |= 1 << i 
            return bm
            
            
        self.bit_masks = { }
        for label in labels:
            self.bit_masks[label]=GetBitMask(label, labels)
            
        print(f"bit masks: {self.bit_masks}")
        
        self.full_dim = 2*self.dim
        if self.nrel == 2:
            self.full_dim = self.dim
            
        self.orbs = orbs
        
            
    def PlotHistograms(self, opts, fig, ax, axi, exi):
        
            
        self.Plot1(ax)
        self.Plot3(axi, fig)
        if self.f == 'gutz':
            try:
                GutzEnergyPlot(self, opts, exi)
            except:
                pass
        else:
            DrawNHistogramDMFT(self, opts, exi)
        
        
    def Plot1(self, ax):
        y = [0.0 for _i in range(self.full_dim+1)]
        
        for conf in range(self.hist.num_configs):
            N = self.hist.N[conf] 
            #print N, conf
            y[N] += self.hist.p[conf]
        
        #print y
        x=list(range(0,self.full_dim+1))
        
        ax.bar(x, y)
        ax.set(xlabel="N", ylabel="probability")
            
        ax.legend()
                    
       
    # plots a wheel of all the probabilities
    def Plot2(self, ax):

            
        self.df = pandas.DataFrame(
                    {'N':self.hist.N[:],
                     
                     'p':self.hist.p[:]})
        
        df1 = self.df.groupby(["N"],as_index=False)
        df3 = df1.sum().reset_index()['p']
        
        allp = []
        
        for i in range(self.full_dim+1):
            allp.extend(df1.get_group(1.0*i).sort_values(by=['p'])['p'].values)
        
        size=0.3
                                
        cmap = plt.get_cmap("tab20c")
        outer_colors = cmap(np.arange(3)*4)
        inner_colors = cmap(np.array([1, 2, 5, 6, 9, 10]))

        labels = [str("%d"%i) if df3.values[i]>0.01 else ""  for i in range(self.full_dim+1)]

        ax.pie(df3.values, radius=1, colors=outer_colors, labels=labels,
               wedgeprops=dict(width=size, edgecolor='w'))
                 
        ax.pie(allp, radius=1-size, colors=inner_colors,
               wedgeprops=dict(width=size, edgecolor='w',linewidth=0.2))
        
        ax.set(aspect="equal")

        
    def ToBits(self, a):
        ret = ""
        while a != 0:
            if a & 0x1:
                ret = "1"+ret 
            else:
                ret = "0"+ret 
            a = a >> 1
        ret = "0" *(self.dim - len(ret)) + ret
        return ret
        
        
    def Plot3(self, ax, fig):
        max_occupancy = 0
        print(f"bit masks:")
        for key in self.bit_masks.keys():
            print(f"{key}: {self.ToBits(self.bit_masks[key])}")
            max_occupancy = max(max_occupancy, countBits(self.bit_masks[key]))
        
        norbs = len(self.orbs)+1
        full_width=0.8
        width = full_width/norbs
        offsets = np.linspace(-(full_width-width)/2, (full_width-width)/2, norbs, endpoint = True)
        
        x = [[i+offset for i in range(max_occupancy+1)] for offset in offsets]
        z = [[0.0 for _i in range(max_occupancy+1)] for offset_ in offsets]
        
        avgN1ratio = [0.0 for _i in self.orbs]
        
        mp = 0.0
        
        for conf in range(self.hist.num_configs):
            p = self.hist.p[conf]
                
            bitsNum = countBits(conf)
            if p>mp:
                mp=p 
                nmax = bitsNum
                maxConfig = conf
                
            for j, orb in enumerate(self.orbs):
                if self.bit_masks[orb] & conf != 0 and p > 1.0e-11:
                    bits = self.bit_masks[orb] & conf
                    #assert(self.bit_masks[orb] & conf == self.bit_masks[orb]), (self.bit_masks[orb], self.bit_masks[orb] & conf)
                    cb = countBits(bits)
                    avgN1ratio[j] += p*cb/bitsNum
                    z[j][cb] += p
                    z[norbs-1][cb] += p
            
        print("Average occupation ratios:", avgN1ratio)
        for j, orb in enumerate(self.orbs):
            print(f"   {orb}:{avgN1ratio[j]}")
        
        print(f"Max Configuration: p={mp} N={nmax} state={self.ToBits(maxConfig)}")
        
        labels = [f"${orb}$" for orb in self.orbs] + ["total"]
        for j, label in enumerate(labels):
            ax.bar(x[j], z[j], width, label=label) 

        ax.axvline(-0.5,linestyle=":",color="gray")
        for j in range(max_occupancy+1):    
            ax.axvline(j+0.5,linestyle=":",color="gray")

        ax.set_xticks(range(max_occupancy+1))
        ax.set_xticklabels(range(max_occupancy+1))

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        ax.legend(loc='center left', bbox_to_anchor=(1.02, 0.5))
        
        
def main():
    
    parser = OptionParser()    
        
    parser.add_option("-v", "--versions", dest="versions", default='',
                      type=None, help="version list, such as 1.1, 2.4 etc. separated by ,")
 
    parser.add_option("-w", dest="width",
                      type=float,
                      default=0.2,
                      help="width of histogram bins in eV")
    
    (opts, _args) = parser.parse_args(sys.argv[1:])
    
    versions = opts.versions.split(",")

    fig = plt.figure(figsize=(12, 20))
    subplot1 = fig.add_subplot(3,len(versions), 1)
    subplot2 = fig.add_subplot(3,len(versions), 2)
    subplot3 = fig.add_subplot(3,len(versions), 3)
    plots = [subplot1, subplot2, subplot3]
    
    #fig, axis = plt.subplots(3, len(versions), sharey='row', figsize=(12, 20))
    
    #plt2 =fig.add_subplot(4,len(versions), 4, projection='3d')

    for i, version in enumerate(versions):
        
        #if len(versions) == 1:
        #    plots = axis
        #else:
        #    plots = axis[:,i]
        
        if version != '':
            plots[0].set_title("$Histogram [\#{%s}]$"%(version))
        else:
            plots[0].set_title("$Histogram [{current}]$")
            
        
        sp = HistogramPlots(version=version)
        sp.PlotHistograms(opts, fig,  *plots)
        
    plt.show(block=True)
        
if __name__ == '__main__':
    main()
    
        

