#!/usr/bin/python

'''
Created on Jul 25, 2018

@author: adler
'''
from optparse import OptionParser
import sys

if __name__ == '__main__':
    print("in TestMpi4py")

    parser = OptionParser()    
    parser.add_option("-p", "--mpi-slots", dest="p", default=8, type=int, help="num processes")

    (opts, _args) = parser.parse_args(sys.argv[1:])
    
    from mpi4py.futures._lib import get_comm_world
    comm = get_comm_world()
    size = comm.Get_size()
    rank = comm.Get_rank()
    print("my size: %d rank %d"% (size, rank))
    if rank == 0 and size == 1:
        if opts.p == 1:
            print("Done here.")
        else:
            spn = comm.Spawn("/usr/bin/python",
                            args=['-m', 'portobello.rhobusta.TestMpi4py'],
                            maxprocs=opts.p-1)
    