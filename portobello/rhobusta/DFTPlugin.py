'''
Created on Aug 13, 2018

@author: adler
'''

# DFT code that it invokes.
from portobello.generated.FlapwMBPT import Input
from portobello.generated.FlapwMBPT_interface import DFT
from portobello.generated.LAPW import Basis, BandStructure, \
    BasisMTInfo, KPath
from portobello.generated import LAPW, FlapwMBPT_interface
from portobello.generated.TransportOptics import Velocities
from pymatgen.core.structure import Structure
from portobello.bus.persistence import Persistent
from portobello.bus.mpi import MPIContainer

class DFTPlugin(object):
    '''
    classdocs
    '''
    
    _plugin = None
    _Gw = False

    @classmethod
    def IsInitialized(cls):
        return cls._plugin is not None

    @classmethod
    def SetGw(cls):
        cls._Gw = True
    
    @classmethod
    def IsGw(cls):
        return cls._Gw

    @classmethod
    def Clear(cls):
        if cls._plugin is not None:
            DFT().DeallocateAll("")
            DFT().CloseHdf5("")
            del cls._plugin
            cls._plugin = None
            cls._Gw = False
       
    @classmethod
    def Instance(cls, restart=True, mpi : MPIContainer=None):
        """
        Note the number of shards is determined in a smart way - 
        if the actual number of running processes is 1, it will be 1, despite
        a value mpi_workers > 1
        """
        if mpi is None:
            # the caller is relying on the instance being present: you cannot shard the dft
            # without providing an explicit number of shards, this should have been done at the
            # top level of the program, where the number of shards was evident
             if cls._plugin is None:
                 raise Exception("""
                programming error: DFTPlugin.Instance() should have been called earlier
                in a place where the number of shards was known - typically at the top level of 
                the running program.""")

        if cls._plugin is None:
            cls._plugin = DFTPlugin(cls._Gw, restart, mpi)
            
        # _Gw cannot change in the same process - it has to be set from the start
        assert(cls._Gw == cls._plugin.Gw)
        return cls._plugin


    def __init__(self, Gw,  restart,  mpi):
        '''
        Constructor
        '''
        self.lapw_basis_index = 0
        self.band_index = 0
        self.Gw = Gw
        opts = ""
        if self.Gw:
            opts += "+Gw"
        if restart:
            opts += ",restart"
        self.dft = DFT()
        self.mpi = mpi
        if mpi.worker:
            num_shards = mpi.num_workers
        else:
            num_shards = 1
        self.dft.ConnectFlapwMBPT("", "./ini.h5:/", num_shards, opts)

    def Reload(self): 
        # make sure we are reading the up-to-date file
        Persistent.Refresh("./flapw_image.h5:/")
        self.dft.Reload("")
        return

    def GetMPIInterface(self):
        return self.mpi.GetMPIInterface()

    def IsMaster(self):
        return self.mpi.IsMaster()

    def GetDFTStuff(self): 
        if self.dft.num_k_all == 0:
            loc = "./dft.core.h5:/"
            self.dft.GetDFTInfo(loc)
            self.dft.load(loc)
        return self.dft
    
    def GetConvergence(self):
        loc =  "./dft.core.h5:/convergence/"
        self.dft.GetConvergence(loc)
        cvg = FlapwMBPT_interface.Convergence()
        cvg.load(loc)
        return cvg
        
    def GetEquivalentAtomSymmetries(self):
        loc = "./lapw.core.h5:/equivAtomSymmetries"
        eas = LAPW.EquivalentAtomSymmetries()
        self.dft.GetEquivalentAtomSymmetries(loc)
        eas.load(loc)
        return eas.g
    
    def GetLAPWBasis(self):
        loc = f"./lapw.core.h5:/lapw"# % self.lapw_basis_index
        basis = Basis()
        #print "- getting lapw basis at", loc
        self.dft.GetLAPWBasis(loc)
        #print "- after getting lapw basis"
        basis.load(loc)
        return basis  
    
    def GetBasisMTInfoImp(self):
        loc = "./MTInfo.h5:/base/"
        self.dft.GetBasisMTInfo(loc)
        bmti = BasisMTInfo()
        bmti.load(loc)
        return bmti
    
    def GetAllBandsForKpoint(self, k):
        bs = BandStructure()
        bs.load(self.dft.GetPartialBandStructure(
                f"./bands-{self.mpi.GetRank()}.core.h5:/full/k={k}",
                1,
                0,
                k+1))
        return bs
    
    def GetBandsForKpoint(self, k, min_band, max_band):
        bs = BandStructure()
        num_bands = max_band - min_band + 1
        bs.load(self.dft.GetPartialBandStructure(
                f"./bands-{self.mpi.GetRank()}.core.h5:/partial_per_k/num_bands={num_bands}/",
                min_band+1,
                max_band+1,
                k+1))
        return bs
    
    def GetStructure(self):
        dft = self.GetDFTStuff()
        species = [int(dft.st.Z[idx-1]) for idx in dft.st.distinct_number]
        # obtain a pymatgen structure from a Kutepov structure
        
        st = Structure([dft.st.a, dft.st.b, dft.st.c], 
                       species, [dft.st.xyz[:,i] for i in range(dft.st.num_atoms)], coords_are_cartesian=True)

        return st

    # this one only reads the ini file
    @staticmethod
    def GetStructureFromIni(dir=""):
        """ note that the measurements here are in bohr """
        ini = Input()
        ini.load(f"./{dir}/ini.h5:/")
        species = [int(ini.strct.ad[ini.strct.isA[i]-1].z) for i in range(ini.strct.natom)]
    
        st = Structure([ini.strct.a[:], ini.strct.b[:], ini.strct.c[:]], 
                       species, 
                       [ini.strct.tau[:,i] for i in range(ini.strct.natom)], 
                       coords_are_cartesian=False)
        return st
    
    def GetBandsForAllKpoints(self, min_band, max_band):
        bs = BandStructure()
        num_bands = max_band - min_band + 1
        bs.load(self.dft.GetPartialBandStructure(
                f"./bands-{self.mpi.GetRank()}.core.h5:/partial/num_bands={num_bands}",
                min_band+1,
                max_band+1,
                -1))
        return bs

    def GetBandsAndGradientsForAllKpoints(self, min_band, max_band):
        bs = BandStructure()
        grad = Velocities()
        num_bands = max_band - min_band + 1
        gradloc = f"./velocities-{self.mpi.GetRank()}.core.h5:/partial/num_bands={num_bands}"
        bs.load(self.dft.GetPartialBandStructureAndGradient(
                f"./bands-{self.mpi.GetRank()}.core.h5:/partial/num_bands={num_bands}",
                gradloc,
                min_band+1,
                max_band+1,
                -1))
        grad.load(gradloc)
        return bs, grad
    
    # here min, max bands should be in Python convention
    def GetBandsForKPath(self, kpath : KPath, min_band, max_band):
        kpathloc = "./kpath.core.h5:/len=%d"%kpath.num_k
        kpath.store(kpathloc)
        bs = BandStructure()
        num_bands = max_band - min_band + 1
        bs.load(self.dft.GetKpathBands(
                f"./bands-{self.mpi.GetRank()}.core.h5:/kpath/num_bands={num_bands}",
                kpathloc,
                min_band+1,
                max_band+1))
        return bs
        
    def GetBandsAndGradientForKPath(self, kpath : KPath, min_band, max_band):
        kpathloc = "./kpath.core.h5:/len=%d"%kpath.num_k
        kpath.store(kpathloc)
        num_bands = max_band - min_band + 1
        bs = BandStructure()
        grad = Velocities()
        gradloc = f"./velocities-{self.mpi.GetRank()}.core.h5:/kpath/num_bands={num_bands}"
        bs.load(self.dft.GetKPathBandsAndGradient(
                f"./bands-{self.mpi.GetRank()}.core.h5:/kpath/num_bands={num_bands}",
                gradloc,
                kpathloc,
                min_band+1,
                max_band+1))
        grad.load(gradloc)
        print(max_band-min_band)
        return bs, grad
        
    
    def CreatePlots(self):
        self.dft.CreatePlots("")
        
    def Disconnect(self):
        self.dft.Disconnect("")
