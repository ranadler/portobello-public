'''
Created on Jun 12, 2019

@author: adler@physics.rutgers.edu
'''
import unittest
from portobello.rhobusta.DFTPlugin import DFTPlugin
import shutil
import os
from portobello.rhobusta.testing.TestRhobustaDFTInput import CreateInitFile

class TestRhobustaDFTInput3(unittest.TestCase):
    @classmethod
    def setUp(cls):
        cls.FeSe = CreateInitFile(cls, "./FeSe-ICSD-163559.cif")

    @classmethod
    def tearDownClass(cls):
        os.chdir(cls.curr_dir)
        shutil.rmtree(cls.test_dir)
        DFTPlugin.Instance().Disconnect()
        
    def testFeSe(self):
        
        self.assertEqual(self.FeSe.strct.nsort,2)
        adFe = self.FeSe.strct.ad[0]
        
        # make sure the radii are good
        self.assertGreater(adFe.smt,2.3)
        
        # make sure the lmb (maxL) is 7,6 at least
        self.assertGreaterEqual(adFe.lmb, 8)
        
        self.assertEqual(sum(adFe.correlated[:,2]), 1)
        self.assertEqual(sum(sum(adFe.correlated[:,:])), 1)
        
        maxK = 0
        maxK = max(maxK, adFe.lmb/adFe.smt)
        self.assertGreater(maxK, 3.0)
        self.assertLess(maxK, 3.45)
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']

    unittest.main()