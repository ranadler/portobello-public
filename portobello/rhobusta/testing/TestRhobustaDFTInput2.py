'''
Created on Jun 12, 2019

@author: adler@physics.rutgers.edu
'''
import unittest
from portobello.rhobusta.testing.TestRhobustaDFTInput import CreateInitFile
import os
import shutil
from portobello.rhobusta.DFTPlugin import DFTPlugin

        
class TestRhobustaDFTInput2(unittest.TestCase):
    
    @classmethod
    def setUp(cls):
        #cls.FeSe = cls.CreateInitFile("./FeSe-ICSD-163559.cif")
        cls.Fe = CreateInitFile(cls, "./Fe-ICSD.cif")

    @classmethod
    def tearDownClass(cls):
        os.chdir(cls.curr_dir)
        shutil.rmtree(cls.test_dir)
        DFTPlugin.Instance().Disconnect()
        
    def testFe(self):
        
        self.assertEqual(self.Fe.strct.nsort,1)
        adFe = self.Fe.strct.ad[0]
        
        # make sure the radii are good
        self.assertGreater(adFe.smt,2.3)
        
        # make sure the lmb (maxL) is 7,6 at least
        self.assertGreaterEqual(adFe.lmb, 8)
        
        self.assertEqual(sum(adFe.correlated[:,2]), 1)
        self.assertEqual(sum(sum(adFe.correlated[:,:])), 1)
        
        maxK = 0
        maxK = max(maxK, adFe.lmb/adFe.smt)
        self.assertGreater(maxK, 3.0)
        self.assertLess(maxK, 3.45)
        

        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']

    unittest.main()