'''
Created on Jun 12, 2019

@author: adler@physics.rutgers.edu
'''
import unittest
import os
import tempfile
from portobello.rhobusta import rhobusta
import shutil
from portobello.generated import FlapwMBPT
import numpy as np
from portobello.rhobusta.DFTPlugin import DFTPlugin


def CreateInitFile(cls, fn):
    cls.test_dir = tempfile.mkdtemp()
    os.system("cp %s %s"% (fn, cls.test_dir))
    cls.curr_dir = os.path.abspath(".")
    os.chdir(cls.test_dir)
    rhobusta.DFT(fn, kMeshFactor=0.0001, num_charge_iterations=1)
    ini = FlapwMBPT.Input()
    ini.load("./ini.h5:/")
    
    return ini
# Note: only one material may be created in one process, since the persistence
# engine cannot handle multiple "ini.h5" files.
class TestRhobustaDFTInput(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        cls.MnO = CreateInitFile(cls, "./MnO.struct")

    @classmethod
    def tearDownClass(cls):
        os.chdir(cls.curr_dir)
        shutil.rmtree(cls.test_dir)
        DFTPlugin.Instance().Disconnect()

    def testMnOSymm(self):
        self.assertEqual(self.MnO.symm.num_ops, 48)
        # this is a symmomorphic group, no shifts should be there
        self.assertTrue(np.array_equiv(self.MnO.symm.shifts[:,:], 
                        np.zeros((3,self.MnO.symm.num_ops), np.float64)))
        
        
    def testAtomicData(self):
        self.assertEqual(self.MnO.strct.nsort,2)
        adMn = self.MnO.strct.ad[0]
        adO = self.MnO.strct.ad[1]
        
        # make sure the radii are good
        self.assertGreater(adMn.smt,2.41)
        self.assertGreater(adO.smt,1.78)
        
        # make sure the lmb (maxL) is 7,6 at least
        self.assertGreaterEqual(adMn.lmb, 7)
        self.assertGreaterEqual(adO.lmb, 6)
        
        self.assertEqual(sum(sum(adO.correlated[:,:])), 0)
        self.assertEqual(sum(adMn.correlated[:,2]), 1)
        self.assertEqual(sum(sum(adMn.correlated[:,:])), 1)
        
        maxK = 0
        maxK = max(maxK, adMn.lmb/adMn.smt)
        maxK = max(maxK, adO.lmb/adO.smt)
        self.assertGreater(maxK, 3.0)
        self.assertLess(maxK, 3.4)
 
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']

    unittest.main()