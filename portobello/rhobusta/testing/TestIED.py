'''
Created on Jun 12, 2019

@author: adler@physics.rutgers.edu
'''
import unittest
from portobello.rhobusta.g.ga_complex_fermiT import RunEDXInParallel
from portobello.generated import edx
from cmath import sqrt
import numpy as np
from portobello.symmetry.general import PrintMatrix

class Test(unittest.TestCase):


    def setUp(self):
        None
        
    def tearDown(self):
        None
        
    def testFromPuFile(self):
        setup = edx.Setup()
        inp = edx.Input()
        setup.load("./edx_Pu.h5:/setup/")
        inp.load("./edx_Pu.h5:/input/")

        setup.ncv = 20
        setup.ed_solver = 2

        setup.store("./edx.h5:/setup/", flush=True)
        inp.store("./edx.h5:/input/", flush=True)

        M = np.zeros((28,28), dtype=np.complex128)
        for i in range(inp.hopping.len):
            M[inp.hopping.i1[i]-1, inp.hopping.i2[i]-1] = inp.hopping.value[i]
        PrintMatrix(np.real(M), max_line_width=400, precision=3)
        

        RunEDXInParallel(2)
        
            
        out = edx.Output()
        out.load("./edx_out.h5:/", disconnect=True)
        
        print("Converged:", out.is_valid)
        if out.is_valid:
            print("Eigenvalue: ", out.E[:])
            v = out.eigvecs[:,0]
            norm1 = sqrt(abs(np.vdot(v,v))) 
            print("norm is:", norm1)
            if np.real(norm1) > 1.0e-10:
                v =  v / norm1
                for i in len(v):
                    if abs(v[i]) > 1.e-2:
                        print(i, v[i])
            else:
                print("eigenvector is zero")
                
    def testFromPuFile2(self):
        self.testFromPuFile()
                
                
    def NOTtestDiagonalHopping(self):
        setup = edx.Setup(num_fock = 1024)
        setup.num_val_orbs=10
        setup.allocate()
        setup.fock[:] = list(range(1024))
        
        setup.coulomb = edx.Tensor4(len=0)
        setup.coulomb.allocate()
        
        setup.ed_solver = 2
        setup.neval = 1
        setup.nvector = 1
        setup.ncv = 3
        setup.eigval_tol = 1.0e-6
        setup.maxiter = 500
        
        setup.hopping_rep = edx.Tensor2(len=10)
        setup.hopping_rep.allocate()
        
        for i in range(10):
            setup.hopping_rep.i1[i] = i + 1
            setup.hopping_rep.i2[i] = i + 1
            setup.hopping_rep.value[i] = (i+1)/1024.0
            
        setup.store("./edx.h5:/setup/", flush=True)
        
        inp = edx.Input()
        inp.hopping = setup.hopping_rep
        
        inp.store("./edx.h5:/input/", flush=True)
        
        RunEDXInParallel(2)
        
            
        out = edx.Output()
        out.load("./edx_out.h5:/", disconnect=True)
        
        print(out.E[:])
        v = out.eigvecs[:,0]
        v =  v/ sqrt(np.vdot(v,v)) 
        for i in range(1024):
            if abs(v[i]) > 1.e-8:
                print(i, v[i])
    

    def TestFromFeFile(self):
        import h5py
        from portobello.rhobusta.simple_ed.ed import simple_ed
        from portobello.rhobusta.simple_ed.basis import table_es

        f = h5py.File('EMBED_HAMIL_1.h5', 'r')
        D = f['/D'][...].T
        E = f['/H1E'][...].T
        no2 = f['/na2'][...][0]
        LAM = f['/LAMBDA'][...].T
        #U = f['/V2E'] [...].T
        #LAM = np.zeros(E.shape)
        #D = np.zeros(LAM.shape)
        U = np.zeros((no2,no2,no2,no2))
        f.close()
        
        print('Testing Fe with simple_ed')
        
        # run simple_ed
        #sed = simple_ed(2*no2, True, True, False, True, np.complex) 
        sed = simple_ed(2*no2, True, True, False, False, np.complex) 
        sed.build_two_body(U)
        sed.build_one_body(E,D,-LAM)
        sed.build_Ham(s2pen=0.0,debug=True)
        sed.diagonalize()
        print('gs energy=', np.loadtxt('EMBED_ENE.dat',dtype=complex)[:,0])
        #denMat = sed.compute_denmat()
        #print 'density matrix='
        #print denMat

        # run ied
        #h1e = get_whole_h1e(E, LAM, D)
        from scipy.linalg import block_diag
        h1e = block_diag(E, -LAM)
        h1e[:no2, no2:] = D.T
        h1e[no2:, :no2] = D.conj()
        print('h1e=')
        print(h1e)
        
        fock_base = table_es(2*no2,no2,0)
        print('size of Fock space:', len(fock_base))
                
        setup = edx.Setup(num_fock = len(fock_base))
        setup.num_val_orbs=2*no2
        setup.allocate()
        fock_base.sort()
        setup.fock[:] = fock_base
        
        setup.coulomb = edx.Tensor4(len = np.sum(np.abs(U) > 1e-12))
        setup.coulomb.allocate()
        idx = 0
        for (i1,i2,i3,i4), val in np.ndenumerate(U):
            if np.abs(val) > 1e-12:
                setup.coulomb.i1[idx] = i1 + 1
                setup.coulomb.i2[idx] = i2 + 1
                setup.coulomb.i3[idx] = i3 + 1 
                setup.coulomb.i4[idx] = i4 + 1
                setup.coulomb.value[idx] = val
                idx+=1
        
        setup.ed_solver = 2#2
        setup.neval = 2
        setup.nvector = 2
        setup.ncv = 10
        setup.eigval_tol = 1.0e-9
        setup.maxiter = 1000
        
        iid, jid = np.nonzero(h1e)
        print(iid)
        print(jid)
        
        setup.hopping_rep = edx.Tensor2(len=len(iid))
        setup.hopping_rep.allocate()
        
        for i in range(len(iid)):
            setup.hopping_rep.i1[i] = iid[i] + 1
            setup.hopping_rep.i2[i] = jid[i] + 1
            setup.hopping_rep.value[i] = h1e[iid[i],jid[i]]
            
        setup.store("./edx.h5:/setup/", flush=True)
        
        inp = edx.Input()
        inp.hopping = setup.hopping_rep
        
        inp.store("./edx.h5:/input/", flush=True)
        
        RunEDXInParallel(2)
        
            
        out = edx.Output()
        out.load("./edx_out.h5:/", disconnect=True)
        
        print(out.E[:])
     

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']

    unittest.main()
