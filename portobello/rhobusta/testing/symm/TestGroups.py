'''
Created on Jan 16, 2020

@author: adler
'''
import unittest
import numpy as np
from portobello.symmetry.characters import AbstractGroup, DoubleCover,\
    ConjClasses, Irreps, PrintMatrix
from portobello.symmetry.general import GenerateGroup
from portobello.bus.Matrix import Matrix
from cmath import pi
import math

class MatrixGroup(AbstractGroup):
    def I(self):
        return np.eye(3, dtype=np.complex128)
    def Inversion(self):
        return -np.eye(3, dtype=np.complex128)
    def Hx(self):
        m = self.I()
        m[0,0] = -1.0
        return m
    def Hy(self):
        m = self.I()
        m[1,1] = -1.0
        return m
    def Hz(self):
        m = self.I()
        m[2,2] = -1.0
        return m
    
    
    def R2_110(self):
        m = self.I()
        m[0,0] = 0.0
        m[1,1] = 0.0
        m[0,1] = 1.0
        m[1,0] = 1.0
        m[2,2] = -1.0
        return m
    
    def R2_010(self):
        m = self.I()
        m[0,0] = -1.0
        m[2,2] = -1.0
        return m
    
    def R2_001(self):
        m = self.I()
        m[0,0] = -1.0
        m[1,1] = -1.0
        return m
    
    def R2_100(self):
        m = self.I()
        m[1,1] = -1.0
        m[2,2] = -1.0
        return m
    
    
    def R4_001(self):
        m = self.I()
        m[0,0] = 0.0
        m[1,1] = 0.0
        m[0,1] = -1.0
        m[1,0] = 1.0
        return m

    def R6_001(self):
        m = self.I()
        cs = math.cos(2.0*pi/6)
        sn = math.sin(2.0*pi/6)
        m[0,0] = cs
        m[1,1] = cs
        m[0,1] = -sn
        m[1,0] = sn
        return m
    
    
    def __init__(self, matrix_gens):
        gens = GenerateGroup([Matrix(g) for g in matrix_gens])
        AbstractGroup.__init__(self, gens)
        
    def Print(self):
        print("group matrices::")
        for i, g in enumerate(self.o3matrices):
            print("(%d)"%i)
            PrintMatrix(g)
        AbstractGroup.Print(self)
    
    
class CiGroup(MatrixGroup):
    
    def __init__(self):
        gen = [ self.Inversion()]
        MatrixGroup.__init__(self, gen)
   

class C2hGroup(MatrixGroup):
    
    def __init__(self):
        gen = [ self.Inversion(), self.Hy()]
        MatrixGroup.__init__(self, gen)
               


class C2vGroup(MatrixGroup):
    
    def __init__(self):
        gen = [ self.Hx(), self.Hy()]
        MatrixGroup.__init__(self, gen)

class D4Group(MatrixGroup):
    
    def __init__(self):
        gen = [ self.R2_010(), 
                self.R2_001(), self.R2_110(),
                self.R4_001()]
        MatrixGroup.__init__(self, gen)

class D4hGroup(MatrixGroup):
    
    def __init__(self):
        gen = [ self.Inversion(), 
                self.R2_010(), self.R2_001(),
                self.R4_001()]
        MatrixGroup.__init__(self, gen)

class C4Group(MatrixGroup):
    def __init__(self):
        gen = [ self.R4_001()]
        MatrixGroup.__init__(self, gen)

class C6Group(MatrixGroup):
    def __init__(self):
        gen = [ self.R6_001()]
        MatrixGroup.__init__(self, gen)



class D2hGroup(MatrixGroup):
    def __init__(self):
        gen = [ self.Inversion(), self.R2_100(), self.R2_010(), self.R2_001()]
        MatrixGroup.__init__(self, gen)




class TestGroups(unittest.TestCase):


    def setUp(self):
        pass
        
    def tearDown(self):
        pass

    def CheckIrrs(self, irrs):
        # check that all rows are orthogonal
        for i in range(2,irrs.ncc):
            for j in range(i+1):
                dp = np.vdot(irrs.chiG[i,:], irrs.chiG[j,:])
                if i!=j and abs(dp) > 1.0e-5:
                    print("irreps %i %j are not orthogonal" % (i,j))
                    assert(False)
                if i==j:
                    assert(abs(dp -irrs.group.N) < 1.0e-6), "squares should sum to N"


    def testC2v(self):
        self.C2v = C2vGroup()
        assert(self.C2v.N == 4)
        cc =ConjClasses(self.C2v)
        assert(cc.num_cc == 4), cc.num_cc
        irrs = Irreps(self.C2v)
        assert(irrs.chi.shape == (4,4)),irrs.chi.shape
        self.CheckIrrs(irrs)
        

    def testDoubleC2v(self):
        self.C2v = C2vGroup()
        dc = DoubleCover(self.C2v.o3matrices)
        cc =ConjClasses(dc)
        assert(cc.num_cc == 5), cc.num_cc
        irrs = Irreps(dc)
        self.CheckIrrs(irrs)
        
        
        
        
    def testD4(self):
        self.D4 = D4Group()
        assert(self.D4.N == 8)
        cc =ConjClasses(self.D4)
        assert(cc.num_cc == 5), cc.num_cc
        irrs = Irreps(self.D4)
        self.CheckIrrs(irrs)
        
        
    def testDoubleD4(self):
        self.D4 = D4Group()
        dc = DoubleCover(self.D4.o3matrices)
        cc = ConjClasses(dc)
        irrs = Irreps(dc)
        self.CheckIrrs(irrs)
        
        assert(cc.num_cc == 7), cc.num_cc


    def testD4h(self):
        self.D4h = D4hGroup()
        assert(self.D4h.N == 16)
        cc =ConjClasses(self.D4h)
        assert(cc.num_cc == 10), cc.num_cc
        irrs = Irreps(self.D4h)
        self.CheckIrrs(irrs)
        
        
    def testDoubleD4h(self):
        print("***** testing double D4h *****")
        self.D4h = D4hGroup()
        dc = DoubleCover(self.D4h.o3matrices)
        cc = ConjClasses(dc)
        irrs = Irreps(dc)
        self.CheckIrrs(irrs)
        
        assert(cc.num_cc == 14), cc.num_cc 

        
    def testCi(self):
        self.Ci = CiGroup()
        assert(self.Ci.N ==2)
        cc =ConjClasses(self.Ci)
        assert(cc.num_cc == 2), cc.num_cc
        dc = DoubleCover(self.Ci.o3matrices)
        dcc = ConjClasses(dc)
        assert(dcc.num_cc == 4), dcc.num_cc
        irrs = Irreps(dc)
        self.CheckIrrs(irrs)
        
        
    def testDoubleC2h(self):
        print("***** testing C2h *****")
        self.C2h = C2hGroup()
        assert(self.C2h.N == 4)
        dc = DoubleCover(self.C2h.o3matrices)
        cc =ConjClasses(dc)
        irrs = Irreps(dc)
        self.CheckIrrs(irrs)
        
        assert(cc.num_cc == 8), cc.num_cc
    
    def testC4AndDouble(self):
        print("***** testing C4 *****")
        self.C4 = C4Group()
        assert(self.C4.N == 4)
        dc = DoubleCover(self.C4.o3matrices)
        cc =ConjClasses(dc)
        irrs = Irreps(dc)
        self.CheckIrrs(irrs)
        
        assert(cc.num_cc == 8), cc.num_cc
        
    def testC6AndDouble(self):
        print("***** testing C6 *****")
        self.C6 = C6Group()
        assert(self.C6.N == 6)
        dc = DoubleCover(self.C6.o3matrices)
        cc =ConjClasses(dc)
        irrs = Irreps(dc)
        self.CheckIrrs(irrs)
        assert(cc.num_cc == 12), cc.num_cc
        
        
    def testD2hAndDouble(self):
        print("***** testing C6 *****")
        self.D2h = D2hGroup()
        assert(self.D2h.N == 8)
        dc = DoubleCover(self.D2h.o3matrices)
        cc =ConjClasses(dc)
        irrs = Irreps(dc)
        self.CheckIrrs(irrs)
        assert(cc.num_cc == 10), cc.num_cc
        
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()