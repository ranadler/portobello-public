'''
Created on Jun 12, 2019

@author: adler@physics.rutgers.edu
'''
import unittest
import os
import tempfile
from portobello.rhobusta import rhobusta
import shutil
from portobello.generated import FlapwMBPT

class Test(unittest.TestCase):


    def setUp(self):
        self.Initialize("./MnO.struct")
        
    def Initialize(self, fn):
        self.test_dir = tempfile.mkdtemp()
        os.system("cp %s %s"% (fn, self.test_dir))
        curr_dir = os.path.abspath(".")
        os.chdir(self.test_dir)
        rhobusta.DFT(fn, kMeshFactor=0.0001, num_charge_iterations=20)

        
    def tearDown(self):
        shutil.rmtree(self.test_dir)


    def testName(self):
        pass


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']

    unittest.main()