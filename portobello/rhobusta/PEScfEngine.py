#!/usr/bin/env python3

'''
Created on Dec 13, 2018

@author: adler@physics.rutgers.edu

This is a common ancestor for methods like DMFT and Slave-Bosons, which defines
the projection and embedding into local orbitals.

'''

import os
import abc
import cmath
from portobello.rhobusta.MatrixParameterization import HermitianSymmetrizer, NonHermitianSymmetrizer, RawHermitianSymmetrizer
import sys
from argparse import ArgumentDefaultsHelpFormatter
from collections import defaultdict
from itertools import product
from math import ceil, exp
from typing import List
import scipy

import numpy as np
from portobello.bus.Matrix import Matrix, ZeroComplexMatrix
from portobello.generated import TransportOptics
from portobello.generated.base_types import EmptyObject, Window
from portobello.generated.PEScf import Histogram, SolverState
from portobello.generated.atomic_orbitals import OrbitalsBandProjector, SelectedOrbitals
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.g.utils import U_matrix_slater, get_average_uj, U_matrix_kanamori
from portobello.rhobusta.observables import (GetAllObservables,
                                             GetLabelsForPlotting)

from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.ProjectorEmbedder import KPathProjector, Projector, CorrelatedSubspaceProjector, GetSuffix
from portobello.generated.LAPW import BandStructure
from typing import Any
from pathlib import Path
from portobello.rhobusta.wannier import WannierProjector, WannierRunner

from portobello.symmetry.local import GetCGMatrix, GetRealSHMatrix
from pymatgen.core.composition import Composition

Ry2eV = 27.2107*0.5  # from Kutepov, don't change unless it changes there

def floatOrString(x):
    try:
        return float(x)
    except:
        return x

class AnyFloat(object):
    def __init__(self, name, other_choices):
        self.name = name
        self.other_choices = other_choices
    def __eq__(self, other):
        try:
            return isinstance(float(other), float)
        except:
            if len(self.other_choices):
                print(f"Arugment to {self.name}, {other}, is not a floating point number or in {self.other_choices}")
            else:
                print(f"Arugment to {self.name}, {other}, is not a floating point number")
            quit()
    
    def __str__(self):
        return "[float]"
    
    

def GetPEScfOptionsParser(filebase, prepArgs, add_help=False, private=False):

    prepArgParser = ArgumentParserThatStoresArgv(add_help=False)
    # read off the --which-shell argument
    prepArgParser.add_argument("--which-shell", dest="which_shell", default=-1, type=int)
    prepOpts, _unparsed = prepArgParser.parse_known_args(prepArgs)
    which_shell = prepOpts.which_shell
    if which_shell < 0: which_shell = 0
    required = True
    if Path(f"{filebase}{which_shell}.h5").exists() or Path(f"{filebase}.h5").exists():
        required = False

    parser = ArgumentParserThatStoresArgv('projector-embedder', add_help=add_help ,
                            parents=[GetMPIProxyOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
           
    tg = parser.add_mutually_exclusive_group(required=required)
    
    tg.add_argument("-b", "--beta", dest="beta", type=float, default=None)

    tg.add_argument("-T", "--Temperature", dest="Temperature", default=None,
                    type=int, help="Temperature in Kelvin")

    mp = parser.add_argument_group(title="model parameters",
                              description="physical parameters of the local (many-body) Hamiltonian")
    
    mp.add_argument("-U", "--U_eV", dest="U", 
                    required=required,
                    type=floatOrString, help=
                    """U [eV]. For multi-impurity problems with different atoms or shells, value applies to all impurities
                    if user supplies a single number, otherwise user may supply comma seperated list of the form [atom].[l]:[value],
                     where l is in s,p,d,f and value is a number, e.g., Fe.d:10,Pu.f:5
                    """)
    mp.add_argument("-J", "--J_eV", dest="J", 
                    required=required, 
                    type=floatOrString, help="J [eV]. See -U/--U_eV for a description of the possible formats.")
    mp.add_argument("-N", "--N0", dest="N0", 
                    required=required,
                    type=floatOrString, help=""" See -U for a description of the possible formats. Here, the value in [atom].[l]:[value]
                      or in the unadorned [value] which applies to all impurities is 
                      either a number (float, int), or 3 numbers separated by colon (:).
                      If a number, it is just the occupation used for double counting.
                      (Gutzwiller) the 1st and last number designate a range of occupations which should
                      be used for the fock space, and the middle number should be the double counting occupation as above.
                      For example 1:3:5 means the double counting N0=3, and the range 1,2,3,4,5 will be used for construction
                      of the fock space.
                      (DMFT) 1st number is not used, last number is used to restrict the maximum size of operator matrices. 
                      Use with care: i.e., only if large sectors of the Hilbert space are not going to be visited and memory runs out in CTQMC.""")
    
    dc_choices = ['FLL', 'scFLL', 'HF', 'Held']
    mp.add_argument("-d", "--DC", dest="double_counting_method", default="FLL",
                    choices = dc_choices + [AnyFloat("--DC", dc_choices)],
                    help="""FLL: DC fixed to the initial parameterization.
                          scFLL: DC updated to current number of electrons in impurity.
                          HF: DC updated to Hartree-Fock self-energy in the impurity solution (DMFT only).
                          [float]: Set the DC to the given number""")
  
    mp.add_argument("-p", "--initial-polarization", dest="initial_spin_polarization",
                    default=0.0, type=float, 
                    help="""polarize the first double counting using Mz multiplied by this number [eV].
                            In this case it may also be useful to break the local symmetry in the same direction,
                            using --break-local-symmetry
                         """)

    mp.add_argument("--dc-term", dest="dc_term",
                    default="", type=str, 
                    help="""add this term, expressed in the observables in observables.py to the DC - for example, can add
                            '-0.5*jj' to decrease the spin orbit interaction, or '0.2*Mx+0.3*My-Mz' to change magnetism
                         """)  
    
    mp.add_argument("--dc-admix", dest="double_counting_admix", 
                    default=0.2, 
                    type=float, 
                    help="admix for double counting if -d is set to -2 (varying double counting)")
    
    mp.add_argument("--interaction-truncation", dest="interaction_truncation", 
                    default=0.0, 
                    type=float, help="values of U below this in [eV] will be truncated to 0.0. Usually useful only for --full-interaction.")
           
    parser.add_argument("--which-shell", dest="which_shell", default=-1, type=int,
                    help="""which shell (index) to project to, in order to build the correlated problem. Note that -1 does not mean that this is 
                    a single-impurity - it merely makes it possible to say that "all" impurities should be considered. The file suffix of gutz or dmft
                    will be determined based on comparing the dim of its correlated subspace to the full dimension of the projector. Note that for 
                    the gutzwiller of dmft computation, -1 is meaningless, so it is set to 0.
                    """)

    parser.add_argument("--recalc-window-bands", dest="recalc_window_bands", 
                    default=False,                           
                    action="store_true",
                    help="force recalculation of window band numbers, even if this is not the 1st iteration in the super-iter")
    
    parser.add_argument("--wannier", dest="wannier", 
                    default=False,                           
                    action="store_true",
                    help="Use wannier orbitals rather than atomic orbitals as projectors")

    parser.add_argument("--ising", dest="ising",
                        default=False,
                        action="store_true",
                        help="use ising interaction, not full (Slater-Condon) U.")

    parser.add_argument("--kanamori", dest="kanamori",
                        default=False,
                        action="store_true",
                        help="use kanamori interaction, not Slater-Condon.")

    
    return parser
    
 
# interface for the visitor
class GreensFunctionVisitor():
    def GetProjector(self) -> Projector:
        return None

    def passWeight(self) -> bool:
        return True

    def GetGlocProjector(self) -> Projector:
        return None

    def NonCorrelatedWidening(self) -> float:
        return 0.05
    
    def CorrelatedWidening(self) -> float:
        return 0.005

    def GetLabel(self) -> str:
        return ""
        
    def GetLowBands(self) -> BandStructure:
        return None

    def GetHighBands(self) -> BandStructure:
        return None

    def GetMidBands(self,proj) -> BandStructure:
        return proj.GetBandStructureInWindow()

    def StartKPointsPerOmega(self,iomega:int, w :float):
        None

    # interface for visiting through the greens function visitor.
    # note that this is visiting each k,si,omega point once where k \in IRR (in the shard)
    def VisitKOmegaPoint(self, k:int, iomega:int, w:float, si:int, 
            Gk, gradients,
            lowEnergyGkDos:float,
            negativeOmegaDos:float, lowOmegaDos:float, positiveOmegaDos:float,
            weight:float, num_atoms:int):
        None
    
    # interface for visiting through the Band Structure visitor, which prepares the QP Hamiltonian,
    # note that this is visiting each k,si point once where k \in IRR  (in the shard)
    def VisitKPoint(self, k, si, Hqp,
            lowEpsK, midEpsK, highEpsK, gradients, lifetimes, qp_weights,
            weight, num_atoms):
        None 

    def EndKPointsPerOmega(self,iomega:int, w:float):
        None
    def End(self):
        None
            
class SingleImpurityDelegator:
    
    def LoadImpurityProblems(self, svs):
        pass
    
    def GetImpurityProblems(self,svs):
        return [svs]

    def LoadSigRealOmega(self, svs, Nw=450, bound=0.6, interp_w=False):
        svs.LoadSigRealOmega(bound=bound, interp_w=interp_w, Nw=Nw)

    def SigmaMinusDC(self, svs, iomega):
        return svs.sig.M[iomega, :, :, :] - svs.DC[:,:, :] 

    def ComputeInverseGreensFunction(self, svs, w, epsAllK, muChoice, epsic, proj, MM, k, si):
        return np.diag(w - epsAllK[:,k, si] + (1.0-muChoice)*svs.mu + epsic) -\
                        proj.EmbeddedIntoKBandWindow(MM ,k,si)
    
    def ComputeMidEpsK(self, w, epsAllK, mu, epsi, k, si):
        return w -  epsAllK[:,k,si] + mu + epsi

    def GenerateKPathProjectors(self, svs, kpath):
        svs.orbBandProj = KPathProjector(BZProjector=svs.orbBandProj, kpath=kpath) 
        return svs.orbBandProj

class PEScfEngine(SolverState):
    '''
    Common base-class for DMFT and Gutzwiller, containing the basic utitlities
    of correlated calculations - common code and data definitions.
    '''
    
    @abc.abstractmethod
    def LoadMyself(self):
        pass
        
    @abc.abstractmethod
    def StoreMyself(self):
        pass        
        
    @abc.abstractmethod
    def StoreStateInHistory(self):
        pass
                             
    # this code is invoked in the Kohn-Sham loop in Rhobusta
    # when it is run with --plus-sigma
    @abc.abstractmethod
    def EmbedSigma(self, location, ignore_state=False, retain_result=False):
        pass
        
    # This enforces the input convention for N0
    @staticmethod
    def GetN0(N0str):
        if isinstance(N0str, int) or isinstance(N0str, float):
            return N0str    
        N0lst = N0str.split(":")
        if len(N0lst) == 1:
            return float(N0lst[0])
        elif len(N0lst) == 2:
            # as default take the average in the range, if this is a range
            return np.average(list(range(int(N0lst[0]), int(N0lst[-1]+1))))
        else:
            # in this case the middle number is N0, and the extremes are used to build the fock space
            assert(len(N0lst) == 3)
            return float(N0lst[1])
           
    @staticmethod
    def GetNRange(N0str): 
        if isinstance(N0str, int) or isinstance(N0str, float):
            return None    
        N0lst = N0str.split(":")
        if len(N0lst) == 1:
            return None
        elif len(N0lst) >= 2:
            # as default take the average in the range, if this is a range
            return int(N0lst[0]), ceil(float(N0lst[-1]))

    def GetValueForAtom(self, s):
        """
        s = (a) single value, e.g., 6, which applies to all atoms
            (b) list of values for atoms, e.g., U:6,Fe:3
            (c) list of values for atom & shell, e.g., U.f:6,U.d:4
            Assumes no one is getting values for different principle 
            quantum numbers on the same atom & angular momenta (e.g., U.6f and U.5f)
        """
        if not isinstance(s, str):
            return s
        st = self.plugin.GetStructure()
        atom_number = self.corrSubshell.atom_number
        sp = str(st.species[atom_number])
        lmap = {'s':0, 'p':1, 'd':2, 'f':3}
        for ip in s.split(","):
            ipl = ip.lstrip()
            if ipl[0].isalpha():  # first character has to be alphabetic for an element
                el,*rest = ipl.split(":")
                el=el.split('.')
                lmatch = True
                if len(el)==2:
                    l = lmap[el[-1]]
                    lmatch = l == self.corrSubshell.l
                    el = el[:-1][0]
                else:
                    el = el[0]
                if el == sp and lmatch:
                    return floatOrString(":".join(rest))
            else:
                return ipl
        raise Exception(f"option value for atomic element {sp} not specified in {s}")

    def GetImpurityDelegator(self):
        return SingleImpurityDelegator()

    def __init__(self, opts):
        '''
        Constructor
        '''
        SolverState.__init__(self)
        self.opts = opts # this is a transient object
        self.plugin = DFTPlugin.Instance()
        # dft, apw are non-persistent, needed in the construction of persistent fields
        self.dft = self.plugin.GetDFTStuff()
        self.lapw = self.plugin.GetLAPWBasis()

        self.impurity_delegator = self.GetImpurityDelegator()
        self.real_axis_sig = False
        
        if hasattr(opts, "wannier") and opts.wannier:
            orbitals_location = WannierRunner.orbitals_location
        else:
            orbitals_location = "./projector.h5:/def/"
        if hasattr(opts, 'projector'):
            orbitals_location = opts.projector
    
        self.which_shell = opts.which_shell
        if self.which_shell < 0:
            self.which_shell = 0 # the default for correlated subspace projector is the first shell
            
        is_new = self.LoadMyself()        
        if is_new:
            print(" - creating tracking file")
                    
            KB = 6.333328e-6 *27.2107* 0.5 # collected from Kutepov's code, to match perfectly
            
            if opts.Temperature is not None and opts.beta is None:
                opts.beta = 1/(opts.Temperature * KB)
            if opts.Temperature is None:
                opts.Temperature = 1/opts.beta / KB
                
            assert(opts.Temperature is not None)
                    
            ewin = [float(f) for f in opts.window.split(":")]
            self.proj_energy_window = Window(low=ewin[0],high=ewin[1])

            # the band projector is calculated every time we construct this
            # object - it is not persisted

            self.orbBandProj = self.GetOrbBandProj(orbitals_location)
            self.corrSubshell = self.orbBandProj.shell
          
            self.dim = self.GetDim()
            #if hasattr(opts, 'spin_sym'):
            #    self.num_si = 1 if opts.spin_sym else 2
            #else:
            self.num_si = self.dft.num_si
            self.allocate()
            self.U = self.GetValueForAtom(opts.U)
            self.J = self.GetValueForAtom(opts.J)
            self.beta = opts.beta
            self.Nimp = self.GetN0(self.GetValueForAtom(opts.N0))
            
            self.ising = False
            if hasattr(opts, 'ising'):
                self.ising = opts.ising
                
            self.kanamori = False
            if hasattr(opts, 'kanamori'):
                self.ising = opts.kanamori
            
            self.Nlatt = self.Nimp
            self.rep = self.corrSubshell.rep
            self.num_unique_orbs = len(PEScfEngine.GetRepOrbitals(self.rep))
            self.num_equivalents = self.orbBandProj.shell.num_equivalents
            self.num_anti_equivalents = self.orbBandProj.shell.num_anti_equivalents
            #if self.num_anti_equivalents: assert(self.num_si > 1 or os.path.exists("./multi-impurity.h5")) 
                # can't do AFM with no spin polarization, unless this is fake (just a multi impurity)
                # but it is too early to check consistency here
            self.full_dim = self.dim*2
            if self.dft.nrel == 2:
                self.full_dim = self.dim
            self.nrel = self.dft.nrel
            self.hist = Histogram(num_configs=2**self.full_dim)
            self.hist.allocate()
            self.RhoImp[:,:,:] = self.ID * self.Nimp / self.full_dim 
            nrange = self.GetNRange(self.GetValueForAtom(opts.N0))
            if nrange is not None:
                self.NRange.low, self.NRange.high = nrange
            
            if hasattr(opts, 'initial_spin_polarization') and opts.initial_spin_polarization is not  None:
                self.initial_spin_polarization = opts.initial_spin_polarization
                if hasattr(opts, 'moment'):
                    self.moment = opts.moment
                else:
                    self.moment = '1.0' # interpreted as along Mz
          
            # note that this option is transient, so it needs to be set on every iteration if it is 
            # to be used
            if hasattr(opts, 'double_counting_method') and opts.double_counting_method is not None:
                self.double_counting_method = opts.double_counting_method
                
                if opts.double_counting_method in ["FLL", "scFLL", "HF"]:
                    self.nominal_dc = self.FLLDoubleCounting(self.Nimp, self.Nimp/2.0)
                    print("- initializing nominal double counting with %f" % self.nominal_dc)

                elif opts.double_counting_method == 'Held':
                    self.nominal_dc = self.HeldDoubleCounting(self.Nimp)

                else:
                    self.nominal_dc = float(opts.double_counting_method)
                    print("- initializing nominal double counting from input --DC: %f" % self.nominal_dc)

            # at this point dmftState.sig = zero self energy
        else:  
            # here we create the rest of the non-persistent fields
            bands_hint = self
            if self.sub_iter <2:
                bands_hint = None
            if hasattr(opts, 'recalc_window_bands') and opts.recalc_window_bands:
                bands_hint = None

            self.orbBandProj = self.GetOrbBandProj(orbitals_location,bands_hint)
            self.corrSubshell = self.orbBandProj.shell
            rep = self.corrSubshell.rep
            if np.any(np.abs(rep-self.rep) > 1.0e-3):
                self.rep = rep
                self.num_unique_orbs = len(PEScfEngine.GetRepOrbitals(self.rep))
                if self.plugin.IsMaster():
                    print("- refreshing representation from projector, to:")
                    Print(rep)
                    self.StoreMyself()
                # wait for the matrix to be stored
                if self.plugin.dft.is_sharded:
                    MPI=self.plugin.GetMPIInterface()
                    MPI.COMM_WORLD.Barrier()

        self.num_corr_bands = self.orbBandProj.max_band - self.orbBandProj.min_band + 1
        self.window_min_band = self.orbBandProj.min_band-1
        self.window_max_band = self.orbBandProj.max_band-1
        self.InitSymmetrization()

        self.norb2 = self.dim * 2
        if self.dft.nrel == 2:
            self.norb2 = self.dim
        
        self.fixed_DC = False if hasattr(opts, "double_counting_method") and opts.double_counting_method in ["scFLL", "HF"] else True

        return is_new

    def GetOrbBandProj(self, orbitals_location, bands_hint = None):
        if hasattr(self.opts, "wannier") and self.opts.wannier:
            orbBandProj = WannierProjector(orbitals_location,
                                                        self.proj_energy_window,  self.dft, self.opts, which_shell=self.which_shell)
        else:
            orbBandProj = CorrelatedSubspaceProjector(orbitals_location, 
                                                        self.proj_energy_window, which_shell=self.which_shell, bands_hint=bands_hint)
        return orbBandProj

    def GetDim(self): #For clusters...
        return self.corrSubshell.dim

        
    def InitSymmetrization(self):
        """ initialize local variables that are used to interface with hermitian, and non-hermitian symmetrizers
        """
        self.HermitianSymmetrizerObj = HermitianSymmetrizer(corrObject=self)
        self.NonHermitianSymmetrizerObj = NonHermitianSymmetrizer(corrObject=self)
        self.RawHermitianSymmetrizerObj = RawHermitianSymmetrizer(corrObject=self)
        
    # create zero complex local matrix, which has spin as well
    def ZeroLocalMatrix(self, isComplex=True):
        t = np.float64
        if isComplex:
            t = np.complex128
        return np.zeros((self.dim, self.dim,  self.num_si), t)
           
    @property
    def ID1(self):
        I = np.eye(self.dim, dtype=np.complex128)
        return I
        
    @property
    def ID1F(self):
        large_dim = self.orbBandProj.max_band - self.orbBandProj.min_band + 1 
        I = np.eye(large_dim, dtype=np.complex128)
        return I
      
    @property
    def ID(self):
        ret = self.ZeroLocalMatrix()
        for si in range(self.num_si):
            ret[:,:,si] = self.ID1[:,:]
        return ret
    
    
    
    def all_k_si_pairs(self):
        return product(list(range(self.dft.num_k_all)), list(range(self.num_si)))
    
    def irr_k_si_pairs(self):
        return product(list(range(self.dft.num_k_irr)), list(range(self.num_si)))
        
    def shard_k_si_pairs(self):
        return product(list(range(self.dft.num_k)), list(range(self.num_si)))
        
    def ferm(self, e): # we call this to act on a vector
        # TODO: should we multiply by nspin (1 or 2) ??
        def FF(E):
            bE = self.beta * E  # note that mu is set to zero in the QP Greens function 
            if np.iscomplex(bE):
                if bE < -100.0:
                    return 1.0
                if bE > 100.0:
                    return 0.0
                return 1.0/(1.0 + exp(bE))
            else:
                if np.real(bE) < -100.0:
                    return 1.0
                return 1.0/(1.0 + np.exp(bE))
 
        return [FF(ei) for ei in e]  # either 0 or 1
    
    def OneAtomProjector(self, k, si):
        return self.orbBandProj.OneAtomProjector(k, si)
    
    def MuChoice(self):
        return 1.0

    def SymmetrizeCorrelated(self, M : np.ndarray, hermitian:bool, raw:bool=False):
        """one entry-point symmetrization for the correlated space, which hides the complexity 
           of setting up the underlying symmetrizers.

        Args:
            M (np.ndarray): matrix (2 dimensional) or 3 dimensional matrix+spin index
            hermitian (bool): is the matrix symmetrized as hermitian
            raw (bool, optional): is the raw representation used (the one before approximation), or not (use the approximation)
        """
        if hermitian:
            if not raw:
                self.HermitianSymmetrizerObj.Symmetrize(M)
            else:
                self.RawHermitianSymmetrizerObj.Symmetrize(M)
        else:
            self.NonHermitianSymmetrizerObj.Symmetrize(M)
    
    def InitOneBody(self):        
        if self.plugin.IsMaster():
            print(" - correlated bands: %d to %d for energy range: %3.3f to %3.3f" % (
            self.orbBandProj.min_band-1, self.orbBandProj.max_band-1, 
            self.proj_energy_window.low, self.proj_energy_window.high))
        
        if self.plugin.IsMaster():
            print(" - num of k points: %d, num of irreducible k points: %d" %(self.dft.num_k_all,self.dft.num_k_irr))
        # first get energy diagonals for all k, and calculate Eimp
        self.Elocal =  self.ZeroLocalMatrix()
        self.occ = self.ZeroLocalMatrix()
        testI = self.ZeroLocalMatrix()
        
        self.energyBands = 0.0
        
        muChoice = self.MuChoice()
        KS = self.orbBandProj.GetBandStructureInWindow() # in the projection range of bands       
        self.epsAllK = (KS.energy[:,:,:]  - KS.chemical_potential*muChoice )*Ry2eV
        is_sharded = KS.is_sharded

        I = np.eye(self.num_corr_bands, dtype=np.complex128)

        for k, si in self.shard_k_si_pairs():    
            Pk = self.OneAtomProjector(k, si)
            self.Elocal[:,:,si] += Pk.H * Matrix(np.diag(self.epsAllK[:,k, si])) * Pk * self.lapw.weight[k]
            
            # note: self.occ will only be valid after set_g (second iteration) because we don't persist
            # g_full (yet)
            #if (k < self.dft.num_k_irr):  # this is not the density matrix, but the trace gives the density
            self.occ[:,:,si]  += Pk.H * Matrix(KS.Occ[:,:,k, si]) * Pk * self.lapw.weight[k]
            self.energyBands += np.real(np.dot(np.diag(KS.Occ[:,:,k, si]), self.epsAllK[:,k,si])) * self.lapw.weight[k]
            
            testI[:,:,si]  += Pk.H * Pk * self.lapw.weight[k]
            
            # ALL these numbers should be the same for every k, use the last one
            if k == 0 and si == 0:    
                self.mu = KS.chemical_potential * Ry2eV
        del KS
        if is_sharded:
            MPI=self.plugin.GetMPIInterface()
            MPI.COMM_WORLD.Barrier()
            testI            = MPI.COMM_WORLD.allreduce(testI, op=MPI.SUM)
            self.Elocal      = MPI.COMM_WORLD.allreduce(self.Elocal, op=MPI.SUM)
            self.occ         = MPI.COMM_WORLD.allreduce(self.occ, op=MPI.SUM)
            self.energyBands = MPI.COMM_WORLD.allreduce(self.energyBands, op=MPI.SUM)
                
        self.SymmetrizeCorrelated(testI, hermitian=True, raw=True)
        for si in range(self.dft.num_si):
            if np.any(abs(testI[:,:,si] - self.ID1) > 0.0001) and self.plugin.IsMaster():
                print("DMFT: testing the projector, it is not properly normalized - Hyb will be bad!")
                print(abs(testI[:,:,si] - self.ID1))
                break
               
        # since we now sum over the irr BZ, some off-diagonals will not cance, which should
        # This is taken care of by symmetrization.
        # Here we also use orbitalSymm=True, which averages all the entries which are supposed to be the same - 
        # this is a choice that can be changed in the options
        self.SymmetrizeCorrelated(self.Elocal, hermitian=True, raw=True)
         
        if self.plugin.IsMaster():
            print("- local H:")
            print(Matrix(self.Elocal[:,:,0]))
    
    def TransformU(self, U, T):
        """ Note that T is the transformation from the simple basis (left) to the more adapted basis (right)
        """
        return np.einsum("Aa,Bb,abcd,cC,dD", T.H, T.H, U, T, T)

    def AddSpinIndex(self, UnoS):
        Ufullmat = np.zeros((self.norb2, self.norb2, self.norb2, self.norb2), dtype=np.complex128)
        dim = self.norb2 // 2
        Ufullmat[:dim, :dim, :dim, :dim] = UnoS  # up, up
        Ufullmat[dim:, dim:, dim:, dim:] = UnoS  # dn, dn
        Ufullmat[:dim, dim:, dim:, :dim] = UnoS  # up, dn
        Ufullmat[dim:, :dim, :dim, dim:] = UnoS  # dn, up  
        return Ufullmat
        
    def GetUMatrix(self, factor = 1):
        #one_atom_dim = self.corrSubshell.basis.shape[0] // self.corrSubshell.cluster_size 
        #one_atom_dim_ss = self.dim // self.corrSubshell.cluster_size #ss=subspace
        basis = Matrix(self.corrSubshell.basis)
        
        if self.corrSubshell.cluster_size == 1:
            return self.GetUMatrixOneAtom(factor,self.ising,basis)

        if self.nrel !=2:
            Ufull = np.zeros((self.dim, self.dim, self.dim, self.dim), dtype=np.complex128)
        else:
            Ufull = np.zeros((self.norb2, self.norb2, self.norb2, self.norb2), dtype=np.complex128)

        one_atom_dim = self.dim // self.corrSubshell.cluster_size #this is the dimension of the subspace
        for i in range(self.corrSubshell.cluster_size):
            transform = self.corrSubshell.rotations[i]
            Uperatom = self.GetUMatrixOneAtom(factor, self.ising, transform, add_spin_index = False)

            sl = slice(i*one_atom_dim, (i+1)*one_atom_dim)
            Ufull[sl,sl,sl,sl] = Uperatom[:,:,:,:]

        Ufull = self.TransformU(Ufull, basis)

        if self.nrel !=2:
            Ufull = self.AddSpinIndex(Ufull)


        count = np.sum(abs(Ufull) > self.opts.interaction_truncation)
        print("*** number of nonzero elements in U tensor:", count)
        return Ufull

    def GetUMatrixOneAtom(self, factor, ising, basis, add_spin_index = True):
        if hasattr(self,"kanamori") and self.kanamori: 
            Umat = U_matrix_kanamori(self.dim, U_int=self.U, J_hund=self.J)
            Ufullmat = self.AddSpinIndex(Umat)
        else:  
            l = self.orbBandProj.l

            # the form of this tensor is the normal form: (see for example in triqs)
            #              C1+ C2+ C2 C1 
            #  where + is a short for dagger
            #  and 1,2 are the indices of the 2 spins (all have their own m indices)
            #
            Umat_spher = U_matrix_slater(l, radial_integrals=None, U_int=self.U, J_hund=self.J)

            if self.dft.nrel != 2:          
                # transform to cubic and then to general basis
                # In non-rel case, the transformation matrix has no spin indices, and therefore
                # does not alter the spins in the tensor, and UFullmat retains the same structure
                # as umat_spher
                T = GetRealSHMatrix(l).H * basis
                # C+ operators (b,d) transform as T, C operator transformas T.H
                Umat = self.TransformU(Umat_spher, T)   
                if add_spin_index:         
                    Ufullmat = self.AddSpinIndex(Umat)         
                else:
                    Ufullmat = Umat
            else:
                Ufullmat0 = self.AddSpinIndex(Umat_spher)
            
                # Here we can't transform *before* creating the full matrix, because the indices are mixed
                # we set the bigger matrix so to have down/up blocks (4 of them), which are transformed by the CGMatrix(l)
                
                T = GetCGMatrix(l) * basis # transform to the native basis (J) and then use the special basis (if not I)
                Ufullmat = self.TransformU(Ufullmat0, T)

        u_avg_cubic, j_avg_cubic = get_average_uj(Ufullmat)
        print('U_avg_cubic(transformed)=', u_avg_cubic, 'J_avg_cubic=(transformed)', j_avg_cubic)

        Ufullmat *= factor
    
        count = np.sum(abs(Ufullmat) > self.opts.interaction_truncation)
        print("*** number of nonzero elements in U tensor:", count)

        # note that ISING-type interaction depends on the basis (the full U does not, up to change of basis) 
        count2 = 0
        if ising:
            for ii, u in np.ndenumerate(Ufullmat):
                # indices 0,3 and indices 1,2 are the same electron - here we make sure they have the same, or opposite index
                if ii[0] == ii[1] and ii[1] == ii[2] and ii[2] == ii[3]:
                    continue
                if ii[0] == ii[3] and ii[1] == ii[2]:
                    if abs(u) > 1.0e-10:
                        count2 += 1 
                    continue
                Ufullmat[ii] = 0.0
            print("*** number of nonzero elements in U ising tensor:", count2)

        return Ufullmat

    def Print(self, M,precision=4):
        if np.any(abs(np.imag(M)) > 1.0e-6):
            print(np.array_str(M, precision=precision, max_line_width=180, suppress_small=True))
        else:
            print(np.array_str(np.real(M), precision=precision, max_line_width=180, suppress_small=True))

    def SetLatticeOccupation(self):
        self.Nlatt =  sum(self.occ.trace().real)  # sum over spins
        if self.plugin.IsMaster():
            print(" - Nlattice: %f, Nimp: %f" % (self.Nlatt, self.Nimp))
        sys.stdout.flush()
    
    def FLLDoubleCounting(self, N, Ns):
        return  self.U*(N -0.5) - self.J*(Ns - 0.5)

    def HeldDoubleCounting(self,N):
        dim = self.dim
        if self.nrel==1 and self.num_si:
            dim *= 2
        print(N, dim)

        num = self.U + (dim - 1)*(self.U - 2*self.J) + (dim - 1)*(self.U - 3*self.J)
        return num*(N-0.5)/(2*dim - 1)
    
    # this can be overriden by sub-classes
    def DoubleCounting(self, is_new=False):
        
        if self.fixed_DC:
            return self.nominal_dc
    
        if is_new:
            return self.nominal_dc
    
        Nimp = np.trace(self.RhoImp[:,:,0]).real
        if self.num_si > 1:
            Nimp += np.trace(self.RhoImp[:,:,1]).real
        elif self.nrel < 2:
            Nimp *= 2.0  # single spin: need to double
        fll = self.FLLDoubleCounting(Nimp, Nimp/2.0)
        print(f"- updated FLL double counting {fll} corresponds to Nimp={Nimp}")
        return fll
    
    
    # note that there is no reordering of spins - patrick's down is first, like ours - there is noly a layout change
    def PatrickFormatToRhobusta(self, M):
        ret = self.ZeroLocalMatrix(isComplex=True)

        if self.num_si == 2:
            ret[:,:,0] = M[self.dim:,self.dim:]
            ret[:,:,1] = M[:self.dim,:self.dim]
        else:
            if self.nrel == 1:
                ret[:,:,0] = M[:self.dim,:self.dim]
            else:
                ret[:,:,0] = M[:,:]
        return ret
            
    def UpdateDoubleCounting(self, is_new=False):
        dc = self.DoubleCounting(is_new)
        if self.plugin.IsMaster(): print(" - DC=", dc)
        if self.fixed_DC or is_new:
            # just to prevent numeric errors .. in the fixed case this should be the same as below
            # because DoubleCounting() already handles the fixed case
            self.DC[...] =  dc * self.ID

        elif self.double_counting_method == "HF":
            admix = self.opts.double_counting_admix
            if self.sig.moments is not None:
                self.DC[...] = (1.0-admix) * self.DC[...]  + admix * self.sig.moments[0,...]
                if self.plugin.IsMaster(): 
                    print(" - Hartree-Fock DC=")
                    print(self.DC)
            else:
                self.DC[...] =  dc * self.ID

        elif  self.double_counting_method == "scFLL":
            admix = self.opts.double_counting_admix
            self.DC[...] = (1.0-admix) * self.DC[...]  + admix * dc * self.ID

        if hasattr(self.opts, 'dc_term') and self.opts.dc_term != "":
            dcMatrix = GetAllObservables(self.corrSubshell).evaluate(self.opts.dc_term)
            self.DC[...] += self.PatrickFormatToRhobusta(dcMatrix)
        
        # initial polarization, if the variable is set
        OPERATOR = "M"
        if hasattr(self, 'moment') and self.super_iter == 1 \
            and abs(self.initial_spin_polarization) > 1.0e-4:
            mag = None
            if self.moment is not None and self.moment != "":
                v = self.moment.split(",")
                if len(v) == 1:
                    mag = [0.0, 0.0, float(v[0])]
                else:
                    mag = [float(vi) for vi in v]
            else:
                mag = [0,0,1]

            obs = GetAllObservables(self.corrSubshell)
            for i,ax in enumerate(["x","y","z"]):
                Pi = self.PatrickFormatToRhobusta(obs.Get(OPERATOR+ax))
                self.DC[...] -= Pi * mag[i] * self.initial_spin_polarization
                #print(f" - Initial polarization matrix (before multiplying by {self.initial_spin_polarization}):")
                #print(Pax)
          
    # note self.rep needs to be set
    @staticmethod
    def GetRepOrbitals(rep, diagonal=False):
        h = {}
        sp = rep.shape
        for i in range(sp[0]):
            for j in range(sp[1]):
                if diagonal and i!=j:
                    continue
                if rep[i,j] >0:  # we don't record the off diagonals
                    h[rep[i,j]] = i if diagonal else (i,j)
        return list(h.values())

    # M[:,:,:] should have a spin index which is the last one
    def EmbeddedIntoKBandWindow(self, M, k, si):
        return self.orbBandProj.EmbeddedIntoKBandWindow(M, k, si)
  
    # given histogram with probabilities only, complete the rest of the information (other than energy)
    # basis is either "separate" or "interlaced" (which is how Gutz likes it)
    def CompleteHistogramData(self, hist, basis="interlaced", verify=False):
        n = self.dim * 2
        if self.nrel == 2:
            n = self.dim
        spins = np.array([1 for i in range(n)])
        if basis == "separate":
            spins[n/2:] = -1
        else:
            spins[1::2] = -1
              
        def GetBits(ibin,n):
            v = np.zeros(n,dtype=int)
            idx = 0
            while (ibin):
                v[idx] = ibin & 1
                ibin >>= 1
                idx += 1
            return v
        
        for i in range(hist.num_configs):
            v = GetBits(i,n)
            if not verify:
                hist.N[i] = np.dot(v, v)
                # TODO: this depends on the basis - modify the code appropriately (mixed basis is different)
                hist.Jz[i] = 0.5*np.dot(v, spins)
            else:
                assert(hist.N[i] == np.dot(v, v))
                assert(abs(hist.Jz[i] - 0.5 * np.dot(v, spins))< 1.0e-6)

        return hist
                
    def CorrectRep(self, is_raw = False):
        newIndex = {}
        idx = 0
        if is_raw:
            rep = np.copy(self.corrSubshell.raw_rep)
        else:
            rep = np.copy(self.rep)
        for (i,j), iorb in np.ndenumerate(rep):
            if iorb > 0:
                if not iorb in newIndex:
                    idx += 1
                    newIndex[iorb] = idx
                rep[i,j] = newIndex[iorb]
        # set -1, -2 indices used as indicators 
        # to actual indices
        for (i,j), iorb in np.ndenumerate(rep):
            if iorb < 0:
                idx += 1
                rep[i,j] = idx
        return rep

    # takes inp from transport
    # note:
    # inp - all energies are in Atomic Rydberg units
    # opts - all energies are eV
    def EmbedInterpolatedOnRealOmega(self, inp:TransportOptics.Input, debug):
        omegaListRy =[i*inp.delta  for i in range(-inp.max_omega_mesh, inp.max_omega_mesh+1)]
        
        self.LoadSigRealOmega(bound=inp.max_omega_mesh*inp.delta*Ry2eV+1.0)
        ese = TransportOptics.EmbeddedSelfEnergyOnRealOmega(
            min_band=self.orbBandProj.min_band,
            max_band=self.orbBandProj.max_band,
            shard_start = self.dft.shard_offset + 1, 
            shard_end = self.dft.shard_offset + self.dft.num_k,
            num_si=self.num_si,
            max_omega=inp.max_omega_mesh)
        ese.allocate()

        ese.omegaRy[:] = omegaListRy[:]
        
        ise = InterpolatedSelfEnergy(self, mesh=ese.omegaRy[:]*Ry2eV)
        for iomega in range(2*inp.max_omega_mesh+1):
            MM = ise.GetMatrix(iomega) 
            for si in range(self.num_si):
                for kidx in range(0, self.dft.num_k):
                    ese.M[:,:,iomega, kidx, si] = self.EmbeddedIntoKBandWindow(MM , kidx, si)
        for kidx in range(0, self.dft.num_k):
            ese.actual_k[kidx] = self.dft.shard_offset + kidx + 1 #1-based for fortran. This is for debug
        return ese
        
    # just load empty object first from history.h5 to make sure it is not truncated.    
    def PreserveHistory(self):
        try:
            eo = EmptyObject()
            eo.load("./history.h5:/")
        except:
            None
        
    def CalculateZValues(self, print_noninter:bool =False):
        """
            Adds the array Z to the object, containing the QP Z's
            This is calculated by DMFT from the real self energy, but in Gutz it is part of the method.
        """
        None

    def AddDensityOrSelfEnergy(left, right):
        pass

    def GetMethodName(self):
        return "correlated"

    def GetMethodLocation(self):
        return ""

    @classmethod
    def GetLabelsForPlotting(cls, state : SolverState):
        if state.nrel > 1:
            obsv = "j"
        else:
            obsv = 'Ylm'
        vals, orbs = GetLabelsForPlotting(state, shell=state.corrSubshell, opn=obsv, subspace_expr = state.orbBandProj.orbsDef.subspace_expr)
        return vals, orbs

    def QPAnalysis(self, epsi = 0.04 * 1j, epsic = 1.0e-8 *1j, epsic2 = 5.0e-5 * 1j, label='DFT+DMFT', obsv=''):
        from pylatexenc.latex2text import LatexNodes2Text
        st = DFTPlugin.Instance().GetStructure()
        comp = Composition(st.formula)
        red_, ucfactor = comp.get_reduced_composition_and_factor()
        print(f"- number of unit formulae in the cell: {ucfactor}")
          
        self.LoadSigRealOmega(Nw=10, bound=0.5) 
        self.InitOneBody()
        
        self.CalculateZValues(print_noninter=False)

        plottingProj = CorrelatedSubspaceProjector("./projector.h5:/def/",
                                          self.proj_energy_window) # no subspace in plotting
                
        correlatedG = ZeroComplexMatrix(plottingProj.shell.dim)
          
        muChoice = self.MuChoice() # 1.0 iff mu is substracted from the epsAllK already
        
        GInvK = ZeroComplexMatrix(self.num_corr_bands)       
        lowKS = self.plugin.GetBandsForAllKpoints(0,  self.orbBandProj.min_band -1 -1)
        highKS = self.plugin.GetBandsForAllKpoints(self.orbBandProj.max_band + 1 - 1, self.dft.num_bands)
        
        I = np.eye(self.num_corr_bands, dtype=np.complex128)
    
        lowDos = 0.0
        highDos = 0.0
        midDos = 0.0
        tcdos = 0.0 # total correlated dos
        iomega = -1
        for i in range(self.sig.num_omega):
            if abs(self.sig.omega[i]) < 1.0e-12:
                iomega = i
                break
        if iomega == -1:
            raise Exception("cannot find zero frequncy in the self energy. quitting.")

        w = self.sig.omega[iomega]
         
        MM = self.sig.M[iomega, :, :, :] - self.DC[:,:, :]  - epsic*self.ID
    
        KB   = 6.333328e-6 *27.2107* 0.5 # collected from Kutepov's code, to match perfectly
        EV2J = 1.60218e-19
        AVOGADRO = 6*1.0e23
        GAMMA_PREF = 1000.0 * cmath.pi*cmath.pi /3.0 * KB*KB * EV2J * AVOGADRO / ucfactor
        
        # calculate full occupation matrix for DFT, since in the main code
        # it is only calculated for the subspace   
        min_corr_band, max_corr_band = self.orbBandProj.min_band -1, self.orbBandProj.max_band -1
        z_dos = 0.0

        for si in range(self.num_si):
                 
            for k in range(self.dft.num_k_irr):
                num_bands = highKS.num_bands[k,si] - self.orbBandProj.max_band
                
                mu = lowKS.chemical_potential
                             
                KS = self.plugin.GetBandsForKpoint(k, min_corr_band, max_corr_band)

                GInvK[:,:] = np.diag(w - self.epsAllK[:,k, si] + epsi) - \
                                self.EmbeddedIntoKBandWindow(MM ,k,si) + (1.0-muChoice)*I*self.mu

                lowEpsK  = w - (lowKS.energy[:,k,si] - mu)*Ry2eV + epsi
                midEpsK  = w -  self.epsAllK[:,k,si] + (1.0-muChoice)*self.mu + epsi
                highEpsK = w - (highKS.energy[:num_bands,k,si] - mu)*Ry2eV + epsi
                                
                lowDos  += -1/np.pi * np.sum(1.0/lowEpsK).imag * self.lapw.weight[k]
                highDos += -1/np.pi * np.sum(1.0/highEpsK).imag * self.lapw.weight[k]
                midDos  += -1/np.pi * np.sum(1.0/midEpsK).imag * self.lapw.weight[k] 
                  
                Gk = GInvK.I
                
                correlatedG = 0.5*1j/np.pi * (Gk - Gk.H)
                Zk = I+self.EmbeddedIntoKBandWindow(self.Z-self.ID,k,si)
                G2 =  -1/np.pi * (Gk*Zk.I).imag # 

                tcdos += np.trace(correlatedG)[0,0]* self.lapw.weight[k]
                z_dos += np.trace(G2)[0,0] * self.lapw.weight[k]

        print(f"total dft+ DOS(0): {tcdos:3.3f}")
        print(f"*low energy γ: {z_dos*GAMMA_PREF:3.3f} [mJ/K^2/mol]")
        print(f"total DOS(0): {lowDos + highDos + tcdos:3.3f}")
        print(f"total γ: {(lowDos + highDos + midDos)*GAMMA_PREF:3.3f} [mJ/K^2/mol]")                            
        print(f"total γ (FL): {(lowDos + highDos + z_dos)*GAMMA_PREF:3.3f} [mJ/K^2/mol]")

    def GetAdditionalDescriptors(self):
        return ""

    @classmethod
    def GetSelectedOrbitals(cls):
        orbitals_location = "./projector.h5:/def/"
        so = SelectedOrbitals()
        so.load(orbitals_location)
        return so

    @classmethod
    def GetSuffixOfFirstImpurity(cls):
        so = cls.GetSelectedOrbitals()
        return GetSuffix(so, 0)     

    def GetSuffixOfImpurity(self):
        so = self.GetSelectedOrbitals()
        return GetSuffix(so, self.which_shell)       

    def GetTopLevelMethodName(self):
        return self.GetMethodFileName(suffix=self.GetSuffixOfImpurity())

    def GetMethodFileName(self, suffix=None):
        if suffix is None:
            suffix = self.orbBandProj.GetSuffix()
        return self.GetMethodLocation().format(suffix)

    def IsQpHamiltonianNonHermitian(self):
        return False
    
    # note that eigenvalues are not sorted (so nor are the eigenvectors)
    def DiagonalizeHamiltonianNoSort(self, H):
        if self.IsQpHamiltonianNonHermitian():
            evsMatrix, v = scipy.linalg.schur(H, output='complex')
            return np.diag(evsMatrix), v
        else:
            return np.linalg.eigh(H)

    def DiagonalizeHamiltonianSort(self, H, complex : bool):
        if self.IsQpHamiltonianNonHermitian():
            evsMatrix, v = scipy.linalg.schur(H, output='complex')
            if complex:
                return np.diag(evsMatrix), v
            else:
                sorted = np.real(np.diag(evsMatrix)).argsort()
                return np.real(np.diag(evsMatrix))[sorted], v[sorted,:]
        else:
            return np.linalg.eigh(H)

    # iff complex=False, we sort 
    def DiagonalEienvalues(self, H, complex : bool, return_vectors = False):
        if self.IsQpHamiltonianNonHermitian():
            evsMatrix, v = scipy.linalg.schur(H, output='complex')
            if complex:
                return np.diag(evsMatrix)
            else:
                # need to sort both ev's and vectors together
                return np.sort(np.real(np.diag(evsMatrix))) # is v the correct matrix or is it v.H?
        else:
            if return_vectors:
                return np.linalg.eigh(H)
            return np.linalg.eigh(H)[0]

    #virtual methods for VisitBandStructure
    def ConstructQuasiparticleHamiltonian(self, proj : OrbitalsBandProjector, epsAllK : np.array, I : np.array, k : int, si : int):
        raise NotImplementedError()

    def GetQuasiparticleWeights(self, proj, uM, I, k, si):
        raise NotImplementedError()

    def GetQuasiparticleLifetimes(self, proj, uM, I, k, si):
        raise NotImplementedError()
    
    def VisitBandStructure(self, visitor: GreensFunctionVisitor):
        """
            This visitor framework iterator takes the argument visitor on a tour in 
            the band structure - which is constructed using the solution, along
            a path and bands described by the visitor.
        """
        #Cannot Call initOneBody here -- projector may be a kpath projector which will cause problems
        #it should be called in the construction of the svs object if it's info is needed.
        
        st = self.plugin.GetStructure()
        num_atoms = len(st.sites)

        proj = visitor.GetProjector()
        passWeight = visitor.passWeight()

        muChoice = self.MuChoice() # 1.0 iff mu is subtracted from the epsAllK already
        
        if self.plugin.IsMaster(): print("Correlated bands range: %d to %d\n" % (proj.min_band-1, proj.max_band-1))
        lowKS = visitor.GetLowBands()
        KS = visitor.GetMidBands(proj)
        highKS = visitor.GetHighBands()
        
        grad = None
        gradients = None
        if type(KS) is tuple:
            gradients = KS[1]
            KS = KS[0]

        epsAllK = np.zeros((self.num_corr_bands, proj.num_k, self.num_si), np.float64)     
 
        mu = KS.chemical_potential
        self.mu = mu*Ry2eV
        epsAllK[:,:,:] = (KS.energy[:,:,:] - mu*self.MuChoice())*Ry2eV

        I = np.eye(self.num_corr_bands, self.num_corr_bands, dtype=np.complex128)
                
        for si in range(self.num_si):
                     
            for k in range(proj.num_k):
                num_bands = highKS.num_bands[k,si] - proj.max_band
                    
                Hqp = self.ConstructQuasiparticleHamiltonian(proj, epsAllK, I, k, si)
                midEpsK, u = self.DiagonalizeHamiltonianSort(Hqp, complex = False)
                uM = Matrix(u)

                lowEpsK  = (lowKS.energy[:,k,si] - mu)*Ry2eV
                    
                highEpsK = (highKS.energy[:num_bands,k,si] - mu)*Ry2eV
                                    
                weight = 0.0
                if passWeight:
                    weight = self.lapw.weight[k]
                    
                if gradients is not None:
                   grad = gradients.data[k,si,:,:,:]

                lifetimes = self.GetQuasiparticleLifetimes(proj, uM, I, k, si)
                qp_weights = self.GetQuasiparticleWeights(proj, uM, I, k, si)
                   
                visitor.VisitKPoint(k = k, si=si, 
                                Hqp = Hqp,
                                lowEpsK = lowEpsK,
                                midEpsK = midEpsK,
                                highEpsK = highEpsK,
                                gradients = grad,
                                lifetimes = lifetimes,
                                qp_weights = qp_weights,
                                weight=weight, num_atoms=num_atoms)
                                
        visitor.End()        

    def VisitGreensFunction(self, visitor: GreensFunctionVisitor):
        """
            This visitor framework iterator takes the argument visitor on a tour in 
            the correlated Greens function - which is constructed using the solution, along
            a path and bands described by the visitor.
        """
        #Cannot Call initOneBody here -- projector may be a kpath projector which will cause problems
        #it should be called in the construction of the svs object if it's info is needed.

        st = self.plugin.GetStructure()
        num_atoms = len(st.sites)

        epsi =  1j * visitor.NonCorrelatedWidening()
        epsic = 1j * visitor.CorrelatedWidening() 
        proj = visitor.GetProjector()
        passWeight = visitor.passWeight()

        muChoice = self.MuChoice() # 1.0 iff mu is subtracted from the epsAllK already
        GInvK = ZeroComplexMatrix(proj.max_band-proj.min_band+1)
        if self.plugin.IsMaster(): print("Correlated bands range: %d to %d\n" % (proj.min_band-1, proj.max_band-1))
        lowKS = None
        if  self.sig.omega[0] <= proj.energyWin.low:
            lowKS = visitor.GetLowBands()
        highKS = None
        if  self.sig.omega[-1] >= proj.energyWin.high:
            highKS = visitor.GetHighBands()
              
        KS = visitor.GetMidBands(proj)
        grad = None
        gradients = None
        if type(KS) is tuple:
            gradients = KS[1]
            KS = KS[0]

        #epsAllK takes 
        epsAllK = (KS.energy - KS.chemical_potential*muChoice) * Ry2eV
        mu = KS.chemical_potential
        self.mu = mu * Ry2eV 
                        
        for iomega in range(self.sig.num_omega):
            w = self.sig.omega[iomega]
          
            MM = self.impurity_delegator.SigmaMinusDC(self,iomega)

            visitor.StartKPointsPerOmega(iomega, w)
            
            for si in range(self.num_si):
                     
                for k in range(proj.num_k):
                    if highKS is not None: num_bands = highKS.num_bands[k,si] - proj.max_band
                    
                    GInvK[:,:] = self.impurity_delegator.ComputeInverseGreensFunction(self, w, epsAllK, muChoice, epsic, proj, MM, k, si)

                    if lowKS is not None: lowEpsK  = w - (lowKS.energy[:,k,si] - mu)*Ry2eV + epsi
                    midEpsK = self.impurity_delegator.ComputeMidEpsK(w, epsAllK, (1.0-muChoice)*self.mu, epsi, k, si)
                    if highKS is not None: highEpsK = w - (highKS.energy[:num_bands,k,si] - mu)*Ry2eV + epsi
                                    
                    negativeOmegaDos = 0.0
                    if lowKS is not None: negativeOmegaDos = -1/np.pi * np.sum(1.0/lowEpsK).imag 
                    positiveOmegaDos = 0.0
                    if highKS is not None: positiveOmegaDos = -1/np.pi * np.sum(1.0/highEpsK).imag

                    lowOmegaDos      = -1/np.pi * np.sum(1.0/midEpsK).imag 
                      
                    Gk = GInvK.I

                    lowEnergyGkDos =  -1/np.pi * np.trace(Gk)[0,0].imag

                    weight = 0.0
                    if passWeight:
                        weight = self.lapw.weight[k]
                    
                    if gradients is not None:
                       grad = gradients.data[k,si,:,:,:]

                    visitor.VisitKOmegaPoint(k = k, iomega=iomega, w=w, si=si, 
                                Gk=Gk, gradients = grad,
                                lowEnergyGkDos=lowEnergyGkDos,
                                negativeOmegaDos=negativeOmegaDos, positiveOmegaDos=positiveOmegaDos, 
                                lowOmegaDos=lowOmegaDos,
                                weight=weight, num_atoms=num_atoms)
            visitor.EndKPointsPerOmega(iomega, w)
        visitor.End()        

# interpolates the self energy on a mesh in eV (values are in eV as well)

class InterpolatedSelfEnergy():
    def __init__(self, sol : PEScfEngine, mesh, debug=False, add_dc:bool = True, addHyb:bool=False):
        self.sol = sol
        self.mesh = mesh
        self.correctedRep = sol.CorrectRep()
        num_orbs = np.max(self.correctedRep)
        indices : List[[int,int]] = [None for _i in range(num_orbs)]
        for i in range(sol.dim):
            for j in range(sol.dim):
                r = self.correctedRep[i,j]
                if r > 0 and indices[r-1] is None:
                    indices[r-1] = (i,j)
                
        w_axis_len = sol.sig.M.shape[0] 
        F = np.zeros((num_orbs,sol.num_si,w_axis_len), dtype=np.complex128)
        self.S = np.zeros((num_orbs,sol.num_si,mesh.shape[0]), dtype=np.complex128)
                
        dcF = 1 if add_dc else 0

        for orb in range(num_orbs):
            i1,i2 = indices[orb]
            for si in range(sol.num_si):
                for i in range(w_axis_len):
                    # note that we extrapolate the full M -DC with 0's outside the domain (which is the correlated window)
                    # because outside the correlated window the effective self energy is 0.0
                    # by definition
                    F[orb,si,i] = sol.sig.M[i, i1, i2, si] - dcF*sol.DC[i1,i2, si]  # - self.ID[i1,i2,si]* 1j * inp.gammc*Ry2eV
                    if addHyb:
                        F[orb,si,i] += sol.hyb.M[i, i1, i2, si]
                # Now interpolate F
                self.S[orb,si,:] = np.interp(mesh, sol.sig.omega[:], F[orb,si,:], 0.0, 0.0, None)
                if debug:
                    import matplotlib.pyplot as plt
                    plt.plot(sol.sig.omega,  F[orb,si,:].real, "-")
                    plt.plot(mesh,self.S[orb,si,:].real, ".")

                    plt.plot(sol.sig.omega,  F[orb,si,:].imag, "-", )
                    plt.plot(mesh,self.S[orb,si,:].imag, "-", ".")

        if debug:
            plt.savefig("interp.eps")
        
    def GetMatrix(self, iomega):
        MM = np.zeros((self.sol.dim, self.sol.dim, self.sol.num_si), dtype=np.complex128)
        for si in range(self.sol.num_si):
            for i in range(self.sol.dim):
                for j in range(self.sol.dim):
                    r = self.correctedRep[i,j]  
                    if r>0:
                        MM[i,j,si] = self.S[r-1,si,iomega]
        return MM

    def GetLocalOmegaFunction(self):
        M =  np.zeros((self.mesh.shape[0], self.sol.dim, self.sol.dim, self.sol.num_si), dtype=np.complex128)
        for si in range(self.sol.num_si):
            for i in range(self.sol.dim):
                for j in range(self.sol.dim):
                    r = self.correctedRep[i,j]  
                    if r>0:
                        M[:, i,j,si] = self.S[r-1,si,:]
        return M
    
    def GetOrbitalFunctions(self):
        return self.S
    