#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

'''
Created on Apr 25, 2020

@author: adler
'''
from argparse import ArgumentDefaultsHelpFormatter, Namespace

import json
import os
import numpy
from pathlib import Path
import sys
from argparse import ArgumentParser

class ArgumentParserThatStoresArgv(ArgumentParser):

    def parse_args(self, args=None, namespace=None):
        namespace = ArgumentParser.parse_args(self, args, namespace) # to actually check for unknown options
        namespace.argv = sys.argv
        
        return namespace

    def parse_known_args(self, args=None, namespace=None):
        namespace, argv = ArgumentParser.parse_known_args(self, args, namespace)
        namespace.argv = sys.argv
        return namespace, argv


def convert(o):
    if isinstance(o, numpy.int64): return int(o)  
    raise TypeError

# save the options into the options file.
# if a file exists - updates the file with the given options. 
# should only be called from the master program: it is not checked
def SaveOptions(opts : Namespace, verbose=False):
    #print(opts)
    opts_to_save = opts

    of = Path('./opts.json').resolve()
        
    if of.exists():
        opts0 = ReadOptions()
        
        for name, val in opts.__dict__.items():
            if hasattr(opts0, name):
                val2 = getattr(opts0,name)
                if val2 != val:
                    if verbose:
                        print(f"-- updating option {name} - from {val2} to {val}")
            setattr(opts0, name, val)
        opts_to_save = opts0 
        sys.stdout.flush()
    
    with of.open(mode='w') as f:
        json.dump(opts_to_save.__dict__, f, indent=2, default=convert)
                
def RefreshOptions(opts : Namespace, verbose=False, no_verbose=[]):
    of = Path('./opts.json').resolve()
    if not of.exists():
        return
    opts2 = Namespace()
    try:
        with of.open(mode='r') as f:
            opts2.__dict__ = json.load(f)
    except BaseException as e:
        # make sure a temporary error doesn't crash the run
        print("*** opts.json file cannot be read - options not refreshed ***", e)
        return 
            
    for name, val2 in opts2.__dict__.items():        
        if verbose and hasattr(opts, name):
            val = getattr(opts,name)
            if name in ['mpi_workers','worker','argv']:
                continue # for sanity: never read these, they are job control parameters
            if val2 != val and not name in no_verbose:
                print(f"-- refreshing option {name} - from {val} to {val2}")
        setattr(opts, name, val2)
    sys.stdout.flush()

def ReadOptions():
    opts = Namespace()
    RefreshOptions(opts)
    return opts
    

def ReadOneOption(name, default):
    opts = ReadOptions()
    if hasattr(opts, name):
        return getattr(opts, name), opts
    else:
        return default, opts
      
def CompleteFromOptions(prefix, **kwargs):
    from argcomplete import warn
    opts  = ReadOptions()
    #warn("COMPLETING options", opts)
    ret = []
    for name,val in opts.__dict__.items():
        if name.startswith(prefix):
            ret.append(f"{name}={val}")
    return ret

def OptionsMain():
    import argcomplete
    #from argcomplete.completers import EnvironCompleter
    parser = ArgumentParserThatStoresArgv('opts', 
                            parents=[],
                            add_help=True ,formatter_class=ArgumentDefaultsHelpFormatter)
    
    parser.add_argument(dest="name_value", nargs="*",
                        help="name=value pairs, sparated by space",
                        type=str).completer = CompleteFromOptions
                            
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    opts  = ReadOptions()
    for nv in args.name_value:
        name, value = nv.split("=")
        setattr(opts, name, value)
    SaveOptions(opts, verbose=True)


if __name__ == '__main__':
    OptionsMain()