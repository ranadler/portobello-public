#!/usr/bin/env python3
'''
Created on Feb 22, 2021

@author: adler
'''
import sys
import argparse
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
from portobello.bus.mpi import GetMPIProxyOptionsParser
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.loader import AnalysisLoader, AnalysisOptionsParser
from portobello.rhobusta.options import ReadOptions, RefreshOptions, SaveOptions
from portobello.rhobusta.plotters import DOSPlotBuilder
  
class DOSRunner(AnalysisLoader):
    def __init__(self, opts):
        AnalysisLoader.__init__(self,opts)

    def MPIWorker(self):
        self.InitializeMPI()
        self.LoadCalculation(loadRealSigma=True)
    
        visitor = DOSPlotBuilder(self.svs, self.opts, label=self.svs.GetMethodName())
        self.svs.VisitGreensFunction(visitor)
        self.Terminate()

if __name__ == '__main__':
    parser = ArgumentParserThatStoresArgv('spectral-function', add_help=True,
                            parents=[GetMPIProxyOptionsParser(), AnalysisOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)

                                
    opts, _args = parser.parse_known_args(sys.argv[1:])

    runner = DOSRunner(opts)
    runner.Run(wait=True)
