
'''
Created on Jul 17, 2018

@author: tl596@physics.rutgers.edu
'''


import numpy as np
from math import factorial
from itertools import combinations
from numba import jit

#class basis(object):
#  '''
#  class for Fock state basis
#  '''
#  def __init__(self, norb, use_ntot=True, use_sz=True, thermal=False):
#    '''
#    Constructor.
#    Input:
#      norb: number of orbital
#      ntot: total particle number. If 'None', partition the Hibert space without using particle number.
#      sz: total Sz number. If 'None', partition the Hilbert space without using the Sz number.
#      thermal: Ture: return all the basis set. False: return only the specific subspace
#
#    Note: Try to leave some room to implement other symmtery such as rotation and translation ...
#    '''
#    self.norb = norb
#    self.use_ntot = use_ntot
#    self.use_sz = use_sz
#    self.thermal = thermal
#    print 'initialize the Hilbert space for %g orbitals using Ntot (%s), Sz (%s), and thermal (%s)'%(self.norb, self.use_ntot, self.use_sz, self.thermal)
#  
#  def create_basis(self, N, Sz):
#    '''
#    Create many-body basis.
#    Input:
#      N: total particle number
#      Sz: total z-component of spin
#
#    Return:
#      basis: Fock space basis, stored as dictionary
#    '''

def table_ep(nstate,nparticle,dtype=np.int64):
    '''
    This function generates the table of binary representations of a particle-conserved and spin-non-conserved basis.
    '''
    result=np.zeros(factorial(nstate)//factorial(nparticle)//factorial(nstate-nparticle),dtype=dtype)
    buff=combinations(range(nstate),nparticle)
    for i,v in enumerate(buff):
        basis=0
        for num in v:
            basis+=(1<<num)
        result[i]=basis
    result.sort()
    return result

def table_es(nstate,nparticle,spinz,dtype=np.int64):
    '''
    This function generates the table of binary representations of a particle-conserved and spin-conserved basis.
    '''
    n=nstate//2
    nup=(nparticle+int(2*spinz))//2
    ndw=(nparticle-int(2*spinz))//2
    result=np.zeros(factorial(n)//factorial(nup)//factorial(n-nup)*factorial(n)//factorial(ndw)//factorial(n-ndw),dtype=dtype)
    buff_up=list(combinations(range(1,2*n,2),nup))
    buff_dw=list(combinations(range(0,2*n,2),ndw))
    count=0
    for vup in buff_up:
        buff=0
        for num in vup:
            buff+=(1<<num)
        for vdw in buff_dw:
            basis=buff
            for num in vdw:
                basis+=(1<<num)
            result[count]=basis
            count+=1
    result.sort()
    return result

@jit(nopython=True)
def power2(a):
    return 1 << a

@jit(nopython=True)
def countBits(a):
    ret = 0
    while a != 0:
        ret += a & 0x1
        a = a >> 1
    return ret

@jit(nopython=True)
def table_ej(l):
    print("- Building J2 preserving fock space for l=", l)
    result = []
    for x in range(power2(2*l)):
        for y in  range(power2(2*l+2)):
            cx = 2*l - countBits(x)
            cy = 2*l+2 - countBits(y)
            base =  y << 2*l | x
            for x2 in range(power2(2*l)):
                for y2 in range(power2(2*l+2)):
                    if countBits(x2) == cx and countBits(y2) == cy:
                        result.append(y2 << (6*l+2) | x2 << (4*l+2) | base)
    result.sort()
    return result
    
@jit(nopython=True)
def table_ep_nrange(l, nrange):
    result = []
    for x in range(power2(2*(2*l+1))):
        cx = countBits(x)
        if not cx in nrange:
            continue    
        for x2 in range(power2(2*(2*l+1))):
            if countBits(x2) == 4*l+2 - cx:
                result.append(x2 << (4*l+2) | x)
    result.sort()
    return result
    
#  only preserving: num in orbitals = num bath.
@jit(nopython=True)
def table_ep2(l):
    result = []
    for x in range(power2(2*(2*l+1))):
        cx = countBits(x)  
        for x2 in range(power2(2*(2*l+1))):
            if countBits(x2) == 4*l+2 - cx:
                result.append(x2 << (4*l+2) | x)
    result.sort()
    return result
    
#  only preserving: num in orbitals = num bath and total spin is 0
@jit(nopython=True)
def table_ep2_spin_symm(l:int):
    result = []

    bound = power2(2*(2*l+1))
    odds = (bound-1)*4//3 + 1
    odds = odds & (bound-1)
    evens = ~odds & (bound-1)

    for x in range(bound):
        cx = countBits(x)  
        cxOdds = countBits(x & odds) 
        cxEvens = countBits(x & evens)
        for x2 in range(bound):
            if countBits(x2) == 4*l+2 - cx and countBits(x2 & evens) + cxEvens != countBits(x2 & odds) + cxOdds:
                result.append(x2 << (4*l+2) | x)
    result.sort()
    return result
    


if __name__ == "__main__":
    #basis = basis(2, True, True, False)
    print(table_ep(4,2))
    print(table_es(4,2,0))
    #basis.create_basis()
      
    ej2 = table_ej(2)
    print([bin(x) for x in ej2])
