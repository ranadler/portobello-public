from scipy.sparse.linalg.eigen.arpack.arpack import eigsh
if __name__ == '__main__':
  from portobello.rhobusta.simple_ed.ed import getInstance, simple_ed
  import numpy as np
  
#  from simple_ga.ed import *
  from scipy.linalg import eigh
  np.set_printoptions(suppress=True)
  #ed_solver = simple_ed(24, use_Ntot=True)

  #test = '1orb_N_Sz'
  #test = '1orb_N'
  #test = '1orb'

  #test = '2orb_N_Sz'
  #test = '2orb_N'
  #test = '2orb'
  
  #test = '4orb_N_Sz'

  #test = '5orb_N_Sz'

  test = '7orb_N_Sz'
  
  if test == '1orb_N_Sz':
    ed_solver = simple_ed(4, use_Ntot=True, use_Sz=True)
    Umatrix = np.zeros((2,2,2,2))
    Umatrix[0,0,1,1] = 1.0
    Umatrix[1,1,0,0] = 1.0
    H1E = np.eye(2)*0.1
    D = np.eye(2)*-0.4 
    Lambdac = np.eye(2)*0.1#*0.1
    ed_solver.build_two_body(Umatrix,debug=True)
    ed_solver.build_one_body(H1E, D, Lambdac, debug=False)
    ed_solver.build_Ham()
    ed_solver.build_denmat_op(debug=False)
    ed_solver.diagonalize()
    print('density matrix=')
    print(ed_solver.compute_denmat())
 
    #FH_list = build_fermion_op(4)
    #Hemb, U2loc = build_Hemb_matrix(D, H1E, Lambdac, Umatrix, FH_list)
    #evals, evecs = eigsh(Hemb,k=2,which='SA')
    #evals, evecs = eigh(Hemb.toarray())
    #print evals
    #print calc_density_matrix(FH_list,evecs[:,0]).real

  if test == '1orb_N':
    ed_solver = simple_ed(4, use_Ntot=True, use_Sz=False)
    Umatrix = np.zeros((2,2,2,2))
    Umatrix[0,0,1,1] = 1.0
    Umatrix[1,1,0,0] = 1.0
    H1E = np.ones((2,2))*0.1
    #H1E = np.eye(2)*0.
    D = np.ones((2,2))*-0.4
    #D = np.eye(2)*-0.4 
    Lambdac = np.ones((2,2))*0.2
    #Lambdac = np.eye(2)*0.#*0.1
    ed_solver.build_two_body(Umatrix)
    ed_solver.build_one_body(H1E, D, Lambdac, debug=False)
    Ham = ed_solver.build_Ham()
    ed_solver.build_denmat_op(debug=False)
    ed_solver.diagonalize()
    #evals, evecs = eigh(Ham.toarray())
    #print evals  
    print('density matrix=')
    print(ed_solver.compute_denmat())

    #FH_list = build_fermion_op(4)
    #Hemb, U2loc = build_Hemb_matrix(D, H1E, Lambdac, Umatrix, FH_list)
    #evals, evecs = eigsh(Hemb,k=2,which='SA')
    #evals, evecs = eigh(Hemb.toarray())
    #print evals
    #print calc_density_matrix(FH_list,evecs[:,0]).real

  if test == '1orb':
    ed_solver = simple_ed(4, use_Ntot=False, use_Sz=False)
    Umatrix = np.zeros((2,2,2,2))
    Umatrix[0,0,1,1] = 1.0
    Umatrix[1,1,0,0] = 1.0
    H1E = np.ones((2,2))*0.1
    #H1E = np.eye(2)*0.1
    D = np.ones((2,2))*-0.4
    #D = np.eye(2)*-0.4 
    Lambdac = np.ones((2,2))*0.2
    #Lambdac = np.eye(2)*0.#*0.1
    ed_solver.build_two_body(Umatrix)
    ed_solver.build_one_body(H1E, D, Lambdac, debug=False)
    Ham = ed_solver.build_Ham()
    ed_solver.build_denmat_op(debug=False)
    ed_solver.diagonalize()
    #print Ham.toarray()
    #evals, evecs = eigh(Ham.toarray())
    #print evals  
    print('density matrix=')
    print(ed_solver.compute_denmat())

    #FH_list = build_fermion_op(4)
    #Hemb, U2loc = build_Hemb_matrix(D, H1E, Lambdac, Umatrix, FH_list)
    #evals, evecs = eigsh(Hemb,k=2,which='SA')
    #evals, evecs = eigh(Hemb.toarray())
    #print 'Hemb='
    #print Hemb.todense().real
    #print 'evals='
    #print evals
    #print calc_density_matrix(FH_list,evecs[:,0]).real

  if test == '2orb_N_Sz':
    ed_solver = simple_ed(8, use_Ntot=True, use_Sz=True)
    #print ed_solver.basis
    Umatrix = np.zeros((4,4,4,4))
    Umatrix[0,0,1,1] = 1.0
    Umatrix[1,1,0,0] = 1.0
    Umatrix[2,2,3,3] = 1.0
    Umatrix[3,3,2,2] = 1.0
    Umatrix[1,3,3,1] = 0.0
    Umatrix[3,1,1,3] = 0.0
    Umatrix[0,2,2,0] = 0.0
    Umatrix[2,0,0,2] = 0.0
    H1E = np.eye(4)*-0.5
    D = np.eye(4)*(-0.4) 
    Lambdac = np.eye(4)*0.
    print('building two-body...')
    ed_solver.build_two_body(Umatrix,debug=False)
    print('building one-body...')
    ed_solver.build_one_body(H1E, D, Lambdac, debug=False)
    #print ed_solver.Htwo
    print('eigvals of Hone')
    print(eigsh(ed_solver.Hone,which='SA')[0])
    print('eigvals of Htwo')
    print(eigsh(ed_solver.Htwo,which='SA')[0])
    ed_solver.build_Ham()
    #ed_solver.build_denmat_op(debug=False)
    ed_solver.diagonalize()
    print('density matrix=')
    print(ed_solver.compute_denmat())

    ed_solver.compute_rho()
    rholoc = ed_solver.compute_rholoc()
    print('rholoc=')
    print(rholoc)
    trrholoc = np.sum(rholoc[list(range(rholoc.shape[0])),list(range(rholoc.shape[1]))])
    print('Tr[rholoc]=', trrholoc)

    print('rholoc_onfly=')
    print(ed_solver.compute_rholoc_onfly())

    #for i in range(ed_solver.bipart_smap.shape[0]):
    #    values = ed_solver.bipart_smap[i]
    #    print values[0], values[1], ed_solver.strb_red.format(values[1]), values[2], ed_solver.strb_red.format(values[2])
  
    #FH_list = build_fermion_op(8)
    #Hemb, U2loc = build_Hemb_matrix(D, H1E, Lambdac, Umatrix, FH_list)
    #Hone = Hemb -U2loc
    #print U2loc
    #print 'eigvals of Hone'
    #print eigsh(Hone,which='SA')[0]   
    #evals, evecs = eigsh(Hemb,k=2,which='SA')
    #print 'eigvals of Htwo'
    #print eigsh(U2loc,which='SA')[0]
    #evals, evecs = eigh(Hemb.toarray())
    #print evals[:3]
    #print calc_density_matrix(FH_list,evecs[:,0]).real


  if test == '2orb_N':
    ed_solver = simple_ed(8, use_Ntot=True, use_Sz=False)
    Umatrix = np.zeros((4,4,4,4))
    Umatrix[0,0,1,1] = 1.0
    Umatrix[1,1,0,0] = 1.0
    Umatrix[2,2,3,3] = 1.0
    Umatrix[3,3,2,2] = 1.0
    Umatrix[1,3,3,1] = 0.0
    Umatrix[3,1,1,3] = 0.0
    Umatrix[0,2,2,0] = 0.0
    Umatrix[2,0,0,2] = 0.0
    H1E = np.eye(4)*-0.5
    D = np.eye(4)*(-0.4) 
    Lambdac = np.eye(4)*0.
    #H1E = np.ones((4,4))*0.
    #D = np.ones((4,4))*-0.4 
    #Lambdac = np.ones((4,4))*0.
    print('building two-body...')
    ed_solver.build_two_body(Umatrix,debug=False)
    print('building one-body...')
    ed_solver.build_one_body(H1E, D, Lambdac, debug=False)
    #print ed_solver.Htwo
    #print ed_solver.Hone
    print('eigvals of Hone')
    print(eigsh(ed_solver.Hone,which='SA')[0])
    print('eigvals of Htwo')
    print(eigsh(ed_solver.Htwo,which='SA')[0])
    ed_solver.build_Ham()
    ed_solver.build_denmat_op(debug=False)
    ed_solver.diagonalize()
    print('density matrix=')
    print(ed_solver.compute_denmat())
 
    #FH_list = build_fermion_op(8)
    #Hemb, U2loc = build_Hemb_matrix(D, H1E, Lambdac, Umatrix, FH_list)
    #Hone = Hemb -U2loc
    #print U2loc
    #print Hone
    #print 'eigvals of Hone'
    #print eigsh(Hone,which='SA')[0]   
    #evals, evecs = eigsh(Hemb,k=2,which='SA')
    #print 'eigvals of Htwo'
    #print eigsh(U2loc,which='SA')[0]
    #evals, evecs = eigsh(Hemb,k=3,which='SA')
    #print evals[:3]
    #print calc_density_matrix(FH_list,evecs[:,0]).real

  if test == '2orb':
    ed_solver = simple_ed(8, use_Ntot=False, use_Sz=False)
    Umatrix = np.zeros((4,4,4,4))
    Umatrix[0,0,1,1] = 1.0
    Umatrix[1,1,0,0] = 1.0
    Umatrix[2,2,3,3] = 1.0
    Umatrix[3,3,2,2] = 1.0
    Umatrix[1,3,3,1] = 0.
    Umatrix[3,1,1,3] = 0.
    Umatrix[0,2,2,0] = 0.
    Umatrix[2,0,0,2] = 0.
    #H1E = np.eye(4)*0.1
    #D = np.eye(4)*(-0.4) 
    #Lambdac = np.eye(4)*0.2
    H1E = np.ones((4,4))*0.1
    D = np.ones((4,4))*-0.4 
    Lambdac = np.ones((4,4))*0.2
    print('building two-body...')
    ed_solver.build_two_body(Umatrix,debug=True)
    print('building one-body...')
    ed_solver.build_one_body(H1E, D, Lambdac, debug=False)
    print(ed_solver.Htwo)
    #print ed_solver.Hone
    print('eigvals of Hone')
    print(eigsh(ed_solver.Hone,which='SA')[0])
    print('eigvals of Htwo')
    print(eigsh(ed_solver.Htwo,which='SA')[0])
    Ham = ed_solver.build_Ham()
    #evals, evecs = eigh(Ham.toarray())
    #print evals[:3]
    #ed_solver.diagonalize(k=6)
    ed_solver.build_denmat_op(debug=False)
    ed_solver.diagonalize()
    print('density matrix=')
    print(ed_solver.compute_denmat())
  
    #FH_list = build_fermion_op(8)
    #Hemb, U2loc = build_Hemb_matrix(D, H1E, Lambdac, Umatrix, FH_list)
    #Hone = Hemb -U2loc
    #print U2loc
    #print Hone
    #print 'eigvals of Hone'
    #print eigsh(Hone,which='SA')[0]   
    #evals, evecs = eigsh(Hemb,k=2,which='SA')
    #print 'eigvals of Htwo'
    #print eigsh(U2loc,which='SA')[0]
    #evals, evecs = eigh(Hemb.toarray())
    #print evals[:3]
  
    #print 'Hamiltonian equal?', np.allclose(Ham.toarray(),Hemb.toarray()) 
    #assert np.allclose(Ham.toarray(),Hemb.toarray())
    #print calc_density_matrix(FH_list,evecs[:,0]).real

  if test == '4orb_N_Sz':
    no = 4*2
    hsize = 2**(2*no)
    ed_solver = simple_ed(16, use_Ntot=True, use_Sz=True)
    Umatrix = np.zeros((no,no,no,no))
    Umatrix[0,0,1,1] = 1.0
    Umatrix[1,1,0,0] = 1.0
    Umatrix[2,2,3,3] = 1.0
    Umatrix[3,3,2,2] = 1.0
    Umatrix[4,4,5,5] = 1.0
    Umatrix[5,5,4,4] = 1.0
    Umatrix[6,6,7,7] = 1.0
    Umatrix[7,7,6,6] = 1.0

    H1E = np.eye(no)*(-0.5)
    D = np.eye(no)*(-0.4) 
    Lambdac = np.eye(no)*0.
    #H1E = np.ones((no,no))*0.1
    #D = np.ones((no,no))*-0.4 
    #Lambdac = np.ones((no,no))*0.2
    print('building two-body...')
    ed_solver.build_two_body(Umatrix,debug=False)
    print('building one-body...')
    ed_solver.build_one_body(H1E, D, Lambdac, debug=False)
    #print ed_solver.Htwo
    #print ed_solver.Hone
    print('eigvals of Hone')
    print(eigsh(ed_solver.Hone,which='SA')[0])
    print('eigvals of Htwo')
    print(eigsh(ed_solver.Htwo,which='SA')[0])
    ed_solver.build_Ham()
    #ed_solver.build_denmat_op(debug=False)
    ed_solver.diagonalize()
    print('density matrix=')
    dm_simple_ed = ed_solver.compute_denmat()
    print(dm_simple_ed)

    #ed_solver.compute_rho()
    #rholoc = ed_solver.compute_rholoc()
    #print 'rholoc='
    #print rholoc
    #trrholoc = np.sum(rholoc[range(rholoc.shape[0]),range(rholoc.shape[1])])
    #print 'Tr[rholoc]=', trrholoc

    rholoc = ed_solver.compute_rholoc_onfly()
    print('rholoc_onfly=')
    print(rholoc)
    trrholoc = np.sum(rholoc[list(range(rholoc.shape[0])),list(range(rholoc.shape[1]))])
    print('Tr[rholoc]=', trrholoc)

    #for i in range(ed_solver.bipart_smap.shape[0]):
    #    values = ed_solver.bipart_smap[i]
    #    print values[0], values[1], ed_solver.strb_red.format(values[1]), values[2], ed_solver.strb_red.format(values[2])


    # Below is the brutforce ED for dubugging
    #FH_list = build_fermion_op(no*2)
    #Hemb, U2loc = build_Hemb_matrix(D, H1E, Lambdac, Umatrix, FH_list)
    #Hone = Hemb -U2loc
    #build spin operators:
    #build total spin operators S+
    #Sp = csr_matrix((hsize,hsize), dtype=np.complex)
    #for i in range(no):
    #    Sp += FH_list[2*i].dot(FH_list[2*i+1].getH())
        #f.write('    +1.0*OppC{}*OppA{}\n'.format(2*_i, 2*_i+1))
    #build total spin operators S-
    #Sm = csr_matrix((hsize,hsize), dtype=np.complex)
    #for i in range(no):
    #    Sm += FH_list[2*i+1].dot(FH_list[2*i].getH())
        #f.write('    +1.0*OppC{}*OppA{}\n'.format(2*_i+1, 2*_i))
    #Sz = csr_matrix((hsize,hsize), dtype=np.complex)
    #for i in range(no):
    #    Sz += ( 0.5*FH_list[2*i].dot(FH_list[2*i].getH()) - 0.5*FH_list[2*i+1].dot(FH_list[2*i+1].getH()) )
        #f.write('    +0.5*OppC{}*OppA{}-0.5*OppC{}*OppA{}\n'.format(2*_i, 2*_i, 2*_i+1, 2*_i+1))
    #build S^2 operator
    #S2 = Sm.dot(Sp)+Sz.dot(Sz)+Sz
    #add spin penalty:
    #Hemb += 0.05*S2
    #print U2loc
    #print Hone
    #print 'eigvals of Hone'
    #print eigsh(Hone,which='SA')[0]   
    #evals, evecs = eigsh(Hemb,k=2,which='SA')
    #print 'eigvals of Htwo'
    #print eigsh(U2loc,which='SA')[0]
    #evals, evecs = eigsh(Hemb,k=3,which='SA')
    #print evals[:3]
    #dm_ed = calc_density_matrix(FH_list,evecs[:,0]).real
    #print 'density matrix='
    #print dm_ed
    #
    #print 'max(|dm_ed - dm_simple_ed|)=', np.max(abs(dm_ed - dm_simple_ed))


  if test == '5orb_N_Sz':
    no = 5*2
    ed_solver = simple_ed(2*no, use_Ntot=True, use_Sz=True)
    Umatrix = np.zeros((no,no,no,no))
    Umatrix[0,0,1,1] = 0.0
    Umatrix[1,1,0,0] = 0.0
    Umatrix[2,2,3,3] = 0.0
    Umatrix[3,3,2,2] = 0.0
    Umatrix[4,4,5,5] = 0.0
    Umatrix[5,5,4,4] = 0.0
    Umatrix[6,6,7,7] = 0.0
    Umatrix[7,7,6,6] = 0.0
    Umatrix[8,8,9,9] = 0.0
    Umatrix[9,9,8,8] = 0.0

    H1E = np.eye(no)*-0.0
    D = np.eye(no)*(-0.4) 
    Lambdac = np.eye(no)*0.
    #H1E = np.ones((no,no))*0.
    #D = np.ones((no,no))*-0.4 
    #Lambdac = np.ones((no,no))*0.
    print('building two-body...')
    ed_solver.build_two_body(Umatrix,debug=False)
    print('building one-body...')
    ed_solver.build_one_body(H1E, D, Lambdac, debug=False)
    #print ed_solver.Htwo
    #print ed_solver.Hone
    print('eigvals of Hone')
    print(eigsh(ed_solver.Hone,which='SA')[0])
    print('eigvals of Htwo')
    print(eigsh(ed_solver.Htwo,which='SA')[0])
    ed_solver.build_Ham()
    #ed_solver.build_denmat_op(debug=False)
    ed_solver.diagonalize()
    print('density matrix=')
    print(ed_solver.compute_denmat())
    print('double occupancy on site-0=')
    print(ed_solver.compute_docc_i(0))

    #ed_solver.compute_rho()
    #rholoc = ed_solver.compute_rholoc()
    #print 'rholoc='
    #print rholoc
    #trrholoc = np.sum(rholoc[range(rholoc.shape[0]),range(rholoc.shape[1])])
    #print 'Tr[rholoc]=', trrholoc

    rholoc = ed_solver.compute_rholoc_onfly()
    print('rholoc_onfly=')
    print(rholoc)
    trrholoc = np.sum(rholoc[list(range(rholoc.shape[0])),list(range(rholoc.shape[1]))])
    print('Tr[rholoc]=', trrholoc)

    #for i in range(ed_solver.bipart_smap.shape[0]):
    #    values = ed_solver.bipart_smap[i]
    #    print values[0], values[1], ed_solver.strb_red.format(values[1]), values[2], ed_solver.strb_red.format(values[2])


  if test == '7orb_N_Sz':
    no = 7*2
    ed_solver = simple_ed(2*no, use_Ntot=True, use_Sz=True)
    Umatrix = np.zeros((no,no,no,no))
    Umatrix[0,0,1,1] = 1.0
    Umatrix[1,1,0,0] = 1.0
    Umatrix[2,2,3,3] = 1.0
    Umatrix[3,3,2,2] = 1.0
    Umatrix[4,4,5,5] = 1.0
    Umatrix[5,5,4,4] = 1.0
    Umatrix[6,6,7,7] = 1.0
    Umatrix[7,7,6,6] = 1.0
    Umatrix[8,8,9,9] = 1.0
    Umatrix[9,9,8,8] = 1.0
    Umatrix[10,10,11,11] = 1.0
    Umatrix[11,11,10,10] = 1.0
    Umatrix[12,12,13,13] = 1.0
    Umatrix[13,13,12,12] = 1.0

    H1E = np.eye(no)*-0.5
    D = np.eye(no)*(-0.4) 
    Lambdac = np.eye(no)*0.
    #H1E = np.ones((no,no))*0.
    #D = np.ones((no,no))*-0.4 
    #Lambdac = np.ones((no,no))*0.
    print('building two-body...')
    ed_solver.build_two_body(Umatrix,debug=False)
    print('building one-body...')
    ed_solver.build_one_body(H1E, D, Lambdac, debug=False)
    #print ed_solver.Htwo
    #print ed_solver.Hone
    #print 'eigvals of Hone'
    #print eigsh(ed_solver.Hone,which='SA')[0]
    #print 'eigvals of Htwo'
    #print eigsh(ed_solver.Htwo,which='SA')[0]
    ed_solver.build_Ham()
    #ed_solver.build_denmat_op(debug=False)
    ed_solver.diagonalize()
    print('density matrix=')
    print(ed_solver.compute_denmat())
    print('double occupancy on site-0=')
    print(ed_solver.compute_docc_i(0))

    #ed_solver.compute_rho()
    #rholoc = ed_solver.compute_rholoc()
    #print 'rholoc='
    #print rholoc
    #trrholoc = np.sum(rholoc[range(rholoc.shape[0]),range(rholoc.shape[1])])
    #print 'Tr[rholoc]=', trrholoc

    rholoc = ed_solver.compute_rholoc_onfly()
    print('rholoc_onfly=')
    print(rholoc)
    trrholoc = np.sum(rholoc[list(range(rholoc.shape[0])),list(range(rholoc.shape[1]))])
    print('Tr[rholoc]=', trrholoc)

    #for i in range(ed_solver.bipart_smap.shape[0]):
    #    values = ed_solver.bipart_smap[i]
    #    print values[0], values[1], ed_solver.strb_red.format(values[1]), values[2], ed_solver.strb_red.format(values[2])


