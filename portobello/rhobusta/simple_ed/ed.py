
'''
Created on Jul 17, 2018

@author: tl596@physics.rutgers.edu
'''

from .basis import table_ep, table_es, table_ep2_spin_symm
from scipy.sparse import csc_matrix, lil_matrix
from scipy.sparse.linalg import eigsh
import numpy as np
from numba import jit

Instance = None
is_ed_initialized = False

def getInstance(*args):#singleton
    global Instance
    if Instance is None:
        Instance = simple_ed(*args)
    return Instance

@jit(nopython=True)
def find_count(i,j,bsr,norb,bsltmp):
    # extract the first j bits from bsr and count the 1s
    bit_tmp = ( ((1 << j) - 1)  &  (bsr >> (norb-j) ) )
    #print 'bsr', j, self.strb.format(bsr), self.strb.format(bit_tmp), self.strb.format(bsr)[:j]
    count = 0
    while ( bit_tmp ):
        count += bit_tmp &1
        bit_tmp >>=1
    # extract the first i bits from bsltmp and count the 1s
    bit_tmp = ( ((1 << i) - 1)  &  (bsltmp >> (norb-i) ) )
    #print 'bsltmp', i, self.strb.format(bsltmp), self.strb.format(bit_tmp), self.strb.format(bsltmp)[:i]
    while ( bit_tmp ):
        count += bit_tmp &1
        bit_tmp >>=1
    return count

@jit(nopython=True)
def build_cid_cj_csc(i, j, basis, bit_max, norb, debug=False):
    #print 'i=',i, 'j=', j, 'bit_max=', bit_max
    row_ind = [] 
    col_ind = []
    data = [] #
    for bsrid,bsr in enumerate(basis):        
        # temporary bit for fliping the bit on j and i.
        tmp_bit1 = bit_max>>j  
        tmp_bit2 = bit_max>>i
        #print i,j,'bsrid=',bsrid,'bsr=',bsr,'',self.strb.format(bsr),'tmp_bit1=',tmp_bit1,self.strb.format(tmp_bit1),'tmp_bit2=',tmp_bit2,self.strb.format(tmp_bit2), self.strb.format(bit_max>>j), self.strb.format(bit_max>>i)
        
        # check if bit on j is 1 and if bit on i is 0 and i!=j
        if (tmp_bit1&bsr)!=tmp_bit1 or (tmp_bit2&bsr)==tmp_bit2 and i!=j:
            continue
        # annhilate particle on j
        bsltmp = bsr ^ tmp_bit1
        # create particles on i
        bsl = bsltmp | tmp_bit2
        # binary search the index for the final state bsl
        id_bsl = np.searchsorted(basis, bsl)
        #print( id_bsl, bsl, basis[id_bsl], len(basis) )
        if bsl != basis[id_bsl]: # The c_i^\dagger c_j may lead to a state that is not in the symmetry constrained states.
            continue
        else:
            bslid = id_bsl       
        #determine sign
        count = find_count(i,j,bsr,norb,bsltmp)
        sign = 1.
        if count%2 == 1:
            sign = -1.
        #if debug:
        #    print np.binary_repr(bsr)[min(i,j)+1:max(i,j)], sign#self.strb.format(bsr)[min(i,j)+1:max(i,j)], sign
        #    #print i,j,'bsrid=',bsrid,'bsr=',bsr,'',self.strb.format(bsr),'tmp_bit1=',tmp_bit1,self.strb.format(tmp_bit1),'tmp_bit2=',tmp_bit2,self.strb.format(tmp_bit2),'bslid=',bslid,'bsl=',bsl,self.strb.format(bsl), 'cumu=',cumu
        #    print i,j,'bsrid=',bsrid,'bsr=',bsr,'',np.binary_repr(bsr),'tmp_bit1=',tmp_bit1,np.binary_repr(tmp_bit1),'tmp_bit2=',tmp_bit2,np.binary_repr(tmp_bit2),'bslid=',bslid,'bsl=',bsl,np.binary_repr(bsl), 'cumu=',cumu
        col_ind.append(bsrid)
        row_ind.append(bslid)
        data.append(sign)

    return row_ind, col_ind, data

@jit(nopython=True)
def build_fi_fjd_csc(i, j, basis, bit_max, norb, debug=False):
    row_ind = [] 
    col_ind = []
    data = []
    #build <bsl|Hone|bsr>
    for bsrid,bsr in enumerate(basis):
        #print i, j
        # temporary bit for hopping operation
        tmp_bit1 = bit_max>>j  
        tmp_bit2 = bit_max>>i
        # see if j orbital is empty and i is occupied if not continue
        #if self.strb.format(bsr)[j] != '0' or self.strb.format(bsr)[i] != '1' and i!=j: #HERE
        if (tmp_bit1&bsr)==tmp_bit1 or (tmp_bit2&bsr)!=tmp_bit2 and i!=j:
            continue
        # create particles on j
        bstmp = bsr | tmp_bit1
        # annhilate particles on i
        bsl = bstmp ^ tmp_bit2
        # binary search the index for the final state bsl
        id_bsl = np.searchsorted(basis, bsl)
        #print( id_bsl, bsl, basis[id_bsl], len(basis) )
        if bsl != basis[id_bsl]: # The c_i^\dagger c_j may lead to a state that is not in the symmetry constrained states.
            continue
        else:
            bslid = id_bsl     
        #determine sign
        # extract the first j bits from bsr and count the 1s
        bit_tmp = ( ((1 << j) - 1)  &  (bsr >> (norb-j) ) )
        #print 'bsr', j, self.strb.format(bsr), self.strb.format(bit_tmp), self.strb.format(bsr)[:j]
        count = 0
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        # extract the first i bits from bsltmp and count the 1s
        bit_tmp = ( ((1 << i) - 1)  &  (bstmp >> (norb-i) ) )
        #print 'bsltmp', i, self.strb.format(bsltmp), self.strb.format(bit_tmp), self.strb.format(bsltmp)[:i]
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        sign = 1.
        if count%2 == 1:
            sign = -1.
        #if debug:
        #  print self.strb.format(bsr)[min(i,j)+1:max(i,j)], sign
        #  print i,j,'bsrid=',bsrid,'bsr=',bsr,'',self.strb.format(bsr),'tmp_bit1=',tmp_bit1,self.strb.format(tmp_bit1),'tmp_bit2=',tmp_bit2,self.strb.format(tmp_bit2),'bslid=',bslid,'bsl=',bsl,self.strb.format(bsl), 'cumu=',cumu
        row_ind.append(bslid)
        col_ind.append(bsrid)
        data.append(sign)    
    return row_ind, col_ind, data

@jit(nopython=True)
def build_two_body_ijkl_csc(i, j, k, l, basis, bit_max, norb, debug=False):
    row_ind = [] 
    col_ind = []
    data = []
    #build <bsl|Htwo|bsr>
    for bsrid,bsr in enumerate(basis):
        # see if l and j orbital are occupied if not continue
        tmp_bit1 = bit_max>>j #2**(self.norb-1-j)
        tmp_bit2 = bit_max>>l #2**(self.norb-1-l)
        tmp_bit3 = bit_max>>k #2**(self.norb-1-k)
        tmp_bit4 = bit_max>>i #2**(self.norb-1-i)
        #if self.strb.format(bsr)[j] != '1' or self.strb.format(bsr)[l] != '1' or abs(Umatrix[i,j,k,l])<1e-12:#HERE
        if (tmp_bit1&bsr)!=tmp_bit1 or (tmp_bit2&bsr)!=tmp_bit2:
            continue
        #annhilate particle j
        bsltmp1 = bsr ^ tmp_bit1 
        #annhilate particle l
        bsltmp2 = bsltmp1 ^ tmp_bit2
        #create particle k
        bsltmp3 = bsltmp2 | tmp_bit3
        #create particle i
        bsl = bsltmp3 | tmp_bit4
        #check if bsl is in basis
        id_bsl = np.searchsorted(basis, bsl)
        #print( id_bsl, bsl, basis[id_bsl], len(basis) )
        if bsl != basis[id_bsl]: # The c_i^\dagger c_j may lead to the state that is not in the symmetry constrained states.
            continue
        else:
            bslid = id_bsl  
        # compute the sign
        count = 0
        bit_tmp = ( ((1 << j) - 1)  &  (bsr >> (norb-j) ) )
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        # extract the first i bits from bsltmp and count the 1s
        bit_tmp = ( ((1 << l) - 1)  &  (bsltmp1 >> (norb-l) ) )
        #print 'bsltmp', i, self.strb.format(bsltmp), self.strb.format(bit_tmp), self.strb.format(bsltmp)[:i]
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        bit_tmp = ( ((1 << k) - 1)  &  (bsltmp2 >> (norb-k) ) )
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        # extract the first i bits from bsltmp and count the 1s
        bit_tmp = ( ((1 << i) - 1)  &  (bsltmp3 >> (norb-i) ) )
        #print 'bsltmp', i, self.strb.format(bsltmp), self.strb.format(bit_tmp), self.strb.format(bsltmp)[:i]
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        sign = 1.
        if count%2 == 1:
            sign = -1.
        #if debug:
        #  print i, j, k, l, 'bsrid=',bsrid,'bsr=',bsr, self.strb.format(bsr),'bslid=',bslid,'bsl=',bsl, self.strb.format(bsl), 'cumu=',cumu
        #construct csc matrix index, pointer, and data
        row_ind.append(bslid)
        col_ind.append(bsrid)
        data.append(sign)
    return row_ind, col_ind, data

@jit(nopython=True)
def build_docc_op_csc(i, basis, bit_max, norb, debug=False):
    row_ind = [] 
    col_ind = []
    data = []
    for bsrid,bsr in enumerate(basis):
        tmp_bit1 = bit_max>>(i+1) #2**(self.norb-1-(i+1))
        tmp_bit2 = bit_max>>i #2**(self.norb-1-i)
        #if strb.format(bsr)[i] != '1' or strb.format(bsr)[i+1] != '1': #HERE
        if (tmp_bit1&bsr)!=tmp_bit1 or (tmp_bit2&bsr)!=tmp_bit2:
            continue
        #annhilate particle i+1
        bsltmp1 = bsr ^ tmp_bit1
        #create particle i+1
        bsltmp2 = bsltmp1 | tmp_bit1
        #annhilate particle i
        bsltmp3 = bsltmp2 ^ tmp_bit2
        #create particle i
        bsl = bsltmp3 | tmp_bit2
        # Check if id_bsl is in the basis
        id_bsl = np.searchsorted(basis, bsl)
        if bsl != basis[id_bsl]: # The c_i^\dagger c_j may lead to the state that is not in the symmetry constrained states.
            continue
        else:
            bslid = id_bsl  
        # compute the sign
        count = 0
        bit_tmp = ( ((1 << i+1) - 1)  &  (bsr >> (norb-i-1) ) )
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        # extract the first i bits from bsltmp and count the 1s
        bit_tmp = ( ((1 << i+1) - 1)  &  (bsltmp1 >> (norb-i-1) ) )
        #print 'bsltmp', i, self.strb.format(bsltmp), self.strb.format(bit_tmp), self.strb.format(bsltmp)[:i]
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        bit_tmp = ( ((1 << i) - 1)  &  (bsltmp2 >> (norb-i) ) )
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        # extract the first i bits from bsltmp and count the 1s
        bit_tmp = ( ((1 << i) - 1)  &  (bsltmp3 >> (norb-i) ) )
        #print 'bsltmp', i, self.strb.format(bsltmp), self.strb.format(bit_tmp), self.strb.format(bsltmp)[:i]
        while ( bit_tmp ):
            count += bit_tmp &1
            bit_tmp >>=1
        sign = 1.
        if count%2 == 1:
            sign = -1.
        #if debug:
        #    print i, 'bsrid=',bsrid,'bsr=',bsr, self.strb.format(bsr),'bslid=',bslid,'bsl=',bsl, self.strb.format(bsl), 'cumu=',cumu
        #construct csc matrix index, pointer, and data
        row_ind.append(bslid)
        col_ind.append(bsrid)
        data.append(sign)  
    return row_ind, col_ind, data

@jit(nopython=True)
def build_rholoc_onfly(basis,gs_wf,rholoc,bipart_smap):
    '''
    Compute local reduced many-body density matrix onfly (without storing |\phi><\phi|)
    '''
    for i in range(len(basis)):
        for j in range(len(basis)):
            #iidx = np.where(basis==bipart_smap[i,0])[0][0]
            #jidx = np.where(basis==bipart_smap[j,0])[0][0]
            #print(iidx, basis[iidx], bipart_smap[i,0], jidx, basis[jidx], bipart_smap[j,0])
            #if bipart_smap[i,2] == bipart_smap[j,2] and abs(gs_wf[iidx]*gs_wf[jidx]) > 1e-12:
            #    rholoc[bipart_smap[i,1],bipart_smap[j,1]] += gs_wf[iidx]*gs_wf[jidx]#M[iidx,jidx]        
            if bipart_smap[i,2] == bipart_smap[j,2] and abs(gs_wf[i]*gs_wf[j]) > 1e-12:
                rholoc[bipart_smap[i,1],bipart_smap[j,1]] += np.conjugate(gs_wf[i])*gs_wf[j]#M[iidx,jidx]        
    return rholoc

class simple_ed(object):
    '''
    Exact diagonalization class aim to solve general impurity Hamiltonian.

    For both the usual form in DMFT and DMET,
    H_imp = H1E_{i,j}*C^\dagger_{i}C_{j} + D_{i,a} C^\dagger_{i}f_{a} + H.C. + Lambda_{a,b}f^\dagger_{a}f_{b}

    and the RISB form
    H_imp = H1E_{i,j}*C^\dagger_{i}C_{j} + D_{a,i} C^\dagger_{i}f_{a} + H.C. + Lambda_{a,b}f_{b}f_{a}^\dagger
    '''
    def __init__(self, norb, use_Ntot=False, use_Sz=False, use_Jz=False, thermal=False, RISB=True, dtype=np.float):
        '''
        Constructor.
        Input:
          norb: int. number of orbitals including spin
          use_Ntot: bool. If using particle number symmetry
          use_Sz: bool. If using Sz symmetry
          thermal: bool. If performing thermal calculation.
          RISB: bool. If performing RISB calculation
          dtype: dtype. data type of the Hamiltonian
        '''
        global is_ed_initialized
        self.norb = norb # number of orbitals
        self.use_Ntot = use_Ntot # use Ntot symmetry
        self.use_Sz = use_Sz # use Sz symmtery
        self.thermal = thermal # use thermal ensemble Not implemented yet!
        self.RISB = RISB # RISB or normal Hamiltonian
        self.strb = '{0:0'+str(norb)+'b}' # string to convert integer to binary string
        self.strb_red = '{0:0'+str(norb//2)+'b}' # string to convert integer to binary string in reudced Hilbert space
        self.data_type = dtype # data type of the Hamiltonian

        # create basis in the ground space half-filled and optionally Sz=0.
        if use_Ntot == False and use_Sz == False: # no symmetry
            self.basis = np.arange(0,2**self.norb)
        if use_Ntot == True and use_Sz == False: # Ntot symmetry
            self.basis = table_ep(norb,norb//2)
        if use_Ntot == True and use_Sz == True: # Ntot and Sz symmetry
            print("l=",norb//4)
            self.basis = table_es(norb,norb//2,0) # adler: not sure what this is, table_ep2_spin_symm() is working in ied, but not here is a bigger space 
        self.hsize = len(self.basis) # hilbert space size
        print('size of basis=', len(self.basis), 'type of basis=', type(self.basis))
        #print 'basis='
        #for bs in self.basis:
        #  print bs, self.strb.format(bs)
        
        if not is_ed_initialized:
            # build operators
            self.build_denmat_op() # density matrix operators. TODO: enforcing hopping structure to speed up the process.
            self.build_docc_op() # double occupancy operators 
            self.build_S2_op()# build total S2

            # create map between the system and local Hilbert space
            self.build_bipart_smap(debug=False)#True)
            
            is_ed_initialized = True

    def build_bipart_smap(self,debug=False):
        '''
        build the bipartite state map between system, local and enviroment in to a dictionary
        with key: system state, element: [local state, enviornment state]
        Another way is 2D array row index correspond to basis set, column index correspond to
        representation of [system, local, environment].
        '''
        # Dictionary
        #self.bipart_smap = {}
        #for s in self.basis:
        #    bs = self.strb.format(s)
        #    #print s, bs
        #    smap[s] = strb.format(s)
        #    # site-1
        #    self.bipart_smap[s] = [int(bs[:self.norb//2],2)]
        #    # site-2
        #    self.bipart_smap[s].append(int(bs[self.norb//2:],2))
        # Can be stored as 2D matrix
        self.bipart_smap = np.zeros((len(self.basis),3),dtype=np.int32)
        for i in range(len(self.basis)):
            self.bipart_smap[i,0] = self.basis[i]
            bs = self.strb.format(self.basis[i])
            self.bipart_smap[i,1] = int(bs[:self.norb//2],2)
            self.bipart_smap[i,2] = int(bs[self.norb//2:],2)
        if debug:
            #for s in self.basis:
                #print self.strb.format(s), self.strb_red.format(self.bipart_smap[s][0]), self.strb_red.format(self.bipart_smap[s][1])
            for i in range(len(self.basis)):
                print(self.strb.format(self.bipart_smape[i][0]), self.strb_red.format(self.bipart_smape[i][1]),  self.strb_red.format(self.bipart_smape[i][2]))

    def trenv(self, M):
        '''
        trace out the environment degrees of freedom of a matrix M
        NOTE: This routine is only applicable for small number of orbitals < 4. It grows exponentially with the
        number of orbitals. 
        Input:
          M: numpy.array
        '''
        #print self.bipart_smap
        #ns = len(self.basis)
        no = 2**(self.norb//2)# special case for single-orbital#int(np.log2(ns))

        #enlarge M to Mijkl tensor, where i,j is the state index for site 1, and
        #k,l is the state index for site 2.
        #Mijkl = np.zeros((no,no,no,no),dtype=M.dtype) # The size of this ndarray is too large for more than 3 orbital
        #for i in self.basis:#loop over system basis
        #    for j in self.basis:#loop over system basis
        #        iidx = np.where(self.basis==i)[0][0]
        #        jidx = np.where(self.basis==j)[0][0]
        #        Mijkl[self.bipart_smap[i][0],self.bipart_smap[i][1],self.bipart_smap[j][0],self.bipart_smap[j][1]] = M[iidx,jidx]
        #trenvM = np.einsum("ikjk",Mijkl)
        
        trenvM = lil_matrix((no,no),dtype=M.dtype)
        for i in range(len(self.basis)):
            for j in range(len(self.basis)):
                if self.bipart_smap[i,2] == self.bipart_smap[j,2] and abs(M[i,j]) > 1e-12:
                    trenvM[self.bipart_smap[i,1],self.bipart_smap[j,1]] += M[i,j]
                
        return trenvM

    def trloc(self, M):
        '''
        trace out the local degrees of freedom of a matrix M.
        NOTE: This routine is only applicable for small number of orbitals < 4. It grows exponentially with the
        number of orbitals. 
        Input:
          M: numpy.array
        '''
        #ns = len(self.basis)
        no = 2**(self.norb//2)# special case for single-orbital#int(np.log2(ns))

        #enlarge M to Mijkl tensor, where i,j is the state index for site 1, and
        #k,l is the state index for site 2.
        #Mijkl = np.zeros((no,no,no,no),dtype=M.dtype)
        #for i in self.basis:#loop over system basis
        #    for j in self.basis:#loop over system basis
        #        iidx = np.where(self.basis==i)[0][0]
        #        jidx = np.where(self.basis==j)[0][0]
        #        Mijkl[self.bipart_smap[i][0],self.bipart_smap[i][1],self.bipart_smap[j][0],self.bipart_smap[j][1]] = M[iidx,jidx]
        #trlocM = np.einsum("kikj",Mijkl)

        trlocM = lil_matrix((no,no),dtype=M.dtype)
        for i in range(len(self.basis)):
            for j in range(len(self.basis)):
                if self.bipart_smap[i,1] == self.bipart_smap[j,1] and abs(M[i,j]) > 1e-12:
                    trlocM[self.bipart_smap[i,2],self.bipart_smap[j,2]] += M[i,j]    
    
        return trlocM

    def enlarge_loc2sys(self,M):
        '''
        enlarge a local matrix M to system Hilbert space, i.e., the operation M \otimes I.
        NOTE: This routine is only applicable for small number of orbitals < 4. It grows exponentially with the
        number of orbitals. 
        Input:
          M: numpy.array
        '''
        Msys = np.zeros((self.hsize,self.hsize),dtype=M.dtype)
        for i,s1 in enumerate(self.basis):
            for j,s2 in enumerate(self.basis):
                if self.bipart_smap[i,2] == self.bipart_smap[j,2]:
                    Msys[i,j] += M[self.bipart_smap[i,1],self.bipart_smap[j,1]]
        return Msys

    def enlarge_env2sys(self,M):
        '''
        enlarge a environment matrix M to system Hilbert space, i.e., the operation M \otimes I.
        NOTE: This routine is only applicable for small number of orbitals < 4. It grows exponentially with the
        number of orbitals. 
        Input:
          M: numpy.array
        '''
        Msys = np.zeros((self.hsize,self.hsize),dtype=M.dtype)
        for i,s1 in enumerate(self.basis):
            for j,s2 in enumerate(self.basis):
                if self.bipart_smap[i,1] == self.bipart_smap[j,1]:
                    Msys[i,j] = M[self.bipart_smap[i,2],self.bipart_smap[j,2]]
        return Msys

    def build_two_body(self,Umatrix, debug=False):
        '''
        build two-body interaction, which usually does not change during the self-consistent iteration
        and can be build once and for all.
        Input:
          Umatrix: numpy array, with index (i,j,k,l) representing 0.5*V_ijkl*CH_i*CH_k*C_l*C_j
          dtype: data type of the matrix
        '''
        bit_max = 2**(self.norb-1)
        # Slow
        #indptr, indices, bsrids, data, cumu = build_two_body_csc(Umatrix, self.basis, bit_max, self.norb)
        #print indptr
        #print indices
        #print data
        #self.Htwo = csc_matrix( (data, indices, indptr), shape=(self.hsize,self.hsize),dtype=self.data_type)
        #print self.Htwo.todense()
        
        #Faster
        self.Htwo = csc_matrix((self.hsize,self.hsize), dtype=self.data_type)
        for i in range(self.norb//2):
            for j in range(self.norb//2):
                for k in range(self.norb//2):
                    for l in range(self.norb//2):
                        # check if l==j or i==k or U=0, if true it has 0 contribution
                        if l==j or i==k or abs(Umatrix[i,j,k,l])<1e-12:
                            continue # 0 contribution
                        else:
                            row_ind, col_ind, data = build_two_body_ijkl_csc(i, j, k, l, self.basis, bit_max, self.norb)
                            self.Htwo +=  0.5*Umatrix[i,j,k,l]*csc_matrix( (data, (row_ind, col_ind)), shape=(self.hsize,self.hsize),dtype=self.data_type)
                        #print i,j,k,l,Umatrix[i,j,k,l]

    def build_one_body(self, H1E, D, Lambdac, debug=False):
        '''
        build one body part of Hamiltonian using denmat operators.
        Input:
          H1E: numpy array, with index (i,j). local part of one-body hamiltonian
          D: numpy array, with index (i,j). hybridization part of one-body hamiltonian
          Lambdac: numpy array, with index (i,j). bath part of one-body hamiltonian
          dtype: data type of the matrix
          debug: bool. print out debug message
        '''
        self.Hone = csc_matrix((self.hsize,self.hsize),dtype=self.data_type)
        self.Honeloc = csc_matrix((self.hsize,self.hsize),dtype=self.data_type) # local part of the one-body term

        #build local
        for i in range(0,self.norb//2):
            for j in range(0,self.norb//2):
                self.Hone += H1E[i,j]*self.denmat_op[(i,j)]
                self.Honeloc += H1E[i,j]*self.denmat_op[(i,j)]
        #build bath
        for i in range(self.norb//2,self.norb):
            for j in range(self.norb//2,self.norb):
                if self.RISB:
                    self.Hone += Lambdac[i-self.norb//2,j-self.norb//2]*self.ffd_op[(j,i)]
                else:
                    self.Hone += Lambdac[i-self.norb//2,j-self.norb//2]*self.denmat_op[(i,j)]

        #build hyb
        for i in range(0,self.norb//2):
            for j in range(self.norb//2,self.norb):
                if self.RISB:
                    self.Hone += D[j-self.norb//2,i]*self.denmat_op[(i,j)]
                else:
                    self.Hone += D[i,j-self.norb//2]*self.denmat_op[(i,j)]      

        #build hyb.conj().T
        for i in range(self.norb//2,self.norb):
            for j in range(0,self.norb//2):
                if self.RISB:
                    self.Hone += D.conj().T[j,i-self.norb//2]*self.denmat_op[(i,j)]
                else:
                    self.Hone += D.conj().T[i-self.norb//2,j]*self.denmat_op[(i,j)]
       

    def build_Ham(self, s2pen=0.0, debug=False):
        '''
        build the Hamiltonian and return Hamiltonian
        '''
        self.Ham = self.Hone + self.Htwo #+ s2pen*self.S2
        if debug:# check hermitian
            print(len(self.Ham.nonzero()[0]))
            print('nonzero element in sparse Ham=', self.Ham.nonzero())
            if abs( (self.Ham - self.Ham.getH()).max() ) > 1e-12:
                print('Hamiltonian is not Hermitian! H.getH()-H=', abs( (self.Ham - self.Ham.getH()).max() ) > 1e-12)
                return self.Ham

    def build_denmat_op(self,debug=False):
        '''
        build the density matrix operators into a dictionary.
        denmat_op: key: tuple (i,j) indicating the orbital i and j.
                   element: scipy.sparse.csc_matrix storing the operator C^\dagger_i C_j
        '''
        self.denmat_op = {}
        bit_max = 2**(self.norb-1)
        use_numba = True
        for i in range(self.norb):
            for j in range(self.norb):
                row_ind, col_ind, data = build_cid_cj_csc(i, j, self.basis, bit_max, self.norb, debug=False)
                #print 'i=',i,'j=',j
                #print 'row_ind=',row_ind
                #print 'col_ind=',col_ind
                #print 'data=',data 
                self.denmat_op[(i,j)] = csc_matrix( (data, (row_ind, col_ind)), shape=(self.hsize,self.hsize),dtype=self.data_type)

        if self.RISB:
            # build RISB f_i f_j^\dagger operator
            self.ffd_op = {}
            for i in range(self.norb//2,self.norb):
                for j in range(self.norb//2,self.norb):
                    row_ind, col_ind, data = build_fi_fjd_csc(i, j, self.basis, bit_max, self.norb, debug=False)
                    #print 'i=',i,'j=',j
                    #print 'row_ind=',row_ind
                    #print 'col_ind=',col_ind
                    #print 'data=',data 
                    self.ffd_op[(i,j)] = csc_matrix( (data, (row_ind, col_ind)), shape=(self.hsize,self.hsize),dtype=self.data_type)

    def build_S2_op(self,debug=False):
        '''
        build total S2 operator
        '''
        #build total spin operator S+
        Sp = csc_matrix((self.hsize,self.hsize),dtype=self.data_type)
        for i in range(self.norb//2):
            #Sp += FH_list[2*i].dot(FH_list[2*i+1].getH())
            Sp += self.denmat_op[(2*i,2*i+1)]
        #build total spin operator S-
        Sm = csc_matrix((self.hsize,self.hsize),dtype=self.data_type)
        for i in range(self.norb//2):
            #Sm += FH_list[2*i+1].dot(FH_list[2*i].getH())
            Sm += self.denmat_op[(2*i+1,2*i)]
        #build total spin opertor Sz
        Sz = csc_matrix((self.hsize,self.hsize),dtype=self.data_type)
        for i in range(self.norb//2):
            #Sz += ( 0.5*FH_list[2*i].dot(FH_list[2*i].getH()) - 0.5*FH_list[2*i+1].dot(FH_list[2*i+1].getH()) )
            Sz += ( 0.5*self.denmat_op[(2*i,2*i)] - 0.5*self.denmat_op[(2*i+1,2*i+1)] )
        #build S^2 operator
        self.S2 = Sm.dot(Sp)+Sz.dot(Sz)+Sz
        self.Sz = Sz
    

    def build_docc_op(self,debug=False):
        '''
        build the double occupancy operators into a dictionary.
        denmat_op: key: int i indicating the orbital i (even number).
                   element: scipy.sparse.csc_matrix storing the operator C^\dagger_{i}C_{i}C^\dagger_{i+1}C_{i+1}
        '''
        self.docc_op = {}
        bit_max = 2**(self.norb-1)
        for i in range(0,self.norb,2):
            row_ind, col_ind, data = build_docc_op_csc(i, self.basis, bit_max, self.norb, debug=False)
            #print i,j
            #print indices
            #print indptr
            #print data 
            self.docc_op[i] = csc_matrix( (data, (row_ind, col_ind)), shape=(self.hsize,self.hsize),dtype=self.data_type)

    def diagonalize(self,k=2,which='SA',tol=1e-12):
        '''
        diagonalize the Hamiltonian
        '''
        vals, vecs = eigsh(self.Ham,k=k,which=which,tol=tol)
        self.gs_wf = vecs[:,0]
        self.gs_ene = vals[0]
        print('3 lowest energies=',vals[:3])
        f = open('EMBED_ENE.dat','w')
        print('# Energy        S2        Sz', file=f)
        for i in range(k):
            S2 = vecs[:,i].conj().T.dot(self.S2.dot(vecs[:,i]))
            Sz = vecs[:,i].conj().T.dot(self.Sz.dot(vecs[:,i]))
            print(vals[i], S2, Sz, file=f) 
        f.close()
        return self.gs_wf

    def compute_denmat(self):
        '''
        Compute denstiy matrix.
        Input:
          dtype: data dtype
        Return:
          denmat: numpy.array. Densty matrix, <c^\dagger_i c_j>, of the system.
        '''
        denmat = np.zeros((self.norb,self.norb),dtype=self.data_type)
        for i in range(self.norb):
            for j in range(self.norb):
                denmat[i,j] = self.gs_wf.conj().T.dot(self.denmat_op[(i,j)].dot(self.gs_wf))
        return denmat

    def compute_Eloc(self):
        '''
        Compute local energy including local one and two-body term from a given wavefunction.
        Input:
        Return:
          Eloc: float. Total local energy.
        '''
        return self.gs_wf.conj().T.dot((self.Htwo+self.Honeloc).dot(self.gs_wf))

    def compute_Eloc_one_body(self):
        '''
        Compute local energy including local one and two-body term from a given wavefunction.
        Input:
        Return:
          Eloc: float. Total local energy.
        '''
        return self.gs_wf.conj().T.dot(self.Honeloc.dot(self.gs_wf))
    
    def compute_Eloc_two_body(self):
        '''
        Compute local energy including local one and two-body term from a given wavefunction.
        Input:
        Return:
          Eloc: float. Total local energy.
        '''
        return self.gs_wf.conj().T.dot(self.Htwo.dot(self.gs_wf))

    def compute_denmat_from_phi(self,phi):
        '''
        Compute denstiy matrix.
        Input:
          dtype: data dtype
        Return:
          denmat: numpy.array. Densty matrix, <c^\dagger_i c_j>, of the system.
        '''
        denmat = np.zeros((self.norb,self.norb),dtype=self.data_type)
        for i in range(self.norb):
            for j in range(self.norb):
                denmat[i,j] = phi.conj().T.dot(self.denmat_op[(i,j)].dot(phi))
        return denmat

    def compute_rholoc_onfly(self):
        '''
        Compute local reduced many-body density matrix onfly (without storing |\phi><\phi|)
        '''
        no = 2**(self.norb//2)# special case for single-orbital#int(np.log2(ns))
        rholoc = np.zeros((no,no),dtype=self.data_type)
        self.rholoc = build_rholoc_onfly(self.basis, self.gs_wf, rholoc, self.bipart_smap)        
        return self.rholoc

    def compute_rho(self):
        '''
        Compute many-body density matrix.
        NOTE: This method only apply to less than 4 orbitals. For larger number of orbitals, the memory size
        would grow exponentially.
        '''
        #Phi = self.gs_wf.reshape((self.gs_wf.shape[0],1))# The product of Phi.Phi.conj().T becomes too large for more than 4 orbital
        #self.rho = Phi.dot(Phi.conj().T)
        Phi = csc_matrix(self.gs_wf.reshape((self.gs_wf.shape[0],1)))# The product of Phi.Phi.conj().T becomes too large for more than 4 orbital
        self.rho = Phi.dot(Phi.getH())
        #print 'rho='
        #print self.rho

    def compute_rholoc(self):
        '''
        Compute local reduced many-body density matrix
        NOTE: This method only apply to less than 4 orbitals. For larger number of orbitals, the memory size
        would grow exponentially. Use compute_rholoc_onfly for memory saving routine.
        '''
        self.rholoc = self.trenv(self.rho)
        #print 'rholoc='
        #print self.rholoc
        return self.rholoc

    def compute_docc_i(self,i):
        '''
        Compute double occupancy on orbital i and i+1.
        Input:
          i: orbital to compute. has to be even number
          dtype: data dtype
        Return:
          docc: float. double occupancy.
        '''
        return self.gs_wf.conj().T.dot(self.docc_op[i].dot(self.gs_wf))

    def compute_docc_i_from_phi(self,i,phi):
        '''
        Compute double occupancy on orbital i and i+1 from a given wavefunction.
        Input:
          i: orbital to compute. has to be even number
        Return:
          docc: float. double occupancy.
        '''
        return phi.conj().T.dot(self.docc_op[i].dot(phi))

    def compute_Eloc_from_phi(self,phi):
        '''
        Compute local energy including local one and two-body term from a given wavefunction.
        Input:
        Return:
          Eloc: float. Total local energy.
        '''
        return phi.conj().T.dot((self.Htwo+self.Honeloc).dot(phi))


    def compute_Gloc(self):
        '''
        compute local impurity Green's function
        '''
        
#  def build_one_body_RISB(self, H1E, D, Lambdac, dtype=np.float, debug=False):
#    '''
#    Depricated! too slow compare to build_one_body above!
#    build one-body interaction which changes with iteration.
#    Input:
#      H1E: numpy array, with index (i,j). local part of one-body hamiltonian
#      D: numpy array, with index (i,j). hybridization part of one-body hamiltonian
#      Lambdac: numpy array, with index (i,j). bath part of one-body hamiltonian
#      dtype: data type of the matrix
#      debug: bool. print out debug message
#    '''
#    #self.Hone = csr_matrix((self.hsize,self.hsize),dtype=dtype)
#    indptr = [] 
#    indices = []
#    bsrids = []
#    data = []
#    cumu = 0
#    #build <bsl|Hone|bsr>
#    for bsrid,bsr in enumerate(self.basis):
#      indptr.append(cumu)
#      #build local
#      #print 'build local'
#      for i in range(0,self.norb/2):
#        for j in range(0,self.norb/2):
#          #print i, j
#          # temporary bit to perform hopping operation
#          tmp_bit1 = 2**(self.norb-1-j)
#          tmp_bit2 = 2**(self.norb-1-i)
#          # see if j orbital is occupied and i is empty if not continue
#          if self.strb.format(bsr)[j] != '1' or self.strb.format(bsr)[i] != '0' and i!=j or abs(H1E[i,j])<1e-12:
#            #print 'ignore'
#            #print i, j, self.strb.format(bsr), self.strb.format(bsr)[j]
#            continue
#          #print 'keep'
#          #print i, j, self.strb.format(bsr), self.strb.format(bsr)[j]
#
#          # annhilate particles on j 
#          bsltmp = bsr ^ tmp_bit1
#          # create particles on i
#          bsl = bsltmp | tmp_bit2
#          #print self.strb.format(tmp_bit1), self.strb.format(bsr), self.strb.format(bsl)
#          #print 'bsrid=',bsrid,'bsr=',bsr,'bsl=',bsl, 'cumu=',cumu
#          if bsl not in self.basis: #continue if bsl is not in the basis list
#            continue
#          else: #else look up bsl index
#            bslid = np.where(self.basis==bsl)[0][0]
#          #print 'bsrid=',bsrid,'bsr=',bsr,'bslid=',bslid,'bsl=',bsl, 'cumu=',cumu
#          #determine sign
#          #sign = 0
#          #for s in self.strb.format(bsr)[min(i,j)+1:max(i,j)]:
#          #  sign += int(s)
#          #sign = (-1.)**sign
#          sign = 0
#          for s in self.strb.format(bsr)[:j]:
#            sign += int(s)
#          for s in self.strb.format(bsltmp)[:i]:
#            sign += int(s)
#          sign = (-1.)**sign
#          if debug:
#            print self.strb.format(bsr)[min(i,j)+1:max(i,j)], sign
#            print i,j,'bsrid=',bsrid,'bsr=',bsr,'',self.strb.format(bsr),'tmp_bit1=',tmp_bit1,self.strb.format(tmp_bit1),'tmp_bit2=',tmp_bit2,self.strb.format(tmp_bit2),'bslid=',bslid,'bsl=',bsl,self.strb.format(bsl), 'cumu=',cumu
#          #construct scs matrix index, pointer, and data
#          if (bslid not in indices) or bsrids[indices.index(bslid)]!= bsrid:#if new element
#            indices.append(bslid)
#            data.append(sign*H1E[i,j])
#            bsrids.append(bsrid)
#            cumu += 1
#          else:#else add to exisiting data
#            idx = indices.index(bslid)
#            data[idx] += sign*H1E[i,j]
#      #build bath
#      #print 'build bath'
#      for i in range(self.norb/2,self.norb):
#        for j in range(self.norb/2,self.norb):
#          #print i, j
#          # temporary bit for hopping operation
#          tmp_bit1 = 2**(self.norb-1-j)
#          tmp_bit2 = 2**(self.norb-1-i)
#          # see if j orbital is empty and i is occupied if not continue
#          if self.strb.format(bsr)[j] != '0' or self.strb.format(bsr)[i] != '1' and i!=j or abs(Lambdac[i-self.norb/2,j-self.norb/2])<1e-12:
#             continue
#          # create particles on j
#          bstmp = bsr | tmp_bit1
#          # annhilate particles on i
#          bsl = bstmp ^ tmp_bit2
#          if bsl not in self.basis:
#            continue
#          else:
#            bslid = np.where(self.basis==bsl)[0][0]
#          #determine sign
#          sign = 0
#          for s in self.strb.format(bsr)[:j]:
#            sign += int(s)
#          for s in self.strb.format(bstmp)[:i]:
#            sign += int(s)
#          sign = (-1.)**sign
#          #for s in self.strb.format(bsr)[min(i,j)+1:max(i,j)]:
#          #  sign += int(s)
#          #sign = (-1.)**(abs(j-i)-sign)
#          if debug:
#            print self.strb.format(bsr)[min(i,j)+1:max(i,j)], sign
#            print i,j,'bsrid=',bsrid,'bsr=',bsr,'',self.strb.format(bsr),'tmp_bit1=',tmp_bit1,self.strb.format(tmp_bit1),'tmp_bit2=',tmp_bit2,self.strb.format(tmp_bit2),'bslid=',bslid,'bsl=',bsl,self.strb.format(bsl), 'cumu=',cumu
#          if (bslid not in indices) or bsrids[indices.index(bslid)]!= bsrid:
#            indices.append(bslid)
#            data.append(sign*Lambdac[i-self.norb/2,j-self.norb/2])
#            bsrids.append(bsrid)
#            cumu += 1
#          else:
#            idx = indices.index(bslid)
#            data[idx] += sign*Lambdac[i-self.norb/2,j-self.norb/2]
#
#      #build hybridization
#      #print 'build hyb'
#      for i in range(0,self.norb/2):
#        for j in range(self.norb/2,self.norb):
#          #print i, j
#          tmp_bit1 = 2**(self.norb-1-j)
#          tmp_bit2 = 2**(self.norb-1-i)
#          # see if j orbital is occupied and i is empty if not continue
#          if self.strb.format(bsr)[j] != '1' or self.strb.format(bsr)[i] != '0' or abs(D[i,j-self.norb/2])<1e-12:
#             continue
#          # annhilate particles on j 
#          bsltmp = bsr ^ tmp_bit1
#          # create particles on i
#          bsl = bsltmp | tmp_bit2
#          if bsl not in self.basis:#continue if bsl is not in the basis
#            continue
#          else:#else look up bsl's index
#            bslid = np.where(self.basis==bsl)[0][0]
#          #determine sign
#          #sign = 0
#          #for s in self.strb.format(bsr)[min(i,j)+1:max(i,j)]:
#          #  sign += int(s)
#          #sign = (-1.)**(sign)
#          sign = 0
#          for s in self.strb.format(bsr)[:j]:
#            sign += int(s)
#          for s in self.strb.format(bsltmp)[:i]:
#            sign += int(s)
#          sign = (-1.)**sign
#          if debug:
#            print self.strb.format(bsr)[min(i,j)+1:max(i,j)], sign
#            print i,j,'bsrid=',bsrid,'bsr=',bsr,'',self.strb.format(bsr),'tmp_bit1=',tmp_bit1,self.strb.format(tmp_bit1),'tmp_bit2=',tmp_bit2,self.strb.format(tmp_bit2),'bslid=',bslid,'bsl=',bsl,self.strb.format(bsl), 'cumu=',cumu
#          #construct csc matrix index, pointer, and data
#          if (bslid not in indices) or bsrids[indices.index(bslid)]!= bsrid:#if new element
#            indices.append(bslid)
#            data.append(sign*D[i,j-self.norb/2])
#            bsrids.append(bsrid)
#            cumu += 1
#          else:# else add to existing data
#            idx = indices.index(bslid)
#            data[idx] += sign*D[i,j-self.norb/2]
#      #print 'build hybT'
#      for i in range(self.norb/2,self.norb):
#        for j in range(0,self.norb/2):
#          #print i, j
#          tmp_bit1 = 2**(self.norb-1-j)
#          tmp_bit2 = 2**(self.norb-1-i)
#          # see if j orbital is occupied and i is empty if not continue
#          if self.strb.format(bsr)[j] != '1' or self.strb.format(bsr)[i] != '0' or abs(D.conj().T[i-self.norb/2,j])<1e-12:
#             continue
#          # annhilate particles on j 
#          bsltmp = bsr ^ tmp_bit1
#          # create particles on i
#          bsl = bsltmp | tmp_bit2
#          if bsl not in self.basis:#continue if bsl is not in the basis list
#            continue
#          else:#else look up bsl index
#            bslid = np.where(self.basis==bsl)[0][0]
#          #determine sign
#          #sign = 0
#          #for s in self.strb.format(bsr)[min(i,j)+1:max(i,j)]:
#          #  sign += int(s)
#          #sign = (-1.)**sign
#          sign = 0
#          for s in self.strb.format(bsr)[:j]:
#            sign += int(s)
#          for s in self.strb.format(bsltmp)[:i]:
#            sign += int(s)
#          sign = (-1.)**sign
#          if debug:
#            print self.strb.format(bsr)[min(i,j)+1:max(i,j)], sign
#            print i,j,'bsrid=',bsrid,'bsr=',bsr,'',self.strb.format(bsr),'tmp_bit1=',tmp_bit1,self.strb.format(tmp_bit1),'tmp_bit2=',tmp_bit2,self.strb.format(tmp_bit2),'bslid=',bslid,'bsl=',bsl,self.strb.format(bsl), 'cumu=',cumu
#          if (bslid not in indices) or bsrids[indices.index(bslid)]!= bsrid:#new element
#            indices.append(bslid)
#            data.append(sign*D.conj().T[i-self.norb/2,j])
#            bsrids.append(bsrid)
#            cumu += 1
#          else:#adde to existing data
#            idx = indices.index(bslid)
#            data[idx] += sign*D.conj().T[i-self.norb/2,j]
#
#    indptr.append(cumu)
#    #print indptr
#    #print indices
#    #print data
#    self.Hone = csc_matrix( (data, indices, indptr), shape=(self.hsize,self.hsize),dtype=dtype)
#    #print self.Hone.todense()
#
#@jit(nopython=True)
#def build_two_body_csc(Umatrix, basis, bit_max, norb, debug=False):
#    indptr = [] 
#    indices = []
#    data = []
#    bsrids = []
#    cumu = 0
#    #build <bsl|Htwo|bsr>
#    for bsrid,bsr in enumerate(basis):
#        indptr.append(cumu)
#        for i in range(norb/2):
#            for j in range(norb/2):
#                for k in range(norb/2):
#                    for l in range(norb/2):
#                        # check if l==j or i==k or U=0, if true it has 0 contribution
#                        if l==j or i==k or abs(Umatrix[i,j,k,l])<1e-12:
#                            continue # 0 contribution
#    
#                        # see if l and j orbital are occupied if not continue
#                        #tmp_bit1 = 2**(self.norb-1-l) + 2**(self.norb-1-j)
#                        #tmp_bit2 = 2**(self.norb-1-i) + 2**(self.norb-1-k)
#                        tmp_bit1 = bit_max>>j #2**(self.norb-1-j)
#                        tmp_bit2 = bit_max>>l #2**(self.norb-1-l)
#                        tmp_bit3 = bit_max>>k #2**(self.norb-1-k)
#                        tmp_bit4 = bit_max>>i #2**(self.norb-1-i)
#                        #if self.strb.format(bsr)[j] != '1' or self.strb.format(bsr)[l] != '1' or abs(Umatrix[i,j,k,l])<1e-12:#HERE
#                        if (tmp_bit1&bsr)!=tmp_bit1 or (tmp_bit2&bsr)!=tmp_bit2 or abs(Umatrix[i,j,k,l])<1e-12:
#                            continue
#                        # annhilate particles on l and j 
#                        #bsltmp = bsr ^ tmp_bit1
#                        # create particles on i and k
#                        #bsl = bsltmp | tmp_bit2
#                        #annhilate particle j
#                        bsltmp1 = bsr ^ tmp_bit1 
#                        #annhilate particle l
#                        bsltmp2 = bsltmp1 ^ tmp_bit2
#                        #create particle k
#                        bsltmp3 = bsltmp2 | tmp_bit3
#                        #create particle i
#                        bsl = bsltmp3 | tmp_bit4
#                        #check if bsl is in basis
#                        id_bsl = np.where(basis==bsl)[0]
#                        #print id, id.size
#                        if id_bsl.size == 0:
#                            continue
#                        else:
#                            bslid = id_bsl[0]
#                        # compute the sign
#                        #sign = 0
#                        #sign += self.strb.format(bsr)[:j].count('1')
#                        #sign += self.strb.format(bsltmp1)[:l].count('1')
#                        #sign += self.strb.format(bsltmp2)[:k].count('1')
#                        #sign += self.strb.format(bsltmp3)[:i].count('1')
#                        #sign = (-1.)**sign #HERE
#                        count = 0
#                        bit_tmp = ( ((1 << j) - 1)  &  (bsr >> (norb-j) ) )
#                        while ( bit_tmp ):
#                            count += bit_tmp &1
#                            bit_tmp >>=1
#                        # extract the first i bits from bsltmp and count the 1s
#                        bit_tmp = ( ((1 << l) - 1)  &  (bsltmp1 >> (norb-l) ) )
#                        #print 'bsltmp', i, self.strb.format(bsltmp), self.strb.format(bit_tmp), self.strb.format(bsltmp)[:i]
#                        while ( bit_tmp ):
#                            count += bit_tmp &1
#                            bit_tmp >>=1
#                        bit_tmp = ( ((1 << k) - 1)  &  (bsltmp2 >> (norb-k) ) )
#                        while ( bit_tmp ):
#                            count += bit_tmp &1
#                            bit_tmp >>=1
#                        # extract the first i bits from bsltmp and count the 1s
#                        bit_tmp = ( ((1 << i) - 1)  &  (bsltmp3 >> (norb-i) ) )
#                        #print 'bsltmp', i, self.strb.format(bsltmp), self.strb.format(bit_tmp), self.strb.format(bsltmp)[:i]
#                        while ( bit_tmp ):
#                            count += bit_tmp &1
#                            bit_tmp >>=1
#                        sign = (-1)**count
#                        #if debug:
#                        #  print i, j, k, l, 'bsrid=',bsrid,'bsr=',bsr, self.strb.format(bsr),'bslid=',bslid,'bsl=',bsl, self.strb.format(bsl), 'cumu=',cumu
#                        #construct csc matrix index, pointer, and data
#                        if bslid not in indices or bsrids[indices.index(bslid)]!= bsrid: # if new element
#                            indices.append(bslid)
#                            data.append(sign*0.5*Umatrix[i,j,k,l])
#                            bsrids.append(bsrid)
#                            cumu += 1
#                        else: # else add to data
#                            idx = indices.index(bslid)
#                            data[idx] += sign*0.5*Umatrix[i,j,k,l]
#    indptr.append(cumu)
#    #print indptr
#    #print indices
#    #print data
#    return indptr, indices, bsrids, data, cumu
#
