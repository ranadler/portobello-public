
'''
Created on Jul 17, 2018

@author: tl596@physics.rutgers.edu
'''


def power(base, exp):
    """ Fast power calculation using repeated squaring """
    if exp < 0:
        return 1.0 / power(base, -exp)
    ans = 1
    while exp:
        if exp & 1:
            ans *= base
        exp >>= 1
        base *= base
    return ans
