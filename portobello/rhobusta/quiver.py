#!/usr/bin/python3

'''
Created on Jun 2, 2020

@author: adler
'''

import sys
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
import matplotlib.pyplot as plt
import numpy as np

from portobello.generated.ARPES import PlaneARPES

def quiver(U, V, aX, aY, centerpoint_label, xlabel, ylabel, max_arrows = 10000): # figures = IntensityFigures

    while True:
        total = np.abs(U) + np.abs(V)

        maxval = np.max(total)

        total = np.clip(total-0.05*maxval, 0, maxval) 

        clippedU = []
        clippedV = []
        X = []
        Y = []
        for i in range(total.shape[0]):
            for j in range(total.shape[0]):
                if total[i,j]> 1e-8:
                    clippedU.append(U[i,j])
                    clippedV.append(V[i,j])
                    X.append(aX[i])
                    Y.append(aY[j])

        if len(X) < max_arrows:
            break
        
        else:    
            U = U[::2, ::2]
            V = V[::2, ::2]
            aX = aX[::2]
            aY = aY[::2]
                

    fig = plt.figure(constrained_layout=True)
    ax = fig.add_subplot()

    ax.quiver(X,Y,clippedU,clippedV)

    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    dx = (xlim[1] - xlim[0])*0.02
    dy = (ylim[1] - ylim[0])*0.02

    ax.scatter(0, 0, color='red')
    ax.text(dx, dy, centerpoint_label, color = 'red')

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    return fig, ax
            
if __name__ == '__main__':
    parser = ArgumentParserThatStoresArgv('show',add_help=True)    

    parser.add_argument("-O","--observable",
                          dest='obs',
                          default="S",
                          help="The observable to plot as a quiver")

    parser.add_argument("-X",
                          dest='x',
                          default=0,
                          type=int,
                          help="index of the x-direction on vectors")

    parser.add_argument("-Y",
                          dest='y',
                          default=1,
                          type=int,
                          help="index of the y-direction on vectors")         

    parser.add_argument("-S",
                          dest='s',
                          default=-1,
                          type=int,
                          help="index of the rep to use in selection")



    opts = parser.parse_args(sys.argv[1:])

    if opts.s >= 0:
        selector = PlaneARPES()
        selector.load(f"./pARPES.rep.h5:/")

    arpes = PlaneARPES()
    arpes.load(f"./pARPES.{opts.obs}.h5:/")

    is_scalar = arpes.num_directions == 1
    
    if not is_scalar:

        U = arpes.Z[:,opts.x,:, :, :]
        V = arpes.Z[:,opts.y,:, :, :]

       
        
        if opts.s >= 0:
            selection = selector.Z[:,0,opts.s,:,:]
            maxval = np.max(selection)
            selection = np.clip(selection-0.5*maxval, 0, maxval)
            ind = selection > 0
            
            selection[ind] = 1
            
            U=U*selection
            V=V*selection

        U = np.sum(U[:,:, :, :], axis=0)
        V = np.sum(V[:,:, :, :], axis=0)

        values=arpes.values
        U = np.tensordot(U, values, axes=(0,0))
        V = np.tensordot(V, values, axes=(0,0))

    else:
        print(arpes.values)
        print(f"plotting {arpes.values[opts.x]} as x")
        print(f"plotting {arpes.values[opts.y]} as y")
        
        if 0:
            U = np.sum(arpes.Z[:,0, opts.x, :, :], axis=0)
            V = np.sum(arpes.Z[:,0, opts.y, :, :], axis=0)  
        else:
            U = arpes.Z[0,0, opts.x, :, :]
            V = arpes.Z[0,0, opts.y, :, :]


    nk = int(np.sqrt(U.shape[0]))
    U = U.reshape(nk,nk)
    V = V.reshape(nk,nk)
    
    X = np.linspace(-arpes.width, arpes.width, nk, endpoint=True)

    quiver(U, V, X, X, arpes.hskp.labels[0].text , arpes.xlabel, arpes.ylabel)
    plt.show()


