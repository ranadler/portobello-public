from scipy import integrate as spIntegrate
from itertools import cycle, product
import numpy as np
from portobello.bus.Matrix import Matrix
from portobello.rhobusta.maxent import Maxent

from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.generated.dmft import DMFTState
from portobello.generated.self_energies import LocalOmegaFunction
from portobello.rhobusta.PEScfEngine import PEScfEngine
from portobello.rhobusta.observables import GetLabelsForPlotting

from pylatexenc.latex2text import LatexNodes2Text

class Mott:
    def __init__(self):
        self.accumulated_lifetime = 0
        self.started = False
        self.ended = False

    def check(self, omega, lifetime, tol, ttol): 
        assert(not self.ended)

        if self.started:
            self.accumulated_lifetime += lifetime
            if self.accumulated_lifetime > ttol:
                self.right_edge = omega
                self.ended = True
                return True

        if lifetime < tol:
            if not self.started:
                self.max = lifetime
                self.imax = omega
                self.accumulated_lifetime += lifetime
                self.left_edge = omega
                self.right_edge = omega
                self.started = True
            else:
                self.right_edge = omega # continues to advance
                
                if lifetime > self.max:
                    self.max = lifetime
                    self.imax = omega

        #returns True when the gap is fully defined and a new gap should be looked for
        return False

class Manipulator(DMFTState):

    def __init__(self, omega_function_to_manipulate : LocalOmegaFunction, window = "-1:1", tol = 1e-3):

        self.window = window
        self.tol = tol

        DMFTState.__init__(self)
        self.load("./dmft.h5:/")

        self.original = omega_function_to_manipulate
        copy_loc = "./copy_function.core.h5:/"
        
        self.original.store(copy_loc)
        self.manipulated = LocalOmegaFunction()
        self.manipulated.load(copy_loc)

        self.original.store(copy_loc)
        self.manipulated_spectral_function = LocalOmegaFunction()
        self.manipulated_spectral_function.load(copy_loc)

        self.original.store(copy_loc)
        self.original_spectral_function = LocalOmegaFunction()
        self.original_spectral_function.load(copy_loc)
        self.GetSpectralFunction(self.original, self.original_spectral_function)

        self.labels, u_labels_ = self.GetLabels()

    def result(self):
        return self.manipulated
        
    def store(self,filename):
        self.manipulated.store(filename)

    def plot(self, filename, *functions):
        
        import matplotlib.pyplot as plt
        from matplotlib.pyplot import cm
        import pickle
        fig,ax = plt.subplots(2,1)
        colors = cm.rainbow(np.linspace(0, 1, self.original.num_orbs*self.original.num_si))
        linestyles = ['-','--',":",'-.']

        j=0
        for i,si in self.orb_si_pairs():
            for ifunc, f in enumerate(functions):
                if ifunc==0:
                    ax[0].plot(f.omega, np.real(f.M[:,i,i,si]), linestyle = linestyles[ifunc], color = colors[j], label = f"${self.labels[si][i]}$")
                else:
                    ax[0].plot(f.omega, np.real(f.M[:,i,i,si]), linestyle = linestyles[ifunc], color = colors[j])
                ax[1].plot(f.omega, np.imag(f.M[:,i,i,si]), linestyle = linestyles[ifunc], color = colors[j])
            j+=1

        ax[0].set_ylabel("")
        fig.legend()
        with open(filename, "wb") as f:
            pickle.dump(fig,f)

    def all_plots(self, prefix = ""):
        self.plot_on_real_axis("real-self-energy-mott.pickle")
        self.plot_on_imaginary_axis("imag-self-energy-mott.pickle")
        self.plot_spectral_functions("spectral-self-energy-mott.pickle")

    def plot_spectral_functions(self, filename):
        self.GetSpectralFunction(self.original, self.original_spectral_function)
        self.GetSpectralFunction(self.manipulated, self.manipulated_spectral_function)
        self.plot(filename,self.original_spectral_function, self.manipulated_spectral_function)

    def plot_on_imaginary_axis(self, filename):
        original_on_imaginary_axis = self.ContinueFromRealToImaginaryAxis(self.original)
        manipulated_on_imaginary_axis = self.ContinueFromRealToImaginaryAxis(self.manipulated)
        self.plot(filename, self.sig, original_on_imaginary_axis, manipulated_on_imaginary_axis)

    def plot_on_real_axis(self,filename):
        self.plot(filename, self.original, self.manipulated)

    def ContinueFromRealToImaginaryAxis(self, real_axis_selfenergy):

        def MakeImagAxisFunc():
            f = LocalOmegaFunction(num_omega=self.num_freqs, 
                                            num_orbs=self.original.num_orbs,
                                            num_si=self.original.num_si)
            f.allocate()
            f.omega = np.array([(2*i+1)*np.pi/self.beta for i in range(0,self.num_freqs)])
            return f

        imaginary_axis_selfenergy = MakeImagAxisFunc()

        iomega = 1j*imaginary_axis_selfenergy.omega
        omega = real_axis_selfenergy.omega

        for i,si in self.orb_si_pairs():
            for iom in range(imaginary_axis_selfenergy.num_omega):
                Aw = -1./np.pi * np.imag(1./(omega[:] - real_axis_selfenergy.M[:,i,i,si]))

                Kw = 1./(iomega[iom] - omega[:])
                Gaux = spIntegrate.simps(Aw*Kw, x = omega)
                imaginary_axis_selfenergy.M[iom,i,i,si] = iomega[iom] + self.sig.moments[0,i,i,si] - 1./Gaux

        return imaginary_axis_selfenergy

    def GetSpectralFunction(self, real_axis_selfenergy, real_axis_spectral_function):
        omega = real_axis_selfenergy.omega

        for i,si in self.orb_si_pairs():
            real_axis_spectral_function.M[:,i,i,si] = -1./np.pi * np.imag(1./(omega[:] - real_axis_selfenergy.M[:,i,i,si]))

    def GetLabels(self, which_shell=0):
        self.orbs = PEScfEngine.GetRepOrbitals(self.rep) # orbital indices

        odef = SelectedOrbitals()
        odef.load("./projector.h5:/def/")

        obsv = ''
        if self.nrel > 1:
            obsv = "j"
        else:
            obsv = 'Ylm'

        vals, orbs = GetLabelsForPlotting(self, shell=odef.shells[which_shell], opn=obsv, subspace_expr = odef.subspace_expr)
        orbs = [f"${val}$" for val in orbs]
        return vals, orbs

    def find_mott_gaps(self):
        window = self.window.split(":")
        window = [float(w) for w in window]

        gaps = np.empty((self.original.num_orbs, self.original.num_si), dtype=list)
        for i,si in self.orb_si_pairs():
            gaps[i,si] = [Mott()]
            for iom, om in enumerate(self.original.omega):
                if om > window[0] and om < window[1]:
                    if (gaps[i,si][-1].check(iom, abs(self.original_spectral_function.M[iom,i,i,si]), self.tol, 10*self.tol)):
                        gaps[i,si].append(Mott())
        
            while len(gaps[i,si]):

                if not gaps[i,si][-1].started: #nothing found
                    gaps[i,si].pop()
                    continue
                
                elif gaps[i,si][-1].right_edge == self.original.num_omega-1: 
                    gaps[i,si].pop()
                    continue

                elif gaps[i,si][0].left_edge == 0:
                    gaps[i,si].pop(0)
                    continue
                
                elif gaps[i,si][0].right_edge - gaps[i,si][0].left_edge < 2: #not a real gap
                    gaps[i,si].pop(0)
                    continue

                else:
                    break
            
        return gaps

    def zero_out_gaps(self):
        #finds gaps where the spectral weight is lower than tol, and sets weight to exactly zero
        #returns indices where spectral weight of all orbitals has been set to zero
        completely_gapped = np.ones(self.original.num_omega, dtype=int)*self.original.num_orbs*self.original.num_si
        gaps = self.find_mott_gaps()
        for i,si in self.orb_si_pairs():
            for gap in gaps[i,si]:
                self.manipulated.M[gap.left_edge:gap.right_edge+1,i,i,si] = self.manipulated.M[gap.left_edge:gap.right_edge+1,i,i,si].real 
                completely_gapped[gap.left_edge:gap.right_edge+1] -= 1
                
                omega_left = self.original.omega[gap.left_edge]
                omega_right = self.original.omega[gap.right_edge]
                print(f"Found gap between {omega_left} and {omega_right} to orbital " + LatexNodes2Text().latex_to_text(self.labels[si][i]))
        return completely_gapped == 0

    @classmethod
    def delta_function_divergence(cls, omega, middle_of_bump, area_of_bump):
        f = np.zeros(omega.shape, dtype=float)
        f[middle_of_bump] = area_of_bump
        return f
        
    @classmethod
    def lorentzian_divergence(cls, omega, middle_of_bump, area_of_bump):
        lorentzian = 1/(omega-middle_of_bump)**2
        lorentzian_area = spIntegrate.simps(lorentzian, x = omega)
        return lorentzian * area_of_bump / lorentzian_area
        
    @classmethod
    def zero_out(cls, omega, middle_of_bump, area_of_bump):
        return np.zeros(omega.shape, dtype=float)

    def replace_bumps_with_func(self, func, allowed_bump_width = 0.1):
        #find small bumps in self energy between gaps, sets the spectral weight to zero
        #adds new function in place of this bump if one is provided (see zero_out for form of function)
        
        gaps = self.find_mott_gaps()
        for i,si in self.orb_si_pairs():

            while len(gaps[i,si]) > 1:

                found = False
                for igap in range(len(gaps[i,si])-1):

                    left_gap = gaps[i,si][igap]
                    right_gap = gaps[i,si][igap+1]

                    left = left_gap.right_edge
                    right = right_gap.left_edge

                    omega_left = self.original.omega[left]
                    omega_right = self.original.omega[right]

                    if omega_right-omega_left < allowed_bump_width:
                        gap_left = igap
                        gap_right = igap + 1
                        found = True
                        break

                if not found:
                    break

                bump_left = gaps[i,si][gap_left].right_edge
                bump_right = gaps[i,si][gap_right].left_edge
                    
                omega_left = self.original.omega[bump_left]
                omega_right = self.original.omega[bump_right]
                omega_mid = omega_left+ (omega_right-omega_left) / 2
                omega = self.original_spectral_function.omega[:]

                bump_mid = np.argmax(omega > omega_mid)
                bump_area = spIntegrate.simps(self.original_spectral_function.M[bump_left:bump_right+1,i,i,si], x = omega[bump_left:bump_right+1])

                self.manipulated_spectral_function.M[:,i,i,si] = self.original_spectral_function.M[:,i,i,si]
                self.manipulated_spectral_function.M[bump_left:bump_right+1,i,i,si] = 0
                self.manipulated_spectral_function.M[:,i,i,si] += func(omega, bump_mid, bump_area)
                
                ImG = -self.manipulated_spectral_function.M[:,i,i,si] * np.pi
                
                ReG = Maxent.KK(self.manipulated_spectral_function.omega, ImG)

                self.manipulated.M[:,i,i,si] = self.manipulated_spectral_function.omega - 1/(ReG +ImG*1j)

                print(f"Manipulated bump between {omega_left} and {omega_right} to orbital " + LatexNodes2Text().latex_to_text(self.labels[si][i]))
                gaps[i,si].pop(gap_left)

    def orb_si_pairs(self):
        return product(range(self.original.num_orbs),range(self.original.num_si))

if __name__ == '__main__':

    sig = LocalOmegaFunction()
    sig.load("real-imp-self-energy.h5:/")

    m = Manipulator(sig)
    m.zero_out_gaps()
    m.replace_bumps_with_func(m.delta_function_divergence)
    m.all_plots()