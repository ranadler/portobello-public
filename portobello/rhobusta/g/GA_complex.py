#!/usr/bin/python3

'''
Created on Nov 12, 2018

@author: adler
'''
import sys
from argparse import ArgumentDefaultsHelpFormatter
from itertools import product

import h5py
import numpy as np
from numpy import hstack
from portobello.bus.Matrix import Matrix
from portobello.generated.PEScf import Histogram
from portobello.rhobusta import rhobusta
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.g import ga_complex_fermiT
from portobello.rhobusta.g.ga_complex_fermiT import TerminateEDXServer
from portobello.rhobusta.g.GEngine import GEngine
from portobello.rhobusta.g.utils import (Hermitian_list, U_matrix_slater,
                                         complexHcombination,
                                         construct_Hsym_list, get_average_uj,
                                         inverse_complexHcombination,
                                         inverse_realHcombination,
                                         realHcombination)
from portobello.rhobusta.options import SaveOptions
from portobello.rhobusta.orbitals import PrintMatrix
from portobello.rhobusta.PEScfEngine import GetPEScfOptionsParser
from portobello.rhobusta.simple_ed.basis import countBits
from portobello.rhobusta.simple_ed.ed import getInstance, simple_ed
from portobello.symmetry.local import GetCGMatrix, GetRealSHMatrix
from scipy import optimize


class SimpleGA:
    
    def __init__(self, opts):        
        self.ri = GEngine(opts)
        #l = (self.ri.dim - 1) /2
        l = self.ri.orbBandProj.l
        
        # normal initialization 
        R = np.copy(self.ri.R[:,:,:])
        lambd = np.copy(self.ri.Lambda)
        self.nrel = self.ri.dft.nrel
        self.isrel = self.nrel == 2
        self.spin_sym = self.ri.num_si == 1 and not self.isrel  
        
        if self.isrel:
            print('relativistic mode')
        elif self.spin_sym:
            print('spin symmetry conserved')
        else:
            print('spin symmetry broken')
          
        if opts.crys_sym:   
            print('using single-particle basis with crystal symmetry')
            # orbital irreducible representation
            self.Hsym_list, self.tHsym_list = construct_Hsym_list(self.ri.rep, dtype=np.complex)
            #print(self.Hsym_list) 
        else:
            print('using single-particle basis without crystal symmetry')
            self.Hsym_list, self.tHsym_list = Hermitian_list(self.ri.dim)  # True=hermitian

        #basis = 'cubic' # rotate to cubic basis
        self.T = 1.0/opts.beta # temperature

        print('----------------------- Computing GA/RISB root problem with U=%g J=%g T=%g -----------------------' % (opts.U, opts.J, self.T))
        # unitary transform matrix from spherical to cubic
        
        # Enlarged to spin-space
        self.norb2 = self.ri.dim * 2
        if self.nrel > 1:
            self.norb2 = self.ri.dim
        
        # initialize solver
        if opts.solver == 'spci':
            self.ed_solver = opts.solver
        elif opts.solver == 'ied':
            self.ed_solver = opts.solver
        elif opts.solver == 'ged':
            self.ed_solver = opts.solver
        elif opts.solver == 'simple_ed':
            use_Sz = True
            use_Jz = False
            if self.isrel:
                use_Sz = False
                use_Jz = True
            # for better documentation we make this explicit
            use_Ntot = True 
            RISB = True
            thermal = False
            
            self.ed_solver = getInstance(2*self.norb2, use_Ntot, use_Sz, use_Jz, thermal, RISB, np.complex)
        else:
            raise Exception('please select the following ED solvers: spci, ged, simple_ed, ied')
        
        self.Ufullmat = self.GetUMatrix(opts, l)  # needs to know the solver
        
        if opts.solver == 'simple_ed':
            self.ed_solver.build_two_body(self.Ufullmat) # needs the U matrix
        
        sys.stdout.flush()

        # Initialize variational parameters      
        if self.spin_sym or self.isrel:
            self.r0 = inverse_complexHcombination(R[:,:,0], self.Hsym_list) 
            self.l0 = inverse_realHcombination(lambd[:,:,0], self.Hsym_list)
            self.x0 = hstack((self.r0, self.l0)).real
        elif not self.isrel:
            # 2 spins, non relativistic
            self.r0_up = inverse_complexHcombination(R[:,:,0], self.Hsym_list)
            self.r0_dn = inverse_complexHcombination(R[:,:,1], self.Hsym_list) 
            self.l0_up = inverse_realHcombination(lambd[:,:,0], self.Hsym_list)
            self.l0_dn = inverse_realHcombination(lambd[:,:,1], self.Hsym_list)
            self.x0 = hstack((self.r0_up, self.r0_dn, self.l0_up, self.l0_dn)).real
              
        print('initial x0=', self.x0)
        
        sys.stdout.flush()
  
    def TransformU(self, U, T):
        """ Note that T is the transformation from the simple basis (left) to the more adapted basis (right)
        """
        return np.einsum("Aa,Bb,abcd,cC,dD", T.H, T.H, U, T, T)

    def GetUMatrix(self, opts, l):
     
        Ufullmat = np.zeros((self.norb2, self.norb2, self.norb2, self.norb2), dtype=np.complex)
        
        # the form of this tensor is the normal form: (see for example in triqs)
        #              C1+ C2+ C2 C1 
        #  where + is a short for dagger
        #  and 1,2 are the indices of the 2 spins (all have their own m indices)
        #
        Umat_spher = U_matrix_slater(l, radial_integrals=None, U_int=opts.U, J_hund=opts.J)

        u_avg_spher, j_avg_spher = get_average_uj(Umat_spher)
        print('U_avg_spher=', u_avg_spher, 'J_avg_spher=', j_avg_spher)

        if not self.isrel:          
            # transform to cubic and then to general basis
            # In non-rel case, the transformation matrix has no spin indices, and therefore
            # does not alter the spins in the tensor, and UFullmat retains the same structure
            # as umat_spher
            T = GetRealSHMatrix(l).H * self.ri.corrSubshell.basis

            # C+ operators (b,d) transform as T, C operator transformas T.H
            Umat = self.TransformU(Umat_spher, T)

            u_avg_cubic, j_avg_cubic = get_average_uj(Umat)
            print('U_avg_cubic(transformed)=', u_avg_cubic, 'J_avg_cubic=(transformed)', j_avg_cubic)
            
            Ufullmat[::2, ::2, ::2, ::2]     = Umat  # up, up
            Ufullmat[1::2, 1::2, 1::2, 1::2] = Umat  # dn, dn
            Ufullmat[::2, 1::2, 1::2, ::2]   = Umat  # up, dn
            Ufullmat[1::2, ::2, ::2, 1::2]   = Umat  # dn, up                
        else:
            dim = self.ri.dim // 2 
            Ufullmat[:dim, :dim, :dim, :dim] = Umat_spher  # up, up
            Ufullmat[dim:, dim:, dim:, dim:] = Umat_spher  # dn, dn

            Ufullmat[:dim, dim:, dim:, :dim] = Umat_spher  # up, dn
            Ufullmat[dim:, :dim, :dim, dim:] = Umat_spher  # dn, up  
           
            # Here we can't transform *before* creating the full matrix, because the indices are mixed
            # we set the bigger matrix so to have down/up blocks (4 of them), which are transformed by the CGMatrix(l)
            
            T = GetCGMatrix(l) * Matrix(self.ri.corrSubshell.basis) # transform to the native basis (J) and then use the special basis (if not I)
            Ufullmat = self.TransformU(Ufullmat, T)

        # ied doesn't multiply by 0.5 like the other solvers
        if self.ed_solver == 'ied':
            Ufullmat *= 0.5 
        count = np.sum(Ufullmat != 0.0, axis=None)
        print("*** number of nonzero elements in U tensor:", count)
            
        # note that ISING-type interaction depends on the basis (the full U does not, up to change of basis) 
        count2 = 0
        if opts.ising:
            for ii, u in np.ndenumerate(Ufullmat):
                # indices 0,2 and indices 1,3 are the same electron - here we make sure they have the same, or opposite index
                if ii[0] == ii[1] and ii[1] == ii[2] and ii[2] == ii[3]:
                    continue
                if ii[0] == ii[3] and ii[1] == ii[2]:
                    continue
                if abs(u) > 1.0e-10:
                    count2 += 1 
                Ufullmat[ii] = 0.0
            print("*** number of nonzero elements in U ising tensor:", count2)
                  
        
        return Ufullmat


    def compute(self, opts):
        # options for root solver
        options = {'xtol': 1.0e-9,  # tolerance for convergence
                   'eps': 1.0e-10,  # step size
                   'maxfev' : 400*(len(self.x0) +1),
                   'factor': 0.1  # initial factor of the step size (use small to avoid overshooting in the first).
                   }
        
        # print umat.shape
        # print umat
        i_docc = 0  # orbital for double occupancy
        
        
        self.ri.success = 1
        self.ri.quality = 10.0
        
        args = (self.T, self.ri, opts, self.Ufullmat, self.Hsym_list, i_docc, self.isrel, self.spin_sym, self.ed_solver, opts.verbose)

        # root solver
        x_ga_ri = optimize.root(ga_complex_fermiT.root_GA, self.x0, tol=1.0e-8,
                                args=args, method='hybr', options=options)
        
        print("success=", x_ga_ri.success)
        print(x_ga_ri.message)
        
        if self.spin_sym:
            # get the parameters r and l from the solved root.
            r = x_ga_ri.x[0:len(self.r0)]
            l = x_ga_ri.x[len(self.r0):]
        
            # map the r an l to the R and Lambda matrices
            Rsym = complexHcombination(r, self.Hsym_list)
            Lambdasym = realHcombination(l, self.Hsym_list)
            self.R_conv = np.kron(Rsym, np.eye(2))
            self.Lambda_conv = np.kron(Lambdasym, np.eye(2))

        elif self.isrel:
            # get the parameters r and l from the solved root.
            r = x_ga_ri.x[0:len(self.r0)]
            l = x_ga_ri.x[len(self.r0):]
                
            # in the relativistic case the matrices have full dimension
            self.R_conv = complexHcombination(r, self.Hsym_list)
            self.Lambda_conv = realHcombination(l, self.Hsym_list)
            
        else:
            # get the parameters r and l from the solved root.
            r_up = x_ga_ri.x[0:len(self.r0_up)]
            r_dn = x_ga_ri.x[len(self.r0_up):len(self.r0_up)+len(self.r0_dn)]
            l_up = x_ga_ri.x[len(self.r0_up)+len(self.r0_dn):len(self.r0_up)+len(self.r0_dn)+len(self.l0_up)]
            l_dn = x_ga_ri.x[len(self.r0_up)+len(self.r0_dn)+len(self.l0_up):
                             len(self.r0_up)+len(self.r0_dn)+len(self.l0_up)+len(self.l0_dn)]
        
            # map the r an l to the R and Lambda matrices
            R_up = complexHcombination(r_up, self.Hsym_list)
            R_dn = complexHcombination(r_dn, self.Hsym_list)
            Lambda_up = realHcombination(l_up, self.Hsym_list)
            Lambda_dn = realHcombination(l_dn, self.Hsym_list)
            self.R_conv = np.zeros((2*R_up.shape[0],2*R_up.shape[1]),dtype=np.complex)
            self.Lambda_conv = np.zeros((2*Lambda_up.shape[0],2*Lambda_up.shape[1]),dtype=np.complex)
            self.R_conv[::2,::2] = R_up
            self.R_conv[1::2,1::2] = R_dn
            self.Lambda_conv[::2,::2] = Lambda_up
            self.Lambda_conv[1::2,1::2] = Lambda_dn

        self.Z_conv = np.dot(self.R_conv.conj().T, self.R_conv)
            
        # read density matrix and Hemb energy from h5 and compute the local energy in the physical subspace
        if self.ed_solver == 'spci':
            fembparam = h5py.File('EMBED_HAMIL_1.h5', 'r')
            femb = h5py.File('EMBED_HAMIL_RES_1.h5', 'r')
            dm_emb = femb['/DM'][...].T # density matrix
            d = fembparam['/D'][...].T # hybridization
            h1e = fembparam['/H1E'][...].T # physical one-body (including local level and double counting potential)
            lamc = fembparam['/LAMBDA'][...].T # auxiliary bath
            emol = femb['/emol'][...] # energy of the Hemb
            #auxiliary one-body energy to be subtracted
            haux = np.zeros( (2*d.shape[0], 2*d.shape[1] ) , dtype=np.complex)
            haux[h1e.shape[0]:,h1e.shape[1]:] = -lamc
            haux[:h1e.shape[0],h1e.shape[1]:] = d.T
            haux[h1e.shape[0]:,:h1e.shape[1]] = np.conjugate(d)
            eaux = np.sum(np.multiply(haux, dm_emb))
            # local physical energy
            eloc = emol - eaux
            femb.close()       
            fembparam.close()
        if self.ed_solver == 'ged':
            fembparam = h5py.File('GEDINP_1.h5', 'r')
            femb = h5py.File('GEDOUT_1.h5', 'r')
            dm_emb = femb['/DM'][...].T # density matrix
            d = fembparam['/D'][...].T # hybridization
            h1e = fembparam['/H1E'][...].T # physical one-body (including local level and double counting potential)
            lamc = fembparam['/LAMBDA'][...].T # auxiliary bath
            emol = femb['/emol'][...] # energy of the Hemb
            #auxiliary one-body energy to be subtracted
            haux = np.zeros( (2*d.shape[0], 2*d.shape[1] ) , dtype=np.complex)
            haux[h1e.shape[0]:,h1e.shape[1]:] = -lamc
            haux[:h1e.shape[0],h1e.shape[1]:] = d.T
            haux[h1e.shape[0]:,:h1e.shape[1]] = np.conjugate(d)
            eaux = np.sum(np.multiply(haux, dm_emb))
            # local physical energy
            eloc = emol - eaux
            femb.close()       
            fembparam.close()
        elif type(self.ed_solver) == simple_ed:
            femb = h5py.File('EMBED_HAMIL_RES_1.h5', 'r')
            dm_emb = femb['/DM'][...].T # density matrix
            eloc = femb['/eloc'][...]
            emol = femb['/emol'][...]
            femb.close()
        elif self.ed_solver == 'ied':
            ied = self.ri.ied
            dm_emb = ied.out.denmat[:,:,0] # density matrix 
            d = ied.D # hybridization
            lamc = ied.Lambda_c # auxiliary bath
            emol = ied.out.E[0] # energy of the Hemb
            rholoc = ied.out.histogram
            #auxiliary one-body energy to be subtracted
            haux = np.block([[ np.zeros(d.shape,  dtype=np.complex),  d.T ],
                             [np.conjugate(d)                      , -lamc]])            
            eaux = np.sum(np.multiply(haux, dm_emb)) # adler: shouldn't this be a block diagonal with 2 dm_emb?
            # local physical energy
            eloc = emol - eaux
            TerminateEDXServer()
        
        # compute moment
        mz = np.sum(dm_emb[:self.ri.dim * 2:2,:self.ri.dim * 2:2]) - np.sum(dm_emb[1:self.ri.dim * 2:2,1:self.ri.dim * 2:2])

        # compute non-local energy
        if self.isrel:
            num_bands = self.ri.Hk.shape[1] # matrices have full dimension
        else:
            num_bands = 2*self.ri.Hk.shape[1]
        
        R_all = np.eye(num_bands,dtype=np.complex)
        R_all[-self.R_conv.shape[0]::,-self.R_conv.shape[1]::] = self.R_conv
        Lambda_all = np.zeros((num_bands,num_bands),dtype=np.complex)
        Lambda_all[-self.Lambda_conv.shape[0]::,-self.Lambda_conv.shape[1]::] = self.Lambda_conv
        
        
        etot = 0.0 # fix this code

        # histogram is currently only implemented for simple_ed
        if type(self.ed_solver) == simple_ed:
            rholoc = np.diag(self.ed_solver.compute_rholoc_onfly()) # local reduced many-body density matrix
            #rholoc[:,:] - diagonal is probability 
        
        def BuildHistogram(rholoc, num_configs):
            his = Histogram(num_configs = num_configs)
            his.allocate()
            total = 0.0
            print ()
            for i in range(num_configs):
                his.p[i] = rholoc[i]
                his.N[i] = countBits(i)
                total += rholoc[i]
            print(f"- Histogram total probability: {total}, #configs; {num_configs}")
            his.p[:] = his.p[:] / total
            return his
        
        # compute quasiparticle density matrix for elam
        rhok_list=ga_complex_fermiT.calc_rhok_rotate_projector(
            self.ri, R_all, Lambda_all, self.T, self.spin_sym, self.isrel)

        Delta_p = ga_complex_fermiT.calc_Delta_p(rhok_list, self.ri)        
        elam = np.sum(np.multiply(Lambda_all,Delta_p))
        # compute the energy pieces from Gutzwiller local+double counting+lambda
        self.egutz = np.real(eloc - elam)
        
        
        
        
        # histogram is only implemented for simple_ed
        if type(self.ed_solver) == simple_ed or self.ed_solver == 'ied':
            self.ri.SetSolution(
                success= x_ga_ri.success,
                R=self.R_conv,
                Lambda=self.Lambda_conv,
                Z=self.Z_conv,
                RhoImp=dm_emb[:self.norb2, :self.norb2],
                Nimp=np.trace(dm_emb[:self.norb2, :self.norb2]).real,
                Etot=etot.real,
                hist=BuildHistogram(rholoc, num_configs=2**self.norb2),
                Mz=mz.real,
                egutz=self.egutz
                )
        else:
            self.ri.SetSolution(
                success= x_ga_ri.success,
                R=self.R_conv,
                Lambda=self.Lambda_conv,
                Z=self.Z_conv,
                RhoImp=dm_emb[:self.norb2, :self.norb2],
                Nimp=np.trace(dm_emb[:self.norb2, :self.norb2]).real,
                Etot=etot.real,
                hist=None,#BuildHistogram(mbBasis, rholoc),
                Mz=mz.real,
                egutz=self.egutz
                )
    
        sys.stdout.flush()
         
def Gutz(opts=None, **kwargs):
    GAMain([], kwargs, opts=opts)

 
def GutzOptionsParser(add_help=False, private=False):
   
    parser = ArgumentParserThatStoresArgv('gutz', add_help=add_help ,
                            parents=[GetPEScfOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
           
    if private:
        parser.add_argument("-w", "--energy-window", dest="window",
                            help="energy window for the cubic_harmonicsands (in eV)",
                            default='-10.0:10.0')

    parser.add_argument("--inputs", dest="only_inputs",
                        action="store_true",
                        default=False,
                        help="Only create inputs R=I Lambda=Eloc")

    parser.add_argument("--no-crys-sym", dest="crys_sym",
                        action="store_false",
                        default=True,
                        help="using the irreducible representation for the local crystal symmetry adapted basis")

    parser.add_argument("-v", dest="verbose",
                        type=int,
                        default=2,
                        help="verbosity for the GA iterations. 1: print all intermediate. 2: Print only the maxerror. 0: No verbosity.")
    
    parser.add_argument("--solver", dest="solver",
                        type=str,
                        default='simple_ed',
                        help="ED solvers. spci, simple_ed, ged,ied")
    
    parser.add_argument("--ising", dest="ising",
                        default=False,
                        action="store_true",
                        help="use ising interaction, not full U (which is the default). Note that this default is the oppposite of that of DMFT.")
    
    parser.add_argument("--solver-tmp-dir", type=str, dest="solver_tmp_dir", default=".", help="directory where large files are created in ied")
    
    return parser
  
 
def GAMain(args, kwargs, opts=None):
    
    if opts is None:
        parser = GutzOptionsParser(add_help=True, private=True)    
        opts, _args = parser.parse_known_args(args)
        SaveOptions(opts)
    
    for key in kwargs:
        setattr(opts, key, kwargs[key])
    
    KB = 6.333328e-6 * 27.2107 * 0.5  # collected from Kutepov's code, to match perfectly
    opts.beta = 1 / (opts.Temperature * KB)
    
    num_workers = opts.mpi_workers
    opts.mpi_workers=0

    raise Exception("This code is not maintained and should not be used.")

    # read it into fortran
    _plugin = DFTPlugin.Instance()
    opts.mpi_workers = num_workers
        
    ga = SimpleGA(opts)
    
    if opts.only_inputs:
        ga.ri.Lambda = ga.ri.Elocal +  ga.ri.DC
        ga.ri.StoreMyself()
        return
        
    ga.compute(opts)
    
    del ga # help the garbage collector
    

if __name__ == '__main__':
    GAMain(sys.argv[1:], {})
