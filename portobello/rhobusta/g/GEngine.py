#!/usr/bin/env python3

'''
Created on Nov 12, 2018

@author: adler@physics.rutgers.edu
'''
import numpy as np
from portobello.bus.Matrix import ZeroComplexMatrix, Matrix
#from pytriqs.operators.util.U_matrix import U_matrix
from portobello.rhobusta.PEScfEngine import PEScfEngine
from portobello.generated.gutz import GState

from portobello.generated.Rho import RhoInBands
import sys
from portobello.generated.self_energies import LocalOmegaFunction
from portobello.generated import Rho
import pathlib
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.rhobusta.ProjectorEmbedder import GetSuffix
from portobello.rhobusta.options import ReadOptions

EPSILON = 1.0e-10
gLocation = "./gutz{0}.h5:/"

def UpdateDensityMatrixForGutz():  
    g = GEngine(ReadOptions(), for_dft=True)
    g.WriteDensityMatrix()
    del g

        

class GEngine(GState, PEScfEngine):
    '''
    This code calculates the Ek and Eloc matrices, for the use of the Gutzwiller code.
    The other numbers that this code provides are:
    
    self.num_si             - number of spin indices
    self.num_k_all          - the number of k points in the FULL Brillouin zone
    self.full_dim  - the non-correlated dimension
    self.dim                - the dimension of the correlated subspace (without spin polarization)
    
    Then we calculate:
    self.Hk[num_k, full_dim, full_dim,num_si]
    self.Hloc[dim,dim,num_si]
    
    '''
    def __init__(self, opts, for_dft=False):
        GState.__init__(self)
        is_new = PEScfEngine.__init__(self, opts)
        if hasattr(opts, 'mpi_workers'):
            self.mpi_workers = opts.mpi_workers
        else:
            self.mpi_workers = 0
        
        self.InitOneBody()    
          
        # to allocate the arrays and use later
        self.num_k = self.dft.num_k
        self.num_bands = self.num_corr_bands
        
        # we don't want to call alloc() here, since InitOneBody() already allocated and initialized some arrays
        self.Hk = np.zeros((self.num_k, self.num_bands, self.num_bands, self.num_si), np.complex128)
        # this one is transient
        self.HkBands = np.zeros((self.dft.num_k, self.num_bands, self.num_bands, self.num_si), np.complex128)
        
        if self.state == 'start':
            # normal initialization 
            self.state = 'initialized'
            self.R = np.copy(self.ID)
            #self.Lambda = ZeroComplexMatrix(self.dim) 
            # TODO: is this correct?
            self.Lambda = self.Elocal

        if for_dft:
            if self.state != 'initialized' and self.state != "dft+sigma":
                self.super_iter += 1
                self.sub_iter = 0
            else:
                self.sub_iter += 1
            self.state = 'dft+sigma'

        # TODO: remove this alias
        self.full_dim = self.num_bands
        # this is the dimension of the non-correlated hamiltonian that GA uses
       
        # first set Hk and Hloc
        for k, si in self.shard_k_si_pairs():
            self.Hk[k,:,:, si] = np.diag(self.epsAllK[:,k, si])
        
        self.Hloc  = np.copy(self.Elocal)
            
        # occupation number
        self.occupancy = np.trace(self.occ).real
            
                    
        # substract Hloc from Hk
        for k, si in self.shard_k_si_pairs():
            self.Hk[k,:,:, si] -= self.EmbeddedIntoKBandWindow(self.Hloc, k, si)
                     
        # return a numbered Identity matrix        
        def MakeNumberedID():
            nid = np.zeros((self.dim, self.dim, self.num_si), dtype=np.complex128)
            for si in range(self.num_si):
                for i in range(self.dim):
                    nid[i,i, si] = i+1  # non zero
            return nid
            
        nid = MakeNumberedID()
          
        # Rotating Hk so that ONE copy of the correlated space is in the bottom right of the matrix
        for k, si in self.shard_k_si_pairs():
            
            # save the non-rotated, band matrix
            self.HkBands[k,:,:, si] = self.Hk[k, :,:, si]
            
            indices = np.array([-self.dim + i for i in range(self.dim)])
            
            em = self.EmbeddedIntoKBandWindow(nid, k, si)
            w, v = np.linalg.eigh(em)
              
            # The matrix v satisfies v.H * w * v = Pk*Pk.H (replicated over all equivalent atoms)
            assert(np.all(w[:-self.dim*(self.num_equivalents+self.num_anti_equivalents)] < EPSILON)) 
            tester = np.repeat(list(range(1,self.dim+1)),self.num_equivalents+self.num_anti_equivalents) 
            assert(np.all(abs(w[-self.dim*(self.num_equivalents+self.num_anti_equivalents):]-tester) < 0.01)), w # embedding can be off by <10% due to normalization
            
            if not np.all(abs(Matrix(v).H * v - self.ID1F) < EPSILON):  # assert u.H = v
                print("**** matrices V violate assumption:")
                self.PrintMatrix(Matrix(v).H * v)
                assert(True), "assumption violated"
            
            # copy the first basis vectors corresponding to non-projected subspace.
            # the last vectors will be replaced
            V = Matrix(np.copy(v))
            
            
            # TODO: can this be skipped if num_equivalents=1?
            
            assert(self.num_equivalents >=0)
            ###### Note: the equivalents are handled first, but appear LAST! 
            ######       the anti-equivalents appear in the start of the bigger matrix
            for eqv in range(self.num_equivalents+self.num_anti_equivalents):
                # the last eigenvalues are 0, which means they belong to the non-correlated subspace
                # this is the subspace that we are interested in when calculating the Ek matrix

                em = self.EmbeddedIntoKBandWindowOneAtom(nid[:,:,si], k, si, eqv)
                w, v = np.linalg.eigh(em)
            
                # verify that the first num_bands-dim values are 0, and last dim  values are 1:
                # embedding is on the last self.dim*num_equivalents eigenvectors
                    
                # The matrix v satisfies v.H * w * v = Pk*Pk.H (replicated over all equivalent atoms)
                assert(np.all(w[:-self.dim] < EPSILON)) 
                assert(np.all(abs(w[-self.dim:]-list(range(1,self.dim+1))) < 0.01)), w # embedding can be off by <10% due to normalization
                
                if not np.all(abs(Matrix(v).H * v - self.ID1F) < EPSILON):  # assert u.H = v
                    print("**** matrices V violate assumption:")
                    self.PrintMatrix(Matrix(v).H * v)
                    assert(True), "assumption violated"
            
                V[:, indices] = v[:, -self.dim:]
                indices[:] -= self.dim
            
            if not np.all(abs(V.H*V- self.ID1F) < 0.01):  # make sure the new v matrix is also unitary
                print("**** V matrix is not unitary:")
                self.PrintMatrix(V.H*V)
                assert(True), "assumption violated"
                
            self.Hk[k,:,:, si] = V.H * self.Hk[k,:,:, si] * V
            
        
        if not for_dft and (is_new or self.success):
            self.UpdateDoubleCounting(is_new=is_new)
        self.Vdc = self.DC
                                    
        sys.stdout.flush()
        self.StoreMyself()

    def GetMethodName(self):
        return "gutzwiller"

    def GetGutzFileName(self, suffix=None):
        if suffix is None:
            suffix = self.orbBandProj.GetSuffix()
        return gLocation.format(suffix)
           
     
    # here M does not have a spin index - this is only used to decompose the orbital space (in GEngine)
    def EmbeddedIntoKBandWindowOneAtom(self, M, k, si, eqv):
        Mfull = ZeroComplexMatrix(self.dim*(self.num_equivalents + self.num_anti_equivalents))
        Mfull[eqv*self.dim:(eqv+1)*self.dim, eqv*self.dim:(eqv+1)*self.dim] = M[:,:]
        Pk = Matrix(self.orbBandProj.P[:,:,k,si])
        return Pk * Mfull * Pk.H
        
        
    def WriteDensityMatrix(self):
        if self.IsMaster(): print(" - writing ρ(λ,R)")
        
        rho = RhoInBands(num_k=self.dft.num_k,
                         min_band=self.orbBandProj.min_band,
                         max_band=self.orbBandProj.max_band,
                         num_si=self.num_si)
        #print "num k irr: ", rho.num_k
        rho.allocate()
        
        # we also record the orbital correlated number matrix
         
        if self.IsMaster(): print(" - μ: %f eV" % self.mu)
        
        # calculate rho_k = Rf(RHkR+L)R  and the projected value of rho_k
        Ncorr = self.ZeroLocalMatrix()
        I = np.eye(self.num_bands, self.num_bands, dtype=np.complex128)

        for k,si in self.shard_k_si_pairs():
            
            # we make all calculations in band basis, which is why we use HkBands, and embed R,L into bands
            HkB = Matrix(self.HkBands[k, :,:, si])
            
            R = Matrix(np.copy(I))
            R += self.EmbeddedIntoKBandWindow(self.R - self.ID, k, si) 
            L = Matrix(self.EmbeddedIntoKBandWindow(self.Lambda, k, si))
            
            # Quasi particle hamiltonian
            Hqp = R * HkB * R.H + L - self.mu *I
            
            Eqp, u = np.linalg.eigh(Hqp)
            uM = Matrix(u)          
            # assert(np.all(abs(uM *np.diag(Eqp)* uM.H - Hqp) < 1.0e-8))
            
            # calculate the fermi function of the Eqp matrix
            UR = uM.H * R #TODO: adler: if these were FL theory, we would drop the R (??)
            rhoBands = UR.H *  np.diag(self.ferm(Eqp)) * UR
            rho.rho[:, :, k, si] = rhoBands[:,:]
            
            # calculate the projected rho (using Rf()R)
            Pk = self.OneAtomProjector(k, si) # importantly, this is the 1st dim dimensions, not last (last may be AFM)
            Ncorr[:,:,si] +=  Pk.H * rhoBands * Pk  * self.lapw.weight[k] # this is why we loop over ALL k: in order to project we need values at all k
             
    
        if self.dft.is_sharded:
            MPI.COMM_WORLD.Barrier()
            Ncorr = MPI.COMM_WORLD.allreduce(Ncorr, op=MPI.SUM)

        # then we correct the local correlated part of this rho_k 
        for k,si in self.shard_k_si_pairs():
            rho.rho[:,:,k, si] -= self.EmbeddedIntoKBandWindow(Ncorr - self.RhoImp, k, si)

        # note the indirection of this write - the location is dependent on the number of bands
        # this is because arrays are fixed sized in storage, and therefore we need to include the number of 
        # bands (which is the varying size) in the output name
        ri = Rho.RhoInfo()
        ri.location = "./rho.core.h5:/bands-%d"% (rho.max_band-rho.min_band+1)
        ri.store("./rho.core.h5:/info", flush=True)
        # write it on the bus so that Fortran code can find it
        rho.store(ri.location, flush=True)
        
        del rho
        
        # this one should be close to Nimp
        sys.stdout.flush()
        
        # Note: the values here will be valid only if set_g was called before in FlapwMBPT,
        #       which IS the case when we embed sigma
        if self.sub_iter >= 1:
            self.SetLatticeOccupation()
            self.StoreMyself()
        
    def SetSolution(self, success, R, Lambda, Z, RhoImp, Nimp, Etot, hist, Mz, egutz):
        print("Gutz solution:")
        print("--------------")
        print("R matrix:")
        self.PrintMatrix(R)
        print("Lambda matrix:")
        self.PrintMatrix(Lambda)
        print("Z matrix:")
        self.PrintMatrix(Z)
        print("RhoImp matrix:")
        self.PrintMatrix(RhoImp)
        print("occupation: %f"%(Nimp))
        #print("total energy: %f"%(Etot))
        #print("Mz_corr: %f"%(Mz))
        #print "histogram:", hist.p[0:100]
        
        self.success = 0
        if success:
            self.success = 1
        
        def SetSpinMatrices(Mself, Min):
            if self.dft.num_si == 2:  # spinful
                Mself[:,:,0] = Min[::2,::2]
                Mself[:,:,1] = Min[1::2,1::2]
            else:
                if self.dft.nrel == 2:
                    # here we have only one spin, the matrix is full
                    Mself[:,:,0] = Min[:,:]
                else:
                    Mself[:,:,0] = Min[::2,::2]
                
        
        SetSpinMatrices(self.R, R)
        SetSpinMatrices(self.Lambda, Lambda)
        SetSpinMatrices(self.Z, Z)
        SetSpinMatrices(self.RhoImp, RhoImp)
        
        # update Nimp (which determines DC) only if it was successful,
        # otherwise, we want to insist on the current DC and try again with the same number,
        # unless the change is small (in which case it will not affect DC)
        #if success or (Nimp - self.Nimp < 0.4):
        self.Nimp = Nimp
        
        # total energy
        self.energyImp = Etot
        
        # magnetic moment
        self.JzImp = Mz
        
        # gutzwiller energy
        self.egutz= egutz
        
        if hist is not None: # hist is None if solver is not simple_ed.
            self.hist = self.CompleteHistogramData(hist)
        
        # in g mode, we have only one object instance
        # and only one sub iteration
        if self.state != 'initialized':
            self.super_iter += 1
            self.sub_iter = 0
        self.state = "g-solved"
            
        print(" - Gutz energy difference from DFT: ", self.energyImp - self.energyBands)
        
        self.StoreMyself()
        self.StoreStateInHistory()
          
    def GetTopLevelGutzName(self):
        orbitals_location = "./projector.h5:/def/"
        so = SelectedOrbitals()
        so.load(orbitals_location)
        return self.GetGutzFileName(suffix=GetSuffix(so, which_shell=self.orbBandProj.which_shell))
          
    def LoadMyself(self):
        is_new = False
        
        name = self.GetTopLevelGutzName()
        
        try:
            GState.load(self, name, validate=True) 
        except:
            #print "***** GUTZ file is new !!! *****"
            is_new = True
        return is_new
        
    def StoreMyself(self):
        if self.IsMaster(): GState.store(self, self.GetGutzFileName(), flush=True)
        
    def StoreStateInHistory(self):
        if not self.IsMaster(): 
            return
        self.PreserveHistory()
        GState.store(self, "./history.h5:/%d/%d/"%(self.super_iter, 
                                                self.sub_iter), flush=True)
             
             
    def SetSigImag(self, sig):
                
        for si in range(self.num_si):
            R = Matrix(self.R[:,:,si])
            sig0 = R.H.I * (self.Lambda[:,:,si]- (1.0 - self.MuChoice())*self.mu) * R.I - self.Hloc[:,:,si]  + self.DC[:,:,si]
            sig1 = self.ID1 - (R*R.H).I
                            
                            
            for iomega in range(sig.num_omega):
                w = sig.omega[iomega]
                
                # valid only within the energy window / 2.0 (is this even too much ?)
                sig.M[iomega, :,:,si] =  sig0 + 1j*w*sig1
                # otherwise it is 0
             
    def MuChoice(self):
        if pathlib.Path("./MuNotSubtracted").exists():
            return 1.0
        return 0.0
             
    def LoadSigRealOmega(self, bound=25.0, Nw = 500):

        sig = LocalOmegaFunction(is_real=True,
                                 num_orbs=self.dim,
                                 num_si=self.num_si,
                                 num_omega = 2*Nw+1)
        sig.allocate()
        
        self.DC[:,:,:] = 0.0 # we avoid adding and substracting it again in the plotting, since the self energy is not defined over the entire range
        
        for si in range(self.num_si):
            R = Matrix(self.R[:,:,si])
            sig0 = (1.0 - self.MuChoice()) * self.mu * self.ID1 + R.H.I * (self.Lambda[:,:,si] - (1.0 - self.MuChoice()) * self.mu *self.ID1)* R.I - self.Hloc[:,:,si]  #+ self.DC[:,:,si]
            sig1 = self.ID1 - (R*R.H).I
                            
            for iomega in range(-Nw, Nw+1):
                w = iomega * bound / max(Nw,1) 
                
                sig.omega[iomega+Nw] = w
                # valid only within the energy window / 2.0 (is this even too much ?)
                #if w < self.proj_energy_window.high / 2.0 and w > self.proj_energy_window.low / 2.0:
                if w < self.proj_energy_window.high and w > self.proj_energy_window.low:
                    sig.M[iomega+Nw, :,:,si] =  sig0 + w*sig1
                # otherwise it is 0
        self.sig = sig
  
