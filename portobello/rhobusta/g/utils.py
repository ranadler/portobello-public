import numpy as np
import h5py
import cmath
import itertools as it
from scipy.special import factorial as fact
from math import sqrt
from scipy.linalg import eigh, block_diag
from itertools import product


def funcMat(H, function, pr=False):
    '''
    Compute function of matrix
    '''
    H = H  # + np.eye(H.shape[0])*tiny
    N = H.shape[0]
    # print np.max(H.conj().T-H)
    assert(H.shape[0] == H.shape[1])
    # assert(np.allclose(H.conj().T,H,rtol=1e-08, atol=1e-08))
    
    if np.max(abs(H.conj().T - H)) > 1e-8:
        print('max(abs(H.conj().T-H))=', np.max(abs(H.conj().T - H)))
        raise
    #
    eigenvalues, U = eigh(H)
    Udagger = U.conj().T
    if pr:
        print(eigenvalues)
    #
    functioneigenvalues = function(eigenvalues)
    if pr:
        print(functioneigenvalues)
    if H.dtype == np.float64:
        assert max(abs(functioneigenvalues.imag)) < 1e-12
        functioneigenvalues = functioneigenvalues.real
    functionE = np.zeros(U.shape, dtype=H.dtype)  # dtype=np.complex)
    functionE[np.arange(N), np.arange(N)] = functioneigenvalues
    functionH = np.dot(np.dot(U, functionE), Udagger)
    # print functionE
    #
    return functionH


def calc_expH(H):
    '''
    exp of matrix with cutoff
    '''
    # print H
    evals, evecs = eigh(H)
    # func = np.exp(evals)
    func = []
    for e in evals:
        if e > 500.:
            func.append(np.exp(500))
        elif e < -500:
            func.append(np.exp(-500))
        else:
            func.append(np.exp(e))
    dm = np.zeros(H.shape)
    dm[list(range(H.shape[0])), list(range(H.shape[0]))] = func
    return np.dot(evecs , np.dot(dm, evecs.conj().T))


def calc_logH(H):
    '''
    log of matrix with cutoff
    '''
    evals, evecs = eigh(H)
    # print evals
    # func = np.log(evals+0.0000001)
    func = [cmath.log(e + 0.0000001) for e in evals]
    dm = np.zeros(H.shape, dtype=np.complex)
    dm[list(range(H.shape[0])), list(range(H.shape[0]))] = func
    return np.dot(evecs , np.dot(dm, evecs.conj().T))


def make_trivial_matrix_basis(N, symmetric=False):
    '''
    Make trivial real matrix basis
    '''
    h_list = []
    for i, j in it.product(list(range(N)), list(range(N))):
        h = np.zeros((N, N))
        if symmetric:
            if i <= j:
                h[i, j] = 1
                h[j, i] = 1
            else:
                continue
        else:
            h[i, j] = 1
        h_list.append(h / np.sqrt(np.trace(np.dot(h.conj().T, h))))
    return h_list


def map_to_herm_matrix(X, sp_basis_herm):
    '''
    map list to Hermitian matrix
    '''
    return np.sum([x * h for x, h in zip(X, sp_basis_herm)], axis=0)


def map_to_matrix(X, sp_basis):
    '''
    map list to matrix
    '''
    return np.sum([x * h for x, h in zip(X, sp_basis)], axis=0)


def map_to_herm_vector(X, sp_basis_herm):
    '''
    map Hermitian matrix to vector
    '''
    return [np.trace(np.dot(np.matrix(h).getH(), X)) / np.trace(np.dot(np.matrix(h).getH(), h)) for h in sp_basis_herm]


def map_to_vector(X, sp_basis):
    '''
    map matrix to vector
    '''
    return [np.trace(np.dot(np.matrix(h).getH(), X)) / np.trace(np.dot(np.matrix(h).getH(), h)) for h in sp_basis]


def Hermitian_list(N):
    '''
    Create basis for Hermitian matrix
    '''
    H_list = []
    tH_list = []
    #
    Z = np.zeros((N, N), dtype=np.complex)
    for i in range(N):
        H = Z * 1.0
        H[i, i] = 1.0
        H_list.append(H * 1.0)
        tH_list.append(H * 1.0)
    #
    for i in range(N):
        for j in range(i + 1, N):
            H = Z * 1.0
            H[i, j] = 1.0
            H[j, i] = 1.0
            H_list.append(H / np.sqrt(2.0))
            tH_list.append(H / np.sqrt(2.0))
            H = Z * 1.0
            H[i, j] = 1.0j
            H[j, i] = -1.0j
            H_list.append(H / np.sqrt(2.0))
            tH_list.append(np.transpose(H) / np.sqrt(2.0))
    #
    assert(len(H_list) == N ** 2)
    return H_list, tH_list


def construct_Hsym_list(rep, dtype=np.float64):
    '''
    Construct basis for real symmetry basis
    '''
    nvars = np.max(rep)
    Hsym_list = []
    tHsym_list = []
    # print nvars
    # print rep
    # print np.array(rep == np.ones((rep.shape[0],rep.shape[1])),dtype=float)
    for i in range(nvars):
        tmp = np.array(rep == (i + 1) * np.ones((rep.shape[0], rep.shape[1])), dtype=dtype)
        tmp /= np.sqrt(np.trace(np.dot(tmp, tmp.conj().T)))
        Hsym_list.append(tmp)
        tHsym_list.append(tmp.T)
        if np.trace(tmp) == 0:
            tmp2 = np.zeros((tmp.shape[0],tmp.shape[1]), dtype=dtype)
            idx = np.nonzero(tmp)
            for j in range(len(idx[0])):
                if idx[0][j]<idx[1][j]:
                    tmp2[idx[0][j],idx[1][j]] = tmp[idx[0][j],idx[1][j]]*1j
                else:
                    tmp2[idx[0][j],idx[1][j]] = tmp[idx[0][j],idx[1][j]]*(-1j)
            Hsym_list.append(tmp2)
            tHsym_list.append(tmp2.T)
    return Hsym_list, tHsym_list


def realHcombination(x, H_list):
    '''
    map a list of parameters to a real symmtery matrix
    '''

    M = len(x)
    N = H_list[0].shape[0]
    assert(H_list[0].shape[0] == H_list[0].shape[1])
    assert(len(x) == len(H_list))
    #
    H = np.zeros((N, N))
    for i in range(M):
        H = H + x[i] * H_list[i]
    #
    return H

# Given REAL array x and list of matrices H_list, construct
# linear combination:  H = \sum_n x_n [H_list]_n


def inverse_realHcombination(H, H_list):
    '''
    map a real symmetry matrix to parameters
    '''
    M = len(H_list)
    assert(H.shape[0] == H.shape[1])
    #
    x_list = []
    for i in range(M):
        x_list.append(np.trace(np.dot(H_list[i], H)))
    x = np.array(x_list)
    #
    return x

# Given Hermitian matrix H extract components x_n with respect
# to a set H_list of Hermitian matrices


def complexHcombination(v, H_list):
    '''
    map a set of parameters to complex Hermitian matrix
    '''
    twiceM = len(v)
    M = twiceM // 2
    N = H_list[0].shape[0]
    assert(twiceM % 2 == 0)  # Checking that dimension of v is even
    assert(H_list[0].shape[0] == H_list[0].shape[1])
    assert(len(v) == 2 * len(H_list))
    #
    x = v[0:M]
    y = v[M:twiceM]
    H = np.zeros((N, N))
    for i in range(M):
        H = H + x[i] * H_list[i]
    for i in range(M):
        H = H + 1j * y[i] * H_list[i]
    #
    return H


def inverse_complexHcombination(H, H_list):
    '''
    map a non-Hermitian matrix to list of parameters
    '''
    M = len(H_list)
    assert(H.shape[0] == H.shape[1])
    #
    xr_list = []
    xi_list = []
    for i in range(M):
        xr_list.append(np.real(np.trace(np.dot(H_list[i], H))))
        xi_list.append(np.imag(np.trace(np.dot(H_list[i], H))))
    xr = np.array(xr_list)
    xi = np.array(xi_list)
    #
    return np.hstack((xr, xi))


def duplicate_in_spin_space(A):
    """
    Take  matrix acting in single-particle spinless space and
    duplicate it to act in spin space, i.e construct :math:`\tilde{A}` such that:
    
    .. math::
               \tilde{A}_{2i, 2j} = \tilde{A}_{2i+1,2j+1} = A_{ij}
    
    Parameters
    ----------
    A : NxN matrix
    
    Returns
    -------
    At : 2Nx2N matrix

    See also
    --------
    spin_symmetrize
    """
    if "Gf" in str(type(A)):
        At = type(A)(mesh=A.mesh, shape=(A.target_shape[0] * 2, A.target_shape[1] * 2))
        for iw in range(len(At.mesh)):
            for i, j in it.product(list(range(A.target_shape[0])), list(range(A.target_shape[1]))):
                At.data[iw, 2 * i, 2 * j] = A.data[iw, i, j]
                At.data[iw, 2 * i + 1, 2 * j + 1] = A.data[iw, i, j]

    else:
        At = np.zeros((A.shape[0] * 2, A.shape[1] * 2), dtype=A.dtype)
        for i, j in it.product(list(range(A.shape[0])), list(range(A.shape[1]))):
            At[2 * i, 2 * j] = A[i, j]
            At[2 * i + 1, 2 * j + 1] = A[i, j]
    return At


def enlarge_in_spin_space(A):
    """
    Take matrix acting in single-particle spinless space and
    enlarge it to act in spin space up and down separately.
    
    Parameters
    ----------
    A : NxN matrix
    
    Returns
    -------
    At : 2 2Nx2N matrices corresponds to spin up and sown

    """
    if "Gf" in str(type(A)):
#     At = type(A)(mesh = A.mesh, shape = (A.target_shape[0]*2,A.target_shape[1]*2))
#     for iw in range(len(At.mesh)):
#      for i,j in it.product(range(A.target_shape[0]), range(A.target_shape[1])):
#        At.data[iw, 2*i,2*j] = A.data[iw,i,j]
#        At.data[iw, 2*i+1,2*j+1] = A.data[iw,i,j]
        raise Exception("Gf is not yet implemented enlarge in spin space!")
    else:
        Atup = np.zeros((A.shape[0] * 2, A.shape[1] * 2), dtype=A.dtype)
        Atdn = np.zeros((A.shape[0] * 2, A.shape[1] * 2), dtype=A.dtype)
        for i, j in it.product(list(range(A.shape[0])), list(range(A.shape[1]))):
            Atup[2 * i, 2 * j] = A[i, j]
            Atdn[2 * i + 1, 2 * j + 1] = A[i, j]
    return Atup, Atdn


def coefficients_identity(N):
    d = 2 * N ** 2
    v = np.zeros(d)
    v[0:N] = np.ones(N)
    #
    return v

def updn_to_spinful_mat(Mup,Mdn):
    M = np.zeros((2*Mup.shape[0],2*Mup.shape[1]),dtype=Mup.dtype)
    M[::2,::2] = Mup
    M[1::2,1::2] = Mdn
    return M

def get_blocks(H, s):
    N = H.shape[0]
    assert(H.shape[0] == H.shape[1])
    assert(s <= N)
    #
    S = H[0:s, 0:s]
    B = H[s:N, s:N]
    V = H[0:s, s:N]
    Vdagger = H[s:N, 0:s]
    #
    return S, B, V, Vdagger


def set_blocks(S, B, V):
    s = S.shape[0]
    b = B.shape[0]
    N = s + b
    #
    H = np.zeros((N, N), dtype=S.dtype)  # dtype=np.complex)
    H[0:s, 0:s] = S
    H[s:N, s:N] = B
    H[0:s, s:N] = V
    H[s:N, 0:s] = np.transpose(np.conjugate(V))
    #
    return H


def set_blocks_not_Hermitian(S, B, V1, V2):
    s = S.shape[0]
    b = B.shape[0]
    N = s + b
    #
    H = np.zeros((N, N), dtype=S.dtype)  # dtype=np.complex)
    H[0:s, 0:s] = S
    H[s:N, s:N] = B
    H[0:s, s:N] = V1
    H[s:N, 0:s] = V2
    #
    return H


def dF(A, H, function, d_function):
    '''
    Compute the derivative of a function of matrix with respect to the matrix parameters
    according to Lowener theorem.
    '''
    #from scipy.misc import derivative
    evals, evecs = eigh(A)
    Hbar = np.dot(np.conj(evecs).T, np.dot(H, evecs))  # transform H to A's basis
    # create Loewner matrix in A's basis
    loewm = np.zeros(evecs.shape, dtype=A.dtype)  # dtype=np.complex)
    for i in range(loewm.shape[0]):
        for j in range(loewm.shape[1]):
            if i == j:
                loewm[i, i] = d_function(evals[i])  # derivative(function, evals[i], dx=1e-12)
            if i != j:
                if evals[i] != evals[j]:
                    loewm[i, j] = (function(evals[i]) - function(evals[j])) / (evals[i] - evals[j])
                else:
                    loewm[i, j] = d_function(evals[i])  # derivative(function, evals[i], dx=1e-12)

    # Perform the Schur product in A's basis then transform back to original basis.
    deriv = np.dot(evecs, np.dot(loewm * Hbar, np.conj(evecs).T))
    return deriv


def dF_real(A, H, function, d_function):
    #from scipy.misc import derivative
    #tiny = 1e-8  # use to regularize eigen problem for singular matrix
    A = A  # + np.eye(H.shape[0])*tiny

    evals, evecs = eigh(A)
    # print A
    # print evals
    # print evecs
    Hbar = np.dot(np.conj(evecs).T, np.dot(H, evecs))  # transform H to A's basis
    # create Loewner matrix in A's basis
    loewm = np.zeros(evecs.shape)
    for i in range(loewm.shape[0]):
        for j in range(loewm.shape[1]):
            if i == j:
                loewm[i, i] = d_function(evals[i]) 
                # loewm[i,i] = derivative(function, evals[i], dx=1e-12)
            if i != j:
                if evals[i] != evals[j]:
                    loewm[i, j] = ((function(evals[i]) - function(evals[j])) / (evals[i] - evals[j]))
                else:
                    loewm[i, j] = d_function(evals[i]) 
                    # loewm[i,j] = derivative(function, evals[i], dx=1e-12)

    # Perform the Schur product in A's basis then transform back to original basis.
    deriv = np.dot(evecs, np.dot(loewm * Hbar, np.conj(evecs).T))
    return deriv


def calc_nf(H, T):
    evals, evecs = eigh(H / T)
    func = calc_Fermi(evals)
    dm = np.zeros(H.shape)
    dm[list(range(H.shape[0])), list(range(H.shape[0]))] = func
    return np.dot(evecs , np.dot(dm, evecs.conj().T))


def calc_nf0(H):
    evals, evecs = eigh(H)
    func = calc_Fermi0(evals)
    dm = np.zeros(H.shape)
    dm[list(range(H.shape[0])), list(range(H.shape[0]))] = func
    return np.dot(evecs , np.dot(dm, evecs.conj().T))


def calc_C(H):
    assert(H.shape[0] == H.shape[1])
    #
    # print "H in calc_C"
    # print H
    C = funcMat(H, calc_Fermi0)
    #
    return C


def calc_C_T(H, T):
    assert(H.shape[0] == H.shape[1])
    #
    # print "H in calc_C"
    # print H
    C = funcMat(H / T, calc_Fermi)
    #
    return C


def calc_C_hole(H):
    assert(H.shape[0] == H.shape[1])
    #
    # print "H in calc_C"
    # print H
    C = funcMat(H, calc_hole_Fermi0)
    #
    return C


def denR(x):
    return (x * ((1.0 + 0.j) - x)) ** (-0.5)


def denR_real(x):
    # return 1./np.sqrt(x*((1.0)-x))
    return (x * ((1.0) - x)) ** (-0.5)


def denRm1(x):
    return (x * ((1.0 + 0.j) - x)) ** (0.5)


def denRm1_real(x):
    return np.sqrt(x * ((1.0) - x))  # (x*((1.0)-x))**(0.5)


def ddenRm1(x):
    return ((0.5 - x) / (x * ((1.0 + 0.j) - x)) ** 0.5)


def ddenRm1_real(x):
    return ((0.5 - x) / np.sqrt(x * ((1.0) - x)))  # (x*((1.0)-x))**0.5)


def calc_Fermi(x):
    """
    calculate the fermi function for a vector x. Sometimes smearing the fermi function can 
    lead to better convergence (but also lead to ficticious result if beta is too small).
    """
    f = []
    for xx in x:
        # This one is used to stablize selective Mott, but would lead to suprious OSMT if temperature is too high.
        # f.append(1./(1+np.exp(500*xx))) 
        # This one is important to get the correct phase diagram (especially for criyical t2/t1), but not stable in OSMP.
        if abs(xx) < 500:
            f.append(1. / (1 + np.exp(xx)))
        elif xx < -500:
            f.append(1)
        elif xx > 500:
            f.append(0)
    return np.array(f)


def calc_Fermi0(x):
    """
    calculate the fermi function for a vector x. Sometimes smearing the fermi function can 
    lead to better convergence (but also lead to ficticious result if beta is too small).
    """
    f = []
    for xx in x:
        # This one is used to stablize selective Mott, but would lead to suprious OSMT if temperature is too high.
        # f.append(1./(1+np.exp(300*xx))) 
        # f.append( np.exp(-300.*xx/2 - np.log(2) - np.log(np.cosh(300.*xx/2)) ) )
        # This one is important to get the correct phase diagram (especially for criyical t2/t1), but not stable in OSMP.
    	# if abs(xx)<1.e-12:
        #    f.append(1./(1+np.exp(500*xx)))
        #    f.append( np.exp(-300.*xx/2 - np.log(2) - np.log(np.cosh(300.*xx/2)) ) )
        #    f.append(0.5)
        # elif xx< -1.e-12:
        #    f.append(1)
        # elif xx> 1.e-12: 
        #    f.append(0)
        if xx < 1.e-12:
            f.append(1)
        elif xx >= 1.e-12: 
            f.append(0)
    return np.array(f)


def calc_hole_Fermi0(x):
    """
    calculate the fermi function for a vector x. Sometimes smearing the fermi function can 
    lead to better convergence (but also lead to ficticious result if beta is too small).
    """
    f = []
    for xx in x:
        # This one is used to stablize selective Mott, but would lead to suprious OSMT if temperature is too high.
        # f.append(1./(1+np.exp(500*xx))) 
        # This one is important to get the correct phase diagram (especially for criyical t2/t1), but not stable in OSMP.
        if abs(xx) < 1.e-12:
    #        f.append(1./(1+np.exp(500*xx)))
            f.append(0.5)
        elif xx < -1.e-12:
            f.append(0)
        elif xx > 1.e-12: 
            f.append(1)
    return np.array(f)


def cut_small(mat, tol=1.0e-10):
    """ Utility function that sets all elements smaller than "tol"
    to zero in an numpy ndarray. """

    mat_return = np.zeros(mat.shape, dtype=mat.dtype)
    mat_return += mat.real * (np.abs(mat.real) > tol)
    if mat.dtype == np.complex:
        mat_return += 1.0j * mat.imag * (np.abs(mat.imag) > tol)
    return mat_return

def single_particle_symmetrize(A, Hsf_list, tol=1e-12):
    """Symmetrize single-particle density matrix with symmetry adapted matrix basis
    """
    alist = inverse_realHcombination(A, Hsf_list)
    A_sym = realHcombination(alist, Hsf_list)
    return A_sym

def spin_symmetrize(A, tol=1e-12):
    """Symmetrize spin up and dn, i.e compute matrix :math:`A^\mathrm{sym}` such that
    
    .. math::
    
      A^\mathrm{sym}_{ij} = \\frac{1}{2} (A_{2i,2j} + A_{2i+1,2j+1})
    
    Parameters
    ------------
    A : matrix or GfImFreq
    
    See also
    --------
    duplicate_in_spin_space
    """
    if "Gf" in str(type(A)):
        A_sym = type(A)(mesh=A.mesh, shape=(A.target_shape[0] / 2, A.target_shape[1] / 2))
        for i, j in it.product(list(range(A_sym.target_shape[0])), list(range(A_sym.target_shape[1]))):
            diff = A[2 * i, 2 * j] - A[2 * i + 1, 2 * j + 1]
            for iw in range(len(diff.mesh)):
                assert (abs(diff.data[iw, 0, 0]) < tol), "symmetrizing matrix with spin differentiation for element %s, %s! : %s neq %s" % (i, j, A[2 * i, 2 * j], A[2 * i + 1, 2 * j + 1])
            A_sym[i, j] = 0.5 * (A[2 * i, 2 * j] + A[2 * i + 1, 2 * j + 1])
    elif (type(A) == np.ndarray and A.ndim == 3) or type(A) == list:
        A_sym = np.zeros((A.shape[0], A.shape[1] / 2, A.shape[2] / 2), dtype=A.dtype)
        for iw in range(len(A)):
            tmp = np.zeros((A[iw].shape[0] / 2, A[iw].shape[1] / 2), dtype=A.dtype)
            for i, j in it.product(list(range(tmp.shape[0])), list(range(tmp.shape[1]))):
                diff = A[iw, 2 * i, 2 * j] - A[iw, 2 * i + 1, 2 * j + 1]
                assert (abs(diff) < tol), "symmetrizing matrix with spin differentiation for element %s, %s! : %s neq %s" % (i, j, A[iw, 2 * i, 2 * j], A[iw, 2 * i + 1, 2 * j + 1])
                tmp[i, j] = 0.5 * (A[iw, 2 * i, 2 * j] + A[iw, 2 * i + 1, 2 * j + 1])
            A_sym[iw, :, :] = tmp
    else:
        A_sym = np.zeros((A.shape[0] / 2, A.shape[1] / 2), dtype=A.dtype)
        for i, j in it.product(list(range(A_sym.shape[0])), list(range(A_sym.shape[1]))):
            assert (abs(A[2 * i, 2 * j] - A[2 * i + 1, 2 * j + 1]) < tol), "symmetrizing matrix with spin differentiation for element %s, %s! : %s neq %s" % (i, j, A[2 * i, 2 * j], A[2 * i + 1, 2 * j + 1])
            A_sym[i, j] = 0.5 * (A[2 * i, 2 * j] + A[2 * i + 1, 2 * j + 1])

    return A_sym


def calc_Ek_T(ri, R, lam, T, spin_sym=True):
    '''
    compute total energy
    '''
    if spin_sym:
        return 2.*sum( [np.sum( np.dot( R[::2,::2], np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T) )*calc_nf( np.dot(R[::2,::2], 
                         np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T ) ) + lam[::2,::2], T).T ) for k in range(ri.dft.num_k_all)] )/ri.dft.num_k_all
    else:
        e_up = sum( [np.sum( np.dot( R[::2,::2], np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T) )*calc_nf( np.dot(R[::2,::2], 
                         np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T ) ) + lam[::2,::2], T).T ) for k in range(ri.dft.num_k_all)] )/ri.dft.num_k_all
        e_dn = sum( [np.sum( np.dot( R[1::2,1::2], np.dot(ri.Hk[k,:,:,1], R[1::2,1::2].conj().T) )*calc_nf( np.dot(R[1::2,1::2], 
                         np.dot(ri.Hk[k,:,:,1], R[1::2,1::2].conj().T ) ) + lam[1::2,1::2], T).T ) for k in range(ri.dft.num_k_all)] )/ri.dft.num_k_all
        ek_tot = e_up + e_dn
        return ek_tot


def calc_Ek_corr_T(ri, R, lam, T, spin_sym=True):
    '''
    compute total energy
    '''
    if spin_sym:
        no = R.shape[0]/2
        return 2.*sum( [np.sum( np.dot( R[::2,::2], np.dot(ri.Hk[k,-no:,-no:,0], R[::2,::2].conj().T) )*calc_nf( np.dot(R[::2,::2], 
                         np.dot(ri.Hk[k,-no:,-no:,0], R[::2,::2].conj().T ) ) + lam[::2,::2], T).T ) for k in range(ri.dft.num_k_all)] )/ri.dft.num_k_all
    else:
        no = R.shape[0]/2
        e_up = sum( [np.sum( np.dot( R[::2,::2], np.dot(ri.Hk[k,-no:,-no:,0], R[::2,::2].conj().T) )*calc_nf( np.dot(R[::2,::2], 
                         np.dot(ri.Hk[k,-no:,-no:,0], R[::2,::2].conj().T ) ) + lam[::2,::2], T).T ) for k in range(ri.dft.num_k_all)] )/ri.dft.num_k_all
        e_dn = sum( [np.sum( np.dot( R[1::2,1::2], np.dot(ri.Hk[k,-no:,-no:,1], R[1::2,1::2].conj().T) )*calc_nf( np.dot(R[1::2,1::2], 
                         np.dot(ri.Hk[k,-no:,-no:,1], R[1::2,1::2].conj().T ) ) + lam[1::2,1::2], T).T ) for k in range(ri.dft.num_k_all)] )/ri.dft.num_k_all
        ek_tot = e_up + e_dn
        return ek_tot


def calc_Sqp(ri, R, lam, T, spin_sym=True):
    '''
    Compute quasiparticle entropy
    '''
    if spin_sym:
        Sqp = -np.sum([np.dot(calc_C_T(np.dot(R[::2,::2], np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T)) + lam[::2,::2], T).T, \
                        calc_logH(calc_C_T(np.dot(R[::2,::2], np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T)) + lam[::2,::2], T).T)) + \
                        np.dot(np.eye(ri.Hk[k,:,:,0].shape[0]) - calc_C_T(np.dot(R[::2,::2], np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T)) \
                        + lam[::2,::2], T).T, calc_logH(np.eye(ri.Hk[k,:,:,0].shape[0]) - calc_C_T(np.dot(R[::2,::2], np.dot(ri.Hk[k,:,:,0], \
                        R[::2,::2].conj().T)) + lam[::2,::2], T).T)) for k in range(ri.dft.num_k_all)] ) / ri.dft.num_k_all
        return 2.*Sqp.real        
    else:
        Sqp_up = -np.sum([np.dot(calc_C_T(np.dot(R[::2,::2], np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T)) + lam[::2,::2], T).T, \
                        calc_logH(calc_C_T(np.dot(R[::2,::2], np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T)) + lam[::2,::2], T).T)) + \
                        np.dot(np.eye(ri.Hk[k,:,:,0].shape[0]) - calc_C_T(np.dot(R[::2,::2], np.dot(ri.Hk[k,:,:,0], R[::2,::2].conj().T)) \
                        + lam[::2,::2], T).T, calc_logH(np.eye(ri.Hk[k,:,:,0].shape[0]) - calc_C_T(np.dot(R[::2,::2], np.dot(ri.Hk[k,:,:,0], \
                        R[::2,::2].conj().T)) + lam[::2,::2], T).T)) for k in range(ri.dft.num_k_all)] ) / ri.dft.num_k_all
        Sqp_dn = -np.sum([np.dot(calc_C_T(np.dot(R[1::2,1::2], np.dot(ri.Hk[k,:,:,1], R[1::2,1::2].conj().T)) + lam[1::2,1::2], T).T, \
                        calc_logH(calc_C_T(np.dot(R[1::2,1::2], np.dot(ri.Hk[k,:,:,1], R[1::2,1::2].conj().T)) + lam[1::2,1::2], T).T)) + \
                        np.dot(np.eye(ri.Hk[k,:,:,1].shape[0]) - calc_C_T(np.dot(R[1::2,1::2], np.dot(ri.Hk[k,:,:,1], R[1::2,1::2].conj().T)) \
                        + lam[1::2,1::2], T).T, calc_logH(np.eye(ri.Hk[k,:,:,1].shape[0]) - calc_C_T(np.dot(R[1::2,1::2], np.dot(ri.Hk[k,:,:,1], \
                        R[1::2,1::2].conj().T)) + lam[1::2,1::2], T).T)) for k in range(ri.dft.num_k_all)] ) / ri.dft.num_k_all
        return Sqp_up.real + Sqp_dn.real
#
#    Sqp = -sum(map(lambda x: np.sum(np.dot(calc_C_T(np.dot(R, np.dot(x, R.conj().T)) + lam, T).T, \
#               calc_logH(calc_C_T(np.dot(R, np.dot(x, R.conj().T)) + lam, T).T)) + \
#               np.dot(np.eye(x.shape[0]) - calc_C_T(np.dot(R, np.dot(x, R.conj().T)) + lam, T).T, \
#               calc_logH(np.eye(x.shape[0]) - calc_C_T(np.dot(R, np.dot(x, R.conj().T)) + lam, T).T)))\
#               , ek_list)) / len(ek_list)
#    return Sqp.real


#########################################################################
# This interface is written Dr Yongxin Yao in Ames Lab
#########################################################################
def gen_spci_input_file(U, E, D, LAM, na2, norb_mott, nval_bot, nval_top, sig):
    '''
    Generate input files for spci.
    '''
    # print sig
    f = h5py.File('EMBED_HAMIL_1.h5', 'w')
    f['/D'] = D.T
    f['/H1E'] = E.T
    f['/LAMBDA'] = LAM.T
    f['/V2E'] = U.T
    f['/na2'] = [na2]
    f['/norb_mott'] = [norb_mott]
    f['/nval_bot'] = [nval_bot]
    f['/nval_top'] = [nval_top]
    f['/sigma_struct'] = sig
    f.close()

def get_full_fock_states_by_int(norb):
    '''generate all the fock states represented by integers.
    '''
    fock_list = [[] for i in range(norb+1)]
    for i in range(2**norb):
        n1 = bin(i).count('1')
        fock_list[n1].append(i)
    return fock_list


def get_double_fock_states_at_half_filling(fock_list):
    '''generate the impurity-bath fock states at half-filling.
    '''
    norb = len(fock_list)-1
    base = 2**norb
    nstates = sum([len(fs)**2 for fs in fock_list])
    fock2_array = np.empty(nstates, dtype=np.int32)
    iadd = 0
    for i, fs in enumerate(fock_list):
        for fs1 in fs:
            for fs2 in fock_list[norb-i]:
                fock2_array[iadd] = fs1 + fs2*base
                iadd += 1
    return fock2_array


def h5add_fock_states(norb, f_h5):
    '''add the fock state basis.
    '''
    fock_list = get_full_fock_states_by_int(norb)
    fock2_array = get_double_fock_states_at_half_filling(fock_list)
    f_h5["/fock_basis/dim"] = [len(fock2_array)]
    f_h5["/fock_basis/vals"] = fock2_array


def get_whole_h1e(h1e, lambdac, daalpha):
    '''get the total one-body part of the embedding hamiltonian
    given the components of impurity, bath and hybridization.
    '''
    norb = h1e.shape[0]
    v1e = block_diag(h1e, -lambdac)
    v1e[:norb, norb:] = daalpha.T
    v1e[norb:, :norb] = daalpha.conj()
    return v1e


def create_ged_inp(imp, h1e, lambdac, daalpha, v2e, istep=0, rtol=1.e-10,
        nev=1, maxiter=1000):
    '''create/update the input file for the parallel ed solver.
    '''
    with h5py.File("GEDINP_{}.h5".format(imp), "w") as f:
        if v2e is not None:
            ind_list = np.where(np.abs(v2e) > rtol)
            val_list = v2e[ind_list]
            # in fortran convention, one-based
            f["/v2e/nnz"] = [len(val_list)]
            ind_list = np.array(ind_list)+1
            f["/v2e/INDS"] = ind_list.T
            # absorb the 1/2 factor.
            f["/v2e/vals"] = val_list/2.

        v1e = get_whole_h1e(h1e, lambdac, daalpha)
        ind_list = np.where(np.abs(v1e) > rtol)
        val_list = v1e[ind_list]
        if "/v1e" in f:
            del f["/v1e"]
        norb = v1e.shape[0]
        f["/v1e/norb"] = [norb]
        f["/v1e/nnz"] = [len(val_list)]
        ind_list = np.array(ind_list)+1
        f["/v1e/INDS"] = ind_list.T
        f["/v1e/vals"] = val_list

        if "/ed" not in f:
            f["/ed/nev"] = [nev]
            f["/ed/maxiter"] = [maxiter]
            f["/ed/rtol"] = [rtol]
            h5add_fock_states(norb/2, f)

        f['/D'] = daalpha.T
        f['/H1E'] = h1e.T
        f['/LAMBDA'] = lambdac.T
        #f['/V2E'] = U.T


#def driver_ed(imp=1, istep=0, mpiexec="mpirun -np 2 "):
#    '''dmrg solver based on ITensor.
#    '''
#    if "-i" in sys.argv:
#        imp = int(sys.argv[sys.argv.index("-i")+1])
#    if os.path.isfile("GEDINP_{}.h5".format(imp)):
#        lv2e = False
#    else:
#        lv2e = True
#    h1e, lambdac, daalpha, v2e = h5gen_embedh_spin_updn(
#            imp=imp, lv2e=lv2e)
#    create_ged_inp(imp, h1e, lambdac, daalpha, v2e, istep=istep)
#
#    if os.path.isfile("mpi_ed.txt"):
#        mpiexec = open("mpi_ed.txt", "r").readline()
#
#    mpiexec = shlex.split(mpiexec)
#    cmd = mpiexec+[os.environ["WIEN_GUTZ_ROOT2"]+"/exe_ed", "-i", str(imp)]
#
#    print(" running {}".format(" ".join(cmd)))
#    subprocess.call(cmd)
#
#    # get the calculation results in cmp_sph_harm basis with
#    # faster spin index.
#    with h5py.File("GEDOUT_{}.h5".format(imp)) as f:
#        dm = f["/DM"][()].T
#        emol = f["/emol"][0]
#
#    # transform it to the symmetry-adpated basis
#    h5wrt_dm_sab_cmplx(dm, emol, imp=imp)

##############################################################################
#                           U_matrix from triqs                              #
##############################################################################


# Rotation matrices: complex harmonics to cubic harmonics
# Complex harmonics basis: ..., Y_{-2}, Y_{-1}, Y_{0}, Y_{1}, Y_{2}, ...
def spherical_to_cubic(l, convention=''):
    r"""
    Get the spherical harmonics to cubic harmonics transformation matrix.

    Parameters
    ----------
    l : integer
        Angular momentum of shell being treated (l=2 for d shell, l=3 for f shell).
    convention : string, optional
                 The basis convention.
                 Takes the values

                 - '': basis ordered as ("xy","yz","z^2","xz","x^2-y^2"),
                 - 'wien2k': basis ordered as ("z^2","x^2-y^2","xy","yz","xz").

    Returns
    -------
    T : real/complex numpy array
        Transformation matrix for basis change.

    """

    if not convention in ('wien2k', ''):
        raise ValueError("Unknown convention: " + str(convention))

    size = 2 * l + 1
    T = np.zeros((size, size), dtype=complex)
    if convention == 'wien2k' and l != 2:
        raise ValueError("spherical_to_cubic: wien2k convention implemented only for l=2")
    if l == 0:
        cubic_names = ("s")
    elif l == 1:
        cubic_names = ("x", "y", "z")
        T[0, 0] = 1.0 / sqrt(2);   T[0, 2] = -1.0 / sqrt(2)
        T[1, 0] = 1j / sqrt(2);    T[1, 2] = 1j / sqrt(2)
        T[2, 1] = 1.0
    elif l == 2:
        if convention == 'wien2k':
            cubic_names = ("z^2", "x^2-y^2", "xy", "yz", "xz")
            T[0, 2] = 1.0
            T[1, 0] = 1.0 / sqrt(2);   T[1, 4] = 1.0 / sqrt(2)
            T[2, 0] = -1.0 / sqrt(2);   T[2, 4] = 1.0 / sqrt(2)
            T[3, 1] = 1.0 / sqrt(2);   T[3, 3] = -1.0 / sqrt(2)
            T[4, 1] = 1.0 / sqrt(2);   T[4, 3] = 1.0 / sqrt(2)
        else:
            cubic_names = ("xy", "yz", "z^2", "xz", "x^2-y^2")
            T[0, 0] = 1j / sqrt(2);    T[0, 4] = -1j / sqrt(2)
            T[1, 1] = 1j / sqrt(2);    T[1, 3] = 1j / sqrt(2)
            T[2, 2] = 1.0
            T[3, 1] = 1.0 / sqrt(2);   T[3, 3] = -1.0 / sqrt(2)
            T[4, 0] = 1.0 / sqrt(2);   T[4, 4] = 1.0 / sqrt(2)
    elif l == 3:
        cubic_names = ("x(x^2-3y^2)", "z(x^2-y^2)", "xz^2", "z^3", "yz^2", "xyz", "y(3x^2-y^2)")
        T[0, 0] = 1.0 / sqrt(2);    T[0, 6] = -1.0 / sqrt(2)
        T[1, 1] = 1.0 / sqrt(2);    T[1, 5] = 1.0 / sqrt(2)
        T[2, 2] = 1.0 / sqrt(2);    T[2, 4] = -1.0 / sqrt(2)
        T[3, 3] = 1.0
        T[4, 2] = 1j / sqrt(2);   T[4, 4] = 1j / sqrt(2)
        T[5, 1] = 1j / sqrt(2);   T[5, 5] = -1j / sqrt(2)
        T[6, 0] = 1j / sqrt(2);   T[6, 6] = 1j / sqrt(2)
    else: raise ValueError("spherical_to_cubic: implemented only for l=0,1,2,3")

    return np.matrix(T)


##############################################################################
#   Below are Routines Copy from Yongxin's pyglib.mbody.coulomb_matrix       #
##############################################################################


def unitary_transform_coulomb_matrix(a, u):
    '''Perform a unitary transformation (u) on the Coulomb matrix (a).
    '''
    a_ = np.asarray(a).copy()
    m_range = list(range(a.shape[0]))
    for i, j in it.product(m_range, m_range):
        a_[i, j, :, :] = u.T.conj().dot(a_[i, j, :, :].dot(u))
    a_ = a_.swapaxes(0, 2).swapaxes(1, 3)
    for i, j in it.product(m_range, m_range):
        a_[i, j, :, :] = u.T.conj().dot(a_[i, j, :, :].dot(u))
    return a_


# The interaction matrix in desired basis.
# U^{spherical}_{m1 m4 m2 m3} =
# \sum_{k=0}^{2l} F_k angular_matrix_element(l, k, m1, m2, m3, m4)
# H = \frac{1}{2} \sum_{ijkl,\sigma \sigma'} U_{ikjl}
# a_{i \sigma}^\dagger a_{j \sigma'}^\dagger a_{l \sigma'} a_{k \sigma}.
def U_matrix_slater(l, radial_integrals=None, U_int=None, J_hund=None):
    r"""
    Calculate the full four-index U matrix being given either
    radial_integrals or U_int and J_hund.
    The convetion for the U matrix is that used to construct
    the Hamiltonians, namely:
    .. math:: H = \frac{1}{2} \sum_{ijkl,\sigma \sigma'} U_{ikjl}
            a_{i \sigma}^\dagger a_{j \sigma'}^\dagger
            a_{l \sigma'} a_{k \sigma}.
    Parameters
    ----------
    l : integer
        Angular momentum of shell being treated
        (l=2 for d shell, l=3 for f shell).
    radial_integrals : list, optional
                       Slater integrals [F0,F2,F4,..].
                       Must be provided if U_int and J_hund are not given.
                       Preferentially used to compute the U_matrix
                       if provided alongside U_int and J_hund.
    U_int : scalar, optional
            Value of the screened Hubbard interaction.
            Must be provided if radial_integrals are not given.
    J_hund : scalar, optional
             Value of the Hund's coupling.
             Must be provided if radial_integrals are not given.
    Returns
    -------
    U_matrix : float numpy array
               The four-index interaction matrix in the chosen basis.
    """

    # Check all necessary information is present and consistent
    if radial_integrals is None and (U_int is None and J_hund is None):
        raise ValueError("U_matrix: provide either the radial_integrals" + 
                " or U_int and J_hund.")
    if radial_integrals is None and (U_int is not None and J_hund is not None):
        radial_integrals = U_J_to_radial_integrals(l, U_int, J_hund)
    if radial_integrals is not None and \
            (U_int is not None and J_hund is not None):
        if len(radial_integrals) - 1 != l:
            raise ValueError("U_matrix: inconsistency in l" + 
                    " and number of radial_integrals provided.")
        if not np.allclose(radial_integrals,
                U_J_to_radial_integrals(l, U_int, J_hund)):
            print((" Warning: U_matrix: radial_integrals provided\n" + 
            " do not match U_int and J_hund.\n" + 
            " Using radial_integrals to calculate U_matrix."))

    # Full interaction matrix
    # Basis of spherical harmonics Y_{-2}, Y_{-1}, Y_{0}, Y_{1}, Y_{2}
    # U^{spherical}_{m1 m4 m2 m3} = \sum_{k=0}^{2l} F_k
    # angular_matrix_element(l, k, m1, m2, m3, m4)
    U_matrix = np.zeros((2 * l + 1, 2 * l + 1, 2 * l + 1, 2 * l + 1), dtype=np.float64)

    m_range = list(range(-l, l + 1))
    for n, F in enumerate(radial_integrals):
        k = 2 * n
        for m1, m2, m3, m4 in it.product(m_range, m_range, m_range, m_range):
            U_matrix[m1 + l, m2 + l, m3 + l, m4 + l] += \
                    F * angular_matrix_element(l, k, m1, m2, m4, m3)
    return U_matrix


def U_matrix_kanamori(n_orb, U_int, J_hund):
    r"""
    Calculate the Kanamori U and Uprime matrices.
    Parameters
    ----------
    n_orb : integer
            Number of orbitals in basis.
    U_int : scalar
            Value of the screened Hubbard interaction.
    J_hund : scalar
             Value of the Hund's coupling.
    Returns
    -------
    U_matrix : float numpy array
               The four-index interaction matrix in the chosen basis.
    """

    U_matrix = np.zeros((n_orb, n_orb, n_orb, n_orb), dtype=np.float64)
    m_range = list(range(n_orb))
    for m, mp in it.product(m_range, m_range):
        if m == mp:
            U_matrix[m, m, mp, mp] = U_int
        else:
            U_matrix[m, mp, mp, m] = U_int - 2.0 * J_hund
            U_matrix[m, m, mp, mp] = J_hund
            U_matrix[m, mp, m, mp] = J_hund
    return U_matrix

# Convert U,J -> radial integrals F_k
def U_J_to_radial_integrals(l, U_int, J_hund):
    r"""
    Determine the radial integrals F_k from U_int and J_hund.
    Parameters
    ----------
    l : integer
        Angular momentum of shell being treated
        (l=2 for d shell, l=3 for f shell).
    U_int : scalar
            Value of the screened Hubbard interaction.
    J_hund : scalar
             Value of the Hund's coupling.
    Returns
    -------
    radial_integrals : list
                       Slater integrals [F0,F2,F4,..].
    """

    F = np.zeros((l + 1), dtype=np.float64)
    F[0] = U_int
    if l == 0:
        pass
    elif l == 1:
        F[1] = J_hund * 5.0
    elif l == 2:
        F[1] = J_hund * 14.0 / (1.0 + 0.625)
        F[2] = 0.625 * F[1]
    elif l == 3:
        F[1] = 6435.0 * J_hund / (286.0 + 195.0 * 0.668 + 250.0 * 0.494)
        F[2] = 0.668 * F[1]
        F[3] = 0.494 * F[1]
    else:
        raise ValueError(
            " U_J_to_radial_integrals: implemented only for l=0,1,2,3")
    return F


# Convert radial integrals F_k -> U,J
def radial_integrals_to_U_J(l, F):
    r"""
    Determine U_int and J_hund from the radial integrals.
    Parameters
    ----------
    l : integer
        Angular momentum of shell being treated
        (l=2 for d shell, l=3 for f shell).
    F : list
        Slater integrals [F0,F2,F4,..].
    Returns
    -------
    U_int : scalar
            Value of the screened Hubbard interaction.
    J_hund : scalar
             Value of the Hund's coupling.
    """

    U_int = F[0]
    if l == 0:
        J_Hund = 0.
    elif l == 1:
        J_hund = F[1] / 5.0
    elif l == 2:
        J_hund = F[1] * (1.0 + 0.625) / 14.0
    elif l == 3:
        J_hund = F[1] * (286.0 + 195.0 * 0.668 + 250.0 * 0.494) / 6435.0
    else: raise ValueError("radial_integrals_to_U_J:" + 
            " implemented only for l=2,3")

    return U_int, J_hund


# Angular matrix elements of particle-particle interaction
# (2l+1)^2 ((l 0) (k 0) (l 0))^2 \sum_{q=-k}^{k} (-1)^{m1+m2+q}
# ((l -m1) (k q) (l m3)) ((l -m2) (k -q) (l m4))
def angular_matrix_element(l, k, m1, m2, m3, m4):
    r"""
    Calculate the angular matrix element
    .. math::
       (2l+1)^2
       \begin{pmatrix}
            l & k & l \\
            0 & 0 & 0
       \end{pmatrix}^2
       \sum_{q=-k}^k (-1)^{m_1+m_2+q}
       \begin{pmatrix}
            l & k & l \\
         -m_1 & q & m_3
       \end{pmatrix}
       \begin{pmatrix}
            l & k  & l \\
         -m_2 & -q & m_4
       \end{pmatrix}.
    Parameters
    ----------
    l : integer
    k : integer
    m1 : integer
    m2 : integer
    m3 : integer
    m4 : integer
    Returns
    -------
    ang_mat_ele : scalar
                  Angular matrix element.
    """
    ang_mat_ele = 0
    for q in range(-k, k + 1):
        ang_mat_ele += three_j_symbol((l, -m1), (k, q), (l, m3)) * \
                three_j_symbol((l, -m2), (k, -q), (l, m4)) * \
                (-1.0 if (m1 + q + m2) % 2 else 1.0)
    ang_mat_ele *= (2 * l + 1) ** 2 * (three_j_symbol((l, 0), (k, 0), (l, 0)) ** 2)
    return ang_mat_ele


# Wigner 3-j symbols
# ((j1 m1) (j2 m2) (j3 m3))
def three_j_symbol(jm1, jm2, jm3):
    r"""
    Calculate the three-j symbol
    .. math::
       \begin{pmatrix}
        l_1 & l_2 & l_3\\
        m_1 & m_2 & m_3
       \end{pmatrix}.
    Parameters
    ----------
    jm1 : tuple of integers
          (j_1 m_1)
    jm2 : tuple of integers
          (j_2 m_2)
    jm3 : tuple of integers
          (j_3 m_3)
    Returns
    -------
    three_j_sym : scalar
                  Three-j symbol.
    """
    j1, m1 = jm1
    j2, m2 = jm2
    j3, m3 = jm3

    if (m1 + m2 + m3 != 0 or
        m1 < -j1 or m1 > j1 or
        m2 < -j2 or m2 > j2 or
        m3 < -j3 or m3 > j3 or
        j3 > j1 + j2 or
        j3 < abs(j1 - j2)):
        return .0

    three_j_sym = -1.0 if (j1 - j2 - m3) % 2 else 1.0
    three_j_sym *= np.sqrt(fact(j1 + j2 - j3) * fact(j1 - j2 + j3) * \
            fact(-j1 + j2 + j3) / fact(j1 + j2 + j3 + 1))
    three_j_sym *= np.sqrt(fact(j1 - m1) * fact(j1 + m1) * fact(j2 - m2) * \
            fact(j2 + m2) * fact(j3 - m3) * fact(j3 + m3))

    t_min = max(j2 - j3 - m1, j1 - j3 + m2, 0)
    t_max = min(j1 - m1, j2 + m2, j1 + j2 - j3)

    t_sum = 0
    for t in range(t_min, t_max + 1):
        t_sum += (-1.0 if t % 2 else 1.0) / (fact(t) * fact(j3 - j2 + m1 + t) * \
                fact(j3 - j1 - m2 + t) * fact(j1 + j2 - j3 - t) * fact(j1 - m1 - t) * fact(j2 + m2 - t))

    three_j_sym *= t_sum
    return three_j_sym


def get_average_uj(v2e):
    m_range = list(range(v2e.shape[0]))
    u_avg = 0; j_avg = 0
    isum_u = 0; isum_j = 0
    for i, j in it.product(m_range, m_range):
        u_avg += v2e[i, i, j, j]
        isum_u += 1
        if i != j:
            j_avg += v2e[i, i, j, j] - v2e[i, j, j, i]
            isum_j += 1
    u_avg /= isum_u
    if isum_j > 0:
        j_avg = u_avg - j_avg / isum_j
    return u_avg, j_avg
