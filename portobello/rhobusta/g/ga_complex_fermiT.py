#!/usr/bin/env python3

###########################################
#      GA root for Hubbard model
#                         Tsung-Han Lee
###########################################

import subprocess as sp
import sys
from itertools import product

import h5py
import numpy as np
from portobello.bus.fortran import Fortran
from portobello.bus.Matrix import Matrix
from portobello.bus.mpi import MPIContainer
from portobello.bus.persistence import Persistent
from portobello.generated import edx
from portobello.generated.edx import Tensor2, Tensor4
from portobello.rhobusta.g.GEngine import GEngine
from portobello.rhobusta.g.utils import (calc_nf, complexHcombination,
                                         create_ged_inp, cut_small, ddenRm1,
                                         denR, denRm1, dF,
                                         duplicate_in_spin_space,
                                         enlarge_in_spin_space, funcMat,
                                         gen_spci_input_file,
                                         inverse_complexHcombination,
                                         inverse_realHcombination,
                                         realHcombination,
                                         single_particle_symmetrize,
                                         get_blocks,
                                         updn_to_spinful_mat)
from portobello.rhobusta.orbitals import PrintMatrix
from portobello.rhobusta.PEScfEngine import PEScfEngine
from portobello.rhobusta.simple_ed.basis import (table_ep, table_ep2,
                                                 table_ep2_spin_symm,
                                                 table_ep_nrange, table_es)
from portobello.rhobusta.simple_ed.ed import simple_ed
from scipy.linalg import inv, block_diag

np.set_printoptions(precision=7,suppress=True)

# note this code is running in a single DFT shard
# so all k points in the irreducible  BZ are available.

# BUG: we have to use weights because this is over irr-bz.

def calc_rhok_rotate_projector(ri : PEScfEngine, R_all, Lambda_all, T, spin_sym, isrel):
    '''
    compute density matrix for each k-point
    '''
    I = ri.ID1F
    if spin_sym:
        rho_up = [calc_nf( np.dot(R_all[::2,::2], np.dot(ri.Hk[k,:,:,0], R_all[::2,::2].conj().T ) ) + Lambda_all[::2,::2] - ri.mu*I, T) 
                        for k in range(ri.dft.num_k_irr)]
        return [updn_to_spinful_mat(rho_up[k],rho_up[k]) for k in range(ri.dft.num_k_irr)]
    elif isrel:
        # full dim
        return [calc_nf( np.dot(R_all, np.dot(ri.Hk[k,:,:,0], R_all.conj().T ) ) + Lambda_all[:,:]- ri.mu*I, T) 
                        for k in range(ri.dft.num_k_irr)]
    else:
        rhok_up = [calc_nf( np.dot(R_all[::2,::2], np.dot(ri.Hk[k,:,:,0], R_all[::2,::2].conj().T ) ) + Lambda_all[::2,::2]- ri.mu*I, T) 
                        for k in range(ri.dft.num_k_irr)]
        rhok_dn = [calc_nf( np.dot(R_all[1::2,1::2], np.dot(ri.Hk[k,:,:,1], R_all[1::2,1::2].conj().T ) ) + Lambda_all[1::2,1::2]- ri.mu*I, T) 
                        for k in range(ri.dft.num_k_irr)]
        rhok = [updn_to_spinful_mat(rhok_up[k],rhok_dn[k]) for k in range(ri.dft.num_k_irr)]
        return rhok

def calc_Delta_p(rhok_list, ri):
    '''
    sum over all the k-points to get local density matrix
    '''
    return np.tensordot(rhok_list, ri.lapw.weight, axes=[0,0])

# RL is the derivative of big R by (small) Ri
def calc_D_rotate_projector(RL, R_all, Delta_p, ri, rhok_list, spin_sym, isrel, Hsf_list):
    '''
    compute embedding hybridization matrix
    '''
    if spin_sym:
        Left_up = [np.dot( np.dot( RL[::2,::2], np.dot(ri.Hk[k,:,:,0], R_all[::2,::2].conj().T ) ), rhok_list[k][::2,::2] )
                        for k in range(ri.dft.num_k_irr)]
        Left = np.tensordot(Left_up,ri.lapw.weight, axes=[0,0])
        ri.SymmetrizeCorrelated(Left, orbitalSymm=True)
        Left = updn_to_spinful_mat(Left, Left)[-2*ri.dim:,-2*ri.dim:]
    elif isrel:
        # full dim
        Left = [np.dot( np.dot( RL, np.dot(ri.Hk[k,:,:,0], R_all.conj().T ) ), rhok_list[k]) 
                    for k in range(ri.dft.num_k_irr)]
        Left = np.tensordot(Left,ri.lapw.weight, axes=[0,0])[-ri.dim:,-ri.dim:]
        ri.SymmetrizeCorrelated(Left, orbitalSymm=True)
    else:
        Left_up = [np.dot( np.dot( RL[::2,::2], np.dot(ri.Hk[k,:,:,0], R_all[::2,::2].conj().T ) ), rhok_list[k][::2,::2] ) 
                    for k in range(ri.dft.num_k_irr)] 
        Left_dn = [np.dot( np.dot( RL[1::2,1::2], np.dot(ri.Hk[k,:,:,1], R_all[1::2,1::2].conj().T ) ), rhok_list[k][1::2,1::2] ) 
                    for k in range(ri.dft.num_k_irr)] 
        Left_up = np.tensordot(ri.lapw.weight, Left_up, axes=[0,0]) 
        Left_dn = np.tensordot(ri.lapw.weight, Left_dn, axes=[0,0]) 
        ri.SymmetrizeCorrelated(Left_up, orbitalSymm=True)
        ri.SymmetrizeCorrelated(Left_dn, orbitalSymm=True)
        Left = updn_to_spinful_mat(Left_up, Left_dn)[-2*ri.dim:,-2*ri.dim:]
        # TODO: test symmetrize

    #Left = single_particle_symmetrize(Left, Hsf_list)
    Right=funcMat(Delta_p, denR)  # already includes inverse
    return np.dot(Right, Left)

def calc_Lambda_c(R, Lambda, Delta_p, D, Hsf_list):
    '''
    Compute embedding bath matrix
    '''
    l=inverse_realHcombination(Lambda,Hsf_list)
    lc=np.copy(l)*0.0
    MM=np.dot(D,np.transpose(R))

    for k in range(len(Hsf_list)):#no*(no+1)//2):
        AA=Delta_p
        HH=Hsf_list[k].T
        derivative=dF(AA,HH, denRm1, ddenRm1)
        tt=np.trace(np.dot(MM,derivative))
        lc[k]=-l[k]-(tt+np.conjugate(tt)).real
    # Equation 3 (l_c)
    Lambda_c=realHcombination(lc,Hsf_list)#realScombination(lc,Hsf_list)
    return Lambda_c

def solve_embedding_problem_spci(_i_docc, spin_sym, isrel, ri, D, Lambda_c, Ed, Umat, no, _verbose):
    '''
    Solving embedding Hamiltonian using Yongxin's spci
    '''
    imp = 1
    #create local one-body and two-body interaction
    E = Ed
 
    D = np.array(D,np.complex)
    Lambda_c = np.array(Lambda_c,np.complex)

    if type(Umat) == np.ndarray:
        Umatrix = np.array(Umat,np.complex)
    else:
        Umatrix=np.zeros((no,no,no,no),np.complex)
        for l in Umat:
            Umatrix[l[0],l[2],l[1],l[3]]=l[4]

    if spin_sym:
        sig = np.arange(1,no//2*no//2+1).reshape((no//2,no//2))
        sig = updn_to_spinful_mat(sig,sig)
    elif isrel:
        sig = ri.rep
    else:
        sig = np.arange(1,no*no+1).reshape((no,no))
    
    gen_spci_input_file(Umatrix, E, D, Lambda_c, no, 0, 0, no, sig)

    #cmd = "my_exe_spci %i"%(imp)
    cmd = "exe_spci_mott %i"%(imp)
    #print "Executing: ", cmd, " to compute ground state wavefunction and density matrix."
    _out, _err = sp.Popen(cmd, shell=True, stdout=sp.PIPE, stderr=sp.PIPE).communicate() # Equation 4 (Phi, E)
    fres = h5py.File('EMBED_HAMIL_RES_1.h5','r')

    denMat = fres['/DM'][...].T
    fres.close()

    return denMat, 0.25

def solve_embedding_problem_yongxin_ged(_i_docc, _spin_sym, D, Lambda_c, Ed, Umat, no, _verbose, ri):
    imp = 1
    #create local one-body and two-body interaction
    E = Ed
 
    D = np.array(D,np.complex)
    Lambda_c = np.array(Lambda_c,np.complex)

    if type(Umat) == np.ndarray:
        Umatrix = np.array(Umat,np.complex)
    else:
        Umatrix=np.zeros((no,no,no,no),np.complex)
        for l in Umat:
            Umatrix[l[0],l[2],l[1],l[3]]=l[4]
    
    create_ged_inp(1, E, Lambda_c, D, Umatrix, istep=0, rtol=1.e-10, nev=1, maxiter=1000)

    print("*** number of workers: ", ri.mpi_workers)
    cmd = 'mpirun -np '+str(ri.mpi_workers)+ " exe_ed"  + ' -i '+str(imp)
    print('running: '+cmd)
    _out, _err = sp.Popen(cmd, shell=True, stdout=sys.stdout, stderr=sys.stdout).communicate() # Equation 4 (Phi, E)

    fres = h5py.File('GEDOUT_1.h5','r')
    _emol = fres['/emol'][...]
    denMat = fres['/DM'][...].T
    fres.close()

    return denMat, 0.25



# called by the main function
def RunEdServer():
    from portobello.bus.persistence import Store
    fortran = Fortran.Instance()
    ex_entry_point = fortran.GetFunction("ed_rhobusta_driver.mp.run", check_gfort = True)
    intercomm = MPI.Comm.Get_parent()
    intercomm.barrier()
    while True:
        command = intercomm.bcast("", root=0) 
        if command == "start":  
            Persistent.Refresh("./edx.tmp.h5:/input/") #refresh this file, it may have been written outside
            # note that at this point we can only refresh in Python (which does the work for the fortran code as well)
            ex_entry_point()
            intercomm.barrier()
        elif command == "terminate":
            sys.stdout.flush()
            intercomm.barrier()
            #MPIContainer.Terminate()
            break
        
class EDXSupervisor(MPIContainer):
    instance = None
    @staticmethod
    def runOneInstance(mpi_workers):
        if EDXSupervisor.instance is None:
            EDXSupervisor.instance = EDXSupervisor(mpi_workers)
            sys.stdout.flush()
            EDXSupervisor.instance.Run(wait=False)
            EDXSupervisor.instance.spn.barrier()
        return EDXSupervisor.instance
    @staticmethod
    def isRunning():
        return not EDXSupervisor.instance is None
    
    def __init__(self, num_workers):
        MPIContainer.__init__(self, opts)
    def WorkerExecutable(self):
        return "python3"
    def WorkerArgs(self):
        return ["-m", "portobello.rhobusta.g.ga_complex_fermiT"] 
    
def RunEDXInParallel(mpi_workers):
    sup = EDXSupervisor.runOneInstance(mpi_workers)
    sup.spn.bcast("start", root=MPI.ROOT)
    sup.spn.barrier()
    sup.spn.bcast("end", root=MPI.ROOT)

def TerminateEDXServer():
    if EDXSupervisor.isRunning():
        sys.stdout.flush()
        sup = EDXSupervisor.instance
        sup.spn.bcast("terminate", root=MPI.ROOT)
        sup.spn.barrier()
        EDXSupervisor.instance = None
 
class IEDState():
    def __init__(self):
        self.iter = 0

#
# -------------------------------------------------------------------------------------------
#      This is a 32-bit version of EDX, because all of the ints have been compiled as int32.
#      This is enough for gutzwiller f-shell (28 bits needed, 31 are the max because of the sign bit)
# -------------------------------------------------------------------------------------------
def SolveEmbeddingWithEdX(D, Lambda_c, H1e, Umat, verbose, ri, opts):    
    if not hasattr(ri, 'ied'):
        ri.ied = IEDState()
    
    #H1e = np.diag(np.diag(H1e)) # want only the diagonal part
    norb = H1e.shape[0]
    assert(H1e.shape[0]==D.shape[0])
    assert(H1e.shape[0]==Lambda_c.shape[0])

    rtol = max(opts.interaction_truncation, 1.0e-10)
    ed_solver = 2 # ARPACK = 2, lanczos = 1, matrix - 0
    
    # initialize the hopping rep
    if ri.nrel == 2:
        rep = np.block([[ri.rep, ri.rep], 
                        [ri.rep, ri.rep]])
    else:
        rep2 = np.zeros((ri.dim*2, ri.dim*2), np.int32)
        rep2[::2,::2] = ri.rep[:,:]
        if ri.num_si == 2:
            maxRi =  np.max(ri.rep[:,:])
        else:
            maxRi = 0
        rep_ = np.copy(ri.rep) 
        for i,j in product(range(rep_.shape[0]), repeat=2):
            if rep_[i,j] > 0:
                rep_[i,j] += maxRi
        rep2[1::2,1::2] = rep_[:,:]
        rep = np.block([[rep2, rep2], 
                        [rep2, rep2]])
        
    
    # only for 1st iteration prepare the setup data structure
    if not EDXSupervisor.isRunning():
        # this is quite inefficient, but it happens only once
        nrange = PEScfEngine.GetNRange(opts.N0)
        l = (norb//2 -1)//2
        if nrange is None:
            if ri.nrel < 2 and ri.num_si ==1:
                fock_states = table_ep2_spin_symm(l) # why doesn't this work?

                # this one is used in Tsung-Han's code. I'm not sure what it sorresponds to. 
                # as it seems smaller than the space table_ep2_spin_symm() which is conserved total N and total spin.
                #fock_states = table_es(norb*2, norb, 0, np.int32) 

                # this one also works (slightly different result), it's the bigger space where only N is conserved.
                #fock_states=table_ep2(l)
            else:
                # only N conserved
                fock_states=table_ep2(l)
        else: 
            print("- Building fock space for l=%d with range %s"%(l, nrange))
            fock_states=table_ep_nrange(l=l, nrange=nrange)
                
                        
        setup = edx.Setup(num_fock = len(fock_states))
        setup.allocate()
        setup.num_val_orbs = norb * 2
        setup.nvector = 1
        setup.neval = 1
        setup.debug_print = verbose == 1
        setup.prefix = f"{opts.solver_tmp_dir}/V0.tmp.part-"
        
        #for arpack
        setup.ncv = 30

        setup.ed_solver = ed_solver
        
        setup.eigval_tol = 1.0e-8 # if this is too large, it will send gutz in the wrong direction
        setup.maxiter = 500
        
        setup.fock[:] = fock_states[:]
        ulen = 0
        for (i1,i2,i3,i4), val in np.ndenumerate(Umat):
            if np.abs(val) >= rtol and i1 != i2 and i3 != i4:
                ulen += 1
            
        setup.coulomb = Tensor4(len = ulen)
        setup.coulomb.allocate()
        idx = 0
        for (i1,i2,i3,i4), val in np.ndenumerate(Umat):
            if np.abs(val) >= rtol and i1 != i2 and i3 != i4: # fermionic exclusion: two incoming and two outoing are different states (note these are full states) 
                setup.coulomb.i1[idx] = i1 + 1
                setup.coulomb.i2[idx] = i2 + 1
                setup.coulomb.i3[idx] = i3 + 1 
                setup.coulomb.i4[idx] = i4 + 1
                setup.coulomb.value[idx] = val
                idx+=1
           
        setup.hopping_rep = Tensor2(len=np.sum(rep != 0))
        setup.hopping_rep.allocate()
        idx = 0
        for (i1,i2), val in np.ndenumerate(rep):
            if val != 0:
                setup.hopping_rep.i1[idx] = i1 + 1
                setup.hopping_rep.i2[idx] = i2 + 1
                setup.hopping_rep.value[idx] = val*1.0 # the value doesn't matter, it's just for documentation
                idx+=1
        setup.store("./edx.tmp.h5:/setup/", flush=True)
    
    inp = edx.Input()
    Lambda_c = np.array(Lambda_c,np.complex128)
    D = np.array(D,np.complex128)
        
    Dt = Matrix(np.transpose(D))
    
    v1e = np.block([[H1e,    Dt      ],
                    [Dt.H,   -Lambda_c]])   

    if verbose==1: PrintMatrix("H1e", H1e)

    inp.hopping = Tensor2(len = np.sum(rep != 0))
    inp.hopping.allocate()
    idx = 0
    num_off_diag = 0
    for (i1,i2), val in np.ndenumerate(v1e):
        if i1 != i2 and rep[i1,i2]:
            num_off_diag +=1
        # the condition about rep here maintains assumption ASSUME:HOPPING_IN_REP, search
        # this string in Fortran to find where it is used
        if rep[i1,i2] != 0: # here we restrict to the rep
            inp.hopping.i1[idx] = i1 + 1
            inp.hopping.i2[idx] = i2 + 1
            inp.hopping.value[idx] = val
            #print "** added hopping ", idx, i1+1, i2+1, val
            idx+=1
    inp.store("./edx.tmp.h5:/input/", flush=True)
  
    if num_off_diag >0 and verbose == 1:
        PrintMatrix(" - hopping matrix has off diagonals:", v1e)
    
    out = edx.Output()
    repeats = 1
    if ed_solver == 1: 
        repeats = 6
    for i in range(repeats):
        if i>0:
            print("convergence failed ... RETRYING solver")
            sys.stdout.flush()
        RunEDXInParallel(ri.mpi_workers)
        out.load("./edx_out.tmp.h5:/", disconnect=True)
        #if not os.path.exists("./edx_first.h5"):
        #    out.store("./edx_first.h5:/", flush=True)
        if out.is_valid:
            break
    if not out.is_valid:
        print("*** diagonalization solver convergence failed! ***")
    ri.ied.out = out
    ri.ied.D = D
    ri.ied.Lambda_c = Lambda_c
    
    ri.ied.iter +=1
    
    return out.denmat[:,:,0] # TODO: complete the energy, occupation
    


def solve_embedding_problem_simple_ed(solver : simple_ed, i_docc, spin_sym, D, Lambda_c, ed_list, Umat, no, _verbose):
    #create local one-body and two-body interaction
    #if type(ed_list) == np.ndarray:
    #  E = ed_list
    #else:
    #  E = np.zeros((no,no))
    #  for l in ed_list:
    #      E[l[0],l[1]]+=l[2]
    #if verbose:
    #  print "E="
    #  print E
    E = np.array(ed_list, np.complex128)
    #E = np.kron(E,np.eye(2))

    # generate h5 for embedding Hamiltonian
    if spin_sym:
        sig = np.arange(1,no//2*no//2+1).reshape((no//2,no//2))
        sig = np.kron(sig,np.eye(2))
    else:
        sig = np.arange(1,no*no+1).reshape((no,no))
    gen_spci_input_file(Umat, E, D, Lambda_c, no, 0, 0, no, sig)

    solver.build_one_body(E, D, Lambda_c, debug=False)
    if spin_sym:
        solver.build_Ham(s2pen=0.02,debug=True)
    else:
        solver.build_Ham(s2pen=0.0,debug=True)
    print("diagonalizing ..................................")
    solver.diagonalize()
    denMat = solver.compute_denmat()
    docc = solver.compute_docc_i(i_docc) # We don't compute double occupancy
    Eloc = solver.compute_Eloc()
    np.savetxt('Eloc.dat',[Eloc.real])

    fres = h5py.File('EMBED_HAMIL_RES_1.h5','w')
    fres['/DM'] = denMat
    fres['/eloc'] = Eloc
    fres['/emol'] = solver.gs_ene
    fres.close()    

    return denMat, docc, Eloc#, Phi, Eloc
    
def root_GA(x, *args):
    """
    GA root function.
    """
    ri : GEngine
    T, ri, opts, Umat, Hsl_list, i_docc, isrel, spin_sym, solver, verbose = args

    if not isrel:
        no = 2*ri.dim # number of correlated orbitals including spin
    else:
        no = ri.dim # this is a full dim case

    if verbose == 1:
        print("number of orbitals=",no)

    #print "x=",x

    # mapt r and l list to R and Lambda matrix
    if spin_sym:
        # Hermitian matrix basis for R and Lambda with duplicate in spin space
        Hsf_list=[duplicate_in_spin_space(m) for m in Hsl_list]
        # normalize
        Hsf_list=[m/np.sqrt(np.trace(np.dot(m.conj().T,m))) for m in Hsf_list]
        
        # get r and l (parameters for R and Lambda) from input x
        r = x[0:2*len(Hsl_list)]
        l = x[2*len(Hsl_list):3*len(Hsl_list)] # for hybr

        # build R and Lambda without duplicate in spin space
        Rsym = complexHcombination(r,Hsl_list)
        Lambdasym = realHcombination(l,Hsl_list)

        if verbose==1:
            PrintMatrix("Rsym", Rsym)
            PrintMatrix("Lambdasym",Lambdasym)

        # Duplicate spin space for R and Lambda 
        R = updn_to_spinful_mat(Rsym,Rsym)
        Lambda = updn_to_spinful_mat(Lambdasym,Lambdasym)
        R_afm = R
        Lambda_afm = R
    elif isrel:
        # normalize
        Hsf_list=[m/np.sqrt(np.trace(np.dot(m.conj().T,m))) for m in Hsl_list]
        
        # get r and l (parameters for R and Lambda) from input x
        r = x[0:2*len(Hsl_list)]
        l = x[2*len(Hsl_list):3*len(Hsl_list)] # for hybr

        # build R and Lambda without duplicate in spin space
        R = complexHcombination(r,Hsl_list)
        Lambda = realHcombination(l,Hsl_list)
        
        R_afm = R
        Lambda_afm = R
    else:
        # Hermitian matrix basis for R and Lambda enlarged in spin space
        Hsf_list = []
        for h in Hsl_list:
            h_up,h_dn = enlarge_in_spin_space(h)
            Hsf_list.append(h_up)
            Hsf_list.append(h_dn)

        # get r and l (parameters for R and Lambda) from input x
        r_up = x[0:2*len(Hsl_list)]
        r_dn = x[2*len(Hsl_list):4*len(Hsl_list)]
        l_up = x[4*len(Hsl_list):5*len(Hsl_list)]
        l_dn = x[5*len(Hsl_list):6*len(Hsl_list)]
    
        # build spinful R and Lambda
        R_up = complexHcombination(r_up,Hsl_list)
        R_dn = complexHcombination(r_dn,Hsl_list)
        Lambda_up = realHcombination(l_up,Hsl_list)
        Lambda_dn = realHcombination(l_dn,Hsl_list)
        R = updn_to_spinful_mat(R_up, R_dn)
        Lambda = updn_to_spinful_mat(Lambda_up, Lambda_dn)
        R_afm = updn_to_spinful_mat(R_dn, R_up)
        Lambda_afm = updn_to_spinful_mat(Lambda_dn, Lambda_up)

    # note: anti-equivalents go FIRST, since we assume that the last subspace be an equivalent copy
    noWithEqvAtoms = no * (ri.num_equivalents+ ri.num_anti_equivalents) 
    noOnlyEquivalent = no * ri.num_equivalents
    sfac = 2
    if isrel: sfac = 1 # relativistic matrices are full
    num_bands = sfac*ri.Hk.shape[1]
    # R and R_afm have full (even) dimension
    R_all = np.eye(num_bands,dtype=np.complex)
    R_all[-noWithEqvAtoms::,-noWithEqvAtoms::] =\
            block_diag(*([R_afm for _i in range(ri.num_anti_equivalents)]+
                            [R for _i in range(ri.num_equivalents)] ))

    # derivative of the big R matrix by Ri
    # what is the correct way to do the AFM blocks?
    RL = np.zeros((num_bands,num_bands),dtype=np.complex)
    RL[-ri.dim*ri.num_equivalents:,-ri.dim*ri.num_equivalents:]= block_diag(*([ri.rep!=0 for _i in range(ri.num_equivalents)] ))

    R_aalpha = R
    Lambda_all = np.zeros((num_bands,num_bands),dtype=np.complex)
    Lambda_all[-noWithEqvAtoms::,-noWithEqvAtoms::] = \
            block_diag(*([Lambda_afm for _i in range(ri.num_anti_equivalents)]+
                            [Lambda for _i in range(ri.num_equivalents)])) 
                            
    if verbose == 1:
        PrintMatrix('R_all', R_all)
        PrintMatrix('Lambda_all=', Lambda_all)

    # calculate density matrices with R, Lambda, and epsilon_k

    rhok_list=calc_rhok_rotate_projector(ri, R_all, Lambda_all, T, spin_sym, isrel)
    
    # calculate Delta_p
    Delta_p = calc_Delta_p(rhok_list, ri)[-no::,-no::]
    #print "Delta_p calculation done!"
    # eliminate small element
    # Delta_p=cut_small(Delta_p)
    # symmetrize density matrix to respect local symmetry
    Delta_p = single_particle_symmetrize(Delta_p, Hsf_list)

    # calculate D
    D=calc_D_rotate_projector(RL, R_all, Delta_p, ri, rhok_list, spin_sym, isrel, Hsf_list)
    
    D = single_particle_symmetrize(D, Hsf_list)

    # calculate Lambda_c
    Lambda_c=calc_Lambda_c(R_aalpha,Lambda_all[-no:,-no:],Delta_p, D, Hsf_list)
    Lambda_c=cut_small(Lambda_c)  
    
    Hloc0 = ri.Hloc[:,:,0] - ri.Vdc[:,:,0]
    if spin_sym:
        Ed = updn_to_spinful_mat(Hloc0, Hloc0) 
    elif isrel:
        Ed = Hloc0
    else:
        Ed = updn_to_spinful_mat(Hloc0,  ri.Hloc[:,:1])
    # symmetrize Ed
    Ed = single_particle_symmetrize(Ed, Hsf_list)

    docc = None
    if solver == 'spci':
        denMat,docc=solve_embedding_problem_spci(i_docc, spin_sym, isrel, ri, D, Lambda_c, Ed, Umat, no, verbose)
    elif solver == 'ied':
        denMat = SolveEmbeddingWithEdX(D=D, Lambda_c=Lambda_c, H1e=Ed, Umat=Umat, verbose=verbose, ri=ri, opts=opts)
    elif solver == 'ged':
        denMat,docc=solve_embedding_problem_yongxin_ged(i_docc, spin_sym, D, Lambda_c, Ed, Umat, no, verbose, ri)
    elif type(solver) == simple_ed:
        denMat,docc,_Eloc=solve_embedding_problem_simple_ed(solver, i_docc=i_docc, spin_sym=spin_sym, 
                            D=D, Lambda_c=Lambda_c, ed_list=Ed, Umat=Umat, no=no, _verbose=verbose)

    if verbose == 1:
        PrintMatrix("denmat", denMat)
    
    #save density matrix
    np.savetxt('DenMat.dat',denMat, newline = "\r\n", fmt = '%.10e%+.10ej '*no*2)
    if docc is not None:
        np.savetxt('Docc.dat',[docc.real])
 
       
    #get <f_{a}f_{b}^{\dagger}>
    ffdagger = denMat[no:,no:]
    ffdagger = (np.eye(no) - ffdagger).T

    #solve the first root problem
    if spin_sym:
        F1 = ( get_blocks(denMat,no)[2] - np.dot(R_aalpha.T, funcMat(Delta_p, denRm1) ) )[::2,::2] # Equation 5 (F1)
    else:
        # relativistic and 2-spin case - no sub-matrix
        F1 = ( get_blocks(denMat,no)[2] - np.dot(R_aalpha.T, funcMat(Delta_p, denRm1) ) ) # Equation 5 (F1)
    
    #solve the second root problem
    if spin_sym:
        F2 = ( ffdagger.T - Delta_p )[::2,::2] # Equation 6 (F2)
    else:
        # relativistic and 2-spin case - no sub-matrix
        F2 = ( ffdagger.T - Delta_p ) # Equation 6 (F2)

    if spin_sym or isrel:
        F1_F2 = np.hstack((inverse_complexHcombination(F1,Hsl_list),inverse_realHcombination(F2,Hsl_list))).real
    else:
        F1_F2 = np.hstack((inverse_complexHcombination(F1[::2,::2],Hsl_list), inverse_complexHcombination(F1[1::2,1::2],Hsl_list),
                           inverse_realHcombination(F2[::2,::2],Hsl_list),inverse_realHcombination(F2[1::2,1::2],Hsl_list))).real

    maxval =  max(abs(F1_F2))

    ri.quality = maxval 
    
    #sum_F1_F2 = np.sum(abs(F1_F2))
    if verbose == 2 or verbose == 1:
        print("MAXERR=",maxval)

    if maxval < 5.0e-10:
        PrintMatrix('Delta_p=', Delta_p)
        PrintMatrix('<c^dagger_alpha c_beta>=', denMat[:no,:no])
        
        ri.success = 1 # signal success, despite just being close as far as the fortran engine knows
        
    # make the calculation stop if we are below this threshold
    # if maxval < 1.0e-8:
    #     F1_F2[:] = 0.0
        
    sys.stdout.flush()
            
    return F1_F2



if __name__ == '__main__':
    RunEdServer()
    
