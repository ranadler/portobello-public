#!/usr/bin/env python3

'''
Created on Dec 8, 2020

@author: adler
'''


from itertools import product
import json
from tkinter import W
from numpy import array,hstack,vstack,arctan, tan, exp,sqrt,pi,linspace,zeros,ones, dot, real, imag
import numpy as np
from numpy import ma, random
from requests import options
from scipy import optimize, interpolate, integrate
from argparse import ArgumentDefaultsHelpFormatter, Namespace

from pathlib import Path

from portobello.bus.Matrix import Matrix
from portobello.bus.mpi import GetMPIProxyOptionsParser, MPIContainer
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.dmft import DMFTOrchastrator
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.generated.dmft import DMFTState, ImpurityMeasurement
from portobello.generated.maxent import Input, Workspace
from portobello.generated.self_energies import LocalOmegaFunction
from portobello.rhobusta.PEScfEngine import PEScfEngine
from portobello.rhobusta.models.PEScfEngine import PEScfEngineForModels
import scipy.fftpack as slib
import sys,os
from scipy import loadtxt
from numba import jit #TODO: enable jit when numa distribution issues are sorted out
import pickle
from distinctipy import distinctipy
from scipy.linalg import block_diag

def KK(mesh, ImG):
    # see for the scipy package http://people.exeter.ac.uk/sh481/kramers-kronig-relations.html
    dd = max(abs(mesh))
    for i in range(1,len(mesh)):
        diff = mesh[i] - mesh[i-1]
        if diff < dd:
            dd = diff
    KK_DOMAIN_FACTOR = 100.0
    KK_DETAIL_FACTOR = 4.0
    # run KK on a much bigger domain with more detail
    mesh2 = np.arange(mesh[0]*KK_DOMAIN_FACTOR, mesh[-1]*KK_DOMAIN_FACTOR, dd/KK_DETAIL_FACTOR)
    ImG2 = np.interp(mesh2, mesh, ImG, 0.0, 0.0, None)
    # the actual transform
    ReG2 = slib.hilbert(ImG2)
    # return back to the original mesh
    return np.interp(mesh, mesh2, ReG2, 0.0, 0.0, None)
         
class MaxentRunner(MPIContainer):

    def __init__(self, opts):
        self.opts = opts
        MPIContainer.__init__(self,opts)


    def MPIWorker(self):
        self.InitializeMPI()
        if self.opts.test_maxent:
            ac = AnalyticalContTester(opts, mpii = self.GetMPIInterface(), rank = self.GetRank(), isMaster = self.IsMaster())
        else:
            ac = AnalyticalCont(opts, mpii = self.GetMPIInterface(), rank = self.GetRank(), isMaster = self.IsMaster())
        
        if self.opts.plot:
            if self.IsMaster():
                ac.PlotContinuedSig(True)
            self.Terminate()
            if self.GetMPIInterface() is None:
                quit()

        ac.MaxentOnAuxiliaryG()
    
        self.Barrier()

        ac.CollectAndCleanup()

        self.Barrier()
        self.Terminate()


class AnalyticalCont(DMFTOrchastrator):
    '''
    classdocs
    '''
    
    def construct_todo_list(self,mpii,rank):

        # diag and off-diag are different speeds so we decompose
        all_diag_problems_todo = np.arange(1,self.dim+1)
        all_offdiag_problems_todo = np.arange(self.dim+1,self.rep.max()+1)

        #We might be truncating a lot of the off-diags, so don't want to add them to the list
        if len(all_diag_problems_todo):
            info = self.opts.info
            self.opts.info = True

            self.problems_todo = all_offdiag_problems_todo
            all_offdiag_problems_todo = self.MaxentOnAuxiliaryG(suppress_print = True)

            self.opts.info = info

        def get_my_chunk(all_problems_todo):
            size = len(all_problems_todo)
            if mpii is None:
                my_chunk = [0,size]
            else:

                num_workers = mpii.COMM_WORLD.Get_size()
                chunk = np.ceil(size/num_workers)
                my_chunk = np.array([chunk*rank,chunk*(rank+1)], dtype=int)
                if my_chunk[1] > size: my_chunk[1] = size

            return list(all_problems_todo[my_chunk[0]:my_chunk[1]])
        
        self.problems_todo = get_my_chunk(all_diag_problems_todo) + get_my_chunk(all_offdiag_problems_todo) 

    def __init__(self, opts, mpii = None, rank = 0, isMaster = True):
        '''
        Constructor
        '''
        DMFTState.__init__(self)
        self.which_shell = opts.which_shell
        self.self_path = self.GetTopLevelMethodName()
        self.file_name = self.self_path.split(":")[0]

        if  Path( self.file_name).exists():
            self.load(self.self_path)
        else:
            raise Exception(f"Cannot find {self.file_name} file in current directory")
        self.opts = opts
        self.rank = rank
        self.mpii = mpii
        self.isMaster = isMaster
        self.have_broadcast_diagonal = False

        im = self.GetImpurityMeasurement()
        self.moments = im.Sigma.moments[0,...]
        self.sign = np.zeros_like(self.moments)
        for si, idx in self.GetIndices():
            iorb = idx[0]
            jorb = idx[1]
            if iorb != jorb:
                mu = 0.0#sign*0.5*(1+np.random.rand())
                for i,j in product(range(self.dim), repeat=2):
                    if self.rep[iorb,jorb] == self.rep[i,j]:
                        self.moments[i,j,si] += mu 
                        """Note that this MUST match definition in ComCTQMC Gaux in order for maxent to work
                            If changing how the sign is determined (or Gaux / off-diagonal strategy) one must
                            change how these things are computed in ComCTQMC to match maxent.py (or vise-versa)
                            ID of Gaux function in ComCTQMC: dd6ec036-64af-445f-b577-209586826604
                        """
                        self.sign[i,j,si] = -np.sign(im.Sigma.M[0,iorb,jorb,si].imag) 

        self.construct_todo_list(mpii,rank)



    # return pairs si, i indices for the orbitals in correct order for 
    # the spinless (num_si == 1) or spinful (num_si == 2) cases
    def GetIndices(self, diagonal = False):
        indices = DMFTOrchastrator.GetRepOrbitals(self.CorrectRep(), diagonal = diagonal)     
        if self.num_si == 1:
            return [(0, i) for i in indices]
        else:
            # this is the case of completely symmetric representation. Change when we generalize.
            return [(0, i) for i in indices] + [(1,i) for i in indices]

    def DiagonalizeCovarianceMatrix(self, coVar, n, eps):
        # isolate the top left matrix where diagonalization is relevant
        VAR_LOWER_BOUND = self.opts.variance_floor
        ii = n
        for i in range(coVar.shape[0]):
            if abs(coVar[i,i]) < eps and np.allclose(coVar[i:,i:], 0.0,  atol=eps):
                    ii = i+1
                    break
        #print(f"diagonalizing up to index {ii}")
            
        ww, U = np.linalg.eigh(coVar[:ii,:ii])
        U = np.conjugate(np.transpose(U))
        
        wwList = [ww[i] for i in range(ii)] + [coVar[i,i] for i in range(ii,n)]
        ww = np.array(wwList)
        
        for i in range(n):
            ww[i] = max(ww[i], VAR_LOWER_BOUND)
        
        return ww, block_diag(U, np.eye(n-ii))
        
    def GetImpurityMeasurement(self):
        im = ImpurityMeasurement()
        im.load(f"{self.self_path}/impurity/measurement/")

        if self.opts.versions != "":
            versions = self.opts.versions.split(",")
            for v in versions:
                maj_sub = v.split(".")
                maj = int(maj_sub[0])
                sub = int(maj_sub[1])
                im2add = DMFTState()
                im2add.load(f"./history.h5:/impurity/{maj}/{sub}/")

                for si in range(self.num_si):
                    for iom, om in enumerate(im.GAux.omega):
                        GAux = 1j*om*np.eye(self.dim) - im2add.sig.M[iom,:,:,si] + im2add.sig.moments[0,:,:,si]
                        GAux = np.linalg.inv(GAux)

                        for i,j in product(range(self.dim), repeat=2):
                            if i!=j:
                                GAux[i,j] = 0.5*(GAux[i,i] + GAux[j,j] + 2*GAux[i,j])
                        
                        im.GAux.M[iom,:,:,si] += GAux
                
                im.Sigma.moments[...] += im2add.sig.moments[...]
                im.Sigma.M[...] += im2add.sig.M[...]
                        
            im.GAux.M[...] /= len(versions)+1
            im.CoVariance[...] /= len(versions)+1
            im.Sigma.moments[...] /= len(versions)+1
            im.Sigma.M[...] /= len(versions)+1
        return im

    def SetupMaxentProblem(self, problem_num:int, i:int, j:int, si : int):
        random.seed(612314) # seed for random numbers

        im = self.GetImpurityMeasurement()
        
        num_x = 400
        if opts.kernel == "fermi-GAux-tau":
            num_x = opts.num_tau
        elif opts.kernel == "fermi-GAux-iw":
            num_x = im.num_x  
            if im.GAux.num_omega == 0: # no auxiliary data in CTQMC
                num_x = im.Sigma.num_omega
            
            if opts.num_om > num_x:
                num_x = opts.num_om

    
        inp = Input(num_x=num_x,
                    beta=self.beta,
                    nomega=self.opts.nomega)
        inp.allocate()

        if opts.kernel == "fermi-GAux-tau":            # auxiliary function in matusbara space
            inp.kernel_choice = inp.FERMIONIC_TAU_KERNEL
            Giw = 1/(self.sig.omega*1j  - (self.sig.M[:,i,j,si] - self.sig.moments[0,i,j,si]))
            # reverse fourier to Gtau
            inp.X[:] =  linspace(0,self.beta,self.opts.num_tau)[:]
              
            Gt = InverseFourier(Giw, self.sig.omega, inp.X, self.beta, self.opts.num_tail)      
            
            # Set error
            inp.invVar[:] = 1.0/self.opts.assumed_error**2
            Gt = -Gt
            normalization = Gt[0]+Gt[-1] 
            inp.signal[:] = Gt[:]
        
        else: 
            assert(opts.kernel == "fermi-GAux-iw") # we don't handle other cases
        
            inp.kernel_choice = inp.FERMIONIC_IOMEGA_KERNEL
            
            if im.GAux.num_omega > 0:
                inp.X[:] = im.GAux.omega[0:im.num_x]
                    
                self.U = None
                try:
                    covarDiag = np.diag(im.CoVariance[:,:,problem_num%im.num_orbs,si])

                except:
                    covarDiag = np.diag(im.CoVariance[:,:,problem_num%im.num_orbs])
                        
                if self.opts.debug:
                    import matplotlib.pyplot as plt
                    plt.plot(range(im.num_x), covarDiag)
                    plt.show()
            
                    plt.matshow(abs(im.CoVariance[:,:,problem_num%im.num_orbs,si]))
                    plt.show()
                
                # eps = 1.0 means that we are basically *not* diagonalizing, since all the matrix falls below that
                try:
                    ww, self.U = self.DiagonalizeCovarianceMatrix(im.CoVariance[:,:,problem_num%im.num_orbs,si], im.num_x, eps=1.0)
                except:
                    ww, self.U = self.DiagonalizeCovarianceMatrix(im.CoVariance[:,:,problem_num%im.num_orbs], im.num_x, eps=1.0)

                Gw = np.dot(self.U, im.GAux.M[:im.num_x,i,j,si])
                
            else:
                inp.X[:] = im.Sigma.omega[:num_x]
                Gw = 1/(im.Sigma.omega[:num_x]*1j -  im.Sigma.M[:num_x,i,j,si] + im.Sigma.moments[0,i,j,si])
                ww = np.array([self.opts.assumed_error**2 for _x in inp.X])
            
            if i!=j:
                absImGw0mat = np.abs(np.imag(np.linalg.inv(1j*im.Sigma.omega[0]*np.eye(self.dim) - im.Sigma.M[0,:,:,si] + im.Sigma.moments[0,:,:,si])))
                test = absImGw0mat[i,j] / (0.5*(absImGw0mat[i,i]+absImGw0mat[j,j]))
                if test < self.opts.truncate_offdiags:
                    Gw[:] = 0
            
            """
            if  (np.abs(Gw[:]) > 1e-8).all(): #until we get good data from ctqmc

                GwMat = np.zeros_like(im.Sigma.M)
                for iom in range(num_x):
                    GwMat[iom,:,:,si] = np.linalg.inv(1j*im.Sigma.omega[iom]*np.eye(self.dim) - im.Sigma.M[iom,:,:,si] + self.moments[:,:,si])
                    
                if i!=j:
                    Gw[:] = 0.5*(GwMat[:num_x,i,i,si] + GwMat[:num_x,j,j,si] + 2*self.sign[i,j,si]*GwMat[:num_x,i,j,si])
                else:
                    Gw[:] = (GwMat[:num_x,i,j,si])

                #Gw = 1/(1j*im.Sigma.omega[:num_x] - im.Sigma.M[:num_x,i,j,si] + im.Sigma.moments[0,i,j,si])
                #ww = np.array([self.opts.assumed_error**2 for _x in inp.X])
                """

            # the error squared is taken from CTQMC, we need it to be calculated with a large number of bins
            inp.invVar[:] = 1 / ww[:] #1.0/im.ErrAuxGreen[:,problem_num]
            #if self.opts.info:
            #    print("variances: ", ww)
                
            inp.signal[:] = Gw[:]
            normalization = 1.0


        if self.opts.tan_omega_mesh_min > 0.0 :
            omega = MakeTanMesh(self.opts.tan_omega_mesh_min,self.opts.energy_limit,self.opts.nomega) + self.opts.tan_omega_mesh_shift
        else:
            omega = linspace(-self.opts.energy_limit,self.opts.energy_limit,2*self.opts.nomega+1)
            
        inp.omega[:] = omega[:]
            
        d_omega = array([0.5*(omega[1]-omega[0])]+[0.5*(omega[i+1]-omega[i-1])\
                           for i in range(1,len(omega)-1)]+[0.5*(omega[-1]-omega[-2])])
        inp.d_omega[:] = d_omega[:] 
    
        
        #print('beta=', self.beta)
        #print('normalization=', normalization)

        # Set model
        if self.opts.info or i==j:
            if self.opts.flat_model:
                model = normalization*ones(len(omega))/sum(d_omega)
            else: # gaussian
                model = exp(-omega**2/self.opts.gaussian_width)
                model *= normalization/dot(model,d_omega)
        else:
            Ai = -1/np.pi*np.imag(self.res.M[:,i,i,si])
            Aj = -1/np.pi*np.imag(self.res.M[:,j,j,si])
            model = 0.5*(Ai+Aj)
            
        inp.model[:] = model[:]
        inp.Dp[:] = model[:]
        inp.Dm[:] = model[:]
              
        #print('Model normalization=', dot(model,d_omega))
        
        inp.store("./maxent.core.h5:/input/")
        inp.CompleteSetup("")
        inp.load("./maxent.core.h5:/input/")
        if self.U is not None:
            inp.kernel =  np.dot(inp.kernel, np.transpose(self.U))
            inp.store("./maxent.core.h5:/input/")
        
        
        ws = Workspace(num_x=num_x,
                       nomega=self.opts.nomega, 
                       normalization = normalization,
                       temp=10.0,
                       rfac=1.0,
                       alpha=opts.alpha0)
        ws.allocate()
        # Set starting Aw(omega)
        def random_a():
            Aw = random.rand(len(omega))
            return Aw * (normalization/dot(Aw,d_omega))
        #if False and i!=j:
        #    ws.Ap[:] = random_a()
        #    ws.Am[:] = random_a()
        #    ws.A = ws.Ap - ws.Am
        #else:
        ws.A[:] = random_a()
        #print('Aw normalization=', dot(ws.A,d_omega))
    
        return inp, ws
    
    def ScipyKK(self, inp, Aw):
        ImG = -Aw * np.pi
        ReG = KK(inp.omega, ImG)
        return inp.omega, ReG + ImG*1j
        
    def allocateResultForWorkersWithoutWork(self):
        if not self.opts.info and self.mpii is not None:
            # if some workers had no work then they haven't allocated self.res yet
            self.mpii.COMM_WORLD.Barrier()
            num_omega = self.mpii.COMM_WORLD.bcast(self.res.num_omega)
            self.mpii.COMM_WORLD.Barrier()
            if self.res.M is None:
                self.res.num_omega = num_omega
                self.res.allocate()
            self.mpii.COMM_WORLD.Barrier()

    def allreduceResult(self):
        if not self.opts.info and self.mpii is not None:

            if self.have_broadcast_diagonal and self.rank > 0:
                for i in range(self.dim):
                    self.res.M[:,i,i,:] = 0

            self.mpii.COMM_WORLD.Barrier()
            self.res.M = self.mpii.COMM_WORLD.allreduce(self.res.M, op=self.mpii.SUM)
            self.mpii.COMM_WORLD.Barrier()

            self.have_broadcast_diagonal = True

    def MaxentOnAuxiliaryG(self, suppress_print = False):
        rep = self.CorrectRep()
        indices = self.GetIndices()

        #res starts as Gaux and then we invert to get sigma
        self.res = LocalOmegaFunction(is_real=True, 
                                 num_orbs=self.dim, 
                                 num_si=self.num_si)

        if self.opts.debug:
            self.debug = self.MakeImagAxisFunc()
            self.debug.allocate()
        
                                 
        def SetupAndRunMaxent(i,j,si): #returns problem_num if it continues / -1 if truncates
            problem_num = self.CorrectRep()[i,j] - 1
            if problem_num+1 not in self.problems_todo:
                return -1
            
            inp, ws = self.SetupMaxentProblem(problem_num, i, j, si)

            if self.opts.debug:
                self.debug.X = inp.X
                self.debug.M[:inp.num_x,i,j,si] = inp.signal

            if np.abs(inp.signal).any() > 1e-8:
                if not suppress_print:
                    print(f"rank {self.rank} continuing {problem_num+1} at indices {i}-{j}")
                if self.opts.info:
                    return self.rep[i,j]

                Aw = MaximumEntropy(self.opts, inp, ws, i==j) 

                om, Gaux = self.ScipyKK(inp, Aw)
                
                if self.res.num_omega == 0:
                    self.res.num_omega = len(om)
                    self.res.allocate()
                    self.res.omega[:] = om[:]

                for iorb,jorb in product(range(rep.shape[0]), repeat=2):
                    if rep[iorb,jorb] == rep[i,j]:
                        self.res.M[:, iorb, jorb, si] = Gaux[:] 

                        # this will truncate moments for self energies we are not continuing -- a good thing
                        self.res.moments[:,iorb,jorb,si] = self.moments[i,j,si] 

            else:
                return -1
            
            return self.rep[i,j]

        for si,idx in indices:
            i,j = idx[0],idx[1]
            if i != j:
                continue
            SetupAndRunMaxent(i,j,si)

        self.allocateResultForWorkersWithoutWork()
        self.allreduceResult()

        done = []
        for si,idx in indices:
            
            i,j = idx[0],idx[1]
            if i == j:
                continue
            problem = SetupAndRunMaxent(i,j,si)
            if problem != -1:
                done.append(problem)

        return done

       
    def CollectAndCleanup(self):
        if self.opts.info:
            return

        if self.mpii is not None:
            self.allocateResultForWorkersWithoutWork()
            self.allreduceResult()

            self.mpii.COMM_WORLD.Barrier()
            self.res.moments = self.mpii.COMM_WORLD.reduce(self.res.moments, op=self.mpii.SUM)
            self.mpii.COMM_WORLD.Barrier()

            if self.opts.debug:
                self.debug.M = self.mpii.COMM_WORLD.reduce(self.debug.M, op=self.mpii.SUM)

        if self.isMaster:
            # Test iw->w->iw
            if self.opts.debug:
                self.res.M[...] = -1/np.pi*np.imag(self.res.M)
                s_, backcalculated_signal = self.ContinueFromRealToImaginaryAxis(self.res)

                import matplotlib.pyplot as plt
                from matplotlib.pyplot import cm
            
                N = len(self.GetIndices())
                #Nd = len(self.GetIndices(diagonal = True))
                fig, axis = plt.subplots(1, 2)

                i=0
                colors = distinctipy.get_colors(N) # cm.rainbow(np.linspace(0, 1, N))
                for si, idx in self.GetIndices():

                    iorb = idx[0]
                    jorb = idx[1]
                    if True or iorb != jorb:

                        label = f"{jorb}-{iorb}" if iorb != jorb else iorb
                        num_x = len(self.debug.X)
                        axis[0].plot(self.debug.X,  np.imag(self.debug.M[:num_x, iorb, jorb, si]), color = colors[i], linestyle="-", label = label)
                        axis[0].plot(backcalculated_signal.omega[:],  np.imag(backcalculated_signal.M[:, iorb, jorb, si]), color = colors[i], linestyle=":", label = label)
                        axis[1].plot(self.res.M[:,iorb, jorb, si], color = colors[i])
                        i+=1

                fig.legend()
                plt.show()
            
            #Go from effective density to offdiagonal G's
            for i,j in product(range(self.dim), repeat=2):
                if i!=j:
                    if np.abs(self.res.M[:,i,j,:]).any() > 1e-8:
                        self.res.M[:,i,j,:] = -0.5*self.sign[i,j,0]*(self.res.M[:,i,i,:] + self.res.M[:,j,j,:] - 2*self.res.M[:,i,j,:])
                    #else:
                        #restore off-diaonal self energies that were not continued but have nonzero moments
                    #    if np.abs(self.sig.moments[0,i,j,:]).any() > 1.0e-8:
                    #        self.res.moments[:,i,j,:] = self.sig.moments[:,i,j,:]

            self.res.store(f"./gaux{self.GetSuffixOfImpurity()}.h5:/", flush=True)
            gaux = np.copy(self.res.M)

            #Go from G's to Sigma's
            for si in range(self.num_si):
                for iom, om in enumerate(self.res.omega):
                    self.res.M[iom,:,:,si] = om*np.eye(self.dim) - np.linalg.inv(gaux[iom,:,:,si])        

            #Go from G's to Sigma's
            #for si in range(self.num_si):
            #    for iom, om in enumerate(self.res.omega):
            #        self.res.M[iom,:,:,si] = om*np.eye(self.dim) - np.linalg.inv(gaux[iom,:,:,si]+1j*b)     
            

            self.res.store(f"./real-imp-self-energy{self.GetSuffixOfImpurity()}.h5:/", flush=True)

        
    def PlotContinuedSig(self, plot_labels):

        import matplotlib.pyplot as plt
        from matplotlib.pyplot import cm
                
        res = LocalOmegaFunction()
        res.load(f"./real-imp-self-energy{self.GetSuffixOfImpurity()}.h5:/")

        gaux = LocalOmegaFunction()
        gaux.load(f"./gaux{self.GetSuffixOfImpurity()}.h5:/")
         
        N = len(self.GetIndices())
        #Nd = len(self.GetIndices(diagonal = True))
        fig, axis = plt.subplots(2, 2, sharex='col', sharey='row')
        #fig2, ax_intuition = plt.subplots(Nd, 1, sharex='col', sharey='row')
        #if N==1:
        #    ax_intuition_handle = [ax_intuition]
        #else:
        #    ax_intuition_handle = ax_intuition

        sig_axr = axis[0,0]
        sig_axi = axis[1,0] 
        sig_axr.set_title("Analytically continued $\Sigma_{imp}$")
        g_axr = axis[0,1]
        g_axi = axis[1,1] 
        g_axr.set_title("Analytically continued $G_{aux}$")
        #ax_intuition_handle[0].set_title("A map of $\epsilon_k$ to $\omega$")
             
        i=0
        colors = distinctipy.get_colors(N)
        for si, idx in self.GetIndices():

            iorb = idx[0]
            jorb = idx[1]

            dash = "-" if iorb == jorb else ":"
            label = f"{jorb}-{iorb}" if iorb != jorb else iorb

            sig_axr.plot(res.omega[:],  np.real(res.M[:, iorb, jorb, si]), color = colors[i], linestyle=dash, label = label)
            sig_axi.plot(res.omega[:],  np.imag(res.M[:, iorb, jorb, si]), color = colors[i], linestyle=dash)
            g_axr.plot(res.omega[:],  np.real(gaux.M[:, iorb, jorb, si]), color = colors[i], linestyle=dash, label = label)
            g_axi.plot(res.omega[:],  np.imag(gaux.M[:, iorb, jorb, si]), color = colors[i], linestyle=dash)
            i+=1

        i=0
        colors = distinctipy.get_colors(N)
        for si, idx in self.GetIndices(diagonal = False):

            iorb = idx[0]
            jorb = idx[1]
            
            #ax_intuition.plot(res.omega[:], np.zeros_like(res.omega), linestyle = ":", color = 'gray')    
            #ax_intuition_handle[i].axvline(0, linestyle = ":", color = 'gray')    
            #ax_intuition_handle[i].plot(res.omega[:], res.omega[:], linestyle = "--", color = 'black')

            adj_omega = res.omega[:] + np.real(self.DC[iorb, iorb, si]) - self.sig.moments[0,iorb,iorb,si]
            
            eps = 0.1/np.pi
            imsig = np.imag(res.M[:, iorb, jorb, si])
            resig = np.real(res.M[:, iorb, jorb, si])

            adj_imsig = - 1/eps * imsig - imsig**2

            residual = adj_omega[:] - resig[:] + np.sqrt(adj_imsig[:])
            residual2 = adj_omega[:] - resig[:] - np.sqrt(adj_imsig[:])
            
            masked_residual = ma.masked_where(adj_imsig < 0, residual)
            masked_residual2 = ma.masked_where(adj_imsig < 0, residual2)
            masked_omega = ma.masked_where(adj_imsig < 0, res.omega)

            #ax_intuition_handle[i].fill_between(masked_omega, masked_residual, masked_residual2,  color = colors[i], alpha=0.3)    
            #ax_intuition_handle[i].set_ylim(self.proj_energy_window.low, self.proj_energy_window.high)

            i+=1


        for ax in [sig_axi, sig_axr, g_axi, g_axr]:
            ax.axhline(0, color='black', linestyle="--", linewidth=0.25)
        sig_axr.set(xlabel='$\omega$', ylabel="real")
        sig_axi.set(xlabel='$\omega$', ylabel="imag")
        #ax_intuition_handle[-1].set_xlabel('$\omega$')
        
        if plot_labels:
            
            proj_location = "./projector.h5:/def/"
            proj = SelectedOrbitals()
            proj.load(proj_location)
             
            
            self.corrSubshell=proj.shells[opts.which_shell]
            self.orbBandProj = Namespace
            self.orbBandProj.orbsDef=proj
            if opts.model:
                vals, orbs = PEScfEngineForModels.GetLabelsForPlotting(self)
            else:
                vals, orbs = PEScfEngine.GetLabelsForPlotting(self)
          
            orb_names = [f"${orb}$" for orb in orbs]

            h, l = sig_axr.get_legend_handles_labels()
            num_label = len(h)
            num_per_legend = min(7,num_label)
            num_legends = int(num_label/num_per_legend+0.5)
            ord = [[sig_axr, 'upper left'], [g_axr, 'upper right'], [sig_axi, 'upper left'], [g_axi, 'upper right']]
            for i in range(num_legends):
                if i<len(ord)-1:
                    sl = slice(i*num_per_legend, (i+1)*num_per_legend)
                else:
                    sl = slice(i*num_per_legend, num_label)
                ord[i][0].legend(h[sl], l[sl], loc=ord[i][1])
                if i == len(ord)-1:
                    break

            #for i, orb in enumerate(orb_names):
            #    ax_intuition_handle[i].set_ylabel(orb)

        sig_axr.label_outer()
        sig_axi.label_outer()
        pickle.dump(fig,open(f'acont-impurity{self.GetSuffixOfImpurity()}-{self.super_iter}.{self.sub_iter}.pickle','wb'))
        #pickle.dump(fig2,open(f'existance{self.GetSuffixOfImpurity()}-{self.super_iter}.{self.sub_iter}.pickle','wb'))
        plt.show(block=True)
   
    def MakeImagAxisFunc(self):
        f = LocalOmegaFunction(num_omega = self.sig.num_omega, 
                                        num_orbs=self.dim,
                                        num_si=self.num_si)
        f.allocate()
        f.omega = np.array([(2*i+1)*np.pi/self.beta for i in range(0,self.sig.num_omega)])
        return f

    def ContinueFromRealToImaginaryAxis(self, spectral_function : LocalOmegaFunction, stddev = 0):

        imaginary_axis_selfenergy = self.MakeImagAxisFunc()
        imaginary_axis_g = self.MakeImagAxisFunc()

        iomega = 1j*imaginary_axis_selfenergy.omega
        omega = spectral_function.omega

        si = 0
        for i,j in product(range(self.dim), repeat=2):
            for iom in range(imaginary_axis_selfenergy.num_omega):
                Aw = spectral_function.M[:,i,j,si]

                Kw = 1./(iomega[iom] - omega[:])
                imaginary_axis_g.M[iom,i,j,si] = integrate.simps(Aw*Kw, x = omega)
                
                imaginary_axis_selfenergy.M[iom,i,j,si] = iomega[iom] - 1./imaginary_axis_g.M[iom,i,j,si] + 1j*(np.random.normal(scale=stddev))

        return imaginary_axis_selfenergy, imaginary_axis_g

class AnalyticalContTester(AnalyticalCont):
    """ 
    Going to abuse AnalyticalCont a bit to test MaxEnt
    This will have its own dim, rep, beta, etc. despite being initialized from a dmft state
    This is so we can run tailored tests but still make use of AnalyticalCont which does the real work
    Without having to properly abstract AnalyticalCont
    """

    def MakeImagAxisFunc(self):
        f = LocalOmegaFunction(num_omega = self.num_iomega, 
                                        num_orbs=self.dim,
                                        num_si=self.num_si)
        f.allocate()
        f.omega = np.array([(2*i+1)*np.pi/self.beta for i in range(0,self.num_iomega)])
        return f

    def MakeRealAxisFunc(self):
        f = LocalOmegaFunction(num_omega = self.num_omega, 
                                        num_orbs=self.dim,
                                        num_si=self.num_si)
        f.allocate()
        f.omega = self.omega
        return f

    def __init__(self, opts, mpii = None, rank = 0, isMaster = True):
        AnalyticalCont.__init__(self, opts, mpii, rank, isMaster)

        self.dim = 2
        self.rep = np.array([[1,3],
                            [3,2]])

        self.construct_todo_list(mpii,rank)

        self.beta=1
        self.num_iomega = 100
        self.num_omega = 2*self.opts.nomega+1
        if self.opts.tan_omega_mesh_min > 1e-8:
            self.omega = MakeTanMesh(self.opts.tan_omega_mesh_min,self.opts.energy_limit,self.opts.nomega)
        else:
            self.omega = linspace(-self.opts.energy_limit,self.opts.energy_limit,2*self.opts.nomega+1)
        self.d_omega = array([0.5*(self.omega[1]-self.omega[0])]+[0.5*(self.omega[i+1]-self.omega[i-1])\
                           for i in range(1,len(self.omega)-1)]+[0.5*(self.omega[-1]-self.omega[-2])])

        self.sig.moments[...] = 0
        
        self.Spectral = self.MakeRealAxisFunc()
        self.SigmaReal = self.MakeRealAxisFunc()

        #mott insulator
        left = -self.opts.gaussian_width/2
        right = self.opts.gaussian_width/2
        self.Spectral.M[:,0,0,0] = (exp(-(self.omega-left)**2/self.opts.gaussian_width) + exp(-(self.omega-right)**2/self.opts.gaussian_width))

        #Not a mott insulator
        w = self.opts.gaussian_width/10
        s = np.sqrt((w)**2 - self.omega[abs(self.omega)<w]**2)
        self.Spectral.M[abs(self.omega)<w,1,1,0] = s.real

        #normalize everything
        for i in range(self.dim):
            scale = integrate.simps(self.Spectral.M[:,i,i,0], x=self.omega)
            self.Spectral.M[:,i,i,0] /= scale

        #rotate the diagonal rep into the off diagonal rep -- needs some bug fixing
        """    
        theta = 45./360.*(2*np.pi)
        rot = Matrix([[np.cos(theta),-np.sin(theta)],[np.sin(theta),np.cos(theta)]])
        for iom in range(self.num_omega):
            self.Spectral.M[iom,:,:,0] = rot.H*self.Spectral.M[iom,:,:,0]*rot
        """
        
        #Random off-diag 
        left = -self.opts.energy_limit/2
        right = self.opts.energy_limit/2
        self.Spectral.M[:,0,1,0] = self.sig_to_spectral(-1j*(2/(5*self.omega-left))+1j*(2/(5*self.omega-right)))
        self.Spectral.M[:,1,0,0] = self.Spectral.M[:,0,1,0]

        for i,j in product(range(self.dim), repeat=2):
            scale = integrate.simps(self.Spectral.M[:,i,j,0], x=self.omega)
            self.Spectral.M[:,i,j,0] /= scale

        self.Sigma, self.GAux = self.ContinueFromRealToImaginaryAxis(self.Spectral, stddev = self.opts.assumed_error)

        if opts.debug:
            import matplotlib.pyplot as plt
            from matplotlib.pyplot import cm
            
            fig,ax = plt.subplots(3,1)
            N = len(self.GetIndices())
            colors = distinctipy.get_colors(N)
            for i,j in product(range(self.dim), repeat=2):
                if i>=j:
                    dash = "-" if i == j else ":"
                    ax[0].plot(self.Sigma.omega, self.Sigma.M[:,i,j,0].real, color = colors[i], linestyle=dash, label=f"{i}-{j}")
                    ax[1].plot(self.Sigma.omega, self.Sigma.M[:,i,j,0].imag, color = colors[i], linestyle=dash)
                    ax[2].plot(self.omega, self.Spectral.M[:,i,j,0].real, color = colors[i], linestyle=dash)

            fig.legend()
            plt.show()
            #fig.clear()
        
    
    def sig_to_spectral(self, sigma):
        return -1./np.pi * np.imag(1./(self.omega[:] - sigma))
    
    def MyKK(self,Aw):
        ImG = -Aw * np.pi
        ReG = KK(self.omega, ImG)
        return self.omega - 1/(ReG +ImG*1j)

    def GetIndices(self):
        indices = DMFTOrchastrator.GetRepOrbitals(self.CorrectRep(), diagonal = False)     
        if self.num_si == 1:
            return [(0, i) for i in indices]
        else:
            # this is the case of completely symmetric representation. Change when we generalize.
            return [(0, i) for i in indices] + [(1,i) for i in indices]

    def GetImpurityMeasurement(self):

        im = ImpurityMeasurement()
        im.load(f"{self.self_path}/impurity/measurement/")

        im.Sigma = self.Sigma
        im.GAux = LocalOmegaFunction(num_omega = 0,
                                        num_orbs=self.dim,
                                        num_si=self.num_si) 

        return im

    
    def PlotContinuedSig(self, a_):

        import matplotlib.pyplot as plt
        from matplotlib.pyplot import cm
                
        res = LocalOmegaFunction()
        res.load(f"./real-imp-self-energy{self.GetSuffixOfImpurity()}.h5:/")

        N = len(self.GetIndices())
        colors = distinctipy.get_colors(N)

        fig,ax = plt.subplots(3,1)
        si = 0
        for i,j in product(range(self.dim), repeat=2):
            if i>=j:
                color = colors[self.rep[i,j]-1]
                dash = "-" if i == j else ":"
                spectral = -1./np.pi * np.imag(1./(res.omega[:-2] - res.M[:-2,i,j,si] - 0.0001*1j))
                sig = self.MyKK(self.Spectral.M[:,i,j,si])
                
                ax[0].set_ylabel(r"$Re[\Sigma]$")
                ax[0].plot(res.omega, res.M[:,i,j,si].real, color = color, linestyle=dash, label = f"{i}-{j}")
                ax[1].set_ylabel(r"$Im[\Sigma]$")
                ax[1].plot(res.omega, res.M[:,i,j,si].imag, color = color, linestyle=dash)

                ax[0].plot(self.Spectral.omega, sig.real, color = color, linestyle="-.")
                ax[1].plot(self.Spectral.omega, sig.imag, color = color, linestyle="-.")

                ax[2].set_ylabel(r"$A(\omega)$")
                ax[2].plot(res.omega[:-2],spectral, color = color, linestyle=dash)
                ax[2].plot(self.omega,self.Spectral.M[:,i,j,si], color = color, linestyle="-.")
                ax[2].set_xlabel(r"$\omega$")

        fig.legend()
        plt.show()
        

def Broaden(width, om, fw):
    " Broadenens the data with gaussian of width=width"
    def MakeTanMesh(N, tanc, tanw, b0, b1):
        if not(b0<b1): print("Relation must hold: b0<b1!")
        if not(b0<tanw and tanw<b1): print("Relation mesu hold: b0<tanw<b1!")
        if not(b0>0): print("b0 must be positive!")
        du = arctan(((tanc-b0)/tanw))
        b1n = arctan((b1-tanc)/tanw)+du
        m0 = [tanc + tanw * tan(b1n*(i-1)/(N-2)-du) for i in range(1,N)]
        return hstack( (-array(m0[::-1]), array([0]+m0) ) )

    if width<1e-5: return fw
    
    w=width
    x = MakeTanMesh(200,0.0,w,w/50,w*20)
    fwi = interpolate.interp1d(om, fw)
    fwn=[]
    for im in range(len(om)):
        x1 = list(filter(lambda t: t>=om[im]-x[-1] and t<=om[im]-x[0], om))
        x2 = list(filter(lambda t: t>=om[0] and t<=om[-1], x+om[im]))
        eps = sorted(hstack((x1, x2)))
        x3 = om[im]-eps
        gs = exp(-x3**2/(2*w**2))/(sqrt(2*pi)*w)
        yn = integrate.trapz(fwi(eps) * gs, x=eps)
        fwn.append(yn)
    return array(fwn)


def MakeTanMesh(x0,L,Nw):
    def fun(x,x0,L,Nw):
        "x[0]=d, x[1]=w"
        d=x[0]
        w=x[1]
        #print 'd=', d, 'w=', w
        return array([L-w/tan(d), x0-w*tan(pi/(2*Nw)-d/Nw) ])
    
    xi=x0/L
    d0 = Nw/2.*(tan(pi/(2*Nw))-sqrt(tan(pi/(2*Nw))**2 - 4*xi/Nw))
    w0 = L*d0

    sol=optimize.root(fun, [d0,w0], args=(x0,L,Nw) )
    (d,w) = sol.x
    om = w*tan(linspace(0,1,2*Nw+1)*(pi-2*d) -pi/2+d)
    return om

def InverseFourier(Gm, om, tau, beta, Nf=40, stat='fermi'):
    """Inverse Fourier from Gm to Gtau
    """
        
    #@jit(nopython=True)
    def TransformF(t:float, Gm, om, ah:float, beta:float):
        dsum=0.
        for im in range(Gm.shape[0]):
            dsum += np.cos(om[im]*t)*real(Gm[im]) + np.sin(om[im]*t) * (imag(Gm[im])+ah/om[im])
        return 2*dsum/beta-0.5*ah
    
    #@jit(nopython=True)
    def TransformB(t:float, Gm, om, beta:float):
        dsum=0.
        for im in range(1,Gm.shape[0]):
            dsum += np.cos(om[im]*t)*real(Gm[im]) 
        dsum = dsum + 0.5*real(Gm[1])
        return 2*dsum/beta

    def FindHighFrequency(Gm,om,Nf):
        S=0.; Sx=0.; Sy=0.; Sxx=0.; Sxy=0;
        for j in range(len(om)-Nf,len(om)):
            x = om[j]
            y = Gm[j].imag * x
            x2= x**2
            Sy += y
            Sx += 1/x2
            Sxx += 1/x2**2
            Sxy += y/x2
            S += 1
    
        dd = S*Sxx-Sx**2
        a = (Sxx*Sy-Sx*Sxy)/dd
        ah = -a;
        if abs(ah-1.0)<1e-3: ah=1.0
        return ah
    
    Gtau = zeros(len(tau),dtype=float)
    # Correction 1/omega^2 (sometimes usefull)
    df = Gm[-1].real*om[-1]/pi
    print('df=', df)
    if stat=='fermi':
        ah = FindHighFrequency(Gm,om,Nf)
        for it,t in enumerate(tau):
            Gtau[it] = TransformF(t,Gm,om,ah,beta)
        Gtau[0] += df
        Gtau[-1] -= df
    else:
        #ah=0
        #om[0]=1e-12
        for it,t in enumerate(tau):
            Gtau[it] = TransformB(t,Gm,om,beta)
        Gtau[0] += df
        Gtau[-1] += df
        
    return Gtau

def MaximumEntropy(opts, inp: Input, ws : Workspace, diag : bool):
    fermionic_sign = -1 if inp.kernel_choice in [inp.FERMIONIC_TAU_KERNEL, inp.FERMIONIC_IOMEGA_KERNEL] else 1

    for itt in range(1,opts.num_iters+1):
        print(itt, 'Restarting maxent with rfac=', ws.rfac, 'alpha=', ws.alpha)
        
        ws.store("./maxent.core.h5:/work/", flush=True)
        if True or diag:
            ws.Maxent("", opts.num_annealing)
        else:
            ws.MaxentOffDiag("", opts.num_annealing)
        ws.load("./maxent.core.h5:/work/")
   
        ratio = abs(-2*ws.entropy*ws.alpha/ws.tr)
        #if offdiag:
        #    ratio *= -1
        print('Finished maxent with alpha=',ws.alpha,'-2*alpha*S=',-2*ws.alpha*ws.entropy,'tr=',ws.tr)
        print('   ratio=', ratio)

        if opts.debug:
            np.savetxt('dos_'+str(itt), vstack((inp.omega,ws.A)).transpose())
        ws.temp=0.001
        ws.rfac=0.05
    
        if abs(ratio-1)<opts.min_ratio: break
    
        if (abs(ratio)<0.05):
            ws.alpha *= 1.5
        else:
            ws.alpha *= (1.+0.001*(random.rand()-0.5))/ratio
        
    for itt in range(opts.num_smooth_iters):
        print('Smoothing iter ', itt)
        Aw = Broaden(opts.smoothing_width,inp.omega, ws.A)
        Aw *= (inp.normalization/dot(Aw,inp.d_omega)) # Normalizing Aw
        
        if opts.debug:
            np.savetxt('dos_'+str(opts.num_smooth_iters), vstack((inp.omega,ws.A)).transpose())
        
        ws.temp=0.005
        ws.rfac=0.005
        ws.A[:] = Aw[:]
        
        ws.store("./maxent.core.h5:/work/", flush=True)
        ws.Maxent("", opts.num_annealing)
        ws.load("./maxent.core.h5:/work/")

        ratio = -2*ws.entropy*ws.alpha/ws.tr
        print('Finished smoothing run with alpha=',ws.alpha,'-2*alpha*S=',-2*ws.alpha*ws.entropy,'tr=',ws.tr)
        print('   ratio=', ratio)
        
    if opts.final_smooth:
        if opts.debug:
            np.savetxt('gtn', vstack((inp.tau, fermionic_sign*dot(ws.A,inp.kernel))).transpose())
        Aw = Broaden(opts.smoothing_width,inp.omega,ws.A)
        ws.A[:] = Aw[:]
        if opts.debug:
            np.savetxt('dos.out', vstack((inp.omega,Aw)).transpose())

    return ws.A

  
def TestKK():
    
    # this compares with KH results, which are in Gc.
    # run the analytical continuation with KH first
    import matplotlib.pyplot as plt

    Gdata = loadtxt('Gc').transpose()
    om = Gdata[0]
    ReG =  Gdata[1]
    ImG = Gdata[2]
    
    
    ReG2 = KK(om, ImG)
    ImG2 = -KK(om, ReG2)
    
    plt.plot(om, ReG)
    plt.plot(om, ReG2)
        
    plt.show()
    
    #plt.plot(om, ReG2)
    #plt.plot(om, ImG)
    plt.plot(om, ImG)
    plt.plot(om, -ImG2)
    
    plt.show()


if __name__ == '__main__':
    
    parser = ArgumentParserThatStoresArgv('maxent', add_help=True ,formatter_class=ArgumentDefaultsHelpFormatter, parents=[GetMPIProxyOptionsParser()])
    has_file = Path("./ini.h5").exists()

    parser.add_argument("--truncate-offdiags", dest="truncate_offdiags", 
                    type=float,
                    default=1e-8,
                    help="Get rid of offdiagonal self energies which have no imaginary component greater than this")

    """
    commit - 7af91459 has the appropariate implementation for A = A^+ - A^- continuations
    including the "poor man's" matrix continutation of off-diagonals PRB 96 155128 (2017)
    To turn back on, one also needs to adjust the G_aux in CTQMC to G_aux = 1/(i\omega - Sigma + Sigma_infinity)

    parser.add_argument("--poor-mans-offdiags", dest="poor_mans_offdiags", 
                    action="store_true",
                    default=False,
                    help="Get some measure of the full-matrix influence on the off-diagonal continuations")
    """

    parser.add_argument("-v", "--versions", dest="versions", default='',
                      type=None, help="combine data from multiple versions in the history. Comma separated list as in dashboard")

    parser.add_argument("--info", dest="info", 
                    action="store_true",
                    default=False,
                    help="Just output some info about the functions to be continued")

    parser.add_argument("-i", "--iterations", dest="num_iters", 
                        type=int,
                        default=100,
                        help="number outer (alpha) loop iterations")
    
    parser.add_argument("-A", "--annealing-iterations", dest="num_annealing", 
                        type=int,
                        default=2000,
                        help="number of annealing iterations")
    
    parser.add_argument("-k", "--kernel",
                      action="store",
                      dest="kernel",
                      choices=["fermi-GAux-tau", "fermi-GAux-iw", "bose-tau", "bose-iw"],
                      default="fermi-GAux-iw",
                      help="""which kernel to run. This also defines the signal source accordingly,
                              Choose fermi-GAux-tau with appropriate -E -N -t parameters to run it the 'old' way,
                              which runs also a Fourier Transform to tau space. """)
    
    parser.add_argument("--alpha0", dest="alpha0", 
                        type=float,
                        default=1000.0,
                        help="initial alpha. The algorithm searches for the optimal value of alpha.")

    parser.add_argument("--min-ratio", dest="min_ratio", 
                        type=float,
                        default=0.001,
                        help="stopping condition")
    
    parser.add_argument("--variance-floor", dest="variance_floor", 
                        type=float,
                        default=1.0e-6,
                        help="""relevant only for fermi-GAux-iw, where variances are read from CTQMC.
                               This is a lower bound on variance - if lower, it will be set to this value (vanishing values were found to cause 
                               overfitting of the data)""")
    
    pg = parser.add_argument_group("Output mesh (real omega) options")

    pg.add_argument("-e", "--energy-limit", dest="energy_limit", 
                        default=20.0, type=float,
                        help="energy limit for self-energy frequency (on positive side)")
    
    pg.add_argument("-O", "--nomega", dest="nomega", 
                        type=int,
                        default=400,
                        help="number of points in the frequency (omega) mesh")
    
    pg.add_argument("-d", "--tan-omega-mesh-min", dest="tan_omega_mesh_min", 
                        type=float,
                        default=0.005,
                        help="the min interval (eV) in a tan()-like mesh around 0 (if not positive - it is a linear mesh)")

    pg.add_argument("--tan-omega-mesh-shift", dest="tan_omega_mesh_shift",
                        type=float,
                        default=0.0,
                        help="shift the omega mesh by this amount")

    pg = parser.add_argument_group("Model")
    
    pg.add_argument("--flat-model", dest="flat_model",
                      action="store_true",
                      default=False,
                      help="True if model is flat and not normal") 
    
    pg.add_argument("--gaussian-width", dest="gaussian_width", 
                        type=float,
                        default=10,
                        help="width of the gaussian model")
    
    pg = parser.add_argument_group("Smooth solutions (optional)")
    
    pg.add_argument("-s", "--smoothing-iterations", dest="num_smooth_iters", 
                    type=int,
                    default=0,
                    help="number of smoothing iterations") 
                        
    pg.add_argument("--smoothing-width", dest="smoothing_width", 
                    type=float,
                    default=0.004,
                    help="width of lorenzians for smoothing")
    
    pg.add_argument("--final-smooth-A", dest="final_smooth", 
                    action="store_true",
                    default=False,
                    help="smooth A before saving it and running KK, run just once (unlike the smoothing iterations)")
    
    pg = parser.add_argument_group("Tau space options (when kernel is fermi-GAux-tau)")
    
    pg.add_argument("--tau-num", dest="num_tau", 
                    type=int,
                    default=301,
                    help="number of points in the tau mesh, applies only for kernel=fermi-GAux-tau")

    pg.add_argument("-N", "--omega-num", dest="num_om", 
                    type=int,
                    default=0,
                    help="number of points in the omega mesh, applies only for kernel=fermi-GAux-iw")
       
    pg.add_argument("-E", "--error", dest="assumed_error", 
                    type=float,
                    default=0.001,
                    help="the CTQMC measurement error (stderr), applies only for kernel=fermi-GAux-tau.")

    pg.add_argument("-t", "--tail-num", dest="num_tail", 
                    type=int,
                    default=4,
                    help="number of tail points to use for the limit, in inverse fourier transform, applies only for kernel=fermi-GAux-tau.")
    
    pg = parser.add_argument_group("debug options")
    
    pg.add_argument("--debug", dest="debug", 
                    action="store_true",
                    default=False,
                    help="write a few text files for debugging")
    
    pg.add_argument("--test-kk", dest="test_kk", 
                    action="store_true",
                    default=False)

    pg.add_argument("--which-shell", dest="which_shell", default=0, type = int,
                    help="which shell (index) to project to, in order to build the correlated problem")

    pg.add_argument("--plot", dest="plot", default=False, action="store_true",
                    help="Do not continue, just plot the result")

    pg.add_argument("--test-maxent", dest="test_maxent", default=False, action="store_true",
                    help="Instead of using real result, continue some known functions and see how maxent does")

    parser.add_argument("--model", dest="model", default=False, action="store_true",
                    help="model, not material")



    (opts, args) = parser.parse_known_args(sys.argv[1:])
    if opts.model:
        opts.which_shell=-1

    if opts.test_kk:
        TestKK()
        exit(0)

    mr = MaxentRunner(opts)
    mr.Run(wait=True)
    

    
    
