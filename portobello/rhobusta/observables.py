#!/usr/bin/env python3

'''
Created on May 5, 2020

@author: adler
'''
import itertools
import numbers
import sys
from argparse import ArgumentDefaultsHelpFormatter, Namespace
from builtins import enumerate
from cmath import sqrt
from fractions import Fraction
from itertools import product
from pathlib import Path

import numpy as np
from portobello.bus.Matrix import Matrix
from portobello.bus.mpi import MPIContainer
from portobello.generated.FlapwMBPT_basis import MuffinTinBasis
from portobello.generated.atomic_orbitals import AtomicShell, SelectedOrbitals
from portobello.generated.dmft import ImpurityMeasurement, ImpurityProblem
from portobello.generated.PEScf import SolverState
from portobello.generated.gutz import GState
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.simple_ed.basis import countBits
from scipy.linalg import kron, block_diag
from numpy.linalg.linalg import norm
from tabulate import tabulate
import json

from portobello.symmetry.spins import SPIN_PARITY_REP

def tr(M):
    return np.trace(M)[0,0]

def comm(M1,M2):
    return np.max(np.abs(M1*M2-M2*M1))

# This is a representation of local operators in the product basis in complex S.H.
# product basis convention is like Patrick's, but this is the (*complex*) spherical harmonic basis
# which are eigenvectors of the Lz (and Sz, Jz, Mz) operators
# hbar = 1 everywhere
class ProductBasisSHObs(object):
    
    def __init__(self, l):
        from portobello.symmetry.local import GetCGMatrix, GetRealSHMatrix

        # spin - pauli matrices, where spin down is first (Patrick)
        # so they are permuted a bit
        
        self.I = Matrix(np.eye(2*l+1))
        self.I2 = Matrix(np.eye(2))
        self.II = Matrix(kron(self.I2,self.I))
        #sqHlf = sqrt(0.5)
        #sq2 = sqrt(2.0)
        
        Pauli_x = Matrix([[0,1],[1,0]])
        Pauli_y = Matrix([[0,-1j],[1j,0]])
        Pauli_z = Matrix([[1,0],[0,-1]])
        
        Pat_x = Matrix([[0,1],[1,0]])
        Pat_y = Pauli_x * Pauli_y * Pauli_x
        Pat_z = Pauli_x * Pauli_z * Pauli_x
             
        sig_x = kron(Pat_x, self.I)
        sig_y = kron(Pat_y, self.I)
        sig_z = kron(Pat_z, self.I)
        
        self.S = [Matrix(0.5*sig_x), Matrix(0.5*sig_y), Matrix(0.5*sig_z)]
        self.S2 = self.Square(self.S)
        self.s = 0.5*sig_z
                
        L_pl = np.zeros((2*l+1, 2*l+1), np.complex128)
        L_mi = np.zeros((2*l+1, 2*l+1), np.complex128)
        for m_ in range(0,2*l+1):
            m = m_ - l
            if m_+1 < 2*l+1:
                L_pl[m_+1, m_] = sqrt((l-m)*(l+m+1))
            if m_-1 >=0:
                L_mi[m_-1, m_] = sqrt((l+m)*(l-m+1))
                                
        L_x = 0.5 * (L_pl+ L_mi)
        L_y = 0.5 * (-1j) * (L_pl - L_mi)
        L_z = np.diag([-l+i for i in range(0,2*l+1)])
        
        self.L = [Matrix(kron(self.I2,L_x)), Matrix(kron(self.I2,L_y)),Matrix(kron(self.I2,L_z))]
        self.L2 = self.Square(self.L)
                
        self.J = [self.L[i]+self.S[i] for i in range(3)]
        self.J2 = self.Square(self.J)
        
        trans0 = Matrix(kron(self.I2,GetRealSHMatrix(l)))
        self.Ylm =  trans0.H * Matrix(kron(self.I2,L_z)) * trans0

        # we translate to coupled basis in order to calculate j, where it is defined in diagonal form
        trans = GetCGMatrix(l)
        J2c = trans.H * self.J2 * trans
        
        # solve j(j+1)=J2
        self.j = trans * Matrix(np.diag([sqrt(J2c[i,i]+0.25)-0.5 for 
                                     i in range(self.J2.shape[0])])) * trans.H
        
        # 1 for l-1/2, -1 for l+1/2 useful for some manipulations
        self.jj = -2.0/(2*l+1) * trans * (J2c-((2*l+1)/2.0+l*l-0.25)*self.II) * trans.H
        self.jlow = (self.jj + self.II) / 2.0
        self.jhigh = (-self.jj + self.II) / 2.0
        
        self.M = [self.L[i]+2.0*self.S[i] for i in range(3)]
        self.M2 = self.Square(self.M)
                
        self.L_squares = [self.L[0]*self.L[0]]  +  [self.L[1]*self.L[1]]  \
                           +  [self.L[2]*self.L[2]]
                           
        self.LS = self.L[0]*self.S[0] +  self.L[1]*self.S[1] +  self.L[2]*self.S[2]

        parityRep = 1 # search SPIN_PARITY_REP
        sgn = -1 if l % 2 == 1 else 1 # (-1)^l
        self.P = parityRep*sgn*Matrix(kron(self.I2,self.I)) # (-1)^l
        sgn_m = np.diag([1 if i % 2 == 1 else -1 for i in range(-l,l+1)]) # (-1)^m
        # Subtlety:
        #  In this definition P*P=1 BUT Pa*Pa=-1, like rotation by 360 degrees, which has a phase
        #  Also, here Px*Py*Pz=P as expected
        self.Pz = 1j*parityRep*Matrix(kron(Pat_z,sgn_m))*sgn # (-1)^(l+m)
        # spatial Py (Py_s): m -> -m, and (-1)^m factor
        Py_s = np.zeros_like(self.I)
        Py_s[l, l] = 1.0
        for m in range(-l,0):
            sgnm = -1 if m % 2 == 1 else 1
            Py_s[l+m, l-m] = sgnm
            Py_s[l-m, l+m] = sgnm
        self.Py = 1j*parityRep*Matrix(kron(Pat_y,Py_s))
        # Px spatially is Py_s * (-1)^m, which just maps Ylm to Yl(-m)
        self.Px = 1j*parityRep*Matrix(kron(Pat_x,np.abs(Matrix(Py_s))))
        self.Ps = [self.Px, self.Py, self.Pz, self.P]
        self.MyBasis = Matrix(np.diag([1+i for i in range(self.P.shape[0])]))
                
        # these 3 lists should have elements that correspond respectively
                
        self.operators = [self.II] + self.L + [self.L2, self.Ylm] + self.S + [self.S2, self.s] + \
                            self.J + [self.J2] + self.Ps + [self.j, self.jj, self.jlow, self.jhigh] + \
                            self.M + [self.M2, self.LS, self.LS*self.jlow] + [self.MyBasis]
        
        
        self.labels = ["N", 
                       "Lx","Ly","Lz","L2", 
                       "Ylm",
                       "Sx","Sy","Sz","S2", 
                       "s",
                       "Jx","Jy","Jz","J2", 
                       "Px","Py","Pz","P",  # reflections / parity 
                       "j", "jj","jlow", "jhigh",
                       "Mx","My","Mz","M2", 
                       "LS", "LSlow",
                       "basis"]
        
        self.prettyLabels = {"N":"N", "Lx":"l_x","Ly":"l_y","Lz":"l_z","L2":"L^2", 
                             "Ylm":"Ylm",
                             "Sx":"s_x","Sy":"s_y","Sz":"s_z","S2":"S^2", 
                             "s":"s",
                             "Jx":"j_x","Jy":"j_y","Jz":"j_z","J2":"J^2", 
                             "Px":"\pi_x","Py":"\pi_y","Pz":"\pi_z","P":"\pi", 
                             "j":"j", "jj":"jj", "jlow":"jlow", "jhigh":"jhigh",
                             "Mx":"m_x","My":"m_y","Mz":"m_z","M2":"M^2", 
                             "LS": "\overrightarrow{L} \dot \overrightarrow{S}",
                             "LSlow":"\overrightarrow{L} \dot \overrightarrow{S}_{low}", 
                             "basis":""}

        class VectorObservable:
            def __init__(self, observable_name : str, operator : Matrix):
                self.name = observable_name
                self.labels = [observable_name + d for d in ['x','y','z']]
                self.operator = operator


        self.vector_labels = ['L','S','J','M']
        self.vector_observables = [VectorObservable(name, getattr(self, name)) for name in self.vector_labels]

        # this is a proxy for counting the number of unique nonzero values in the diagonalized operator
        # the best labels for plotting are those with a higher score
        self.scores =   [0,
                        5,5,5,0, # L's
                        5,       # RSH
                        1,1,1,0, #S's
                        1, #s
                        5,5,5,0, #J's - don't use J2 but j inestead, prefer j over jz 
                        2,2,2,2,
                        5,0,2,2,
                        4,4,4,0,
                        2,
                        1,
                        4  #basis
                        ]
        
        
        assert(len(self.labels) == len(self.operators))
        assert(len(self.labels) == len(self.scores))
        
    def GetOperators(self):
        return self.operators
    
    def GetLabels(self):
        return self.labels
    
    def GetPrettyLabels(self):
        return self.prettyLabels 
        
    def Square(self, op):
        return sum([op[i]*op[i] for i in range(3)]) 
    
    def Transform(self, T):
        operators = []
        for i, op in enumerate(self.operators):
            if self.labels[i] == 'basis':
                operators.append(op)
            else:
                operators.append( T.H * op * T )
        self.operators = operators

        for i, vobs in enumerate(self.vector_observables):
            self.vector_observables[i].operator = [ T.H * op * T  for op in vobs.operator]
               
    
    def IsDiagonal(self, M):
        mat = np.copy(M)
        return np.all(abs(M-np.diag(np.diag(mat))) < 1.0e-5)
 
    def Commute(self, M1, M2:Matrix):
        D = abs(M1*M2 - M2*M1)
        ret = np.all(D < 1.0e-4)
        #if not ret:
        #    print(D)
        return ret
    
    def Get(self, name):
        for i,op in enumerate(self.operators):
            if name == self.labels[i]:
                return op
        raise Exception(f"Cannot find observable {name}")

    def Has(self, name):
        return name in self.labels
 
    # not used at the moment - was written for QuantumNumbers
    def FilterDiagonalObservables(self, printout=False):
        ops = []
        labels = []
        scores = []
        for i, op in enumerate(self.operators):
            if self.labels[i] in ["L2","S2"]:
                continue # these are trivial
            if self.IsDiagonal(op): 
                ops.append(op)
                labels.append(self.labels[i])
                scores.append(self.scores[i])
        self.operators = ops
        self.labels = labels
        self.scores = scores
        if printout:
            print("diagonal operators:")
            for i,op in enumerate(self.operators):
                opn = np.copy(op)
                print(self.labels[i], np.diag(opn).real)        
            
    def FilterConservedObservables(self, M : Matrix):
        ops = []
        labels = []
        scores = []
        for i, op in enumerate(self.operators):
            label = self.labels[i]
            if self.Commute(op, M): 
                ops.append(op)
                labels.append(self.labels[i])
                scores.append(self.scores[i])
                print(f"{label} commutes with M:")
                print(op)
            else:
                print(f"{label} does not commute with M:")
                print(op)
        self.operators = ops
        self.labels = labels
        self.scores = scores
      
    def GetBestLabel(self, ignore = []):
        score = -1
        lbl = ''
        for i,label in enumerate(self.labels):
            if self.scores[i] >= score and label not in ignore:
                score = self.scores[i]
                lbl = label
        return lbl
      
    def evaluate(self, expr : str, opts = None):
        obj = locals()
        for i, label in enumerate(self.labels):
            obj[label] = self.operators[i]

        for i, label in enumerate(self.vector_labels):
            obj[label] = self.vector_observables[i].operator

        ret = eval(expr)
            
        if opts is not None and opts.prompt:
            from code import InteractiveConsole
            # add this for convenience in the interactive shell
            from portobello.rhobusta.observables import tr,comm
            import numpy as np
            import pathlib
            console = InteractiveConsole(locals())
            historyFileName="./.observable.py.history"
            console.push("import readline")
            if pathlib.Path(historyFileName).exists():
                console.push(f"readline.read_history_file('{historyFileName}')")
            console.interact(banner="prepared variables, such as ρ, Z, Eimp, Lx, My, Sz, Ylm, N, j, run self.GetLabels() for full list")
            console.push(f"readline.write_history_file('{historyFileName}')")
            return None
        else:
            return ret


    def CoDiagonalizeMatrices(self, ms, eps=1.0e-5):
        diag, u = np.linalg.eigh(ms[0].astype(dtype=np.complex128))
        # uM.H * M * uM is the diagonal
        uM = Matrix(u)
        if len(ms) == 1:
            return uM
        d0 = diag[0]
        i0 = 0
        uMs = []
        ms2 = [uM.H*m*uM for m in ms[1:]]
        for i,d in enumerate(diag):
            last = i == diag.shape[0]-1
            if abs(d-d0) > eps or last:
                if last: i+=1
                uMs.append(self.CoDiagonalizeMatrices([M[i0:i,i0:i] for M in ms2], eps=eps))
                d0 = d
                i0 = i
        return uM * Matrix(block_diag(*uMs))
                
    def CoDiagonalize(self, expr:str, prepend_Sz=False):
        exps = expr.split(",")
        if prepend_Sz and "Sz" != exps[0]:
            exps = ["Sz"] + exps
        ms = [self.evaluate(se) for se in exps]# list of matrices to co-diagonalize
        if len(ms) > 1:
            rng = range(len(ms))
            for m1, m2 in itertools.combinations(rng,2):
                assert(self.Commute(ms[m1], ms[m2])), f"matrices {exps[m1]}, {exps[m2]} to co-diagonalize do not commute"
        return self.CoDiagonalizeMatrices(ms)

    def evaluateEigh(self, expr:str):
        res = self.evaluate(expr)
        if not isinstance(res,list): #scalar observable
            res = [res]

        diags = []
        uMs = []
        for M in res:
            diag, u = np.linalg.eigh(M.astype(dtype=np.complex128))
            uM = Matrix(u)
            diags.append(diag)
            uMs.append(uM)

        return uMs, diags

    def evaluateObsList(self, expr : str):
        desired_obs = expr.split(",")

        decompositions = []
        for dobs in desired_obs:
            uMs, diags = self.evaluateEigh(dobs)
            decompositions.append(ObservableDecomposition(uMs, diags, dobs))

        return decompositions

    def AddObservable(self, M : Matrix, label : str, score = -1):
        self.operators += [M]
        self.labels += [label]
        self.scores += [score]

class ObservableDecomposition:
    def __init__(self, eigenspace, eigenvalues, expr) -> None:

        self.size = len(eigenvalues)
        self.is_scalar =  self.size == 1
        assert (self.is_scalar or self.size == 3)

        self.directions = ["x","y","z"] if not self.is_scalar else [""]
        
        self.eigenspace = eigenspace
        self.eigenvalues = eigenvalues
        self.expr = expr           
            
# this is also the FLAPW native basis in non-relativistic computations
class ProductBasisRSHObs(ProductBasisSHObs):
    def __init__(self, l, T=None):
        from portobello.symmetry.local import GetRealSHMatrix

        ProductBasisSHObs.__init__(self, l)
        trans = Matrix(kron(self.I2,GetRealSHMatrix(l))).H
        if T is not None:
            trans = trans * Matrix(kron(self.I2,T))
        self.Transform(trans)
        
# coupled basis is also the FLAPW native basis in relativistic computations
class CoupledBasisObs(ProductBasisSHObs):
    def __init__(self, l, T=None):
        from portobello.symmetry.local import GetCGMatrix
        ProductBasisSHObs.__init__(self, l)
        trans = GetCGMatrix(l)
        if T is not None:
            trans = trans * T

        self.Transform(trans)
 
# get a boolean matrix that defines the subspace within the shell
# get a boolean matrix that defines the subspace within the shell
def GetSubspace(shell : AtomicShell, obs, subspace_expr : str):
    cluster_size = max(1,shell.cluster_size) #earlier data with no cluster size
    subspace = np.ndarray(shape=(shell.dim), dtype=bool)
    dim = shell.dim//cluster_size
    for i in range(cluster_size):
        sl = slice(i*dim,(i+1)*dim)
        subspace[sl] = np.invert(np.isclose(np.diag(obs.evaluate(subspace_expr))[0,:dim], 0))# invert(isclose(...) is to get rid of small values
    return subspace

# Get list of all local observables (related to moment, spin and magnetism)
def GetAllObservables(shell : AtomicShell, subspace_expr = "", printout=False):
    cluster_size = max(1,shell.cluster_size) #earlier data with no cluster size
    dim = shell.dim//cluster_size
    basis = shell.basis#TODO: [:dim,:] test once we have clusters working
    if shell.nrel > 1:
        obs = CoupledBasisObs(shell.l, T=Matrix(basis))
    else:
        obs = ProductBasisRSHObs(shell.l, T=Matrix(basis))
    if printout:
        for i,op in enumerate(obs.operators):
            print(obs.labels[i], ":")
            print(op)

    if not subspace_expr == "":
        subspace = GetSubspace(shell, obs, subspace_expr)
        shell.basis = shell.basis[:, subspace]
        obs = GetAllObservables(shell, printout = False)

    if cluster_size > 1:
        for iop,op in enumerate(obs.operators):
            dim = op.shape[0]//cluster_size
            if shell.nrel == 1:
                dim = dim// 2

            op_block = op[:dim,:dim]
            for i in range(cluster_size):
                sl = slice(i*dim,(i+1)*dim)
                obs.operators[iop][sl,sl] = op_block
            
            if shell.nrel == 1:
                offset = dim*cluster_size
                op_block = op[offset:offset+dim,offset:offset+dim]
                for i in range(cluster_size):
                    sl = slice(offset + i*dim,offset + (i+1)*dim)
                    obs.operators[iop][sl,sl] = op_block

    return obs

def GetAllCommutingObservables(shell : AtomicShell, Hloc:Matrix, printout=False):
    if shell.nrel > 1:
        obs = CoupledBasisObs(shell.l, T=Matrix(shell.basis))
    else:
        obs = ProductBasisRSHObs(shell.l, T=Matrix(shell.basis))
    obs.FilterConservedObservables(Hloc)
    if printout:
        for i,op in enumerate(obs.operators):
            print(obs.labels[i], ":")
            print(op)
            
    return obs
  
def GetLabelsForPlotting(state : SolverState, shell : AtomicShell, opn = "", subspace_expr = "", ignore = []):
    from portobello.symmetry.local import GetRSHLabels
    """Returns two lists 
    1.  labels for each element in the diagonal of the rep, one list per spin index
    2.  the unique elements of this label list, with the order preserved
    """

    obs = GetAllObservables(shell, subspace_expr = subspace_expr)
    
    obs.FilterDiagonalObservables()
    labels = obs.GetLabels()
    if opn == '' or opn not in labels:
        opn = obs.GetBestLabel(ignore)
    if opn == '':
        opn = 'basis'
    spin = None
    if 's' in labels:
        # spin is diagonal - add it to the label
        spin = obs.Get('s')

    pl = obs.GetPrettyLabels()[opn]
    op = obs.Get(opn)
    vals = []
    
    if opn not in ("Ylm", "Jz"): 
        opn = 'basis'
        op = obs.Get('basis')

    for s in range(op.shape[0]):
        if opn == "Ylm":
            vs = set()
            for i in range(op.shape[0]): #(2l+1) * 2
                bi = int(round(op[i,i].real))+shell.l
                ri = shell.rep[i% state.dim,i% state.dim]
                if  ri == shell.rep[s % state.dim, s % state.dim]:
                    sp = ''
                    if spin is not None:
                        sp=" \\downarrow" if spin[i,i] < 0 else " \\uparrow"
                    vs.add(GetRSHLabels(shell.l)[bi].latex_label + sp)
                        
            vs = "/".join(vs)
            
        elif opn=='basis':
            vs = f"{s+1}"
        else:
            v = Fraction.from_float(float(np.real(op[s,s]))).limit_denominator(16)
            vs = ""
            sgn = ""
            if v <0:
                sgn="-"
            if v.denominator == 1:
                vs += "%s=%s%d"%(pl, sgn, abs(v.numerator))
            else:
                vs += "%s=%s\\frac{%d}{%d}"%(pl, sgn, abs(v.numerator), abs(v.denominator))
        
        vals.append(vs)
        
    orbs = list(dict.fromkeys(vals)) #an ordered set 
    if state.num_si == 1:
        if state.nrel == 2:
            vals2 = [vals]
        else:
            vals2 = [vals[:op.shape[0]//2]]
    else:
        vals2 = [vals, 
                 vals]

    causing_problems = """
    #even the best label can have fewer elements than the rep
    #so we combine labels, ignoring those we've previously collected until we have 
    #len(orbs) == number of elements in rep
    if len(ignore) > 0:
        return vals2, orbs, opn #early return for internal use (concatenating labels -- otherwise we get recursive calls)

    n = np.max(np.diag(shell.rep))
    while len(orbs) < n:
        ignore += [opn]
        new_vals, orbs_, opn = GetLabelsForPlotting(state, shell, subspace_expr = subspace_expr, ignore = ignore)
        for si, v_si in enumerate(new_vals):
            for i, v in enumerate(v_si):
                vals2[si][i] += f", {v}"
        orbs = list(dict.fromkeys(vals2[0]))

    if len(orbs) > n:
        new_orbs = {}
        for i in range(shell.dim):
            rep = shell.rep[i][i]
            if rep not in new_orbs.keys():
                new_orbs[rep] = orbs[i]
            else:
                new_orbs[rep] += f", {orbs[i]}"

        for i in new_orbs.keys():
            label = ", ".join(list(set(new_orbs[i].split(", "))))
            new_orbs[i] = label
        orbs = list(new_orbs.values())
    """
    
    # the sets are per-spin-index, for convenience
    return vals2, orbs
    

def RhobustaMatrixToPatrickMatrix(matrix, state : SolverState):
    if state.num_si == 1:
        if state.nrel == 1:
            return kron(np.eye(2),matrix[:,:,0])
        else:
            return matrix[:,:,0]
    else:
        return block_diag(matrix[:,:,0], # up - Patrick convention
                            matrix[:,:,1]) # down

def AddSpinPatrick(matrix, state : SolverState):
    if state.num_si == 1:
        if state.nrel == 1:
            return Matrix(kron(np.eye(2),matrix[:,:]))
        else:
            return Matrix(matrix)
    return Matrix(block_diag(matrix, # up - Patrick convention
                      matrix)) # down

def GetDenmat(state : SolverState):
    return Matrix(RhobustaMatrixToPatrickMatrix(state.RhoImp, state))

def GetEimp(state : SolverState, opts):
    if hasattr(state, 'Elocal'): # init one body was run
        return Matrix(RhobustaMatrixToPatrickMatrix(state.Elocal, state))
    imp = ImpurityProblem()
    imp.load(opts.impurity)
    return Matrix(RhobustaMatrixToPatrickMatrix(imp.E, state))
    #return Matrix(np.diag(np.diag(RhobustaMatrixToPatrickMatrix(imp.E, state)[:,:,0])))
 
def GetOccSusc(impMeas : ImpurityMeasurement, state : SolverState):
    occSusc = [Matrix(mat[:,:,0]) for mat in impMeas.OccSusc.M]
    return occSusc
    
def MatrixFunction(M, function):
    dd, u = np.linalg.eigh(M)
    uM = Matrix(u)
    return uM *  np.diag(function(dd)) * uM.H

def AddObservables(obs, state : SolverState, opts):    
    loc, isDmft = FindMainFile(opts)
    denmat = GetDenmat(state)
    obs.AddObservable(denmat, "ρ")
    obs.AddObservable(denmat/(np.trace(denmat)[0,0])**2, "purity") # tr((rho/tr(rho))^2)
    obs.AddObservable(-MatrixFunction(denmat, np.log), "entropy") # tr((rho/tr(rho))^2)
    if isDmft and hasattr(opts,"Eimp"): obs.AddObservable(GetEimp(state, opts)*1000.0, "Eimp")
    if not isDmft: 
        gs = GState()
        gs.load(loc)
        if hasattr(opts,"Eimp"): 
            obs.AddObservable(RhobustaMatrixToPatrickMatrix(gs.Hloc, state)*1000.0, "Eimp")
            obs.AddObservable(RhobustaMatrixToPatrickMatrix(gs.Z, state), "Z")
        obs.AddObservable(RhobustaMatrixToPatrickMatrix(gs.Lambda, state), "λ")

    dim = obs.evaluate("Mx").shape[0]

    # Add directional operators in the direction of magnetism (if there is a moment)
    Mv = np.array([np.trace(denmat[:dim,:dim]*obs.evaluate("Mx"))[0,0],
                   np.trace(denmat[:dim,:dim]*obs.evaluate("My"))[0,0],
                   np.trace(denmat[:dim,:dim]*obs.evaluate("Mz"))[0,0]])
    if norm(Mv) < 1.0e-4:
        Mv = np.array([0.0, 0.0, 1.0]) # orient at Z
    Mv /= norm(Mv)
    def GetOpInMdir(op):
        return Mv[0]*obs.Get(op+"x") + Mv[1]*obs.Get(op+"y") +Mv[2]*obs.Get(op+"z")
    obs.AddObservable(GetOpInMdir("S"),"Sm")
    obs.AddObservable(GetOpInMdir("L"),"Lm")
    obs.AddObservable(GetOpInMdir("J"),"Jm")
    obs.AddObservable(GetOpInMdir("M"),"Mm")
    obs.AddObservable(GetOpInMdir("P"),"Pm")


    from portobello.rhobusta.PEScfEngine import PEScfEngine # prevent cycle
    obs.AddObservable(AddSpinPatrick(PEScfEngine.CorrectRep(state), state), "rep")
    basis = AddSpinPatrick(state.rep, state)
    
def CalculateR(proj : SelectedOrbitals, state: SolverState, opts:Namespace):
    shell = proj.shells[opts.which_shell]
    isRel = state.nrel > 1

    opts.num_mpi=0
    opts.worker=False
    mpi = MPIContainer(opts)
    dft = DFTPlugin.Instance(restart=True, mpi=mpi).GetDFTStuff()

    loc = "./flapw_basis.tmp.h5:/"
    basis = MuffinTinBasis()
    basis.GetMtBasis(loc)
    basis.load(loc)
    isort = dft.st.distinct_number[shell.atom_number] -1

    tin = basis.tins[isort]

    import matplotlib.pyplot as plt
    from scipy.integrate import trapz

    BohrToAng = 1./1.88973
    c = 274.074
    c2 = c**2
    tt = 0.5
        
    for iorb in shell.lapw_indices[:shell.dim]:
        ibasis = tin.lapw2self.data[iorb]
        
        fun = tin.radial_functions[ibasis, :] ** 2
        plt.plot(tin.radial_mesh, tin.radial_functions[ibasis, :])

        if isRel:
            fun += tin.radial_functions2[ibasis, :] ** 2 / c2
            plt.plot(tin.radial_mesh, tin.radial_functions2[ibasis, :]/c, ".")

        avg_r = trapz( fun*tin.radial_mesh, tin.radial_mesh) * BohrToAng
        avg_r2 = trapz( fun*tin.radial_mesh*tin.radial_mesh, tin.radial_mesh)* BohrToAng* BohrToAng

        p_1 = trapz( fun, tin.radial_mesh)
        r_peak = tin.radial_mesh[np.argmax(fun)]* BohrToAng
        for i in range(tin.radial_mesh.shape[0]):
            if tin.radial_mesh[i] > tt:
                fun[i:] = 0.0
                break
        p_0 =  trapz( fun, tin.radial_mesh)

        print(f"<r>: {avg_r:3.3f}, <r^2>: {np.sqrt(avg_r2-avg_r**2):3.3f} p(r<{tt* BohrToAng:3.3f}[A]): {p_0/p_1:3.3f} r_peak: {r_peak:3.3f}")


    plt.show()

def FindMainFile(opts):
    loc = None
    isDmft = False
    if hasattr(opts,'version') and opts.version != "":
        maj, sub = opts.version.split(".")
        loc = f"./history.h5:/impurity/{maj}/{sub}/"

    if Path("gutz.h5").exists():
        if loc is None: loc = "./gutz.h5:/"
        assert(not hasattr(opts,'susceptibility') or not opts.susceptibility)
    elif Path(f"./gutz{opts.which_shell}.h5").exists():
        if loc is None: loc = f"./gutz{opts.which_shell}.h5:/"
        assert(not hasattr(opts,'susceptibility') or not opts.susceptibility)
    elif Path("dmft.h5").exists():
        if loc is None: loc = "./dmft.h5:/"
        isDmft = True
    elif Path(f"dmft{opts.which_shell}.h5").exists():
        if loc is None: loc = f"./dmft{opts.which_shell}.h5:/"
        isDmft = True
    else:
        raise Exception("no dmft.h5 or gutz.h5 file around")
    return loc, isDmft


def main():
    parser = ArgumentParserThatStoresArgv('obs', add_help=True ,formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--verbose", dest="verbose", 
                      action="store_true",
                      default=False,
                      help="print matrices of observables")
    
        
    parser.add_argument("-v", "--version", dest="version", default='',
                      type=str, help="version such as 1.1 or 2.4")
 
    parser.add_argument("-H", "--histogram-configs",
                         dest="histogram_configs",
                    default=0,
                    type=int,
                    help = "this number of highest probability configurations will be examined and their observable value evaluated") 

    parser.add_argument("-e", "--eval", dest="expr", default="", 
                        help="""evaluate the expression in the local basis. The expression can contain any of the 
                                local operators, and allows algebraic operations (eg. matrix product) and more. For example
                                '2.0*Mx+My-Mz' returns a magnetization matrix, 'jjlow*0.2' returns the lower j sector identity multiplied by 0.2 """)
    
    parser.add_argument("--full-susceptibility",
                        dest="full_susceptibility",
                        default=False,
                        action="store_true",
                        help = "Instead of computing the static observable, plot the dynamic susceptibility of the desired observable (Must have run DMFT/CTQMC with the  --worms=\"susc ph\" option; Must give an expression with -e/--eval); Must be using DMFT and not Gutz")

    parser.add_argument("-Q", "--quantum-numbers", dest="qn", default="",
                      help="Quantum numbers to use.")
                      
    parser.add_argument("--print-eigs",
                        dest="print_eigs",
                        default=False,
                        action="store_true",
                        help = "print eigenvalues and eigenvectors of the observable")
     
    parser.add_argument("--Eimp",
                        dest="Eimp",
                        default=False,
                        action="store_true",
                        help = "add Eimp to the observables (and related observables)")

    parser.add_argument("--dc",
                        dest="double_counting",
                        default=False,
                        action="store_true",
                        help = "print current double counting")

    parser.add_argument("--susceptibility", 
                        dest="susceptibility", 
                        default=False, 
                        action="store_true",
                        help = "Instead of computing the static observable, plot the dynamic susceptibility of the desired observable (Must have run DMFT/CTQMC with the  --occupation-susceptibility option; Must give an expression with -e/--eval); Must be using DMFT and not Gutz")
    
    parser.add_argument("-I", "--impurity-problem", dest="impurity", default="./dmft.h5:/impurity/",
                      help="designates an object of type OrbitalsBandProjector")
                      
    parser.add_argument("--which-shell", dest="which_shell", default=0, type=int)

    parser.add_argument("-r",
                        dest="calculate_r",
                        default=False,
                        action="store_true",
                        help = "Calculate <r> for the correlated orbital")
     
    parser.add_argument("---",
                        dest="prompt",
                        default=False,
                        action="store_true",
                        help = "open shell with the local variables")

    parser.add_argument("-P", "--plot", dest="plot", default=False, action="store_true",
                      help="Plot susceptibilities")
                      
    opts = parser.parse_args(sys.argv[1:])
 

    #from portobello.rhobusta.ctqmc import Serializer
    # simple program - print the good quantum numbers
    # for the first shell in the (default) projector
    proj_location = "./projector.h5:/def/"
    proj = SelectedOrbitals()
    proj.load(proj_location)
    
    #imp = ImpurityProblem()
    #imp.load("./dmft.h5:/impurity/")
    #sz = Serializer(imp)
        
    obs = GetAllObservables(
            proj.shells[opts.which_shell],
            subspace_expr = proj.subspace_expr,
            printout=opts.verbose)

    loc, isDmft = FindMainFile(opts)
    opts.impurity = loc+"impurity/"

    state = SolverState()
    print(f"- loading from {loc}")
    state.load(loc)
    if opts.prompt: 
        opts.Eimp = True
        opts.expr = "Eimp"
    AddObservables(obs, state, opts)
    
    
    if opts.qn != "":
        uM = obs.CoDiagonalize(opts.qn)
        obs.Transform(uM)
        
    
    denmat = obs.Get("ρ")

    if opts.calculate_r:
        CalculateR(proj, state, opts)

    if opts.histogram_configs > 0:
        exps = opts.expr.split(",")
        ms = [Matrix(obs.evaluate(exp)) for exp in exps]

        table = []
        tableH = ["N", "prob."]
        isD = [obs.IsDiagonal(m) for m in ms]
        sums = {} 
        averages = {}
        bit_exps = []
        for i, exp in enumerate(exps):
            if exp.startswith("++"): 
                averages[exp] = 0.0
                tableH.append(f"Σ{exp[2:]}/N")
            elif exp.startswith("+"): 
                tableH.append(f"{exp[1:]}")
                sums[exp] = 0.0
            else:
                bit_exps.append(exp)

        def Frac(ie, f):
            if isD[ie]:
                if exps[ie].startswith("S"): # if spin
                    if abs(f.real - 0.5)<1.0e-3:
                        return "↑"
                    else:
                        return "↓"
                return Fraction.from_float(float(f.real)).limit_denominator(16) 
            else:
                return f"{float(f.real):3.2f}"

        def summarize_sums(num_bits):
            return [f"{sums[s]:3.2f}" for s in sums] + [f"{averages[s]/float(num_bits):3.2f}" for s in averages]

        # check that these matrices are diagonal
        ii = list(reversed(np.argsort(state.hist.p)))[:opts.histogram_configs]
        unique_mtplts = []
        for conf in ii: 
            p = state.hist.p[conf]
            numBits = countBits(conf)
            if len(ms) > 0: denmat = np.zeros_like(ms[0])
            bits = conf
            n = 0
            bit_values = []
            for s in sums: sums[s] = 0.0
            for s in averages: averages[s] = 0.0
            while bits != 0:
                b = bits & 0x1
                if b == 1: 
                    denmat2 = np.copy(denmat)
                    denmat2[n,n] = 1.0
                    values = []
                    for ie, expr in enumerate(ms):
                        value  = np.trace(expr * denmat2)[0,0]
                        if exps[ie] in sums:
                            sums[exps[ie]] += np.real(value)
                        if exps[ie] in averages:
                            averages[exps[ie]] += np.real(value)
                        values.append(f"{Frac(ie, value)}")
                    
                    mltplt = (";".join(values),n)
                    if not mltplt in unique_mtplts:
                        unique_mtplts.append(mltplt)
                    bit_values.append(unique_mtplts.index(mltplt))
                bits = bits >> 1
                n +=1

            dim = state.dim
            if state.num_si == 1 and state.nrel == 1:
                dim = state.dim*2
            table.append([#"".join(reversed(np.binary_repr(conf, width=dim))),
                          numBits, 
                          f"{p:3.3f}",
                          *summarize_sums(numBits), 
                          bit_values])

        def ValuesAtIndex(t, bit_values, jj, uv):
            if jj in bit_values: 
                return uv[0]
            else: 
                return "" 

        for it, t in enumerate(table):
            bit_values = t[-1]
            table[it] = t[:-1] + [ValuesAtIndex(t, bit_values, jj, uv) for jj,uv in enumerate(unique_mtplts)]

        tableH.extend([u[0] for u in unique_mtplts])
        tt = tabulate(table, headers=tableH)
        print(tt)
        return
  
    if opts.expr == "" and not opts.prompt:
        print(f"μ: {state.mu} [eV]")
    
    if opts.full_susceptibility:
        if opts.expr == "":
            print("To compute the susceptibility, input an expression from which to compute one, e.g., --eval=Mz")
            return

        res = obs.evaluate(opts.expr, opts)
        if res is None:
            return
        #not sure what to do with non matrix expressions...
        assert(isinstance(res, Matrix) and not res.shape == (1,1))
        
        res = Matrix(res)
       
        n = res.shape[0]
        susc = np.zeros((n,n,n,n),dtype=np.complex128)
	
        def compute_susc(fname):
            s = None
            with open(fname,"r") as f:
                data = f.read()
                data = json.loads(data)
                if "susc ph" in data.keys():
                    susc_dict = data["susc ph"]["susceptibility"]
                    for entry, value in susc_dict.items():
                        entry = entry.split("_")

                        for i in range(len(entry)):
                            entry[i] = int(int(entry[i])/2)
                   
                        susc[entry[0], entry[1], entry[2], entry[3]] = value["function"]["real"][0] + 1j*np.array(value["function"]["imag"][0])
                    s = np.tensordot( np.tensordot(res,susc,[[0,1],[0,1]]), res, [[0,1],[0,1]])
                else:
                    s = "unknown"
            return s

        s = compute_susc("Impurity/params.obs.json")
        e = compute_susc("Impurity/params.err.json")
        
        print(f"Susceptibility for {opts.expr}: {s} +/- {e}")

    if opts.susceptibility:
        if opts.expr == "":
            print("To compute the susceptibility, input an expression from which to compute one, e.g., --eval=Mz")
            return
        
        res = obs.evaluate(opts.expr, opts)
        #not sure what to do with non matrix expressions...
        assert(isinstance(res, Matrix) and not res.shape == (1,1))
        
        imp = ImpurityProblem()
        imp.load(opts.impurity)
        
        impMeas = ImpurityMeasurement()
        impMeas.load(imp.self_path + "/measurement/")
      
        res = Matrix(res)
        occSusc = GetOccSusc(impMeas, state)
        susc = np.zeros(len(occSusc), dtype=np.complex128)
        #for i,j in product(range(occSusc[0].shape[1]),repeat=2):
        #    susc += [res[i,i] * s[i,j] * res[j,j] for s in occSusc]
        
        d = np.diag(res)
        susc = np.array([np.dot(np.dot(d, s), np.transpose(d))[0,0] for s in occSusc])

        print(res)
        v = np.real(np.trace(res * denmat)[0,0])
        print(f"{opts.expr}: {v}")
        print(f"Susceptibility for {opts.expr}: {susc.real}")

        if opts.plot:
            import matplotlib.pyplot as plt
            plt.plot(susc)
            plt.show()
        
        return
        
    obsValues = {}
    
    if opts.expr == "":
        labels = obs.GetLabels()
        for i,op in enumerate(obs.GetOperators()):
            v = np.real(np.trace(Matrix(op) * denmat)[0,0])
            obsValues[labels[i]] = v
            #opM = Matrix(op)
            #op2 = opM.H * opM
            #Eimp = obs.Get("Eimp")
            #trE = np.real(tr(op2*Eimp)/tr(op2))
            print(f"{labels[i]:10s}: {v:4.4f}") # [{trE:4.4f}]")
    else:
        res = obs.evaluate(opts.expr, opts)
        if res is None:
            return
        if isinstance(res, numbers.Number):
            print(res)
        elif isinstance(res, Matrix) and res.shape == (1,1):
            print(res[0,0])
        elif isinstance(res, Matrix):
            print(res)
            v = np.real(np.trace(res * denmat)[0,0])
            print(f"{opts.expr}: {v}")
        else:
            v=[]
            for i, m in enumerate(res):
                print(f"{i+1}:")
                print(Matrix(m))
                v.append(np.real(np.trace(Matrix(m) * denmat)[0,0]))
            print(f"{opts.expr}: {v}")
        if opts.print_eigs:
            uMs, diags = obs.evaluateEigh(opts.expr)
            print("unitary:")
            print(Matrix(uMs[0]))
            print(diags[0])
        return
        
    #denmat2 = Matrix(np.copy(denmat))
    #for m1,m2 in product(range(denmat2.shape[0]), repeat=2):
    #    if m1 != m2: denmat2[m1,m2]=0.0

    #print("diagonal Mx:", np.trace(denmat2*obs.Get("Mx"))[0,0])
    #print("diagonal My:", np.trace(denmat2*obs.Get("My"))[0,0])
    #print("diagonal Mz:", np.trace(denmat2*obs.Get("Mz"))[0,0])
    
    LiSj = kron(obs.Get("Lx"), obs.Get("Sx")) + kron(obs.Get("Ly"), obs.Get("Sy")) \
              + kron(obs.Get("Lz"), obs.Get("Sz"))
        
    rho2 = Matrix(kron(denmat, denmat))
    
    #for idx, x in np.ndenumerate(LiSj):
    #    if abs(LiSj[idx]) > 0.2 and abs(rho2[idx]) > 0.1:
    #        print(f"LiSj, {idx} = {x}, rho={rho2[idx]}")
    
            
    # get the nontrivial diagonal operators
    obs.FilterDiagonalObservables()
    labels = obs.GetLabels()

    print("Quantum numbers:", labels)  
    
    #obsv = obs.GetBestLabel()
                
    #vals, orbs = GetLabelsForPlotting(state, shell=proj.shells[0], opn=obsv)
    #print(f"basis labels (using {obsv}):", vals, "unique:", orbs)

    if opts.double_counting:
        print(Matrix(state.DC[:,:,0]))

if __name__ == '__main__':
    main()

