#!/usr/bin/env python3

'''
Created on Jul 17, 2018

@author: adler@physics.rutgers.edu
'''

import logging
import os
from typing import List

import scipy
from portobello.jobs.Organizer import with_organizer
from portobello.bus.mpi import MPIContainer
import signal
import sys
from argparse import ArgumentDefaultsHelpFormatter
from itertools import product

import numpy as np
from numpy.linalg.linalg import norm
from portobello.bus.Matrix import Matrix, ZeroComplexMatrix
from portobello.bus.persistence import Persistent, Store
from portobello.generated import self_energies
from portobello.generated.dmft import DMFTState, ImpurityProblem
from portobello.generated.PEScf import SolverState
from portobello.generated.self_energies import (LocalOmegaFunction,
                                                OmegaFunctionInBands,
                                                OmegaMesh)
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.ctqmc import (CTQMCOptionsParser, GetMeasurement,
                                       CTQMC)
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.observables import GetLabelsForPlotting
from portobello.rhobusta.options import RefreshOptions, SaveOptions
from portobello.rhobusta.PEScfEngine import ( GetPEScfOptionsParser,
                                             InterpolatedSelfEnergy,
                                             PEScfEngine, Ry2eV)
from portobello.rhobusta.ProjectorEmbedder import CorrelatedSubspaceProjector
from portobello.rhobusta.multi_impurity import MultiImpurityEmbedder


def VerifyOrthonormalityOfKSBasis(KS, lapw):
    mtO = Matrix(lapw.mo_overlap)
    
    # verify that the basis vectors are orthonormal for every k
    print("Verifying that the Kohn-Sham basis is orthonormal")
    for k,si in  product(list(range(KS.num_k_irr)), list(range(lapw.num_si))):
        d = lapw.pw_overlap[k].num_pw
        num_bands = KS.num_bands[k,si]
        
        A = Matrix(KS.A[0:d, 0:num_bands, k,si])
        Z = Matrix(KS.Z[:,   0:num_bands, k,si])
        
        O = Matrix(lapw.pw_overlap[k].overlap)
        
        testI = Z.H*mtO*Z + A.H*O*A
        
        nrm = norm(testI - np.eye(*testI.shape)) 
        if nrm > 1.0e-8:
            print("matrix for k=", k, "is not unity, norm=", nrm)
            #print np.array2string(testI[0:2,0:2], 150, 3, suppress_small=True)
    print("ended verification")

# TODO: move to options
dmftLocation = "./dmft{0}.h5:/"
        
# note that this inheritace means that all the fields with the same names as in DMFTState
# will be persisted
class DMFTOrchastrator(DMFTState, PEScfEngine):
    
    # opts has to be defined, if running for the first time (there is no dmft state)
    def __init__(self, opts, dry_run=False, for_dft=False):
        self.dry_run = dry_run
        self.mpi_workers = opts.mpi_workers
        
        # just initialize the superclass object 
        DMFTState.__init__(self)
        
        # this one does most of the work
        is_new = PEScfEngine.__init__(self, opts)

        if is_new:
            self.num_freqs =  int(opts.energy_limit * self.beta / (2*np.pi))
            print(" - energy limit dmft: %f, beta=%f"% (opts.energy_limit, opts.beta)) 
            print(" - number of frequency points in dmft: %d"% (self.num_freqs)) 
            
            self.UpdateDoubleCounting()
            
            self.sig = self.NewFreqDepLocalEnergyMatrix()
            for iw in range(self.sig.num_omega):
                self.sig.M[iw,:,:,:] = self.DC[:,:,:]
                
            self.sig.moments[0,:,:,:] = self.DC[:,:,:]
            self.GImp = self.NewFreqDepLocalEnergyMatrix()
            self.hyb = self.NewFreqDepLocalEnergyMatrix()
            

        self.real_axis_sig = False # False until loading real axis sig
        self.StoreMyself()
        if self.plugin.IsMaster(): print(f" - number of equivalent atoms: {self.num_equivalents}")
            
    def GetMethodName(self):
        return "dmft"

    def GetMethodLocation(self):
        return dmftLocation

    def NewFreqDepLocalEnergyMatrix(self):
        fdlem = LocalOmegaFunction(num_omega=self.num_freqs, 
                                   num_orbs=self.corrSubshell.dim,
                                   num_si=self.num_si)
        fdlem.allocate()
        fdlem.omega[:] = [(2*i+1)*np.pi/self.beta for i in range(0,self.num_freqs)]
        return fdlem
                   
    def GetSigmaLocation(self, final = False):
        if final:
            return f"./self-energy-{self.plugin.mpi.GetRank()}.core.h5:/"
        else:
            return f"./self-energy{self.GetSuffixOfImpurity()}-{self.plugin.mpi.GetRank()}.core.h5:/"

    def GetHybridizationLikeMoment(self, f):
        N = f.omega.shape[0]
        n = max(1, int(N*0.1))
        omegas_for_fit = range(N-n,N)

        for si in range(self.num_si):

            D = np.zeros_like(f.M[0, :,:, si])
            iwD = np.zeros_like(f.M[0, :,:, si])
            D2 = np.zeros_like(f.M[0, :,:, si])
            iwD2 = np.zeros_like(f.M[0, :,:, si])

            for iomega in omegas_for_fit:
                hyb = Matrix(f.M[iomega, :,:, si])
                hybH = hyb.H
                iw = 1j*f.omega[iomega]

                D +=  hyb + hybH
                iwD += iw*(hyb - hybH)
                D2 += np.multiply(np.abs(hyb),np.abs(hyb)) + np.multiply(np.abs(hybH),np.abs(hybH))
                iwD2 += iw*(np.multiply(np.abs(hyb),np.abs(hyb)) - np.multiply(np.abs(hybH),np.abs(hybH)))

            #hyb = moment/(iw - eps)
            denom = 2*n*D2 - np.abs(D)*np.abs(D)
            f.moments[0,:,:,si] = np.real((iwD*D2 - D*iwD2) / denom) #moment
            f.moments[1,:,:,si] = np.real((2*n*iwD2 - iwD*np.conj(D)) / denom) #eps
        return f

    def GetHybridizationMoment(self):
        self.hyb = self.GetHybridizationLikeMoment(self.hyb)

    @classmethod
    def GenerateMeshForSigDeriv(cls, beta, nn=3, delta=1.0e-4):
        delta = 1.0e-4
        nn = 3
        interp_range = nn * 2*np.pi / beta # eV
        mesh = np.array([i*delta  for i in range(int(interp_range / delta))])
        return mesh

    @classmethod
    def SigExtrapolate(cls, sigma_in, omega_in, omega_out, n=10):
        n=min(n, len(sigma_in))
        from scipy.optimize import least_squares
        def func(x, a, b, c):
            return a + b*x + c*x*x

        def func_formatted(x):
            return sigma_in[:n] - func(omega_in[:n], x[0], x[1], x[2])
            
        res = least_squares(func_formatted, [0,0,0])
        
        return func(omega_out, res.x[0], res.x[1], res.x[2])

    def GetSigDeriv(self, raw = False, from_imaginary = False):
        if from_imaginary:
            mesh = self.GenerateMeshForSigDeriv(self.beta)

            correctedRep = self.CorrectRep()
            
            F = np.zeros((self.dim,self.dim,self.num_si,self.sig.M.shape[0]), dtype=np.float64)
            if not raw:
                S = np.zeros((self.dim,self.dim,self.num_si,mesh.shape[0]), dtype=np.float64)
                
            for i,j in product(range(self.dim), repeat=2):
                if correctedRep[i,j] != 0:
                    for si in range(self.num_si):
                        for om in range(self.sig.M.shape[0] ):
                            F[i,j,si,om] = self.sig.M[om, i, j, si].imag
                        # Now interpolate F
                        if not raw:
                            S[i,j,si,:] = self.SigExtrapolate(F[i,j,si,:], self.sig.omega, mesh)
            
            if raw:
                slope = (F[:,:,:,1] - F[:,:,:,0])/(self.sig.omega[1]-self.sig.omega[0])

            else:
                slope = np.real(S[:,:,:,1] - S[:,:,:,0])/(mesh[1]-mesh[0])

            eps=1e-3
            for si in range(self.num_si):
                for i,j in product(range(self.dim), repeat=2):
                    if correctedRep[i,j] != 0:
                        if slope[i,j,si] > 1-eps:
                            slope[i,j,si] = -100

            Z = Matrix(np.eye(self.dim) - slope[:,:,0])

            return slope
        else:
            mid = np.argmax(self.sig.omega > 0)
            domega = self.sig.omega[mid] - self.sig.omega[mid-1]
            slope = np.zeros_like(self.sig.M[0,:,:,:])

            for (i,j) in product(range(self.corrSubshell.dim), repeat=2):
                if self.rep[i,j] != 0:
                    slope[i,j,:] = (self.sig.M[mid,i,j,:].real - self.sig.M[mid-1,i,j,:].real)/domega

            return slope


    def ConstructZ(self, proj, I, k, si):
        embedded_dsig = proj.EmbeddedIntoKBandWindow(self.GetSigDeriv(from_imaginary = not self.real_axis_sig, raw = True), k, si)
        Zinv = Matrix(I - embedded_dsig)
        
        return Zinv.I

    def ConstructQuasiparticleHamiltonian(self, proj, epsAllK, I, k, si):
        Hk = np.diag(epsAllK[:,k, si])

        sig = self.sig.M[np.argmax(self.sig.omega > -1e-10),:,:,:]
        Hqp = Matrix( Hk + proj.EmbeddedIntoKBandWindow(sig-self.DC, k, si) )

        if self.real_axis_sig:
            return Hqp
        else:
            sqrtZk = Matrix( scipy.linalg.sqrtm( self.ConstructZ(proj, I, k, si) ) ) 
            return sqrtZk * Hqp * sqrtZk

    def GetQuasiparticleWeights(self, proj, uM, I, k, si):
        rotated_Z = uM.H*self.ConstructZ(proj, I, k, si)*uM
        return np.diag(rotated_Z)

    def GetQuasiparticleLifetimes(self, proj, uM, I, k, si):
        sig = self.sig.M[np.argmax(self.sig.omega > -1e-10),:,:,:]
        embedded_sig = proj.EmbeddedIntoKBandWindow(sig-self.DC[:,:,:], k, si)
        rotated_sig = uM.H*embedded_sig*uM
        return np.diag(rotated_sig)
    
    # this code is invoked in the Kohn-Sham loop in Rhobusta
    # when it is run with --plus-sigma
    def EmbedSigma(self, sigma_info, ignore_state=False, retain_result=False, final = True):        
        if not ignore_state:
            assert(self.state == 'self-energy-ready' or self.state == 'dft+sigma'), self.state
        
        # increase the super iteration if just finished DMFT 
        if self.state == 'self-energy-ready':
            self.super_iter += 1
            self.sub_iter = 0
        self.sub_iter += 1    
        self.state = 'dft+sigma'

        self.InitOneBody()
        
        # Note: the values here will be valid only if set_g was called before in FlapwMBPT,
        #       which IS the case when we embed sigma
        if self.sub_iter > 1:
            self.SetLatticeOccupation()
         
        # do *NOT* update double counting: use the last one from DMFT
        #self.UpdateDoubleCounting()
        self.StoreMyself()     
        
        mesh = OmegaMesh()  # this is the mesh in Kutepov's code
        res1 = mesh.GetOmegaMesh(sigma_info)
        mesh.load(res1)
        
        #print mesh.index[:]
        
        res = OmegaFunctionInBands(
            min_band = self.orbBandProj.min_band, # start at index 1
            max_band = self.orbBandProj.max_band,
            num_k_irr = self.orbBandProj.num_k,
            n_omega = mesh.n_omega,
            num_si = self.num_si)
        res.allocate()
        res.omega = mesh.omega
        res.index = mesh.index
        
        # set the hybridization object for every omega
        
        M = self.ZeroLocalMatrix(isComplex=True)               
        sig = self.sig
                                
        assert(mesh.index[0] == 0)
        for i in range(mesh.n_omega+1):
            iomega = mesh.index[i]
            w = mesh.omega[i] * Ry2eV
                        
            if iomega < self.num_freqs:
                M[:,:,:] = sig.M[iomega, :,:,:]
            else:
                # beyond the sampled number of points,
                # extrapolate using the calculated moments
                M[:,:,:] = sig.moments[0,:,:,:] - sig.moments[1,:,:,:] / w * 1j 
            
            M =  (M - self.DC) / Ry2eV
            
            for k, si in self.shard_k_si_pairs():
                res.M[:,:,i, k, si] =  self.EmbeddedIntoKBandWindow(M, k, si)
            
        for k, si in self.shard_k_si_pairs():
            res.Minf[:,:,k,si] = self.EmbeddedIntoKBandWindow((sig.moments[0,:,:,:]-self.DC) / Ry2eV, k, si)
                        
        ri = self_energies.LocalOmegaFunctionInfo()

        ri.location = self.GetSigmaLocation(final)+f"full_{res.max_band - res.min_band + 1}"
        ri.store(sigma_info, flush=True)
        res.my_location = ri.location 
        # here we overlay the sigma on the mesh object, since it is a subclass
        res.store(ri.location, flush=True) # note this number is found in dmftStat
        self.StoreStateInHistory()
        if not retain_result:
            del res
            return None
        else:
            return res
         
    def StartMIEmbed(self):
        self.MISigma = self.EmbedSigma(f"./self-energy-{self.plugin.mpi.GetRank()}.core.h5:/info/", 
            retain_result=True, final = False, ignore_state=True)

    def AddMI(self, imp : PEScfEngine) -> bool:
        MISigma = imp.EmbedSigma(f"./self-energy-{imp.plugin.mpi.GetRank()}.core.h5:/info/", 
            retain_result=True, final = False, ignore_state=True)
        self.MISigma.M[...] += MISigma.M[...]
        return True # caller may discard the imp

    def FinalizeEmbed(self):
        pass

    def WriteMIEmbed(self):
        self.MISigma.store(self.MISigma.my_location, flush=True)

    def CorrelatedG(self, w, M, si):
        dim = self.corrSubshell.dim
        corrGw = ZeroComplexMatrix(dim)
        GInvK = ZeroComplexMatrix(self.num_corr_bands)

        for k in range(self.dft.num_k):
            Pk = self.OneAtomProjector(k, si)
            
            GInvK[:,:] = np.diag(w  - self.epsAllK[:, k, si]) - self.EmbeddedIntoKBandWindow(M - self.DC, k, si)
            
            corrGw[:,:] += Pk.H * GInvK.I * Pk * self.lapw.weight[k]

        if self.dft.is_sharded:
            MPI=self.plugin.GetMPIInterface()
            corrGw = MPI.COMM_WORLD.allreduce(corrGw, op=MPI.SUM)

        self.SymmetrizeCorrelated(corrGw, hermitian=False)

        return Matrix(corrGw)

    def PrepareAndRunCTQMC(self):     

        if False in self.orbBandProj.subspace or self.orbBandProj.l < 2:
            explicitU = self.GetUMatrix().flatten()
            Udim = len(explicitU)
        else:
            Udim = 0

        imp = ImpurityProblem(isReal=False,
                              U=self.U,
                              J=self.J,
                              beta=self.beta,
                              subshell=self.corrSubshell,
                              num_si=self.num_si,
                              nrel=self.dft.nrel,
                              num_freqs=self.num_freqs,
                              max_sampling_energy=0.75*self.num_freqs *2*np.pi /self.beta, # in eV. Make this an option?
                              hamiltonian_approximation='ising' if self.ising else 'none',
                              mu=0.0,
                              dim=self.corrSubshell.dim,
                              self_path=self.GetImpurityLocation(),
                              os_dir = f"./Impurity{self.GetSuffixOfImpurity()}{self.GetAdditionalDescriptors()}/",
                              two_body_size = Udim)
        imp.allocate()
        # initialize the impurity object
        imp.hyb = self.NewFreqDepLocalEnergyMatrix()
        if Udim:
            imp.two_body = explicitU
        
        for si in range(self.num_si):
            # this includes off-diagonals
            imp.E[:,:,si] = self.Elocal[:,:,si] - self.DC[:,:,si]
                
            w = 1000.0 * 1j # eV
            print(" - Hybridization matrix at very large omega %f eV:" % w.imag)
            corrG = self.CorrelatedG(w, self.sig.moments[0,:,:,:], si)

            print(self.ID1 * w  - corrG.I - imp.E[:,:,si] - self.sig.moments[0,:,:, si])
                        
            for iomega in range(self.num_freqs):
                w = self.sig.omega[iomega] * 1j
                corrG = self.CorrelatedG(w, self.sig.M[iomega,:,:,:], si)        
                imp.hyb.M[iomega, :,:,si] = \
                  self.ID1 * w  - corrG.I - imp.E[:,:,si] - self.sig.M[iomega, :,:, si]
                  
                if iomega == (self.num_freqs-1) or iomega == 0:                  
                    print("iomega: %d (%f eV) [spin=%d]" % (iomega, w.imag, si))   
                    print(Matrix(imp.hyb.M[iomega, :,:, si]))
                    
        self.hyb = imp.hyb
       
        self.RunCTQMC(imp)
        
    def RunCTQMC(self, imp : ImpurityProblem):        
        self.state = "ctqmc"
        self.StoreMyself() 
        
        imp.store(imp.self_path)
        Store.Singleton().CloseFile(imp.self_path)
        
        self.opts.impurity = imp.self_path
        self.opts.files = False
        self.opts.eval_sim = False

        CTQMC(self.opts)

        self.state = "ctqmc-done"
        
    def UpdateMeasurementTime(self):
        if self.opts.target_error > 0 and self.opts.time_measurement > 0:
            imp = ImpurityProblem()
            imp.load(self.GetImpurityLocation())
            IM = GetMeasurement(imp)  # of type ImpurityMeasurement

            if self.quality < 1e-8 or not hasattr(self, "measurement"):
                if IM.relative_error > self.opts.target_error:
                    fac = 2
                else:
                    fac = 0.5

            else:
                times = [self.measurement_time, self.opts.time_measurement]
                errors = [self.quality, IM.relative_error]

                def f(x,a,b):
                    return a*x**b
                ab, pcov_ = scipy.optimize.curve_fit(f,times,errors)
                time = (self.opts.target_error/ab[0])**(1/ab[1])
                
                fac = time / times[-1]
                if fac > self.opts.maximum_time_change:
                    fac = self.opts.maximum_time_change
                elif fac < 1/self.opts.maximum_time_change:
                    fac = 1/self.opts.maximum_time_change

            self.measurement_time = np.ceil(self.opts.time_measurement)
            self.opts.time_measurement = np.ceil(self.measurement_time*fac)

            if self.plugin.IsMaster():
                print(f" - updating measurement time from {self.measurement_time} to {self.opts.time_measurement} to hit error goal of {self.opts.target_error}")
                SaveOptions(self.opts)      
            

    def UpdateSigAfterCTQMC(self):
        # calculate the new sigma
        assert(self.state == "ctqmc-done")
        imp = ImpurityProblem()
        imp.load(self.GetImpurityLocation())
        IM = GetMeasurement(imp)  # of type ImpurityMeasurement
        
        if self.opts.target_error < 0 or IM.relative_error < self.opts.max_error:
            assert(self.sig.num_omega == IM.Sigma.num_omega)
            self.sig = IM.Sigma
            self.GImp = IM.GImp
            self.RhoImp = IM.RhoImp
            self.Nimp = IM.N # this does not update the DC yet!
            self.NN = IM.NN
            self.energyImp = IM.E     
            self.hist = IM.hist
            self.sign = IM.sign
            self.quality = IM.relative_error
            self.measurement_time = self.opts.time_measurement
                
            if self.plugin.IsMaster(): print("- DMFT energy difference from DFT: ", self.energyImp - self.energyBands)
                        
            self.state = "self-energy-ready"
            self.StoreMyself()
            self.StoreStateInHistory()
            self.UpdateDoubleCounting()  # necessary to unset an initial polarization

        else:
            if self.plugin.IsMaster(): print("- CTQMC error too large as compared to the specified maximum error accetable; Not recording result")
        
        self.sub_iter += 1 #
        
    
    def ContinueAfterCTQMC(self):
        print(" - continuing after CTQMC from iteration %d.%d" % (
            self.super_iter, self.sub_iter))
        
        self.UpdateMeasurementTime()
        self.UpdateSigAfterCTQMC()
        
        
    def LoadSigRealOmega(self, Nw=800, bound=0.6, interp_w=False):
        real_self_energy_file = f"./real-imp-self-energy{self.GetSuffixOfImpurity()}.h5"
        """ Notes:
            defaults here are overridden in the SingleImpurityDelegator and MultiImpurityDelegator classes
        """ 
        
        # if it doesn't exist, assume there is no self energy
        if not os.path.exists(real_self_energy_file):
            print("************************************* IMPORTANT ***************************************************")
            print(" loading constant Σ(ω)==Vdc because %s could not be found" % real_self_energy_file)
            print(" if this is not what you want, make an analytically-continued self-energy (for example with maxent)")
            print("***************************************************************************************************")
            sig = self_energies.LocalOmegaFunction(isReal=True)
            sig.num_omega = Nw
            sig.num_si = self.num_si
            sig.num_orbs=self.corrSubshell.dim
            sig.allocate()
            sig.omega[:] = np.linspace(start=-bound, stop=bound, num=sig.num_omega)[:]
            sig.moments[0,:,:,:] = self.DC[:,:,:]
            for iomega in range(sig.num_omega):
                sig.M[iomega, :,:,:] = self.DC[:,:,:]
            self.sig = sig
            return
            #raise Exception("real self-energy file missing (should be called %s)"%real_self_energy_file)
        
        sig = self_energies.LocalOmegaFunction(isReal=True)
        sig.load(real_self_energy_file+":/")

        sig2 = self_energies.LocalOmegaFunction(isReal=True)
        sig2.num_orbs = sig.num_orbs
        sig2.num_si = sig.num_si
        sig2.allocate()

        if not interp_w:
            sig2.omega = sig.omega[abs(sig.omega) < bound]
            sig2.num_omega = sig2.omega.shape[0]
            sig2.M = sig.M[abs(sig.omega) < bound,:,:,:]
            sig2.moments = sig.moments
        else:
            self.sig = sig # for the interpolation
            sig2.num_omega = Nw
            sig2.omega = np.linspace(start=-bound, stop=bound, num=Nw)
            ise = InterpolatedSelfEnergy(self, mesh=sig2.omega[:], add_dc=False, addHyb=False, debug=False)
            sig2.M = ise.GetLocalOmegaFunction()
            sig2.moments = sig.moments

        # add moments
        for iomega in range(sig2.num_omega):
            sig2.M[iomega, :,:,:] += sig.moments[0,:,:,:]
            
        self.sig = sig2

        self.real_axis_sig = True

    def CalculateRealHybridization(self):
        """
            Complete the hybridization on the real axis using the real self energy (which should
            have been loaded before), and the DMFT equation
        """
        self.hyb = self_energies.LocalOmegaFunction(num_si=self.sig.num_si, 
            num_orbs=self.sig.num_orbs, num_omega=self.sig.num_omega)
        self.hyb.allocate()
        self.omega = np.copy(self.sig.omega)
        self.hyb.M[...] = 0.0
        impE = np.zeros_like(self.Elocal)

        for si in range(self.num_si):
            impE[:,:,si] = self.Elocal[:,:,si] - self.DC[:,:,si]
                
            for iomega in range(self.hyb.num_omega):
                w = self.hyb.omega[iomega] #real
                corrG = self.CorrelatedG(w, self.sig.M[iomega,:,:,:], si)        
                self.hyb.M[iomega, :,:,si] = \
                  self.ID1 * w  - corrG.I - impE[:,:,si] - self.sig.M[iomega, :,:, si]

                
    def LoadMyself(self):
        is_new = False
        try:
            self.load(self.GetTopLevelMethodName(), checkExists=True)
        except:
            is_new = True

        return is_new
                    
    def StoreMyself(self):
        self.self_path = self.GetTopLevelMethodName()
        index = self.self_path.find(".h5")
        self.self_path = self.self_path[:index] + self.GetAdditionalDescriptors() + self.self_path[index:]
        
        if not self.dry_run and self.plugin.IsMaster():
            self.store(self.self_path, flush=True)
            MultiImpurityEmbedder.StoreImpurityDetails(self, self.opts)
        else:
            Persistent.Refresh(self.self_path)
        
           
    def StoreStateInHistory(self):
        if not self.dry_run and self.plugin.IsMaster():
            self.PreserveHistory()
            if self.state == 'dft+sigma':
                sos = SolverState()
                # don't store sig, hyb and Gimp - just the lightweight solver state
                for name, value in vars(self).items():
                     setattr(sos, name, value)
                sos.hist.num_configs = 0 # don't save whole histogram
                sos.store(f"./history.h5:/impurity{self.GetSuffixOfImpurity()}/{self.super_iter}/{self.sub_iter}/", flush=True)
            else:
                self.store(f"./history.h5:/impurity{self.GetSuffixOfImpurity()}/{self.super_iter}/{self.sub_iter}/", flush=True)
     
        
    def CalculateZValues(self, print_noninter:bool =False):
        from pylatexenc.latex2text import LatexNodes2Text

        se = self.sig
        self.CalculateRealHybridization()
        delta = 2.0e-4
        numMesh = max(int(1/self.beta / delta), 2)
        mesh = np.array([i*delta  for i in range(-numMesh,numMesh+1)])
        ise = InterpolatedSelfEnergy(self, mesh=mesh, addHyb=self.opts.add_hyb)
        ofs = ise.GetOrbitalFunctions()

        vals_, orbs = self.GetLabelsForPlotting(self)
        
        indices = self.GetRepOrbitals(self.CorrectRep())   
        if self.num_si == 1:
            indices =  [(0, i) for i in indices]
        else:
            # this is the case of completely symmetric representation. Change when we generalize.
            indices = [(0, i) for i in indices] + [(1,i) for i in indices]   

        deltaR = se.omega[1]-se.omega[0]
        for i,val in enumerate(se.omega[:]):
            if val >=0:
                assert(i>0)
                assert(se.omega[i-1]<=0)
                deltaR = se.omega[i] - se.omega[i-1]
                break

        self.correctedRep = self.CorrectRep()
        Zraw = {}
        for si, (iorb,jorb) in indices:
            M = np.real(se.M[:, iorb, jorb,si])
            if self.opts.add_hyb: M += np.real(self.hyb.M[:, iorb, jorb,si])
            slope = (np.real(M[i] - M[i-1]))/deltaR
            rr = self.correctedRep[iorb,jorb]
            Zraw[rr-1] = 1.0/(1.0-slope)

        # now complete the values of Z in the dmft from the analytically continued self energy
        self.Z = np.copy(self.RhoImp)
        self.Z[...] = 0.0


        def CheckIfImaginaryGoodFL(sig):
            import statistics
            zeroIdx = 0
            orbs = range(sig.shape[0])
            isGood = ["Bad" for _o in orbs]
            sig0 = [0.0 for _o in orbs]
            for idx,orb in enumerate(orbs):
                vals = []
                for i in range(sig.shape[1]):
                    if abs(mesh[i]) < 1.0e-8:
                        zeroValue = sig[orb, i]
                        zeroIdx = i
                        break
                    
                n2 = mesh[-1] * mesh[-1] / abs(sig[orb, -1]-sig[orb,zeroIdx]) # normalization, > 0
                
                for i in range(sig.shape[1]):
                    if i == zeroIdx: continue
                    vals.append((sig[orb,i] - sig[orb,zeroIdx]) / mesh[i] / mesh[i] *  n2) # allow for small error at 0

                sig0[idx] = -sig[orb,zeroIdx]

                var = statistics.variance(vals)
                mean = statistics.mean(vals)
                if abs(zeroValue) < 0.1: # the other conditions don't seem to be relevant: and abs(var)<2.0 and mean < 0:
                    isGood[idx] = "Good"

                if self.opts.plots:
                    print(f"Im sigma(0): {zeroValue}, variance: {var}, mean: {mean}")
                    import matplotlib.pyplot as plt
                    import matplotlib
                    matplotlib.use("TkAgg")

                    plt.plot(mesh, sig[orb, :]-sig[orb,zeroIdx], label="Im(sig)")
                    plt.plot(mesh, [-x*x / n2 for x in mesh], "-", label="square")
                    plt.legend()
                    plt.show()

            return zeroIdx, isGood, sig0

        for si in range(self.num_si):
            zeroIdx, isGood, zeroValue = CheckIfImaginaryGoodFL(np.imag(ofs[:,si,:]))
            slope = (np.real(ofs[:,si,zeroIdx+1] - ofs[:,si,zeroIdx]))/delta
            Z=1.0/(1.0-slope)
            print("Σ(ω) (analytically continued) estimate of Zs: ")
            for oi, orb in enumerate(orbs):
                if print_noninter:
                    print(f"Z ({LatexNodes2Text().latex_to_text(orb)}):\t\t\t{Z[oi]:.5f} {isGood[oi]} ({zeroValue[oi]:3.3f})\t\t\tnon-interpolated:\t{Zraw[oi]:.5f}")
                else:
                    print(f"Z ({LatexNodes2Text().latex_to_text(orb)}):\t\t\t{Z[oi]:.5f} {isGood[oi]} ({zeroValue[oi]:3.3f})")
                    
            for i in range(self.dim):
                self.Z[i, i,si] = Z[self.correctedRep[i,i]-1]

    #From ComCTQMC evalsim/partition/Functions.C add_self_tail
    def GetSelfEnergyFittingParams(self):
        alpha = np.zeros_like(self.sig.M[0,:,:,:])
        gamma = np.zeros_like(alpha)

        nFit = max(int(self.sig.num_omega/8),1)
        for (i,j) in product(range(self.corrSubshell.dim), repeat=2):
            if self.rep[i,j] != 0:
                for si in range(self.num_si):
                    
                    iomegaAvg = 0
                    selfPosAvg = 0
                    selfNegAvg = 0
                    for om in range(self.sig.num_omega-nFit, self.sig.num_omega):
                        iomegaAvg += 1j*self.sig.omega[om]
                        selfPosAvg += self.sig.M[om,i,j,si]
                        selfNegAvg += np.conj(self.sig.M[om,j,i,si])

                    iomegaAvg /= float(nFit)
                    selfPosAvg /= float(nFit)
                    selfNegAvg /= float(nFit)

                    alpha[i,j,si] = (-self.sig.moments[1,i,j,si]/2.*
                        (1./(selfPosAvg - self.sig.moments[0,i,j,si]) + 1./(selfNegAvg - self.sig.moments[0,i,j,si])))
                    gamma[i,j,si] = (iomegaAvg*self.sig.moments[1,i,j,si]/2.*
                        (-1./(selfPosAvg - self.sig.moments[0,i,j,si]) + 1./(selfNegAvg - self.sig.moments[0,i,j,si])) + iomegaAvg*iomegaAvg)

        return alpha, gamma

    def ResizeAll(self, num_omega):
        self.InitOneBody()
        self.ResizeSelfEnergy(num_omega)
        self.ResizeHyb(num_omega)
        self.ResizeGImp(num_omega)

    def ResizeHyb(self, num_omega):
        if num_omega == self.hyb.num_omega:
            return

        elif num_omega < self.hyb.num_omega:
            self.num_freqs = num_omega
            self.hyb.num_omega = num_omega
            self.hyb.omega = self.hyb.omega[:num_omega]
            self.hyb.M =  self.hyb.M[:num_omega, :, :, :]

        else:
            self.GetHybridizationMoment()
            self.num_freqs = num_omega 
            new_hyb = self.NewFreqDepLocalEnergyMatrix()
            new_hyb.M[:self.hyb.num_omega,:,:,:] = self.hyb.M[:,:,:,:]
            new_hyb.moments = self.hyb.moments
            
            tail_ix = 1j * new_hyb.omega[self.hyb.num_omega:]
            for si in range(self.num_si):
                for i,j in product(range(self.dim), repeat=2):
                    if self.rep[i,j] != 0:
                        new_hyb.M[self.hyb.num_omega:,i,j,si] = self.hyb.moments[0,i,j,si]/(tail_ix - self.hyb.moments[1,i,j,si])
            
            self.hyb = new_hyb

    def ResizeGImp(self, num_omega):
        if num_omega == self.GImp.num_omega:
            return

        elif num_omega < self.GImp.num_omega:
            self.num_freqs = num_omega
            self.GImp.num_omega = num_omega
            self.GImp.omega = self.GImp.omega[:num_omega]
            self.GImp.M =  self.GImp.M[:num_omega, :, :, :]
        
        else:
            if self.hyb.num_omega < num_omega:
                self.InitOneBody()
                self.ResizeHyb(num_omega)
            self.num_freqs = num_omega 
            new_GImp = self.NewFreqDepLocalEnergyMatrix()
            new_GImp.M[:self.GImp.num_omega] =  self.GImp.M[:, :, :, :]
            new_GImp.moments = self.GImp.moments

            impE = np.zeros_like(self.Elocal)
            for si in range(self.num_si):
                impE[:,:,si] = self.Elocal[:,:,si] - self.DC[:,:,si]
                    
                for iomega in range(self.GImp.num_omega, num_omega):
                    w = 1j*self.hyb.omega[iomega]
                    
                    Ginv = Matrix(self.ID1 * w - self.hyb.M[iomega,:,:,si] - impE[:,:,si] - self.sig.M[iomega, :,:, si])

                    new_GImp.M[iomega,:,:,si] = Ginv.I

            self.GImp = new_GImp
            
    def ResizeSelfEnergy(self, num_omega):
        if num_omega == self.sig.num_omega:
            return

        elif num_omega < self.sig.num_omega:
            self.num_freqs = num_omega
            self.sig.num_omega = num_omega
            self.sig.omega = self.sig.omega[:num_omega]
            self.sig.M =  self.sig.M[:num_omega, :, :, :]

        else:
            alpha, gamma = self.GetSelfEnergyFittingParams()
            self.num_freqs = num_omega 
            new_sig = self.NewFreqDepLocalEnergyMatrix()
            new_sig.M[:self.sig.num_omega,:,:,:] = self.sig.M[:,:,:,:]
            new_sig.moments = self.sig.moments
            for om in range(self.sig.num_omega,num_omega):
                new_sig.M[om,:,:,:] = new_sig.moments[0,:,:,:] + new_sig.moments[1,:,:,:]/(1j*new_sig.omega[om] - alpha[:,:,:] - gamma[:,:,:]/(1j*new_sig.omega[om]))
            
            self.sig = new_sig

    def AllCorrelatedG(self):
        corrG = self.NewFreqDepLocalEnergyMatrix()
        for si in range(self.num_si):
            for iomega in range(self.num_freqs):
                w = self.sig.omega[iomega] * 1j
                corrG.M[iomega, :,:, si] = self.CorrelatedG(w, self.sig.M[iomega,:,:,:], si)
        
        return corrG

    def PlotGlocAndGimp(self):
        import matplotlib
        matplotlib.use("TkAgg")
        import matplotlib.pyplot as plt
        from matplotlib import cm
        
        imp = ImpurityProblem()
        imp.load(self.GetImpurityLocation())
        IM = GetMeasurement(imp)

        self.InitOneBody()
        corrG = self.AllCorrelatedG()    
        
        fig, ax = plt.subplots(2,1)

        indices = DMFTOrchastrator.GetRepOrbitals(self.CorrectRep(), diagonal = False)  
        colors = cm.rainbow(np.linspace(0, 1, self.num_si*self.dim))

        first = True
        for si in range(self.num_si):
            for i,j in indices:
                if first:
                    ax[0].plot(self.sig.omega, np.real(corrG.M[:,i,i,si]), color = colors[si*self.dim+i], label = r"$G_{loc}$")
                else:
                    ax[0].plot(self.sig.omega, np.real(corrG.M[:,i,i,si]), color = colors[si*self.dim+i])
                ax[1].plot(self.sig.omega, np.imag(corrG.M[:,i,i,si]), color = colors[si*self.dim+i], label = f"rep={self.rep[i,j]}")

                if first:
                    ax[0].plot(self.sig.omega, np.real(IM.GImp.M[:,i,i,si]), color = colors[si*self.dim+i], linestyle=":", label = r"$G_{imp}$")
                else:
                    ax[0].plot(self.sig.omega, np.real(IM.GImp.M[:,i,i,si]), color = colors[si*self.dim+i], linestyle=":")
                ax[1].plot(self.sig.omega, np.imag(IM.GImp.M[:,i,i,si]), color = colors[si*self.dim+i], linestyle=":")

                first = False

        ax[0].set_ylabel(r"Re$[G]$ (eV)")
        ax[1].set_ylabel(r"Im$[G]$ (eV)")
        ax[1].set_xlabel(r"$i\omega$ (eV)")

        fig.legend()

        plt.show()
                
    def self_consistency(self):
        self.InitOneBody()
        corrG = self.AllCorrelatedG()   

        imp = ImpurityProblem()
        imp.load(self.GetImpurityLocation())
        IM = GetMeasurement(imp)

        indices = DMFTOrchastrator.GetRepOrbitals(self.CorrectRep(), diagonal = False)  

        diff = IM.GImp.M - corrG.M
        relative_err = np.zeros(self.sig.num_omega, dtype=float)
        for si in range(self.num_si):
            for i,j in indices:
                relative_err += np.abs(np.real(diff[:,i,j,si])/np.real(corrG.M[:,i,j,si])) + np.abs(np.imag(diff[:,i,j,si])/np.imag(corrG.M[:,i,j,si])) #sum over everything but frequency first

        weighted_relative_error = relative_err/(self.sig.omega+1)

        return np.sum(weighted_relative_error)

    def is_done(self):
        if self.opts.dmft_conv > 0: 
            err = self.self_consistency()
            print(" --------------------------------------- ")
            print(f" - DMFT self consistency error is {err}")
            print(" --------------------------------------- ")
            self.PlotGlocAndGimp()
            return err < self.opts.dmft_conv
        else:
            return self.sub_iter > self.opts.num_impurity_iters
            
    def dispatch(self, cont):
        state = self.state
        
        print(state)
        if not cont:
            if state == "start":
                print("---> Starting CTQMC 1st iteration")
                self.sub_iter = 1
                self.InitOneBody()
                self.UpdateDoubleCounting(is_new=True)
                #self.DC[:,:] = self.ID * 0.0
                self.StoreMyself()
                self.PrepareAndRunCTQMC()
                self.ContinueAfterCTQMC()
            else:
                print(f"---> Starting CTQMC major iteration {self.super_iter+2}")
                self.super_iter += 2
                self.sub_iter = 1
                
                self.InitOneBody()  # with new band structure
                self.UpdateDoubleCounting()
                # TODO: do we want to update the double counting or not ??
                self.StoreMyself()
                self.PrepareAndRunCTQMC()
                self.ContinueAfterCTQMC()
        else:
            if state == "ctqmc-done": #interrupted before measurements were saved
                print(f"---> continuing after CTQMC iteration {self.super_iter+1}.{self.sub_iter+1}")
                #self.InitOneBody()
                self.ContinueAfterCTQMC()
            elif state == "ctqmc":  #restart
                print(f"---> restarting CTQMC")
                self.PrepareAndRunCTQMC()
                self.ContinueAfterCTQMC()
            elif state == "self-energy-ready":
                print(f"---> running CTQMC iteration {self.super_iter+1}.{self.sub_iter+1}")
                self.PrepareAndRunCTQMC()
                self.ContinueAfterCTQMC()
         
    def GetImpurityLocation(self):
        return self.GetTopLevelMethodName()+"impurity/" # save only one verision - everything else is saved in history
         
        
@with_organizer
def DMFT(opts):
    DMFTMain([], {}, opts=opts)

def DMFTOptionsParser(prepArgs, add_help=False, private=False):
        
    parser = ArgumentParserThatStoresArgv('dmft', add_help=add_help ,
                            parents=[GetPEScfOptionsParser('dmft', prepArgs), CTQMCOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
 
    parser.add_argument("-D", "--imp-iterations", dest="num_impurity_iters", 
                     default=6, type=int,
                     help="number of iterations to solve the DMFT self-consistency condition")

    parser.add_argument("-e", "--energy-limit", dest="energy_limit", default=15.0, type=float,
                     help="energy limit for self-energy frequency")
    
    if private:
        parser.add_argument("-w", "--energy-window", dest="window",
                            help="energy window for the cubic_harmonicsands (in eV)",
                            default='-10.0:10.0')
     
    parser.add_argument("--dmft-dry-run", dest="dry_run",
                  action="store_true",
                  default=False,
                  help="Dry run - don't call ImpuritySolver") 

    parser.add_argument("--examine-convergence", dest="examine_convergence",
                  action="store_true",
                  default=False,
                  help="Plot Gloc and Gimp") 
  
    eg = parser.add_argument_group(title="Automation of CTQMC measurement times",
                                   description="""Instead of supplying a CTQMC measurement time, DMFT can search for the ideal time.
                                                  These parameters control this search""")

    eg.add_argument("--target-error", dest="target_error",
                  type=float,
                  default=-1,
                  help="If positive, then adapt the measurement time in an effort to reach this error") 

    eg.add_argument("--maximum-time-change-factor", dest="maximum_time_change",
                  type=float,
                  default=5,
                  help="If adapting the measurement time, only change the measurement time by at most a factor of this or 1/this") 

    eg.add_argument("--maximum-accepted-error", dest="max_error",
                  type=float,
                  default=1,
                  help="If adapting the measurement time, reject measurements with an error greater than this") 
                  
    eg = parser.add_argument_group(title="Automation of DMFT iterations",
                                   description="""Instead of supplying a number of iterations""")  

    eg.add_argument("--dmft-convergence-threshold", dest="dmft_conv",
                    type=float,
                    default=-1,
                    help=r"""If positive, stop the DMFT loop when $\sum\{|G_{imp}-G_{loc}|/[|G_{loc}|(\omega+1)]\}$ < eps.
                             Suggestions: 1 = extremely converged (may not converge ever)\n
                                          5 = moderately converged\n
                                         10 = not very converged\n
                             If negative, run for set number of iterations""")
  
    return parser


def DMFTMain(args, kwargs, opts=None):
    
    if opts is None:
        parser = DMFTOptionsParser(args, add_help=True, private=True)    
        opts, _args = parser.parse_known_args(args)
        SaveOptions(opts)
    
    for key in kwargs:
        setattr(opts, key, kwargs[key])
    
    
    logging.basicConfig()
    #logging.getLogger().setLevel(logging.INFO)
    logging.info("running in directory "+ os.path.abspath("."))
    
    def handler(_signum, _unused):
        print(" .. saving all buffers before exit")
        Store.FlushAll()  # make sure we do nothing. we handle signals in Fortran
        print(" - exiting.")
        sys.exit(0)
        
    signal.signal(signal.SIGINT, handler)
    
    if opts.target_error > 0:
        assert opts.mpi_workers > 1, "Need CTQMC error estimates which requires multiple workers"
        

    mpi = MPIContainer(opts)
    _plugin = DFTPlugin.Instance(restart=True, mpi=mpi)
    
    orc = DMFTOrchastrator(opts,
                        dry_run=opts.dry_run)
    
    if opts.examine_convergence:
        orc.PlotGlocAndGimp()
        return

    orc.dispatch(cont=False)
   
    if opts.pre_process:
        return

    if opts.analysis_only:
        orc.dispatch(cont=True)
        return
   
    if opts.dry_run:
        return
    
    i = 0
    while True:
        i+=1
        RefreshOptions(opts)
        if orc.is_done(): break
        
        print("Impurity solver iteration #%d" % (i+1))
        orc.dispatch(cont=True)

if __name__ == '__main__':
    DMFTMain(sys.argv[1:], {})
    
