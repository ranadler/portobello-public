#!/usr/bin/env python3

from argparse import ArgumentDefaultsHelpFormatter
import numpy as np

from portobello.generated.PEScf import SolverState, Histogram
from portobello.generated.models import ModelState
from portobello.generated.LAPW import Basis

from portobello.bus.Matrix import Matrix
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.PEScfEngine import PEScfEngine, SingleImpurityDelegator, Ry2eV
from portobello.rhobusta.models.ProjectorEmbedder import Model, ModelProjector, ModelKPathProjector
from portobello.rhobusta.models.BandModels import ModelPlugin

def ModelOptionsParser(add_help=False, private=False):
        
    parser = ArgumentParserThatStoresArgv('models', add_help=add_help ,
                parents=[],
                formatter_class=ArgumentDefaultsHelpFormatter)    
 

    mg = parser.add_argument_group(title="Lattice Model Options",
                              description="""Describe the lattice model""")

    mg.add_argument("--model-type", dest="model_type",
                  default="semicircular",
                  help="Type of model",
                  choices=["semicircular", "wannier90", "flat", "tight binding"]) 

    mg.add_argument("--model-bands", dest="model_bands",
                  default=1,
                  type=int,
                  help="Number of bands") 

    mg.add_argument("--model-mu", dest="model_mu",
                  default=0,
                  type=float,
                  help="Chemical potential") 

    mg.add_argument("--model-halfwidth", dest="model_halfwidth",
                  default=1,
                  type=float,
                  help="halfwidth of the model bands (semicircular / flat)") 

    mg.add_argument("--model-num-energy-bins", dest="model_num_bins",
                  default=101,
                  type=int,
                  help="number of energy bins in the density of states") 

    mg.add_argument("--wannier-correlated-orbs", dest="wannier_correlated_orbs",
                  default="",
                  help="Dictionary giving names and indices of correlated orbitals in wannier model") 

    return parser


class ModelSingleImpurityDelegator(SingleImpurityDelegator):
    
    def GenerateKPathProjectors(self, svs, kpath):
        svs.orbBandProj = ModelKPathProjector(BZProjector=svs.orbBandProj, kpath=kpath) 
        return svs.orbBandProj

    def ComputeInverseGreensFunction(self, svs, w, Hk, muChoice, epsic, proj, MM, k, si):
        return np.eye(Hk.shape[0])*(w + epsic)+ (1.0-muChoice)*svs.mu[:,:, k, si] - Hk[:,:, k, si]  -\
                        proj.EmbeddedIntoKBandWindow(MM ,k,si)
    
    def ComputeMidEpsK(self, w, Hk, mu, epsi, k, si):
        e, v = np.linalg.eigh(Hk[:,:,k,si])
        return w -  e[:] + mu + epsi

class PEScfEngineForModels(PEScfEngine):
    
    def GetPlugin(self):
        return self.plugin

    def GetSuffixOfImpurity(self):
        return ""

    def UpdateDoubleCounting(self, is_new=False):
        pass

    @classmethod
    def GetLabelsForPlotting(cls, state : SolverState):
        rep = np.diag(state.rep)
        return [[str(r) for r in rep]], [str(r) for r in np.unique(rep)]

    def __init__(self, opts, mpi):
        self.opts = opts
        self.mpi_workers = opts.mpi_workers
        self.dry_run = False

        #We cannot just make ModelState a parent
        #also saving a ModelState file lets us check if we're dealing with a model
        #in analysis scripts without providing --model
        self.ms = ModelState()
        
        SolverState.__init__(self)

        is_new = self.LoadMyself()   
        if is_new:
            self.ms.model_type = opts.model_type
            self.mu = opts.model_mu #in eV
            self.ms.store("./model.h5:/")
        else:
            try:
                self.ms.load("./model.h5:/")
            except:
                print("Missing model.h5 -- using defaults")
                self.ms.store("./model.h5:/")
        if self.ms.model_type == "":
            self.ms.model_type = "wannier90"
            
        self.plugin = ModelPlugin(10, mpi, self.ms.model_type, self.mu, opts)
        self.corrSubshell = Model(opts, self.plugin, is_new)
        self.orbBandProj = ModelProjector(self.corrSubshell, self.plugin)
        self.lapw = Basis(num_k_all = self.orbBandProj.num_k_all,
                            num_k_irr = self.orbBandProj.num_k_all,
                            num_k_shard = self.orbBandProj.num_k_all,
                            num_si = 1)
        self.lapw.allocate()
        self.lapw.weight[:] = np.ones(self.lapw.num_k_irr, dtype=float) / self.lapw.num_k_irr

        self.num_equivalents = self.corrSubshell.num_equivalents
        self.num_anti_equivalents = self.corrSubshell.num_anti_equivalents

        if is_new:
            KB = 6.333328e-6 *27.2107* 0.5 # collected from Kutepov's code, to match perfectly
            
            if opts.Temperature is not None and opts.beta is None:
                opts.beta = 1/(opts.Temperature * KB)
            if opts.Temperature is None:
                opts.Temperature = 1/opts.beta / KB
            assert(opts.Temperature is not None)
            self.beta = opts.beta

            self.dim = self.corrSubshell.dim
            self.full_dim = self.dim*2
            self.num_si = 1
            self.which_shell=0
            self.allocate()
                
            self.U = float(opts.U)
            self.J = float(opts.J)

            self.nominal_dc = self.HeldDoubleCounting(opts.N0)
            self.fixed_DC = True
            self.DC = self.nominal_dc * self.ID

            print(" - double counting:")
            for si in range(self.num_si):
                PrintMatrix(self.DC[:,:,si])

            self.hist = Histogram(num_configs=2**self.full_dim)
            self.hist.allocate()
            self.RhoImp = self.ID

            self.ising = False
            if hasattr(opts, 'ising'):
                self.ising = opts.ising

            self.kanamori = True

        self.rep = self.corrSubshell.rep
        self.norb2 = self.dim * 2

        self.impurity_delegator = self.GetImpurityDelegator()
        self.dft = self.plugin

        self.num_corr_bands = self.orbBandProj.max_band - self.orbBandProj.min_band + 1
        self.window_min_band = self.orbBandProj.min_band-1
        self.window_max_band = self.orbBandProj.max_band-1
        self.InitSymmetrization()
        return is_new

    def GetImpurityDelegator(self):
        return ModelSingleImpurityDelegator()

    def InitOneBody(self):        
        self.Elocal =  self.ZeroLocalMatrix()

        if self.dim == 1:
            self.Elocal[:,:,:] = np.dot(self.plugin.dos[:,0], self.plugin.dos_energy[:]) - self.plugin.mu_ev

        else:
            self.energyBands = 0.0
            
            BS = self.orbBandProj.GetBandStructureInWindow()
            Hk = (BS.energy - BS.chemical_potential) * Ry2eV 

            si=0
            for k in range(self.orbBandProj.num_k_all):    
                Pk = self.OneAtomProjector(k, si)
                self.Elocal[:,:,si] += Pk.H * Matrix(Hk[:,:,k,si]) * Pk
            self.Elocal[:,:,si] /= self.orbBandProj.num_k_all

        self.SymmetrizeCorrelated(self.Elocal, hermitian=True, raw=True)

        if self.plugin.IsMaster():
            print("- local H:")
            self.PrintMatrix(self.Elocal[:,:,0])