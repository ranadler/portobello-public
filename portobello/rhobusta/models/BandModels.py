
import numpy as np
from pymatgen.core.structure import Structure
from pymatgen.symmetry.bandstructure import HighSymmKpath
import pickle
import os
import scipy.integrate as scintegrate

from portobello.generated.LAPW import KPath, BandStructure

from portobello.bus.Matrix import Matrix
from portobello.rhobusta.bandstructure_utilities import CreateKPath
from portobello.rhobusta.spectral_plotting import plot_bands
from portobello.rhobusta.InputBuilder import GetRefinedStructureWithSGAAndOps
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.PEScfEngine import Ry2eV

class TightBindingModel:

    def __init__(self, structure, nfun, nk, hopping_dict, dim=3):
        self.nfun = nfun
        self.nk = nk
        self.hopping_dict = hopping_dict
        self.dim = dim

        self.st = structure
        
    def compute_Hk(self, klist):
        Hk = np.zeros(( self.nfun, self.nfun,len(klist),1), dtype=np.complex)
        for i, k in enumerate(klist):
            for r in self.hopping_dict.keys():
                Hk[:,:,i,0] += np.exp(1j*2*np.pi*np.dot(k,r)) * self.hopping_dict[r]["h"][:,:] / self.hopping_dict[r]["deg"]
        self.num_k = len(klist)
        return Hk

    def compute_epsK(self, klist):
        Hk = self.compute_Hk(klist)
        ek = np.zeros(( self.nfun,len(klist),1), dtype=np.complex)
        for i, k in enumerate(klist):
            e, v = np.linalg.eigh(Hk[:,:,i,0])
            ek[:,i,0] = e[:]
        return ek

    def compute_bands(self, opts):
        hskp = HighSymmKpath(self.st, symprec=0.001, angle_tolerance=opts.spacegroup_angle_tolerance)
        kpath, labels = CreateKPath(hskp, opts, cartesian = False)
        kpath = np.swapaxes(kpath.k,0,1)
        if self.dim == 2:
            kpath=kpath[:,:2]

        Hk = self.compute_Hk(kpath)
        num_k = len(kpath)
        energies = np.zeros((len(labels), self.nfun, 1))
        for ik in range(num_k):
            energies[ik,:,0], v_ = np.linalg.eigh(Hk[:,:,ik,0])

        fig, ax = plot_bands(labels,energies,energies-100,opts)
        pickle.dump(fig,open(f'tight-binding-bands.pickle','wb'))

        return energies

    def compute_dos(self,ne,kmesh):
        Hks = self.compute_Hk(kmesh)
        num_k = len(kmesh)

        omegas = np.linspace(-5,5,ne)
        eta=0.1

        dos = np.zeros((ne, self.nfun))
        for iw, om in enumerate(omegas):
            
            for ik in range(num_k):
                Ak = -1/np.pi * np.imag( np.linalg.inv( (om+1j*eta)*np.eye(self.nfun)-Hks[:,:,ik,0]) ) 
                dos[iw,:] += np.diag(Ak)

        return dos/len(kmesh), omegas
   
class SimpleTightBindingModel(TightBindingModel):

    def __init__(self, opts):
        import json
        with open("tb.json") as f:
            data = json.load(f)
        
        jlattice = Matrix(data["lattice vectors"])
        nk = int(data["nk"])
        nfun = 1

        dim = jlattice.shape[0]

        if dim == 3:
            lattice = jlattice
        elif dim == 2:
            lattice = np.zeros((3,3), dtype=float)
            lattice[:2,:2] = jlattice[:,:]
            lattice[2,2] = 1

        st = Structure(lattice, 
                    [1], #species
                    [[0,0,0]], # 
                    coords_are_cartesian=True)
        
        hopping_dict = {}
        for i, vector in enumerate(data["hopping vectors"]):
            vector = [float(v) for v in vector]
            if dim == 2:
                vector += [0]
            vector = (vector[0], vector[1], vector[2])
            hopping_dict[vector] = {}
            hopping_dict[vector]["h"] = np.eye(1)*float(data["hopping integrals"][i])
            hopping_dict[vector]["deg"] = 1

        TightBindingModel.__init__(self, st, nfun, nk, hopping_dict, dim = dim)
         
class Wannier90TightBindingModel(TightBindingModel):

    def parse_hopping_from_wannier90_hr_dat(self, filename): #from Tsung-Han
        # read in hamiltonian matrix, in eV
        f=open(filename,"r")
        ln=f.readlines()
        f.close()
        #
        # get number of wannier functions
        num_wan=int(ln[1])
        # get number of Wigner-Seitz points
        num_ws=int(ln[2])
        # get degenereacies of Wigner-Seitz points
        deg_ws=[]
        for j in range(3,len(ln)):
            sp=ln[j].split()
            for s in sp:
                deg_ws.append(int(s))
            if len(deg_ws)==num_ws:
                last_j=j
                break
            if len(deg_ws)>num_ws:
                raise Exception("Too many degeneracies for WS points!")
        deg_ws=np.array(deg_ws,dtype=int)
        # now read in matrix elements
        # Convention used in w90 is to write out:
        # R1, R2, R3, i, j, ham_r(i,j,R)
        # where ham_r(i,j,R) corresponds to matrix element < i | H | j+R >
        ham_r={} # format is ham_r[(R1,R2,R3)]["h"][i,j] for < i | H | j+R >
        ind_R=0 # which R vector in line is this?
        for j in range(last_j+1,len(ln)):
            sp=ln[j].split()
            # get reduced lattice vector components
            ham_R1=int(sp[0])
            ham_R2=int(sp[1])
            ham_R3=int(sp[2])
            # get Wannier indices
            ham_i=int(sp[3])-1
            ham_j=int(sp[4])-1
            # get matrix element
            ham_val=float(sp[5])+1.0j*float(sp[6])
            # store stuff, for each R store hamiltonian and degeneracy
            ham_key=(ham_R1,ham_R2,ham_R3)
            if (ham_key in ham_r)==False:
                ham_r[ham_key]={
                    "h":np.zeros((num_wan,num_wan),dtype=complex),
                    "deg":deg_ws[ind_R]
                    }
                ind_R+=1
            ham_r[ham_key]["h"][ham_i,ham_j]=ham_val
            #if (ham_key in ham_r)==False:
            #    ham_r[ham_key]=np.zeros((num_wan,num_wan),dtype=complex)
            #    ind_R+=1
            #ham_r[ham_key][ham_i,ham_j]=ham_val/deg_ws[ind_R]
        return ham_r, num_wan

    def __init__(self, opts):
        
        import glob
        hr_files = glob.glob("./*_hr.dat")
        assert len(hr_files)==1, f"found too many wannier90 tb hamiltonian files: {hr_files}"

        if os.path.exists(hr_files[0]):
            ham_r, num_wan = self.parse_hopping_from_wannier90_hr_dat(hr_files[0])

        st_files = glob.glob("./*.struct") + glob.glob("./*.cif") + glob.glob("./POSCAR")  
        assert len(st_files)==1, f"found too many structure files: {st_files}"
        
        print(" - Loading structure and refining to primitive")
        opts.use_given_cell = False
        opts.spacegroup_angle_tolerance=0.005
        st, sga_, ops_, extra_  = GetRefinedStructureWithSGAAndOps(st_files[0], opts)
        
        nk = 10
        TightBindingModel.__init__(self, st, num_wan, nk, ham_r)

#Changes Bandstructure -- Hk no longer assumed diagonal
class ModelBandStructure(BandStructure):

    def __init__(self, num_k, num_bands, mu, num_si=1, is_sharded = False):
        BandStructure.__init__(self)
        self.num_k = num_k
        self.num_expanded = num_bands
        self.num_si = num_si
        self.is_sharded = is_sharded
        self.allocate()
        
        #These are different sizes! A bit of a faux pas... TODO: Handle more transparently thru schema?
        self.energy = np.zeros((self.num_expanded, self.num_expanded, self.num_k, self.num_si), dtype=float)
        self.chemical_potential = np.zeros((self.num_expanded, self.num_expanded, self.num_k, self.num_si), dtype=float)
        for k in range(self.num_k):
            for si in range(self.num_si):
                self.chemical_potential[:,:,k,si] = np.eye(self.num_expanded)*mu

#As opposed to ModelBandStructure, maintains the BandStructure sizes
class DiagonalModelBandStructure(BandStructure):

    def __init__(self, num_k, num_bands, mu, num_si=1, is_sharded = False):
        BandStructure.__init__(self)
        self.num_k = num_k
        self.num_expanded = num_bands
        self.num_si = num_si
        self.is_sharded = is_sharded
        self.allocate()
        self.chemical_potential = mu
        

class ModelPlugin(DFTPlugin):

    def __init__(self, nk, mpi, model_type, chemical_pot, opts):
        self.mpi = mpi 
        self.model_type = model_type
        self.mu = chemical_pot/Ry2eV #in Ry
        self.mu_ev = chemical_pot
        self.opts = opts
        if self.model_type == "semicircular":
            self.semicircular()
        elif self.model_type == "flat":
            self.flat()
        elif self.model_type == "wannier90":
            print(" - Loading Wannier90 model")
            self.set_tightbinding(Wannier90TightBindingModel)
        elif self.model_type == "tight binding":
            print(" - Loading simple tight binding model")
            self.set_tightbinding(SimpleTightBindingModel)
            nk = self.tightbinding.nk
        else:
            raise Exception("Model type not understood")

        self.kmesh = self.generate_kmesh(nk)
        self.num_k_all = len(self.kmesh)

        self.dos, self.dos_energy = self.tightbinding.compute_dos(100, self.kmesh)
        self.normalize()

    def generate_kmesh(self,nk):
        ks = np.linspace(-0.5,0.5,nk)
        if self.tightbinding.dim == 2:
            kx,ky,kz = np.meshgrid(ks,ks,[0])
        else:
            kx,ky,kz = np.meshgrid(ks,ks,ks)
        mesh = np.array([kx.flatten(),ky.flatten(),kz.flatten()])
        return mesh.swapaxes(0,-1)

    def GetBandsForKPath(self, kpath: KPath, min_band, max_band):
        return None
        #Assumption: this is used only to generate bands outside the correlated window within PEScfEngine
        #in a model, everything is in the window. 
        #Will cause some problems in QP visitor...

        BS = DiagonalModelBandStructure(kpath.num_k, max_band - min_band, self.mu)
        kpath = np.swapaxes(kpath.k,0,1)
        BS.energy[:,:,:] = self.tightbinding.compute_epsK(kpath)[min_band:max_band,:,:] / Ry2eV
        return BS


    def GetBandsForAllKpoints(self, min_band, max_band):
        return None
        #Assumption: this is used only to generate bands outside the correlated window within PEScfEngine
        #in a model, everything is in the window. 
        #Will cause some problems in QP visitor...

        BS = DiagonalModelBandStructure(self.num_k_all, max_band - min_band, self.mu)
        BS.energy[:,:,:] = self.tightbinding.compute_epsK(self.kmesh)[min_band:max_band,:,:] / Ry2eV
        return BS

    def GetStructure(self):
        return self.structure
    
    def GetLAPWBasis(self):
        return None

    def normalize(self):
        for i in range(self.num_bands):
            total = scintegrate.simps(self.dos[:,i])
            self.dos[:,i] /= total

    not_sure_how_to_deal_wth_fake_bands = """
    def semicircular(self):
        self.omegas = np.linspace(-self.opts.model_halfwidth, self.opts.model_halfwidth, self.opts.model_num_bins, endpoint = True)
        self.dos = 2.0/(np.pi*self.opts.model_halfwidth)*np.sqrt(1 - (self.omegas/self.opts.model_halfwidth)**2)
        self.normalize()
        self.dim = self.opts.model_bands
        self.mu = self.opts.model_mu

    def flat(self):
        self.omegas = np.linspace(-self.opts.model_halfwidth, self.opts.model_halfwidth, self.opts.model_num_bins, endpoint = True)
        self.dos = np.ones_like(self.omegas)
        self.normalize()
        self.dim = self.opts.model_bands
        self.mu = self.opts.model_mu
    """
    
    def set_tightbinding(self, TightBindingClass):
        self.tightbinding = TightBindingClass(self.opts)
        self.num_bands = self.tightbinding.nfun
        self.structure = self.tightbinding.st