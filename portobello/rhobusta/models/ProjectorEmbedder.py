import numpy as np

from portobello.generated.atomic_orbitals import SelectedOrbitals, OrbitalsBandProjector
from portobello.generated.models import ModelShell
from portobello.generated.LAPW import KPath
from portobello.generated.base_types import Window

from portobello.rhobusta.models.BandModels import ModelPlugin, ModelBandStructure
from portobello.rhobusta.ProjectorEmbedder import CorrelatedSubspaceProjector
from portobello.rhobusta.PEScfEngine import Ry2eV


class Model(ModelShell):

    def __init__(self, opts, plugin, is_new = True):
        self.opts = opts
        ModelShell.__init__(self)

        if is_new:

            if  opts.wannier_correlated_orbs == "":
                correlated_orb_indices = range(plugin.num_bands)
            else:
                correlated_orbs =  opts.wannier_correlated_orbs.split("...")
                correlated_orbs_dict = {}
                correlated_orb_indices = []
                for orb in correlated_orbs:
                    dict_entry = orb.split(":")
                    indices = [int(o) for o in dict_entry[1].split(",")]
                    correlated_orbs_dict[dict_entry[0]] = indices
                    correlated_orb_indices += indices

            self.dim = len(correlated_orb_indices)
            self.which_shell=0
            self.num_anti_equivalents=0
            self.num_equivalents=1

            self.allocate()
            self.correlated_orb_indices[:] = correlated_orb_indices[:]
            self.model_type = self.opts.model_type

            if  opts.wannier_correlated_orbs == "":
                self.rep[:,:] = np.diag(range(1,self.dim+1))

            else:
                k=0
                for j,name in enumerate(correlated_orbs_dict.keys()):
                    norbs = len(correlated_orbs_dict[name])
                    for i_ in range(norbs):
                        self.rep[k,k] = j+1
                        k+=1

            self.raw_rep[:,:] = self.rep[:,:]
            self.basis[:,:] = np.eye(self.dim)[:,:]
            self.rotations[0,:,:] = np.eye(self.dim)[:,:]

            model_so = SelectedOrbitals(num_shells = 1)
            model_so.allocate()
            model_so.shells[0] = self

            model_so.store("./projector.h5:/def/")
            self.store("./projector.h5:/model/")
        
        else:

            self.load("./projector.h5:/model/")
            self.rotations = np.array([np.eye(self.dim)])

class ModelProjector(CorrelatedSubspaceProjector):
    def __init__(self, model : Model, plugin : ModelPlugin):

        self.shell = model
        self.plugin = plugin
        self.energyWin = Window(low=-10, high=10)    

        OrbitalsBandProjector.__init__(self, 
            min_band = 0,
            max_band = self.plugin.num_bands-1,
            num_k_all = self.GetNumberOfKPoints(),
            num_orbs =  self.shell.dim,
            num_si =  1)      

        self.num_k = self.num_k_all
        self.num_corr_bands = self.plugin.num_bands
        
        self.allocate()
        self.CalculateP()

        self.BS = None

    def GetNumberOfKPoints(self):
        return len(self.plugin.kmesh)
        
    def CalculateP(self):
        self.BS = ModelBandStructure(self.num_k_all, self.num_corr_bands, self.plugin.mu)
        for i in self.shell.correlated_orb_indices:
            self.P[i,i,:,:] = 1
        
    def GetBandStructureInWindow(self):
        if self.BS is None:
            self.BS = ModelBandStructure(self.num_k_all, self.num_corr_bands, self.plugin.mu)
            # Energy is a different shape than normal - because Hk is not diagonal
            # But we need this field in order to use visitor frameworks
            self.BS.energy = self.plugin.tightbinding.compute_Hk(self.plugin.kmesh) / Ry2eV

        return self.BS



class ModelKPathProjector(ModelProjector):
    
    def __init__(self, BZProjector : ModelProjector, kpath : KPath) :
        self.bzp = BZProjector
        self.kpath = kpath
        ModelProjector.__init__(self, self.bzp.shell, self.bzp.plugin)
        self.num_k = self.kpath.num_k
        
    def GetNumberOfKPoints(self):
        return self.kpath.num_k
    
    def GetBandBoundsWithinWindow(self):
        return self.bzp.min_band-1, self.bzp.max_band-1  #python convention
    
    def GetBandStructureInWindow(self):
        kpath = np.swapaxes(self.kpath.k,0,1)
        BS = ModelBandStructure(self.kpath.num_k, self.num_corr_bands, self.plugin.mu)
        BS.energy = self.plugin.tightbinding.compute_Hk(kpath) / Ry2eV # Energy is a different shape than normal
        return BS
      

