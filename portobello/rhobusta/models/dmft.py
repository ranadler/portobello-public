#!/usr/bin/env python3

import logging
import os

from scipy import integrate as scintegrate
from portobello.bus.mpi import MPIContainer
import signal
import sys
from argparse import ArgumentDefaultsHelpFormatter
import numpy as np

from portobello.generated.dmft import DMFTState, ImpurityProblem

from portobello.bus.Matrix import Matrix, ZeroComplexMatrix
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.options import RefreshOptions, SaveOptions
from portobello.rhobusta.dmft import DMFTOrchastrator, DMFTOptionsParser
from portobello.bus.persistence import Store
from portobello.rhobusta.models.PEScfEngine import ModelOptionsParser, PEScfEngineForModels, Ry2eV

def DMFTModelOptionsParser(prepArgs, add_help=False, private=False):
        
    parser = ArgumentParserThatStoresArgv('dmft for models', add_help=add_help ,
                            parents=[DMFTOptionsParser(prepArgs, add_help=False), ModelOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)    

    return parser


class DMFTForModelsOrchastrator(DMFTOrchastrator, PEScfEngineForModels):

    def __init__(self, opts, mpi):
        DMFTState.__init__(self)
        
        is_new = PEScfEngineForModels.__init__(self, opts, mpi)  
        if is_new:
            self.num_freqs =  int(opts.energy_limit * self.beta / (2*np.pi))
            print(" - energy limit dmft: %f, beta=%f"% (opts.energy_limit, self.beta)) 
            print(" - number of frequency points in dmft: %d"% (self.num_freqs)) 

            self.sig = self.NewFreqDepLocalEnergyMatrix()
            for iw in range(self.sig.num_omega):
                self.sig.M[iw,:,:,:] = self.DC[:,:,:]
                
            self.sig.moments[0,:,:,:] = self.DC[:,:,:]
            self.GImp = self.NewFreqDepLocalEnergyMatrix()
            self.hyb = self.NewFreqDepLocalEnergyMatrix()
        
    def CorrelatedG_from_dos(self,w,M,si):
        dim = self.corrSubshell.dim
        corrGw = ZeroComplexMatrix(dim)
        ginv = ZeroComplexMatrix(dim)
        
        for i, d in enumerate(self.plugin.dos[:,0]):
            ginv[:,:] = (w  - (self.plugin.dos_energy[i] - self.plugin.mu_ev))*np.eye(self.dim) - (M[:,:,si] - self.DC[:,:,si])
            corrGw[:,:] += d * ginv.I
        
        return Matrix(corrGw)

    def CorrelatedG(self, w, M, si):
        dim = self.corrSubshell.dim
        if dim == 1:
            return self.CorrelatedG_from_dos(w,M,si)

        corrGw = ZeroComplexMatrix(dim)
        GInvK = ZeroComplexMatrix(self.orbBandProj.num_corr_bands)
        
        BS = self.orbBandProj.GetBandStructureInWindow()
         
        Hk = (BS.energy - BS.chemical_potential) * Ry2eV

        for k in range(self.orbBandProj.num_k_all):
            Pk = self.OneAtomProjector(k, si)
            
            GInvK[:,:] = w*np.eye(self.orbBandProj.num_corr_bands)  - Hk[:,:,k,si]  - self.EmbeddedIntoKBandWindow(M - self.DC, k, si)
            
            corrGw[:,:] += Pk.H * GInvK.I * Pk

        self.SymmetrizeCorrelated(corrGw, hermitian=False)
        
        return Matrix(corrGw)/self.orbBandProj.num_k_all



    def PrepareAndRunCTQMC(self):     

        explicitU = self.GetUMatrix().flatten()
        Udim = len(explicitU)

        imp = ImpurityProblem(isReal=False,
                              U=self.U,
                              J=self.J,
                              beta=self.beta,
                              subshell=self.corrSubshell,
                              num_si=self.num_si,
                              nrel=1,
                              num_freqs=self.num_freqs,
                              max_sampling_energy=0.75*self.num_freqs *2*np.pi /self.beta, # in eV. Make this an option?
                              hamiltonian_approximation='ising' if self.ising else 'none',
                              mu=0.0,
                              dim=self.corrSubshell.dim,
                              self_path=self.GetImpurityLocation(),
                              os_dir = f"./Impurity/",
                              two_body_size = Udim)

        imp.allocate()
        imp.two_body = explicitU

        # initialize the impurity object
        imp.hyb = self.NewFreqDepLocalEnergyMatrix()
        
        for si in range(self.num_si):
            # this includes off-diagonals
            self.PrintMatrix(self.DC[:,:,si])
            imp.E[:,:,si] = self.Elocal[:,:,si] - self.DC[:,:,si]
            w = 1000.0 * 1j # eV
            print(" - Hybridization matrix at very large omega %f eV:" % w.imag)
            corrG = self.CorrelatedG(w, self.sig.moments[0,:,:,:], si)

            self.PrintMatrix(self.ID1 * w  - corrG.I - imp.E[:,:,si] - self.sig.moments[0,:,:, si])
                        
            for iomega in range(self.num_freqs):
                w = self.sig.omega[iomega] * 1j
                corrG = self.CorrelatedG(w, self.sig.M[iomega,:,:,:], si)        
                imp.hyb.M[iomega, :,:,si] = \
                  self.ID1 * w  - corrG.I - imp.E[:,:,si] - self.sig.M[iomega, :,:, si]
                  
                if iomega == (self.num_freqs-1) or iomega == 0:                  
                    print("iomega: %d (%f eV) [spin=%d]" % (iomega, w.imag, si))   
                    self.PrintMatrix(imp.hyb.M[iomega, :,:, si])
                    
        self.hyb = imp.hyb
       
        self.RunCTQMC(imp)

def DMFTForModelsMain(args, kwargs, opts=None):
    
    if opts is None:
        parser = DMFTModelOptionsParser(args, add_help=True, private=True)    
        opts, _args = parser.parse_known_args(args)
        SaveOptions(opts)
    
    for key in kwargs:
        setattr(opts, key, kwargs[key])
    
    
    logging.basicConfig()
    #logging.getLogger().setLevel(logging.INFO)
    logging.info("running in directory "+ os.path.abspath("."))
    
    def handler(_signum, _unused):
        print(" .. saving all buffers before exit")
        Store.FlushAll()  # make sure we do nothing. we handle signals in Fortran
        print(" - exiting.")
        sys.exit(0)
        
    signal.signal(signal.SIGINT, handler)
        
    mpi = MPIContainer(opts)
    
    orc = DMFTForModelsOrchastrator(opts, mpi)

    if opts.examine_convergence:
        orc.PlotGlocAndGimp()
        return
        
    orc.dispatch(cont=False)
   
    if opts.pre_process:
        return

    if opts.analysis_only:
        orc.dispatch(cont=True)
        return
   
    if opts.dry_run:
        return
    
    i = 0
    while True:
        i+=1
        RefreshOptions(opts)
        if orc.sub_iter > opts.num_impurity_iters: break
        
        print("Impurity solver iteration #%d" % (i+1))
        orc.dispatch(cont=True)

if __name__ == '__main__':
    DMFTForModelsMain(sys.argv[1:], {})
    