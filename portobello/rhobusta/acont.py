#!/usr/bin/python3

'''
Created on Aug 21, 2018

@author: adler@physics.rutgers.edu
'''


from portobello.generated.dmft import DMFTState
import numpy as np
from subprocess import call
import matplotlib.pyplot as plt
from portobello.generated.self_energies import LocalOmegaFunction
from portobello.rhobusta.dmft import DMFTOrchastrator
import sys
from optparse import OptionParser
from portobello.rhobusta.ProjectorEmbedder import CorrelatedSubspaceProjector
from portobello.rhobusta.observables import GetLabelsForPlotting
from pathlib import Path
import os

KHMaxentExecutable = "/home/adler/maxent_source/maxent_run.py"


MaxentParams = """
params={{'statistics': 'fermi', # fermi/bose
        'Nw'        : 400,     # number of frequency points on real axis
        'Ntau'      : 300,     # Number of time points
        'L'         : {L},    # cutoff frequency on real axis
        'x0'        : 0.005,    # low energy cutoff
        'gwidth'    : 10,  # width of gaussian
        'idg'       : 1,       # error scheme: idg=1 -> sigma=deltag ; idg=0 -> sigma=deltag*G(tau)
        
        'deltag'    : {deltag},    # error - NOTE this is the most important number here.
        
        'Asteps'    : 4000,    # anealing steps
        'alpha0'    : 1000,    # starting alpha
        'min_ratio' : 0.001,    # condition to finish, what should be the ratio
        'iflat'     : 1,       # iflat=0 : constant model, iflat=1 : gaussian of width gwidth, iflat=2 : input using file model.dat
        'Nitt'      : 1000,    # maximum number of outside iterations
        'Nr'        : 0,       # number of smoothing runs
        'bwdth'     : 0.004,    # smoothing width
        'Nf'        : {Nf},      # to perform inverse Fourier, high frequency limit is computed from the last Nf points
    }}
"""


class AnalyticalCont(DMFTOrchastrator):
    '''
    classdocs
    '''
    
    def __init__(self, opts):
        '''
        Constructor
        '''
        DMFTState.__init__(self)
    
        if Path("./dmft.h5").exists():
            self.load("./dmft.h5:/")
            print("Using dmft file from current directory.")
        elif  Path("../dmft.h5").exists():
            print("Using dmft file from parent directory.")
            self.load("../dmft.h5:/")
        else:
            raise Exception("Cannot find dmft.h5 file")
        self.opts = opts
      
    # return pairs si, i indices for the orbitals in correct order for 
    # the spinless (num_si == 1) or spinful (num_si == 2) cases
    def GetIndices(self):
        indices = DMFTOrchastrator.GetRepOrbitals(self.CorrectRep())     
        if self.num_si == 1:
            return [(0, i) for i in indices]
        else:
            # this is the case of completely symmetric representation. Change when we generalize.
            return [(0, i) for i in indices] + [(1,i) for i in indices]
            
        
        
    def KHauleMaxEnt(self):
        indices = self.GetIndices()
        
        sig = open("Sig.inp","w")
        sig.write("#s_oo=%s\n"% str([self.sig.moments[0,i,i,si] for si, i in indices]))
        sig.write("#Edc=%s" % str([self.DC[i,i,si].real for si, i in indices]))
        
        for iomega, omega in enumerate(self.sig.omega):
            d =  ["%f %f"%(self.sig.M[iomega,i,i,si].real - self.sig.moments[0,i,i,si].real, 
                           self.sig.M[iomega,i,i,si].imag - self.sig.moments[0,i,i,si].imag) 
                  for si,i in indices]
        # TODO: make sure the units here are correct (read KH code), this seems to be correct (Ry input, Ev output)
            sig.write("\n%f %s" % (omega, " ".join(d)))
        sig.close()
                
        def getSymmetricEWindow():
            return self.opts.energy_limit
            #return (self.proj_energy_window.high -  self.proj_energy_window.low) / 2.0 
        
        params = open("./maxent_params.dat", "w")
        params.write(MaxentParams.format(Nf=4,
                                         deltag=self.opts.deltag,
                                         L=getSymmetricEWindow()))
        params.close()

        call([KHMaxentExecutable,"Sig.inp"])
        
    def ReadKHauleOutput(self):
        res = LocalOmegaFunction(is_real=True, 
                                 num_orbs=self.dim, 
                                 num_si=self.num_si)
        
        # count number of frequencies in the file
        sigf = open("Sig.out", "r")
        sigf.readline()
        sigf.readline()
        for _ in sigf.readlines():
            res.num_omega += 1
        sigf.close()
        
        res.allocate()
        
        sigf = open("Sig.out", "r")
        sigf.readline()
        sigf.readline()
        iomega = 0
        
        # copy moments
        res.moments = self.sig.moments
        
        for line in sigf.readlines():
            values = [float(f) for f in line.split()]
            res.omega[iomega] = values[0]
            for iorb, idx in enumerate(np.diag(self.CorrectRep())):
                col = idx -1               
                res.M[iomega, iorb, iorb, 0] = values[2*col+1] + 1j * values[2*col+2]
                if self.num_si > 1:
                    start = (len(values)-1)//2  #*2 for the real/imag. *2 for the spin
                    res.M[iomega, iorb, iorb, 1] = values[start + 2*col] + 1j * values[start + 2*col+1]
            iomega += 1
        sigf.close()
        
        res.store("./real-imp-self-energy.h5:/")
        
        
        
    def PlotContinuedSig(self, plot_labels):
                
        res = LocalOmegaFunction()
        res.load("./real-imp-self-energy.h5:/")
         
        _f, axis = plt.subplots(2, 1, sharex='col', sharey='row')
    
        axr = axis[0]
        axi = axis[1] 
        axr.set_title("Analytically continued $\Sigma_{imp}$")
                        
        for si, iorb in self.GetIndices():
            axr.plot(res.omega[:], np.real(res.M[:, iorb, iorb,si]))    
            axi.plot(res.omega[:],  np.imag(res.M[:, iorb, iorb,si]))
        
        axr.set(xlabel='$\omega$', ylabel="real")
        axi.set(xlabel='$\omega$', ylabel="imag")
        
        if plot_labels:
            try:
                cwd = Path.cwd()
                os.chdir(str(cwd.parent))
                plottingProj = CorrelatedSubspaceProjector("./projector.h5:/def/", self.proj_energy_window)
            finally:
                os.chdir(str(cwd))
                    
            vals, orbs = GetLabelsForPlotting(self, shell=plottingProj.shell, opn='')
            orb_names = [f"${orb}$" for orb in orbs]
            plt.legend(orb_names, loc='upper left')       #'Kohn-Sham only'], loc='upper left') 

        axr.label_outer()
        axi.label_outer()
        plt.show(block=True)
               
    def ManipulateSig(self):
        res = LocalOmegaFunction()
        res.load("./real-imp-self-energy.h5:/")
        
        zi = res.num_omega // 2-10

        #for omi in range(res.num_omega):
        #    if abs(res.omega[omi]) > 0.3:  res.M[omi, :, :,:] = np.real(res.M[omi, :, :,:]) + np.imag(res.M[omi, :, :,:])*1j*exp(-abs(res.omega[omi])*1000.0)
        #    else:
                #res.M[omi, :, :,:] = np.real(res.M[omi, :, :,:]) + np.imag(res.M[omi, :, :,:])*1.07*1j # amlify
         
        indices = self.GetIndices()
        
        for si, iorb in indices: 
            #if np.imag(res.M[zi, iorb, iorb,si]) > -0.008:
            res.M[:, iorb, iorb,si] = np.real(res.M[:, iorb, iorb,si]) # zero DOS
            
        res.store("./real-imp-self-energy.h5:/", flush=True)

def main():
    
    parser = OptionParser()    
    parser.add_option("-P", "--plot", dest="plot_only",
                      action="store_true",
                      default=False)

    parser.add_option("-X", "--manipulate", dest="manipulate",
                      action="store_true",
                      default=False)
    
    parser.add_option("-e", "--energy-limit", dest="energy_limit", default=20.0, type=float,
                      help="energy limit for self-energy frequency (on positive side) [default: %default eV]")
    
    
    parser.add_option("--deltag", dest="deltag",
                      default=0.008,
                      type=float,
                      help="""The estimated error in the CTQMC data. 
                               Guessing this number correctly is the key to success
                               (although it may impossible to use one guess for multiple orbitals).""")
    
    (opts, _args) = parser.parse_args(sys.argv[1:])
        
    sp = AnalyticalCont(opts)
        
    if not opts.plot_only:
        sp.KHauleMaxEnt()
        sp.ReadKHauleOutput()
    
    if opts.manipulate:
        if opts.plot_only:
            sp.ReadKHauleOutput()
        sp.ManipulateSig()
        
    sp.PlotContinuedSig(opts.plot_only)
    
        
if __name__ == '__main__':
    main()
    
        

