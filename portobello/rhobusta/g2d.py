#!/usr/bin/python
'''
Created on Oct 12, 2018

@author: adler
'''
from portobello.rhobusta.dmft import DMFTOrchastrator
from optparse import OptionParser
import sys
from portobello.bus.persistence import Store
from portobello.rhobusta.g.GEngine import GEngine
from os.path import exists
import os


class G2D(DMFTOrchastrator):
    '''
    classdocs
    '''


    def __init__(self, opts):
        '''
        Constructor
        '''
        beta = opts.beta # store it
        g = GEngine(opts, for_dft=False)
        
        opts.U = g.U
        opts.J = g.J
        opts.N0 = g.Nimp
        opts.fixed_double_counting = g.DoubleCounting(is_new=False)
        opts.window = ":".join(
            [str(g.proj_energy_window.low), 
             str(g.proj_energy_window.high)])
        opts.beta = beta
        
        newdir = './g2d'
               
        if not exists(newdir):
            os.mkdir(newdir)
        
        os.chdir(newdir)
        
        DMFTOrchastrator.__init__(self, opts, mpi_workers=0)
        
        g.SetSigImag(self.sig)
        
        self.super_iter = 2
        self.state = 'self-energy-ready'
        
        self.StoreMyself()

if __name__ == '__main__':
    
    parser = OptionParser()
       
    parser.add_option("-T", "--Temperature", dest="Temperature", default=None,
                      type=int, help="Temperature in Kelvin")
 
    parser.add_option("-b", "--beta", dest="beta", type=float, default=None, help="[default: %default]")

    parser.add_option("-e", "--energy-limit", dest="energy_limit", default=15.0, type=float,
                     help="energy limit for self-energy frequency [default: %default eV]")

    (opts, args) = parser.parse_args(sys.argv[1:])
    
    KB = 6.333328e-6 *27.2107* 0.5 # collected from Kutepov's code, to match perfectly

    if opts.Temperature is not None and opts.beta is None:
        opts.beta = 1.0/(opts.Temperature * KB)
    if opts.Temperature is None:
        opts.Temperature = 1.0/opts.beta / KB
        
    assert(opts.Temperature is not None)
    
    fd = G2D(opts)
    
    Store.FlushAll()
    