#!/usr/bin/python

from portobello.generated.force import FlapwForce
from portobello.rhobusta.DFTPlugin import DFTPlugin

# note: this is not mainained and will not work as is

plugin = DFTPlugin.Instance()

plugin.dft.DFTPlusSigmaOneIteration("", "",
                        checkpoint=False,
                        search_mu=True,
                        update_lapw=True,     
                        save_potentials=0,
                        mu_search_variant='dft')

print("Calculating forces ...")
FlapwForce.CalculateForce('')


FF=FlapwForce()
FF.load('./force.h5:/')
print(FF.force_cont)