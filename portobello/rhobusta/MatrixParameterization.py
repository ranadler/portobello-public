#!/usr/bin/env python3

'''
Created on March 15, 2021

@author: adler@physics.rutgers.edu
'''

from typing import List
import numpy as np

class MatrixParameterizer:
    """ parametrize a matrix as a vector of real numbers.
        The matrix is possibly assumed Hermitian.
        In the Hermitian case, the diagonal is always real, and if the same rep
        appears on opposite off-diagonals, then it will be assumed real
    """

    def __init__(self, corrObject : object, attr : str, si : int, isHermitian : bool, obj: object = None, isReal : bool =False):
        self.corrObject = corrObject
        if obj is not None:
            self.obj = obj
        else:
            self.obj = self.corrObject
        self.si = si
        self.rep = self.SelectRep(corrObject)
        self.attr = attr
        self.isHermitian = isHermitian
        self.repMax = np.amax(self.rep)
        self.isReal = np.zeros((self.repMax,), dtype=np.int16) #(-1) - complex, 1 - real
        self.indices = [[] for _i in range(self.repMax)]

        for (i,j), rr in np.ndenumerate(self.rep):
            if rr == 0: continue
            if i==j:
                if isReal or self.isHermitian:
                    self.isReal[rr-1] = 1
                else:
                    self.isReal[rr-1] = -1
            else:
                if isReal or self.isHermitian and self.rep[j,i] == rr:
                    self.isReal[rr-1] = 1
                else:
                    self.isReal[rr-1] = -1
            self.indices[rr-1].append((i,j))

        # this array maps the vector index to ((i,j),isRealPart)
        self.toArray = []
        for r in range(self.repMax):
            assert(len(self.indices[r])>0), (r, self.indices[r])
            indices = self.indices[r][0]
            assert(indices[0]>=0)
            assert(indices[1]>=0)
            if self.isReal[r] > 0:
                self.toArray.append((r, True))
            else:
                self.toArray.append((r, True))
                self.toArray.append((r, False))
        self.count = len(self.toArray)

    def SelectRep(self, corrObject):
        # this one may be a permutation of correctObject.rep. It is different than raw_rep
        # in that it contains only the selected subset of orbitals (for example in -Xjj this will have only 
        # two orbitals, whereas the raw rep typically will be at least diagonal)
        return corrObject.CorrectRep()

    def Length(self):
        return self.count

    def Pack(self) -> List[float]:
        x = np.zeros((self.count,),dtype=np.float64)
        M = getattr(self.obj, self.attr)
        #print(f"Matrix {self.attr} shape is: {M.shape}, type: {type(M)}")
        for e in range(self.count):
            r,isRealPart = self.toArray[e]
            indices = self.indices[r]
            for i,j in indices:
                if isRealPart:
                    x[e] += np.real(M[i,j,self.si])
                else:
                    x[e] += np.imag(M[i,j,self.si])
            x[e] /= len(indices)
        return x

    def PackComplex(self) -> List[float]:
        x = np.zeros((self.repMax,),dtype=np.complex128)
        M = getattr(self.obj, self.attr)
        for r in range(self.repMax):
            i,j = self.indices[r][0]
            x[r] = M[i,j]
        return x
        
    def Unpack(self, x : List[float]):
        z = [] # the full complex values for each rep
        for xidx,(_r,isRealPart) in enumerate(self.toArray):
            if isRealPart:
                z.append(x[xidx])
            else:
                assert(xidx > 0)
                assert(len(z)>0)
                z[-1] += 1j * x[xidx]
        assert(len(z)==self.repMax)
        return self.UnpackFromComplexVector(z)
        
    def UnpackFromComplexVector(self, z : List[complex]):
        M = getattr(self.obj, self.attr)
        M[:,:,self.si] = 0.0
        for (i,j), rr in np.ndenumerate(self.rep):
            if rr == 0: continue
            M[i,j,self.si] = z[rr-1]
        return M

    def DerivativeByComplexParam(self, i : int):
        assert(i <= self.repMax)
        return self.rep == i

    def HeuristicBound(self, lowerBound) -> List[float]:
        x = np.zeros((self.count,),dtype=np.float64)
        M = getattr(self.obj, self.attr)
        for e in range(self.count):
            r,isRealPart = self.toArray[e]
            indices = self.indices[r]
            for i,j in indices:
                if isRealPart:
                    m = max(lowerBound, abs(np.real(M[i,j,self.si])))
                else:
                    m = max(lowerBound, abs(np.imag(M[i,j,self.si])))
                x[e] = max(x[e], m)
        return x

    def BoundsVector(self, relative : bool, vmin : float, vmax : float, vmin_c : float, vmax_c : float):
        bounds = []
        if relative: x = self.Pack()
        for xidx,(_i,_j,isRealPart) in enumerate(self.toArray):
            if isRealPart:
                if relative:
                    bounds.append((x[xidx]+vmin,x[xidx]+vmax))
                else:
                    bounds.append((vmin,vmax))
            else:
                if relative:
                    bounds.append((x[xidx]+vmin_c,x[xidx]+vmax_c))
                else:
                    bounds.append((vmin_c,vmax_c))
        return bounds

    def GetError2(self):
        """
            calculate the off-representation component of the matrix
        Returns:
            [float]: sum of square off-representations elements (which are not 
            used by the Pack() operation)
        """
        err = 0.0
        M = getattr(self.obj, self.attr)[:,:,self.si]
        for (i,j), val in np.ndenumerate(M):
            av = abs(val)
            if av>1.0e-10 and self.rep[i,j] == 0:
                err += av
        return err

class RawMatrixParameterizer(MatrixParameterizer):
    def SelectRep(self, corrObject):
        return corrObject.CorrectRep(is_raw=True)

class MatrixSymmetrizer():
    def __init__(self, corrObject : object, isHermitian:bool, is_raw=False):
        self.corrObject=corrObject
        self.M = np.zeros_like(corrObject.RhoImp) # has num_si spins
        self.num_si =  self.corrObject.num_si
        if not is_raw:
            self.mp = [MatrixParameterizer(corrObject, "M", si, isHermitian=isHermitian, obj=self) 
                        for si in range(self.num_si)]
        else:
            self.mp = [RawMatrixParameterizer(corrObject, "M", si, isHermitian=isHermitian, obj=self) 
                        for si in range(self.num_si)]

    def Symmetrize(self, M):
        num_si =self.num_si
        if len(M.shape) == 2:
            num_si = 1
            self.M[:,:,0] = M[:,:]
        else:
            self.M[:,:,:num_si] = M[...]

        for si in range(num_si):
            sym = self.mp[si]
            sym.Unpack(sym.Pack())

        if len(M.shape) == 2:
            M[:,:] = self.M[:,:,0]
        else:
            M[...] =  self.M[:,:,:num_si]

class HermitianSymmetrizer(MatrixSymmetrizer):
    def __init__(self, corrObject : object):
        MatrixSymmetrizer.__init__(self, corrObject, isHermitian=True, is_raw=False)
 
class NonHermitianSymmetrizer(MatrixSymmetrizer):
    def __init__(self, corrObject : object):
        MatrixSymmetrizer.__init__(self, corrObject, isHermitian=False, is_raw=False)
 
class RawHermitianSymmetrizer(MatrixSymmetrizer):
    def __init__(self, corrObject : object):
        MatrixSymmetrizer.__init__(self, corrObject, isHermitian=True, is_raw=True)


class Parameterizer:
    def __init__(self, corrObject : object) -> None:
        self.corrObject = corrObject
        self.params = []
        self.snap = None

    def AddSpinfulMatrix(self, obj:object, attr : str, isHermitian : bool, isReal=False):
        for si in range(self.corrObject.num_si):
            self.params.append(MatrixParameterizer(self.corrObject, attr=attr, si=si, isHermitian=isHermitian, obj=obj, isReal=isReal))
    
    def Pack(self):
        ret = np.hstack([pz.Pack() for pz in self.params])
        return ret
    
    def Unpack(self, x):
        start = 0
        for pz in self.params:
            pz.Unpack(x[start:start+pz.Length()])
            start+=pz.Length()
        assert(len(x)==start), (start, len(x)) # exhausted x
    
    def Symmetrize(self):
        self.Unpack(self.Pack())

    def Capture(self, src=None):
        if src is not None:
            self.Unpack(src)

        self.snap = self.Pack()
        return self.snap

    def ImageAnd𝛿(self):
        if self.snap is None:
            raise Exception("no snapshot recorded yet")
        else:
            val = self.Pack()
            return val, val - self.snap
