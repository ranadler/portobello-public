#!/usr/bin/env python3

'''
Created on Jul 25, 2019

@author: adler
'''
import argparse
import sys
from argparse import ArgumentDefaultsHelpFormatter
import numpy as np
from portobello.bus.Matrix import Matrix
from portobello.bus.mpi import GetMPIProxyOptionsParser
from portobello.generated.fermi_surface import LowEnergyHamiltonian
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.plotters import FSBandBuilder, FSBuilder
from portobello.rhobusta.loader import AnalysisLoader, AnalysisOptionsParser, DFTLoader
from portobello.rhobusta.ProjectorEmbedder import (
    CalculateBandBoundsWithinWindow, Projector)
#from mayavi.tools.sources import scalar_field
from scipy.interpolate.rbf import Rbf


class LowEnergyHamiltonianBuilder(AnalysisLoader):

    def MPIWorker(self):
        self.InitializeMPI()

        methodName = self.LoadCalculation(loadRealSigma=self.opts.from_spectral_function or self.opts.from_real_axis)
        if self.svs is None:
            self.svs = DFTLoader(opts)

        if self.opts.from_spectral_function and methodName == "dmft":
            fsb = FSBuilder(self.svs, f"FS-{self.svs.super_iter}.{self.svs.sub_iter}", self.opts)
            self.svs.VisitGreensFunction(fsb)
        else:
            fsb = FSBandBuilder(self.svs, f"FS-{self.svs.super_iter}.{self.svs.sub_iter}", self.opts)
            self.svs.VisitBandStructure(fsb)

        if self.svs.plugin.IsMaster():
            fsb.LEH.store('./fs.core.h5:/',flush=True)   
            fsb.LEH.WriteFRMSF(selfpath="")
        self.Terminate()
    
      
if __name__ == '__main__':
    
    parser = ArgumentParserThatStoresArgv('fermi-surface', add_help=True,
                            parents=[GetMPIProxyOptionsParser(), AnalysisOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)
                            
    parser.add_argument("--lifetime", dest="lifetime",
                    action="store_true",
                    default=False,
                    help = "show life time in scalar field plot")

    parser.add_argument("--qp-weight", dest="qp_weight",
                    action="store_true",
                    default=False,
                    help = "show quasi-particle weight in scalar field plot")

    parser.add_argument("--lifetime-distribution", dest="lifetime_distribution",
                    action="store_true",
                    default=False,
                    help = "show the distribution of lifetimes on the fermi surface")

    parser.add_argument("--lifetime-distribution-nbin", dest="lifetime_distribution_nbin",
                    default=25,
                    help = "number of bins for lifetime histogram")

    parser.add_argument("--from-spectral-function", dest="from_spectral_function",
                    action="store_true",
                    default=False,
                    help = "Determine the spectral function from the peaks in the spectral function rather than from the quasiparticle hamiltonian (dmft)")


    (opts, _args) = parser.parse_known_args(sys.argv[1:])

    lehb = LowEnergyHamiltonianBuilder(opts)
    lehb.Run()
    
    
