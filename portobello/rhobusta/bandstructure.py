#!/usr/bin/env python3


'''
Created on Sep 9, 2020

@author: melnick
'''

import sys
from argparse import ArgumentDefaultsHelpFormatter, Namespace
from cmath import pi

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
#from mpi4py import MPI
from numpy.linalg.linalg import norm

from portobello.bus.Matrix import Matrix
from portobello.bus.mpi import GetMPIProxyOptionsParser
from portobello.generated.LAPW import KPath
from portobello.generated.symmetry import Bootstrap
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.bandstructure_utilities import getHighSymmKpath, CreateKPath, selectSpecifiedPath, PlaneWithSymmetry
from portobello.rhobusta.loader import AnalysisLoader, AnalysisOptionsParser
from portobello.rhobusta.plotters import ARPESBuilder, MultiARPESBuilder, BandBuilder, MultiBandBuilder
from pymatgen.electronic_structure.plotter import (
    plot_brillouin_zone_from_kpath, plot_points)
from portobello.generated.self_energies import LocalOmegaFunction


# rotate each point on the kpath to a copy where it has more neighbors in kpoints
# kpoints here is usually the irreducible BZ
def TransformKpath(kpath : KPath, kpoints, envEpsilon=0.3):
    bs = Bootstrap() 
    bs.load("./spacegroup.core.h5:/")
    for i in range(kpath.num_k):
        kp = np.array(kpath.k[:, i])
        orbit = [kp]
                    
        for g in range(bs.num_ops):
            rot = Matrix(bs.rots[:,:,g])
            kp2 = rot * Matrix(kp).T
            found = False
            for k3 in orbit:
                if norm(k3 - kp2) < 1.0e-9:
                    found = True
                    break
            if not found:
                orbit.append(kp2)
                
        for kp2 in orbit:
            maxKInEnv = -1
            kInEnv = 0
            for point in kpoints:
                if norm(point-kp2) < envEpsilon:
                    kInEnv += 1
            if kInEnv > maxKInEnv:
                kpath.k[:, i] = kp2.T
                maxKInEnv = kInEnv
                
        if norm(kp - kpath.k[:, i]) > 1.0e-9:
            print(f"rotated kpoint: maxKInEnv={maxKInEnv}  {kp} => {kpath.k[:,i]}")

class ARPESRunner(AnalysisLoader):

    def __init__(self,opts):
        AnalysisLoader.__init__(self,opts)
        self.pws = None

    def Prepare(self):
        return True

    def GetSigma(self):
        return self.svs.sig

    def GetKPathAndLabels(self):
        st, hskp = getHighSymmKpath(self.dftplugin, self.opts)
        
        basis = self.dftplugin.GetLAPWBasis()
        fac = 2*np.pi
        
        if self.opts.cell:
            assert(self.num_workers<=1)
            points = set()
            for kp in hskp.get_kpoints(line_density = self.opts.kdensity, coords_are_cartesian=True)[0]: #st.lattice.reciprocal_lattice):
                for k in range(basis.num_k_irr):
                    pnt = np.array(basis.KPoints[k,:])*fac
                    if norm(pnt- np.array(kp[:])) < 0.05:
                        points.add(k)
                 
            kpoints = []
            if self.opts.show_kpoints:   
                #kpoints = [np.array(basis.KPoints[k,:])*fac for k in points]
                kpoints = [np.array(basis.KPoints[k,:])*fac for k in range(self.dftplugin.dft.num_k_irr)]
                
            fig, ax = plot_points(kpoints, coords_are_cartesian=True, color="grey", marker="X")

            fig = plot_brillouin_zone_from_kpath(hskp, ax=ax, fold=True, lattice=st.lattice.reciprocal_lattice)

            plt.show()
            exit(0)
        
        cartesian = True
        if hasattr(self.opts, "model") and self.opts.model:
            cartesian = False

        kpath, labels =  CreateKPath(hskp, self.opts, cartesian=cartesian)
        
        if self.opts.transform:
            kpoints = [np.array(basis.KPoints[k,:])*fac for k in range(self.dftplugin.dft.num_k_irr)]
            TransformKpath(kpath, kpoints)
        
        return kpath, labels

    def GetVisitor(self):

        if self.opts.bands_only:

            if self.opts.obs != "":
                visitor = MultiBandBuilder(self.svs, self.kpath, self.labels, self.opts)
            else:
                visitor = BandBuilder(self.svs, self.kpath, self.labels, self.opts) 
    
        else:
            if self.opts.obs != "":
                visitor = MultiARPESBuilder(self.svs, self.kpath, self.labels, self.opts, pws = self.pws)
            else:
                visitor = ARPESBuilder(self.svs, self.kpath, self.labels, self.opts, pws = self.pws) 

        return visitor

    def Work(self, visitor):
        if self.opts.bands_only:
            self.svs.VisitBandStructure(visitor)
        else:
            self.svs.VisitGreensFunction(visitor)

    def MPIWorker(self):
        self.InitializeMPI()
        self.LoadCalculation(loadRealSigma = not self.opts.bands_only or self.opts.from_real_axis) 
        self.kpath, self.labels = self.GetKPathAndLabels()

        continue_after_prep = self.Prepare()

        if continue_after_prep:
            if not self.opts.bands_only:
                self.svs.sig = self.GetSigma()

            visitor = self.GetVisitor()
                
            self.Work(visitor)
        
        self.Barrier()
        self.Terminate()


class ARPESRunnerOnPlane(ARPESRunner):

    def __init__(self,opts):

        #TODO: add ability to load solutions with asymmetric bounds 
        #Because we can't, we're going to load something that will have our selected energy within the bounds
        #and figure out the index of the spot in the omega mesh which is nearest our desired energy
        self.selected_energy = opts.omega_window
        opts.omega_window = abs(opts.omega_window)+0.01

        AnalysisLoader.__init__(self,opts)
        
    def GetSigma(self):
        #define a new self energy, which is just the single component we care about
        selected_energy_index = np.argwhere(self.svs.sig.omega > self.selected_energy)[0]

        sig = LocalOmegaFunction(is_real=True,
                                 num_orbs=self.svs.sig.num_orbs,
                                 num_si=self.svs.sig.num_si,
                                 num_omega = 1)
        sig.allocate()
        sig.omega[0] = self.svs.sig.omega[selected_energy_index]
        sig.M[0, :,:,:] =  self.svs.sig.M[selected_energy_index, :,:,:]
        return sig

    def GetKPathAndLabels(self):
        label = "\Gamma" if self.opts.path == "" else self.opts.path 
        st, hskp = getHighSymmKpath(self.dftplugin, self.opts) 
        points, labels = hskp.get_kpoints(line_density = 2, coords_are_cartesian=True)
        if label is not None:
            assert label in labels, f"Must provide high symmetry point on the high symmetry path: {label} missing from {labels}"
            center_point = points[labels.index(label)]

        plane_vectors = self.opts.plane_vectors.split('...')
        for i,v in enumerate(plane_vectors):
            entries = v.split(',')
            try:
                float(entries[0])
                float_entries = True
            except:
                float_entries = False

            if float_entries:
                plane_vectors[i] = np.array([float(x) for x in entries])
            else:
                points, labels = hskp.get_kpoints(line_density = 2, coords_are_cartesian=True)
                vec = np.zeros((2,3), dtype=float)
                for j, vec_endpoint_label in enumerate(entries):
                    assert vec_endpoint_label in labels, f"Must provide high symmetry point on the high symmetry path: {vec_endpoint_label} missing from {labels}"
                    kp = points[labels.index(vec_endpoint_label)]
                    vec[j,:] = kp[:]
                plane_vectors[i] = vec[1] - vec[0]
        
        if self.opts.width < 0:
            width = [np.linalg.norm(v) for v in plane_vectors]
        else:
            width = [self.opts.width, self.opts.width]

        labels = [label, f"k along {np.round(plane_vectors[0],2)}", f"k along {np.round(plane_vectors[1],2)}"]

        self.pws = self.generate_plane_with_symm(width, plane_vectors, center_point, self.opts.kdensity)

        points = self.pws.get_irr_points()

        kpath = KPath()
        kpath.num_k = len(points)
        kpath.allocate()
        for ik in range(kpath.num_k):
            kpath.k[:,ik] = points[ik] / (2*np.pi)

        return kpath, labels

    def generate_plane_with_symm(self, width, plane_vectors, center_point, kdensity):

        for i, v in enumerate(plane_vectors):
            plane_vectors[i] = v/np.linalg.norm(v)*width[i]
        assert np.dot(plane_vectors[0],plane_vectors[1])<1e-13, f"Must provide orthogonal vectors defining the plane: {plane_vectors}"

        kdensity = self.opts.kdensity
        if kdensity%2 == 0: kdensity += 1 #include center point

        return PlaneWithSymmetry(self.dftplugin, center_point, plane_vectors, kdensity)

    
class EffectiveMassRunner(ARPESRunner):

    def GetKPathAndLabels(self):

        st, hskp = getHighSymmKpath(self.dftplugin, self.opts)
        
        points, labels = hskp.get_kpoints(line_density = 1, coords_are_cartesian=True)

        if self.opts.path == "":
            print(r"Effective mass requires desired point -- defaulting to $\Gamma$")
            print(f"Choices are: {labels}")
            self.opts.path = "\Gamma"


        offset = 0.1
        my_points = []
        my_labels = []
        for direction in ["kx","ky","kz"]:
            path = f"{offset}*{direction}+{self.opts.path}"
            path = f"-{path},{path}"
            ps,ls = selectSpecifiedPath(path, points, labels, 101)
            my_points += ps
            my_labels += ls

        kpath = KPath()
        kpath.num_k = len(my_points)
        kpath.allocate()

        fac = 2*np.pi
        for ik in range(kpath.num_k):
            kpath.k[:,ik] = my_points[ik] / fac

        return kpath, my_labels
        
    def Work(self, visitor):
        self.svs.VisitBandStructure(visitor) #force this to be a band calculation

    


def BandstructureParser(add_help = False, private = True):
    parser = ArgumentParserThatStoresArgv('spectral-function', add_help=add_help,
                            parents=[GetMPIProxyOptionsParser(), AnalysisOptionsParser(private=private)],
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument("-K", "--kpoint-density",
                    dest="kdensity",
                    default=50,
                    type=int,
                    help = "Density of kpoints along the lines between high-symmetry points")

    parser.add_argument("-c", dest="cell",
                    action="store_true",
                    default=False,
                    help = "Display the BZ and the path taken between high-symmetry points when plotting the spectral function")
    parser.add_argument("--show-kpoints", dest="show_kpoints",
                    action="store_true",
                    default=False,
                    help = "show k points in the BZ plot of the path")

    parser.add_argument("--bands-only", dest="bands_only",
                    action="store_true",
                    default=False,
                    help = "Plot the bandstructure (Kohn Sham eigenvalues) and not the ARPES")

    parser.add_argument("--test", dest="test_sf",
                    action="store_true",
                    default=False,
                    help = "simple test function")
    parser.add_argument("--transform", dest="transform",
                    action="store_true",
                    default=False,
                    help = "rotate k points to the copy which is closer to more points in the irr BZ")

           
    parser.add_argument("--susceptibility", 
                        dest="susceptibility", 
                        default=False, 
                        action="store_true",
                        help = "Instead of computing the static observable, plot the dynamic susceptibility of the desired observable (Must have run DMFT/CTQMC with the  --occupation-susceptibility option; Must give an expression with -e/--eval); Must be using DMFT and not Gutz")
    
    parser.add_argument("--path", dest="path", default="", 
                        help="""evaluate a portion of the path as defined by pairs of labels,
                                each entry in pair separated by a comma, each pair seperated by a semicolon
                                e.g., \Gamma,X;X,M;M,\Gamma;\Gamma,Z\n
                                one can add offsets from these lines, e.g., \Gamma,Z+0.1*kx-0.1*ky\n
                                """)
                          
    parser.add_argument("--plane", dest="plane",
                        action="store_true",
                        default=False,
                        help="""
                            Plot the spectral function on a plane at a given frequency, rather than along a path.
                            This changes the behavior of some parameters:\n
                            -W now sets the frequency rather than the range of frequencies\n
                            --path indicates the center point and width of the plane (\Gamma,0.5)\n
                             """)   


    parser.add_argument("--effective-mass", dest="effective_mass",
                        action="store_true",
                        default=False,
                        help="""
                            Compute the effective mass rather than plot a bandstructure
                             """)   

    parser.add_argument("--plane-vectors", dest="plane_vectors", default="1,0,0...0,1,0", 
                        help="""Two vectors defining the plane on which the spectral function is to be plotted. 
                        Can be entered as comma-separated pairs of high symmetry k points (separated by ...)
                        Or as two vectors of comma separated floats""")

    parser.add_argument("--plane-width", 
                          dest='width',
                          type=float,
                          default=-1,
                          help="Width of plane (otherwise it spans the vectors)")


    return parser
        
if __name__ == '__main__':
    
    parser = BandstructureParser(True)
    opts = parser.parse_args(sys.argv[1:])

    matplotlib.use("TkAgg")
    
    if opts.plane:
        runner = ARPESRunnerOnPlane(opts)
    elif opts.effective_mass:
        runner = EffectiveMassRunner(opts)
    else:
        runner = ARPESRunner(opts)
    runner.Run(wait=True)
