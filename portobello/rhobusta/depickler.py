#!/usr/bin/python3

'''
Created on Jun 2, 2020

@author: adler
'''


import matplotlib.text as plt_txt
import numpy as np


class IntensityFigure:

    ############################### Constructors ###############################

    def __init__(self, data, hskp, xlabels, ylabels, name):
        self.naxes = len(data)
        self.data = data
        self.hskp = hskp
        self.xlabels = xlabels
        self.ylabels = ylabels
        self.name = name

    #Generate from a pickled figure
    @classmethod
    def fromExistingFig(cls, fig, name):
        axes=fig.get_axes()
        
        for ax in reversed(axes):
            if ax.images == []: axes.pop()
           
        transforms = [ [cls.Transform(ax, i) for i in range(2) ] for ax in axes ] #i=0 -> x, i=1 ->y

        data = [cls.IntensityXYZ.fromAxis(ax, transforms[i]) for i, ax in enumerate(axes)]
        HSKPs = [cls.HSKP(ax,transforms[i]) for i, ax in enumerate(axes)]
        xlabels = [ax.get_xlabel() for ax in axes]
        ylabels = [ax.get_ylabel() for ax in axes]

        return cls(data,HSKPs,xlabels,ylabels, name)

    ############################### methods ################################

    #add Z values of intensity maps
    def __add__(self, other):
        new_data = [d + other.data[i] for i,d in enumerate(self.data)]
        return IntensityFigure(new_data, self.hskp, self.xlabels, self.ylabels, self.name)

    #subtract Z values of intensity maps
    def __sub__(self, other):
        new_data = [d - other.data[i] for i,d in enumerate(self.data)]
        return IntensityFigure(new_data, self.hskp, self.xlabels, self.ylabels, self.name)


    ############################### internal ###############################

    class Transform:
        
        def __init__(self, ax, axis_index):

            try:
                labels = ax.get_xticklabels() if axis_index == 0 else ax.get_yticklabels()

                actual_position = [float(l.get_text()) for l in labels]
                image_position = ax.get_xlim() if axis_index == 0 else ax.get_ylim()

                self.shift_1 = -image_position[0]
                self.scale = (actual_position[-1] - actual_position[0]) / (image_position[-1] - image_position[0])
                self.shift_2 = actual_position[0]

            except:

                self.shift_1 = 0
                self.scale = 1
                self.shift_2 = 0

        def apply(self, v):

            r = np.copy(v)
            r += self.shift_1
            r *= self.scale
            r += self.shift_2

            return r

    class IntensityXYZ:
        info = """stores intensity, Z, and positions, X,Y, on image
                also stores actual positions, aX, aY, associated with axis labeling"""

        def __init__(self, Z, X, aX, Y, aY):
            self.Z = Z
            self.maximum = np.max(self.Z)

            self.X = X
            self.aX = aX
            self.Y = Y
            self.aY = aY

        @classmethod
        def fromAxis(cls, ax, transforms):
            image = ax.get_images()[0]
            Z = np.transpose(image.get_array()) #the image was transposed from the original data

            xlim = ax.get_xlim()
            X = np.linspace(xlim[0], xlim[1], Z.shape[0], endpoint = True) 
            aX = transforms[0].apply(X)
            
            ylim = ax.get_ylim()
            Y = np.linspace(ylim[0], ylim[1], Z.shape[1], endpoint = True)
            aY = transforms[1].apply(Y)

            return cls(Z,X,aX,Y,aY)

        def __add__(self, other):
            new_Z = self.Z + other.Z
            return IntensityFigure.IntensityXYZ(new_Z, self.X, self.aX, self.Y, self.aY)

        def __sub__(self, other):
            new_Z = self.Z - other.Z
            return IntensityFigure.IntensityXYZ(new_Z, self.X, self.aX, self.Y, self.aY)

    class HSKP:
        info = """stores positions and labels of HSKP on image
                   also stores actual positions (self.apositions) assocaited with axis labeling"""

        def __init__(self, ax, transforms):

            #This grabs HSPK which are scatter points on the image
            if len( ax.collections ):
                self.positions = []
                for collection in ax.collections:
                    self.positions.append(collection.get_offsets().data)

                self.labels = []
                for child in ax.get_children():
                    if isinstance(child, plt_txt.Text):
                        txt = child.get_text()
                        if txt != '':
                            self.labels.append(txt[1:-1])

                self.plane = True
                
            #Or  which are at some x position
            else:
                labels = ax.get_xticklabels()
                self.labels = [l.get_text() for l in labels]
                if len(ax.get_xticks()) > 0:
                    self.positions = ax.get_xticks()
                else:
                    self.positions = [l.get_position()[0] for l in labels]
                self.plane = False
                
            self.apositions = np.zeros_like(self.positions)
            for i, p in enumerate(self.positions):
                self.apositions[i] = transforms[0].apply(p)

            self.nhskp = len(self.labels)
            assert(self.nhskp == len(self.positions))

            
if __name__ == '__main__':
    pass


