#!/usr/bin/env python3

from argparse import ArgumentDefaultsHelpFormatter
from matplotlib.text import Annotation
from matplotlib import cm
import numpy as np
import sys
from fractions import Fraction
import matplotlib.pyplot as plt
from portobello.bus.Matrix import Matrix
from portobello.bus.mpi import GetMPIProxyOptionsParser
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.rhobusta.crystal_field import CFRunner, LocalQPHBUilder

from portobello.rhobusta.histogram_plots import HistogramPlots
from portobello.rhobusta.observables import AddObservables, GetAllObservables
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.simple_ed.basis import countBits

def sort(ux,uy):
    index = np.argsort(ux)
    return ux[index], uy[index]

class InteractiveHistogramPlots(HistogramPlots):

    def __init__(self):
        HistogramPlots.__init__(self,"")
        self.record = {}
        self.on = {}
        self.mb_states = {}

    def Plot1(self, ax):
        y = [0.0 for _i in range(self.full_dim+1)]
        
        for conf in range(self.hist.num_configs):
            N = self.hist.N[conf] 
            y[N] += self.hist.p[conf]
        
        x=[]
        yn=[]
        for iy, p in enumerate(y):
            if p > 1e-4:
                x.append(iy)
                yn.append(p)
        y=yn        


        colors = cm.rainbow(np.linspace(0, 1, len(x)))
        for i, ix in enumerate(x):
            ax.bar(ix, y[i], picker=True, color = colors[i])
        
        ax.set(xlabel="N", ylabel="probability")
        ax.legend()


    def DisplayMBState(self, color, value, exp, ax):
        record_key = str(color)
        record_sub_key = str(exp)

        values = np.array(list(self.mb_states[record_key][record_sub_key].keys()))
        dif = np.abs(values-value)
        key = values[np.argmin(dif)]

        states = np.array(self.mb_states[record_key][record_sub_key][key])
        ps = np.array([np.abs(self.hist.p[s]) for s in states])

        ps,states = sort(ps,states)
        states = states[::-1]
        ps = ps[::-1]
        
        ax.clear()
        for i, p in enumerate(ps):
            ax.scatter(i, p, marker='.', color = color, picker=True)
        ax.set_yscale('log')

        ax.set_xticks([])
        ax.set_xticklabels([])
        ax.set(xlabel=f"Many body states w/ {record_sub_key}={key}", ylabel="probability")
        ax.yaxis.set_label_position("right")
        ax.yaxis.tick_right()

        self.current_bin_states = states
        self.current_bin_states_probability = ps
        
    def AnnotateMBState(self, value, ax_to_highlight):
        
        i=int(round(value))
        p = self.current_bin_states_probability[i]
        n = len(self.current_bin_states_probability)

        for child in ax_to_highlight.get_children():
            if isinstance(child, Annotation):
                child.remove()
        ax_to_highlight.annotate('', xy=(i, p), xytext = (i+0.1*n, 2*p), ha='center', va='center',arrowprops=dict(arrowstyle="->",
                            connectionstyle="arc3"))

        return self.current_bin_states[i]


    def SubPlot(self, number, normalization, ax, obs, exp, color, opts):
        record_key = str(color)
        record_sub_key = str(exp)

        if record_key in self.record.keys():
            if record_sub_key in self.record[record_key].keys():
                self.on[record_key][record_sub_key] = not self.on[record_key][record_sub_key]

        else:
            self.record[record_key] = {}
            self.on[record_key] = {}
            self.mb_states[record_key] = {}

        if not (record_key in self.record.keys() and record_sub_key in self.record[record_key].keys()):

            m = Matrix(obs.evaluate(exp))

            to_plot={}
            self.mb_states[record_key][record_sub_key] = {}

            for conf in range(self.hist.num_configs):
                p = np.abs(self.hist.p[conf])
                    
                numBits = countBits(conf)
                if numBits == number:
                    
                    denmat = np.zeros_like(m)
                    bits = conf
                    n = 0
                    total = 0
                    while bits != 0:
                        b = bits & 0x1
                        if b == 1: 
                            denmat2 = np.copy(denmat)
                            denmat2[n,n] = 1.0
                            value = np.trace(m * denmat2)[0,0]
                            total+=value.real
                        bits = bits >> 1
                        n +=1
                    total = np.round(total, 4)
                    
                    if total in to_plot.keys():
                        to_plot[total] += p
                        self.mb_states[str(color)][record_sub_key][total].append(conf)
                    else:
                        to_plot[total] = p
                        self.mb_states[str(color)][record_sub_key][total] = [conf]
                    
            x = []
            y = []
            for key in to_plot.keys():
                if to_plot[key] > 1e-9:
                    x.append(key)
                    y.append(to_plot[key])

            x, y = sort(np.array(x), np.array(y))

            if opts.normalize_height:
                y /= normalization

            self.record[record_key][record_sub_key] = (x,y,color)
            self.on[record_key][record_sub_key] = True

        num = 0
        for key in self.on.keys():
            if self.on[key][record_sub_key]:
                num += 1

        dx=1e14
        for key in self.on.keys():
            x,y,color = self.record[key][record_sub_key]
            
        ax.clear()
        i = -0.5*(num-1)
        for key in self.on.keys():
            x,y,color = self.record[key][record_sub_key]
            try:
                dxs = np.min(x[1:] - x[:-1])
            except:
                dxs = 0.5
            dx=min(dx, dxs)

            width = 0.9*(dx)
            shift = 0
            if opts.normalize_width:
                width /= num
                shift = i*width

            if self.on[key][record_sub_key]:
                for ix in range(len(x)):
                    ax.bar(x[ix]+shift, y[ix], color = color, width=width, picker = True)
                i+=1
        
        if opts.normalize_height:
            ax.set(xlabel=f"{exp}", ylabel=f"p({exp})/p(N)")
        else:
            ax.set(xlabel=f"{exp}", ylabel=f"p({exp})")
        
                    
if __name__ == '__main__':

    parser = ArgumentParserThatStoresArgv('crystal-field', add_help=True,
                            parents=[GetMPIProxyOptionsParser()],
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument("--normalize-widths", dest="normalize_width", default=False, action="store_true",
                        help="""Offset bars and shrink widths so they fit side-by-side (if they share values)
                        """)

    parser.add_argument("-H", "--normalize-heights", dest="normalize_height", default=False, action="store_true",
                        help="""Normalize sub-plot heights by p(N)""")
    
    parser.add_argument("-Q", "--quantum-numbers", dest="qn", default="",
                      help="Quantum numbers to use. By default, 'j,Jm' is used on rel., 'Lm,Sm' in non-rel, but any observables can be used ")
                      
    parser.add_argument("--eimp",
                        dest="eimp_not_eqp",
                        default=False,
                        action="store_true",
                        help = "use Eimp, not Eqp_loc to calculate the plitting")

    parser.add_argument("--from-real-axis", dest="from_real_axis",
                    action="store_true",
                    default=False,
                    help = "When constructing a quasiparticle hamiltonian, use Z's computed from the derivative of the real-axis self energy (dmft)")

    # for the loaded real self-energy
    parser.add_argument("-W", "--window",
                    dest="omega_window",
                    default=10.0,
                    type=float,
                    help = "omega energy window (-w to w) in eV") 
     
    parser.add_argument("--which-shell", dest="which_shell", default=0, type=int)

    opts = parser.parse_args(sys.argv[1:])
 
    proj_location = "./projector.h5:/def/"
    proj = SelectedOrbitals()
    proj.load(proj_location)
    
    obs = GetAllObservables(
            proj.shells[opts.which_shell],
            subspace_expr = proj.subspace_expr,
            printout=False)

    hp = InteractiveHistogramPlots()

    if opts.qn == "":
        if hp.nrel > 1:
            exprs = ["j","Jm"]
        else:
            exprs = ["Lm","Sm"]
    else:
        exprs = opts.qn.split(",")

    fig, axes = plt.subplots(1+len(exprs),2)
    gs = axes[0, 1].get_gridspec()
    # remove the underlying axes
    for ax in axes[1:, -1]:
        ax.remove()
    axbig = fig.add_subplot(gs[1:, -1])

    opts.plot = True
    runner = CFRunner(opts, axbig, fig)
    runner.Run()
    runner.plot.ax.yaxis.set_label_position("right")
    runner.plot.ax.yaxis.tick_right()
    runner.plot.ax.legend(loc="upper left")

    hp.Plot1(axes[-1,0])
    AddedObs = False

    def on_pick(event):
        # On the pick event, find the original line corresponding to the legend
        # proxy line, and toggle its visibility.
        artist = event.artist
        try:
            x = artist.xy[0] + artist._width*0.5
            y = artist._height
        except:
            artist.set_offset_position('data')
            xy = artist.get_offsets()
            x = xy[0,0]
            y = xy[0,1]

        c = artist.get_facecolor()

        global AddedObs
        if artist.axes == axes[-1,0]:
            if not AddedObs:
                AddObservables(obs, runner.svs, opts)
                AddedObs = True
            for i, expr in enumerate(exprs):
                hp.SubPlot(x,y,axes[i,0],obs,expr,c, opts)

        else:
            for i, expr in enumerate(exprs):
                if artist.axes == axes[i,0]:
                    hp.DisplayMBState(c,x,expr,axes[0,1])

                elif artist.axes == axes[0,1]:
                    state = hp.AnnotateMBState(x, axes[0,1])
                    runner.plot.place_mb_state(state,c) 


        fig.canvas.draw()
        
    import matplotlib
    matplotlib.use("TkAgg")

    fig.canvas.mpl_connect('pick_event', on_pick)
    plt.show()