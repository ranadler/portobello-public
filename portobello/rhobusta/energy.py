#!/usr/bin/env python3

'''
Created on Jul 25, 2019

@author: adler
'''
            
import os
from portobello.bus.mpi import GetMPIProxyOptionsParser
from portobello.jobs.Organizer import with_organizer
from portobello.rhobusta.PEScfEngine import Ry2eV
from portobello.generated.LAPW import BandStructure
import sys
from scipy.optimize import curve_fit

import numpy as np
from itertools import product
from portobello.bus.Matrix import Matrix
from portobello.generated.energy import FreeEnergy
from portobello.generated.dmft import ImpurityMeasurement
from portobello.rhobusta.dft_plus_sigma import EmbedSigma
from portobello.rhobusta.loader import AnalysisLoader
from portobello.rhobusta.rhobusta import RhobustaOptionsParser

from portobello.generated.dmft import DMFTFreeEnergy
from portobello.generated.FlapwMBPT_basis import MuffinTinBasis, MuffinTin

class DensityRunner(AnalysisLoader):
    def __init__(self, opts):
        AnalysisLoader.__init__(self,opts)
        self.MTScale = opts.mt_scale
        self.ZOZ_threshold = opts.ZOZ_threshold
    
    def GetLowBands(self):
        return self.svs.plugin.GetBandsForAllKpoints(self.num_low_bands,  self.svs.orbBandProj.min_band -1 -1)
    def GetHighBands(self):
        return self.svs.plugin.GetBandsForAllKpoints(self.svs.orbBandProj.max_band + 1 - 1, self.num_high_bands)
    def BelowThreshold(self, x):
        if abs(x) < self.ZOZ_threshold:
            return x
        return 0.0

    def TotalMTDensityInBands(self, KS : BandStructure, project : bool=False):
        total = 0.0
        total_itinerant = 0;
        projected = 0.0
        weight = self.svs.lapw.weight     
        MPI = self.mpi
        for k, si in product(range(self.svs.dft.num_k), range(self.svs.dft.num_si)):  
            ferm = np.array(self.svs.ferm( (KS.energy[:, k, si]-KS.chemical_potential)*Ry2eV )) # length is number of bands  
            if project: 
                Pk = self.svs.OneAtomProjector(k, si)
                projected += np.real(np.trace(Pk.H * np.diag(ferm) * Pk)[0,0]) * weight[k]
            Z = Matrix(KS.Z[:,:, k, si]) # num_muffin_orbs x num_expanded
            M = np.diag(Z.H * self.overlap * Z) # for each num_expanded (band) - we don't need the cross-bands elements
            M2 = [self.BelowThreshold(x) for x in M.flat]
            total += np.real(np.dot(M, ferm)[0,0]) * weight[k]
            total_itinerant +=  np.real(np.dot(M2, ferm))* weight[k]
        if self.svs.dft.is_sharded:
            total = MPI.COMM_WORLD.allreduce(total, op=MPI.SUM)
            total_itinerant = MPI.COMM_WORLD.allreduce(total_itinerant, op=MPI.SUM)
            if project:
                projected = MPI.COMM_WORLD.allreduce(projected, op=MPI.SUM)
        if project:
            return total, total_itinerant, projected
        else:
            return total, total_itinerant
        

    def MPIWorker(self):
        self.InitializeMPI()
        method = self.LoadCalculation(loadRealSigma=False)


        loc = "./flapw_basis.core.h5:/"
        basis = MuffinTinBasis()
        basis.GetMtBasis(loc)
        basis.load(loc)

        #for ishell, shell in enumerate(orbitals.shells):
        #    isort = dft.st.distinct_number[ shell.atom_number ] - 1
        #    tin = mtb.tins[isort]


        if self.opts.num_high_energy_bands < 0:
            self.num_high_bands = self.svs.dft.num_bands
        else:
            self.num_high_bands = min(self.svs.orbBandProj.max_band + self.opts.num_high_energy_bands, self.svs.dft.num_bands)
        if self.opts.num_low_energy_bands < 0:
            self.num_low_bands = 0
        else:
            self.num_low_bands = max(self.svs.orbBandProj.min_band - 2 -  self.opts.num_low_energy_bands, 0)
        self.svs.InitOneBody()
        self.overlap = Matrix(self.svs.lapw.mo_overlap) * self.MTScale

        total = 0.0
        total_itinerant = 0.0
        KS = self.GetLowBands()
        lowMT, itinerant = self.TotalMTDensityInBands(KS)
        if self.IsMaster():
            print(f"Total in semicore MT bands: {lowMT}")
            print(f"itinerant in semicore MT bands: {itinerant}")
        total += lowMT
        total_itinerant += itinerant
        del KS

        KS = self.svs.orbBandProj.GetBandStructureInWindow()
        windowMT, itinerant, Nlatt = self.TotalMTDensityInBands(KS, project=True)
        if self.IsMaster(): 
            print(f"Total in window MT bands: {windowMT}")
            print(f"Nlatt={Nlatt}, Nimp={self.svs.Nimp}")
            print(f"itinerant in semicore MT bands: {itinerant}")
        total += windowMT
        total_itinerant += itinerant

        KS = self.GetHighBands()
        highMT, itinerant = self.TotalMTDensityInBands(KS)
        if self.IsMaster(): 
            print(f"Total in excited MT bands: {highMT}")
            print(f"itinerant in excited bands: {itinerant}")
        total += highMT
        total_itinerant += itinerant
        del KS
    
        total += (self.svs.Nimp - Nlatt)*(self.svs.num_equivalents+self.svs.num_anti_equivalents)  #  = Nimp - tr P(rho_{KS})
        if self.IsMaster():
            print(f"Total density in MT: {total}\n")
            print("Measures of itineracy")
            print("---------------------")
            print(f"Total density in interstitial: {self.svs.dft.num_valence_electrons - total}")
            # itinerant bands are those whose support on the MT cells is <= ZOZ_threshold
            print(f"Total 'itinerant' band occupation: {total_itinerant}")
            print(f"Nimp-Nlatt itineracy: {self.svs.Nimp-Nlatt}")
        self.Terminate()


class FERunner(AnalysisLoader):
    def __init__(self, opts):
        AnalysisLoader.__init__(self,opts)

    def MPIWorker(self):
        self.InitializeMPI()

        method = self.LoadCalculation(loadRealSigma=False)
            
        Vdc = self.svs.DC
        # Edc does NOT need to be subtracted from gutz: it is already accounted for in the impurity
        # as it is subtracted in the embedding Hamiltonian
        

        st = self.svs.plugin.GetStructure()
        num_atoms = len(st.sites)

        if method == "gutz":
            gutz = self.svs
            muChoice = gutz.MuChoice() # 1.0 iff mu is substracted from the epsAllK already. typically 0 in gutz.
            # Important: Here we add mu if it was subtracted, because we are re-building the band:
            # this is Hqp that substitutes eps_k, and therefore has to be absolute w.r.t mu

            spf =  2.0/(gutz.num_si * gutz.nrel)
            Eimp = gutz.Elocal - gutz.ID*gutz.mu*(1-muChoice)
            trLogHloc = 0.0
            for si in range(self.svs.num_si):
                for i in range(self.svs.dim):
                    be = self.svs.beta*np.real(Eimp[i,i,si])
                    if be < -100:
                        trLogHloc += be
                    elif be < 0.0:
                        trLogHloc += be - np.log(1.0+np.exp(be))
                    elif be < 100.0:
                        trLogHloc += -np.log(1+np.exp(-be))
                    # else it is 0 for high be
            trLogHloc /= self.svs.beta
            trLogHloc += gutz.mu * (1-muChoice) * gutz.Nlatt
            trLogHloc *= (1.0 + gutz.num_anti_equivalents)*spf
            


            FG = FreeEnergy(num_k_irr=gutz.dft.num_k_irr, num_si=gutz.num_si, 
                            corr_energy_term = 0,
                            num_atoms = num_atoms,
                            min_band = gutz.orbBandProj.min_band, 
                            max_band = gutz.orbBandProj.max_band)
            FG.allocate()
        
            HlocCorr = gutz.ZeroLocalMatrix()
            for k, si in gutz.irr_k_si_pairs():
            
                # we make all calculations in band basis, which is why we use HkBands, and embed R,L into bands
                HkB = Matrix(gutz.HkBands[k, :,:, si])
                
                I = np.eye(gutz.num_bands, gutz.num_bands, dtype=np.complex128)
                R = Matrix(I)
                R += gutz.EmbeddedIntoKBandWindow(gutz.R - gutz.ID, k, si)
                L = Matrix(gutz.EmbeddedIntoKBandWindow(gutz.Lambda, k, si))
            
                # Quasi particle hamiltonian
                Hqp = R * HkB * R.H + L + gutz.mu *I*muChoice 
        
                Eqp = self.svs.DiagonalEienvalues(Hqp, complex=False)
            
                assert(FG.Hqp.shape[0] == Eqp.shape[0])
                FG.Hqp[:,k, si] = Eqp[:]
                Pk = gutz.OneAtomProjector(k, si) 
                HlocCorr[:,:,si] +=  Pk.H * Hqp * Pk  * gutz.lapw.weight[k] 
            
            print(f"mu: {gutz.mu} mu choice: {muChoice}")
            HlocCorr -= gutz.ID*gutz.mu*(1-muChoice)
            gutz.SymmetrizeCorrelated(HlocCorr, hermitian=True, raw=True)
      
            trLogHlocCorr = 0.0
            for si in range(self.svs.num_si):
                for i in range(self.svs.dim):
                    be = self.svs.beta*np.real(HlocCorr[i,i,si])
                    if be < -100:
                        trLogHlocCorr += be
                    elif be < 0.0:
                        trLogHlocCorr += be - np.log(1.0+np.exp(be))
                    elif be < 100.0:
                        trLogHlocCorr += -np.log(1+np.exp(-be))
                    # else it is 0 for high be
            trLogHlocCorr /= self.svs.beta
            trLogHlocCorr += gutz.mu * (1-muChoice) * gutz.Nimp
            trLogHlocCorr *= (1.0 + gutz.num_anti_equivalents)*spf
            
            print("Hloc-correlated:")
            print(Matrix(HlocCorr[:,:,0]))
            
            # ignore + trLogHlocCorr - trLogHloc
            FG.trLogHloc = trLogHloc
            FG.trLogHlocCorr = trLogHlocCorr
            #FG.corr_energy_term= np.real(gutz.energyImp - trLogHloc +  trLogHlocCorr) * gutz.num_equivalents,
            FG.corr_energy_term= np.real(gutz.energyImp) * gutz.num_equivalents,
               
            FG.store('./energy.h5:/',flush=True)
            
            EmbedSigma()
            
            FG.CalculateFreeEnergyGutz('')
            
        elif method == "dmft":
            
            self.svs.ResizeAll(self.svs.sig.num_omega*self.opts.frequency_factor)

            Eimp = self.svs.Elocal[:,:,:]  + self.svs.sig.moments[0,:,:,:] - self.svs.DC[:,:,:] 

            # calculate trLnGx
            trLnGx = 0.0
            for si in range(self.svs.num_si):
                for i in range(self.svs.dim):
                    be = self.svs.beta*np.real(Eimp[i,i,si])
                    if be < -100:
                        trLnGx += be
                    elif be < 0.0:
                        trLnGx += be - np.log(1.0+np.exp(be))
                    elif be < 100.0:
                        trLnGx += -np.log(1+np.exp(-be))
                    # else it is 0 for high be
            trLnGx /= self.svs.beta

            trGdeltaG = 0.0
            trLnGc = 0.0
    
            if self.opts.GK_functional or self.opts.KH_functional:

                # Gloc(iw) = P G(iw), and we add the term trGdeltaG which corresponds to trace of the DMFT equation multiplied by Gloc
                for si in range(self.svs.num_si):
                    for iomega in range(self.svs.num_freqs):
                        w = self.svs.sig.omega[iomega] * 1j
                        Gloc = self.svs.CorrelatedG(w, self.svs.sig.M[iomega,:,:,:], si)
                        GlocI = Gloc.I
                        iw = self.svs.ID1*w
                        sigc  = Matrix(iw - GlocI - Eimp[:,:,si]) # this is self energy relative to Sigma[inf], computed from Gloc(iw) = P(G)(iw)
                        gx    = Matrix(iw - Eimp[:,:,si]).I
                        argiw = Matrix(self.svs.ID1 -  sigc * gx)
                        w, _unitary = np.linalg.eigh(argiw.H*argiw)
                        trLnGc += np.real(np.sum(np.log(w))) #factor of two to get negative iomegas, factor of half from formula 

                        if self.opts.GK_functional:
                            EimpI = iw - self.svs.Elocal[:,:,si] + self.svs.DC[:,:,si] - self.svs.sig.M[iomega,:,:,si] - self.svs.hyb.M[iomega,:,:,si]
                            trGdeltaG += np.real(np.trace(Matrix(EimpI-GlocI) * Gloc)[0,0])

                trGdeltaG /= self.svs.beta
                trLnGc /= self.svs.beta
            
            else:
               
                # Gloc(iw) = Gimp assumed
                iw = 1j*self.svs.sig.omega
                for si in range(self.svs.num_si):
                    for i in range(self.svs.dim):
                        gx = 1.0/(iw - Eimp[i,i,si])
                        sigc = self.svs.sig.M[:,i,i,si] - self.svs.sig.moments[0,i,i,si] + self.svs.hyb.M[:,i,i,si]
                        #if self.IsMaster(): print((sigc*gx)[-10:])
                        arg = 1.0 - sigc*gx 
                        arg = np.log(arg*np.conj(arg))
                        trLnGc += np.real(np.sum(arg)) #factor of two to get negative iomegas, factor of half from formula 
                trLnGc /= self.svs.beta
                
            trLnGloc = (trLnGx - trLnGc) # spin factor included in fortran for this and F_imp = IMP_F.result

            if self.svs.num_si == 1 and self.svs.nrel == 1:
                trLnGloc*=2

            EmbedSigma() # prepares the embedded self energy for fortran

            IMP_F =  DMFTFreeEnergy()

            if self.IsMaster():
                IMP_F.load("./dmft_free_energy.h5:/")
                print("impurity internal energy", IMP_F.internal_energies[0])
                print(f"Trace Log Gloc={trLnGloc} trLnGx={trLnGx} trLnGc={trLnGc}")
                print("Trace Gloc*(Gimp^-1 - Gloc^-1)", trGdeltaG)
            FG = FreeEnergy(num_k_irr=self.svs.dft.num_k_irr, num_si=self.svs.num_si,
                            corr_energy_term = (IMP_F.result - trLnGloc + trGdeltaG) * (self.svs.num_equivalents+self.svs.num_anti_equivalents),
                            num_atoms = num_atoms,
                            min_band = 1,
                            max_band = 0)

            FG.allocate()
            FG.store('./energy.core.h5:/',flush=True)
            self.Barrier()
            FG.CalculateFreeEnergyDMFT('')

        else:

            FG = FreeEnergy()
            FG.CalculateFreeEnergyDFT('')

        self.Terminate()

def EnergyOptionParser():
    parser = GetMPIProxyOptionsParser(private=True)

    parser.add_argument("--which-shell", dest="which_shell", default=0,  type=int,
                    help="which shell (index) to project to, in order to build the correlated problem")

    # the rest of options are for density calculation

    parser.add_argument("--density", 
                        dest="density_calculation", 
                        default=False, 
                        action="store_true",
                        help = "print densities outside and inside MT")
    
    parser.add_argument("--GK-functional", 
                        dest="GK_functional", 
                        default=False, 
                        action="store_true",
                        help = "add dmft full functional (GK-review style) terms, which cancel at the saddle point")
    
    parser.add_argument("--KH-functional", 
                        dest="KH_functional", 
                        default=False, 
                        action="store_true",
                        help = "add the trlogGloc not trlogGimp")
    
    parser.add_argument("--num-high-energy-bands",
                    dest="num_high_energy_bands",
                    default=20,
                    type=int,
                    help = "Number of bands above the correlated window to include")

    parser.add_argument("--num-low-energy-bands",
                    dest="num_low_energy_bands",
                    default=-1,
                    type=int,
                    help = "Number of bands below the correlated window to include")

    parser.add_argument("--mt-scale",
                    dest="mt_scale",
                    default=1.0,
                    type=float)

    parser.add_argument("--frequency-factor",
                    dest="frequency_factor",
                    default=100,
                    type=int,
                    help = "Expand the summation of the dynamical part of the Green's function by this factor")

    parser.add_argument("--ZOZ-threshold",
                    dest="ZOZ_threshold",
                    default=0.8,
                    help="below this threshold, electrons with this value of Z.H*O*Z are itinerant",
                    type=float)

    parser.add_argument("--directory", dest="dir", 
                        default=".",
                        help="""Directory in which to run. [for organized runs only]""")

    return parser

@with_organizer
def ENERGY(opts):
    EnergyMain([], {}, opts = opts)

def EnergyMain(argv, kwargs, opts = None):

    if opts is None:
        parser = EnergyOptionParser()
        (opts, args) = parser.parse_known_args(argv)

    curdir = os.getcwd()
    if hasattr(opts,"dir"):
        try:
            os.chdir(opts.dir)
        except:
            pass
        
    if not opts.density_calculation:
        runner = FERunner(opts)
        runner.Run(wait=True)
    else:
        runner = DensityRunner(opts)
        runner.Run(wait=True)

    os.chdir(curdir)

if __name__ == '__main__':
    EnergyMain(sys.argv[1:], {})

