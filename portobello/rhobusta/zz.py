#!/usr/bin/env python3
'''
Created on Dec 4, 2020

@author: adler
'''
import sys
from argparse import ArgumentDefaultsHelpFormatter
from portobello.bus.mpi import GetMPIProxyOptionsParser
from pathlib import Path
from typing import List

import numpy as np
from portobello.generated.atomic_orbitals import SelectedOrbitals
from portobello.generated.dmft import DMFTState
from portobello.rhobusta.dmft import DMFTOrchastrator
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.loader import AnalysisLoader
from portobello.rhobusta.observables import Print
from scipy.interpolate import InterpolatedUnivariateSpline
from portobello.rhobusta.PEScfEngine import PEScfEngine
from portobello.rhobusta.models.PEScfEngine import PEScfEngineForModels

def OutputZImag(dmft : DMFTState, opts):
    mesh = DMFTOrchastrator.GenerateMeshForSigDeriv(dmft.beta)

    dmft.correctedRep = dmft.CorrectRep()
    num_orbs = np.max(dmft.correctedRep)       
    indices : List[[int,int]] = [None for _i in range(num_orbs)]       
    for i in range(dmft.dim):
        for j in range(dmft.dim):
            r = dmft.correctedRep[i,j]
            if r > 0 and indices[r-1] is None:
                indices[r-1] = (i,j)
    
    F = np.zeros((num_orbs,dmft.num_si,dmft.sig.M.shape[0]), dtype=np.float64)
    S = np.zeros((num_orbs,dmft.num_si,mesh.shape[0]), dtype=np.float64)
          
    for orb in range(num_orbs):
        i1,i2 = indices[orb]
        for si in range(dmft.num_si):
            for i in range(dmft.sig.M.shape[0] ):
                # note that we extrapolate the full M -DC with 0's outside the domain (which is the correlated window)
                # because outside the correlated window the effective self energy is 0.0
                # by definition
                F[orb,si,i] = dmft.sig.M[i, i1, i2, si].imag  # - self.ID[i1,i2,si]* 1j * inp.gammc*Ry2eV
            # Now interpolate F
                        
            S[orb,si,:] = DMFTOrchastrator.SigExtrapolate(F[orb,si,:], dmft.sig.omega[:], mesh)
            #spl = InterpolatedUnivariateSpline( self.sig.omega[:], F[orb,si,:])
            #S[orb,si,:] = spl(mesh)

    if opts.plots:
        import matplotlib
        matplotlib.use("TkAgg")
        import matplotlib.pyplot as plt
        fig,ax = plt.subplots(1,1)
        for i in range(num_orbs):
            ax.plot(mesh,S[i,si,:], "-", alpha=1)
            ax.plot(dmft.sig.omega[:10], F[i,si,:10], "--", alpha=1)
        ax.set_title("Interpolated imaginary axis  Σ(iω) (zoom to 0.0)")
        plt.show()
       
    if opts.model:
        vals, orbs = PEScfEngineForModels.Print(dmft)
    else:
        vals, orbs = PEScfEngine.Print(dmft)

    raw = (F[:,:,1] - F[:,:,0])/(dmft.sig.omega[1]-dmft.sig.omega[0])

    for si in range(dmft.num_si):
        slope = np.real(S[:,si,1] - S[:,si,0])/(mesh[1]-mesh[0])
        Z=1.0/(1.0-slope)
        Zraw=1.0/(1-raw[:,si])
        print("Σ(iω) (matsubara) estimate of Zs: ")
        for oi, orb in enumerate(orbs):
            if raw[oi,si] >=0:
                print(f"warning: Im Σ(iω) not decreasing at 0 (slope is {raw[oi,si]:.5f})")
            from pylatexenc.latex2text import LatexNodes2Text
            if opts.noninter:
                print(f"Z ({LatexNodes2Text().latex_to_text(orb)}):\t\t\t{Z[oi]:.5f}\t\t\tnon-interpolated:\t{Zraw[oi]:.5f}")
            else:
                print(f"Z ({LatexNodes2Text().latex_to_text(orb)}):\t\t\t{Z[oi]:.5f}")

def ZZ(dmft, opts):
    if not Path("./real-imp-self-energy.h5").exists():
        print("this program requires a real-variable self-energy file (real-imp-self-energy.h5) in the directory")
       
    OutputZImag(dmft, opts)
    dmft.InitOneBody()
    dmft.LoadSigRealOmega()
    dmft.CalculateZValues(print_noninter=opts.noninter)

if __name__ == '__main__':

      
    parser = ArgumentParserThatStoresArgv('zz', parents=[GetMPIProxyOptionsParser()], add_help=True,formatter_class=ArgumentDefaultsHelpFormatter)
    has_file = Path("./ini.h5").exists()
        
    parser.add_argument("--plots", dest="plots",
                      action="store_true",
                      default=False,
                      help="show interpolation plots") 

    parser.add_argument("--non-interpolated", dest="noninter",
                      action="store_true",
                      default=False,
                      help="print non interpolated derivative estimates") 

    parser.add_argument("--qp", dest="qp_analysis",
                      action="store_true",
                      help="print summary of fermi-surface properties",
                      default=False)

    parser.add_argument("--add-hyb", dest="add_hyb",
                      action="store_true",
                      help="add hyb to the self energy",
                      default=False)

    parser.add_argument("-W", "--window",
                    dest="omega_window",
                    default=10.0,
                    type=float,
                    help = "omega energy window (-w to w) in eV") 
     
    parser.add_argument("--which-shell", dest="which_shell", default=0, type=int,
                    help="which shell (index) to project to, in order to build the correlated problem")

    parser.add_argument("--model", dest="model", default=False, action="store_true",
                    help="model, not material")

    (opts, args) = parser.parse_known_args(sys.argv[1:])
     
    analysis = AnalysisLoader(opts)
    method = analysis.LoadCalculation(loadRealSigma=False)

    if method == "gutz":
        PrintMatrix(analysis.svs.Z[:,:,0])
        #quit()

    if opts.qp_analysis:
        assert(analysis.svs is not None)
        analysis.svs.QPAnalysis(label=analysis.svs.GetMethodName())
    else:
        ZZ(analysis.svs, opts)
