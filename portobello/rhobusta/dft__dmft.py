#!/usr/bin/env python3
# PYTHON_ARGCOMPLETE_OK

'''
Created on Aug 19, 2018

@author: adler
'''
import sys
import os
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.dmft import DMFT, DMFTOptionsParser
from portobello.rhobusta.rhobusta import DFTPlusSigma, RhobustaOptionsParser, DFT0
from portobello.rhobusta.DFTPlugin import DFTPlugin
from portobello.rhobusta.ProjectorEmbedder import DefineProjector, TestProjector,\
    ProjectorOptionsParser
from argparse import ArgumentDefaultsHelpFormatter
from portobello.rhobusta.options import SaveOptions, RefreshOptions
#import argcomplete
from time import sleep
from portobello.bus.persistence import Persistent
from portobello.bus.mpi import MPIContainer
from portobello.jobs import Organizer 
from portobello.jobs.Organizer import with_organizer
from portobello.generated.MI import MultiImpuritySupervisor
from portobello.rhobusta.multi_impurity import MultiImpurityEmbedder
from portobello.rhobusta.wannier import MainWannier


def DFTPlusDMFTOptionsParser(prepArgs, add_help=False):
    
    parser = ArgumentParserThatStoresArgv('dft+dmft', add_help=add_help, conflict_handler='resolve',
                            parents=[RhobustaOptionsParser(), 
                                     ProjectorOptionsParser(),
                                     DMFTOptionsParser(prepArgs)],
                            formatter_class=ArgumentDefaultsHelpFormatter)    
   
    #argcomplete.autocomplete(parser)
    parser.add_argument("-I", "--super-iters", dest="num_super_iterations", 
                     default=20, type=int,
                     help="number of iterations of the charge self consistency equations, negative numbers -> start DMFT first (even if dmft file exists)")
  
    parser.add_argument("-y", "--dft-plus-iters", dest="dft_plus_sigma_iters", 
                     default=20, type=int,
                     help="number of iterations of the charge self consistency equations in subsequent dft+dmft iterations")
  
    parser.add_argument("--force-dmft-first", dest="force_dmft_first", 
                      action="store_true",
                      help="force dmft to run first even if the dmft file exists",
                      default=False)
    
    parser.add_argument("--reconstruct", dest="reconstruct", 
                      action="store_true",
                      help="run DFT from input with new parameters, re-define projector, and re-converge dft+sigma",
                      default=False)

    parser.add_argument("--directory", dest="dir", 
                      default=".",
                      help="Run in the given directory")
    return parser

@with_organizer
def DFT__DMFT(opts):
    DFT__DMFT0([], {}, opts = opts, wait = False)

def DFT__DMFT0(args, kwargs, opts = None, wait = True):

    curdir = os.getcwd()
    os.chdir(opts.dir)

    KB = 6.333328e-6 *27.2107* 0.5 # collected from Kutepov's code, to match perfectly

    if opts.Temperature is not None and opts.beta is None:
        opts.beta = 1.0/(opts.Temperature * KB)
    if opts.Temperature is None:
        opts.Temperature = 1.0/opts.beta / KB
    assert(opts.Temperature is not None)
    assert(opts.mpi_workers > 0) # following CTQMC code is designed for at least 1 worker
       
    DFTPlugin.SetGw()
    
    if opts.reconstruct:
        assert(os.path.exists(opts.input_file))
        assert(not os.path.exists("./ini.h5"))
        assert(not os.path.exists("./projector.h5"))
        assert(os.path.exists("./dmft.h5"))
       
    SaveOptions(opts)

    if opts.input_file is not None:
        DFT0(opts) # can override argument here if needed by adding args

    # after the first DFT, all further DFT+X are NOT spin polarized funcionals:
    opts.dft_spin_polarized = False   
    opts.plus_sigma = True
    opts.gutz = False
    opts.with_full_green = True
    opts.input_file = None
    SaveOptions(opts)    

    mpi = MPIContainer(opts)
    plugin = DFTPlugin.Instance(restart=True, mpi=mpi)
    
    if opts.wannier and not os.path.exists("./wannier.h5"):
        MainWannier([],{},opts)

    elif not os.path.exists("./projector.h5"): 
        DefineProjector(opts)
        p = TestProjector(ewin_str=opts.window)
    
    num_impurities=1
    if os.path.exists("./multi-impurity.h5"):
        mi = MultiImpuritySupervisor()
        mi.load("./multi-impurity.h5:/")
        num_impurities = mi.num_impurities

    if not os.path.exists("./dmft.h5") or opts.force_dmft_first:
        for i in range(num_impurities):
            print(f"Working on impurity {i}")
            opts.which_shell = i
            DMFT(opts)

    if wait:
        Organizer.Wait()

    si = 0
    while True:
        sleep(5) # for job control to realize that DMFT workers are done
        RefreshOptions(opts)
        si += 1
        if si > opts.num_super_iterations: break
        
        DFTPlusSigma(opts)
        
        RefreshOptions(opts)
        if opts.reconstruct:
            break
         
        Persistent.Refresh(["./dmft.h5:/", "./history.h5:/"])  # reopen to make sure we read the latest
        DFTPlugin.Instance().Reload() # reload the memory image of this process' DFT

        for i in range(num_impurities):
            print(f"Working on impurity {i}")
            opts.which_shell = i
            if opts.wannier:
                MainWannier([],{},opts)
            DMFT(opts)

        if wait:
            Organizer.Wait()   

    os.chdir(curdir)
    if wait:
        Organizer.Terminate()

def main():
    parser = DFTPlusDMFTOptionsParser(sys.argv[1:], add_help=True) 
    
    # all args should be recognized
    opts = parser.parse_args(sys.argv[1:])
    DFT__DMFT0({},[],opts)
    
if __name__ == '__main__':
    main()
    
