#!/usr/bin/python3


'''
Created on Jan 31, 2020

@author: adler
'''
from optparse import OptionParser
import sys
from pathlib import Path
from portobello.rhobusta.orbitals import PrintMatrix
from portobello.generated.gutz import GState
from portobello.bus.Matrix import Matrix
import numpy as np
from collections import defaultdict

import matplotlib.pyplot as plt
from portobello.generated.PEScf import SolverState
import pathlib

def ToBits(sol: SolverState, a):
    ret = ""
    while a != 0:
        if a & 0x1:
            ret += "1" 
        else:
            ret += "0" 
        a = a >> 1
    ret += "0" *(sol.dim - len(ret))
    return ret
    

def DrawNHistogramDMFT(dmft : SolverState, opts, ax):
    Nlines = defaultdict(list)
    nc = dmft.hist.num_configs
    
    table = np.zeros((nc, 3), np.float64) ## for each config its N, P, E
    
    maxE = -1.0e20
    minE = -maxE
    entropy = 0.0
    eAv = 0.0
    e2Av = 0.0
    
    for c, p in enumerate(dmft.hist.p):
     
        if p < 1.0e-5:
            continue
        entropy += -p*np.log(p)

        N = dmft.hist.N[c]
        E = np.real(dmft.hist.energy[c])
        eAv += E * p 
        e2Av += E*E*p
        maxE = max(maxE, E)
        minE = min(minE, E)
        table[c, 0] = N
        table[c, 1] = p
        table[c, 2] = E
        Nlines[N].append((np.real(E),np.real(p)))
    
    totlow = 0.0
    for c, p in enumerate(dmft.hist.p):
        E = np.real(dmft.hist.energy[c])
        if E-minE <0.01:
            print(f"p={p} N={dmft.hist.N[c]} state={ToBits(dmft,c)} energy: {E-minE}")
            totlow += p 
    print(f"Total low energy probability: {totlow}")
     
    dE = maxE - minE
    nBins = int(dE / opts.width)
    print(f"number of bins is: {nBins}\n")
    print(f"entropy: {entropy}")
    print(f"standard error: {np.sqrt(e2Av - eAv*eAv)}")
            
    for N in Nlines:
        Nlines[N].sort()
        vals = Nlines[N]
        weights=[v[1] for v in vals]
        counts, bins, patches = ax.hist([v[0] for v in vals], 
                 bins=nBins, weights=weights, 
                 alpha=0.4,
                 rwidth=None,
                 #stacked=False,
                 #histtype='bar',
                 label=f"N={N}")
    
    ax.set_ylabel("total prob. in energy bin")
    ax.set_xlabel("Energy [eV]")
    ax.set_xlim(left=minE-0.5, right=maxE+0.5)
    
    ax.legend(Nlines.keys()) 
    return Nlines

def DrawNHistograms(gutz : GState, ax, opts, H : Matrix):
    Nlines = defaultdict(list)
    nc = gutz.hist.num_configs
    
    maxE = -1.0e20
    minE = -maxE
    table = np.zeros((nc, 3), np.float64) ## for each config its N, P, E
    
    for c, p in enumerate(gutz.hist.p):
        # c is the configuration
        cc = c 
        E = 0.0
        idx = 0
        if p < 1.0e-5:
            continue
        while cc !=0:
            if cc & 1 == 1:
                E += H[idx % gutz.dim, idx % gutz.dim]
            cc >>= 1
            idx += 1
        N = gutz.hist.N[c]
        table[c, 0] = N
        table[c, 1] = p
        table[c, 2] = np.real(E)
        maxE = max(maxE, E)
        minE = min(minE, E)
        Nlines[N].append((np.real(E),np.real(p)))
            
    dE = maxE - minE
    nBins = int(dE / opts.width)
    print(f"number of bins is: {nBins}\n")
    
    for N in Nlines:
        Nlines[N].sort()
        vals = Nlines[N]
        weights=[v[1] for v in vals]
        counts, bins, patches = ax.hist([v[0] for v in vals], 
                 bins=nBins, weights=weights, 
                 alpha=0.6,
                 rwidth=1.0,
                 label=f"N={N}")
    
    ax.set_ylabel("total prob. in energy bin")
    ax.set_xlabel("Energy [eV]")
    ax.set_xlim(left=minE-0.5, right=maxE+0.5)
    
    ax.legend(Nlines.keys()) 
    return Nlines
       # otherwise it is 0
         
def MuChoice():
    if pathlib.Path("./MuNotSubtracted").exists():
        return 1.0
    return 0.0

def ID1(gutz):      
    return np.eye(gutz.dim, dtype=np.complex128)
        
           
def GutzEnergyPlot(gutz : GState, opts, ax):
        
    gutz = GState()
    gutz.load("./gutz.h5:/")
    # HlocQP = R_{i}^{\dagger^{-1}}\lambda_{i}R_{i}^{-1}
    R = gutz.R[:,:,:]
    L = gutz.Lambda[:,:,:]

    dim = gutz.dim*gutz.nrel*gutz.num_si
    HlocQP : Matrix = Matrix(np.zeros((dim, dim)), np.float64)
    if gutz.num_si == 1:
        HlocQP = Matrix(R[...,0]).H.I * (Matrix(L[...,0])-(1.0 - MuChoice()*ID1(gutz))) * Matrix(R[...,0]).I
    else:
        HlocQP[:dim//2, :dim//2] = Matrix(R[...,0]).H.I * (Matrix(L[...,0])-(1.0 - MuChoice()*ID1(gutz))) * Matrix(R[...,0]).I
        HlocQP[dim//2:, dim//2:] = Matrix(R[...,1]).H.I * (Matrix(L[...,1])-(1.0 - MuChoice()*ID1(gutz))) * Matrix(R[...,1]).I
        
    
    PrintMatrix("HlocQP:", HlocQP, max_line_width=400)
    
    
    Eqp = gutz.DiagonalEienvalues(HlocQP, complex=False)
    PrintMatrix("Eqp (diagonal):", Eqp)
        
    DrawNHistograms(gutz, ax, opts, H=HlocQP)

def Postprocess(args, kwargs):
    parser = OptionParser()    
            
    parser.add_option("-W", dest="width",
                      type=float,
                      default=0.1,
                      help="width of histogram bins in eV")
    
    (opts, _args) = parser.parse_args(args)
    
    for key in kwargs:
        setattr(opts, key, kwargs[key])
    
  
    f = ""
    fig, ax = plt.subplots()
    if Path("./gutz.h5").exists():
        
        GutzEnergyPlot(opts, ax)
    elif Path("./dmft.h5").exists():
        None
    else:
        raise Exception("no dmft.h5 or gutz.h5 file around")
        
        
       
    plt.show()

if __name__ == '__main__':
    Postprocess(sys.argv[1:], {})