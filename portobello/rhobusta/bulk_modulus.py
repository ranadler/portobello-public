#!/usr/bin/python3

from re import S
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as optimize
import scipy.interpolate as interp

Ry2eV = 27.2107*0.5 

class EnergyData:
    def __init__(self, free_energies : str, impurity_energies : str,  temperatures : str, lattice_constants : str, default_lattice_constant : float, name : str, idx, natoms):
        
        self.name = name
        self.idx = idx
        self.natoms = natoms

        try:
            v = float(default_lattice_constant)**3
        except:
            v = 1
            for a in default_lattice_constant:
                v *= a
        
        v = v**(1/3)

        self.lattice_constants = [float(a) for a in lattice_constants.split()]
        self.volumes = np.array(self.lattice_constants)*v
        self.volumes = 0.25*self.volumes**3/natoms

        self.temperatures = np.array([float(T) for T in temperatures.split()])
        
        self.num_t = len(self.temperatures)
        self.num_a = len(self.lattice_constants)

        self.energies = np.array([float(e) for e in free_energies.split()]).reshape(self.num_a, self.num_t, order="F")*Ry2eV
        self.impurity_energies = np.array([float(e) for e in impurity_energies.split()]).reshape(self.num_a, self.num_t, order="F")*Ry2eV

        self.a_interpolation_kind = "cubic" if self.num_a > 3 else "quadratic" if self.num_t==3 else "linear"
        self.t_interpolation_kind = "cubic" if self.num_t > 3 else "quadratic" if self.num_t==3 else "linear"

    def prepare_fit(self,temp_idx):
        vfit = np.linspace(self.volumes[0], self.volumes[-1], 101, endpoint=True)
        efit =  interp.interp1d(self.volumes, self.energies[:,temp_idx], kind=self.a_interpolation_kind)
        efit = efit(vfit)
        #plt.plot(efit)
        self.E0 = np.min(efit)
        i = np.argwhere(efit==self.E0)[0][0]
        self.V0 = vfit[i]

    def get_U0(self, temp_idx, V0):
        efit =  interp.interp1d(self.volumes, self.impurity_energies[:,temp_idx], kind=self.a_interpolation_kind)
        return efit(V0)

    def Birch_Murnaghan_EOS(self, V, B, dB):
        vr = (self.V0/V)**(2/3)
        return self.E0 + 9./16.*self.V0*B*((vr-1)**3*dB + (vr-1)**2 * (6-4*vr))

    def Analyze(self):
        self.impurity_energies0_p=np.zeros(self.num_t)
        self.impurity_energies0_v=np.zeros(self.num_t)
        self.min_energies=np.zeros(self.num_t)
        self.min_volumes=np.zeros(self.num_t)
        self.bulk_moduli=np.zeros(self.num_t)
        self.bulk_moduli_prime=np.zeros(self.num_t)
        for i in range(self.num_t):
            self.prepare_fit(i)
            popt, pcov = optimize.curve_fit(self.Birch_Murnaghan_EOS, self.volumes, self.energies[:,i])
            self.min_energies[i] = self.E0
            self.min_volumes[i] = self.V0
            self.bulk_moduli[i] = popt[0]
            self.bulk_moduli_prime[i] = popt[1]

        for i in range(self.num_t):
            self.impurity_energies0_p[i] = self.get_U0(i, self.min_volumes[i])
            self.impurity_energies0_v[i] = self.get_U0(i, self.min_volumes[self.idx])

        #plt.show()

        self.min_energies_nonrel = np.copy(self.min_energies)
        self.min_energies -= np.min(self.min_energies)
        
        self.cp=np.zeros(self.num_t)
        self.cv=np.zeros(self.num_t)
        self.beta=np.zeros(self.num_t)

        tm = self.temperatures - 0.1
        tp = self.temperatures + 0.1
        epfit =  interp.interp1d(self.temperatures, self.impurity_energies0_p, kind=self.t_interpolation_kind, fill_value = "extrapolate")
        evfit =  interp.interp1d(self.temperatures, self.impurity_energies0_v, kind=self.t_interpolation_kind, fill_value = "extrapolate")
        vfit =  interp.interp1d(self.temperatures, self.min_volumes, kind='linear', fill_value = "extrapolate")
        for i in range(self.num_t):
            self.cp[i] = (epfit(tp[i]) - epfit(tm[i]))/0.2
            self.cv[i] = (evfit(tp[i]) - evfit(tm[i]))/0.2
            self.beta[i] = (vfit(tp[i]) - vfit(self.temperatures[i]))/0.1
        
        #tcheck = np.linspace(self.temperatures[0], self.temperatures[-1],101, endpoint=True)
        #tcheckm = tcheck  - 0.1
        #tcheckp = tcheck + 0.1
        #plt.plot(tcheck, epfit(tcheckp) - epfit(tcheckm) )
        #plt.show()
        
    def plot(self, ax):
        ax[0].plot(self.temperatures, self.min_energies, marker="o")
        ax[0].set_ylabel("Energy (eV)")
        ax[0].set_xticks([])

        ax[1].plot(self.temperatures, self.cp, marker="o")
        ax[1].plot(self.temperatures, self.cv, marker="o")
        ax[1].set_ylabel("c_p (eV/atom-K)")
        ax[1].set_xticks([])
        m = max(np.max(self.cp), np.max(self.cv), 0.01)
        ax[1].set_ylim([0,m])

        ax[2].plot(self.temperatures, self.min_volumes)
        ax[2].set_ylabel("Volume (A^3)")
        ax[2].set_xticks([])
        ax[2].set_ylim([12,20])

        ax[3].plot(self.temperatures, self.beta, marker="o")
        ax[3].set_ylabel(r"$\beta (A^3 / K)$")
        ax[3].set_xticks([])
        ax[3].set_ylim([-0.02,0.02])

        ax[4].plot(self.temperatures, self.bulk_moduli, marker="o")
        ax[4].set_ylabel("B (eV/A^3)")
        ax[4].set_xticks([])

        ax[5].plot(self.temperatures, self.bulk_moduli_prime, marker="o")
        ax[5].set_ylabel("B'")
        ax[5].set_xlabel("T (K)")
    
        ax[0].set_title(self.name)


if __name__ == '__main__':
    delta = EnergyData(
        """-187249.7546	-187250.5399	-187250.7021	-187250.4798	-187249.1606
-187250.0417	-187250.8506	-187251.0965	-187251.1172	-187249.3614
-187250.159	-187251.2257	-187251.3354	-187250.9624	-187249.512
-187250.4919	-187251.6401	-187251.6505	-187251.4524	-187249.806""",
"""-187249.7546	-187250.5399	-187250.7021	-187250.4798	-187249.1606
-187250.0417	-187250.8506	-187251.0965	-187251.1172	-187249.3614
-187250.159	-187251.2257	-187251.3354	-187250.9624	-187249.512
-187250.4919	-187251.6401	-187251.6505	-187251.4524	-187249.806""",
        "500 600 700 800",
        "1.05 1.08 1.1 1.12 1.15",
        4.6323,
        "delta",
        1,
        2
    )

    delta_prime = EnergyData(
        """-186069.4612	-186070.3111	-186070.9078	-186069.893
-186070.3398	-186070.9258	-186070.9718	-186070.16
-186070.3143	-186070.4561	-186071.4741	-186070.0298
-186070.7161	-186071.1555	-186071.7802	-186070.671""",
"""-185.2543804	-182.98736	-186.4017872	-187.3596437
-187.0803361	-185.0436854	-185.8654478	-186.0808096
-187.290007	-184.2879199	-185.9995464	-186.3344189
-186.9126189	-184.2001345	-186.0110222	-186.9735188""",
        "500 600 700 800",
        "1.0 1.05 1.1 1.15",
        [3.339, 3.339, 4.446],
        "delta prime",
        3,
        1
    )

    epsilon = EnergyData(
        """
-187246.0101	-187245.7653	-187244.2143
-187245.8678	-187245.9873	-187244.6095""",
"""
-187246.0101	-187245.7653	-187244.2143
-187245.8678	-187245.9873	-187244.6095""",
        "700 800",
        "1.0 1.05 1.1",
        3.64,
        "epsilon",
        1,
        1
    )


    gamma = EnergyData(
        """
-186048.96	-186050.2699	-186048.7585
-186050.3147	-186051.0192	-186049.3372
-186050.5288	-186050.9284	-186049.666
-186049.6461	-186051.4793	-186049.8065""",
"""
-184.0415257	-186.2184869	-184.4940249
-184.8560823	-186.3991318	-184.2443959
-184.9746882	-186.5264233	-184.356185
-184.4810444	-186.3623828	-183.9918006""",
        "500 600 700 800",
        "1.0 1.05 1.1",
        [3.1587, 5.7682, 10.162],
        "gamma",
        1,
        4
    )

    
    mats = [gamma, delta, delta_prime, epsilon]
    for m in mats:
        fig, ax = plt.subplots(6,1)
        m.Analyze()
        m.plot(ax)
        plt.savefig(f"/Users/Corey/Dropbox/DMFT/manuscripts/Computer Time/INCITE_2020/results/{m.name}.eps")
    plt.show()
    #plt.clear()

    fig, ax = plt.subplots(1,1)
    for m in mats:
        ax.plot(m.temperatures, m.min_energies_nonrel, label = m.name)
    ax.legend()
    #plt.show()