#!/usr/bin/python3

'''
Created on Jun 2, 2020

@author: adler
'''

import pickle
import sys
from matplotlib.collections import PathCollection
import numpy as np
from matplotlib import cm
from matplotlib.patches import Rectangle
from portobello.rhobusta.depickler import IntensityFigure
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.rhobusta.options import ReadOptions
from pathlib import Path
from matplotlib.figure import Figure
from matplotlib.patches import Patch
from argparse import Namespace
import matplotlib.pyplot as plt
from matplotlib import lines as mpl_lines
from distinctipy import distinctipy

from portobello.rhobusta.spectral_plotting import Evaluator, plot_spectral_f
from portobello.rhobusta.depickler import IntensityFigure

class LineData:
    def __init__(self, data, color, style, ax):
        self.xdata = data[0]
        self.ydata = data[1]
        self.color = color
        self.style = style

        self.getName(ax)


    def getName(self, ax):
        try:
            handles, labels = ax.get_legend_handles_labels()
            legend = ax.get_legend()
            lines = legend.get_lines()
            for i, line in enumerate(lines):
                if line.get_color() == self.color and line.get_linestyle() == self.style:
                    self.name = handles[i]
        except:
            name = ""




class PlotCombiner:

    class FileClassifier:
        line_plots = ["DOS", "BS", "acont", "iBS", "EVC"]
        imshow_plots = ["ARPES", "pARPES"]

        def __init__(self, filename):
            self.name = filename.split("/")[-1].split(".pickle")[0]
            mytype = self.name.split("-")[0]
            self.line_plot = mytype in self.line_plots
            self.imshow_plot = mytype in self.imshow_plots

            if not self.line_plot and not self.imshow_plot:
                mytype = self.name.split(".")[0]
                self.line_plot = mytype in self.line_plots
                self.imshow_plot = mytype in self.imshow_plots

            assert self.line_plot or self.imshow_plot, f"plot type of {self.name} not recognized -- cant combine"
            assert self.line_plot != self.imshow_plot, f"plot {self.name} cannot be both line and imshow"

    def getFigSpec(self, index = 0):
        return self.fig_handles[index].axes[0].get_gridspec()
    
    def getNumAxes(self, index = 0):
        return len(self.fig_handles[index].axes)

    def __init__(self, opts):
        self.file_names = opts.input_file
        self.num_files = len(self.file_names)
        self.fig_handles = [pickle.load(open(f,'rb')) for f in self.file_names]
        self.classified_files = [self.FileClassifier(fn) for fn in self.file_names]

        self.opts = opts

        self.imshows = np.array([cf.imshow_plot for cf in self.classified_files], dtype=int)

    def combine(self):
        if np.sum(self.imshows) > 1.5:
            self.combine_spectral_plots()
        else: 
            self.combine_line_plots()#can handle one spectral plot

    def handle(self):
        return self.fig, self.ax

    def combine_spectral_plots(self):

        assert np.sum(self.imshows) == self.num_files, "cannot combine more than two spectral plots with a line plot currently"

        if self.opts.eqn != "":
            evaluator = Evaluator(self.opts.eqn)

            fig, ax = self.combine_spectral_per_equation(evaluator.get())

        else:
            figures = []
            for f,fig in zip(self.file_names,self.fig_handles):
                print("De-pickling")
                figures.append(IntensityFigure.fromExistingFig(fig, f))
                plt.close(fig)
                print("Done")

            fig, ax = self.combine_spectral_equally(figures)
        
        
        self.fig = fig
        self.ax = ax

         
    def combine_spectral(self):

        if self.opts.eqn != "":
            evaluator = Evaluator(self.opts.eqn)

            fig, ax = self.combine_spectral_per_equation(evaluator.get(), opts)

        else:
            figures = []
            for f in self.opts.fnames:
                print(f"Loading file {f}")
                fig : Figure = pickle.load(open(f"{f}",'rb'))
                print("De-pickling")
                figures.append(IntensityFigure.fromExistingFig(fig, f))
                plt.close(fig)
                print("Done")

            fig, ax = self.combine_spectral_equally(figures)
        plt.show()

    def combine_line_plots(self):
        
        num_axes = self.getNumAxes()
        spec = self.getFigSpec()

        styles = list(mpl_lines.lineStyles.keys())
        colors = distinctipy.get_colors(self.num_files)

        assert np.sum(self.imshows) < 1.5, "Cant combine line plot with more than one spectral plot currently"

        is_imshow = np.sum(self.imshows) > 1e-8
        imshow_and_line = (is_imshow) and (True in [cf.line_plot for cf in self.classified_files])

        ax_fig_data = []
        for i in range(num_axes):
            data=[]
            for j, f in enumerate(self.fig_handles):
                if self.classified_files[j].line_plot and i < len(f.axes):
                    if self.opts.use_color or is_imshow:
                        data.append([LineData(l.get_data(),colors[j],l.get_linestyle(), f.axes[i])  for l in f.axes[i].get_lines()])
                    else:
                        data.append([LineData(l.get_data(),l.get_color(),styles[j%len(styles)], f.axes[i])  for l in f.axes[i].get_lines()])
                else:
                    data.append(None)
            ax_fig_data.append(data)
            
        axes_axes = []
        for ax in self.fig_handles[0].axes:
            axes_axes.append([ax.get_xlim(), ax.get_ylim()])
        

        if is_imshow:
            i = np.argwhere(self.imshows == 1)[0,0]
            depickled_imshow_fig = IntensityFigure.fromExistingFig(self.fig_handles[i], self.classified_files[i].name)
            Z = [ d.Z for d in depickled_imshow_fig.data ]
            for_plotter, omegas, labels, opts = self.prepare_spectral_for_plot(depickled_imshow_fig, Z, False)
            opts.cmap = "viridis"
            fig, axes = plot_spectral_f(labels, omegas, for_plotter, opts)
        else:
            fig = plt.figure()
            axes = [fig.add_subplot(spec[sec]) for sec in range(num_axes)]

        for i, ax in enumerate(axes):

            for j, fig_data in enumerate(ax_fig_data[i]):
                if self.classified_files[j].line_plot:

                    for k, line in enumerate(fig_data):
                        if line.xdata[0] == line.xdata[-1]:
                            ax.axvline(line.xdata[0], color='gray', linestyle = "--")
                        else:
                            color = 'red' if imshow_and_line else line.color
                            if k == 0:
                                ax.plot(line.xdata, line.ydata, color=color, linestyle = line.style, label = self.classified_files[j].name)
                            else:
                                ax.plot(line.xdata, line.ydata, color=color, linestyle = line.style)
                                

            ref_axis = self.fig_handles[0].axes[i]

            ax.set_xlim(ref_axis.get_xlim())
            ax.set_ylim(ref_axis.get_ylim())

            ax.set_xticks(ref_axis.get_xticks())
            ax.set_yticks(ref_axis.get_yticks())
            
            ax.set_xlabel(ref_axis.get_xlabel())
            ax.set_ylabel(ref_axis.get_ylabel())

            xticklabels = ref_axis.get_xticklabels()
            test = np.array([t.get_text() for t in xticklabels])
            if is_imshow and not (test == '').all():
                ax.set_xticklabels(xticklabels)

        handles_a, labels_a = self.fig_handles[0].axes[0].get_legend_handles_labels()
        handles_b, labels_b = axes[0].get_legend_handles_labels()
        axes[0].legend(handles_a+handles_b,labels_a+labels_b)

        self.fig = fig
        self.ax = ax

        for fig in self.fig_handles:
            plt.close(fig)
        
    @classmethod
    def prepare_spectral_for_plot(cls, ff, Z, plane):

        opts = Namespace()

        if plane:
            for_plotter = Z

            labels = [ff.hskp[0].labels[0], ff.xlabels[0], ff.ylabels[0]]

            omegas = None

            opts.width = ff.data[0].aX[-1]
            opts.plane = True

            
        else:
            nk = [d.X.shape[0] for d in ff.data]
            nomega = ff.data[0].aY.shape[0]

            myshape = (np.sum(nk),nomega,4) if Z[0].shape[-1] == 4 else (np.sum(nk),nomega)
            for_plotter = np.zeros(myshape, dtype=float)

            index=0
            for i, c in enumerate(Z):
                for_plotter[ index:index + nk[i],...] = c[...]
                index += nk[i]
            
            labels = []
            for ax in range(ff.naxes):
                hskp = ff.hskp[ax]

                i=0
                for x in ff.data[ax].X:
                    found = False
                    if x+0.5 >= hskp.positions[i]:
                        labels.append(hskp.labels[i])
                        found = True
                        i+=1
                        
                    if not found:
                        labels.append('')

            omegas = ff.data[0].aY

            opts.plane = False

        if not hasattr(opts,'interpolation'):
            opts.interpolation = 'bilinear'

        return for_plotter, omegas, labels, opts

    def combine_spectral_equally(self, figures): #figures = IntensityFigure

        cmaps = ['Reds','Greens','Blues','Greys','Purples']
        nmaps = len(cmaps)

        ff = figures[0]

        zmax = [0 for ax in ff.data]
        for fig in figures:
            for ax in range(fig.naxes):
                zmax[ax] = max(zmax[ax], fig.data[ax].maximum)
                plane = fig.hskp[ax].plane

        combined_rgb = [[] for i in range(ff.naxes)]
        for i, fig in enumerate(figures):
            cmap = plt.get_cmap(cmaps[i%nmaps])
            for ax in range(fig.naxes):
                rgba = 1 - cmap(fig.data[ax].Z/zmax[ax]) 
                if len(combined_rgb[ax]) == 0:
                    combined_rgb[ax] = rgba
                else: 
                    combined_rgb[ax] += rgba
        
        maxv = 0
        minv = len(figures)
        for rgb in combined_rgb:
            maxv = max(maxv,np.max(1-rgb[:,:,:3]))
            minv = min(minv,np.min(1-rgb[:,:,:3]))
        scale = 1./(maxv-minv)
        
        for ax in range(fig.naxes):
            combined_rgb[ax] = 1 - combined_rgb[ax]*scale
            combined_rgb[ax] = np.clip(combined_rgb[ax], 0, 1)

        if plane:
            combined_rgb = combined_rgb[0]

        for_plotter, omegas, labels, opts = self.prepare_spectral_for_plot(ff, combined_rgb, plane)
        
        opts.cmap = None

        def transpose(a):
            return np.swapaxes(a,0,1) #can't transpose the rgba data
        
        from portobello.rhobusta.bandstructure_utilities import PlaneWithSymmetry
        hskpp = PlaneWithSymmetry.HSKP_Placer.FromIntensityFigure(figures[0], opts)
        fig, ax = plot_spectral_f(labels, omegas, for_plotter, opts, reshape = False, op = transpose, add_colorbar=False, hskpp = hskpp)
        legend_elements = [ Patch( facecolor=cmaps[i%nmaps][:-1], label = figures[i].name ) for i in range(len(figures))]  
        
        try:
            first_ax = ax[0]
        except:
            first_ax = ax

        first_ax.legend(handles=legend_elements, loc = 'best')
        
        return fig, ax

    def combine_spectral_per_equation(self, figure):
        Z = figure.data[0].Z
        if self.opts.dotk:
            for ix, x in enumerate(figure.data[0].aX):
                Z[ix,:] = Z[ix,:] * x  

        for_plotter, omegas, labels, opts = self.prepare_spectral_for_plot(figure, Z, figure.hskp[0].plane)

        from portobello.rhobusta.bandstructure_utilities import PlaneWithSymmetry
        hskpp = PlaneWithSymmetry.HSKP_Placer.FromIntensityFigure(figure, opts)
        return plot_spectral_f(labels, omegas, for_plotter, opts, normalize_cmap = opts.norm_cmap,  reshape = False, add_colorbar=False, hskpp = hskpp)

if __name__ == '__main__':
    parser = ArgumentParserThatStoresArgv('show',add_help=True)    

    parser.add_argument("-t", "--title", dest="title",
                      default="",
                      help="title")

    parser.add_argument("-x", "--xlim", dest="xlim",
                      default=[None,None],
                      nargs=2,
                      type=float,
                      help="2 numbers to use as xlim")

    parser.add_argument("-y", "--ylim", dest="ylim",
                      default=[None,None],
                      nargs=2,
                      type=float,
                      help="2 numbers to use as ylim")

    parser.add_argument("-S", "--save", dest="save",
                      default=False,
                      action="store_true",
                      help="save plot as .eps")

    parser.add_argument("-D", "--dump", dest="dump",
                      default=False,
                      action="store_true",
                      help="save plot as .pickle")

    parser.add_argument("-C", "--use-color", dest="use_color",
                      default=False,
                      action="store_true",
                      help="When combining plots, use color to differentiate between files instead of linestyles")

    parser.add_argument(dest="input_file", nargs='*',
                    help="required input file",
                    type=str)
    
    pg = parser.add_argument_group(title="combining ARPES plots", description = "Some extra control when combining ARPES plots")
    pg.add_argument("-E", 
                          dest='eqn',
                          default="",
                          help="""Equation defining the X-direction of the arrows.\n
                                  Observables should exist in the working directory as pARPES.[obs].pickle\n
                                  Spaces are required between observables and operators.""")

    pg.add_argument("--cmap", 
                          dest='cmap',
                          default="bwr",
                          help="""Color map used to display equation""")


    pg.add_argument("--normalize-cmap", 
                          dest='norm_cmap',
                          action="store_true",
                          default=False,
                          help="""apply a normalization to the color map s.t. zero = middle of color map""")

    pg.add_argument("--dot-k",
                          dest='dotk',
                          action="store_true",
                          default=False,
                          help="""plot dot(A(k,omega),k)""")
    

    
    popts = parser.parse_args(sys.argv[1:])
    
    if len(popts.input_file) > 1:
        print(f"combining and showing files {popts.input_file}")
        pc = PlotCombiner(popts)
        pc.combine()
        fig_handle, ax = pc.handle()
    else:
        print(f"showing file {sys.argv[1]}")
        fig_handle : Figure = pickle.load(open(popts.input_file[0],'rb'))
    try:
        opts = ReadOptions()
    except:
        opts = Namespace()
        
    if popts.title == "":
        title = str(Path(popts.input_file[0]).absolute()) + "\n"
        for v in sys.argv[1:]:
            if hasattr(opts, v):
                title += f"{v}={getattr(opts,v)}\n"
            else:
                title += f"{v}\n"
    else:
        title = popts.title.format(**opts.__dict__)
    
    fig_handle.suptitle(title)
    
    for ax in fig_handle.axes:
        if popts.xlim[0] is not None:
            ax.set_xlim(popts.xlim)
        if popts.ylim[0] is not None:
            ax.set_ylim(popts.ylim)
    #plt.grid()
    if popts.save:
        file = popts.input_file[0].split(".pickle")[0]
        plt.savefig(file+".eps")
    if popts.dump:
        file = popts.input_file[0].split(".pickle")[0]
        pickle.dump(fig_handle, open(f'{title}.pickle','wb'))

    try: #attempt to get some fun interactivity
        associations = {}
        handles, labels = fig_handle.axes[0].get_legend_handles_labels()
        legend = fig_handle.axes[0].get_legend()

        def recursively_get_type(objects : list, type):
            accumulator = []
            for obj in objects:
                children = obj.get_children()
                for child in children:
                    if isinstance(child, type):
                        accumulator.append(child)
                accumulator += recursively_get_type(children, type)
            return accumulator

        lines = legend.get_lines()
        rectangles = recursively_get_type(legend.get_children(), Rectangle)
        scatter_points = recursively_get_type(legend.get_children(), PathCollection)
        
        def get_color(obj):
            if len(lines):
                color = str(obj._color)
            elif len(scatter_points):
                color = str(obj.get_facecolor()[0])
            elif len(rectangles):
                color = str(obj._edgecolor)
            return color

        choices = lines+rectangles+scatter_points

        for c in choices:
            c.set_picker(True)

        nothing = len(lines) + len(rectangles) + len(scatter_points) == 0

        if not nothing:
            for i, ax in enumerate(fig_handle.axes):
                
                artists = ax.get_children()

                for h in handles:
                    color = get_color(h)

                    if not i:
                        associations[color] = []

                    for a in artists:
                        if type(a) == type(h):
                            if color == get_color(a):
                                associations[color].append(a)
    except:
        pass


    def on_pick(event):
        # On the pick event, find the original line corresponding to the legend
        # proxy line, and toggle its visibility.
        artist = event.artist
        color = get_color(artist)

        for associated in associations[color]:
            visible = not associated.get_visible()
            associated.set_visible(visible)
        
        fig_handle.canvas.draw()

    fig_handle.canvas.mpl_connect('pick_event', on_pick)
    plt.show()
