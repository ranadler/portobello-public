#!/usr/bin/env python3
'''
Created on Oct 12, 2018

@author: adler
'''
from portobello.rhobusta.dmft import DMFTOrchastrator
from portobello.generated.dmft import DMFTState, ImpurityMeasurement, ImpurityProblem
from optparse import OptionParser
import sys
from portobello.bus.persistence import Store
import os
from os.path import exists


class ForkDmft(DMFTOrchastrator):
    '''
    classdocs
    '''


    def __init__(self, opts, arg):
        '''
        Constructor
        '''
        DMFTState.__init__(self)
        super_iter, sub_iter = [int(i) for i in arg.split(".")]
        
        try:
            print("... looking in dmft.h5:/history")
            self.load(f"./dmft.h5:/history/{super_iter}/{sub_iter}/")
        except:
            print("... looking in history.h5")
            self.load(f"./history.h5:/{super_iter}/{sub_iter}/")
                       
        print("original state:", self.state)
        print("Nimp:", self.Nimp)
        print("Nlatt", self.Nlatt)
        print("DC: ")
        self.PrintMatrix(self.DC[:,:,0])
        
        assert(self.state not in ['dft+dmft', 'dft']) # cannot fork from a non-dmft iteration, try prev. super iteration
        
        self.state = opts.state
        self.super_iter = 1
        self.sub_iter = 1
        
        newdir = "./fork%d.%d"%(super_iter, sub_iter)
        if not exists(newdir):
            os.mkdir(newdir)
        
        impurity = ImpurityProblem()
        impurity.load("./dmft.h5:/impurity/")

        # don't overwrite the file if it exist - read it first
        newfile = "%s/dmft.h5"%newdir
        if os.path.exists(newfile):
            dmft2 = DMFTState()
            dmft2.load("%s/dmft.h5:/"%newdir)
            
        self.store("%s/history.h5:/1/1/"%newdir)
        
        self.super_iter = 2
        self.sub_iter = 0
        self.store("%s/dmft.h5:/"%newdir)
        impurity.store("%s/dmft.h5:/impurity/"%newdir)
        
        im = ImpurityMeasurement()
        im.load("./dmft.h5:/impurity/measurement/")
        im.store("%s/dmft.h5:/impurity/measurement/"%newdir)
        

if __name__ == '__main__':
    
    parser = OptionParser()
       
    parser.add_option("-s", "--set-state", dest="state", default="dft+sigma",
                      help="reset the state to this one")
    
    (opts, args) = parser.parse_args(sys.argv[1:])
    
    
    fd = ForkDmft(opts, args[0])
    
    Store.FlushAll()
    