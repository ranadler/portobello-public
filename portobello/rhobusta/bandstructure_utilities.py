#These are separated from bandstructure.py so they can be imported without importing much else
import numpy as np
from pymatgen.symmetry.bandstructure import HighSymmKpath
from portobello.generated.LAPW import KPath
import pymatgen.symmetry as pysymm
from portobello.rhobusta.DFTPlugin import DFTPlugin
from itertools import product


def getHighSymmKpath(dft,opts):
    st = dft.GetStructure()
    # TODO: add magnetic moments and get the magnetically modified Kpath (with less symmetry)
    #st.add_site_property("magmom",[[0,0,1]])
    return st, HighSymmKpath(st, symprec=0.001, angle_tolerance=opts.spacegroup_angle_tolerance)#, has_magmoms=True)

def CreateKPath(hskp, opts, cartesian=True, strip_repeated = True):
    points, labels = hskp.get_kpoints(line_density = opts.kdensity, coords_are_cartesian=cartesian)
    if opts.path != "":
        points,labels = selectSpecifiedPath(opts.path, points, labels, opts.kdensity)
    if strip_repeated:
        stripRepeatedHSPoints(points, labels)

    kpath = KPath()
    kpath.num_k = len(points)
    kpath.allocate()

    fac = 2*np.pi if cartesian else 1
    for ik in range(kpath.num_k):
        kpath.k[:,ik] = points[ik] / fac

    return kpath, labels

def stripRepeatedHSPoints(points,labels):
    i = 0
    repeated_entries=[]
    for label in labels:
        if i and not label == '':
            if label == labels[i-1]:
                repeated_entries.append(i)
        i += 1
    
    for i in reversed(repeated_entries):
        points.pop(i)
        labels.pop(i)

def selectSpecifiedPath(path, all_points, all_labels, kdensity):
    labels = path.split(",")
    nlabels = len(labels)
    assert nlabels>1, "Must request at least a pair of points in path"

    points = []
    for label in labels:
        found = False
        label = label.replace("\\", "")

        for j, hskp_label in enumerate(all_labels):
            hskp_label = hskp_label.replace("\\", "")

            if hskp_label != '' and hskp_label in label:
                if ('_' in label and '_' in hskp_label) or '_' not in label:
                    l = str(list(all_points[j])) #eval doesn't like numpy arrays as str(arr)
                    s = f"np.array({l})"
                    eq = label.replace(hskp_label,s)

                    found = True
                    break
                

        assert found, f"label {label} is not in high symmetry path, which includes {set(all_labels)}"

        kx = np.array([1,0,0])
        ky = np.array([0,1,0])
        kz = np.array([0,0,1])
        
        points.append( eval(eq) )

    dk = 1./kdensity

    new_points = None
    new_labels = []
    for i in range(nlabels-1):
        n = int(round(np.linalg.norm(points[i+1] - points[i]) / dk))
        p = np.linspace(points[i], points[i+1], n, endpoint = False)
        if new_points is None:
            new_points = p
        else:
            new_points = np.concatenate((new_points, p))
        
        new_labels = new_labels + [labels[i]] + ['']*(n-1)

    new_points = np.concatenate((new_points, [points[-1]]))
    new_labels += [labels[-1]]

    return list(new_points), list(new_labels)

class PlaneWithSymmetry:

    def __init__(self, dft : DFTPlugin, center : np.array, plane_vectors : list, kdensity : int, pos_and_neg = True):

        self.plane_vectors = plane_vectors
        self.center = center

        self.structure = dft.GetStructure()
        symm = pysymm.analyzer.SpacegroupAnalyzer(self.structure, )
        self.symmOps = symm.get_point_group_operations(cartesian=True)

        self.x = plane_vectors[0][:]
        self.y = plane_vectors[1][:]
        self.width = [np.linalg.norm(self.x), np.linalg.norm(self.y)]
        
        assert np.abs(np.dot(self.x,self.y)) < 1e-8, f"Plane vectors must be perpendicular: vectors given are {self.x}, {self.y}"

        def gen_segment(r):
            if pos_and_neg:
                return self.LineSegment("","",-r,r)
            else:
                return self.LineSegment("","",0,r)

        self.segment_x = gen_segment(self.x)
        self.segment_y = gen_segment(self.y)

        self.nx = self.x / np.linalg.norm(self.x)
        self.ny = self.y / np.linalg.norm(self.y)
        self.npv = [self.nx, self.ny]

        self.normal = np.cross(self.nx,self.ny)
        self.center_z = np.dot(self.normal, self.center)

        self.hskp_placer = self.HSKP_Placer.FromPWS(self)
        self.plane_points_with_symm = self.generate_kmesh(kdensity)

    def generate_kmesh(self, kdensity):

        self.plane_points_with_symm = []
        
        ks = [np.linspace(-v, v, kdensity, endpoint=True) for v in self.plane_vectors]

        print(" - generating mesh")
        ik = 0
        total = 0
        for i,j in product(range(kdensity), repeat=2):
            point = ks[0][i] + ks[1][j] + self.center

            kstar, along_plane_ = self.generate_k_in_star_on_plane(point)
            
            if len(kstar):
                found = False
                for ip, p in enumerate(self.plane_points_with_symm):
                    check = [np.allclose(point, x, rtol = 1e-8) for x in p['kstar']]
                    if any(check):
                        self.plane_points_with_symm[ip]['indices'].append(ik)
                        found = True
                        total+=1
                        break
                
                if not found:
                    self.plane_points_with_symm.append({'kstar':kstar, 'k':point, 'indices': [ik]})
                    total +=1
            else:
                total +=1
            
            ik+=1

        self.nk_full = kdensity**2
        self.nk = len(self.plane_points_with_symm)
        print(f"   - number of k points in plane {self.nk_full} reduced to {self.nk} symmetry reduced points")
        assert total == self.nk_full, f"Problem generating symmetry reduced mesh: {total} != {self.nk_full}"
        print(f"   - done!")
        return self.plane_points_with_symm

    def get_irr_points(self):
        return [p["k"] for p in self.plane_points_with_symm]

    def unfold(self,folded_result):
        unfolded_result = np.zeros((self.nk_full), dtype=float)
        for ik, k in enumerate(self.plane_points_with_symm):
            for jk in k['indices']:
                unfolded_result[jk] = folded_result[ik]
        return unfolded_result

    def generate_k_in_star_on_plane(self, k, eps=1e-8):
        ks_dot_vectors = []
        ks=[]
        for op in self.symmOps:
            symmk = op.operate(k)
            symmk_x = np.dot(symmk, self.nx)*self.nx
            symmk_y = np.dot(symmk, self.ny)*self.ny
            symmk_z = np.dot(symmk, self.normal)
            check = [np.allclose(symmk, x, rtol = 1e-8) for x in ks] #don't put copies in
            if not any(check):
                if self.segment_y.isWithinSegment(symmk_y, eps) and self.segment_x.isWithinSegment(symmk_x, eps)  and np.abs(symmk_z - self.center_z) < 1e-8:

                    along_plane = [np.dot(symmk, self.npv[i]) for i in range(2)]
                    
                    ks_dot_vectors.append(along_plane)
                    ks.append(symmk)
        
        return ks, ks_dot_vectors

    class LineSegment:
                
            eps = 1E-8
                
            def __init__(self, aLabel, bLabel, a, b):
                self.aLabel = aLabel
                self.bLabel = bLabel
                self.a = a
                self.b = b
                    
                self.dif = b - a
                self.length2 = np.dot(self.dif, self.dif)
                self.length = np.sqrt(self.length2)
                        
            def isCollinear(self, c):
                return np.linalg.norm(np.cross(c - self.a, self.dif)) < self.eps
                        
            def isBetween(self, c, eps = 0):
                dp = np.dot(c - self.a, self.dif)
                return dp > -eps and dp < self.length2+eps
                    
            def isWithinSegment(self, c, eps=0):
                return self.isCollinear(c) and self.isBetween(c, eps)
                            
            def operateOn(self,op):
                self.a = op.operate(self.a)
                self.b = op.operate(self.b)
                    
            def projectOnto(self,vec):
                return np.dot(vec, self.dif) / np.sqrt(self.length) * self.dif

    class HSKP_Placer:

        @classmethod
        def FromIntensityFigure(cls, IntensityFigure, opts):

            hskp = IntensityFigure.hskp[0]
            if hskp.plane:
                points = {}
                data = IntensityFigure.data[0]
                
                xlim = [data.X[0], data.X[-1]]
                ylim = [data.Y[0], data.Y[-1]]

                scaley = np.sum(np.abs(xlim))/opts.width/2
                scalex = np.sum(np.abs(ylim))/opts.width/2

                midpointx = np.average(xlim)
                midpointy = np.average(ylim)

                for pos, label in zip(hskp.positions, hskp.labels):
                    apos = [(pos[0,0] - midpointx)/scalex, (pos[0,1] - midpointy)/scaley]

                    if label in points.keys():
                        points[label].append(apos)
                    else:
                        points[label] = [apos]

                return cls(points, [xlim[-1],ylim[-1]])
            else:
                return None

        @classmethod
        def FromPWS(cls, pws):
            
            def stripNonSymmPoints(points, labels):
                i = 0
                entries=[]
                for label in labels:
                    if label == '':
                        entries.append(i)
                    i += 1
                    
                for i in reversed(entries):
                    points.pop(i)
                    labels.pop(i)
                
                return points, labels

            hskp = pysymm.bandstructure.HighSymmKpath(pws.structure)
            hskp_path, hskp_labels = hskp.get_kpoints(line_density=1, coords_are_cartesian=True)
            hskp_path, hskp_labels = stripNonSymmPoints(hskp_path,hskp_labels)

            uhskp_path, indx = np.unique(np.array(hskp_path).round(decimals=10),axis=0,return_index=True)
            uhskp_labels = [hskp_labels[i] for i in indx]

            points = cls.findHSKPwithin(pws, uhskp_path, uhskp_labels) 
            return cls(points, pws.width)

        def __init__(self, points : dict, width : list):
            self.points = points # dictionary which maps label (a hskp) to list of points in plane
            self.width = width
            
        def placePoints(self, ax, opts):
            xlim = ax.get_xlim()
            ylim = ax.get_ylim()

            dx = np.sum(np.abs(xlim))*0.02
            dy = np.sum(np.abs(ylim))*0.02

            scalex = (xlim[1] - xlim[0]) / self.width[0] / 2
            scaley = (ylim[1] - ylim[0]) / self.width[1] / 2

            offsetx = np.average(xlim)
            offsety = np.average(ylim)

            for label in self.points.keys():
                for point in self.points[label]:
                    x = point[0]*scalex+offsetx
                    sx = np.sign(x)
                    y = point[1]*scaley+offsety
                    sy = np.sign(y)
                    ax.scatter(x, y, color = "gray")
                    ax.text(x + dx, y + dy, f"${label}$", color = "gray")
        
        @classmethod
        def findHSKPwithin(cls, pws, uhskp_path, uhskp_labels):
            
            points = {}
            for ik, k in enumerate(uhskp_path):
                key = uhskp_labels[ik]
                kstar_, along_plane = pws.generate_k_in_star_on_plane(k,eps=1e-8) # eps to find points on, not just within, the plane boundaries
                if len(along_plane):
                    
                    if key in points.keys():
                        points[key] += along_plane
                    else:
                        points[key] = along_plane
            
            return points

    