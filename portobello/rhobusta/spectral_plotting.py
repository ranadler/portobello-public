'''
Created on Sep 10, 2020

@author: adler
'''
from argparse import Namespace
import numpy as np
import pickle
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit

from portobello.rhobusta.depickler import IntensityFigure

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import colors



class Evaluator:

    def __init__(self, equation):
        x = equation.split()
        
        input_files = [x[i] for i in range(0,len(x),2)]
        if len(x)>1:
            ops = [x[i] for i in range(1,len(x),2)]
        else:
            ops = []

        self.figures = []

        for f in input_files:
            print(f"Opening file pARPES.{f}.pickle")
            fig = pickle.load(open(f"pARPES.{f}.pickle",'rb'))
               
            self.figures.append(IntensityFigure.fromExistingFig(fig, f))
            plt.close(fig)
            plt.close(plt.gcf())
                
        self.res = self.figures[0]
        for i, op in enumerate(ops):
            self.res = eval(f"self.res{op}self.figures[i+1]")

    def get(self):
        return self.res

#Sometimes we do not draw a line between points, instead we jump from one to another
#Here, we find those points and report their index and number
def findHSPointJumps(labels):
    
    jumps = [0]
    for i in range(1,len(labels)):
        if not labels[i] == '' and not labels[i-1] == '': #back to back hsp
            if not labels[i] == labels[i-1]: #which are dissimilar
                jumps.append(i)
    jumps.append(len(labels))
    
    return jumps, len(jumps) - 1


def get_fig(nk, njumps, jumps):
    
    #allocate to each subplot (continuous section of kpoints) a ratio of the total figure width
    widths = []
    for sec in range(njumps):
        nk_sec = jumps[sec+1] - jumps[sec]
        widths.append(nk_sec / nk)
    
    fig = plt.figure(constrained_layout=True)
    spec = gridspec.GridSpec(ncols = njumps, nrows = 1, width_ratios = widths,
                            top=0.95, bottom=0.05, left=0.5/(njumps+1), right=1-0.5/(njumps+1),
                            wspace = 0.05, hspace = 0)
    ax = [fig.add_subplot(spec[sec]) for sec in range(njumps)]

    return fig, ax

def set_ticks_and_labels(ax,jumps,labels,sec):
    #create new x-ticks / lines for the high symmetry points
    i=jumps[sec]
    hsp_l = []
    hsp_k = []
    for l in labels[jumps[sec]:jumps[sec+1]]:
        if not l == '':
            #vertical line denoting high symm points
            ax[sec].axvline(x = i+0.5, color = 'gray', linestyle = '--', linewidth = 0.5)
            
            #get matplotlib to recongize greek characters
            if '\\' in l and l[0]!='$':
                l = r'$'+ l +'$'
                    
            hsp_l.append(l)
            hsp_k.append(i+0.5)
                
        i+=1
    ax[sec].set_xticks(hsp_k)
    ax[sec].set_xticklabels(hsp_l)
    ax[sec].set_xlabel('k')
        
    #set y-axis
    if sec:
        ax[sec].set_yticks([])
        ax[sec].set_yticklabels([])
    else:
        ax[sec].set_ylabel(r'$\omega$ (eV)')
        
    ax[sec].axhline(color = 'gray', linestyle = '--', linewidth = 0.5)



#Plot spectral functions
#Produces separate subplots when there is a jump between k points (based on labels)
#for any function func[omega,k]
def plot_spectral_f(labels : np.array, #list of labels for each k point if not opts.plane | shape = nk
                                                  #center point and axis labels if opts.plane | shape = 3
                    omegas : np.array, #list of frequencies | shape = num_omega
                    spectral_f  : np.array, #A(k,omega) | shape = nk,num_omega
                    opts : Namespace, #run options -- assumes AnalysisLoader options available
                    normalize_cmap : bool = False, #when A has positive and negative values
                                                   #and we want to dedicate similar colorbar ranges to positive and negative 
                                                   #(and see A=0 with an appropariate colormap)
                    reshape : bool = True, #when plotting a plane (k is two dimensional -- sqrt(nk) x sqrt(nk)) and A is not shaped
                    op = np.transpose, #instead of plotting A(k,omega), plot op(A(k,w)) -- transposing for matplotlib.pyplot.imshow
                    hskpp = None, #bandstructure.ArpesRunnerOnPlane.PlaneWithSymmetry.HSKP_Placer | if provided and opts.plane, places hskp on plot
                    add_colorbar = True, #Add default colorbar
                    truncate_negatives=True): #Set spectral_f<0 = 0

    if truncate_negatives:
        spectral_f[spectral_f<0] = 0 

    im = None
    
    if normalize_cmap:
        norm=colors.TwoSlopeNorm(vmin=np.nanmin(spectral_f), vcenter=0., vmax=np.nanmax(spectral_f))
    else:
        norm=colors.Normalize(vmin=np.nanmin(spectral_f), vmax=np.nanmax(spectral_f))

    if opts.plane:

        nk = int(np.sqrt(spectral_f.shape[0]))
        if reshape:
            spectral_f = spectral_f.reshape(nk,nk)

        fig = plt.figure(constrained_layout=True)
        ax = fig.add_subplot()
        
        im = ax.imshow( op(spectral_f) ,
                        origin='lower', aspect='auto', 
                        cmap = opts.cmap, interpolation = opts.interpolation, 
                        norm = norm
                    )
        
        width = [opts.width, opts.width]
        if hskpp is not None:
            hskpp.placePoints(ax, opts)
            width = hskpp.width

        else:
            xlim = ax.get_xlim()
            ylim = ax.get_ylim()
            x = np.average(xlim)
            y = np.average(ylim)
            dx = np.sum(np.abs(xlim))*0.02
            dy = np.sum(np.abs(ylim))*0.02

            ax.scatter(x, y, color = "gray")
            if labels[0][0] != '$':
                label = f"${labels[0]}$"
            else:
                label = f"{labels[0]}"
            
            ax.text(x + dx, y + dy, label, color = "gray")

        ax.set_xlabel(labels[1])
        ax.set_ylabel(labels[2])

        nticks = len(ax.get_xticks())
        newlabels = np.round(np.linspace(-width[0], width[0], nticks, endpoint = True), decimals=2)
        ax.set_xticklabels(newlabels)

        nticks = len(ax.get_yticks())
        newlabels = np.round(np.linspace(-width[1], width[1], nticks, endpoint = True), decimals=2)
        ax.set_yticklabels(newlabels)

    else:
        nk = len(labels)

        jumps, njumps = findHSPointJumps(labels)
        fig,ax = get_fig(nk,njumps,jumps)
        
        for sec in range(njumps):
            
            #main plot
            im = ax[sec].imshow(op( spectral_f[jumps[sec]:jumps[sec+1], :] ),
                            origin='lower', extent=[jumps[sec], jumps[sec+1], omegas[0], omegas[-1]],
                            aspect = 'equal', cmap = opts.cmap, interpolation = opts.interpolation,
                            norm = norm
                        )
            
            #make the figure have a reasonable aspect ratio that is independent of energy range and nk
            ax[sec].set_aspect(nk / (omegas[-1] - omegas[0]))
            
            set_ticks_and_labels(ax,jumps,labels,sec)

    if add_colorbar:
        cb_ax = fig.add_axes([.91,.124,.04,.754])
        fig.colorbar(im, cax=cb_ax)

    return fig, ax

class Character:
    def __init__(self, label, value, color):
        self.weight = None
        self.label = label
        self.value = value
        self.color = color

    def add(self,val):
        if self.weight is None:
            self.weight = val
        self.weight += val



#for any function func[omega,k]
def plot_bands(labels, correlated, uncorrelated, opts, characters = None, no_spin = True):

    get_masses = False
    if hasattr(opts, "effective_mass"):
        get_masses = opts.effective_mass
    
    if get_masses:
        scale = abs(float(labels[0].split("*")[0]))

    nk = len(labels)
    
    jumps, njumps = findHSPointJumps(labels)
    fig, ax = get_fig(nk,njumps,jumps)

    if characters is not None:

        m = 0 
        num_char = 0
        for key in characters.keys():
            to_delete = []
            character_list = list(characters[key].values())
            for c, character in enumerate(character_list):

                max_weight = np.max(character.weight)
                m = max(m,max_weight)

                if max_weight < 1e-13:
                    to_delete.append(c)

            if no_spin:
                for i in to_delete[::-1]:
                    print(f" - Observable with label {character_list[i].label} removed from projected plot because it has no weight. ")
                    print( "  Possible causes include an attempt to project onto a spinfull variable in a spinless run.")
                    character_list.pop(i)
            
            characters[key] = character_list
            num_char += len(character_list)

        max_size_pnt = 20
        normalization_marker = m / (max_size_pnt) 

    linestyle=["-",":"]
    directions = ["x","y","z"]
    num_si = correlated.shape[2]
    nband = correlated.shape[1]
    for si in range(num_si):
        for sec in range(njumps):
            
            x = np.arange(jumps[sec], jumps[sec+1])

            if get_masses:
                real_x = np.linspace(-scale, scale, len(x), endpoint = True)
                mid_x =  int((jumps[sec+1] - jumps[sec]) / 2)
                mid_data =  jumps[sec] + mid_x

                last_c = -10000
                for ib in range(nband):

                    c = correlated[mid_data, ib, si]
                    def parabola(k, a):
                        return a*k**2

                    fit_params, pcov = curve_fit(parabola, real_x, correlated[jumps[sec]:jumps[sec+1], ib, si] - c) 

                    mass = fit_params[0]

                    if abs(c - last_c) < 0.1:
                        plot_c = last_plot_c+0.1*opts.omega_window
                    else:
                        plot_c = c

                    ax[sec].annotate(f"{np.round(mass,1)}", 
                        xy=(x[mid_x],c), xycoords='data',
                        xytext = (x[0],plot_c) , textcoords='data',
                        arrowprops=dict(arrowstyle="->",
                        connectionstyle="arc3"), color = 'red')

                    last_c = c
                    last_plot_c = plot_c

            if characters is not None:
                fine_x = np.linspace(x[0],x[-1], max(len(x), min(20*len(x),500)), endpoint=True)
                
                for key in characters.keys():
                    for c, character in enumerate(characters[key]):                   
                        for ib in range(nband):
                    
                            bands = correlated[jumps[sec]:jumps[sec+1], ib, si]
                            widths = character.weight[jumps[sec]:jumps[sec+1], ib, si]
                            marker_widths = (widths / normalization_marker)**2
                            
                            fine_bands = interp1d(x, bands)(fine_x)
                            fine_widths = interp1d(x, marker_widths)(fine_x)

                            if ib == 0:
                                ax[sec].scatter(fine_x, fine_bands, s=fine_widths, color=character.color, label = character.label)
                            else:
                                ax[sec].scatter(fine_x, fine_bands, s=fine_widths, color=character.color)
                    

            ylim = (-opts.omega_window, opts.omega_window)

            ax[sec].plot(x,correlated[jumps[sec]:jumps[sec+1], :, si], color='black', linestyle=linestyle[si])
            ax[sec].plot(x,uncorrelated[jumps[sec]:jumps[sec+1], :, si], color='b', linestyle=linestyle[si])
        
            ax[sec].set_ylim(ylim)
            ax[sec].set_xlim([x[0],x[-1]])
            #make the figure have a reasonable aspect ratio that is independent of energy range and nk
            #ax[sec].set_aspect(nk / (ylim[-1] - ylim[0]))
            
            #create new x-ticks / lines for the high symmetry points
            set_ticks_and_labels(ax,jumps,labels,sec)      
    
    lgnd = ax[0].legend()
    for handle in lgnd.legendHandles:
        handle.set_sizes([20.0])

    return fig, ax


            


if __name__ == '__main__':
    pass
