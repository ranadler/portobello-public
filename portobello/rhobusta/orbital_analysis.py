#!/usr/bin/env python3



from argparse import ArgumentDefaultsHelpFormatter, Namespace
from portobello.generated.base_types import Window
from portobello.rhobusta.ProjectorEmbedder import CorrelatedSubspaceProjector, ProjectorOptionsParser
from portobello.rhobusta.options import ArgumentParserThatStoresArgv
from portobello.generated.FlapwMBPT import Input
from portobello.rhobusta.orbitals import OrbitalsDefinition

import sys
import numpy as np

def OrbitalAnalysisParser(add_help = True, private = True):

    parser = ArgumentParserThatStoresArgv('orbital analysis', add_help=add_help,
                            parents=[ProjectorOptionsParser(private=private)],
                            formatter_class=ArgumentDefaultsHelpFormatter)      

    parser.add_argument("-C", dest="coverage",
                      default=0.15,
                      type=float,
                      help="Require that coverage of one component of the shell exceeds this value.") 

    parser.add_argument("--orb-search", dest="orbital_search",
                      default=False,
                      action="store_true",
                      help="Search for shells which exceed the requested coverage.") 

    parser.add_argument("--win-search", dest="window_search",
                      default=False,
                      action="store_true",
                      help="Expand the window until the specified shell exceeds the requested coverage. (not implemented)") 


    parser.add_argument("--win-search-mode", dest="window_search_mode",
                      default="equal", choices=["equal", "up", "down"],
                      help="Expand the window until the specified shell exceeds the requested coverage. Equal: expand window up and down.") 

    return parser


def searchForHighCoverageOrbitals(opts : Namespace, projClass = CorrelatedSubspaceProjector, extraProjKwargs={}):
    """
    Searches MT basis for orbitals of 
    """

    window = [float(f) for f in opts.window.split(":")]
    window = Window(low=window[0], high = window[1])

    l2s={0:'s',1:'p',2:'d',3:'f'}

    ini = Input()
    ini.load("./ini.h5:/")
    shells_to_search = []
    for isort in range(ini.strct.nsort):
        ad = ini.strct.ad[isort]
        for i in range(ad.maxntle):
            for l in range(min(ad.lmb+1,4)):
                if (l<=1 and ad.augm[i,l] == 1) or (l>1 and ad.augm[i,l]==0):
                    n = int(ad.ptnl[i,l])
                    txtel = ad.txtel
                    if txtel[0] == "_": txtel=txtel[1]
                    shell = f"{txtel}:{n}{l2s[l]}" 
                    if shell not in shells_to_search:
                        shells_to_search.append(shell)


    shells_to_keep = []
    for shell in shells_to_search:
        opts.orbitals = shell
        odef = OrbitalsDefinition(opts)
        if odef.total_dim > 0:

            proj = projClass(odef, window, **extraProjKwargs)
            if np.all(proj.coverage < opts.coverage): # too little weight in desired window
                print( f"---- {shell}: too little coverage in desired window | coverage = {proj.coverage}")
                continue

            shells_to_keep.append(shell)
    
    orbitals = ",".join(shells_to_keep)

    print(f"-----------------------------------")
    print(f"searched shells {shells_to_search}")
    print(f"kept shells {shells_to_keep}")
    print(f"-----------------------------------")

    return orbitals

def MainOrbitalAnalysis(args, kwargs, opts=None):

    parser = OrbitalAnalysisParser()

    if opts is None:
        opts, argv_ = parser.parse_known_args(args)
        opts.which_shell = 0

    else:
        default_opts = parser.parse_args([])
        defaults = {key: parser.get_default(key) for key in vars(default_opts)}
        for key in defaults.keys():
            if not hasattr(opts, key):
                setattr(opts, key, defaults[key])

    for key in kwargs:
        setattr(opts, key, kwargs[key])

    if not opts.orbital_search and not opts.window_search:
        print(" -- no search requested!")

    if opts.orbital_search:
        return searchForHighCoverageOrbitals(opts)

if __name__ == '__main__':
    MainOrbitalAnalysis(sys.argv[1:],{})