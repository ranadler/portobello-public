
module gutz
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use PEScf
    use array4
    use cube
    use vector


    implicit none
    public

    type, extends(SolverState) :: GState
        integer(kind=int32)  ::  num_k_all =  0

        type(cube_complex)  ::  Hloc
        complex(kind=dp),pointer :: Hloc_(:,:,:)
        type(cube_complex)  ::  R
        complex(kind=dp),pointer :: R_(:,:,:)
        type(cube_complex)  ::  Lambda
        complex(kind=dp),pointer :: Lambda_(:,:,:)
        type(cube_complex)  ::  Z
        complex(kind=dp),pointer :: Z_(:,:,:)
        integer(kind=int32)  ::  success =  0

        real(kind=dp)  ::  egutz =  real(0.000000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateGStateObjectFields
        procedure :: ResetSectionFields   => ResetGStateSectionFields
        procedure :: StoreScalarFields    => StoreGStateScalarFields
        procedure :: StoreObjectFields    => StoreGStateObjectFields
        procedure :: LoadScalarFields     => LoadGStateScalarFields
        procedure :: LoadObjectFields     => LoadGStateObjectFields
        procedure :: DisconnectObjectFields => DisconnectGStateObjectFields
        procedure :: IsEqual              => IsGStateEqual
        procedure :: AssignmentOperator   => AssignmentOperatorGState
        procedure :: clear                => ClearGState
        procedure :: init => InitGState
#ifndef __GFORTRAN__
        final     :: FinalizeGState
#endif
        procedure :: GetGstateHlocExtents
        procedure :: GetGstateRExtents
        procedure :: GetGstateLambdaExtents
        procedure :: GetGstateZExtents

    end type

    type, extends(SolverProblem) :: GProblem
        integer(kind=int32)  ::  num_k =  0

        integer(kind=int32)  ::  num_k_irr =  0

        integer(kind=int32)  ::  num_bands =  0

        integer(kind=int32)  ::  norb2 =  0

        type(string)  ::  gutz_name
        type(vector_real)  ::  weight
        real(kind=dp),pointer :: weight_(:)
        type(array4_complex)  ::  Hk
        complex(kind=dp),pointer :: Hk_(:,:,:,:)
        type(array4_complex)  ::  HkBands
        complex(kind=dp),pointer :: HkBands_(:,:,:,:)
        type(array4_complex)  ::  UTensor
        complex(kind=dp),pointer :: UTensor_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateGProblemObjectFields
        procedure :: ResetSectionFields   => ResetGProblemSectionFields
        procedure :: StoreScalarFields    => StoreGProblemScalarFields
        procedure :: StoreObjectFields    => StoreGProblemObjectFields
        procedure :: LoadScalarFields     => LoadGProblemScalarFields
        procedure :: LoadObjectFields     => LoadGProblemObjectFields
        procedure :: DisconnectObjectFields => DisconnectGProblemObjectFields
        procedure :: IsEqual              => IsGProblemEqual
        procedure :: AssignmentOperator   => AssignmentOperatorGProblem
        procedure :: clear                => ClearGProblem
        procedure :: init => InitGProblem
#ifndef __GFORTRAN__
        final     :: FinalizeGProblem
#endif
        procedure :: GetGproblemWeightExtents
        procedure :: GetGproblemHkExtents
        procedure :: GetGproblemHkbandsExtents
        procedure :: GetGproblemUtensorExtents

    end type

    type, extends(SolverSolution) :: GSolution
        integer(kind=int32)  ::  dim =  0

        integer(kind=int32)  ::  num_si =  0

        type(cube_complex)  ::  R
        complex(kind=dp),pointer :: R_(:,:,:)
        type(cube_complex)  ::  Lambda
        complex(kind=dp),pointer :: Lambda_(:,:,:)
        type(cube_complex)  ::  Z
        complex(kind=dp),pointer :: Z_(:,:,:)
        type(cube_complex)  ::  RhoImp
        complex(kind=dp),pointer :: RhoImp_(:,:,:)
        real(kind=dp)  ::  energyImp =  0.0d0

        integer(kind=int32)  ::  success =  0



        contains
        procedure :: AllocateObjectFields => AllocateGSolutionObjectFields
        procedure :: ResetSectionFields   => ResetGSolutionSectionFields
        procedure :: StoreScalarFields    => StoreGSolutionScalarFields
        procedure :: StoreObjectFields    => StoreGSolutionObjectFields
        procedure :: LoadScalarFields     => LoadGSolutionScalarFields
        procedure :: LoadObjectFields     => LoadGSolutionObjectFields
        procedure :: DisconnectObjectFields => DisconnectGSolutionObjectFields
        procedure :: IsEqual              => IsGSolutionEqual
        procedure :: AssignmentOperator   => AssignmentOperatorGSolution
        procedure :: clear                => ClearGSolution
        procedure :: init => InitGSolution
#ifndef __GFORTRAN__
        final     :: FinalizeGSolution
#endif
        procedure :: GetGsolutionRExtents
        procedure :: GetGsolutionLambdaExtents
        procedure :: GetGsolutionZExtents
        procedure :: GetGsolutionRhoimpExtents

    end type

    contains
        subroutine InitGState(self)
                class(GState), intent(inout) :: self
                call self%SolverState%Init()
                self%num_k_all =  0
                self%success =  0
                self%egutz =  real(0.000000,kind=16)
        end subroutine
        subroutine StoreGStateObjectFields(self)
                class(GState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverState%StoreObjectFields()
                call self%Hloc%StoreObject(ps, gid,  'Hloc')
                call self%R%StoreObject(ps, gid,  'R')
                call self%Lambda%StoreObject(ps, gid,  'Lambda')
                call self%Z%StoreObject(ps, gid,  'Z')
        end subroutine
        subroutine LoadGStateObjectFields(self)
                class(GState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverState%LoadObjectFields()
                call self%Hloc%LoadObject(ps, gid,  'Hloc')
                call self%R%LoadObject(ps, gid,  'R')
                call self%Lambda%LoadObject(ps, gid,  'Lambda')
                call self%Z%LoadObject(ps, gid,  'Z')
        end subroutine
        subroutine ResetGStateSectionFields(self)
                class(GState), intent(inout) :: self
                call self%SolverState%ResetSectionFields()
                self%Hloc_ => self%Hloc%GetWithExtents(self%GetGStateHlocExtents())
                self%R_ => self%R%GetWithExtents(self%GetGStateRExtents())
                self%Lambda_ => self%Lambda%GetWithExtents(self%GetGStateLambdaExtents())
                self%Z_ => self%Z%GetWithExtents(self%GetGStateZExtents())
        end subroutine
        subroutine DisconnectGStateObjectFields(self)
                class(GState), intent(inout) :: self
               type(iterator) :: iter
                call self%SolverState%DisconnectObjectFields()
                call self%Hloc%DisconnectFromStore()
                call self%R%DisconnectFromStore()
                call self%Lambda%DisconnectFromStore()
                call self%Z%DisconnectFromStore()
        end subroutine
        subroutine StoreGStateScalarFields(self)
                class(GState), intent(inout) :: self
                call self%SolverState%StoreScalarFields()
                call self%write('num_k_all', self%num_k_all)
                call self%write('success', self%success)
                call self%write('egutz', self%egutz)
        end subroutine
        subroutine LoadGStateScalarFields(self)
                class(GState), intent(inout) :: self
                call self%SolverState%LoadScalarFields()
                call self%read('num_k_all', self%num_k_all)
                call self%read('success', self%success)
                call self%read('egutz', self%egutz)
        end subroutine
        subroutine FinalizeGState(self)
               type(GState), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearGState(self)
                class(GState), intent(inout) :: self
                type(GState), save :: empty
                self = empty
        end subroutine
        pure elemental function IsGStateEqual(lhs, rhs) result(iseq)
                class(GState), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (GState)
                       iseq = iseq .and. (lhs%SolverState == rhs%SolverState)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_all == rhs%num_k_all)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Hloc == rhs%Hloc)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%R == rhs%R)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Lambda == rhs%Lambda)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Z == rhs%Z)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%success == rhs%success)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%egutz == rhs%egutz)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorGState(lhs, rhs)
                class(GState), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (GState)
                       lhs%SolverState = rhs%SolverState
                       lhs%num_k_all = rhs%num_k_all
                       lhs%Hloc = rhs%Hloc
                       lhs%R = rhs%R
                       lhs%Lambda = rhs%Lambda
                       lhs%Z = rhs%Z
                       lhs%success = rhs%success
                       lhs%egutz = rhs%egutz
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetGstateHlocExtents(self) result(res)
                class(GState), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        function GetGstateRExtents(self) result(res)
                class(GState), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        function GetGstateLambdaExtents(self) result(res)
                class(GState), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        function GetGstateZExtents(self) result(res)
                class(GState), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        subroutine AllocateGStateObjectFields(self)
                class(GState), intent(inout) :: self
                call self%SolverState%AllocateObjectFields()
                call self%Hloc%init(int(self%dim),int(self%dim),int(self%num_si))
                call self%R%init(int(self%dim),int(self%dim),int(self%num_si))
                call self%Lambda%init(int(self%dim),int(self%dim),int(self%num_si))
                call self%Z%init(int(self%dim),int(self%dim),int(self%num_si))
        end subroutine


        subroutine InitGProblem(self)
                class(GProblem), intent(inout) :: self
                call self%SolverProblem%Init()
                self%num_k =  0
                self%num_k_irr =  0
                self%num_bands =  0
                self%norb2 =  0
        end subroutine
        subroutine StoreGProblemObjectFields(self)
                class(GProblem), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverProblem%StoreObjectFields()
                call self%weight%StoreObject(ps, gid,  'weight')
                call self%Hk%StoreObject(ps, gid,  'Hk')
                call self%HkBands%StoreObject(ps, gid,  'HkBands')
                call self%UTensor%StoreObject(ps, gid,  'UTensor')
        end subroutine
        subroutine LoadGProblemObjectFields(self)
                class(GProblem), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverProblem%LoadObjectFields()
                call self%weight%LoadObject(ps, gid,  'weight')
                call self%Hk%LoadObject(ps, gid,  'Hk')
                call self%HkBands%LoadObject(ps, gid,  'HkBands')
                call self%UTensor%LoadObject(ps, gid,  'UTensor')
        end subroutine
        subroutine ResetGProblemSectionFields(self)
                class(GProblem), intent(inout) :: self
                call self%SolverProblem%ResetSectionFields()
                self%weight_ => self%weight%GetWithExtents(self%GetGProblemweightExtents())
                self%Hk_ => self%Hk%GetWithExtents(self%GetGProblemHkExtents())
                self%HkBands_ => self%HkBands%GetWithExtents(self%GetGProblemHkBandsExtents())
                self%UTensor_ => self%UTensor%GetWithExtents(self%GetGProblemUTensorExtents())
        end subroutine
        subroutine DisconnectGProblemObjectFields(self)
                class(GProblem), intent(inout) :: self
               type(iterator) :: iter
                call self%SolverProblem%DisconnectObjectFields()
                call self%weight%DisconnectFromStore()
                call self%Hk%DisconnectFromStore()
                call self%HkBands%DisconnectFromStore()
                call self%UTensor%DisconnectFromStore()
        end subroutine
        subroutine StoreGProblemScalarFields(self)
                class(GProblem), intent(inout) :: self
                call self%SolverProblem%StoreScalarFields()
                call self%write('num_k', self%num_k)
                call self%write('num_k_irr', self%num_k_irr)
                call self%write('num_bands', self%num_bands)
                call self%write('norb2', self%norb2)
                call self%write('gutz_name', self%gutz_name)
        end subroutine
        subroutine LoadGProblemScalarFields(self)
                class(GProblem), intent(inout) :: self
                call self%SolverProblem%LoadScalarFields()
                call self%read('num_k', self%num_k)
                call self%read('num_k_irr', self%num_k_irr)
                call self%read('num_bands', self%num_bands)
                call self%read('norb2', self%norb2)
                call self%read('gutz_name', self%gutz_name)
        end subroutine
        subroutine FinalizeGProblem(self)
               type(GProblem), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearGProblem(self)
                class(GProblem), intent(inout) :: self
                type(GProblem), save :: empty
                self = empty
        end subroutine
        pure elemental function IsGProblemEqual(lhs, rhs) result(iseq)
                class(GProblem), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (GProblem)
                       iseq = iseq .and. (lhs%SolverProblem == rhs%SolverProblem)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_irr == rhs%num_k_irr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_bands == rhs%num_bands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%norb2 == rhs%norb2)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%gutz_name == rhs%gutz_name)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%weight == rhs%weight)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Hk == rhs%Hk)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%HkBands == rhs%HkBands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%UTensor == rhs%UTensor)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorGProblem(lhs, rhs)
                class(GProblem), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (GProblem)
                       lhs%SolverProblem = rhs%SolverProblem
                       lhs%num_k = rhs%num_k
                       lhs%num_k_irr = rhs%num_k_irr
                       lhs%num_bands = rhs%num_bands
                       lhs%norb2 = rhs%norb2
                       lhs%gutz_name = rhs%gutz_name
                       lhs%weight = rhs%weight
                       lhs%Hk = rhs%Hk
                       lhs%HkBands = rhs%HkBands
                       lhs%UTensor = rhs%UTensor
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetGproblemWeightExtents(self) result(res)
                class(GProblem), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_k)]
        end function
        function GetGproblemHkExtents(self) result(res)
                class(GProblem), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%num_k),extent(1,self%num_bands),extent(1,self%num_bands),extent(1,self%num_si)]
        end function
        function GetGproblemHkbandsExtents(self) result(res)
                class(GProblem), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%num_k),extent(1,self%num_bands),extent(1,self%num_bands),extent(1,self%num_si)]
        end function
        function GetGproblemUtensorExtents(self) result(res)
                class(GProblem), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%norb2),extent(1,self%norb2),extent(1,self%norb2),extent(1,self%norb2)]
        end function
        subroutine AllocateGProblemObjectFields(self)
                class(GProblem), intent(inout) :: self
                call self%SolverProblem%AllocateObjectFields()
                call self%weight%init(int(self%num_k))
                call self%Hk%init(int(self%num_k),int(self%num_bands),int(self%num_bands),int(self%num_si))
                call self%HkBands%init(int(self%num_k),int(self%num_bands),int(self%num_bands),int(self%num_si))
                call self%UTensor%init(int(self%norb2),int(self%norb2),int(self%norb2),int(self%norb2))
        end subroutine


        subroutine InitGSolution(self)
                class(GSolution), intent(inout) :: self
                call self%SolverSolution%Init()
                self%dim =  0
                self%num_si =  0
                self%energyImp =  0.0d0
                self%success =  0
        end subroutine
        subroutine StoreGSolutionObjectFields(self)
                class(GSolution), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverSolution%StoreObjectFields()
                call self%R%StoreObject(ps, gid,  'R')
                call self%Lambda%StoreObject(ps, gid,  'Lambda')
                call self%Z%StoreObject(ps, gid,  'Z')
                call self%RhoImp%StoreObject(ps, gid,  'RhoImp')
        end subroutine
        subroutine LoadGSolutionObjectFields(self)
                class(GSolution), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverSolution%LoadObjectFields()
                call self%R%LoadObject(ps, gid,  'R')
                call self%Lambda%LoadObject(ps, gid,  'Lambda')
                call self%Z%LoadObject(ps, gid,  'Z')
                call self%RhoImp%LoadObject(ps, gid,  'RhoImp')
        end subroutine
        subroutine ResetGSolutionSectionFields(self)
                class(GSolution), intent(inout) :: self
                call self%SolverSolution%ResetSectionFields()
                self%R_ => self%R%GetWithExtents(self%GetGSolutionRExtents())
                self%Lambda_ => self%Lambda%GetWithExtents(self%GetGSolutionLambdaExtents())
                self%Z_ => self%Z%GetWithExtents(self%GetGSolutionZExtents())
                self%RhoImp_ => self%RhoImp%GetWithExtents(self%GetGSolutionRhoImpExtents())
        end subroutine
        subroutine DisconnectGSolutionObjectFields(self)
                class(GSolution), intent(inout) :: self
               type(iterator) :: iter
                call self%SolverSolution%DisconnectObjectFields()
                call self%R%DisconnectFromStore()
                call self%Lambda%DisconnectFromStore()
                call self%Z%DisconnectFromStore()
                call self%RhoImp%DisconnectFromStore()
        end subroutine
        subroutine StoreGSolutionScalarFields(self)
                class(GSolution), intent(inout) :: self
                call self%SolverSolution%StoreScalarFields()
                call self%write('dim', self%dim)
                call self%write('num_si', self%num_si)
                call self%write('energyImp', self%energyImp)
                call self%write('success', self%success)
        end subroutine
        subroutine LoadGSolutionScalarFields(self)
                class(GSolution), intent(inout) :: self
                call self%SolverSolution%LoadScalarFields()
                call self%read('dim', self%dim)
                call self%read('num_si', self%num_si)
                call self%read('energyImp', self%energyImp)
                call self%read('success', self%success)
        end subroutine
        subroutine FinalizeGSolution(self)
               type(GSolution), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearGSolution(self)
                class(GSolution), intent(inout) :: self
                type(GSolution), save :: empty
                self = empty
        end subroutine
        pure elemental function IsGSolutionEqual(lhs, rhs) result(iseq)
                class(GSolution), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (GSolution)
                       iseq = iseq .and. (lhs%SolverSolution == rhs%SolverSolution)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dim == rhs%dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%R == rhs%R)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Lambda == rhs%Lambda)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Z == rhs%Z)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%RhoImp == rhs%RhoImp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%energyImp == rhs%energyImp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%success == rhs%success)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorGSolution(lhs, rhs)
                class(GSolution), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (GSolution)
                       lhs%SolverSolution = rhs%SolverSolution
                       lhs%dim = rhs%dim
                       lhs%num_si = rhs%num_si
                       lhs%R = rhs%R
                       lhs%Lambda = rhs%Lambda
                       lhs%Z = rhs%Z
                       lhs%RhoImp = rhs%RhoImp
                       lhs%energyImp = rhs%energyImp
                       lhs%success = rhs%success
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetGsolutionRExtents(self) result(res)
                class(GSolution), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        function GetGsolutionLambdaExtents(self) result(res)
                class(GSolution), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        function GetGsolutionZExtents(self) result(res)
                class(GSolution), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        function GetGsolutionRhoimpExtents(self) result(res)
                class(GSolution), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        subroutine AllocateGSolutionObjectFields(self)
                class(GSolution), intent(inout) :: self
                call self%SolverSolution%AllocateObjectFields()
                call self%R%init(int(self%dim),int(self%dim),int(self%num_si))
                call self%Lambda%init(int(self%dim),int(self%dim),int(self%num_si))
                call self%Z%init(int(self%dim),int(self%dim),int(self%num_si))
                call self%RhoImp%init(int(self%dim),int(self%dim),int(self%num_si))
        end subroutine



end module
