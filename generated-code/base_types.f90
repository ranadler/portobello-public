
module base_types
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter



    implicit none
    public

    type, extends(persistent) :: EmptyObject


        contains
        procedure :: AllocateObjectFields => AllocateEmptyObjectObjectFields
        procedure :: ResetSectionFields   => ResetEmptyObjectSectionFields
        procedure :: StoreScalarFields    => StoreEmptyObjectScalarFields
        procedure :: StoreObjectFields    => StoreEmptyObjectObjectFields
        procedure :: LoadScalarFields     => LoadEmptyObjectScalarFields
        procedure :: LoadObjectFields     => LoadEmptyObjectObjectFields
        procedure :: DisconnectObjectFields => DisconnectEmptyObjectObjectFields
        procedure :: IsEqual              => IsEmptyObjectEqual
        procedure :: AssignmentOperator   => AssignmentOperatorEmptyObject
        procedure :: clear                => ClearEmptyObject
        procedure :: init => InitEmptyObject
#ifndef __GFORTRAN__
        final     :: FinalizeEmptyObject
#endif

    end type

    type, extends(persistent) :: ComputationInput
        type(datetime)  ::  date_computed


        contains
        procedure :: AllocateObjectFields => AllocateComputationInputObjectFields
        procedure :: ResetSectionFields   => ResetComputationInputSectionFields
        procedure :: StoreScalarFields    => StoreComputationInputScalarFields
        procedure :: StoreObjectFields    => StoreComputationInputObjectFields
        procedure :: LoadScalarFields     => LoadComputationInputScalarFields
        procedure :: LoadObjectFields     => LoadComputationInputObjectFields
        procedure :: DisconnectObjectFields => DisconnectComputationInputObjectFields
        procedure :: IsEqual              => IsComputationInputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorComputationInput
        procedure :: clear                => ClearComputationInput
        procedure :: init => InitComputationInput
#ifndef __GFORTRAN__
        final     :: FinalizeComputationInput
#endif

    end type

    type, extends(persistent) :: ComputationOutput
        type(string)  ::  binary_name
        type(string)  ::  binary_release
        type(datetime)  ::  date_computed
        type(string)  ::  researcher
        type(string)  ::  description


        contains
        procedure :: AllocateObjectFields => AllocateComputationOutputObjectFields
        procedure :: ResetSectionFields   => ResetComputationOutputSectionFields
        procedure :: StoreScalarFields    => StoreComputationOutputScalarFields
        procedure :: StoreObjectFields    => StoreComputationOutputObjectFields
        procedure :: LoadScalarFields     => LoadComputationOutputScalarFields
        procedure :: LoadObjectFields     => LoadComputationOutputObjectFields
        procedure :: DisconnectObjectFields => DisconnectComputationOutputObjectFields
        procedure :: IsEqual              => IsComputationOutputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorComputationOutput
        procedure :: clear                => ClearComputationOutput
        procedure :: init => InitComputationOutput
#ifndef __GFORTRAN__
        final     :: FinalizeComputationOutput
#endif

    end type

    type, extends(ComputationOutput) :: LDADescriptor
        type(string)  ::  method
        type(string)  ::  location
        type(string)  ::  seed_name


        contains
        procedure :: AllocateObjectFields => AllocateLDADescriptorObjectFields
        procedure :: ResetSectionFields   => ResetLDADescriptorSectionFields
        procedure :: StoreScalarFields    => StoreLDADescriptorScalarFields
        procedure :: StoreObjectFields    => StoreLDADescriptorObjectFields
        procedure :: LoadScalarFields     => LoadLDADescriptorScalarFields
        procedure :: LoadObjectFields     => LoadLDADescriptorObjectFields
        procedure :: DisconnectObjectFields => DisconnectLDADescriptorObjectFields
        procedure :: IsEqual              => IsLDADescriptorEqual
        procedure :: AssignmentOperator   => AssignmentOperatorLDADescriptor
        procedure :: clear                => ClearLDADescriptor
        procedure :: init => InitLDADescriptor
#ifndef __GFORTRAN__
        final     :: FinalizeLDADescriptor
#endif

    end type

    type, extends(persistent) :: Window
        real(kind=dp)  ::  low =  0.0d0

        real(kind=dp)  ::  high =  0.0d0



        contains
        procedure :: AllocateObjectFields => AllocateWindowObjectFields
        procedure :: ResetSectionFields   => ResetWindowSectionFields
        procedure :: StoreScalarFields    => StoreWindowScalarFields
        procedure :: StoreObjectFields    => StoreWindowObjectFields
        procedure :: LoadScalarFields     => LoadWindowScalarFields
        procedure :: LoadObjectFields     => LoadWindowObjectFields
        procedure :: DisconnectObjectFields => DisconnectWindowObjectFields
        procedure :: IsEqual              => IsWindowEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWindow
        procedure :: clear                => ClearWindow
        procedure :: init => InitWindow
#ifndef __GFORTRAN__
        final     :: FinalizeWindow
#endif

    end type

    contains
        subroutine InitEmptyObject(self)
                class(EmptyObject), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreEmptyObjectObjectFields(self)
                class(EmptyObject), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadEmptyObjectObjectFields(self)
                class(EmptyObject), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetEmptyObjectSectionFields(self)
                class(EmptyObject), intent(inout) :: self
        end subroutine
        subroutine DisconnectEmptyObjectObjectFields(self)
                class(EmptyObject), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreEmptyObjectScalarFields(self)
                class(EmptyObject), intent(inout) :: self
        end subroutine
        subroutine LoadEmptyObjectScalarFields(self)
                class(EmptyObject), intent(inout) :: self
        end subroutine
        subroutine FinalizeEmptyObject(self)
               type(EmptyObject), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearEmptyObject(self)
                class(EmptyObject), intent(inout) :: self
                type(EmptyObject), save :: empty
                self = empty
        end subroutine
        pure elemental function IsEmptyObjectEqual(lhs, rhs) result(iseq)
                class(EmptyObject), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (EmptyObject)
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorEmptyObject(lhs, rhs)
                class(EmptyObject), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (EmptyObject)
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateEmptyObjectObjectFields(self)
                class(EmptyObject), intent(inout) :: self
        end subroutine


        subroutine InitComputationInput(self)
                class(ComputationInput), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreComputationInputObjectFields(self)
                class(ComputationInput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadComputationInputObjectFields(self)
                class(ComputationInput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetComputationInputSectionFields(self)
                class(ComputationInput), intent(inout) :: self
        end subroutine
        subroutine DisconnectComputationInputObjectFields(self)
                class(ComputationInput), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreComputationInputScalarFields(self)
                class(ComputationInput), intent(inout) :: self
                call self%write('date_computed', self%date_computed)
        end subroutine
        subroutine LoadComputationInputScalarFields(self)
                class(ComputationInput), intent(inout) :: self
                call self%read('date_computed', self%date_computed)
        end subroutine
        subroutine FinalizeComputationInput(self)
               type(ComputationInput), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearComputationInput(self)
                class(ComputationInput), intent(inout) :: self
                type(ComputationInput), save :: empty
                self = empty
        end subroutine
        pure elemental function IsComputationInputEqual(lhs, rhs) result(iseq)
                class(ComputationInput), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ComputationInput)
                       iseq = iseq .and. (lhs%date_computed == rhs%date_computed)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorComputationInput(lhs, rhs)
                class(ComputationInput), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ComputationInput)
                       lhs%date_computed = rhs%date_computed
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateComputationInputObjectFields(self)
                class(ComputationInput), intent(inout) :: self
        end subroutine


        subroutine InitComputationOutput(self)
                class(ComputationOutput), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreComputationOutputObjectFields(self)
                class(ComputationOutput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadComputationOutputObjectFields(self)
                class(ComputationOutput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetComputationOutputSectionFields(self)
                class(ComputationOutput), intent(inout) :: self
        end subroutine
        subroutine DisconnectComputationOutputObjectFields(self)
                class(ComputationOutput), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreComputationOutputScalarFields(self)
                class(ComputationOutput), intent(inout) :: self
                call self%write('binary_name', self%binary_name)
                call self%write('binary_release', self%binary_release)
                call self%write('date_computed', self%date_computed)
                call self%write('researcher', self%researcher)
                call self%write('description', self%description)
        end subroutine
        subroutine LoadComputationOutputScalarFields(self)
                class(ComputationOutput), intent(inout) :: self
                call self%read('binary_name', self%binary_name)
                call self%read('binary_release', self%binary_release)
                call self%read('date_computed', self%date_computed)
                call self%read('researcher', self%researcher)
                call self%read('description', self%description)
        end subroutine
        subroutine FinalizeComputationOutput(self)
               type(ComputationOutput), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearComputationOutput(self)
                class(ComputationOutput), intent(inout) :: self
                type(ComputationOutput), save :: empty
                self = empty
        end subroutine
        pure elemental function IsComputationOutputEqual(lhs, rhs) result(iseq)
                class(ComputationOutput), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ComputationOutput)
                       iseq = iseq .and. (lhs%binary_name == rhs%binary_name)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%binary_release == rhs%binary_release)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%date_computed == rhs%date_computed)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%researcher == rhs%researcher)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%description == rhs%description)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorComputationOutput(lhs, rhs)
                class(ComputationOutput), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ComputationOutput)
                       lhs%binary_name = rhs%binary_name
                       lhs%binary_release = rhs%binary_release
                       lhs%date_computed = rhs%date_computed
                       lhs%researcher = rhs%researcher
                       lhs%description = rhs%description
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateComputationOutputObjectFields(self)
                class(ComputationOutput), intent(inout) :: self
        end subroutine


        subroutine InitLDADescriptor(self)
                class(LDADescriptor), intent(inout) :: self
                call self%ComputationOutput%Init()
        end subroutine
        subroutine StoreLDADescriptorObjectFields(self)
                class(LDADescriptor), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ComputationOutput%StoreObjectFields()
        end subroutine
        subroutine LoadLDADescriptorObjectFields(self)
                class(LDADescriptor), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ComputationOutput%LoadObjectFields()
        end subroutine
        subroutine ResetLDADescriptorSectionFields(self)
                class(LDADescriptor), intent(inout) :: self
                call self%ComputationOutput%ResetSectionFields()
        end subroutine
        subroutine DisconnectLDADescriptorObjectFields(self)
                class(LDADescriptor), intent(inout) :: self
               type(iterator) :: iter
                call self%ComputationOutput%DisconnectObjectFields()
        end subroutine
        subroutine StoreLDADescriptorScalarFields(self)
                class(LDADescriptor), intent(inout) :: self
                call self%ComputationOutput%StoreScalarFields()
                call self%write('method', self%method)
                call self%write('location', self%location)
                call self%write('seed_name', self%seed_name)
        end subroutine
        subroutine LoadLDADescriptorScalarFields(self)
                class(LDADescriptor), intent(inout) :: self
                call self%ComputationOutput%LoadScalarFields()
                call self%read('method', self%method)
                call self%read('location', self%location)
                call self%read('seed_name', self%seed_name)
        end subroutine
        subroutine FinalizeLDADescriptor(self)
               type(LDADescriptor), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearLDADescriptor(self)
                class(LDADescriptor), intent(inout) :: self
                type(LDADescriptor), save :: empty
                self = empty
        end subroutine
        pure elemental function IsLDADescriptorEqual(lhs, rhs) result(iseq)
                class(LDADescriptor), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (LDADescriptor)
                       iseq = iseq .and. (lhs%ComputationOutput == rhs%ComputationOutput)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%method == rhs%method)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%location == rhs%location)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%seed_name == rhs%seed_name)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorLDADescriptor(lhs, rhs)
                class(LDADescriptor), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (LDADescriptor)
                       lhs%ComputationOutput = rhs%ComputationOutput
                       lhs%method = rhs%method
                       lhs%location = rhs%location
                       lhs%seed_name = rhs%seed_name
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateLDADescriptorObjectFields(self)
                class(LDADescriptor), intent(inout) :: self
                call self%ComputationOutput%AllocateObjectFields()
        end subroutine


        subroutine InitWindow(self)
                class(Window), intent(inout) :: self
                call self%InitPersistent()
                self%low =  0.0d0
                self%high =  0.0d0
        end subroutine
        subroutine StoreWindowObjectFields(self)
                class(Window), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadWindowObjectFields(self)
                class(Window), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetWindowSectionFields(self)
                class(Window), intent(inout) :: self
        end subroutine
        subroutine DisconnectWindowObjectFields(self)
                class(Window), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreWindowScalarFields(self)
                class(Window), intent(inout) :: self
                call self%write('low', self%low)
                call self%write('high', self%high)
        end subroutine
        subroutine LoadWindowScalarFields(self)
                class(Window), intent(inout) :: self
                call self%read('low', self%low)
                call self%read('high', self%high)
        end subroutine
        subroutine FinalizeWindow(self)
               type(Window), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWindow(self)
                class(Window), intent(inout) :: self
                type(Window), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWindowEqual(lhs, rhs) result(iseq)
                class(Window), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Window)
                       iseq = iseq .and. (lhs%low == rhs%low)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%high == rhs%high)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWindow(lhs, rhs)
                class(Window), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Window)
                       lhs%low = rhs%low
                       lhs%high = rhs%high
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateWindowObjectFields(self)
                class(Window), intent(inout) :: self
        end subroutine



end module
