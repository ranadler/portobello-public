
module CTQMC_GPU
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter



    implicit none
    public

    type, extends(persistent) :: Parameters
        integer(kind=int32)  ::  sim_per_device =  int(25,kind=int32)



        contains
        procedure :: AllocateObjectFields => AllocateParametersObjectFields
        procedure :: ResetSectionFields   => ResetParametersSectionFields
        procedure :: StoreScalarFields    => StoreParametersScalarFields
        procedure :: StoreObjectFields    => StoreParametersObjectFields
        procedure :: LoadScalarFields     => LoadParametersScalarFields
        procedure :: LoadObjectFields     => LoadParametersObjectFields
        procedure :: DisconnectObjectFields => DisconnectParametersObjectFields
        procedure :: IsEqual              => IsParametersEqual
        procedure :: AssignmentOperator   => AssignmentOperatorParameters
        procedure :: clear                => ClearParameters
        procedure :: init => InitParameters
#ifndef __GFORTRAN__
        final     :: FinalizeParameters
#endif

    end type

    contains
        subroutine InitParameters(self)
                class(Parameters), intent(inout) :: self
                call self%InitPersistent()
                self%sim_per_device =  int(25,kind=int32)
        end subroutine
        subroutine StoreParametersObjectFields(self)
                class(Parameters), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadParametersObjectFields(self)
                class(Parameters), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetParametersSectionFields(self)
                class(Parameters), intent(inout) :: self
        end subroutine
        subroutine DisconnectParametersObjectFields(self)
                class(Parameters), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreParametersScalarFields(self)
                class(Parameters), intent(inout) :: self
                call self%write('sim_per_device', self%sim_per_device)
        end subroutine
        subroutine LoadParametersScalarFields(self)
                class(Parameters), intent(inout) :: self
                call self%read('sim_per_device', self%sim_per_device)
        end subroutine
        subroutine FinalizeParameters(self)
               type(Parameters), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearParameters(self)
                class(Parameters), intent(inout) :: self
                type(Parameters), save :: empty
                self = empty
        end subroutine
        pure elemental function IsParametersEqual(lhs, rhs) result(iseq)
                class(Parameters), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Parameters)
                       iseq = iseq .and. (lhs%sim_per_device == rhs%sim_per_device)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorParameters(lhs, rhs)
                class(Parameters), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Parameters)
                       lhs%sim_per_device = rhs%sim_per_device
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateParametersObjectFields(self)
                class(Parameters), intent(inout) :: self
        end subroutine



end module
