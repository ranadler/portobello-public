
module symmetry
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: Bootstrap
        integer(kind=int32)  ::  num_ops =  0

        integer(kind=int32)  ::  max_l =  0

        integer(kind=int32)  ::  maxIndexDl =  0

        integer(kind=int32)  ::  maxIndexDj =  0

        type(matrix_real)  ::  Dl
        real(kind=dp),pointer :: Dl_(:,:)
        type(matrix_complex)  ::  Dj
        complex(kind=dp),pointer :: Dj_(:,:)
        type(cube_real)  ::  rots
        real(kind=dp),pointer :: rots_(:,:,:)
        type(matrix_real)  ::  shifts
        real(kind=dp),pointer :: shifts_(:,:)
        type(matrix_real)  ::  recip
        real(kind=dp),pointer :: recip_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateBootstrapObjectFields
        procedure :: ResetSectionFields   => ResetBootstrapSectionFields
        procedure :: StoreScalarFields    => StoreBootstrapScalarFields
        procedure :: StoreObjectFields    => StoreBootstrapObjectFields
        procedure :: LoadScalarFields     => LoadBootstrapScalarFields
        procedure :: LoadObjectFields     => LoadBootstrapObjectFields
        procedure :: DisconnectObjectFields => DisconnectBootstrapObjectFields
        procedure :: IsEqual              => IsBootstrapEqual
        procedure :: AssignmentOperator   => AssignmentOperatorBootstrap
        procedure :: clear                => ClearBootstrap
        procedure :: init => InitBootstrap
#ifndef __GFORTRAN__
        final     :: FinalizeBootstrap
#endif
        procedure :: GetBootstrapDlExtents
        procedure :: GetBootstrapDjExtents
        procedure :: GetBootstrapRotsExtents
        procedure :: GetBootstrapShiftsExtents
        procedure :: GetBootstrapRecipExtents

    end type

    type, extends(persistent) :: GL3Group
        integer(kind=int32)  ::  num_ops =  0

        type(cube_real)  ::  rots
        real(kind=dp),pointer :: rots_(:,:,:)
        type(cube_real)  ::  WigD0
        real(kind=dp),pointer :: WigD0_(:,:,:)
        type(cube_real)  ::  WigD1
        real(kind=dp),pointer :: WigD1_(:,:,:)
        type(cube_real)  ::  WigD2
        real(kind=dp),pointer :: WigD2_(:,:,:)
        type(cube_real)  ::  WigD3
        real(kind=dp),pointer :: WigD3_(:,:,:)
        logical  ::  hasWigJ = .False.

        type(cube_complex)  ::  WigD0J
        complex(kind=dp),pointer :: WigD0J_(:,:,:)
        type(cube_complex)  ::  WigD1J
        complex(kind=dp),pointer :: WigD1J_(:,:,:)
        type(cube_complex)  ::  WigD2J
        complex(kind=dp),pointer :: WigD2J_(:,:,:)
        type(cube_complex)  ::  WigD3J
        complex(kind=dp),pointer :: WigD3J_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateGL3GroupObjectFields
        procedure :: ResetSectionFields   => ResetGL3GroupSectionFields
        procedure :: StoreScalarFields    => StoreGL3GroupScalarFields
        procedure :: StoreObjectFields    => StoreGL3GroupObjectFields
        procedure :: LoadScalarFields     => LoadGL3GroupScalarFields
        procedure :: LoadObjectFields     => LoadGL3GroupObjectFields
        procedure :: DisconnectObjectFields => DisconnectGL3GroupObjectFields
        procedure :: IsEqual              => IsGL3GroupEqual
        procedure :: AssignmentOperator   => AssignmentOperatorGL3Group
        procedure :: clear                => ClearGL3Group
        procedure :: init => InitGL3Group
#ifndef __GFORTRAN__
        final     :: FinalizeGL3Group
#endif
        procedure :: GetGl3groupRotsExtents
        procedure :: GetGl3groupWigd0Extents
        procedure :: GetGl3groupWigd1Extents
        procedure :: GetGl3groupWigd2Extents
        procedure :: GetGl3groupWigd3Extents
        procedure :: GetGl3groupWigd0jExtents
        procedure :: GetGl3groupWigd1jExtents
        procedure :: GetGl3groupWigd2jExtents
        procedure :: GetGl3groupWigd3jExtents

    end type
    interface
         function GetWignerMatricesI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    type, extends(GL3Group) :: SpaceGroup
        type(matrix_real)  ::  shifts
        real(kind=dp),pointer :: shifts_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateSpaceGroupObjectFields
        procedure :: ResetSectionFields   => ResetSpaceGroupSectionFields
        procedure :: StoreScalarFields    => StoreSpaceGroupScalarFields
        procedure :: StoreObjectFields    => StoreSpaceGroupObjectFields
        procedure :: LoadScalarFields     => LoadSpaceGroupScalarFields
        procedure :: LoadObjectFields     => LoadSpaceGroupObjectFields
        procedure :: DisconnectObjectFields => DisconnectSpaceGroupObjectFields
        procedure :: IsEqual              => IsSpaceGroupEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSpaceGroup
        procedure :: clear                => ClearSpaceGroup
        procedure :: init => InitSpaceGroup
#ifndef __GFORTRAN__
        final     :: FinalizeSpaceGroup
#endif
        procedure :: GetSpacegroupShiftsExtents

    end type

    type, extends(GL3Group) :: GroupRepresentation
        integer(kind=int32)  ::  dim =  0

        type(cube_complex)  ::  rep
        complex(kind=dp),pointer :: rep_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateGroupRepresentationObjectFields
        procedure :: ResetSectionFields   => ResetGroupRepresentationSectionFields
        procedure :: StoreScalarFields    => StoreGroupRepresentationScalarFields
        procedure :: StoreObjectFields    => StoreGroupRepresentationObjectFields
        procedure :: LoadScalarFields     => LoadGroupRepresentationScalarFields
        procedure :: LoadObjectFields     => LoadGroupRepresentationObjectFields
        procedure :: DisconnectObjectFields => DisconnectGroupRepresentationObjectFields
        procedure :: IsEqual              => IsGroupRepresentationEqual
        procedure :: AssignmentOperator   => AssignmentOperatorGroupRepresentation
        procedure :: clear                => ClearGroupRepresentation
        procedure :: init => InitGroupRepresentation
#ifndef __GFORTRAN__
        final     :: FinalizeGroupRepresentation
#endif
        procedure :: GetGrouprepresentationRepExtents

    end type

    type, extends(persistent) :: OrbitalLabels
        type(string)  ::  short_label
        type(string)  ::  latex_label


        contains
        procedure :: AllocateObjectFields => AllocateOrbitalLabelsObjectFields
        procedure :: ResetSectionFields   => ResetOrbitalLabelsSectionFields
        procedure :: StoreScalarFields    => StoreOrbitalLabelsScalarFields
        procedure :: StoreObjectFields    => StoreOrbitalLabelsObjectFields
        procedure :: LoadScalarFields     => LoadOrbitalLabelsScalarFields
        procedure :: LoadObjectFields     => LoadOrbitalLabelsObjectFields
        procedure :: DisconnectObjectFields => DisconnectOrbitalLabelsObjectFields
        procedure :: IsEqual              => IsOrbitalLabelsEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOrbitalLabels
        procedure :: clear                => ClearOrbitalLabels
        procedure :: init => InitOrbitalLabels
#ifndef __GFORTRAN__
        final     :: FinalizeOrbitalLabels
#endif

    end type

    type, extends(persistent) :: AnnotatedRepresentation
        integer(kind=int32)  ::  num_ops =  0

        integer(kind=int32)  ::  l =  0

        logical  ::  nrel = .False.

        logical  ::  num_si = .False.

        integer(kind=int32)  ::  dim =  0

        integer(kind=int32)  ::  num_blocks =  0

        real(kind=dp)  ::  blocking_tol =  0.0d0

        real(kind=dp)  ::  G_tolerance =  0.0d0

        type(cube_complex)  ::  rep
        complex(kind=dp),pointer :: rep_(:,:,:)
        type(vector_int)  ::  blocks
        integer(kind=int32),pointer :: blocks_(:)
        type(matrix_complex)  ::  characters
        complex(kind=dp),pointer :: characters_(:,:)
        type(matrix_complex)  ::  chiHchi
        complex(kind=dp),pointer :: chiHchi_(:,:)
        type(matrix_int)  ::  H
        integer(kind=int32),pointer :: H_(:,:)
        type(matrix_real)  ::  rot
        real(kind=dp),pointer :: rot_(:,:)
        type(matrix_complex)  ::  rotD
        complex(kind=dp),pointer :: rotD_(:,:)
        type(matrix_complex)  ::  basisChangeD
        complex(kind=dp),pointer :: basisChangeD_(:,:)
        type(OrbitalLabels), allocatable  ::  labels(:)


        contains
        procedure :: AllocateObjectFields => AllocateAnnotatedRepresentationObjectFields
        procedure :: ResetSectionFields   => ResetAnnotatedRepresentationSectionFields
        procedure :: StoreScalarFields    => StoreAnnotatedRepresentationScalarFields
        procedure :: StoreObjectFields    => StoreAnnotatedRepresentationObjectFields
        procedure :: LoadScalarFields     => LoadAnnotatedRepresentationScalarFields
        procedure :: LoadObjectFields     => LoadAnnotatedRepresentationObjectFields
        procedure :: DisconnectObjectFields => DisconnectAnnotatedRepresentationObjectFields
        procedure :: IsEqual              => IsAnnotatedRepresentationEqual
        procedure :: AssignmentOperator   => AssignmentOperatorAnnotatedRepresentation
        procedure :: clear                => ClearAnnotatedRepresentation
        procedure :: init => InitAnnotatedRepresentation
#ifndef __GFORTRAN__
        final     :: FinalizeAnnotatedRepresentation
#endif
        procedure :: GetAnnotatedrepresentationRepExtents
        procedure :: GetAnnotatedrepresentationBlocksExtents
        procedure :: GetAnnotatedrepresentationCharactersExtents
        procedure :: GetAnnotatedrepresentationChihchiExtents
        procedure :: GetAnnotatedrepresentationHExtents
        procedure :: GetAnnotatedrepresentationRotExtents
        procedure :: GetAnnotatedrepresentationRotdExtents
        procedure :: GetAnnotatedrepresentationBasischangedExtents

    end type

    type, extends(persistent) :: PeriodicSite
        integer(kind=int32)  ::  site_index =  0

        type(vector_real)  ::  xyz
        real(kind=dp),pointer :: xyz_(:)


        contains
        procedure :: AllocateObjectFields => AllocatePeriodicSiteObjectFields
        procedure :: ResetSectionFields   => ResetPeriodicSiteSectionFields
        procedure :: StoreScalarFields    => StorePeriodicSiteScalarFields
        procedure :: StoreObjectFields    => StorePeriodicSiteObjectFields
        procedure :: LoadScalarFields     => LoadPeriodicSiteScalarFields
        procedure :: LoadObjectFields     => LoadPeriodicSiteObjectFields
        procedure :: DisconnectObjectFields => DisconnectPeriodicSiteObjectFields
        procedure :: IsEqual              => IsPeriodicSiteEqual
        procedure :: AssignmentOperator   => AssignmentOperatorPeriodicSite
        procedure :: clear                => ClearPeriodicSite
        procedure :: init => InitPeriodicSite
#ifndef __GFORTRAN__
        final     :: FinalizePeriodicSite
#endif
        procedure :: GetPeriodicsiteXyzExtents

    end type

    type, extends(persistent) :: LocalSymmetry
        integer(kind=int32)  ::  site_index =  0

        type(GL3Group)  ::  local_group
        type(AnnotatedRepresentation), allocatable  ::  annotated_reps_l(:)


        contains
        procedure :: AllocateObjectFields => AllocateLocalSymmetryObjectFields
        procedure :: ResetSectionFields   => ResetLocalSymmetrySectionFields
        procedure :: StoreScalarFields    => StoreLocalSymmetryScalarFields
        procedure :: StoreObjectFields    => StoreLocalSymmetryObjectFields
        procedure :: LoadScalarFields     => LoadLocalSymmetryScalarFields
        procedure :: LoadObjectFields     => LoadLocalSymmetryObjectFields
        procedure :: DisconnectObjectFields => DisconnectLocalSymmetryObjectFields
        procedure :: IsEqual              => IsLocalSymmetryEqual
        procedure :: AssignmentOperator   => AssignmentOperatorLocalSymmetry
        procedure :: clear                => ClearLocalSymmetry
        procedure :: init => InitLocalSymmetry
#ifndef __GFORTRAN__
        final     :: FinalizeLocalSymmetry
#endif

    end type

    type, extends(persistent) :: StructLocalSymmetry
        integer(kind=int32)  ::  num_distinct =  0

        integer(kind=int32)  ::  num_atoms =  0

        integer(kind=int32)  ::  num_si =  0

        integer(kind=int32)  ::  nrel =  0

        type(LocalSymmetry), allocatable  ::  distinct_symm(:)


        contains
        procedure :: AllocateObjectFields => AllocateStructLocalSymmetryObjectFields
        procedure :: ResetSectionFields   => ResetStructLocalSymmetrySectionFields
        procedure :: StoreScalarFields    => StoreStructLocalSymmetryScalarFields
        procedure :: StoreObjectFields    => StoreStructLocalSymmetryObjectFields
        procedure :: LoadScalarFields     => LoadStructLocalSymmetryScalarFields
        procedure :: LoadObjectFields     => LoadStructLocalSymmetryObjectFields
        procedure :: DisconnectObjectFields => DisconnectStructLocalSymmetryObjectFields
        procedure :: IsEqual              => IsStructLocalSymmetryEqual
        procedure :: AssignmentOperator   => AssignmentOperatorStructLocalSymmetry
        procedure :: clear                => ClearStructLocalSymmetry
        procedure :: init => InitStructLocalSymmetry
#ifndef __GFORTRAN__
        final     :: FinalizeStructLocalSymmetry
#endif

    end type

    contains
        subroutine InitBootstrap(self)
                class(Bootstrap), intent(inout) :: self
                call self%InitPersistent()
                self%num_ops =  0
                self%max_l =  0
                self%maxIndexDl =  0
                self%maxIndexDj =  0
        end subroutine
        subroutine StoreBootstrapObjectFields(self)
                class(Bootstrap), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Dl%StoreObject(ps, gid,  'Dl')
                call self%Dj%StoreObject(ps, gid,  'Dj')
                call self%rots%StoreObject(ps, gid,  'rots')
                call self%shifts%StoreObject(ps, gid,  'shifts')
                call self%recip%StoreObject(ps, gid,  'recip')
        end subroutine
        subroutine LoadBootstrapObjectFields(self)
                class(Bootstrap), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Dl%LoadObject(ps, gid,  'Dl')
                call self%Dj%LoadObject(ps, gid,  'Dj')
                call self%rots%LoadObject(ps, gid,  'rots')
                call self%shifts%LoadObject(ps, gid,  'shifts')
                call self%recip%LoadObject(ps, gid,  'recip')
        end subroutine
        subroutine ResetBootstrapSectionFields(self)
                class(Bootstrap), intent(inout) :: self
                self%Dl_ => self%Dl%GetWithExtents(self%GetBootstrapDlExtents())
                self%Dj_ => self%Dj%GetWithExtents(self%GetBootstrapDjExtents())
                self%rots_ => self%rots%GetWithExtents(self%GetBootstraprotsExtents())
                self%shifts_ => self%shifts%GetWithExtents(self%GetBootstrapshiftsExtents())
                self%recip_ => self%recip%GetWithExtents(self%GetBootstraprecipExtents())
        end subroutine
        subroutine DisconnectBootstrapObjectFields(self)
                class(Bootstrap), intent(inout) :: self
               type(iterator) :: iter
                call self%Dl%DisconnectFromStore()
                call self%Dj%DisconnectFromStore()
                call self%rots%DisconnectFromStore()
                call self%shifts%DisconnectFromStore()
                call self%recip%DisconnectFromStore()
        end subroutine
        subroutine StoreBootstrapScalarFields(self)
                class(Bootstrap), intent(inout) :: self
                call self%write('num_ops', self%num_ops)
                call self%write('max_l', self%max_l)
                call self%write('maxIndexDl', self%maxIndexDl)
                call self%write('maxIndexDj', self%maxIndexDj)
        end subroutine
        subroutine LoadBootstrapScalarFields(self)
                class(Bootstrap), intent(inout) :: self
                call self%read('num_ops', self%num_ops)
                call self%read('max_l', self%max_l)
                call self%read('maxIndexDl', self%maxIndexDl)
                call self%read('maxIndexDj', self%maxIndexDj)
        end subroutine
        subroutine FinalizeBootstrap(self)
               type(Bootstrap), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearBootstrap(self)
                class(Bootstrap), intent(inout) :: self
                type(Bootstrap), save :: empty
                self = empty
        end subroutine
        pure elemental function IsBootstrapEqual(lhs, rhs) result(iseq)
                class(Bootstrap), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Bootstrap)
                       iseq = iseq .and. (lhs%num_ops == rhs%num_ops)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_l == rhs%max_l)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxIndexDl == rhs%maxIndexDl)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxIndexDj == rhs%maxIndexDj)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Dl == rhs%Dl)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Dj == rhs%Dj)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rots == rhs%rots)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%shifts == rhs%shifts)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%recip == rhs%recip)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorBootstrap(lhs, rhs)
                class(Bootstrap), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Bootstrap)
                       lhs%num_ops = rhs%num_ops
                       lhs%max_l = rhs%max_l
                       lhs%maxIndexDl = rhs%maxIndexDl
                       lhs%maxIndexDj = rhs%maxIndexDj
                       lhs%Dl = rhs%Dl
                       lhs%Dj = rhs%Dj
                       lhs%rots = rhs%rots
                       lhs%shifts = rhs%shifts
                       lhs%recip = rhs%recip
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetBootstrapDlExtents(self) result(res)
                class(Bootstrap), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%maxIndexDl),extent(1,self%num_ops)]
        end function
        function GetBootstrapDjExtents(self) result(res)
                class(Bootstrap), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%maxIndexDj),extent(1,self%num_ops)]
        end function
        function GetBootstrapRotsExtents(self) result(res)
                class(Bootstrap), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,3),extent(1,3),extent(1,self%num_ops)]
        end function
        function GetBootstrapShiftsExtents(self) result(res)
                class(Bootstrap), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_ops)]
        end function
        function GetBootstrapRecipExtents(self) result(res)
                class(Bootstrap), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,3)]
        end function
        subroutine AllocateBootstrapObjectFields(self)
                class(Bootstrap), intent(inout) :: self
                call self%Dl%init(int(self%maxIndexDl),int(self%num_ops))
                call self%Dj%init(int(self%maxIndexDj),int(self%num_ops))
                call self%rots%init(int(3),int(3),int(self%num_ops))
                call self%shifts%init(int(3),int(self%num_ops))
                call self%recip%init(int(3),int(3))
        end subroutine


        subroutine InitGL3Group(self)
                class(GL3Group), intent(inout) :: self
                call self%InitPersistent()
                self%num_ops =  0
                self%hasWigJ = .False.
        end subroutine
        subroutine StoreGL3GroupObjectFields(self)
                class(GL3Group), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%rots%StoreObject(ps, gid,  'rots')
                call self%WigD0%StoreObject(ps, gid,  'WigD0')
                call self%WigD1%StoreObject(ps, gid,  'WigD1')
                call self%WigD2%StoreObject(ps, gid,  'WigD2')
                call self%WigD3%StoreObject(ps, gid,  'WigD3')
                call self%WigD0J%StoreObject(ps, gid,  'WigD0J')
                call self%WigD1J%StoreObject(ps, gid,  'WigD1J')
                call self%WigD2J%StoreObject(ps, gid,  'WigD2J')
                call self%WigD3J%StoreObject(ps, gid,  'WigD3J')
        end subroutine
        subroutine LoadGL3GroupObjectFields(self)
                class(GL3Group), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%rots%LoadObject(ps, gid,  'rots')
                call self%WigD0%LoadObject(ps, gid,  'WigD0')
                call self%WigD1%LoadObject(ps, gid,  'WigD1')
                call self%WigD2%LoadObject(ps, gid,  'WigD2')
                call self%WigD3%LoadObject(ps, gid,  'WigD3')
                call self%WigD0J%LoadObject(ps, gid,  'WigD0J')
                call self%WigD1J%LoadObject(ps, gid,  'WigD1J')
                call self%WigD2J%LoadObject(ps, gid,  'WigD2J')
                call self%WigD3J%LoadObject(ps, gid,  'WigD3J')
        end subroutine
        subroutine ResetGL3GroupSectionFields(self)
                class(GL3Group), intent(inout) :: self
                self%rots_ => self%rots%GetWithExtents(self%GetGL3GrouprotsExtents())
                self%WigD0_ => self%WigD0%GetWithExtents(self%GetGL3GroupWigD0Extents())
                self%WigD1_ => self%WigD1%GetWithExtents(self%GetGL3GroupWigD1Extents())
                self%WigD2_ => self%WigD2%GetWithExtents(self%GetGL3GroupWigD2Extents())
                self%WigD3_ => self%WigD3%GetWithExtents(self%GetGL3GroupWigD3Extents())
                self%WigD0J_ => self%WigD0J%GetWithExtents(self%GetGL3GroupWigD0JExtents())
                self%WigD1J_ => self%WigD1J%GetWithExtents(self%GetGL3GroupWigD1JExtents())
                self%WigD2J_ => self%WigD2J%GetWithExtents(self%GetGL3GroupWigD2JExtents())
                self%WigD3J_ => self%WigD3J%GetWithExtents(self%GetGL3GroupWigD3JExtents())
        end subroutine
        subroutine DisconnectGL3GroupObjectFields(self)
                class(GL3Group), intent(inout) :: self
               type(iterator) :: iter
                call self%rots%DisconnectFromStore()
                call self%WigD0%DisconnectFromStore()
                call self%WigD1%DisconnectFromStore()
                call self%WigD2%DisconnectFromStore()
                call self%WigD3%DisconnectFromStore()
                call self%WigD0J%DisconnectFromStore()
                call self%WigD1J%DisconnectFromStore()
                call self%WigD2J%DisconnectFromStore()
                call self%WigD3J%DisconnectFromStore()
        end subroutine
        subroutine StoreGL3GroupScalarFields(self)
                class(GL3Group), intent(inout) :: self
                call self%write('num_ops', self%num_ops)
                call self%write('hasWigJ', self%hasWigJ)
        end subroutine
        subroutine LoadGL3GroupScalarFields(self)
                class(GL3Group), intent(inout) :: self
                call self%read('num_ops', self%num_ops)
                call self%read('hasWigJ', self%hasWigJ)
        end subroutine
        subroutine FinalizeGL3Group(self)
               type(GL3Group), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearGL3Group(self)
                class(GL3Group), intent(inout) :: self
                type(GL3Group), save :: empty
                self = empty
        end subroutine
        pure elemental function IsGL3GroupEqual(lhs, rhs) result(iseq)
                class(GL3Group), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (GL3Group)
                       iseq = iseq .and. (lhs%num_ops == rhs%num_ops)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rots == rhs%rots)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%WigD0 == rhs%WigD0)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%WigD1 == rhs%WigD1)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%WigD2 == rhs%WigD2)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%WigD3 == rhs%WigD3)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hasWigJ .eqv. rhs%hasWigJ)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%WigD0J == rhs%WigD0J)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%WigD1J == rhs%WigD1J)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%WigD2J == rhs%WigD2J)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%WigD3J == rhs%WigD3J)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorGL3Group(lhs, rhs)
                class(GL3Group), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (GL3Group)
                       lhs%num_ops = rhs%num_ops
                       lhs%rots = rhs%rots
                       lhs%WigD0 = rhs%WigD0
                       lhs%WigD1 = rhs%WigD1
                       lhs%WigD2 = rhs%WigD2
                       lhs%WigD3 = rhs%WigD3
                       lhs%hasWigJ = rhs%hasWigJ
                       lhs%WigD0J = rhs%WigD0J
                       lhs%WigD1J = rhs%WigD1J
                       lhs%WigD2J = rhs%WigD2J
                       lhs%WigD3J = rhs%WigD3J
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetGl3groupRotsExtents(self) result(res)
                class(GL3Group), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,3),extent(1,3),extent(1,self%num_ops)]
        end function
        function GetGl3groupWigd0Extents(self) result(res)
                class(GL3Group), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,1),extent(1,1),extent(1,self%num_ops)]
        end function
        function GetGl3groupWigd1Extents(self) result(res)
                class(GL3Group), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,3),extent(1,3),extent(1,self%num_ops)]
        end function
        function GetGl3groupWigd2Extents(self) result(res)
                class(GL3Group), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,5),extent(1,5),extent(1,self%num_ops)]
        end function
        function GetGl3groupWigd3Extents(self) result(res)
                class(GL3Group), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,7),extent(1,7),extent(1,self%num_ops)]
        end function
        function GetGl3groupWigd0jExtents(self) result(res)
                class(GL3Group), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,2),extent(1,2),extent(1,self%num_ops)]
        end function
        function GetGl3groupWigd1jExtents(self) result(res)
                class(GL3Group), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,6),extent(1,6),extent(1,self%num_ops)]
        end function
        function GetGl3groupWigd2jExtents(self) result(res)
                class(GL3Group), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,10),extent(1,10),extent(1,self%num_ops)]
        end function
        function GetGl3groupWigd3jExtents(self) result(res)
                class(GL3Group), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,14),extent(1,14),extent(1,self%num_ops)]
        end function
        subroutine AllocateGL3GroupObjectFields(self)
                class(GL3Group), intent(inout) :: self
                call self%rots%init(int(3),int(3),int(self%num_ops))
                call self%WigD0%init(int(1),int(1),int(self%num_ops))
                call self%WigD1%init(int(3),int(3),int(self%num_ops))
                call self%WigD2%init(int(5),int(5),int(self%num_ops))
                call self%WigD3%init(int(7),int(7),int(self%num_ops))
                call self%WigD0J%init(int(2),int(2),int(self%num_ops))
                call self%WigD1J%init(int(6),int(6),int(self%num_ops))
                call self%WigD2J%init(int(10),int(10),int(self%num_ops))
                call self%WigD3J%init(int(14),int(14),int(self%num_ops))
        end subroutine


        subroutine GetWignerMatricesWrapper(ret, imp_fp, selfpath) bind(C,name='symmetry_mp_getwignermatriceswrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(GetWignerMatricesI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine InitSpaceGroup(self)
                class(SpaceGroup), intent(inout) :: self
                call self%GL3Group%Init()
        end subroutine
        subroutine StoreSpaceGroupObjectFields(self)
                class(SpaceGroup), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%GL3Group%StoreObjectFields()
                call self%shifts%StoreObject(ps, gid,  'shifts')
        end subroutine
        subroutine LoadSpaceGroupObjectFields(self)
                class(SpaceGroup), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%GL3Group%LoadObjectFields()
                call self%shifts%LoadObject(ps, gid,  'shifts')
        end subroutine
        subroutine ResetSpaceGroupSectionFields(self)
                class(SpaceGroup), intent(inout) :: self
                call self%GL3Group%ResetSectionFields()
                self%shifts_ => self%shifts%GetWithExtents(self%GetSpaceGroupshiftsExtents())
        end subroutine
        subroutine DisconnectSpaceGroupObjectFields(self)
                class(SpaceGroup), intent(inout) :: self
               type(iterator) :: iter
                call self%GL3Group%DisconnectObjectFields()
                call self%shifts%DisconnectFromStore()
        end subroutine
        subroutine StoreSpaceGroupScalarFields(self)
                class(SpaceGroup), intent(inout) :: self
                call self%GL3Group%StoreScalarFields()
        end subroutine
        subroutine LoadSpaceGroupScalarFields(self)
                class(SpaceGroup), intent(inout) :: self
                call self%GL3Group%LoadScalarFields()
        end subroutine
        subroutine FinalizeSpaceGroup(self)
               type(SpaceGroup), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSpaceGroup(self)
                class(SpaceGroup), intent(inout) :: self
                type(SpaceGroup), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSpaceGroupEqual(lhs, rhs) result(iseq)
                class(SpaceGroup), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (SpaceGroup)
                       iseq = iseq .and. (lhs%GL3Group == rhs%GL3Group)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%shifts == rhs%shifts)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSpaceGroup(lhs, rhs)
                class(SpaceGroup), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (SpaceGroup)
                       lhs%GL3Group = rhs%GL3Group
                       lhs%shifts = rhs%shifts
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetSpacegroupShiftsExtents(self) result(res)
                class(SpaceGroup), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_ops)]
        end function
        subroutine AllocateSpaceGroupObjectFields(self)
                class(SpaceGroup), intent(inout) :: self
                call self%GL3Group%AllocateObjectFields()
                call self%shifts%init(int(3),int(self%num_ops))
        end subroutine


        subroutine InitGroupRepresentation(self)
                class(GroupRepresentation), intent(inout) :: self
                call self%GL3Group%Init()
                self%dim =  0
        end subroutine
        subroutine StoreGroupRepresentationObjectFields(self)
                class(GroupRepresentation), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%GL3Group%StoreObjectFields()
                call self%rep%StoreObject(ps, gid,  'rep')
        end subroutine
        subroutine LoadGroupRepresentationObjectFields(self)
                class(GroupRepresentation), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%GL3Group%LoadObjectFields()
                call self%rep%LoadObject(ps, gid,  'rep')
        end subroutine
        subroutine ResetGroupRepresentationSectionFields(self)
                class(GroupRepresentation), intent(inout) :: self
                call self%GL3Group%ResetSectionFields()
                self%rep_ => self%rep%GetWithExtents(self%GetGroupRepresentationrepExtents())
        end subroutine
        subroutine DisconnectGroupRepresentationObjectFields(self)
                class(GroupRepresentation), intent(inout) :: self
               type(iterator) :: iter
                call self%GL3Group%DisconnectObjectFields()
                call self%rep%DisconnectFromStore()
        end subroutine
        subroutine StoreGroupRepresentationScalarFields(self)
                class(GroupRepresentation), intent(inout) :: self
                call self%GL3Group%StoreScalarFields()
                call self%write('dim', self%dim)
        end subroutine
        subroutine LoadGroupRepresentationScalarFields(self)
                class(GroupRepresentation), intent(inout) :: self
                call self%GL3Group%LoadScalarFields()
                call self%read('dim', self%dim)
        end subroutine
        subroutine FinalizeGroupRepresentation(self)
               type(GroupRepresentation), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearGroupRepresentation(self)
                class(GroupRepresentation), intent(inout) :: self
                type(GroupRepresentation), save :: empty
                self = empty
        end subroutine
        pure elemental function IsGroupRepresentationEqual(lhs, rhs) result(iseq)
                class(GroupRepresentation), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (GroupRepresentation)
                       iseq = iseq .and. (lhs%GL3Group == rhs%GL3Group)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dim == rhs%dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rep == rhs%rep)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorGroupRepresentation(lhs, rhs)
                class(GroupRepresentation), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (GroupRepresentation)
                       lhs%GL3Group = rhs%GL3Group
                       lhs%dim = rhs%dim
                       lhs%rep = rhs%rep
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetGrouprepresentationRepExtents(self) result(res)
                class(GroupRepresentation), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_ops)]
        end function
        subroutine AllocateGroupRepresentationObjectFields(self)
                class(GroupRepresentation), intent(inout) :: self
                call self%GL3Group%AllocateObjectFields()
                call self%rep%init(int(self%dim),int(self%dim),int(self%num_ops))
        end subroutine


        subroutine InitOrbitalLabels(self)
                class(OrbitalLabels), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreOrbitalLabelsObjectFields(self)
                class(OrbitalLabels), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadOrbitalLabelsObjectFields(self)
                class(OrbitalLabels), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetOrbitalLabelsSectionFields(self)
                class(OrbitalLabels), intent(inout) :: self
        end subroutine
        subroutine DisconnectOrbitalLabelsObjectFields(self)
                class(OrbitalLabels), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreOrbitalLabelsScalarFields(self)
                class(OrbitalLabels), intent(inout) :: self
                call self%write('short_label', self%short_label)
                call self%write('latex_label', self%latex_label)
        end subroutine
        subroutine LoadOrbitalLabelsScalarFields(self)
                class(OrbitalLabels), intent(inout) :: self
                call self%read('short_label', self%short_label)
                call self%read('latex_label', self%latex_label)
        end subroutine
        subroutine FinalizeOrbitalLabels(self)
               type(OrbitalLabels), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOrbitalLabels(self)
                class(OrbitalLabels), intent(inout) :: self
                type(OrbitalLabels), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOrbitalLabelsEqual(lhs, rhs) result(iseq)
                class(OrbitalLabels), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (OrbitalLabels)
                       iseq = iseq .and. (lhs%short_label == rhs%short_label)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%latex_label == rhs%latex_label)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOrbitalLabels(lhs, rhs)
                class(OrbitalLabels), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (OrbitalLabels)
                       lhs%short_label = rhs%short_label
                       lhs%latex_label = rhs%latex_label
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateOrbitalLabelsObjectFields(self)
                class(OrbitalLabels), intent(inout) :: self
        end subroutine


        subroutine InitAnnotatedRepresentation(self)
                class(AnnotatedRepresentation), intent(inout) :: self
                call self%InitPersistent()
                self%num_ops =  0
                self%l =  0
                self%nrel = .False.
                self%num_si = .False.
                self%dim =  0
                self%num_blocks =  0
                self%blocking_tol =  0.0d0
                self%G_tolerance =  0.0d0
        end subroutine
        subroutine StoreAnnotatedRepresentationObjectFields(self)
                class(AnnotatedRepresentation), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%rep%StoreObject(ps, gid,  'rep')
                call self%blocks%StoreObject(ps, gid,  'blocks')
                call self%characters%StoreObject(ps, gid,  'characters')
                call self%chiHchi%StoreObject(ps, gid,  'chiHchi')
                call self%H%StoreObject(ps, gid,  'H')
                call self%rot%StoreObject(ps, gid,  'rot')
                call self%rotD%StoreObject(ps, gid,  'rotD')
                call self%basisChangeD%StoreObject(ps, gid,  'basisChangeD')

                call iter%Init(lbound(self%labels), ubound(self%labels))
                do while (.not. iter%Done())
                    call self%labels(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'labels', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadAnnotatedRepresentationObjectFields(self)
                class(AnnotatedRepresentation), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%rep%LoadObject(ps, gid,  'rep')
                call self%blocks%LoadObject(ps, gid,  'blocks')
                call self%characters%LoadObject(ps, gid,  'characters')
                call self%chiHchi%LoadObject(ps, gid,  'chiHchi')
                call self%H%LoadObject(ps, gid,  'H')
                call self%rot%LoadObject(ps, gid,  'rot')
                call self%rotD%LoadObject(ps, gid,  'rotD')
                call self%basisChangeD%LoadObject(ps, gid,  'basisChangeD')

                allocate(self%labels(int(1):int(self%dim)))
                call iter%Init(lbound(self%labels), ubound(self%labels))
                do while (.not. iter%Done())
                    call self%labels(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'labels', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetAnnotatedRepresentationSectionFields(self)
                class(AnnotatedRepresentation), intent(inout) :: self
                self%rep_ => self%rep%GetWithExtents(self%GetAnnotatedRepresentationrepExtents())
                self%blocks_ => self%blocks%GetWithExtents(self%GetAnnotatedRepresentationblocksExtents())
                self%characters_ => self%characters%GetWithExtents(self%GetAnnotatedRepresentationcharactersExtents())
                self%chiHchi_ => self%chiHchi%GetWithExtents(self%GetAnnotatedRepresentationchiHchiExtents())
                self%H_ => self%H%GetWithExtents(self%GetAnnotatedRepresentationHExtents())
                self%rot_ => self%rot%GetWithExtents(self%GetAnnotatedRepresentationrotExtents())
                self%rotD_ => self%rotD%GetWithExtents(self%GetAnnotatedRepresentationrotDExtents())
                self%basisChangeD_ => self%basisChangeD%GetWithExtents(self%GetAnnotatedRepresentationbasisChangeDExtents())
        end subroutine
        subroutine DisconnectAnnotatedRepresentationObjectFields(self)
                class(AnnotatedRepresentation), intent(inout) :: self
               type(iterator) :: iter
                call self%rep%DisconnectFromStore()
                call self%blocks%DisconnectFromStore()
                call self%characters%DisconnectFromStore()
                call self%chiHchi%DisconnectFromStore()
                call self%H%DisconnectFromStore()
                call self%rot%DisconnectFromStore()
                call self%rotD%DisconnectFromStore()
                call self%basisChangeD%DisconnectFromStore()

                call iter%Init(lbound(self%labels), ubound(self%labels))
                do while (.not. iter%Done())
                    call self%labels(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreAnnotatedRepresentationScalarFields(self)
                class(AnnotatedRepresentation), intent(inout) :: self
                call self%write('num_ops', self%num_ops)
                call self%write('l', self%l)
                call self%write('nrel', self%nrel)
                call self%write('num_si', self%num_si)
                call self%write('dim', self%dim)
                call self%write('num_blocks', self%num_blocks)
                call self%write('blocking_tol', self%blocking_tol)
                call self%write('G_tolerance', self%G_tolerance)
        end subroutine
        subroutine LoadAnnotatedRepresentationScalarFields(self)
                class(AnnotatedRepresentation), intent(inout) :: self
                call self%read('num_ops', self%num_ops)
                call self%read('l', self%l)
                call self%read('nrel', self%nrel)
                call self%read('num_si', self%num_si)
                call self%read('dim', self%dim)
                call self%read('num_blocks', self%num_blocks)
                call self%read('blocking_tol', self%blocking_tol)
                call self%read('G_tolerance', self%G_tolerance)
        end subroutine
        subroutine FinalizeAnnotatedRepresentation(self)
               type(AnnotatedRepresentation), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearAnnotatedRepresentation(self)
                class(AnnotatedRepresentation), intent(inout) :: self
                type(AnnotatedRepresentation), save :: empty
                self = empty
        end subroutine
        pure elemental function IsAnnotatedRepresentationEqual(lhs, rhs) result(iseq)
                class(AnnotatedRepresentation), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (AnnotatedRepresentation)
                       iseq = iseq .and. (lhs%num_ops == rhs%num_ops)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%l == rhs%l)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrel .eqv. rhs%nrel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si .eqv. rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dim == rhs%dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_blocks == rhs%num_blocks)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%blocking_tol == rhs%blocking_tol)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%G_tolerance == rhs%G_tolerance)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rep == rhs%rep)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%blocks == rhs%blocks)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%characters == rhs%characters)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%chiHchi == rhs%chiHchi)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%H == rhs%H)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rot == rhs%rot)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rotD == rhs%rotD)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%basisChangeD == rhs%basisChangeD)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%labels) .eqv. allocated(rhs%labels))
                       if (.not. iseq) return
                       if (allocated(lhs%labels)) then
                           iseq = iseq .and. all(shape(lhs%labels) == shape(rhs%labels))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%labels(:) == rhs%labels(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorAnnotatedRepresentation(lhs, rhs)
                class(AnnotatedRepresentation), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (AnnotatedRepresentation)
                       lhs%num_ops = rhs%num_ops
                       lhs%l = rhs%l
                       lhs%nrel = rhs%nrel
                       lhs%num_si = rhs%num_si
                       lhs%dim = rhs%dim
                       lhs%num_blocks = rhs%num_blocks
                       lhs%blocking_tol = rhs%blocking_tol
                       lhs%G_tolerance = rhs%G_tolerance
                       lhs%rep = rhs%rep
                       lhs%blocks = rhs%blocks
                       lhs%characters = rhs%characters
                       lhs%chiHchi = rhs%chiHchi
                       lhs%H = rhs%H
                       lhs%rot = rhs%rot
                       lhs%rotD = rhs%rotD
                       lhs%basisChangeD = rhs%basisChangeD
                       lhs%labels = rhs%labels
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetAnnotatedrepresentationRepExtents(self) result(res)
                class(AnnotatedRepresentation), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_ops)]
        end function
        function GetAnnotatedrepresentationBlocksExtents(self) result(res)
                class(AnnotatedRepresentation), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%dim)]
        end function
        function GetAnnotatedrepresentationCharactersExtents(self) result(res)
                class(AnnotatedRepresentation), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_blocks),extent(1,self%num_ops)]
        end function
        function GetAnnotatedrepresentationChihchiExtents(self) result(res)
                class(AnnotatedRepresentation), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_blocks),extent(1,self%num_blocks)]
        end function
        function GetAnnotatedrepresentationHExtents(self) result(res)
                class(AnnotatedRepresentation), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        function GetAnnotatedrepresentationRotExtents(self) result(res)
                class(AnnotatedRepresentation), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,3)]
        end function
        function GetAnnotatedrepresentationRotdExtents(self) result(res)
                class(AnnotatedRepresentation), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        function GetAnnotatedrepresentationBasischangedExtents(self) result(res)
                class(AnnotatedRepresentation), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        subroutine AllocateAnnotatedRepresentationObjectFields(self)
                class(AnnotatedRepresentation), intent(inout) :: self
                call self%rep%init(int(self%dim),int(self%dim),int(self%num_ops))
                call self%blocks%init(int(self%dim))
                call self%characters%init(int(self%num_blocks),int(self%num_ops))
                call self%chiHchi%init(int(self%num_blocks),int(self%num_blocks))
                call self%H%init(int(self%dim),int(self%dim))
                call self%rot%init(int(3),int(3))
                call self%rotD%init(int(self%dim),int(self%dim))
                call self%basisChangeD%init(int(self%dim),int(self%dim))
                allocate(self%labels(int(1):int(self%dim)))
        end subroutine


        subroutine InitPeriodicSite(self)
                class(PeriodicSite), intent(inout) :: self
                call self%InitPersistent()
                self%site_index =  0
        end subroutine
        subroutine StorePeriodicSiteObjectFields(self)
                class(PeriodicSite), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%xyz%StoreObject(ps, gid,  'xyz')
        end subroutine
        subroutine LoadPeriodicSiteObjectFields(self)
                class(PeriodicSite), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%xyz%LoadObject(ps, gid,  'xyz')
        end subroutine
        subroutine ResetPeriodicSiteSectionFields(self)
                class(PeriodicSite), intent(inout) :: self
                self%xyz_ => self%xyz%GetWithExtents(self%GetPeriodicSitexyzExtents())
        end subroutine
        subroutine DisconnectPeriodicSiteObjectFields(self)
                class(PeriodicSite), intent(inout) :: self
               type(iterator) :: iter
                call self%xyz%DisconnectFromStore()
        end subroutine
        subroutine StorePeriodicSiteScalarFields(self)
                class(PeriodicSite), intent(inout) :: self
                call self%write('site_index', self%site_index)
        end subroutine
        subroutine LoadPeriodicSiteScalarFields(self)
                class(PeriodicSite), intent(inout) :: self
                call self%read('site_index', self%site_index)
        end subroutine
        subroutine FinalizePeriodicSite(self)
               type(PeriodicSite), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearPeriodicSite(self)
                class(PeriodicSite), intent(inout) :: self
                type(PeriodicSite), save :: empty
                self = empty
        end subroutine
        pure elemental function IsPeriodicSiteEqual(lhs, rhs) result(iseq)
                class(PeriodicSite), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (PeriodicSite)
                       iseq = iseq .and. (lhs%site_index == rhs%site_index)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%xyz == rhs%xyz)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorPeriodicSite(lhs, rhs)
                class(PeriodicSite), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (PeriodicSite)
                       lhs%site_index = rhs%site_index
                       lhs%xyz = rhs%xyz
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetPeriodicsiteXyzExtents(self) result(res)
                class(PeriodicSite), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        subroutine AllocatePeriodicSiteObjectFields(self)
                class(PeriodicSite), intent(inout) :: self
                call self%xyz%init(int(3))
        end subroutine


        subroutine InitLocalSymmetry(self)
                class(LocalSymmetry), intent(inout) :: self
                call self%InitPersistent()
                self%site_index =  0
                call self%local_group%InitPersistent()
        end subroutine
        subroutine StoreLocalSymmetryObjectFields(self)
                class(LocalSymmetry), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%local_group%store(ps%FetchSubGroup(gid,  'local_group'))

                call iter%Init(lbound(self%annotated_reps_l), ubound(self%annotated_reps_l))
                do while (.not. iter%Done())
                    call self%annotated_reps_l(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'annotated_reps_l', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadLocalSymmetryObjectFields(self)
                class(LocalSymmetry), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%local_group%load(ps%FetchSubGroup(gid,  'local_group'))

                allocate(self%annotated_reps_l(int(0):int(3)))
                call iter%Init(lbound(self%annotated_reps_l), ubound(self%annotated_reps_l))
                do while (.not. iter%Done())
                    call self%annotated_reps_l(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'annotated_reps_l', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetLocalSymmetrySectionFields(self)
                class(LocalSymmetry), intent(inout) :: self
        end subroutine
        subroutine DisconnectLocalSymmetryObjectFields(self)
                class(LocalSymmetry), intent(inout) :: self
               type(iterator) :: iter
                call self%local_group%disconnect()

                call iter%Init(lbound(self%annotated_reps_l), ubound(self%annotated_reps_l))
                do while (.not. iter%Done())
                    call self%annotated_reps_l(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreLocalSymmetryScalarFields(self)
                class(LocalSymmetry), intent(inout) :: self
                call self%write('site_index', self%site_index)
        end subroutine
        subroutine LoadLocalSymmetryScalarFields(self)
                class(LocalSymmetry), intent(inout) :: self
                call self%read('site_index', self%site_index)
        end subroutine
        subroutine FinalizeLocalSymmetry(self)
               type(LocalSymmetry), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearLocalSymmetry(self)
                class(LocalSymmetry), intent(inout) :: self
                type(LocalSymmetry), save :: empty
                self = empty
        end subroutine
        pure elemental function IsLocalSymmetryEqual(lhs, rhs) result(iseq)
                class(LocalSymmetry), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (LocalSymmetry)
                       iseq = iseq .and. (lhs%site_index == rhs%site_index)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%local_group == rhs%local_group)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%annotated_reps_l) .eqv. allocated(rhs%annotated_reps_l))
                       if (.not. iseq) return
                       if (allocated(lhs%annotated_reps_l)) then
                           iseq = iseq .and. all(shape(lhs%annotated_reps_l) == shape(rhs%annotated_reps_l))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%annotated_reps_l(:) == rhs%annotated_reps_l(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorLocalSymmetry(lhs, rhs)
                class(LocalSymmetry), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (LocalSymmetry)
                       lhs%site_index = rhs%site_index
                       lhs%local_group = rhs%local_group
                       lhs%annotated_reps_l = rhs%annotated_reps_l
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateLocalSymmetryObjectFields(self)
                class(LocalSymmetry), intent(inout) :: self
                allocate(self%annotated_reps_l(int(0):int(3)))
        end subroutine


        subroutine InitStructLocalSymmetry(self)
                class(StructLocalSymmetry), intent(inout) :: self
                call self%InitPersistent()
                self%num_distinct =  0
                self%num_atoms =  0
                self%num_si =  0
                self%nrel =  0
        end subroutine
        subroutine StoreStructLocalSymmetryObjectFields(self)
                class(StructLocalSymmetry), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                call iter%Init(lbound(self%distinct_symm), ubound(self%distinct_symm))
                do while (.not. iter%Done())
                    call self%distinct_symm(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'distinct_symm', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadStructLocalSymmetryObjectFields(self)
                class(StructLocalSymmetry), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                allocate(self%distinct_symm(int(1):int(self%num_distinct)))
                call iter%Init(lbound(self%distinct_symm), ubound(self%distinct_symm))
                do while (.not. iter%Done())
                    call self%distinct_symm(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'distinct_symm', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetStructLocalSymmetrySectionFields(self)
                class(StructLocalSymmetry), intent(inout) :: self
        end subroutine
        subroutine DisconnectStructLocalSymmetryObjectFields(self)
                class(StructLocalSymmetry), intent(inout) :: self
               type(iterator) :: iter

                call iter%Init(lbound(self%distinct_symm), ubound(self%distinct_symm))
                do while (.not. iter%Done())
                    call self%distinct_symm(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreStructLocalSymmetryScalarFields(self)
                class(StructLocalSymmetry), intent(inout) :: self
                call self%write('num_distinct', self%num_distinct)
                call self%write('num_atoms', self%num_atoms)
                call self%write('num_si', self%num_si)
                call self%write('nrel', self%nrel)
        end subroutine
        subroutine LoadStructLocalSymmetryScalarFields(self)
                class(StructLocalSymmetry), intent(inout) :: self
                call self%read('num_distinct', self%num_distinct)
                call self%read('num_atoms', self%num_atoms)
                call self%read('num_si', self%num_si)
                call self%read('nrel', self%nrel)
        end subroutine
        subroutine FinalizeStructLocalSymmetry(self)
               type(StructLocalSymmetry), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearStructLocalSymmetry(self)
                class(StructLocalSymmetry), intent(inout) :: self
                type(StructLocalSymmetry), save :: empty
                self = empty
        end subroutine
        pure elemental function IsStructLocalSymmetryEqual(lhs, rhs) result(iseq)
                class(StructLocalSymmetry), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (StructLocalSymmetry)
                       iseq = iseq .and. (lhs%num_distinct == rhs%num_distinct)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_atoms == rhs%num_atoms)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrel == rhs%nrel)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%distinct_symm) .eqv. allocated(rhs%distinct_symm))
                       if (.not. iseq) return
                       if (allocated(lhs%distinct_symm)) then
                           iseq = iseq .and. all(shape(lhs%distinct_symm) == shape(rhs%distinct_symm))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%distinct_symm(:) == rhs%distinct_symm(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorStructLocalSymmetry(lhs, rhs)
                class(StructLocalSymmetry), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (StructLocalSymmetry)
                       lhs%num_distinct = rhs%num_distinct
                       lhs%num_atoms = rhs%num_atoms
                       lhs%num_si = rhs%num_si
                       lhs%nrel = rhs%nrel
                       lhs%distinct_symm = rhs%distinct_symm
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateStructLocalSymmetryObjectFields(self)
                class(StructLocalSymmetry), intent(inout) :: self
                allocate(self%distinct_symm(int(1):int(self%num_distinct)))
        end subroutine



end module
