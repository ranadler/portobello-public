
module force
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use cube


    implicit none
    public

    type, extends(persistent) :: FlapwForce
        integer(kind=int32)  ::  num_atoms =  0

        integer(kind=int32)  ::  num_cont =  int(5,kind=int32)

        type(cube_real)  ::  force_cont
        real(kind=dp),pointer :: force_cont_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateFlapwForceObjectFields
        procedure :: ResetSectionFields   => ResetFlapwForceSectionFields
        procedure :: StoreScalarFields    => StoreFlapwForceScalarFields
        procedure :: StoreObjectFields    => StoreFlapwForceObjectFields
        procedure :: LoadScalarFields     => LoadFlapwForceScalarFields
        procedure :: LoadObjectFields     => LoadFlapwForceObjectFields
        procedure :: DisconnectObjectFields => DisconnectFlapwForceObjectFields
        procedure :: IsEqual              => IsFlapwForceEqual
        procedure :: AssignmentOperator   => AssignmentOperatorFlapwForce
        procedure :: clear                => ClearFlapwForce
        procedure :: init => InitFlapwForce
#ifndef __GFORTRAN__
        final     :: FinalizeFlapwForce
#endif
        procedure :: GetFlapwforceForce_contExtents

    end type
    interface
         function CalculateForceI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    contains
        subroutine InitFlapwForce(self)
                class(FlapwForce), intent(inout) :: self
                call self%InitPersistent()
                self%num_atoms =  0
                self%num_cont =  int(5,kind=int32)
        end subroutine
        subroutine StoreFlapwForceObjectFields(self)
                class(FlapwForce), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%force_cont%StoreObject(ps, gid,  'force_cont')
        end subroutine
        subroutine LoadFlapwForceObjectFields(self)
                class(FlapwForce), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%force_cont%LoadObject(ps, gid,  'force_cont')
        end subroutine
        subroutine ResetFlapwForceSectionFields(self)
                class(FlapwForce), intent(inout) :: self
                self%force_cont_ => self%force_cont%GetWithExtents(self%GetFlapwForceforce_contExtents())
        end subroutine
        subroutine DisconnectFlapwForceObjectFields(self)
                class(FlapwForce), intent(inout) :: self
               type(iterator) :: iter
                call self%force_cont%DisconnectFromStore()
        end subroutine
        subroutine StoreFlapwForceScalarFields(self)
                class(FlapwForce), intent(inout) :: self
                call self%write('num_atoms', self%num_atoms)
                call self%write('num_cont', self%num_cont)
        end subroutine
        subroutine LoadFlapwForceScalarFields(self)
                class(FlapwForce), intent(inout) :: self
                call self%read('num_atoms', self%num_atoms)
                call self%read('num_cont', self%num_cont)
        end subroutine
        subroutine FinalizeFlapwForce(self)
               type(FlapwForce), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearFlapwForce(self)
                class(FlapwForce), intent(inout) :: self
                type(FlapwForce), save :: empty
                self = empty
        end subroutine
        pure elemental function IsFlapwForceEqual(lhs, rhs) result(iseq)
                class(FlapwForce), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (FlapwForce)
                       iseq = iseq .and. (lhs%num_atoms == rhs%num_atoms)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_cont == rhs%num_cont)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%force_cont == rhs%force_cont)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorFlapwForce(lhs, rhs)
                class(FlapwForce), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (FlapwForce)
                       lhs%num_atoms = rhs%num_atoms
                       lhs%num_cont = rhs%num_cont
                       lhs%force_cont = rhs%force_cont
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetFlapwforceForce_contExtents(self) result(res)
                class(FlapwForce), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,3),extent(1,self%num_atoms),extent(1,self%num_cont)]
        end function
        subroutine AllocateFlapwForceObjectFields(self)
                class(FlapwForce), intent(inout) :: self
                call self%force_cont%init(int(3),int(self%num_atoms),int(self%num_cont))
        end subroutine


        subroutine CalculateForceWrapper(ret, imp_fp, selfpath) bind(C,name='force_mp_calculateforcewrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(CalculateForceI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine


end module
