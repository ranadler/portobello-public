
module susceptibility
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use array5
    use array6
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: LatticeFunction
        integer(kind=int32)  ::  num_omega =  0

        integer(kind=int32)  ::  num_nu =  0

        integer(kind=int32)  ::  num_bands =  0

        integer(kind=int32)  ::  num_si =  0

        integer(kind=int32)  ::  num_k =  0

        integer(kind=int32)  ::  num_q =  0

        type(vector_real)  ::  omega
        real(kind=dp),pointer :: omega_(:)
        type(vector_real)  ::  nu
        real(kind=dp),pointer :: nu_(:)


        contains
        procedure :: AllocateObjectFields => AllocateLatticeFunctionObjectFields
        procedure :: ResetSectionFields   => ResetLatticeFunctionSectionFields
        procedure :: StoreScalarFields    => StoreLatticeFunctionScalarFields
        procedure :: StoreObjectFields    => StoreLatticeFunctionObjectFields
        procedure :: LoadScalarFields     => LoadLatticeFunctionScalarFields
        procedure :: LoadObjectFields     => LoadLatticeFunctionObjectFields
        procedure :: DisconnectObjectFields => DisconnectLatticeFunctionObjectFields
        procedure :: IsEqual              => IsLatticeFunctionEqual
        procedure :: AssignmentOperator   => AssignmentOperatorLatticeFunction
        procedure :: clear                => ClearLatticeFunction
        procedure :: init => InitLatticeFunction
#ifndef __GFORTRAN__
        final     :: FinalizeLatticeFunction
#endif
        procedure :: GetLatticefunctionOmegaExtents
        procedure :: GetLatticefunctionNuExtents

    end type

    type, extends(LatticeFunction) :: LatticeGreensFunctions
        type(array6_complex)  ::  M
        complex(kind=dp),pointer :: M_(:,:,:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateLatticeGreensFunctionsObjectFields
        procedure :: ResetSectionFields   => ResetLatticeGreensFunctionsSectionFields
        procedure :: StoreScalarFields    => StoreLatticeGreensFunctionsScalarFields
        procedure :: StoreObjectFields    => StoreLatticeGreensFunctionsObjectFields
        procedure :: LoadScalarFields     => LoadLatticeGreensFunctionsScalarFields
        procedure :: LoadObjectFields     => LoadLatticeGreensFunctionsObjectFields
        procedure :: DisconnectObjectFields => DisconnectLatticeGreensFunctionsObjectFields
        procedure :: IsEqual              => IsLatticeGreensFunctionsEqual
        procedure :: AssignmentOperator   => AssignmentOperatorLatticeGreensFunctions
        procedure :: clear                => ClearLatticeGreensFunctions
        procedure :: init => InitLatticeGreensFunctions
#ifndef __GFORTRAN__
        final     :: FinalizeLatticeGreensFunctions
#endif
        procedure :: GetLatticegreensfunctionsMExtents

    end type

    type, extends(LatticeFunction) :: LatticeBubble_at_qnu
        type(array6_complex)  ::  M
        complex(kind=dp),pointer :: M_(:,:,:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateLatticeBubble_at_qnuObjectFields
        procedure :: ResetSectionFields   => ResetLatticeBubble_at_qnuSectionFields
        procedure :: StoreScalarFields    => StoreLatticeBubble_at_qnuScalarFields
        procedure :: StoreObjectFields    => StoreLatticeBubble_at_qnuObjectFields
        procedure :: LoadScalarFields     => LoadLatticeBubble_at_qnuScalarFields
        procedure :: LoadObjectFields     => LoadLatticeBubble_at_qnuObjectFields
        procedure :: DisconnectObjectFields => DisconnectLatticeBubble_at_qnuObjectFields
        procedure :: IsEqual              => IsLatticeBubble_at_qnuEqual
        procedure :: AssignmentOperator   => AssignmentOperatorLatticeBubble_at_qnu
        procedure :: clear                => ClearLatticeBubble_at_qnu
        procedure :: init => InitLatticeBubble_at_qnu
#ifndef __GFORTRAN__
        final     :: FinalizeLatticeBubble_at_qnu
#endif
        procedure :: GetLatticebubble_at_qnuMExtents

    end type

    type, extends(persistent) :: IrreducibleVertex
        integer(kind=int32)  ::  num_omega =  0

        integer(kind=int32)  ::  num_bands =  0

        integer(kind=int32)  ::  num_kind =  0

        type(array5_complex)  ::  M
        complex(kind=dp),pointer :: M_(:,:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateIrreducibleVertexObjectFields
        procedure :: ResetSectionFields   => ResetIrreducibleVertexSectionFields
        procedure :: StoreScalarFields    => StoreIrreducibleVertexScalarFields
        procedure :: StoreObjectFields    => StoreIrreducibleVertexObjectFields
        procedure :: LoadScalarFields     => LoadIrreducibleVertexScalarFields
        procedure :: LoadObjectFields     => LoadIrreducibleVertexObjectFields
        procedure :: DisconnectObjectFields => DisconnectIrreducibleVertexObjectFields
        procedure :: IsEqual              => IsIrreducibleVertexEqual
        procedure :: AssignmentOperator   => AssignmentOperatorIrreducibleVertex
        procedure :: clear                => ClearIrreducibleVertex
        procedure :: init => InitIrreducibleVertex
#ifndef __GFORTRAN__
        final     :: FinalizeIrreducibleVertex
#endif
        procedure :: GetIrreduciblevertexMExtents

    end type

    type, extends(LatticeFunction) :: LatticeSusceptibility
        integer(kind=int32)  ::  num_kind =  0

        type(array6_complex)  ::  M
        complex(kind=dp),pointer :: M_(:,:,:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateLatticeSusceptibilityObjectFields
        procedure :: ResetSectionFields   => ResetLatticeSusceptibilitySectionFields
        procedure :: StoreScalarFields    => StoreLatticeSusceptibilityScalarFields
        procedure :: StoreObjectFields    => StoreLatticeSusceptibilityObjectFields
        procedure :: LoadScalarFields     => LoadLatticeSusceptibilityScalarFields
        procedure :: LoadObjectFields     => LoadLatticeSusceptibilityObjectFields
        procedure :: DisconnectObjectFields => DisconnectLatticeSusceptibilityObjectFields
        procedure :: IsEqual              => IsLatticeSusceptibilityEqual
        procedure :: AssignmentOperator   => AssignmentOperatorLatticeSusceptibility
        procedure :: clear                => ClearLatticeSusceptibility
        procedure :: init => InitLatticeSusceptibility
#ifndef __GFORTRAN__
        final     :: FinalizeLatticeSusceptibility
#endif
        procedure :: GetLatticesusceptibilityMExtents

    end type

    type, extends(LatticeFunction) :: ContinuedLatticeSusceptibility
        integer(kind=int32)  ::  num_kind =  0

        type(matrix_complex)  ::  M
        complex(kind=dp),pointer :: M_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateContinuedLatticeSusceptibilityObjectFields
        procedure :: ResetSectionFields   => ResetContinuedLatticeSusceptibilitySectionFields
        procedure :: StoreScalarFields    => StoreContinuedLatticeSusceptibilityScalarFields
        procedure :: StoreObjectFields    => StoreContinuedLatticeSusceptibilityObjectFields
        procedure :: LoadScalarFields     => LoadContinuedLatticeSusceptibilityScalarFields
        procedure :: LoadObjectFields     => LoadContinuedLatticeSusceptibilityObjectFields
        procedure :: DisconnectObjectFields => DisconnectContinuedLatticeSusceptibilityObjectFields
        procedure :: IsEqual              => IsContinuedLatticeSusceptibilityEqual
        procedure :: AssignmentOperator   => AssignmentOperatorContinuedLatticeSusceptibility
        procedure :: clear                => ClearContinuedLatticeSusceptibility
        procedure :: init => InitContinuedLatticeSusceptibility
#ifndef __GFORTRAN__
        final     :: FinalizeContinuedLatticeSusceptibility
#endif
        procedure :: GetContinuedlatticesusceptibilityMExtents

    end type

    type, extends(persistent) :: SusceptibilityCalculation
        type(string)  ::  next_todo
        integer(kind=int32)  ::  current_nu =  int(0,kind=int32)

        integer(kind=int32)  ::  current_q =  int(0,kind=int32)

        integer(kind=int32)  ::  num_q =  int(0,kind=int32)

        integer(kind=int32)  ::  num_nu =  int(0,kind=int32)

        integer(kind=int32)  ::  num_k =  int(0,kind=int32)

        integer(kind=int32)  ::  num_omega =  int(0,kind=int32)

        integer(kind=int32)  ::  num_bands =  int(0,kind=int32)



        contains
        procedure :: AllocateObjectFields => AllocateSusceptibilityCalculationObjectFields
        procedure :: ResetSectionFields   => ResetSusceptibilityCalculationSectionFields
        procedure :: StoreScalarFields    => StoreSusceptibilityCalculationScalarFields
        procedure :: StoreObjectFields    => StoreSusceptibilityCalculationObjectFields
        procedure :: LoadScalarFields     => LoadSusceptibilityCalculationScalarFields
        procedure :: LoadObjectFields     => LoadSusceptibilityCalculationObjectFields
        procedure :: DisconnectObjectFields => DisconnectSusceptibilityCalculationObjectFields
        procedure :: IsEqual              => IsSusceptibilityCalculationEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSusceptibilityCalculation
        procedure :: clear                => ClearSusceptibilityCalculation
        procedure :: init => InitSusceptibilityCalculation
#ifndef __GFORTRAN__
        final     :: FinalizeSusceptibilityCalculation
#endif

    end type

    contains
        subroutine InitLatticeFunction(self)
                class(LatticeFunction), intent(inout) :: self
                call self%InitPersistent()
                self%num_omega =  0
                self%num_nu =  0
                self%num_bands =  0
                self%num_si =  0
                self%num_k =  0
                self%num_q =  0
        end subroutine
        subroutine StoreLatticeFunctionObjectFields(self)
                class(LatticeFunction), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omega%StoreObject(ps, gid,  'omega')
                call self%nu%StoreObject(ps, gid,  'nu')
        end subroutine
        subroutine LoadLatticeFunctionObjectFields(self)
                class(LatticeFunction), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omega%LoadObject(ps, gid,  'omega')
                call self%nu%LoadObject(ps, gid,  'nu')
        end subroutine
        subroutine ResetLatticeFunctionSectionFields(self)
                class(LatticeFunction), intent(inout) :: self
                self%omega_ => self%omega%GetWithExtents(self%GetLatticeFunctionomegaExtents())
                self%nu_ => self%nu%GetWithExtents(self%GetLatticeFunctionnuExtents())
        end subroutine
        subroutine DisconnectLatticeFunctionObjectFields(self)
                class(LatticeFunction), intent(inout) :: self
               type(iterator) :: iter
                call self%omega%DisconnectFromStore()
                call self%nu%DisconnectFromStore()
        end subroutine
        subroutine StoreLatticeFunctionScalarFields(self)
                class(LatticeFunction), intent(inout) :: self
                call self%write('num_omega', self%num_omega)
                call self%write('num_nu', self%num_nu)
                call self%write('num_bands', self%num_bands)
                call self%write('num_si', self%num_si)
                call self%write('num_k', self%num_k)
                call self%write('num_q', self%num_q)
        end subroutine
        subroutine LoadLatticeFunctionScalarFields(self)
                class(LatticeFunction), intent(inout) :: self
                call self%read('num_omega', self%num_omega)
                call self%read('num_nu', self%num_nu)
                call self%read('num_bands', self%num_bands)
                call self%read('num_si', self%num_si)
                call self%read('num_k', self%num_k)
                call self%read('num_q', self%num_q)
        end subroutine
        subroutine FinalizeLatticeFunction(self)
               type(LatticeFunction), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearLatticeFunction(self)
                class(LatticeFunction), intent(inout) :: self
                type(LatticeFunction), save :: empty
                self = empty
        end subroutine
        pure elemental function IsLatticeFunctionEqual(lhs, rhs) result(iseq)
                class(LatticeFunction), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (LatticeFunction)
                       iseq = iseq .and. (lhs%num_omega == rhs%num_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_nu == rhs%num_nu)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_bands == rhs%num_bands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_q == rhs%num_q)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega == rhs%omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nu == rhs%nu)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorLatticeFunction(lhs, rhs)
                class(LatticeFunction), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (LatticeFunction)
                       lhs%num_omega = rhs%num_omega
                       lhs%num_nu = rhs%num_nu
                       lhs%num_bands = rhs%num_bands
                       lhs%num_si = rhs%num_si
                       lhs%num_k = rhs%num_k
                       lhs%num_q = rhs%num_q
                       lhs%omega = rhs%omega
                       lhs%nu = rhs%nu
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetLatticefunctionOmegaExtents(self) result(res)
                class(LatticeFunction), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_omega)]
        end function
        function GetLatticefunctionNuExtents(self) result(res)
                class(LatticeFunction), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_nu)]
        end function
        subroutine AllocateLatticeFunctionObjectFields(self)
                class(LatticeFunction), intent(inout) :: self
                call self%omega%init(int(self%num_omega))
                call self%nu%init(int(self%num_nu))
        end subroutine


        subroutine InitLatticeGreensFunctions(self)
                class(LatticeGreensFunctions), intent(inout) :: self
                call self%LatticeFunction%Init()
        end subroutine
        subroutine StoreLatticeGreensFunctionsObjectFields(self)
                class(LatticeGreensFunctions), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%LatticeFunction%StoreObjectFields()
                call self%M%StoreObject(ps, gid,  'M')
        end subroutine
        subroutine LoadLatticeGreensFunctionsObjectFields(self)
                class(LatticeGreensFunctions), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%LatticeFunction%LoadObjectFields()
                call self%M%LoadObject(ps, gid,  'M')
        end subroutine
        subroutine ResetLatticeGreensFunctionsSectionFields(self)
                class(LatticeGreensFunctions), intent(inout) :: self
                call self%LatticeFunction%ResetSectionFields()
                self%M_ => self%M%GetWithExtents(self%GetLatticeGreensFunctionsMExtents())
        end subroutine
        subroutine DisconnectLatticeGreensFunctionsObjectFields(self)
                class(LatticeGreensFunctions), intent(inout) :: self
               type(iterator) :: iter
                call self%LatticeFunction%DisconnectObjectFields()
                call self%M%DisconnectFromStore()
        end subroutine
        subroutine StoreLatticeGreensFunctionsScalarFields(self)
                class(LatticeGreensFunctions), intent(inout) :: self
                call self%LatticeFunction%StoreScalarFields()
        end subroutine
        subroutine LoadLatticeGreensFunctionsScalarFields(self)
                class(LatticeGreensFunctions), intent(inout) :: self
                call self%LatticeFunction%LoadScalarFields()
        end subroutine
        subroutine FinalizeLatticeGreensFunctions(self)
               type(LatticeGreensFunctions), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearLatticeGreensFunctions(self)
                class(LatticeGreensFunctions), intent(inout) :: self
                type(LatticeGreensFunctions), save :: empty
                self = empty
        end subroutine
        pure elemental function IsLatticeGreensFunctionsEqual(lhs, rhs) result(iseq)
                class(LatticeGreensFunctions), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (LatticeGreensFunctions)
                       iseq = iseq .and. (lhs%LatticeFunction == rhs%LatticeFunction)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M == rhs%M)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorLatticeGreensFunctions(lhs, rhs)
                class(LatticeGreensFunctions), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (LatticeGreensFunctions)
                       lhs%LatticeFunction = rhs%LatticeFunction
                       lhs%M = rhs%M
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetLatticegreensfunctionsMExtents(self) result(res)
                class(LatticeGreensFunctions), intent(inout) :: self
                type(extent) :: res(6)
                res = [extent(1,self%num_k),extent(1,self%num_si),extent(1,self%num_omega),extent(1,self%num_bands),extent(1,self%num_bands)]
        end function
        subroutine AllocateLatticeGreensFunctionsObjectFields(self)
                class(LatticeGreensFunctions), intent(inout) :: self
                call self%LatticeFunction%AllocateObjectFields()
                call self%M%init(int(self%num_k),int(self%num_si),int(self%num_omega),int(self%num_bands),int(self%num_bands))
        end subroutine


        subroutine InitLatticeBubble_at_qnu(self)
                class(LatticeBubble_at_qnu), intent(inout) :: self
                call self%LatticeFunction%Init()
        end subroutine
        subroutine StoreLatticeBubble_at_qnuObjectFields(self)
                class(LatticeBubble_at_qnu), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%LatticeFunction%StoreObjectFields()
                call self%M%StoreObject(ps, gid,  'M')
        end subroutine
        subroutine LoadLatticeBubble_at_qnuObjectFields(self)
                class(LatticeBubble_at_qnu), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%LatticeFunction%LoadObjectFields()
                call self%M%LoadObject(ps, gid,  'M')
        end subroutine
        subroutine ResetLatticeBubble_at_qnuSectionFields(self)
                class(LatticeBubble_at_qnu), intent(inout) :: self
                call self%LatticeFunction%ResetSectionFields()
                self%M_ => self%M%GetWithExtents(self%GetLatticeBubble_at_qnuMExtents())
        end subroutine
        subroutine DisconnectLatticeBubble_at_qnuObjectFields(self)
                class(LatticeBubble_at_qnu), intent(inout) :: self
               type(iterator) :: iter
                call self%LatticeFunction%DisconnectObjectFields()
                call self%M%DisconnectFromStore()
        end subroutine
        subroutine StoreLatticeBubble_at_qnuScalarFields(self)
                class(LatticeBubble_at_qnu), intent(inout) :: self
                call self%LatticeFunction%StoreScalarFields()
        end subroutine
        subroutine LoadLatticeBubble_at_qnuScalarFields(self)
                class(LatticeBubble_at_qnu), intent(inout) :: self
                call self%LatticeFunction%LoadScalarFields()
        end subroutine
        subroutine FinalizeLatticeBubble_at_qnu(self)
               type(LatticeBubble_at_qnu), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearLatticeBubble_at_qnu(self)
                class(LatticeBubble_at_qnu), intent(inout) :: self
                type(LatticeBubble_at_qnu), save :: empty
                self = empty
        end subroutine
        pure elemental function IsLatticeBubble_at_qnuEqual(lhs, rhs) result(iseq)
                class(LatticeBubble_at_qnu), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (LatticeBubble_at_qnu)
                       iseq = iseq .and. (lhs%LatticeFunction == rhs%LatticeFunction)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M == rhs%M)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorLatticeBubble_at_qnu(lhs, rhs)
                class(LatticeBubble_at_qnu), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (LatticeBubble_at_qnu)
                       lhs%LatticeFunction = rhs%LatticeFunction
                       lhs%M = rhs%M
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetLatticebubble_at_qnuMExtents(self) result(res)
                class(LatticeBubble_at_qnu), intent(inout) :: self
                type(extent) :: res(6)
                res = [extent(1,self%num_k),extent(1,self%num_omega),extent(1,self%num_bands),extent(1,self%num_bands),extent(1,self%num_bands),extent(1,self%num_bands)]
        end function
        subroutine AllocateLatticeBubble_at_qnuObjectFields(self)
                class(LatticeBubble_at_qnu), intent(inout) :: self
                call self%LatticeFunction%AllocateObjectFields()
                call self%M%init(int(self%num_k),int(self%num_omega),int(self%num_bands),int(self%num_bands),int(self%num_bands),int(self%num_bands))
        end subroutine


        subroutine InitIrreducibleVertex(self)
                class(IrreducibleVertex), intent(inout) :: self
                call self%InitPersistent()
                self%num_omega =  0
                self%num_bands =  0
                self%num_kind =  0
        end subroutine
        subroutine StoreIrreducibleVertexObjectFields(self)
                class(IrreducibleVertex), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%M%StoreObject(ps, gid,  'M')
        end subroutine
        subroutine LoadIrreducibleVertexObjectFields(self)
                class(IrreducibleVertex), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%M%LoadObject(ps, gid,  'M')
        end subroutine
        subroutine ResetIrreducibleVertexSectionFields(self)
                class(IrreducibleVertex), intent(inout) :: self
                self%M_ => self%M%GetWithExtents(self%GetIrreducibleVertexMExtents())
        end subroutine
        subroutine DisconnectIrreducibleVertexObjectFields(self)
                class(IrreducibleVertex), intent(inout) :: self
               type(iterator) :: iter
                call self%M%DisconnectFromStore()
        end subroutine
        subroutine StoreIrreducibleVertexScalarFields(self)
                class(IrreducibleVertex), intent(inout) :: self
                call self%write('num_omega', self%num_omega)
                call self%write('num_bands', self%num_bands)
                call self%write('num_kind', self%num_kind)
        end subroutine
        subroutine LoadIrreducibleVertexScalarFields(self)
                class(IrreducibleVertex), intent(inout) :: self
                call self%read('num_omega', self%num_omega)
                call self%read('num_bands', self%num_bands)
                call self%read('num_kind', self%num_kind)
        end subroutine
        subroutine FinalizeIrreducibleVertex(self)
               type(IrreducibleVertex), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearIrreducibleVertex(self)
                class(IrreducibleVertex), intent(inout) :: self
                type(IrreducibleVertex), save :: empty
                self = empty
        end subroutine
        pure elemental function IsIrreducibleVertexEqual(lhs, rhs) result(iseq)
                class(IrreducibleVertex), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (IrreducibleVertex)
                       iseq = iseq .and. (lhs%num_omega == rhs%num_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_bands == rhs%num_bands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_kind == rhs%num_kind)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M == rhs%M)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorIrreducibleVertex(lhs, rhs)
                class(IrreducibleVertex), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (IrreducibleVertex)
                       lhs%num_omega = rhs%num_omega
                       lhs%num_bands = rhs%num_bands
                       lhs%num_kind = rhs%num_kind
                       lhs%M = rhs%M
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetIrreduciblevertexMExtents(self) result(res)
                class(IrreducibleVertex), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(1,self%num_omega),extent(1,self%num_omega),extent(1,self%num_kind),extent(1,self%num_bands),extent(1,self%num_bands)]
        end function
        subroutine AllocateIrreducibleVertexObjectFields(self)
                class(IrreducibleVertex), intent(inout) :: self
                call self%M%init(int(self%num_omega),int(self%num_omega),int(self%num_kind),int(self%num_bands),int(self%num_bands))
        end subroutine


        subroutine InitLatticeSusceptibility(self)
                class(LatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%Init()
                self%num_kind =  0
        end subroutine
        subroutine StoreLatticeSusceptibilityObjectFields(self)
                class(LatticeSusceptibility), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%LatticeFunction%StoreObjectFields()
                call self%M%StoreObject(ps, gid,  'M')
        end subroutine
        subroutine LoadLatticeSusceptibilityObjectFields(self)
                class(LatticeSusceptibility), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%LatticeFunction%LoadObjectFields()
                call self%M%LoadObject(ps, gid,  'M')
        end subroutine
        subroutine ResetLatticeSusceptibilitySectionFields(self)
                class(LatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%ResetSectionFields()
                self%M_ => self%M%GetWithExtents(self%GetLatticeSusceptibilityMExtents())
        end subroutine
        subroutine DisconnectLatticeSusceptibilityObjectFields(self)
                class(LatticeSusceptibility), intent(inout) :: self
               type(iterator) :: iter
                call self%LatticeFunction%DisconnectObjectFields()
                call self%M%DisconnectFromStore()
        end subroutine
        subroutine StoreLatticeSusceptibilityScalarFields(self)
                class(LatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%StoreScalarFields()
                call self%write('num_kind', self%num_kind)
        end subroutine
        subroutine LoadLatticeSusceptibilityScalarFields(self)
                class(LatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%LoadScalarFields()
                call self%read('num_kind', self%num_kind)
        end subroutine
        subroutine FinalizeLatticeSusceptibility(self)
               type(LatticeSusceptibility), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearLatticeSusceptibility(self)
                class(LatticeSusceptibility), intent(inout) :: self
                type(LatticeSusceptibility), save :: empty
                self = empty
        end subroutine
        pure elemental function IsLatticeSusceptibilityEqual(lhs, rhs) result(iseq)
                class(LatticeSusceptibility), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (LatticeSusceptibility)
                       iseq = iseq .and. (lhs%LatticeFunction == rhs%LatticeFunction)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_kind == rhs%num_kind)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M == rhs%M)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorLatticeSusceptibility(lhs, rhs)
                class(LatticeSusceptibility), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (LatticeSusceptibility)
                       lhs%LatticeFunction = rhs%LatticeFunction
                       lhs%num_kind = rhs%num_kind
                       lhs%M = rhs%M
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetLatticesusceptibilityMExtents(self) result(res)
                class(LatticeSusceptibility), intent(inout) :: self
                type(extent) :: res(6)
                res = [extent(1,self%num_q),extent(1,self%num_nu),extent(1,self%num_k),extent(1,self%num_kind),extent(1,self%num_bands),extent(1,self%num_bands)]
        end function
        subroutine AllocateLatticeSusceptibilityObjectFields(self)
                class(LatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%AllocateObjectFields()
                call self%M%init(int(self%num_q),int(self%num_nu),int(self%num_k),int(self%num_kind),int(self%num_bands),int(self%num_bands))
        end subroutine


        subroutine InitContinuedLatticeSusceptibility(self)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%Init()
                self%num_kind =  0
        end subroutine
        subroutine StoreContinuedLatticeSusceptibilityObjectFields(self)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%LatticeFunction%StoreObjectFields()
                call self%M%StoreObject(ps, gid,  'M')
        end subroutine
        subroutine LoadContinuedLatticeSusceptibilityObjectFields(self)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%LatticeFunction%LoadObjectFields()
                call self%M%LoadObject(ps, gid,  'M')
        end subroutine
        subroutine ResetContinuedLatticeSusceptibilitySectionFields(self)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%ResetSectionFields()
                self%M_ => self%M%GetWithExtents(self%GetContinuedLatticeSusceptibilityMExtents())
        end subroutine
        subroutine DisconnectContinuedLatticeSusceptibilityObjectFields(self)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
               type(iterator) :: iter
                call self%LatticeFunction%DisconnectObjectFields()
                call self%M%DisconnectFromStore()
        end subroutine
        subroutine StoreContinuedLatticeSusceptibilityScalarFields(self)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%StoreScalarFields()
                call self%write('num_kind', self%num_kind)
        end subroutine
        subroutine LoadContinuedLatticeSusceptibilityScalarFields(self)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%LoadScalarFields()
                call self%read('num_kind', self%num_kind)
        end subroutine
        subroutine FinalizeContinuedLatticeSusceptibility(self)
               type(ContinuedLatticeSusceptibility), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearContinuedLatticeSusceptibility(self)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
                type(ContinuedLatticeSusceptibility), save :: empty
                self = empty
        end subroutine
        pure elemental function IsContinuedLatticeSusceptibilityEqual(lhs, rhs) result(iseq)
                class(ContinuedLatticeSusceptibility), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ContinuedLatticeSusceptibility)
                       iseq = iseq .and. (lhs%LatticeFunction == rhs%LatticeFunction)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_kind == rhs%num_kind)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M == rhs%M)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorContinuedLatticeSusceptibility(lhs, rhs)
                class(ContinuedLatticeSusceptibility), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ContinuedLatticeSusceptibility)
                       lhs%LatticeFunction = rhs%LatticeFunction
                       lhs%num_kind = rhs%num_kind
                       lhs%M = rhs%M
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetContinuedlatticesusceptibilityMExtents(self) result(res)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_q),extent(1,self%num_nu)]
        end function
        subroutine AllocateContinuedLatticeSusceptibilityObjectFields(self)
                class(ContinuedLatticeSusceptibility), intent(inout) :: self
                call self%LatticeFunction%AllocateObjectFields()
                call self%M%init(int(self%num_q),int(self%num_nu))
        end subroutine


        subroutine InitSusceptibilityCalculation(self)
                class(SusceptibilityCalculation), intent(inout) :: self
                call self%InitPersistent()
                self%next_todo = ""
                self%current_nu =  int(0,kind=int32)
                self%current_q =  int(0,kind=int32)
                self%num_q =  int(0,kind=int32)
                self%num_nu =  int(0,kind=int32)
                self%num_k =  int(0,kind=int32)
                self%num_omega =  int(0,kind=int32)
                self%num_bands =  int(0,kind=int32)
        end subroutine
        subroutine StoreSusceptibilityCalculationObjectFields(self)
                class(SusceptibilityCalculation), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadSusceptibilityCalculationObjectFields(self)
                class(SusceptibilityCalculation), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetSusceptibilityCalculationSectionFields(self)
                class(SusceptibilityCalculation), intent(inout) :: self
        end subroutine
        subroutine DisconnectSusceptibilityCalculationObjectFields(self)
                class(SusceptibilityCalculation), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreSusceptibilityCalculationScalarFields(self)
                class(SusceptibilityCalculation), intent(inout) :: self
                call self%write('next_todo', self%next_todo)
                call self%write('current_nu', self%current_nu)
                call self%write('current_q', self%current_q)
                call self%write('num_q', self%num_q)
                call self%write('num_nu', self%num_nu)
                call self%write('num_k', self%num_k)
                call self%write('num_omega', self%num_omega)
                call self%write('num_bands', self%num_bands)
        end subroutine
        subroutine LoadSusceptibilityCalculationScalarFields(self)
                class(SusceptibilityCalculation), intent(inout) :: self
                call self%read('next_todo', self%next_todo)
                call self%read('current_nu', self%current_nu)
                call self%read('current_q', self%current_q)
                call self%read('num_q', self%num_q)
                call self%read('num_nu', self%num_nu)
                call self%read('num_k', self%num_k)
                call self%read('num_omega', self%num_omega)
                call self%read('num_bands', self%num_bands)
        end subroutine
        subroutine FinalizeSusceptibilityCalculation(self)
               type(SusceptibilityCalculation), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSusceptibilityCalculation(self)
                class(SusceptibilityCalculation), intent(inout) :: self
                type(SusceptibilityCalculation), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSusceptibilityCalculationEqual(lhs, rhs) result(iseq)
                class(SusceptibilityCalculation), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (SusceptibilityCalculation)
                       iseq = iseq .and. (lhs%next_todo == rhs%next_todo)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%current_nu == rhs%current_nu)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%current_q == rhs%current_q)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_q == rhs%num_q)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_nu == rhs%num_nu)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_omega == rhs%num_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_bands == rhs%num_bands)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSusceptibilityCalculation(lhs, rhs)
                class(SusceptibilityCalculation), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (SusceptibilityCalculation)
                       lhs%next_todo = rhs%next_todo
                       lhs%current_nu = rhs%current_nu
                       lhs%current_q = rhs%current_q
                       lhs%num_q = rhs%num_q
                       lhs%num_nu = rhs%num_nu
                       lhs%num_k = rhs%num_k
                       lhs%num_omega = rhs%num_omega
                       lhs%num_bands = rhs%num_bands
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateSusceptibilityCalculationObjectFields(self)
                class(SusceptibilityCalculation), intent(inout) :: self
        end subroutine



end module
