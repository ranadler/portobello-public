
module FlapwMBPT_interface
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use matrix
    use symmetry
    use vector


    implicit none
    public

    type, extends(persistent) :: CrystalStructure
        integer(kind=int32)  ::  num_atoms =  0

        integer(kind=int32)  ::  num_distinct =  0

        type(vector_real)  ::  a
        real(kind=dp),pointer :: a_(:)
        type(vector_real)  ::  b
        real(kind=dp),pointer :: b_(:)
        type(vector_real)  ::  c
        real(kind=dp),pointer :: c_(:)
        type(matrix_real)  ::  xyz
        real(kind=dp),pointer :: xyz_(:,:)
        type(vector_real)  ::  Z
        real(kind=dp),pointer :: Z_(:)
        type(vector_int)  ::  distinct_number
        integer(kind=int32),pointer :: distinct_number_(:)
        type(vector_int)  ::  max_l_per_distinct
        integer(kind=int32),pointer :: max_l_per_distinct_(:)
        integer(kind=int32)  ::  max_l =  0



        contains
        procedure :: AllocateObjectFields => AllocateCrystalStructureObjectFields
        procedure :: ResetSectionFields   => ResetCrystalStructureSectionFields
        procedure :: StoreScalarFields    => StoreCrystalStructureScalarFields
        procedure :: StoreObjectFields    => StoreCrystalStructureObjectFields
        procedure :: LoadScalarFields     => LoadCrystalStructureScalarFields
        procedure :: LoadObjectFields     => LoadCrystalStructureObjectFields
        procedure :: DisconnectObjectFields => DisconnectCrystalStructureObjectFields
        procedure :: IsEqual              => IsCrystalStructureEqual
        procedure :: AssignmentOperator   => AssignmentOperatorCrystalStructure
        procedure :: clear                => ClearCrystalStructure
        procedure :: init => InitCrystalStructure
#ifndef __GFORTRAN__
        final     :: FinalizeCrystalStructure
#endif
        procedure :: GetCrystalstructureAExtents
        procedure :: GetCrystalstructureBExtents
        procedure :: GetCrystalstructureCExtents
        procedure :: GetCrystalstructureXyzExtents
        procedure :: GetCrystalstructureZExtents
        procedure :: GetCrystalstructureDistinct_numberExtents
        procedure :: GetCrystalstructureMax_l_per_distinctExtents

    end type

    type, extends(persistent) :: Convergence
        integer(kind=int32)  ::  num_valence_electrons =  int(0,kind=int32)

        real(kind=dp)  ::  sum_valence_electrons =  0.0d0

        real(kind=dp)  ::  charge_convergence =  0.0d0

        real(kind=dp)  ::  mag_convergence =  0.0d0



        contains
        procedure :: AllocateObjectFields => AllocateConvergenceObjectFields
        procedure :: ResetSectionFields   => ResetConvergenceSectionFields
        procedure :: StoreScalarFields    => StoreConvergenceScalarFields
        procedure :: StoreObjectFields    => StoreConvergenceObjectFields
        procedure :: LoadScalarFields     => LoadConvergenceScalarFields
        procedure :: LoadObjectFields     => LoadConvergenceObjectFields
        procedure :: DisconnectObjectFields => DisconnectConvergenceObjectFields
        procedure :: IsEqual              => IsConvergenceEqual
        procedure :: AssignmentOperator   => AssignmentOperatorConvergence
        procedure :: clear                => ClearConvergence
        procedure :: init => InitConvergence
#ifndef __GFORTRAN__
        final     :: FinalizeConvergence
#endif

    end type

    type, extends(persistent) :: DFT
        integer(kind=int32)  ::  num_k_all =  0

        integer(kind=int32)  ::  num_k_irr =  0

        integer(kind=int32)  ::  num_si =  int(1,kind=int32)

        integer(kind=int32)  ::  nrel =  int(1,kind=int32)

        type(SpaceGroup)  ::  sg
        type(CrystalStructure)  ::  st
        logical  ::  is_sharded = .False.

        integer(kind=int32)  ::  shard_offset =  int(0,kind=int32)

        integer(kind=int32)  ::  num_k =  0

        type(vector_int)  ::  k_irr
        integer(kind=int32),pointer :: k_irr_(:)
        type(vector_int)  ::  ig
        integer(kind=int32),pointer :: ig_(:)
        integer(kind=int32)  ::  num_bands =  0

        integer(kind=int32)  ::  num_valence_electrons =  0



        contains
        procedure :: AllocateObjectFields => AllocateDFTObjectFields
        procedure :: ResetSectionFields   => ResetDFTSectionFields
        procedure :: StoreScalarFields    => StoreDFTScalarFields
        procedure :: StoreObjectFields    => StoreDFTObjectFields
        procedure :: LoadScalarFields     => LoadDFTScalarFields
        procedure :: LoadObjectFields     => LoadDFTObjectFields
        procedure :: DisconnectObjectFields => DisconnectDFTObjectFields
        procedure :: IsEqual              => IsDFTEqual
        procedure :: AssignmentOperator   => AssignmentOperatorDFT
        procedure :: clear                => ClearDFT
        procedure :: init => InitDFT
#ifndef __GFORTRAN__
        final     :: FinalizeDFT
#endif
        procedure :: GetDftK_irrExtents
        procedure :: GetDftIgExtents

    end type
    interface
         function AdjustLAPWBasisI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function DFTPlusSigmaOneIterationI(selfpath, sigma_location, checkpoint, search_mu, update_lapw, admix, save_potentials, mu_search_variant) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                 type(string), intent(in)            :: sigma_location
                integer(kind=c_int),intent(in),value :: checkpoint
                integer(kind=c_int),intent(in),value :: search_mu
                integer(kind=c_int),intent(in),value :: update_lapw
                real(kind=c_float),intent(in),value :: admix
                integer(kind=c_int),intent(in),value :: save_potentials
                 type(string), intent(in)            :: mu_search_variant
         end function
    end interface

    interface
         function ConnectFlapwMBPTI(selfpath, inpfile, num_workers, opts) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                 type(string), intent(in)            :: inpfile
                integer(kind=c_int),intent(in),value :: num_workers
                 type(string), intent(in)            :: opts
         end function
    end interface

    interface
         function ReloadI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function GetDFTInfoI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function GetLAPWBasisI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function GetBasisMTInfoI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function GetPartialBandStructureI(selfpath, min_band, max_band, k) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
                integer(kind=c_int),intent(in),value :: k
         end function
    end interface

    interface
         function GetPartialBandStructureAndGradientI(selfpath, gradpath, min_band, max_band, k) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                 type(string), intent(in)            :: gradpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
                integer(kind=c_int),intent(in),value :: k
         end function
    end interface

    interface
         function GetKpathBandsI(selfpath, kpathpath, min_band, max_band) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                 type(string), intent(in)            :: kpathpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
         end function
    end interface

    interface
         function GetKPathBandsAndGradientI(selfpath, gradpath, kpathpath, min_band, max_band) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                 type(string), intent(in)            :: gradpath
                 type(string), intent(in)            :: kpathpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
         end function
    end interface

    interface
         function GetEquivalentAtomSymmetriesI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function CreatePlotsI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function GetConvergenceI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function DisconnectI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    contains
        subroutine InitCrystalStructure(self)
                class(CrystalStructure), intent(inout) :: self
                call self%InitPersistent()
                self%num_atoms =  0
                self%num_distinct =  0
                self%max_l =  0
        end subroutine
        subroutine StoreCrystalStructureObjectFields(self)
                class(CrystalStructure), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%a%StoreObject(ps, gid,  'a')
                call self%b%StoreObject(ps, gid,  'b')
                call self%c%StoreObject(ps, gid,  'c')
                call self%xyz%StoreObject(ps, gid,  'xyz')
                call self%Z%StoreObject(ps, gid,  'Z')
                call self%distinct_number%StoreObject(ps, gid,  'distinct_number')
                call self%max_l_per_distinct%StoreObject(ps, gid,  'max_l_per_distinct')
        end subroutine
        subroutine LoadCrystalStructureObjectFields(self)
                class(CrystalStructure), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%a%LoadObject(ps, gid,  'a')
                call self%b%LoadObject(ps, gid,  'b')
                call self%c%LoadObject(ps, gid,  'c')
                call self%xyz%LoadObject(ps, gid,  'xyz')
                call self%Z%LoadObject(ps, gid,  'Z')
                call self%distinct_number%LoadObject(ps, gid,  'distinct_number')
                call self%max_l_per_distinct%LoadObject(ps, gid,  'max_l_per_distinct')
        end subroutine
        subroutine ResetCrystalStructureSectionFields(self)
                class(CrystalStructure), intent(inout) :: self
                self%a_ => self%a%GetWithExtents(self%GetCrystalStructureaExtents())
                self%b_ => self%b%GetWithExtents(self%GetCrystalStructurebExtents())
                self%c_ => self%c%GetWithExtents(self%GetCrystalStructurecExtents())
                self%xyz_ => self%xyz%GetWithExtents(self%GetCrystalStructurexyzExtents())
                self%Z_ => self%Z%GetWithExtents(self%GetCrystalStructureZExtents())
                self%distinct_number_ => self%distinct_number%GetWithExtents(self%GetCrystalStructuredistinct_numberExtents())
                self%max_l_per_distinct_ => self%max_l_per_distinct%GetWithExtents(self%GetCrystalStructuremax_l_per_distinctExtents())
        end subroutine
        subroutine DisconnectCrystalStructureObjectFields(self)
                class(CrystalStructure), intent(inout) :: self
               type(iterator) :: iter
                call self%a%DisconnectFromStore()
                call self%b%DisconnectFromStore()
                call self%c%DisconnectFromStore()
                call self%xyz%DisconnectFromStore()
                call self%Z%DisconnectFromStore()
                call self%distinct_number%DisconnectFromStore()
                call self%max_l_per_distinct%DisconnectFromStore()
        end subroutine
        subroutine StoreCrystalStructureScalarFields(self)
                class(CrystalStructure), intent(inout) :: self
                call self%write('num_atoms', self%num_atoms)
                call self%write('num_distinct', self%num_distinct)
                call self%write('max_l', self%max_l)
        end subroutine
        subroutine LoadCrystalStructureScalarFields(self)
                class(CrystalStructure), intent(inout) :: self
                call self%read('num_atoms', self%num_atoms)
                call self%read('num_distinct', self%num_distinct)
                call self%read('max_l', self%max_l)
        end subroutine
        subroutine FinalizeCrystalStructure(self)
               type(CrystalStructure), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearCrystalStructure(self)
                class(CrystalStructure), intent(inout) :: self
                type(CrystalStructure), save :: empty
                self = empty
        end subroutine
        pure elemental function IsCrystalStructureEqual(lhs, rhs) result(iseq)
                class(CrystalStructure), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (CrystalStructure)
                       iseq = iseq .and. (lhs%num_atoms == rhs%num_atoms)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_distinct == rhs%num_distinct)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%a == rhs%a)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%b == rhs%b)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%c == rhs%c)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%xyz == rhs%xyz)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Z == rhs%Z)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%distinct_number == rhs%distinct_number)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_l_per_distinct == rhs%max_l_per_distinct)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_l == rhs%max_l)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorCrystalStructure(lhs, rhs)
                class(CrystalStructure), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (CrystalStructure)
                       lhs%num_atoms = rhs%num_atoms
                       lhs%num_distinct = rhs%num_distinct
                       lhs%a = rhs%a
                       lhs%b = rhs%b
                       lhs%c = rhs%c
                       lhs%xyz = rhs%xyz
                       lhs%Z = rhs%Z
                       lhs%distinct_number = rhs%distinct_number
                       lhs%max_l_per_distinct = rhs%max_l_per_distinct
                       lhs%max_l = rhs%max_l
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetCrystalstructureAExtents(self) result(res)
                class(CrystalStructure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetCrystalstructureBExtents(self) result(res)
                class(CrystalStructure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetCrystalstructureCExtents(self) result(res)
                class(CrystalStructure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetCrystalstructureXyzExtents(self) result(res)
                class(CrystalStructure), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_atoms)]
        end function
        function GetCrystalstructureZExtents(self) result(res)
                class(CrystalStructure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_distinct)]
        end function
        function GetCrystalstructureDistinct_numberExtents(self) result(res)
                class(CrystalStructure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_atoms)]
        end function
        function GetCrystalstructureMax_l_per_distinctExtents(self) result(res)
                class(CrystalStructure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_distinct)]
        end function
        subroutine AllocateCrystalStructureObjectFields(self)
                class(CrystalStructure), intent(inout) :: self
                call self%a%init(int(3))
                call self%b%init(int(3))
                call self%c%init(int(3))
                call self%xyz%init(int(3),int(self%num_atoms))
                call self%Z%init(int(self%num_distinct))
                call self%distinct_number%init(int(self%num_atoms))
                call self%max_l_per_distinct%init(int(self%num_distinct))
        end subroutine


        subroutine InitConvergence(self)
                class(Convergence), intent(inout) :: self
                call self%InitPersistent()
                self%num_valence_electrons =  int(0,kind=int32)
                self%sum_valence_electrons =  0.0d0
                self%charge_convergence =  0.0d0
                self%mag_convergence =  0.0d0
        end subroutine
        subroutine StoreConvergenceObjectFields(self)
                class(Convergence), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadConvergenceObjectFields(self)
                class(Convergence), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetConvergenceSectionFields(self)
                class(Convergence), intent(inout) :: self
        end subroutine
        subroutine DisconnectConvergenceObjectFields(self)
                class(Convergence), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreConvergenceScalarFields(self)
                class(Convergence), intent(inout) :: self
                call self%write('num_valence_electrons', self%num_valence_electrons)
                call self%write('sum_valence_electrons', self%sum_valence_electrons)
                call self%write('charge_convergence', self%charge_convergence)
                call self%write('mag_convergence', self%mag_convergence)
        end subroutine
        subroutine LoadConvergenceScalarFields(self)
                class(Convergence), intent(inout) :: self
                call self%read('num_valence_electrons', self%num_valence_electrons)
                call self%read('sum_valence_electrons', self%sum_valence_electrons)
                call self%read('charge_convergence', self%charge_convergence)
                call self%read('mag_convergence', self%mag_convergence)
        end subroutine
        subroutine FinalizeConvergence(self)
               type(Convergence), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearConvergence(self)
                class(Convergence), intent(inout) :: self
                type(Convergence), save :: empty
                self = empty
        end subroutine
        pure elemental function IsConvergenceEqual(lhs, rhs) result(iseq)
                class(Convergence), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Convergence)
                       iseq = iseq .and. (lhs%num_valence_electrons == rhs%num_valence_electrons)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sum_valence_electrons == rhs%sum_valence_electrons)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%charge_convergence == rhs%charge_convergence)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%mag_convergence == rhs%mag_convergence)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorConvergence(lhs, rhs)
                class(Convergence), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Convergence)
                       lhs%num_valence_electrons = rhs%num_valence_electrons
                       lhs%sum_valence_electrons = rhs%sum_valence_electrons
                       lhs%charge_convergence = rhs%charge_convergence
                       lhs%mag_convergence = rhs%mag_convergence
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateConvergenceObjectFields(self)
                class(Convergence), intent(inout) :: self
        end subroutine


        subroutine InitDFT(self)
                class(DFT), intent(inout) :: self
                call self%InitPersistent()
                self%num_k_all =  0
                self%num_k_irr =  0
                self%num_si =  int(1,kind=int32)
                self%nrel =  int(1,kind=int32)
                call self%sg%InitPersistent()
                call self%st%InitPersistent()
                self%is_sharded = .False.
                self%shard_offset =  int(0,kind=int32)
                self%num_k =  0
                self%num_bands =  0
                self%num_valence_electrons =  0
        end subroutine
        subroutine StoreDFTObjectFields(self)
                class(DFT), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%sg%store(ps%FetchSubGroup(gid,  'sg'))
                call self%st%store(ps%FetchSubGroup(gid,  'st'))
                call self%k_irr%StoreObject(ps, gid,  'k_irr')
                call self%ig%StoreObject(ps, gid,  'ig')
        end subroutine
        subroutine LoadDFTObjectFields(self)
                class(DFT), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%sg%load(ps%FetchSubGroup(gid,  'sg'))
                call self%st%load(ps%FetchSubGroup(gid,  'st'))
                call self%k_irr%LoadObject(ps, gid,  'k_irr')
                call self%ig%LoadObject(ps, gid,  'ig')
        end subroutine
        subroutine ResetDFTSectionFields(self)
                class(DFT), intent(inout) :: self
                self%k_irr_ => self%k_irr%GetWithExtents(self%GetDFTk_irrExtents())
                self%ig_ => self%ig%GetWithExtents(self%GetDFTigExtents())
        end subroutine
        subroutine DisconnectDFTObjectFields(self)
                class(DFT), intent(inout) :: self
               type(iterator) :: iter
                call self%sg%disconnect()
                call self%st%disconnect()
                call self%k_irr%DisconnectFromStore()
                call self%ig%DisconnectFromStore()
        end subroutine
        subroutine StoreDFTScalarFields(self)
                class(DFT), intent(inout) :: self
                call self%write('num_k_all', self%num_k_all)
                call self%write('num_k_irr', self%num_k_irr)
                call self%write('num_si', self%num_si)
                call self%write('nrel', self%nrel)
                call self%write('is_sharded', self%is_sharded)
                call self%write('shard_offset', self%shard_offset)
                call self%write('num_k', self%num_k)
                call self%write('num_bands', self%num_bands)
                call self%write('num_valence_electrons', self%num_valence_electrons)
        end subroutine
        subroutine LoadDFTScalarFields(self)
                class(DFT), intent(inout) :: self
                call self%read('num_k_all', self%num_k_all)
                call self%read('num_k_irr', self%num_k_irr)
                call self%read('num_si', self%num_si)
                call self%read('nrel', self%nrel)
                call self%read('is_sharded', self%is_sharded)
                call self%read('shard_offset', self%shard_offset)
                call self%read('num_k', self%num_k)
                call self%read('num_bands', self%num_bands)
                call self%read('num_valence_electrons', self%num_valence_electrons)
        end subroutine
        subroutine FinalizeDFT(self)
               type(DFT), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearDFT(self)
                class(DFT), intent(inout) :: self
                type(DFT), save :: empty
                self = empty
        end subroutine
        pure elemental function IsDFTEqual(lhs, rhs) result(iseq)
                class(DFT), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (DFT)
                       iseq = iseq .and. (lhs%num_k_all == rhs%num_k_all)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_irr == rhs%num_k_irr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrel == rhs%nrel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sg == rhs%sg)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%st == rhs%st)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%is_sharded .eqv. rhs%is_sharded)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%shard_offset == rhs%shard_offset)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%k_irr == rhs%k_irr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ig == rhs%ig)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_bands == rhs%num_bands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_valence_electrons == rhs%num_valence_electrons)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorDFT(lhs, rhs)
                class(DFT), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (DFT)
                       lhs%num_k_all = rhs%num_k_all
                       lhs%num_k_irr = rhs%num_k_irr
                       lhs%num_si = rhs%num_si
                       lhs%nrel = rhs%nrel
                       lhs%sg = rhs%sg
                       lhs%st = rhs%st
                       lhs%is_sharded = rhs%is_sharded
                       lhs%shard_offset = rhs%shard_offset
                       lhs%num_k = rhs%num_k
                       lhs%k_irr = rhs%k_irr
                       lhs%ig = rhs%ig
                       lhs%num_bands = rhs%num_bands
                       lhs%num_valence_electrons = rhs%num_valence_electrons
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetDftK_irrExtents(self) result(res)
                class(DFT), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_k_all)]
        end function
        function GetDftIgExtents(self) result(res)
                class(DFT), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_k_all)]
        end function
        subroutine AllocateDFTObjectFields(self)
                class(DFT), intent(inout) :: self
                call self%k_irr%init(int(self%num_k_all))
                call self%ig%init(int(self%num_k_all))
        end subroutine


        subroutine AdjustLAPWBasisWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_interface_mp_adjustlapwbasiswrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(AdjustLAPWBasisI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine DFTPlusSigmaOneIterationWrapper(ret, imp_fp, selfpath, sigma_location, checkpoint, search_mu, update_lapw, admix, save_potentials, mu_search_variant) bind(C,name='flapwmbpt_interface_mp_dftplussigmaoneiterationwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                type(c_ptr),intent(in),value        :: sigma_location
                integer(kind=c_int),intent(in),value :: checkpoint
                integer(kind=c_int),intent(in),value :: search_mu
                integer(kind=c_int),intent(in),value :: update_lapw
                real(kind=c_float),intent(in),value :: admix
                integer(kind=c_int),intent(in),value :: save_potentials
                type(c_ptr),intent(in),value        :: mu_search_variant
            procedure(DFTPlusSigmaOneIterationI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), FromCStr(sigma_location), checkpoint, search_mu, update_lapw, admix, save_potentials, FromCStr(mu_search_variant))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine ConnectFlapwMBPTWrapper(ret, imp_fp, selfpath, inpfile, num_workers, opts) bind(C,name='flapwmbpt_interface_mp_connectflapwmbptwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                type(c_ptr),intent(in),value        :: inpfile
                integer(kind=c_int),intent(in),value :: num_workers
                type(c_ptr),intent(in),value        :: opts
            procedure(ConnectFlapwMBPTI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), FromCStr(inpfile), num_workers, FromCStr(opts))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine ReloadWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_interface_mp_reloadwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(ReloadI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine GetDFTInfoWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_interface_mp_getdftinfowrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(GetDFTInfoI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine GetLAPWBasisWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_interface_mp_getlapwbasiswrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(GetLAPWBasisI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine GetBasisMTInfoWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_interface_mp_getbasismtinfowrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(GetBasisMTInfoI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine GetPartialBandStructureWrapper(ret, imp_fp, selfpath, min_band, max_band, k) bind(C,name='flapwmbpt_interface_mp_getpartialbandstructurewrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
                integer(kind=c_int),intent(in),value :: k
            procedure(GetPartialBandStructureI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), min_band, max_band, k)
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine GetPartialBandStructureAndGradientWrapper(ret, imp_fp, selfpath, gradpath, min_band, max_band, k) bind(C,name='flapwmbpt_interface_mp_getpartialbandstructureandgradientwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                type(c_ptr),intent(in),value        :: gradpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
                integer(kind=c_int),intent(in),value :: k
            procedure(GetPartialBandStructureAndGradientI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), FromCStr(gradpath), min_band, max_band, k)
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine GetKpathBandsWrapper(ret, imp_fp, selfpath, kpathpath, min_band, max_band) bind(C,name='flapwmbpt_interface_mp_getkpathbandswrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                type(c_ptr),intent(in),value        :: kpathpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
            procedure(GetKpathBandsI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), FromCStr(kpathpath), min_band, max_band)
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine GetKPathBandsAndGradientWrapper(ret, imp_fp, selfpath, gradpath, kpathpath, min_band, max_band) bind(C,name='flapwmbpt_interface_mp_getkpathbandsandgradientwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                type(c_ptr),intent(in),value        :: gradpath
                type(c_ptr),intent(in),value        :: kpathpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
            procedure(GetKPathBandsAndGradientI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), FromCStr(gradpath), FromCStr(kpathpath), min_band, max_band)
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine GetEquivalentAtomSymmetriesWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_interface_mp_getequivalentatomsymmetrieswrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(GetEquivalentAtomSymmetriesI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine CreatePlotsWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_interface_mp_createplotswrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(CreatePlotsI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine GetConvergenceWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_interface_mp_getconvergencewrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(GetConvergenceI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine DisconnectWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_interface_mp_disconnectwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(DisconnectI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine


end module
