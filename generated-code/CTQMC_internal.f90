
module CTQMC_internal
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: ComplexArray
        integer(kind=int32)  ::  num =  0

        type(vector_real)  ::  imag
        real(kind=dp),pointer :: imag_(:)
        type(vector_real)  ::  real
        real(kind=dp),pointer :: real_(:)


        contains
        procedure :: AllocateObjectFields => AllocateComplexArrayObjectFields
        procedure :: ResetSectionFields   => ResetComplexArraySectionFields
        procedure :: StoreScalarFields    => StoreComplexArrayScalarFields
        procedure :: StoreObjectFields    => StoreComplexArrayObjectFields
        procedure :: LoadScalarFields     => LoadComplexArrayScalarFields
        procedure :: LoadObjectFields     => LoadComplexArrayObjectFields
        procedure :: DisconnectObjectFields => DisconnectComplexArrayObjectFields
        procedure :: IsEqual              => IsComplexArrayEqual
        procedure :: AssignmentOperator   => AssignmentOperatorComplexArray
        procedure :: clear                => ClearComplexArray
        procedure :: init => InitComplexArray
#ifndef __GFORTRAN__
        final     :: FinalizeComplexArray
#endif
        procedure :: GetComplexarrayImagExtents
        procedure :: GetComplexarrayRealExtents

    end type

    type, extends(ComplexArray) :: Hyb
        real(kind=dp)  ::  beta =  0.0d0



        contains
        procedure :: AllocateObjectFields => AllocateHybObjectFields
        procedure :: ResetSectionFields   => ResetHybSectionFields
        procedure :: StoreScalarFields    => StoreHybScalarFields
        procedure :: StoreObjectFields    => StoreHybObjectFields
        procedure :: LoadScalarFields     => LoadHybScalarFields
        procedure :: LoadObjectFields     => LoadHybObjectFields
        procedure :: DisconnectObjectFields => DisconnectHybObjectFields
        procedure :: IsEqual              => IsHybEqual
        procedure :: AssignmentOperator   => AssignmentOperatorHyb
        procedure :: clear                => ClearHyb
        procedure :: init => InitHyb
#ifndef __GFORTRAN__
        final     :: FinalizeHyb
#endif

    end type

    type, extends(persistent) :: ComplexSquareMatrix
        integer(kind=int32)  ::  dim =  0

        type(matrix_real)  ::  imag
        real(kind=dp),pointer :: imag_(:,:)
        type(matrix_real)  ::  real
        real(kind=dp),pointer :: real_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateComplexSquareMatrixObjectFields
        procedure :: ResetSectionFields   => ResetComplexSquareMatrixSectionFields
        procedure :: StoreScalarFields    => StoreComplexSquareMatrixScalarFields
        procedure :: StoreObjectFields    => StoreComplexSquareMatrixObjectFields
        procedure :: LoadScalarFields     => LoadComplexSquareMatrixScalarFields
        procedure :: LoadObjectFields     => LoadComplexSquareMatrixObjectFields
        procedure :: DisconnectObjectFields => DisconnectComplexSquareMatrixObjectFields
        procedure :: IsEqual              => IsComplexSquareMatrixEqual
        procedure :: AssignmentOperator   => AssignmentOperatorComplexSquareMatrix
        procedure :: clear                => ClearComplexSquareMatrix
        procedure :: init => InitComplexSquareMatrix
#ifndef __GFORTRAN__
        final     :: FinalizeComplexSquareMatrix
#endif
        procedure :: GetComplexsquarematrixImagExtents
        procedure :: GetComplexsquarematrixRealExtents

    end type

    type, extends(persistent) :: BasisStruct
        type(string)  ::  orbitals
        type(ComplexSquareMatrix)  ::  transformation
        type(string)  ::  type


        contains
        procedure :: AllocateObjectFields => AllocateBasisStructObjectFields
        procedure :: ResetSectionFields   => ResetBasisStructSectionFields
        procedure :: StoreScalarFields    => StoreBasisStructScalarFields
        procedure :: StoreObjectFields    => StoreBasisStructObjectFields
        procedure :: LoadScalarFields     => LoadBasisStructScalarFields
        procedure :: LoadObjectFields     => LoadBasisStructObjectFields
        procedure :: DisconnectObjectFields => DisconnectBasisStructObjectFields
        procedure :: IsEqual              => IsBasisStructEqual
        procedure :: AssignmentOperator   => AssignmentOperatorBasisStruct
        procedure :: clear                => ClearBasisStruct
        procedure :: init => InitBasisStruct
#ifndef __GFORTRAN__
        final     :: FinalizeBasisStruct
#endif

    end type

    type, extends(persistent) :: QuantumNumbersPlaceHolder


        contains
        procedure :: AllocateObjectFields => AllocateQuantumNumbersPlaceHolderObjectFields
        procedure :: ResetSectionFields   => ResetQuantumNumbersPlaceHolderSectionFields
        procedure :: StoreScalarFields    => StoreQuantumNumbersPlaceHolderScalarFields
        procedure :: StoreObjectFields    => StoreQuantumNumbersPlaceHolderObjectFields
        procedure :: LoadScalarFields     => LoadQuantumNumbersPlaceHolderScalarFields
        procedure :: LoadObjectFields     => LoadQuantumNumbersPlaceHolderObjectFields
        procedure :: DisconnectObjectFields => DisconnectQuantumNumbersPlaceHolderObjectFields
        procedure :: IsEqual              => IsQuantumNumbersPlaceHolderEqual
        procedure :: AssignmentOperator   => AssignmentOperatorQuantumNumbersPlaceHolder
        procedure :: clear                => ClearQuantumNumbersPlaceHolder
        procedure :: init => InitQuantumNumbersPlaceHolder
#ifndef __GFORTRAN__
        final     :: FinalizeQuantumNumbersPlaceHolder
#endif

    end type

    type, extends(persistent) :: CTQMCObservable
        type(ComplexSquareMatrix)  ::  one_body


        contains
        procedure :: AllocateObjectFields => AllocateCTQMCObservableObjectFields
        procedure :: ResetSectionFields   => ResetCTQMCObservableSectionFields
        procedure :: StoreScalarFields    => StoreCTQMCObservableScalarFields
        procedure :: StoreObjectFields    => StoreCTQMCObservableObjectFields
        procedure :: LoadScalarFields     => LoadCTQMCObservableScalarFields
        procedure :: LoadObjectFields     => LoadCTQMCObservableObjectFields
        procedure :: DisconnectObjectFields => DisconnectCTQMCObservableObjectFields
        procedure :: IsEqual              => IsCTQMCObservableEqual
        procedure :: AssignmentOperator   => AssignmentOperatorCTQMCObservable
        procedure :: clear                => ClearCTQMCObservable
        procedure :: init => InitCTQMCObservable
#ifndef __GFORTRAN__
        final     :: FinalizeCTQMCObservable
#endif

    end type

    type, extends(persistent) :: ObservablesPlaceHolder


        contains
        procedure :: AllocateObjectFields => AllocateObservablesPlaceHolderObjectFields
        procedure :: ResetSectionFields   => ResetObservablesPlaceHolderSectionFields
        procedure :: StoreScalarFields    => StoreObservablesPlaceHolderScalarFields
        procedure :: StoreObjectFields    => StoreObservablesPlaceHolderObjectFields
        procedure :: LoadScalarFields     => LoadObservablesPlaceHolderScalarFields
        procedure :: LoadObjectFields     => LoadObservablesPlaceHolderObjectFields
        procedure :: DisconnectObjectFields => DisconnectObservablesPlaceHolderObjectFields
        procedure :: IsEqual              => IsObservablesPlaceHolderEqual
        procedure :: AssignmentOperator   => AssignmentOperatorObservablesPlaceHolder
        procedure :: clear                => ClearObservablesPlaceHolder
        procedure :: init => InitObservablesPlaceHolder
#ifndef __GFORTRAN__
        final     :: FinalizeObservablesPlaceHolder
#endif

    end type

    type, extends(persistent) :: TwoBodyStruct
        real(kind=dp)  ::  F0 =  0.0d0

        real(kind=dp)  ::  F2 =  0.0d0

        real(kind=dp)  ::  F4 =  0.0d0

        real(kind=dp)  ::  F6 =  0.0d0

        type(string)  ::  approximation
        type(string)  ::  parametrisation


        contains
        procedure :: AllocateObjectFields => AllocateTwoBodyStructObjectFields
        procedure :: ResetSectionFields   => ResetTwoBodyStructSectionFields
        procedure :: StoreScalarFields    => StoreTwoBodyStructScalarFields
        procedure :: StoreObjectFields    => StoreTwoBodyStructObjectFields
        procedure :: LoadScalarFields     => LoadTwoBodyStructScalarFields
        procedure :: LoadObjectFields     => LoadTwoBodyStructObjectFields
        procedure :: DisconnectObjectFields => DisconnectTwoBodyStructObjectFields
        procedure :: IsEqual              => IsTwoBodyStructEqual
        procedure :: AssignmentOperator   => AssignmentOperatorTwoBodyStruct
        procedure :: clear                => ClearTwoBodyStruct
        procedure :: init => InitTwoBodyStruct
#ifndef __GFORTRAN__
        final     :: FinalizeTwoBodyStruct
#endif

    end type

    type, extends(persistent) :: HlocStruct
        type(ComplexSquareMatrix)  ::  one_body
        type(TwoBodyStruct)  ::  two_body


        contains
        procedure :: AllocateObjectFields => AllocateHlocStructObjectFields
        procedure :: ResetSectionFields   => ResetHlocStructSectionFields
        procedure :: StoreScalarFields    => StoreHlocStructScalarFields
        procedure :: StoreObjectFields    => StoreHlocStructObjectFields
        procedure :: LoadScalarFields     => LoadHlocStructScalarFields
        procedure :: LoadObjectFields     => LoadHlocStructObjectFields
        procedure :: DisconnectObjectFields => DisconnectHlocStructObjectFields
        procedure :: IsEqual              => IsHlocStructEqual
        procedure :: AssignmentOperator   => AssignmentOperatorHlocStruct
        procedure :: clear                => ClearHlocStruct
        procedure :: init => InitHlocStruct
#ifndef __GFORTRAN__
        final     :: FinalizeHlocStruct
#endif

    end type

    type, extends(persistent) :: HybridisationStruct
        integer(kind=int32)  ::  dim =  0

        type(string)  ::  functions
        type(matrix_int)  ::  matrix
        integer(kind=int32),pointer :: matrix_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateHybridisationStructObjectFields
        procedure :: ResetSectionFields   => ResetHybridisationStructSectionFields
        procedure :: StoreScalarFields    => StoreHybridisationStructScalarFields
        procedure :: StoreObjectFields    => StoreHybridisationStructObjectFields
        procedure :: LoadScalarFields     => LoadHybridisationStructScalarFields
        procedure :: LoadObjectFields     => LoadHybridisationStructObjectFields
        procedure :: DisconnectObjectFields => DisconnectHybridisationStructObjectFields
        procedure :: IsEqual              => IsHybridisationStructEqual
        procedure :: AssignmentOperator   => AssignmentOperatorHybridisationStruct
        procedure :: clear                => ClearHybridisationStruct
        procedure :: init => InitHybridisationStruct
#ifndef __GFORTRAN__
        final     :: FinalizeHybridisationStruct
#endif
        procedure :: GetHybridisationstructMatrixExtents

    end type

    type, extends(persistent) :: WormStruct
        type(string)  ::  basis
        type(string)  ::  meas
        logical  ::  diagonal = .False.



        contains
        procedure :: AllocateObjectFields => AllocateWormStructObjectFields
        procedure :: ResetSectionFields   => ResetWormStructSectionFields
        procedure :: StoreScalarFields    => StoreWormStructScalarFields
        procedure :: StoreObjectFields    => StoreWormStructObjectFields
        procedure :: LoadScalarFields     => LoadWormStructScalarFields
        procedure :: LoadObjectFields     => LoadWormStructObjectFields
        procedure :: DisconnectObjectFields => DisconnectWormStructObjectFields
        procedure :: IsEqual              => IsWormStructEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWormStruct
        procedure :: clear                => ClearWormStruct
        procedure :: init => InitWormStruct
#ifndef __GFORTRAN__
        final     :: FinalizeWormStruct
#endif

    end type

    type, extends(WormStruct) :: OneTimeWormStruct
        integer(kind=int32)  ::  cutoff =  int(50,kind=int32)



        contains
        procedure :: AllocateObjectFields => AllocateOneTimeWormStructObjectFields
        procedure :: ResetSectionFields   => ResetOneTimeWormStructSectionFields
        procedure :: StoreScalarFields    => StoreOneTimeWormStructScalarFields
        procedure :: StoreObjectFields    => StoreOneTimeWormStructObjectFields
        procedure :: LoadScalarFields     => LoadOneTimeWormStructScalarFields
        procedure :: LoadObjectFields     => LoadOneTimeWormStructObjectFields
        procedure :: DisconnectObjectFields => DisconnectOneTimeWormStructObjectFields
        procedure :: IsEqual              => IsOneTimeWormStructEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOneTimeWormStruct
        procedure :: clear                => ClearOneTimeWormStruct
        procedure :: init => InitOneTimeWormStruct
#ifndef __GFORTRAN__
        final     :: FinalizeOneTimeWormStruct
#endif

    end type

    type, extends(WormStruct) :: MultTimeWormStruct
        integer(kind=int32)  ::  fermion_cutoff =  int(50,kind=int32)

        integer(kind=int32)  ::  boson_cutoff =  int(10,kind=int32)



        contains
        procedure :: AllocateObjectFields => AllocateMultTimeWormStructObjectFields
        procedure :: ResetSectionFields   => ResetMultTimeWormStructSectionFields
        procedure :: StoreScalarFields    => StoreMultTimeWormStructScalarFields
        procedure :: StoreObjectFields    => StoreMultTimeWormStructObjectFields
        procedure :: LoadScalarFields     => LoadMultTimeWormStructScalarFields
        procedure :: LoadObjectFields     => LoadMultTimeWormStructObjectFields
        procedure :: DisconnectObjectFields => DisconnectMultTimeWormStructObjectFields
        procedure :: IsEqual              => IsMultTimeWormStructEqual
        procedure :: AssignmentOperator   => AssignmentOperatorMultTimeWormStruct
        procedure :: clear                => ClearMultTimeWormStruct
        procedure :: init => InitMultTimeWormStruct
#ifndef __GFORTRAN__
        final     :: FinalizeMultTimeWormStruct
#endif

    end type

    type, extends(persistent) :: AnalyticalContinuation
        integer(kind=int32)  ::  ntau =  int(400,kind=int32)

        integer(kind=int32)  ::  nf =  int(1000,kind=int32)



        contains
        procedure :: AllocateObjectFields => AllocateAnalyticalContinuationObjectFields
        procedure :: ResetSectionFields   => ResetAnalyticalContinuationSectionFields
        procedure :: StoreScalarFields    => StoreAnalyticalContinuationScalarFields
        procedure :: StoreObjectFields    => StoreAnalyticalContinuationObjectFields
        procedure :: LoadScalarFields     => LoadAnalyticalContinuationScalarFields
        procedure :: LoadObjectFields     => LoadAnalyticalContinuationObjectFields
        procedure :: DisconnectObjectFields => DisconnectAnalyticalContinuationObjectFields
        procedure :: IsEqual              => IsAnalyticalContinuationEqual
        procedure :: AssignmentOperator   => AssignmentOperatorAnalyticalContinuation
        procedure :: clear                => ClearAnalyticalContinuation
        procedure :: init => InitAnalyticalContinuation
#ifndef __GFORTRAN__
        final     :: FinalizeAnalyticalContinuation
#endif

    end type

    type, extends(persistent) :: PartitionSpaceStruct
        real(kind=dp)  ::  green_matsubara_cutoff =  0.0d0

        type(QuantumNumbersPlaceHolder)  ::  quantum_numbers
        type(ObservablesPlaceHolder)  ::  observables
        integer(kind=int32)  ::  susceptibility_cutoff =  int(10,kind=int32)

        integer(kind=int32)  ::  susceptibility_tail =  int(100,kind=int32)

        logical  ::  quantum_number_susceptibility = .False.

        logical  ::  occupation_susceptibility = .False.

        logical  ::  occupation_susceptibility_bulla = .False.

        logical  ::  occupation_susceptibility_direct = .False.

        logical  ::  green_bulla = .False.

        logical  ::  density_matrix_precise = .False.

        integer(kind=int32)  ::  sweepA =  int(50,kind=int32)

        integer(kind=int32)  ::  storeA =  int(100,kind=int32)

        integer(kind=int32)  ::  sweepB =  int(250,kind=int32)

        integer(kind=int32)  ::  storeB =  int(20,kind=int32)

        type(string)  ::  green_basis
        type(string)  ::  probabilities


        contains
        procedure :: AllocateObjectFields => AllocatePartitionSpaceStructObjectFields
        procedure :: ResetSectionFields   => ResetPartitionSpaceStructSectionFields
        procedure :: StoreScalarFields    => StorePartitionSpaceStructScalarFields
        procedure :: StoreObjectFields    => StorePartitionSpaceStructObjectFields
        procedure :: LoadScalarFields     => LoadPartitionSpaceStructScalarFields
        procedure :: LoadObjectFields     => LoadPartitionSpaceStructObjectFields
        procedure :: DisconnectObjectFields => DisconnectPartitionSpaceStructObjectFields
        procedure :: IsEqual              => IsPartitionSpaceStructEqual
        procedure :: AssignmentOperator   => AssignmentOperatorPartitionSpaceStruct
        procedure :: clear                => ClearPartitionSpaceStruct
        procedure :: init => InitPartitionSpaceStruct
#ifndef __GFORTRAN__
        final     :: FinalizePartitionSpaceStruct
#endif

    end type

    type, extends(persistent) :: Parameters
        real(kind=dp)  ::  beta =  0.0d0

        real(kind=dp)  ::  mu =  0.0d0

        logical  ::  complex = .False.

        type(BasisStruct)  ::  basis
        type(HlocStruct)  ::  hloc
        type(HybridisationStruct)  ::  hybridisation
        type(PartitionSpaceStruct)  ::  partition
        type(AnalyticalContinuation)  ::  analytical_continuation
        type(OneTimeWormStruct)  ::  green
        type(OneTimeWormStruct)  ::  susc_ph
        type(OneTimeWormStruct)  ::  susc_pp
        type(MultTimeWormStruct)  ::  hedin_ph
        type(MultTimeWormStruct)  ::  hedin_pp
        type(MultTimeWormStruct)  ::  vertex
        integer(kind=int32)  ::  measurement_steps =  int(0,kind=int32)

        integer(kind=int32)  ::  thermalisation_steps =  int(0,kind=int32)

        integer(kind=int32)  ::  measurement_time =  int(20,kind=int32)

        integer(kind=int32)  ::  thermalisation_time =  int(5,kind=int32)

        integer(kind=int32)  ::  sim_per_device =  int(25,kind=int32)

        logical  ::  quad_insert = .False.

        logical  ::  all_errors = .False.

        type(string)  ::  error
        integer(kind=int32)  ::  trunc_dim =  0

        integer(kind=int32)  ::  interaction_truncation =  0



        contains
        procedure :: AllocateObjectFields => AllocateParametersObjectFields
        procedure :: ResetSectionFields   => ResetParametersSectionFields
        procedure :: StoreScalarFields    => StoreParametersScalarFields
        procedure :: StoreObjectFields    => StoreParametersObjectFields
        procedure :: LoadScalarFields     => LoadParametersScalarFields
        procedure :: LoadObjectFields     => LoadParametersObjectFields
        procedure :: DisconnectObjectFields => DisconnectParametersObjectFields
        procedure :: IsEqual              => IsParametersEqual
        procedure :: AssignmentOperator   => AssignmentOperatorParameters
        procedure :: clear                => ClearParameters
        procedure :: init => InitParameters
#ifndef __GFORTRAN__
        final     :: FinalizeParameters
#endif

    end type

    contains
        subroutine InitComplexArray(self)
                class(ComplexArray), intent(inout) :: self
                call self%InitPersistent()
                self%num =  0
        end subroutine
        subroutine StoreComplexArrayObjectFields(self)
                class(ComplexArray), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%imag%StoreObject(ps, gid,  'imag')
                call self%real%StoreObject(ps, gid,  'real')
        end subroutine
        subroutine LoadComplexArrayObjectFields(self)
                class(ComplexArray), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%imag%LoadObject(ps, gid,  'imag')
                call self%real%LoadObject(ps, gid,  'real')
        end subroutine
        subroutine ResetComplexArraySectionFields(self)
                class(ComplexArray), intent(inout) :: self
                self%imag_ => self%imag%GetWithExtents(self%GetComplexArrayimagExtents())
                self%real_ => self%real%GetWithExtents(self%GetComplexArrayrealExtents())
        end subroutine
        subroutine DisconnectComplexArrayObjectFields(self)
                class(ComplexArray), intent(inout) :: self
               type(iterator) :: iter
                call self%imag%DisconnectFromStore()
                call self%real%DisconnectFromStore()
        end subroutine
        subroutine StoreComplexArrayScalarFields(self)
                class(ComplexArray), intent(inout) :: self
                call self%write('num', self%num)
        end subroutine
        subroutine LoadComplexArrayScalarFields(self)
                class(ComplexArray), intent(inout) :: self
                call self%read('num', self%num)
        end subroutine
        subroutine FinalizeComplexArray(self)
               type(ComplexArray), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearComplexArray(self)
                class(ComplexArray), intent(inout) :: self
                type(ComplexArray), save :: empty
                self = empty
        end subroutine
        pure elemental function IsComplexArrayEqual(lhs, rhs) result(iseq)
                class(ComplexArray), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ComplexArray)
                       iseq = iseq .and. (lhs%num == rhs%num)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%imag == rhs%imag)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%real == rhs%real)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorComplexArray(lhs, rhs)
                class(ComplexArray), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ComplexArray)
                       lhs%num = rhs%num
                       lhs%imag = rhs%imag
                       lhs%real = rhs%real
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetComplexarrayImagExtents(self) result(res)
                class(ComplexArray), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num)]
        end function
        function GetComplexarrayRealExtents(self) result(res)
                class(ComplexArray), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num)]
        end function
        subroutine AllocateComplexArrayObjectFields(self)
                class(ComplexArray), intent(inout) :: self
                call self%imag%init(int(self%num))
                call self%real%init(int(self%num))
        end subroutine


        subroutine InitHyb(self)
                class(Hyb), intent(inout) :: self
                call self%ComplexArray%Init()
                self%beta =  0.0d0
        end subroutine
        subroutine StoreHybObjectFields(self)
                class(Hyb), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ComplexArray%StoreObjectFields()
        end subroutine
        subroutine LoadHybObjectFields(self)
                class(Hyb), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ComplexArray%LoadObjectFields()
        end subroutine
        subroutine ResetHybSectionFields(self)
                class(Hyb), intent(inout) :: self
                call self%ComplexArray%ResetSectionFields()
        end subroutine
        subroutine DisconnectHybObjectFields(self)
                class(Hyb), intent(inout) :: self
               type(iterator) :: iter
                call self%ComplexArray%DisconnectObjectFields()
        end subroutine
        subroutine StoreHybScalarFields(self)
                class(Hyb), intent(inout) :: self
                call self%ComplexArray%StoreScalarFields()
                call self%write('beta', self%beta)
        end subroutine
        subroutine LoadHybScalarFields(self)
                class(Hyb), intent(inout) :: self
                call self%ComplexArray%LoadScalarFields()
                call self%read('beta', self%beta)
        end subroutine
        subroutine FinalizeHyb(self)
               type(Hyb), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearHyb(self)
                class(Hyb), intent(inout) :: self
                type(Hyb), save :: empty
                self = empty
        end subroutine
        pure elemental function IsHybEqual(lhs, rhs) result(iseq)
                class(Hyb), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Hyb)
                       iseq = iseq .and. (lhs%ComplexArray == rhs%ComplexArray)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%beta == rhs%beta)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorHyb(lhs, rhs)
                class(Hyb), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Hyb)
                       lhs%ComplexArray = rhs%ComplexArray
                       lhs%beta = rhs%beta
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateHybObjectFields(self)
                class(Hyb), intent(inout) :: self
                call self%ComplexArray%AllocateObjectFields()
        end subroutine


        subroutine InitComplexSquareMatrix(self)
                class(ComplexSquareMatrix), intent(inout) :: self
                call self%InitPersistent()
                self%dim =  0
        end subroutine
        subroutine StoreComplexSquareMatrixObjectFields(self)
                class(ComplexSquareMatrix), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%imag%StoreObject(ps, gid,  'imag')
                call self%real%StoreObject(ps, gid,  'real')
        end subroutine
        subroutine LoadComplexSquareMatrixObjectFields(self)
                class(ComplexSquareMatrix), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%imag%LoadObject(ps, gid,  'imag')
                call self%real%LoadObject(ps, gid,  'real')
        end subroutine
        subroutine ResetComplexSquareMatrixSectionFields(self)
                class(ComplexSquareMatrix), intent(inout) :: self
                self%imag_ => self%imag%GetWithExtents(self%GetComplexSquareMatriximagExtents())
                self%real_ => self%real%GetWithExtents(self%GetComplexSquareMatrixrealExtents())
        end subroutine
        subroutine DisconnectComplexSquareMatrixObjectFields(self)
                class(ComplexSquareMatrix), intent(inout) :: self
               type(iterator) :: iter
                call self%imag%DisconnectFromStore()
                call self%real%DisconnectFromStore()
        end subroutine
        subroutine StoreComplexSquareMatrixScalarFields(self)
                class(ComplexSquareMatrix), intent(inout) :: self
                call self%write('dim', self%dim)
        end subroutine
        subroutine LoadComplexSquareMatrixScalarFields(self)
                class(ComplexSquareMatrix), intent(inout) :: self
                call self%read('dim', self%dim)
        end subroutine
        subroutine FinalizeComplexSquareMatrix(self)
               type(ComplexSquareMatrix), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearComplexSquareMatrix(self)
                class(ComplexSquareMatrix), intent(inout) :: self
                type(ComplexSquareMatrix), save :: empty
                self = empty
        end subroutine
        pure elemental function IsComplexSquareMatrixEqual(lhs, rhs) result(iseq)
                class(ComplexSquareMatrix), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ComplexSquareMatrix)
                       iseq = iseq .and. (lhs%dim == rhs%dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%imag == rhs%imag)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%real == rhs%real)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorComplexSquareMatrix(lhs, rhs)
                class(ComplexSquareMatrix), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ComplexSquareMatrix)
                       lhs%dim = rhs%dim
                       lhs%imag = rhs%imag
                       lhs%real = rhs%real
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetComplexsquarematrixImagExtents(self) result(res)
                class(ComplexSquareMatrix), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        function GetComplexsquarematrixRealExtents(self) result(res)
                class(ComplexSquareMatrix), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        subroutine AllocateComplexSquareMatrixObjectFields(self)
                class(ComplexSquareMatrix), intent(inout) :: self
                call self%imag%init(int(self%dim),int(self%dim))
                call self%real%init(int(self%dim),int(self%dim))
        end subroutine


        subroutine InitBasisStruct(self)
                class(BasisStruct), intent(inout) :: self
                call self%InitPersistent()
                call self%transformation%InitPersistent()
                self%type = "product"
        end subroutine
        subroutine StoreBasisStructObjectFields(self)
                class(BasisStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%transformation%store(ps%FetchSubGroup(gid,  'transformation'))
        end subroutine
        subroutine LoadBasisStructObjectFields(self)
                class(BasisStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%transformation%load(ps%FetchSubGroup(gid,  'transformation'))
        end subroutine
        subroutine ResetBasisStructSectionFields(self)
                class(BasisStruct), intent(inout) :: self
        end subroutine
        subroutine DisconnectBasisStructObjectFields(self)
                class(BasisStruct), intent(inout) :: self
               type(iterator) :: iter
                call self%transformation%disconnect()
        end subroutine
        subroutine StoreBasisStructScalarFields(self)
                class(BasisStruct), intent(inout) :: self
                call self%write('orbitals', self%orbitals)
                call self%write('type', self%type)
        end subroutine
        subroutine LoadBasisStructScalarFields(self)
                class(BasisStruct), intent(inout) :: self
                call self%read('orbitals', self%orbitals)
                call self%read('type', self%type)
        end subroutine
        subroutine FinalizeBasisStruct(self)
               type(BasisStruct), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearBasisStruct(self)
                class(BasisStruct), intent(inout) :: self
                type(BasisStruct), save :: empty
                self = empty
        end subroutine
        pure elemental function IsBasisStructEqual(lhs, rhs) result(iseq)
                class(BasisStruct), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (BasisStruct)
                       iseq = iseq .and. (lhs%orbitals == rhs%orbitals)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%transformation == rhs%transformation)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%type == rhs%type)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorBasisStruct(lhs, rhs)
                class(BasisStruct), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (BasisStruct)
                       lhs%orbitals = rhs%orbitals
                       lhs%transformation = rhs%transformation
                       lhs%type = rhs%type
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateBasisStructObjectFields(self)
                class(BasisStruct), intent(inout) :: self
        end subroutine


        subroutine InitQuantumNumbersPlaceHolder(self)
                class(QuantumNumbersPlaceHolder), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreQuantumNumbersPlaceHolderObjectFields(self)
                class(QuantumNumbersPlaceHolder), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadQuantumNumbersPlaceHolderObjectFields(self)
                class(QuantumNumbersPlaceHolder), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetQuantumNumbersPlaceHolderSectionFields(self)
                class(QuantumNumbersPlaceHolder), intent(inout) :: self
        end subroutine
        subroutine DisconnectQuantumNumbersPlaceHolderObjectFields(self)
                class(QuantumNumbersPlaceHolder), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreQuantumNumbersPlaceHolderScalarFields(self)
                class(QuantumNumbersPlaceHolder), intent(inout) :: self
        end subroutine
        subroutine LoadQuantumNumbersPlaceHolderScalarFields(self)
                class(QuantumNumbersPlaceHolder), intent(inout) :: self
        end subroutine
        subroutine FinalizeQuantumNumbersPlaceHolder(self)
               type(QuantumNumbersPlaceHolder), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearQuantumNumbersPlaceHolder(self)
                class(QuantumNumbersPlaceHolder), intent(inout) :: self
                type(QuantumNumbersPlaceHolder), save :: empty
                self = empty
        end subroutine
        pure elemental function IsQuantumNumbersPlaceHolderEqual(lhs, rhs) result(iseq)
                class(QuantumNumbersPlaceHolder), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (QuantumNumbersPlaceHolder)
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorQuantumNumbersPlaceHolder(lhs, rhs)
                class(QuantumNumbersPlaceHolder), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (QuantumNumbersPlaceHolder)
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateQuantumNumbersPlaceHolderObjectFields(self)
                class(QuantumNumbersPlaceHolder), intent(inout) :: self
        end subroutine


        subroutine InitCTQMCObservable(self)
                class(CTQMCObservable), intent(inout) :: self
                call self%InitPersistent()
                call self%one_body%InitPersistent()
        end subroutine
        subroutine StoreCTQMCObservableObjectFields(self)
                class(CTQMCObservable), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%one_body%store(ps%FetchSubGroup(gid,  'one_body'))
        end subroutine
        subroutine LoadCTQMCObservableObjectFields(self)
                class(CTQMCObservable), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%one_body%load(ps%FetchSubGroup(gid,  'one_body'))
        end subroutine
        subroutine ResetCTQMCObservableSectionFields(self)
                class(CTQMCObservable), intent(inout) :: self
        end subroutine
        subroutine DisconnectCTQMCObservableObjectFields(self)
                class(CTQMCObservable), intent(inout) :: self
               type(iterator) :: iter
                call self%one_body%disconnect()
        end subroutine
        subroutine StoreCTQMCObservableScalarFields(self)
                class(CTQMCObservable), intent(inout) :: self
        end subroutine
        subroutine LoadCTQMCObservableScalarFields(self)
                class(CTQMCObservable), intent(inout) :: self
        end subroutine
        subroutine FinalizeCTQMCObservable(self)
               type(CTQMCObservable), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearCTQMCObservable(self)
                class(CTQMCObservable), intent(inout) :: self
                type(CTQMCObservable), save :: empty
                self = empty
        end subroutine
        pure elemental function IsCTQMCObservableEqual(lhs, rhs) result(iseq)
                class(CTQMCObservable), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (CTQMCObservable)
                       iseq = iseq .and. (lhs%one_body == rhs%one_body)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorCTQMCObservable(lhs, rhs)
                class(CTQMCObservable), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (CTQMCObservable)
                       lhs%one_body = rhs%one_body
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateCTQMCObservableObjectFields(self)
                class(CTQMCObservable), intent(inout) :: self
        end subroutine


        subroutine InitObservablesPlaceHolder(self)
                class(ObservablesPlaceHolder), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreObservablesPlaceHolderObjectFields(self)
                class(ObservablesPlaceHolder), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadObservablesPlaceHolderObjectFields(self)
                class(ObservablesPlaceHolder), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetObservablesPlaceHolderSectionFields(self)
                class(ObservablesPlaceHolder), intent(inout) :: self
        end subroutine
        subroutine DisconnectObservablesPlaceHolderObjectFields(self)
                class(ObservablesPlaceHolder), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreObservablesPlaceHolderScalarFields(self)
                class(ObservablesPlaceHolder), intent(inout) :: self
        end subroutine
        subroutine LoadObservablesPlaceHolderScalarFields(self)
                class(ObservablesPlaceHolder), intent(inout) :: self
        end subroutine
        subroutine FinalizeObservablesPlaceHolder(self)
               type(ObservablesPlaceHolder), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearObservablesPlaceHolder(self)
                class(ObservablesPlaceHolder), intent(inout) :: self
                type(ObservablesPlaceHolder), save :: empty
                self = empty
        end subroutine
        pure elemental function IsObservablesPlaceHolderEqual(lhs, rhs) result(iseq)
                class(ObservablesPlaceHolder), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ObservablesPlaceHolder)
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorObservablesPlaceHolder(lhs, rhs)
                class(ObservablesPlaceHolder), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ObservablesPlaceHolder)
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateObservablesPlaceHolderObjectFields(self)
                class(ObservablesPlaceHolder), intent(inout) :: self
        end subroutine


        subroutine InitTwoBodyStruct(self)
                class(TwoBodyStruct), intent(inout) :: self
                call self%InitPersistent()
                self%F0 =  0.0d0
                self%F2 =  0.0d0
                self%F4 =  0.0d0
                self%F6 =  0.0d0
                self%approximation = "none"
                self%parametrisation = "slater-condon"
        end subroutine
        subroutine StoreTwoBodyStructObjectFields(self)
                class(TwoBodyStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadTwoBodyStructObjectFields(self)
                class(TwoBodyStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetTwoBodyStructSectionFields(self)
                class(TwoBodyStruct), intent(inout) :: self
        end subroutine
        subroutine DisconnectTwoBodyStructObjectFields(self)
                class(TwoBodyStruct), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreTwoBodyStructScalarFields(self)
                class(TwoBodyStruct), intent(inout) :: self
                call self%write('F0', self%F0)
                call self%write('F2', self%F2)
                call self%write('F4', self%F4)
                call self%write('F6', self%F6)
                call self%write('approximation', self%approximation)
                call self%write('parametrisation', self%parametrisation)
        end subroutine
        subroutine LoadTwoBodyStructScalarFields(self)
                class(TwoBodyStruct), intent(inout) :: self
                call self%read('F0', self%F0)
                call self%read('F2', self%F2)
                call self%read('F4', self%F4)
                call self%read('F6', self%F6)
                call self%read('approximation', self%approximation)
                call self%read('parametrisation', self%parametrisation)
        end subroutine
        subroutine FinalizeTwoBodyStruct(self)
               type(TwoBodyStruct), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearTwoBodyStruct(self)
                class(TwoBodyStruct), intent(inout) :: self
                type(TwoBodyStruct), save :: empty
                self = empty
        end subroutine
        pure elemental function IsTwoBodyStructEqual(lhs, rhs) result(iseq)
                class(TwoBodyStruct), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (TwoBodyStruct)
                       iseq = iseq .and. (lhs%F0 == rhs%F0)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%F2 == rhs%F2)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%F4 == rhs%F4)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%F6 == rhs%F6)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%approximation == rhs%approximation)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%parametrisation == rhs%parametrisation)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorTwoBodyStruct(lhs, rhs)
                class(TwoBodyStruct), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (TwoBodyStruct)
                       lhs%F0 = rhs%F0
                       lhs%F2 = rhs%F2
                       lhs%F4 = rhs%F4
                       lhs%F6 = rhs%F6
                       lhs%approximation = rhs%approximation
                       lhs%parametrisation = rhs%parametrisation
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateTwoBodyStructObjectFields(self)
                class(TwoBodyStruct), intent(inout) :: self
        end subroutine


        subroutine InitHlocStruct(self)
                class(HlocStruct), intent(inout) :: self
                call self%InitPersistent()
                call self%one_body%InitPersistent()
                call self%two_body%InitPersistent()
        end subroutine
        subroutine StoreHlocStructObjectFields(self)
                class(HlocStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%one_body%store(ps%FetchSubGroup(gid,  'one_body'))
                call self%two_body%store(ps%FetchSubGroup(gid,  'two_body'))
        end subroutine
        subroutine LoadHlocStructObjectFields(self)
                class(HlocStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%one_body%load(ps%FetchSubGroup(gid,  'one_body'))
                call self%two_body%load(ps%FetchSubGroup(gid,  'two_body'))
        end subroutine
        subroutine ResetHlocStructSectionFields(self)
                class(HlocStruct), intent(inout) :: self
        end subroutine
        subroutine DisconnectHlocStructObjectFields(self)
                class(HlocStruct), intent(inout) :: self
               type(iterator) :: iter
                call self%one_body%disconnect()
                call self%two_body%disconnect()
        end subroutine
        subroutine StoreHlocStructScalarFields(self)
                class(HlocStruct), intent(inout) :: self
        end subroutine
        subroutine LoadHlocStructScalarFields(self)
                class(HlocStruct), intent(inout) :: self
        end subroutine
        subroutine FinalizeHlocStruct(self)
               type(HlocStruct), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearHlocStruct(self)
                class(HlocStruct), intent(inout) :: self
                type(HlocStruct), save :: empty
                self = empty
        end subroutine
        pure elemental function IsHlocStructEqual(lhs, rhs) result(iseq)
                class(HlocStruct), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (HlocStruct)
                       iseq = iseq .and. (lhs%one_body == rhs%one_body)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%two_body == rhs%two_body)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorHlocStruct(lhs, rhs)
                class(HlocStruct), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (HlocStruct)
                       lhs%one_body = rhs%one_body
                       lhs%two_body = rhs%two_body
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateHlocStructObjectFields(self)
                class(HlocStruct), intent(inout) :: self
        end subroutine


        subroutine InitHybridisationStruct(self)
                class(HybridisationStruct), intent(inout) :: self
                call self%InitPersistent()
                self%dim =  0
                self%functions = "Hyb.json"
        end subroutine
        subroutine StoreHybridisationStructObjectFields(self)
                class(HybridisationStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%matrix%StoreObject(ps, gid,  'matrix')
        end subroutine
        subroutine LoadHybridisationStructObjectFields(self)
                class(HybridisationStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%matrix%LoadObject(ps, gid,  'matrix')
        end subroutine
        subroutine ResetHybridisationStructSectionFields(self)
                class(HybridisationStruct), intent(inout) :: self
                self%matrix_ => self%matrix%GetWithExtents(self%GetHybridisationStructmatrixExtents())
        end subroutine
        subroutine DisconnectHybridisationStructObjectFields(self)
                class(HybridisationStruct), intent(inout) :: self
               type(iterator) :: iter
                call self%matrix%DisconnectFromStore()
        end subroutine
        subroutine StoreHybridisationStructScalarFields(self)
                class(HybridisationStruct), intent(inout) :: self
                call self%write('dim', self%dim)
                call self%write('functions', self%functions)
        end subroutine
        subroutine LoadHybridisationStructScalarFields(self)
                class(HybridisationStruct), intent(inout) :: self
                call self%read('dim', self%dim)
                call self%read('functions', self%functions)
        end subroutine
        subroutine FinalizeHybridisationStruct(self)
               type(HybridisationStruct), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearHybridisationStruct(self)
                class(HybridisationStruct), intent(inout) :: self
                type(HybridisationStruct), save :: empty
                self = empty
        end subroutine
        pure elemental function IsHybridisationStructEqual(lhs, rhs) result(iseq)
                class(HybridisationStruct), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (HybridisationStruct)
                       iseq = iseq .and. (lhs%dim == rhs%dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%functions == rhs%functions)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%matrix == rhs%matrix)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorHybridisationStruct(lhs, rhs)
                class(HybridisationStruct), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (HybridisationStruct)
                       lhs%dim = rhs%dim
                       lhs%functions = rhs%functions
                       lhs%matrix = rhs%matrix
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetHybridisationstructMatrixExtents(self) result(res)
                class(HybridisationStruct), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        subroutine AllocateHybridisationStructObjectFields(self)
                class(HybridisationStruct), intent(inout) :: self
                call self%matrix%init(int(self%dim),int(self%dim))
        end subroutine


        subroutine InitWormStruct(self)
                class(WormStruct), intent(inout) :: self
                call self%InitPersistent()
                self%basis = "matsubara"
                self%diagonal = .False.
        end subroutine
        subroutine StoreWormStructObjectFields(self)
                class(WormStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadWormStructObjectFields(self)
                class(WormStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetWormStructSectionFields(self)
                class(WormStruct), intent(inout) :: self
        end subroutine
        subroutine DisconnectWormStructObjectFields(self)
                class(WormStruct), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreWormStructScalarFields(self)
                class(WormStruct), intent(inout) :: self
                call self%write('basis', self%basis)
                call self%write('meas', self%meas)
                call self%write('diagonal', self%diagonal)
        end subroutine
        subroutine LoadWormStructScalarFields(self)
                class(WormStruct), intent(inout) :: self
                call self%read('basis', self%basis)
                call self%read('meas', self%meas)
                call self%read('diagonal', self%diagonal)
        end subroutine
        subroutine FinalizeWormStruct(self)
               type(WormStruct), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWormStruct(self)
                class(WormStruct), intent(inout) :: self
                type(WormStruct), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWormStructEqual(lhs, rhs) result(iseq)
                class(WormStruct), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (WormStruct)
                       iseq = iseq .and. (lhs%basis == rhs%basis)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%meas == rhs%meas)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%diagonal .eqv. rhs%diagonal)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWormStruct(lhs, rhs)
                class(WormStruct), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (WormStruct)
                       lhs%basis = rhs%basis
                       lhs%meas = rhs%meas
                       lhs%diagonal = rhs%diagonal
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateWormStructObjectFields(self)
                class(WormStruct), intent(inout) :: self
        end subroutine


        subroutine InitOneTimeWormStruct(self)
                class(OneTimeWormStruct), intent(inout) :: self
                call self%WormStruct%Init()
                self%cutoff =  int(50,kind=int32)
        end subroutine
        subroutine StoreOneTimeWormStructObjectFields(self)
                class(OneTimeWormStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%WormStruct%StoreObjectFields()
        end subroutine
        subroutine LoadOneTimeWormStructObjectFields(self)
                class(OneTimeWormStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%WormStruct%LoadObjectFields()
        end subroutine
        subroutine ResetOneTimeWormStructSectionFields(self)
                class(OneTimeWormStruct), intent(inout) :: self
                call self%WormStruct%ResetSectionFields()
        end subroutine
        subroutine DisconnectOneTimeWormStructObjectFields(self)
                class(OneTimeWormStruct), intent(inout) :: self
               type(iterator) :: iter
                call self%WormStruct%DisconnectObjectFields()
        end subroutine
        subroutine StoreOneTimeWormStructScalarFields(self)
                class(OneTimeWormStruct), intent(inout) :: self
                call self%WormStruct%StoreScalarFields()
                call self%write('cutoff', self%cutoff)
        end subroutine
        subroutine LoadOneTimeWormStructScalarFields(self)
                class(OneTimeWormStruct), intent(inout) :: self
                call self%WormStruct%LoadScalarFields()
                call self%read('cutoff', self%cutoff)
        end subroutine
        subroutine FinalizeOneTimeWormStruct(self)
               type(OneTimeWormStruct), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOneTimeWormStruct(self)
                class(OneTimeWormStruct), intent(inout) :: self
                type(OneTimeWormStruct), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOneTimeWormStructEqual(lhs, rhs) result(iseq)
                class(OneTimeWormStruct), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (OneTimeWormStruct)
                       iseq = iseq .and. (lhs%WormStruct == rhs%WormStruct)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%cutoff == rhs%cutoff)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOneTimeWormStruct(lhs, rhs)
                class(OneTimeWormStruct), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (OneTimeWormStruct)
                       lhs%WormStruct = rhs%WormStruct
                       lhs%cutoff = rhs%cutoff
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateOneTimeWormStructObjectFields(self)
                class(OneTimeWormStruct), intent(inout) :: self
                call self%WormStruct%AllocateObjectFields()
        end subroutine


        subroutine InitMultTimeWormStruct(self)
                class(MultTimeWormStruct), intent(inout) :: self
                call self%WormStruct%Init()
                self%fermion_cutoff =  int(50,kind=int32)
                self%boson_cutoff =  int(10,kind=int32)
        end subroutine
        subroutine StoreMultTimeWormStructObjectFields(self)
                class(MultTimeWormStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%WormStruct%StoreObjectFields()
        end subroutine
        subroutine LoadMultTimeWormStructObjectFields(self)
                class(MultTimeWormStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%WormStruct%LoadObjectFields()
        end subroutine
        subroutine ResetMultTimeWormStructSectionFields(self)
                class(MultTimeWormStruct), intent(inout) :: self
                call self%WormStruct%ResetSectionFields()
        end subroutine
        subroutine DisconnectMultTimeWormStructObjectFields(self)
                class(MultTimeWormStruct), intent(inout) :: self
               type(iterator) :: iter
                call self%WormStruct%DisconnectObjectFields()
        end subroutine
        subroutine StoreMultTimeWormStructScalarFields(self)
                class(MultTimeWormStruct), intent(inout) :: self
                call self%WormStruct%StoreScalarFields()
                call self%write('fermion_cutoff', self%fermion_cutoff)
                call self%write('boson_cutoff', self%boson_cutoff)
        end subroutine
        subroutine LoadMultTimeWormStructScalarFields(self)
                class(MultTimeWormStruct), intent(inout) :: self
                call self%WormStruct%LoadScalarFields()
                call self%read('fermion_cutoff', self%fermion_cutoff)
                call self%read('boson_cutoff', self%boson_cutoff)
        end subroutine
        subroutine FinalizeMultTimeWormStruct(self)
               type(MultTimeWormStruct), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearMultTimeWormStruct(self)
                class(MultTimeWormStruct), intent(inout) :: self
                type(MultTimeWormStruct), save :: empty
                self = empty
        end subroutine
        pure elemental function IsMultTimeWormStructEqual(lhs, rhs) result(iseq)
                class(MultTimeWormStruct), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (MultTimeWormStruct)
                       iseq = iseq .and. (lhs%WormStruct == rhs%WormStruct)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%fermion_cutoff == rhs%fermion_cutoff)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%boson_cutoff == rhs%boson_cutoff)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorMultTimeWormStruct(lhs, rhs)
                class(MultTimeWormStruct), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (MultTimeWormStruct)
                       lhs%WormStruct = rhs%WormStruct
                       lhs%fermion_cutoff = rhs%fermion_cutoff
                       lhs%boson_cutoff = rhs%boson_cutoff
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateMultTimeWormStructObjectFields(self)
                class(MultTimeWormStruct), intent(inout) :: self
                call self%WormStruct%AllocateObjectFields()
        end subroutine


        subroutine InitAnalyticalContinuation(self)
                class(AnalyticalContinuation), intent(inout) :: self
                call self%InitPersistent()
                self%ntau =  int(400,kind=int32)
                self%nf =  int(1000,kind=int32)
        end subroutine
        subroutine StoreAnalyticalContinuationObjectFields(self)
                class(AnalyticalContinuation), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadAnalyticalContinuationObjectFields(self)
                class(AnalyticalContinuation), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetAnalyticalContinuationSectionFields(self)
                class(AnalyticalContinuation), intent(inout) :: self
        end subroutine
        subroutine DisconnectAnalyticalContinuationObjectFields(self)
                class(AnalyticalContinuation), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreAnalyticalContinuationScalarFields(self)
                class(AnalyticalContinuation), intent(inout) :: self
                call self%write('ntau', self%ntau)
                call self%write('nf', self%nf)
        end subroutine
        subroutine LoadAnalyticalContinuationScalarFields(self)
                class(AnalyticalContinuation), intent(inout) :: self
                call self%read('ntau', self%ntau)
                call self%read('nf', self%nf)
        end subroutine
        subroutine FinalizeAnalyticalContinuation(self)
               type(AnalyticalContinuation), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearAnalyticalContinuation(self)
                class(AnalyticalContinuation), intent(inout) :: self
                type(AnalyticalContinuation), save :: empty
                self = empty
        end subroutine
        pure elemental function IsAnalyticalContinuationEqual(lhs, rhs) result(iseq)
                class(AnalyticalContinuation), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (AnalyticalContinuation)
                       iseq = iseq .and. (lhs%ntau == rhs%ntau)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nf == rhs%nf)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorAnalyticalContinuation(lhs, rhs)
                class(AnalyticalContinuation), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (AnalyticalContinuation)
                       lhs%ntau = rhs%ntau
                       lhs%nf = rhs%nf
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateAnalyticalContinuationObjectFields(self)
                class(AnalyticalContinuation), intent(inout) :: self
        end subroutine


        subroutine InitPartitionSpaceStruct(self)
                class(PartitionSpaceStruct), intent(inout) :: self
                call self%InitPersistent()
                self%green_matsubara_cutoff =  0.0d0
                call self%quantum_numbers%InitPersistent()
                call self%observables%InitPersistent()
                self%susceptibility_cutoff =  int(10,kind=int32)
                self%susceptibility_tail =  int(100,kind=int32)
                self%quantum_number_susceptibility = .False.
                self%occupation_susceptibility = .False.
                self%occupation_susceptibility_bulla = .False.
                self%occupation_susceptibility_direct = .False.
                self%green_bulla = .False.
                self%density_matrix_precise = .False.
                self%sweepA =  int(50,kind=int32)
                self%storeA =  int(100,kind=int32)
                self%sweepB =  int(250,kind=int32)
                self%storeB =  int(20,kind=int32)
                self%green_basis = "matsubara"
                self%probabilities = ""
        end subroutine
        subroutine StorePartitionSpaceStructObjectFields(self)
                class(PartitionSpaceStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%quantum_numbers%store(ps%FetchSubGroup(gid,  'quantum_numbers'))
                call self%observables%store(ps%FetchSubGroup(gid,  'observables'))
        end subroutine
        subroutine LoadPartitionSpaceStructObjectFields(self)
                class(PartitionSpaceStruct), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%quantum_numbers%load(ps%FetchSubGroup(gid,  'quantum_numbers'))
                call self%observables%load(ps%FetchSubGroup(gid,  'observables'))
        end subroutine
        subroutine ResetPartitionSpaceStructSectionFields(self)
                class(PartitionSpaceStruct), intent(inout) :: self
        end subroutine
        subroutine DisconnectPartitionSpaceStructObjectFields(self)
                class(PartitionSpaceStruct), intent(inout) :: self
               type(iterator) :: iter
                call self%quantum_numbers%disconnect()
                call self%observables%disconnect()
        end subroutine
        subroutine StorePartitionSpaceStructScalarFields(self)
                class(PartitionSpaceStruct), intent(inout) :: self
                call self%write('green_matsubara_cutoff', self%green_matsubara_cutoff)
                call self%write('susceptibility_cutoff', self%susceptibility_cutoff)
                call self%write('susceptibility_tail', self%susceptibility_tail)
                call self%write('quantum_number_susceptibility', self%quantum_number_susceptibility)
                call self%write('occupation_susceptibility', self%occupation_susceptibility)
                call self%write('occupation_susceptibility_bulla', self%occupation_susceptibility_bulla)
                call self%write('occupation_susceptibility_direct', self%occupation_susceptibility_direct)
                call self%write('green_bulla', self%green_bulla)
                call self%write('density_matrix_precise', self%density_matrix_precise)
                call self%write('sweepA', self%sweepA)
                call self%write('storeA', self%storeA)
                call self%write('sweepB', self%sweepB)
                call self%write('storeB', self%storeB)
                call self%write('green_basis', self%green_basis)
                call self%write('probabilities', self%probabilities)
        end subroutine
        subroutine LoadPartitionSpaceStructScalarFields(self)
                class(PartitionSpaceStruct), intent(inout) :: self
                call self%read('green_matsubara_cutoff', self%green_matsubara_cutoff)
                call self%read('susceptibility_cutoff', self%susceptibility_cutoff)
                call self%read('susceptibility_tail', self%susceptibility_tail)
                call self%read('quantum_number_susceptibility', self%quantum_number_susceptibility)
                call self%read('occupation_susceptibility', self%occupation_susceptibility)
                call self%read('occupation_susceptibility_bulla', self%occupation_susceptibility_bulla)
                call self%read('occupation_susceptibility_direct', self%occupation_susceptibility_direct)
                call self%read('green_bulla', self%green_bulla)
                call self%read('density_matrix_precise', self%density_matrix_precise)
                call self%read('sweepA', self%sweepA)
                call self%read('storeA', self%storeA)
                call self%read('sweepB', self%sweepB)
                call self%read('storeB', self%storeB)
                call self%read('green_basis', self%green_basis)
                call self%read('probabilities', self%probabilities)
        end subroutine
        subroutine FinalizePartitionSpaceStruct(self)
               type(PartitionSpaceStruct), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearPartitionSpaceStruct(self)
                class(PartitionSpaceStruct), intent(inout) :: self
                type(PartitionSpaceStruct), save :: empty
                self = empty
        end subroutine
        pure elemental function IsPartitionSpaceStructEqual(lhs, rhs) result(iseq)
                class(PartitionSpaceStruct), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (PartitionSpaceStruct)
                       iseq = iseq .and. (lhs%green_matsubara_cutoff == rhs%green_matsubara_cutoff)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%quantum_numbers == rhs%quantum_numbers)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%observables == rhs%observables)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%susceptibility_cutoff == rhs%susceptibility_cutoff)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%susceptibility_tail == rhs%susceptibility_tail)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%quantum_number_susceptibility .eqv. rhs%quantum_number_susceptibility)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%occupation_susceptibility .eqv. rhs%occupation_susceptibility)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%occupation_susceptibility_bulla .eqv. rhs%occupation_susceptibility_bulla)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%occupation_susceptibility_direct .eqv. rhs%occupation_susceptibility_direct)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%green_bulla .eqv. rhs%green_bulla)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%density_matrix_precise .eqv. rhs%density_matrix_precise)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sweepA == rhs%sweepA)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%storeA == rhs%storeA)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sweepB == rhs%sweepB)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%storeB == rhs%storeB)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%green_basis == rhs%green_basis)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%probabilities == rhs%probabilities)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorPartitionSpaceStruct(lhs, rhs)
                class(PartitionSpaceStruct), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (PartitionSpaceStruct)
                       lhs%green_matsubara_cutoff = rhs%green_matsubara_cutoff
                       lhs%quantum_numbers = rhs%quantum_numbers
                       lhs%observables = rhs%observables
                       lhs%susceptibility_cutoff = rhs%susceptibility_cutoff
                       lhs%susceptibility_tail = rhs%susceptibility_tail
                       lhs%quantum_number_susceptibility = rhs%quantum_number_susceptibility
                       lhs%occupation_susceptibility = rhs%occupation_susceptibility
                       lhs%occupation_susceptibility_bulla = rhs%occupation_susceptibility_bulla
                       lhs%occupation_susceptibility_direct = rhs%occupation_susceptibility_direct
                       lhs%green_bulla = rhs%green_bulla
                       lhs%density_matrix_precise = rhs%density_matrix_precise
                       lhs%sweepA = rhs%sweepA
                       lhs%storeA = rhs%storeA
                       lhs%sweepB = rhs%sweepB
                       lhs%storeB = rhs%storeB
                       lhs%green_basis = rhs%green_basis
                       lhs%probabilities = rhs%probabilities
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocatePartitionSpaceStructObjectFields(self)
                class(PartitionSpaceStruct), intent(inout) :: self
        end subroutine


        subroutine InitParameters(self)
                class(Parameters), intent(inout) :: self
                call self%InitPersistent()
                self%beta =  0.0d0
                self%mu =  0.0d0
                self%complex = .False.
                call self%basis%InitPersistent()
                call self%hloc%InitPersistent()
                call self%hybridisation%InitPersistent()
                call self%partition%InitPersistent()
                call self%analytical_continuation%InitPersistent()
                call self%green%InitPersistent()
                call self%susc_ph%InitPersistent()
                call self%susc_pp%InitPersistent()
                call self%hedin_ph%InitPersistent()
                call self%hedin_pp%InitPersistent()
                call self%vertex%InitPersistent()
                self%measurement_steps =  int(0,kind=int32)
                self%thermalisation_steps =  int(0,kind=int32)
                self%measurement_time =  int(20,kind=int32)
                self%thermalisation_time =  int(5,kind=int32)
                self%sim_per_device =  int(25,kind=int32)
                self%quad_insert = .False.
                self%all_errors = .False.
                self%error = "parallel"
                self%trunc_dim =  0
                self%interaction_truncation =  0
        end subroutine
        subroutine StoreParametersObjectFields(self)
                class(Parameters), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%basis%store(ps%FetchSubGroup(gid,  'basis'))
                call self%hloc%store(ps%FetchSubGroup(gid,  'hloc'))
                call self%hybridisation%store(ps%FetchSubGroup(gid,  'hybridisation'))
                call self%partition%store(ps%FetchSubGroup(gid,  'partition'))
                call self%analytical_continuation%store(ps%FetchSubGroup(gid,  'analytical_continuation'))
                call self%green%store(ps%FetchSubGroup(gid,  'green'))
                call self%susc_ph%store(ps%FetchSubGroup(gid,  'susc_ph'))
                call self%susc_pp%store(ps%FetchSubGroup(gid,  'susc_pp'))
                call self%hedin_ph%store(ps%FetchSubGroup(gid,  'hedin_ph'))
                call self%hedin_pp%store(ps%FetchSubGroup(gid,  'hedin_pp'))
                call self%vertex%store(ps%FetchSubGroup(gid,  'vertex'))
        end subroutine
        subroutine LoadParametersObjectFields(self)
                class(Parameters), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%basis%load(ps%FetchSubGroup(gid,  'basis'))
                call self%hloc%load(ps%FetchSubGroup(gid,  'hloc'))
                call self%hybridisation%load(ps%FetchSubGroup(gid,  'hybridisation'))
                call self%partition%load(ps%FetchSubGroup(gid,  'partition'))
                call self%analytical_continuation%load(ps%FetchSubGroup(gid,  'analytical_continuation'))
                call self%green%load(ps%FetchSubGroup(gid,  'green'))
                call self%susc_ph%load(ps%FetchSubGroup(gid,  'susc_ph'))
                call self%susc_pp%load(ps%FetchSubGroup(gid,  'susc_pp'))
                call self%hedin_ph%load(ps%FetchSubGroup(gid,  'hedin_ph'))
                call self%hedin_pp%load(ps%FetchSubGroup(gid,  'hedin_pp'))
                call self%vertex%load(ps%FetchSubGroup(gid,  'vertex'))
        end subroutine
        subroutine ResetParametersSectionFields(self)
                class(Parameters), intent(inout) :: self
        end subroutine
        subroutine DisconnectParametersObjectFields(self)
                class(Parameters), intent(inout) :: self
               type(iterator) :: iter
                call self%basis%disconnect()
                call self%hloc%disconnect()
                call self%hybridisation%disconnect()
                call self%partition%disconnect()
                call self%analytical_continuation%disconnect()
                call self%green%disconnect()
                call self%susc_ph%disconnect()
                call self%susc_pp%disconnect()
                call self%hedin_ph%disconnect()
                call self%hedin_pp%disconnect()
                call self%vertex%disconnect()
        end subroutine
        subroutine StoreParametersScalarFields(self)
                class(Parameters), intent(inout) :: self
                call self%write('beta', self%beta)
                call self%write('mu', self%mu)
                call self%write('complex', self%complex)
                call self%write('measurement_steps', self%measurement_steps)
                call self%write('thermalisation_steps', self%thermalisation_steps)
                call self%write('measurement_time', self%measurement_time)
                call self%write('thermalisation_time', self%thermalisation_time)
                call self%write('sim_per_device', self%sim_per_device)
                call self%write('quad_insert', self%quad_insert)
                call self%write('all_errors', self%all_errors)
                call self%write('error', self%error)
                call self%write('trunc_dim', self%trunc_dim)
                call self%write('interaction_truncation', self%interaction_truncation)
        end subroutine
        subroutine LoadParametersScalarFields(self)
                class(Parameters), intent(inout) :: self
                call self%read('beta', self%beta)
                call self%read('mu', self%mu)
                call self%read('complex', self%complex)
                call self%read('measurement_steps', self%measurement_steps)
                call self%read('thermalisation_steps', self%thermalisation_steps)
                call self%read('measurement_time', self%measurement_time)
                call self%read('thermalisation_time', self%thermalisation_time)
                call self%read('sim_per_device', self%sim_per_device)
                call self%read('quad_insert', self%quad_insert)
                call self%read('all_errors', self%all_errors)
                call self%read('error', self%error)
                call self%read('trunc_dim', self%trunc_dim)
                call self%read('interaction_truncation', self%interaction_truncation)
        end subroutine
        subroutine FinalizeParameters(self)
               type(Parameters), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearParameters(self)
                class(Parameters), intent(inout) :: self
                type(Parameters), save :: empty
                self = empty
        end subroutine
        pure elemental function IsParametersEqual(lhs, rhs) result(iseq)
                class(Parameters), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Parameters)
                       iseq = iseq .and. (lhs%beta == rhs%beta)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%mu == rhs%mu)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%complex .eqv. rhs%complex)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%basis == rhs%basis)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hloc == rhs%hloc)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hybridisation == rhs%hybridisation)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%partition == rhs%partition)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%analytical_continuation == rhs%analytical_continuation)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%green == rhs%green)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%susc_ph == rhs%susc_ph)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%susc_pp == rhs%susc_pp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hedin_ph == rhs%hedin_ph)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hedin_pp == rhs%hedin_pp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%vertex == rhs%vertex)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%measurement_steps == rhs%measurement_steps)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%thermalisation_steps == rhs%thermalisation_steps)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%measurement_time == rhs%measurement_time)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%thermalisation_time == rhs%thermalisation_time)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sim_per_device == rhs%sim_per_device)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%quad_insert .eqv. rhs%quad_insert)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%all_errors .eqv. rhs%all_errors)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%error == rhs%error)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%trunc_dim == rhs%trunc_dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%interaction_truncation == rhs%interaction_truncation)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorParameters(lhs, rhs)
                class(Parameters), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Parameters)
                       lhs%beta = rhs%beta
                       lhs%mu = rhs%mu
                       lhs%complex = rhs%complex
                       lhs%basis = rhs%basis
                       lhs%hloc = rhs%hloc
                       lhs%hybridisation = rhs%hybridisation
                       lhs%partition = rhs%partition
                       lhs%analytical_continuation = rhs%analytical_continuation
                       lhs%green = rhs%green
                       lhs%susc_ph = rhs%susc_ph
                       lhs%susc_pp = rhs%susc_pp
                       lhs%hedin_ph = rhs%hedin_ph
                       lhs%hedin_pp = rhs%hedin_pp
                       lhs%vertex = rhs%vertex
                       lhs%measurement_steps = rhs%measurement_steps
                       lhs%thermalisation_steps = rhs%thermalisation_steps
                       lhs%measurement_time = rhs%measurement_time
                       lhs%thermalisation_time = rhs%thermalisation_time
                       lhs%sim_per_device = rhs%sim_per_device
                       lhs%quad_insert = rhs%quad_insert
                       lhs%all_errors = rhs%all_errors
                       lhs%error = rhs%error
                       lhs%trunc_dim = rhs%trunc_dim
                       lhs%interaction_truncation = rhs%interaction_truncation
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateParametersObjectFields(self)
                class(Parameters), intent(inout) :: self
        end subroutine



end module
