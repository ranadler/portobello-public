
module edx
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: Tensor2
        integer(kind=int32)  ::  len =  0

        type(vector_int)  ::  i1
        integer(kind=int32),pointer :: i1_(:)
        type(vector_int)  ::  i2
        integer(kind=int32),pointer :: i2_(:)
        type(vector_complex)  ::  value
        complex(kind=dp),pointer :: value_(:)


        contains
        procedure :: AllocateObjectFields => AllocateTensor2ObjectFields
        procedure :: ResetSectionFields   => ResetTensor2SectionFields
        procedure :: StoreScalarFields    => StoreTensor2ScalarFields
        procedure :: StoreObjectFields    => StoreTensor2ObjectFields
        procedure :: LoadScalarFields     => LoadTensor2ScalarFields
        procedure :: LoadObjectFields     => LoadTensor2ObjectFields
        procedure :: DisconnectObjectFields => DisconnectTensor2ObjectFields
        procedure :: IsEqual              => IsTensor2Equal
        procedure :: AssignmentOperator   => AssignmentOperatorTensor2
        procedure :: clear                => ClearTensor2
        procedure :: init => InitTensor2
#ifndef __GFORTRAN__
        final     :: FinalizeTensor2
#endif
        procedure :: GetTensor2I1Extents
        procedure :: GetTensor2I2Extents
        procedure :: GetTensor2ValueExtents

    end type

    type, extends(persistent) :: Tensor4
        integer(kind=int32)  ::  len =  0

        type(vector_int)  ::  i1
        integer(kind=int32),pointer :: i1_(:)
        type(vector_int)  ::  i2
        integer(kind=int32),pointer :: i2_(:)
        type(vector_int)  ::  i3
        integer(kind=int32),pointer :: i3_(:)
        type(vector_int)  ::  i4
        integer(kind=int32),pointer :: i4_(:)
        type(vector_complex)  ::  value
        complex(kind=dp),pointer :: value_(:)


        contains
        procedure :: AllocateObjectFields => AllocateTensor4ObjectFields
        procedure :: ResetSectionFields   => ResetTensor4SectionFields
        procedure :: StoreScalarFields    => StoreTensor4ScalarFields
        procedure :: StoreObjectFields    => StoreTensor4ObjectFields
        procedure :: LoadScalarFields     => LoadTensor4ScalarFields
        procedure :: LoadObjectFields     => LoadTensor4ObjectFields
        procedure :: DisconnectObjectFields => DisconnectTensor4ObjectFields
        procedure :: IsEqual              => IsTensor4Equal
        procedure :: AssignmentOperator   => AssignmentOperatorTensor4
        procedure :: clear                => ClearTensor4
        procedure :: init => InitTensor4
#ifndef __GFORTRAN__
        final     :: FinalizeTensor4
#endif
        procedure :: GetTensor4I1Extents
        procedure :: GetTensor4I2Extents
        procedure :: GetTensor4I3Extents
        procedure :: GetTensor4I4Extents
        procedure :: GetTensor4ValueExtents

    end type

    type, extends(persistent) :: T_csr
        integer(kind=int32)  ::  nnz =  0

        integer(kind=int32)  ::  m =  0

        integer(kind=int32)  ::  row_shift =  0

        integer(kind=int32)  ::  col_shift =  0

        type(vector_int)  ::  iaa
        integer(kind=int32),pointer :: iaa_(:)
        type(vector_int)  ::  jaa
        integer(kind=int32),pointer :: jaa_(:)
        type(vector_complex)  ::  aa
        complex(kind=dp),pointer :: aa_(:)


        contains
        procedure :: AllocateObjectFields => AllocateT_csrObjectFields
        procedure :: ResetSectionFields   => ResetT_csrSectionFields
        procedure :: StoreScalarFields    => StoreT_csrScalarFields
        procedure :: StoreObjectFields    => StoreT_csrObjectFields
        procedure :: LoadScalarFields     => LoadT_csrScalarFields
        procedure :: LoadObjectFields     => LoadT_csrObjectFields
        procedure :: DisconnectObjectFields => DisconnectT_csrObjectFields
        procedure :: IsEqual              => IsT_csrEqual
        procedure :: AssignmentOperator   => AssignmentOperatorT_csr
        procedure :: clear                => ClearT_csr
        procedure :: init => InitT_csr
#ifndef __GFORTRAN__
        final     :: FinalizeT_csr
#endif
        procedure :: GetT_csrIaaExtents
        procedure :: GetT_csrJaaExtents
        procedure :: GetT_csrAaExtents

    end type

    type, extends(persistent) :: CSRBlocks
        integer(kind=int32)  ::  nblock =  0

        type(T_csr), allocatable  ::  ham_csr(:)


        contains
        procedure :: AllocateObjectFields => AllocateCSRBlocksObjectFields
        procedure :: ResetSectionFields   => ResetCSRBlocksSectionFields
        procedure :: StoreScalarFields    => StoreCSRBlocksScalarFields
        procedure :: StoreObjectFields    => StoreCSRBlocksObjectFields
        procedure :: LoadScalarFields     => LoadCSRBlocksScalarFields
        procedure :: LoadObjectFields     => LoadCSRBlocksObjectFields
        procedure :: DisconnectObjectFields => DisconnectCSRBlocksObjectFields
        procedure :: IsEqual              => IsCSRBlocksEqual
        procedure :: AssignmentOperator   => AssignmentOperatorCSRBlocks
        procedure :: clear                => ClearCSRBlocks
        procedure :: init => InitCSRBlocks
#ifndef __GFORTRAN__
        final     :: FinalizeCSRBlocks
#endif

    end type

    type, extends(persistent) :: Setup
        integer(kind=int32)  ::  ed_solver =  int(1,kind=int32)

        integer(kind=int32)  ::  num_val_orbs =  int(2,kind=int32)

        integer(kind=int32)  ::  maxiter =  int(500,kind=int32)

        real(kind=dp)  ::  eigval_tol =  real(0.000000,kind=16)

        integer(kind=int32)  ::  nvector =  0

        integer(kind=int32)  ::  neval =  0

        integer(kind=int32)  ::  num_fock =  0

        logical  ::  debug_print = .False.

        type(vector_int)  ::  fock
        integer(kind=int32),pointer :: fock_(:)
        type(Tensor4)  ::  coulomb
        type(Tensor2)  ::  hopping_rep
        integer(kind=int32)  ::  ncv =  0

        type(string)  ::  prefix


        contains
        procedure :: AllocateObjectFields => AllocateSetupObjectFields
        procedure :: ResetSectionFields   => ResetSetupSectionFields
        procedure :: StoreScalarFields    => StoreSetupScalarFields
        procedure :: StoreObjectFields    => StoreSetupObjectFields
        procedure :: LoadScalarFields     => LoadSetupScalarFields
        procedure :: LoadObjectFields     => LoadSetupObjectFields
        procedure :: DisconnectObjectFields => DisconnectSetupObjectFields
        procedure :: IsEqual              => IsSetupEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSetup
        procedure :: clear                => ClearSetup
        procedure :: init => InitSetup
#ifndef __GFORTRAN__
        final     :: FinalizeSetup
#endif
        procedure :: GetSetupFockExtents

    end type

    type, extends(persistent) :: Input
        type(Tensor2)  ::  hopping


        contains
        procedure :: AllocateObjectFields => AllocateInputObjectFields
        procedure :: ResetSectionFields   => ResetInputSectionFields
        procedure :: StoreScalarFields    => StoreInputScalarFields
        procedure :: StoreObjectFields    => StoreInputObjectFields
        procedure :: LoadScalarFields     => LoadInputScalarFields
        procedure :: LoadObjectFields     => LoadInputObjectFields
        procedure :: DisconnectObjectFields => DisconnectInputObjectFields
        procedure :: IsEqual              => IsInputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorInput
        procedure :: clear                => ClearInput
        procedure :: init => InitInput
#ifndef __GFORTRAN__
        final     :: FinalizeInput
#endif

    end type

    type, extends(persistent) :: Output
        logical  ::  is_valid = .False.

        integer(kind=int32)  ::  num_val_orbs =  0

        integer(kind=int32)  ::  nvector =  0

        integer(kind=int32)  ::  neval =  0

        integer(kind=int32)  ::  num_fock =  0

        integer(kind=int32)  ::  num_orbital_fock_states =  0

        type(cube_complex)  ::  denmat
        complex(kind=dp),pointer :: denmat_(:,:,:)
        type(vector_complex)  ::  E
        complex(kind=dp),pointer :: E_(:)
        type(matrix_complex)  ::  eigvecs
        complex(kind=dp),pointer :: eigvecs_(:,:)
        type(vector_real)  ::  histogram
        real(kind=dp),pointer :: histogram_(:)


        contains
        procedure :: AllocateObjectFields => AllocateOutputObjectFields
        procedure :: ResetSectionFields   => ResetOutputSectionFields
        procedure :: StoreScalarFields    => StoreOutputScalarFields
        procedure :: StoreObjectFields    => StoreOutputObjectFields
        procedure :: LoadScalarFields     => LoadOutputScalarFields
        procedure :: LoadObjectFields     => LoadOutputObjectFields
        procedure :: DisconnectObjectFields => DisconnectOutputObjectFields
        procedure :: IsEqual              => IsOutputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOutput
        procedure :: clear                => ClearOutput
        procedure :: init => InitOutput
#ifndef __GFORTRAN__
        final     :: FinalizeOutput
#endif
        procedure :: GetOutputDenmatExtents
        procedure :: GetOutputEExtents
        procedure :: GetOutputEigvecsExtents
        procedure :: GetOutputHistogramExtents

    end type

    type, extends(persistent) :: EDSolverState
        integer(kind=int32)  ::  num_x =  0

        type(vector_real)  ::  x
        real(kind=dp),pointer :: x_(:)


        contains
        procedure :: AllocateObjectFields => AllocateEDSolverStateObjectFields
        procedure :: ResetSectionFields   => ResetEDSolverStateSectionFields
        procedure :: StoreScalarFields    => StoreEDSolverStateScalarFields
        procedure :: StoreObjectFields    => StoreEDSolverStateObjectFields
        procedure :: LoadScalarFields     => LoadEDSolverStateScalarFields
        procedure :: LoadObjectFields     => LoadEDSolverStateObjectFields
        procedure :: DisconnectObjectFields => DisconnectEDSolverStateObjectFields
        procedure :: IsEqual              => IsEDSolverStateEqual
        procedure :: AssignmentOperator   => AssignmentOperatorEDSolverState
        procedure :: clear                => ClearEDSolverState
        procedure :: init => InitEDSolverState
#ifndef __GFORTRAN__
        final     :: FinalizeEDSolverState
#endif
        procedure :: GetEdsolverstateXExtents

    end type

    contains
        subroutine InitTensor2(self)
                class(Tensor2), intent(inout) :: self
                call self%InitPersistent()
                self%len =  0
        end subroutine
        subroutine StoreTensor2ObjectFields(self)
                class(Tensor2), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%i1%StoreObject(ps, gid,  'i1')
                call self%i2%StoreObject(ps, gid,  'i2')
                call self%value%StoreObject(ps, gid,  'value')
        end subroutine
        subroutine LoadTensor2ObjectFields(self)
                class(Tensor2), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%i1%LoadObject(ps, gid,  'i1')
                call self%i2%LoadObject(ps, gid,  'i2')
                call self%value%LoadObject(ps, gid,  'value')
        end subroutine
        subroutine ResetTensor2SectionFields(self)
                class(Tensor2), intent(inout) :: self
                self%i1_ => self%i1%GetWithExtents(self%GetTensor2i1Extents())
                self%i2_ => self%i2%GetWithExtents(self%GetTensor2i2Extents())
                self%value_ => self%value%GetWithExtents(self%GetTensor2valueExtents())
        end subroutine
        subroutine DisconnectTensor2ObjectFields(self)
                class(Tensor2), intent(inout) :: self
               type(iterator) :: iter
                call self%i1%DisconnectFromStore()
                call self%i2%DisconnectFromStore()
                call self%value%DisconnectFromStore()
        end subroutine
        subroutine StoreTensor2ScalarFields(self)
                class(Tensor2), intent(inout) :: self
                call self%write('len', self%len)
        end subroutine
        subroutine LoadTensor2ScalarFields(self)
                class(Tensor2), intent(inout) :: self
                call self%read('len', self%len)
        end subroutine
        subroutine FinalizeTensor2(self)
               type(Tensor2), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearTensor2(self)
                class(Tensor2), intent(inout) :: self
                type(Tensor2), save :: empty
                self = empty
        end subroutine
        pure elemental function IsTensor2Equal(lhs, rhs) result(iseq)
                class(Tensor2), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Tensor2)
                       iseq = iseq .and. (lhs%len == rhs%len)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%i1 == rhs%i1)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%i2 == rhs%i2)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%value == rhs%value)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorTensor2(lhs, rhs)
                class(Tensor2), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Tensor2)
                       lhs%len = rhs%len
                       lhs%i1 = rhs%i1
                       lhs%i2 = rhs%i2
                       lhs%value = rhs%value
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetTensor2I1Extents(self) result(res)
                class(Tensor2), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%len)]
        end function
        function GetTensor2I2Extents(self) result(res)
                class(Tensor2), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%len)]
        end function
        function GetTensor2ValueExtents(self) result(res)
                class(Tensor2), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%len)]
        end function
        subroutine AllocateTensor2ObjectFields(self)
                class(Tensor2), intent(inout) :: self
                call self%i1%init(int(self%len))
                call self%i2%init(int(self%len))
                call self%value%init(int(self%len))
        end subroutine


        subroutine InitTensor4(self)
                class(Tensor4), intent(inout) :: self
                call self%InitPersistent()
                self%len =  0
        end subroutine
        subroutine StoreTensor4ObjectFields(self)
                class(Tensor4), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%i1%StoreObject(ps, gid,  'i1')
                call self%i2%StoreObject(ps, gid,  'i2')
                call self%i3%StoreObject(ps, gid,  'i3')
                call self%i4%StoreObject(ps, gid,  'i4')
                call self%value%StoreObject(ps, gid,  'value')
        end subroutine
        subroutine LoadTensor4ObjectFields(self)
                class(Tensor4), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%i1%LoadObject(ps, gid,  'i1')
                call self%i2%LoadObject(ps, gid,  'i2')
                call self%i3%LoadObject(ps, gid,  'i3')
                call self%i4%LoadObject(ps, gid,  'i4')
                call self%value%LoadObject(ps, gid,  'value')
        end subroutine
        subroutine ResetTensor4SectionFields(self)
                class(Tensor4), intent(inout) :: self
                self%i1_ => self%i1%GetWithExtents(self%GetTensor4i1Extents())
                self%i2_ => self%i2%GetWithExtents(self%GetTensor4i2Extents())
                self%i3_ => self%i3%GetWithExtents(self%GetTensor4i3Extents())
                self%i4_ => self%i4%GetWithExtents(self%GetTensor4i4Extents())
                self%value_ => self%value%GetWithExtents(self%GetTensor4valueExtents())
        end subroutine
        subroutine DisconnectTensor4ObjectFields(self)
                class(Tensor4), intent(inout) :: self
               type(iterator) :: iter
                call self%i1%DisconnectFromStore()
                call self%i2%DisconnectFromStore()
                call self%i3%DisconnectFromStore()
                call self%i4%DisconnectFromStore()
                call self%value%DisconnectFromStore()
        end subroutine
        subroutine StoreTensor4ScalarFields(self)
                class(Tensor4), intent(inout) :: self
                call self%write('len', self%len)
        end subroutine
        subroutine LoadTensor4ScalarFields(self)
                class(Tensor4), intent(inout) :: self
                call self%read('len', self%len)
        end subroutine
        subroutine FinalizeTensor4(self)
               type(Tensor4), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearTensor4(self)
                class(Tensor4), intent(inout) :: self
                type(Tensor4), save :: empty
                self = empty
        end subroutine
        pure elemental function IsTensor4Equal(lhs, rhs) result(iseq)
                class(Tensor4), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Tensor4)
                       iseq = iseq .and. (lhs%len == rhs%len)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%i1 == rhs%i1)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%i2 == rhs%i2)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%i3 == rhs%i3)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%i4 == rhs%i4)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%value == rhs%value)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorTensor4(lhs, rhs)
                class(Tensor4), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Tensor4)
                       lhs%len = rhs%len
                       lhs%i1 = rhs%i1
                       lhs%i2 = rhs%i2
                       lhs%i3 = rhs%i3
                       lhs%i4 = rhs%i4
                       lhs%value = rhs%value
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetTensor4I1Extents(self) result(res)
                class(Tensor4), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%len)]
        end function
        function GetTensor4I2Extents(self) result(res)
                class(Tensor4), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%len)]
        end function
        function GetTensor4I3Extents(self) result(res)
                class(Tensor4), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%len)]
        end function
        function GetTensor4I4Extents(self) result(res)
                class(Tensor4), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%len)]
        end function
        function GetTensor4ValueExtents(self) result(res)
                class(Tensor4), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%len)]
        end function
        subroutine AllocateTensor4ObjectFields(self)
                class(Tensor4), intent(inout) :: self
                call self%i1%init(int(self%len))
                call self%i2%init(int(self%len))
                call self%i3%init(int(self%len))
                call self%i4%init(int(self%len))
                call self%value%init(int(self%len))
        end subroutine


        subroutine InitT_csr(self)
                class(T_csr), intent(inout) :: self
                call self%InitPersistent()
                self%nnz =  0
                self%m =  0
                self%row_shift =  0
                self%col_shift =  0
        end subroutine
        subroutine StoreT_csrObjectFields(self)
                class(T_csr), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%iaa%StoreObject(ps, gid,  'iaa')
                call self%jaa%StoreObject(ps, gid,  'jaa')
                call self%aa%StoreObject(ps, gid,  'aa')
        end subroutine
        subroutine LoadT_csrObjectFields(self)
                class(T_csr), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%iaa%LoadObject(ps, gid,  'iaa')
                call self%jaa%LoadObject(ps, gid,  'jaa')
                call self%aa%LoadObject(ps, gid,  'aa')
        end subroutine
        subroutine ResetT_csrSectionFields(self)
                class(T_csr), intent(inout) :: self
                self%iaa_ => self%iaa%GetWithExtents(self%GetT_csriaaExtents())
                self%jaa_ => self%jaa%GetWithExtents(self%GetT_csrjaaExtents())
                self%aa_ => self%aa%GetWithExtents(self%GetT_csraaExtents())
        end subroutine
        subroutine DisconnectT_csrObjectFields(self)
                class(T_csr), intent(inout) :: self
               type(iterator) :: iter
                call self%iaa%DisconnectFromStore()
                call self%jaa%DisconnectFromStore()
                call self%aa%DisconnectFromStore()
        end subroutine
        subroutine StoreT_csrScalarFields(self)
                class(T_csr), intent(inout) :: self
                call self%write('nnz', self%nnz)
                call self%write('m', self%m)
                call self%write('row_shift', self%row_shift)
                call self%write('col_shift', self%col_shift)
        end subroutine
        subroutine LoadT_csrScalarFields(self)
                class(T_csr), intent(inout) :: self
                call self%read('nnz', self%nnz)
                call self%read('m', self%m)
                call self%read('row_shift', self%row_shift)
                call self%read('col_shift', self%col_shift)
        end subroutine
        subroutine FinalizeT_csr(self)
               type(T_csr), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearT_csr(self)
                class(T_csr), intent(inout) :: self
                type(T_csr), save :: empty
                self = empty
        end subroutine
        pure elemental function IsT_csrEqual(lhs, rhs) result(iseq)
                class(T_csr), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (T_csr)
                       iseq = iseq .and. (lhs%nnz == rhs%nnz)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%m == rhs%m)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%row_shift == rhs%row_shift)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%col_shift == rhs%col_shift)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%iaa == rhs%iaa)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%jaa == rhs%jaa)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%aa == rhs%aa)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorT_csr(lhs, rhs)
                class(T_csr), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (T_csr)
                       lhs%nnz = rhs%nnz
                       lhs%m = rhs%m
                       lhs%row_shift = rhs%row_shift
                       lhs%col_shift = rhs%col_shift
                       lhs%iaa = rhs%iaa
                       lhs%jaa = rhs%jaa
                       lhs%aa = rhs%aa
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetT_csrIaaExtents(self) result(res)
                class(T_csr), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%m + 1)]
        end function
        function GetT_csrJaaExtents(self) result(res)
                class(T_csr), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%nnz)]
        end function
        function GetT_csrAaExtents(self) result(res)
                class(T_csr), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%nnz)]
        end function
        subroutine AllocateT_csrObjectFields(self)
                class(T_csr), intent(inout) :: self
                call self%iaa%init(int(self%m + 1))
                call self%jaa%init(int(self%nnz))
                call self%aa%init(int(self%nnz))
        end subroutine


        subroutine InitCSRBlocks(self)
                class(CSRBlocks), intent(inout) :: self
                call self%InitPersistent()
                self%nblock =  0
        end subroutine
        subroutine StoreCSRBlocksObjectFields(self)
                class(CSRBlocks), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                call iter%Init(lbound(self%ham_csr), ubound(self%ham_csr))
                do while (.not. iter%Done())
                    call self%ham_csr(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'ham_csr', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadCSRBlocksObjectFields(self)
                class(CSRBlocks), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                allocate(self%ham_csr(int(1):int(self%nblock)))
                call iter%Init(lbound(self%ham_csr), ubound(self%ham_csr))
                do while (.not. iter%Done())
                    call self%ham_csr(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'ham_csr', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetCSRBlocksSectionFields(self)
                class(CSRBlocks), intent(inout) :: self
        end subroutine
        subroutine DisconnectCSRBlocksObjectFields(self)
                class(CSRBlocks), intent(inout) :: self
               type(iterator) :: iter

                call iter%Init(lbound(self%ham_csr), ubound(self%ham_csr))
                do while (.not. iter%Done())
                    call self%ham_csr(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreCSRBlocksScalarFields(self)
                class(CSRBlocks), intent(inout) :: self
                call self%write('nblock', self%nblock)
        end subroutine
        subroutine LoadCSRBlocksScalarFields(self)
                class(CSRBlocks), intent(inout) :: self
                call self%read('nblock', self%nblock)
        end subroutine
        subroutine FinalizeCSRBlocks(self)
               type(CSRBlocks), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearCSRBlocks(self)
                class(CSRBlocks), intent(inout) :: self
                type(CSRBlocks), save :: empty
                self = empty
        end subroutine
        pure elemental function IsCSRBlocksEqual(lhs, rhs) result(iseq)
                class(CSRBlocks), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (CSRBlocks)
                       iseq = iseq .and. (lhs%nblock == rhs%nblock)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%ham_csr) .eqv. allocated(rhs%ham_csr))
                       if (.not. iseq) return
                       if (allocated(lhs%ham_csr)) then
                           iseq = iseq .and. all(shape(lhs%ham_csr) == shape(rhs%ham_csr))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%ham_csr(:) == rhs%ham_csr(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorCSRBlocks(lhs, rhs)
                class(CSRBlocks), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (CSRBlocks)
                       lhs%nblock = rhs%nblock
                       lhs%ham_csr = rhs%ham_csr
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateCSRBlocksObjectFields(self)
                class(CSRBlocks), intent(inout) :: self
                allocate(self%ham_csr(int(1):int(self%nblock)))
        end subroutine


        subroutine InitSetup(self)
                class(Setup), intent(inout) :: self
                call self%InitPersistent()
                self%ed_solver =  int(1,kind=int32)
                self%num_val_orbs =  int(2,kind=int32)
                self%maxiter =  int(500,kind=int32)
                self%eigval_tol =  real(0.000000,kind=16)
                self%nvector =  0
                self%neval =  0
                self%num_fock =  0
                self%debug_print = .False.
                call self%coulomb%InitPersistent()
                call self%hopping_rep%InitPersistent()
                self%ncv =  0
                self%prefix = "V0-"
        end subroutine
        subroutine StoreSetupObjectFields(self)
                class(Setup), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%fock%StoreObject(ps, gid,  'fock')
                call self%coulomb%store(ps%FetchSubGroup(gid,  'coulomb'))
                call self%hopping_rep%store(ps%FetchSubGroup(gid,  'hopping_rep'))
        end subroutine
        subroutine LoadSetupObjectFields(self)
                class(Setup), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%fock%LoadObject(ps, gid,  'fock')
                call self%coulomb%load(ps%FetchSubGroup(gid,  'coulomb'))
                call self%hopping_rep%load(ps%FetchSubGroup(gid,  'hopping_rep'))
        end subroutine
        subroutine ResetSetupSectionFields(self)
                class(Setup), intent(inout) :: self
                self%fock_ => self%fock%GetWithExtents(self%GetSetupfockExtents())
        end subroutine
        subroutine DisconnectSetupObjectFields(self)
                class(Setup), intent(inout) :: self
               type(iterator) :: iter
                call self%fock%DisconnectFromStore()
                call self%coulomb%disconnect()
                call self%hopping_rep%disconnect()
        end subroutine
        subroutine StoreSetupScalarFields(self)
                class(Setup), intent(inout) :: self
                call self%write('ed_solver', self%ed_solver)
                call self%write('num_val_orbs', self%num_val_orbs)
                call self%write('maxiter', self%maxiter)
                call self%write('eigval_tol', self%eigval_tol)
                call self%write('nvector', self%nvector)
                call self%write('neval', self%neval)
                call self%write('num_fock', self%num_fock)
                call self%write('debug_print', self%debug_print)
                call self%write('ncv', self%ncv)
                call self%write('prefix', self%prefix)
        end subroutine
        subroutine LoadSetupScalarFields(self)
                class(Setup), intent(inout) :: self
                call self%read('ed_solver', self%ed_solver)
                call self%read('num_val_orbs', self%num_val_orbs)
                call self%read('maxiter', self%maxiter)
                call self%read('eigval_tol', self%eigval_tol)
                call self%read('nvector', self%nvector)
                call self%read('neval', self%neval)
                call self%read('num_fock', self%num_fock)
                call self%read('debug_print', self%debug_print)
                call self%read('ncv', self%ncv)
                call self%read('prefix', self%prefix)
        end subroutine
        subroutine FinalizeSetup(self)
               type(Setup), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSetup(self)
                class(Setup), intent(inout) :: self
                type(Setup), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSetupEqual(lhs, rhs) result(iseq)
                class(Setup), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Setup)
                       iseq = iseq .and. (lhs%ed_solver == rhs%ed_solver)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_val_orbs == rhs%num_val_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxiter == rhs%maxiter)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%eigval_tol == rhs%eigval_tol)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nvector == rhs%nvector)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%neval == rhs%neval)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_fock == rhs%num_fock)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%debug_print .eqv. rhs%debug_print)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%fock == rhs%fock)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%coulomb == rhs%coulomb)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hopping_rep == rhs%hopping_rep)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ncv == rhs%ncv)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%prefix == rhs%prefix)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSetup(lhs, rhs)
                class(Setup), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Setup)
                       lhs%ed_solver = rhs%ed_solver
                       lhs%num_val_orbs = rhs%num_val_orbs
                       lhs%maxiter = rhs%maxiter
                       lhs%eigval_tol = rhs%eigval_tol
                       lhs%nvector = rhs%nvector
                       lhs%neval = rhs%neval
                       lhs%num_fock = rhs%num_fock
                       lhs%debug_print = rhs%debug_print
                       lhs%fock = rhs%fock
                       lhs%coulomb = rhs%coulomb
                       lhs%hopping_rep = rhs%hopping_rep
                       lhs%ncv = rhs%ncv
                       lhs%prefix = rhs%prefix
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetSetupFockExtents(self) result(res)
                class(Setup), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_fock)]
        end function
        subroutine AllocateSetupObjectFields(self)
                class(Setup), intent(inout) :: self
                call self%fock%init(int(self%num_fock))
        end subroutine


        subroutine InitInput(self)
                class(Input), intent(inout) :: self
                call self%InitPersistent()
                call self%hopping%InitPersistent()
        end subroutine
        subroutine StoreInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%hopping%store(ps%FetchSubGroup(gid,  'hopping'))
        end subroutine
        subroutine LoadInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%hopping%load(ps%FetchSubGroup(gid,  'hopping'))
        end subroutine
        subroutine ResetInputSectionFields(self)
                class(Input), intent(inout) :: self
        end subroutine
        subroutine DisconnectInputObjectFields(self)
                class(Input), intent(inout) :: self
               type(iterator) :: iter
                call self%hopping%disconnect()
        end subroutine
        subroutine StoreInputScalarFields(self)
                class(Input), intent(inout) :: self
        end subroutine
        subroutine LoadInputScalarFields(self)
                class(Input), intent(inout) :: self
        end subroutine
        subroutine FinalizeInput(self)
               type(Input), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearInput(self)
                class(Input), intent(inout) :: self
                type(Input), save :: empty
                self = empty
        end subroutine
        pure elemental function IsInputEqual(lhs, rhs) result(iseq)
                class(Input), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Input)
                       iseq = iseq .and. (lhs%hopping == rhs%hopping)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorInput(lhs, rhs)
                class(Input), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Input)
                       lhs%hopping = rhs%hopping
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateInputObjectFields(self)
                class(Input), intent(inout) :: self
        end subroutine


        subroutine InitOutput(self)
                class(Output), intent(inout) :: self
                call self%InitPersistent()
                self%is_valid = .False.
                self%num_val_orbs =  0
                self%nvector =  0
                self%neval =  0
                self%num_fock =  0
                self%num_orbital_fock_states =  0
        end subroutine
        subroutine StoreOutputObjectFields(self)
                class(Output), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%denmat%StoreObject(ps, gid,  'denmat')
                call self%E%StoreObject(ps, gid,  'E')
                call self%eigvecs%StoreObject(ps, gid,  'eigvecs')
                call self%histogram%StoreObject(ps, gid,  'histogram')
        end subroutine
        subroutine LoadOutputObjectFields(self)
                class(Output), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%denmat%LoadObject(ps, gid,  'denmat')
                call self%E%LoadObject(ps, gid,  'E')
                call self%eigvecs%LoadObject(ps, gid,  'eigvecs')
                call self%histogram%LoadObject(ps, gid,  'histogram')
        end subroutine
        subroutine ResetOutputSectionFields(self)
                class(Output), intent(inout) :: self
                self%denmat_ => self%denmat%GetWithExtents(self%GetOutputdenmatExtents())
                self%E_ => self%E%GetWithExtents(self%GetOutputEExtents())
                self%eigvecs_ => self%eigvecs%GetWithExtents(self%GetOutputeigvecsExtents())
                self%histogram_ => self%histogram%GetWithExtents(self%GetOutputhistogramExtents())
        end subroutine
        subroutine DisconnectOutputObjectFields(self)
                class(Output), intent(inout) :: self
               type(iterator) :: iter
                call self%denmat%DisconnectFromStore()
                call self%E%DisconnectFromStore()
                call self%eigvecs%DisconnectFromStore()
                call self%histogram%DisconnectFromStore()
        end subroutine
        subroutine StoreOutputScalarFields(self)
                class(Output), intent(inout) :: self
                call self%write('is_valid', self%is_valid)
                call self%write('num_val_orbs', self%num_val_orbs)
                call self%write('nvector', self%nvector)
                call self%write('neval', self%neval)
                call self%write('num_fock', self%num_fock)
                call self%write('num_orbital_fock_states', self%num_orbital_fock_states)
        end subroutine
        subroutine LoadOutputScalarFields(self)
                class(Output), intent(inout) :: self
                call self%read('is_valid', self%is_valid)
                call self%read('num_val_orbs', self%num_val_orbs)
                call self%read('nvector', self%nvector)
                call self%read('neval', self%neval)
                call self%read('num_fock', self%num_fock)
                call self%read('num_orbital_fock_states', self%num_orbital_fock_states)
        end subroutine
        subroutine FinalizeOutput(self)
               type(Output), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOutput(self)
                class(Output), intent(inout) :: self
                type(Output), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOutputEqual(lhs, rhs) result(iseq)
                class(Output), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Output)
                       iseq = iseq .and. (lhs%is_valid .eqv. rhs%is_valid)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_val_orbs == rhs%num_val_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nvector == rhs%nvector)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%neval == rhs%neval)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_fock == rhs%num_fock)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_orbital_fock_states == rhs%num_orbital_fock_states)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%denmat == rhs%denmat)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%E == rhs%E)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%eigvecs == rhs%eigvecs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%histogram == rhs%histogram)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOutput(lhs, rhs)
                class(Output), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Output)
                       lhs%is_valid = rhs%is_valid
                       lhs%num_val_orbs = rhs%num_val_orbs
                       lhs%nvector = rhs%nvector
                       lhs%neval = rhs%neval
                       lhs%num_fock = rhs%num_fock
                       lhs%num_orbital_fock_states = rhs%num_orbital_fock_states
                       lhs%denmat = rhs%denmat
                       lhs%E = rhs%E
                       lhs%eigvecs = rhs%eigvecs
                       lhs%histogram = rhs%histogram
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetOutputDenmatExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_val_orbs),extent(1,self%num_val_orbs),extent(1,self%nvector)]
        end function
        function GetOutputEExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%neval)]
        end function
        function GetOutputEigvecsExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_fock),extent(1,self%nvector)]
        end function
        function GetOutputHistogramExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_orbital_fock_states)]
        end function
        subroutine AllocateOutputObjectFields(self)
                class(Output), intent(inout) :: self
                call self%denmat%init(int(self%num_val_orbs),int(self%num_val_orbs),int(self%nvector))
                call self%E%init(int(self%neval))
                call self%eigvecs%init(int(self%num_fock),int(self%nvector))
                call self%histogram%init(int(self%num_orbital_fock_states))
        end subroutine


        subroutine InitEDSolverState(self)
                class(EDSolverState), intent(inout) :: self
                call self%InitPersistent()
                self%num_x =  0
        end subroutine
        subroutine StoreEDSolverStateObjectFields(self)
                class(EDSolverState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%x%StoreObject(ps, gid,  'x')
        end subroutine
        subroutine LoadEDSolverStateObjectFields(self)
                class(EDSolverState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%x%LoadObject(ps, gid,  'x')
        end subroutine
        subroutine ResetEDSolverStateSectionFields(self)
                class(EDSolverState), intent(inout) :: self
                self%x_ => self%x%GetWithExtents(self%GetEDSolverStatexExtents())
        end subroutine
        subroutine DisconnectEDSolverStateObjectFields(self)
                class(EDSolverState), intent(inout) :: self
               type(iterator) :: iter
                call self%x%DisconnectFromStore()
        end subroutine
        subroutine StoreEDSolverStateScalarFields(self)
                class(EDSolverState), intent(inout) :: self
                call self%write('num_x', self%num_x)
        end subroutine
        subroutine LoadEDSolverStateScalarFields(self)
                class(EDSolverState), intent(inout) :: self
                call self%read('num_x', self%num_x)
        end subroutine
        subroutine FinalizeEDSolverState(self)
               type(EDSolverState), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearEDSolverState(self)
                class(EDSolverState), intent(inout) :: self
                type(EDSolverState), save :: empty
                self = empty
        end subroutine
        pure elemental function IsEDSolverStateEqual(lhs, rhs) result(iseq)
                class(EDSolverState), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (EDSolverState)
                       iseq = iseq .and. (lhs%num_x == rhs%num_x)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%x == rhs%x)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorEDSolverState(lhs, rhs)
                class(EDSolverState), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (EDSolverState)
                       lhs%num_x = rhs%num_x
                       lhs%x = rhs%x
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetEdsolverstateXExtents(self) result(res)
                class(EDSolverState), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_x)]
        end function
        subroutine AllocateEDSolverStateObjectFields(self)
                class(EDSolverState), intent(inout) :: self
                call self%x%init(int(self%num_x))
        end subroutine



end module
