
module wannier_internal
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use array4
    use base_types
    use cube
    use matrix


    implicit none
    public

    type, extends(persistent) :: Wannier90Bounds
        integer(kind=int32)  ::  min_band =  0

        integer(kind=int32)  ::  max_band =  0

        integer(kind=int32)  ::  num_bands =  0

        integer(kind=int32)  ::  num_wann =  0

        integer(kind=int32)  ::  num_k_all =  0



        contains
        procedure :: AllocateObjectFields => AllocateWannier90BoundsObjectFields
        procedure :: ResetSectionFields   => ResetWannier90BoundsSectionFields
        procedure :: StoreScalarFields    => StoreWannier90BoundsScalarFields
        procedure :: StoreObjectFields    => StoreWannier90BoundsObjectFields
        procedure :: LoadScalarFields     => LoadWannier90BoundsScalarFields
        procedure :: LoadObjectFields     => LoadWannier90BoundsObjectFields
        procedure :: DisconnectObjectFields => DisconnectWannier90BoundsObjectFields
        procedure :: IsEqual              => IsWannier90BoundsEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWannier90Bounds
        procedure :: clear                => ClearWannier90Bounds
        procedure :: init => InitWannier90Bounds
#ifndef __GFORTRAN__
        final     :: FinalizeWannier90Bounds
#endif

    end type

    type, extends(Wannier90Bounds) :: Wannier90Overlap
        integer(kind=int32)  ::  nntot =  0

        type(array4_complex)  ::  M
        complex(kind=dp),pointer :: M_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateWannier90OverlapObjectFields
        procedure :: ResetSectionFields   => ResetWannier90OverlapSectionFields
        procedure :: StoreScalarFields    => StoreWannier90OverlapScalarFields
        procedure :: StoreObjectFields    => StoreWannier90OverlapObjectFields
        procedure :: LoadScalarFields     => LoadWannier90OverlapScalarFields
        procedure :: LoadObjectFields     => LoadWannier90OverlapObjectFields
        procedure :: DisconnectObjectFields => DisconnectWannier90OverlapObjectFields
        procedure :: IsEqual              => IsWannier90OverlapEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWannier90Overlap
        procedure :: clear                => ClearWannier90Overlap
        procedure :: init => InitWannier90Overlap
#ifndef __GFORTRAN__
        final     :: FinalizeWannier90Overlap
#endif
        procedure :: GetWannier90overlapMExtents

    end type

    type, extends(Wannier90Bounds) :: Wannier90SymAdapted
        integer(kind=int32)  ::  num_k_irr =  0

        integer(kind=int32)  ::  num_ops =  0

        type(array4_complex)  ::  KSRep
        complex(kind=dp),pointer :: KSRep_(:,:,:,:)
        type(array4_complex)  ::  OrbitalRep
        complex(kind=dp),pointer :: OrbitalRep_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateWannier90SymAdaptedObjectFields
        procedure :: ResetSectionFields   => ResetWannier90SymAdaptedSectionFields
        procedure :: StoreScalarFields    => StoreWannier90SymAdaptedScalarFields
        procedure :: StoreObjectFields    => StoreWannier90SymAdaptedObjectFields
        procedure :: LoadScalarFields     => LoadWannier90SymAdaptedScalarFields
        procedure :: LoadObjectFields     => LoadWannier90SymAdaptedObjectFields
        procedure :: DisconnectObjectFields => DisconnectWannier90SymAdaptedObjectFields
        procedure :: IsEqual              => IsWannier90SymAdaptedEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWannier90SymAdapted
        procedure :: clear                => ClearWannier90SymAdapted
        procedure :: init => InitWannier90SymAdapted
#ifndef __GFORTRAN__
        final     :: FinalizeWannier90SymAdapted
#endif
        procedure :: GetWannier90symadaptedKsrepExtents
        procedure :: GetWannier90symadaptedOrbitalrepExtents

    end type

    type, extends(Wannier90Bounds) :: Wannier90Workspace
        integer(kind=int32)  ::  num_nnmax =  int(12,kind=int32)

        type(Window)  ::  dis_window
        type(matrix_real)  ::  k_lat
        real(kind=dp),pointer :: k_lat_(:,:)
        type(matrix_int)  ::  nnlist
        integer(kind=int32),pointer :: nnlist_(:,:)
        type(cube_int)  ::  nncell
        integer(kind=int32),pointer :: nncell_(:,:,:)
        type(matrix_real)  ::  eigenvalues
        real(kind=dp),pointer :: eigenvalues_(:,:)
        type(Wannier90Overlap)  ::  overlap
        type(cube_complex)  ::  U
        complex(kind=dp),pointer :: U_(:,:,:)
        type(cube_complex)  ::  U_opt
        complex(kind=dp),pointer :: U_opt_(:,:,:)
        type(cube_complex)  ::  M0
        complex(kind=dp),pointer :: M0_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateWannier90WorkspaceObjectFields
        procedure :: ResetSectionFields   => ResetWannier90WorkspaceSectionFields
        procedure :: StoreScalarFields    => StoreWannier90WorkspaceScalarFields
        procedure :: StoreObjectFields    => StoreWannier90WorkspaceObjectFields
        procedure :: LoadScalarFields     => LoadWannier90WorkspaceScalarFields
        procedure :: LoadObjectFields     => LoadWannier90WorkspaceObjectFields
        procedure :: DisconnectObjectFields => DisconnectWannier90WorkspaceObjectFields
        procedure :: IsEqual              => IsWannier90WorkspaceEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWannier90Workspace
        procedure :: clear                => ClearWannier90Workspace
        procedure :: init => InitWannier90Workspace
#ifndef __GFORTRAN__
        final     :: FinalizeWannier90Workspace
#endif
        procedure :: GetWannier90workspaceK_latExtents
        procedure :: GetWannier90workspaceNnlistExtents
        procedure :: GetWannier90workspaceNncellExtents
        procedure :: GetWannier90workspaceEigenvaluesExtents
        procedure :: GetWannier90workspaceUExtents
        procedure :: GetWannier90workspaceU_optExtents
        procedure :: GetWannier90workspaceM0Extents

    end type

    contains
        subroutine InitWannier90Bounds(self)
                class(Wannier90Bounds), intent(inout) :: self
                call self%InitPersistent()
                self%min_band =  0
                self%max_band =  0
                self%num_bands =  0
                self%num_wann =  0
                self%num_k_all =  0
        end subroutine
        subroutine StoreWannier90BoundsObjectFields(self)
                class(Wannier90Bounds), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadWannier90BoundsObjectFields(self)
                class(Wannier90Bounds), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetWannier90BoundsSectionFields(self)
                class(Wannier90Bounds), intent(inout) :: self
        end subroutine
        subroutine DisconnectWannier90BoundsObjectFields(self)
                class(Wannier90Bounds), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreWannier90BoundsScalarFields(self)
                class(Wannier90Bounds), intent(inout) :: self
                call self%write('min_band', self%min_band)
                call self%write('max_band', self%max_band)
                call self%write('num_bands', self%num_bands)
                call self%write('num_wann', self%num_wann)
                call self%write('num_k_all', self%num_k_all)
        end subroutine
        subroutine LoadWannier90BoundsScalarFields(self)
                class(Wannier90Bounds), intent(inout) :: self
                call self%read('min_band', self%min_band)
                call self%read('max_band', self%max_band)
                call self%read('num_bands', self%num_bands)
                call self%read('num_wann', self%num_wann)
                call self%read('num_k_all', self%num_k_all)
        end subroutine
        subroutine FinalizeWannier90Bounds(self)
               type(Wannier90Bounds), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWannier90Bounds(self)
                class(Wannier90Bounds), intent(inout) :: self
                type(Wannier90Bounds), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWannier90BoundsEqual(lhs, rhs) result(iseq)
                class(Wannier90Bounds), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Wannier90Bounds)
                       iseq = iseq .and. (lhs%min_band == rhs%min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_band == rhs%max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_bands == rhs%num_bands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_wann == rhs%num_wann)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_all == rhs%num_k_all)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWannier90Bounds(lhs, rhs)
                class(Wannier90Bounds), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Wannier90Bounds)
                       lhs%min_band = rhs%min_band
                       lhs%max_band = rhs%max_band
                       lhs%num_bands = rhs%num_bands
                       lhs%num_wann = rhs%num_wann
                       lhs%num_k_all = rhs%num_k_all
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateWannier90BoundsObjectFields(self)
                class(Wannier90Bounds), intent(inout) :: self
        end subroutine


        subroutine InitWannier90Overlap(self)
                class(Wannier90Overlap), intent(inout) :: self
                call self%Wannier90Bounds%Init()
                self%nntot =  0
        end subroutine
        subroutine StoreWannier90OverlapObjectFields(self)
                class(Wannier90Overlap), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Wannier90Bounds%StoreObjectFields()
                call self%M%StoreObject(ps, gid,  'M')
        end subroutine
        subroutine LoadWannier90OverlapObjectFields(self)
                class(Wannier90Overlap), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Wannier90Bounds%LoadObjectFields()
                call self%M%LoadObject(ps, gid,  'M')
        end subroutine
        subroutine ResetWannier90OverlapSectionFields(self)
                class(Wannier90Overlap), intent(inout) :: self
                call self%Wannier90Bounds%ResetSectionFields()
                self%M_ => self%M%GetWithExtents(self%GetWannier90OverlapMExtents())
        end subroutine
        subroutine DisconnectWannier90OverlapObjectFields(self)
                class(Wannier90Overlap), intent(inout) :: self
               type(iterator) :: iter
                call self%Wannier90Bounds%DisconnectObjectFields()
                call self%M%DisconnectFromStore()
        end subroutine
        subroutine StoreWannier90OverlapScalarFields(self)
                class(Wannier90Overlap), intent(inout) :: self
                call self%Wannier90Bounds%StoreScalarFields()
                call self%write('nntot', self%nntot)
        end subroutine
        subroutine LoadWannier90OverlapScalarFields(self)
                class(Wannier90Overlap), intent(inout) :: self
                call self%Wannier90Bounds%LoadScalarFields()
                call self%read('nntot', self%nntot)
        end subroutine
        subroutine FinalizeWannier90Overlap(self)
               type(Wannier90Overlap), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWannier90Overlap(self)
                class(Wannier90Overlap), intent(inout) :: self
                type(Wannier90Overlap), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWannier90OverlapEqual(lhs, rhs) result(iseq)
                class(Wannier90Overlap), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Wannier90Overlap)
                       iseq = iseq .and. (lhs%Wannier90Bounds == rhs%Wannier90Bounds)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nntot == rhs%nntot)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M == rhs%M)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWannier90Overlap(lhs, rhs)
                class(Wannier90Overlap), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Wannier90Overlap)
                       lhs%Wannier90Bounds = rhs%Wannier90Bounds
                       lhs%nntot = rhs%nntot
                       lhs%M = rhs%M
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetWannier90overlapMExtents(self) result(res)
                class(Wannier90Overlap), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%num_bands),extent(1,self%num_bands),extent(1,self%nntot),extent(1,self%num_k_all)]
        end function
        subroutine AllocateWannier90OverlapObjectFields(self)
                class(Wannier90Overlap), intent(inout) :: self
                call self%Wannier90Bounds%AllocateObjectFields()
                call self%M%init(int(self%num_bands),int(self%num_bands),int(self%nntot),int(self%num_k_all))
        end subroutine


        subroutine InitWannier90SymAdapted(self)
                class(Wannier90SymAdapted), intent(inout) :: self
                call self%Wannier90Bounds%Init()
                self%num_k_irr =  0
                self%num_ops =  0
        end subroutine
        subroutine StoreWannier90SymAdaptedObjectFields(self)
                class(Wannier90SymAdapted), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Wannier90Bounds%StoreObjectFields()
                call self%KSRep%StoreObject(ps, gid,  'KSRep')
                call self%OrbitalRep%StoreObject(ps, gid,  'OrbitalRep')
        end subroutine
        subroutine LoadWannier90SymAdaptedObjectFields(self)
                class(Wannier90SymAdapted), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Wannier90Bounds%LoadObjectFields()
                call self%KSRep%LoadObject(ps, gid,  'KSRep')
                call self%OrbitalRep%LoadObject(ps, gid,  'OrbitalRep')
        end subroutine
        subroutine ResetWannier90SymAdaptedSectionFields(self)
                class(Wannier90SymAdapted), intent(inout) :: self
                call self%Wannier90Bounds%ResetSectionFields()
                self%KSRep_ => self%KSRep%GetWithExtents(self%GetWannier90SymAdaptedKSRepExtents())
                self%OrbitalRep_ => self%OrbitalRep%GetWithExtents(self%GetWannier90SymAdaptedOrbitalRepExtents())
        end subroutine
        subroutine DisconnectWannier90SymAdaptedObjectFields(self)
                class(Wannier90SymAdapted), intent(inout) :: self
               type(iterator) :: iter
                call self%Wannier90Bounds%DisconnectObjectFields()
                call self%KSRep%DisconnectFromStore()
                call self%OrbitalRep%DisconnectFromStore()
        end subroutine
        subroutine StoreWannier90SymAdaptedScalarFields(self)
                class(Wannier90SymAdapted), intent(inout) :: self
                call self%Wannier90Bounds%StoreScalarFields()
                call self%write('num_k_irr', self%num_k_irr)
                call self%write('num_ops', self%num_ops)
        end subroutine
        subroutine LoadWannier90SymAdaptedScalarFields(self)
                class(Wannier90SymAdapted), intent(inout) :: self
                call self%Wannier90Bounds%LoadScalarFields()
                call self%read('num_k_irr', self%num_k_irr)
                call self%read('num_ops', self%num_ops)
        end subroutine
        subroutine FinalizeWannier90SymAdapted(self)
               type(Wannier90SymAdapted), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWannier90SymAdapted(self)
                class(Wannier90SymAdapted), intent(inout) :: self
                type(Wannier90SymAdapted), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWannier90SymAdaptedEqual(lhs, rhs) result(iseq)
                class(Wannier90SymAdapted), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Wannier90SymAdapted)
                       iseq = iseq .and. (lhs%Wannier90Bounds == rhs%Wannier90Bounds)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_irr == rhs%num_k_irr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_ops == rhs%num_ops)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%KSRep == rhs%KSRep)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%OrbitalRep == rhs%OrbitalRep)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWannier90SymAdapted(lhs, rhs)
                class(Wannier90SymAdapted), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Wannier90SymAdapted)
                       lhs%Wannier90Bounds = rhs%Wannier90Bounds
                       lhs%num_k_irr = rhs%num_k_irr
                       lhs%num_ops = rhs%num_ops
                       lhs%KSRep = rhs%KSRep
                       lhs%OrbitalRep = rhs%OrbitalRep
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetWannier90symadaptedKsrepExtents(self) result(res)
                class(Wannier90SymAdapted), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%num_bands),extent(1,self%num_bands),extent(1,self%num_ops),extent(1,self%num_k_irr)]
        end function
        function GetWannier90symadaptedOrbitalrepExtents(self) result(res)
                class(Wannier90SymAdapted), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%num_wann),extent(1,self%num_wann),extent(1,self%num_ops),extent(1,self%num_k_irr)]
        end function
        subroutine AllocateWannier90SymAdaptedObjectFields(self)
                class(Wannier90SymAdapted), intent(inout) :: self
                call self%Wannier90Bounds%AllocateObjectFields()
                call self%KSRep%init(int(self%num_bands),int(self%num_bands),int(self%num_ops),int(self%num_k_irr))
                call self%OrbitalRep%init(int(self%num_wann),int(self%num_wann),int(self%num_ops),int(self%num_k_irr))
        end subroutine


        subroutine InitWannier90Workspace(self)
                class(Wannier90Workspace), intent(inout) :: self
                call self%Wannier90Bounds%Init()
                self%num_nnmax =  int(12,kind=int32)
                call self%dis_window%InitPersistent()
                call self%overlap%InitPersistent()
        end subroutine
        subroutine StoreWannier90WorkspaceObjectFields(self)
                class(Wannier90Workspace), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Wannier90Bounds%StoreObjectFields()
                call self%dis_window%store(ps%FetchSubGroup(gid,  'dis_window'))
                call self%k_lat%StoreObject(ps, gid,  'k_lat')
                call self%nnlist%StoreObject(ps, gid,  'nnlist')
                call self%nncell%StoreObject(ps, gid,  'nncell')
                call self%eigenvalues%StoreObject(ps, gid,  'eigenvalues')
                call self%overlap%store(ps%FetchSubGroup(gid,  'overlap'))
                call self%U%StoreObject(ps, gid,  'U')
                call self%U_opt%StoreObject(ps, gid,  'U_opt')
                call self%M0%StoreObject(ps, gid,  'M0')
        end subroutine
        subroutine LoadWannier90WorkspaceObjectFields(self)
                class(Wannier90Workspace), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Wannier90Bounds%LoadObjectFields()
                call self%dis_window%load(ps%FetchSubGroup(gid,  'dis_window'))
                call self%k_lat%LoadObject(ps, gid,  'k_lat')
                call self%nnlist%LoadObject(ps, gid,  'nnlist')
                call self%nncell%LoadObject(ps, gid,  'nncell')
                call self%eigenvalues%LoadObject(ps, gid,  'eigenvalues')
                call self%overlap%load(ps%FetchSubGroup(gid,  'overlap'))
                call self%U%LoadObject(ps, gid,  'U')
                call self%U_opt%LoadObject(ps, gid,  'U_opt')
                call self%M0%LoadObject(ps, gid,  'M0')
        end subroutine
        subroutine ResetWannier90WorkspaceSectionFields(self)
                class(Wannier90Workspace), intent(inout) :: self
                call self%Wannier90Bounds%ResetSectionFields()
                self%k_lat_ => self%k_lat%GetWithExtents(self%GetWannier90Workspacek_latExtents())
                self%nnlist_ => self%nnlist%GetWithExtents(self%GetWannier90WorkspacennlistExtents())
                self%nncell_ => self%nncell%GetWithExtents(self%GetWannier90WorkspacenncellExtents())
                self%eigenvalues_ => self%eigenvalues%GetWithExtents(self%GetWannier90WorkspaceeigenvaluesExtents())
                self%U_ => self%U%GetWithExtents(self%GetWannier90WorkspaceUExtents())
                self%U_opt_ => self%U_opt%GetWithExtents(self%GetWannier90WorkspaceU_optExtents())
                self%M0_ => self%M0%GetWithExtents(self%GetWannier90WorkspaceM0Extents())
        end subroutine
        subroutine DisconnectWannier90WorkspaceObjectFields(self)
                class(Wannier90Workspace), intent(inout) :: self
               type(iterator) :: iter
                call self%Wannier90Bounds%DisconnectObjectFields()
                call self%dis_window%disconnect()
                call self%k_lat%DisconnectFromStore()
                call self%nnlist%DisconnectFromStore()
                call self%nncell%DisconnectFromStore()
                call self%eigenvalues%DisconnectFromStore()
                call self%overlap%disconnect()
                call self%U%DisconnectFromStore()
                call self%U_opt%DisconnectFromStore()
                call self%M0%DisconnectFromStore()
        end subroutine
        subroutine StoreWannier90WorkspaceScalarFields(self)
                class(Wannier90Workspace), intent(inout) :: self
                call self%Wannier90Bounds%StoreScalarFields()
                call self%write('num_nnmax', self%num_nnmax)
        end subroutine
        subroutine LoadWannier90WorkspaceScalarFields(self)
                class(Wannier90Workspace), intent(inout) :: self
                call self%Wannier90Bounds%LoadScalarFields()
                call self%read('num_nnmax', self%num_nnmax)
        end subroutine
        subroutine FinalizeWannier90Workspace(self)
               type(Wannier90Workspace), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWannier90Workspace(self)
                class(Wannier90Workspace), intent(inout) :: self
                type(Wannier90Workspace), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWannier90WorkspaceEqual(lhs, rhs) result(iseq)
                class(Wannier90Workspace), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Wannier90Workspace)
                       iseq = iseq .and. (lhs%Wannier90Bounds == rhs%Wannier90Bounds)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_nnmax == rhs%num_nnmax)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dis_window == rhs%dis_window)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%k_lat == rhs%k_lat)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nnlist == rhs%nnlist)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nncell == rhs%nncell)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%eigenvalues == rhs%eigenvalues)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%overlap == rhs%overlap)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%U == rhs%U)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%U_opt == rhs%U_opt)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M0 == rhs%M0)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWannier90Workspace(lhs, rhs)
                class(Wannier90Workspace), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Wannier90Workspace)
                       lhs%Wannier90Bounds = rhs%Wannier90Bounds
                       lhs%num_nnmax = rhs%num_nnmax
                       lhs%dis_window = rhs%dis_window
                       lhs%k_lat = rhs%k_lat
                       lhs%nnlist = rhs%nnlist
                       lhs%nncell = rhs%nncell
                       lhs%eigenvalues = rhs%eigenvalues
                       lhs%overlap = rhs%overlap
                       lhs%U = rhs%U
                       lhs%U_opt = rhs%U_opt
                       lhs%M0 = rhs%M0
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetWannier90workspaceK_latExtents(self) result(res)
                class(Wannier90Workspace), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_k_all)]
        end function
        function GetWannier90workspaceNnlistExtents(self) result(res)
                class(Wannier90Workspace), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_k_all),extent(1,self%num_nnmax)]
        end function
        function GetWannier90workspaceNncellExtents(self) result(res)
                class(Wannier90Workspace), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,3),extent(1,self%num_k_all),extent(1,self%num_nnmax)]
        end function
        function GetWannier90workspaceEigenvaluesExtents(self) result(res)
                class(Wannier90Workspace), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_bands),extent(1,self%num_k_all)]
        end function
        function GetWannier90workspaceUExtents(self) result(res)
                class(Wannier90Workspace), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_wann),extent(1,self%num_wann),extent(1,self%num_k_all)]
        end function
        function GetWannier90workspaceU_optExtents(self) result(res)
                class(Wannier90Workspace), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_bands),extent(1,self%num_wann),extent(1,self%num_k_all)]
        end function
        function GetWannier90workspaceM0Extents(self) result(res)
                class(Wannier90Workspace), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_bands),extent(1,self%num_bands),extent(1,self%num_k_all)]
        end function
        subroutine AllocateWannier90WorkspaceObjectFields(self)
                class(Wannier90Workspace), intent(inout) :: self
                call self%Wannier90Bounds%AllocateObjectFields()
                call self%k_lat%init(int(3),int(self%num_k_all))
                call self%nnlist%init(int(self%num_k_all),int(self%num_nnmax))
                call self%nncell%init(int(3),int(self%num_k_all),int(self%num_nnmax))
                call self%eigenvalues%init(int(self%num_bands),int(self%num_k_all))
                call self%U%init(int(self%num_wann),int(self%num_wann),int(self%num_k_all))
                call self%U_opt%init(int(self%num_bands),int(self%num_wann),int(self%num_k_all))
                call self%M0%init(int(self%num_bands),int(self%num_bands),int(self%num_k_all))
        end subroutine



end module
