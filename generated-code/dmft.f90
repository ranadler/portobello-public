
module dmft
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use PEScf
    use array4
    use cube
    use self_energies
    use vector


    implicit none
    public

    type, extends(persistent) :: DMFTFreeEnergy
        integer(kind=int32)  ::  num_temperatures =  0

        type(vector_real)  ::  temperatures
        real(kind=dp),pointer :: temperatures_(:)
        type(vector_real)  ::  internal_energies
        real(kind=dp),pointer :: internal_energies_(:)
        real(kind=dp)  ::  entropy =  0.0d0

        real(kind=dp)  ::  result =  0.0d0

        logical  ::  ready = .False.



        contains
        procedure :: AllocateObjectFields => AllocateDMFTFreeEnergyObjectFields
        procedure :: ResetSectionFields   => ResetDMFTFreeEnergySectionFields
        procedure :: StoreScalarFields    => StoreDMFTFreeEnergyScalarFields
        procedure :: StoreObjectFields    => StoreDMFTFreeEnergyObjectFields
        procedure :: LoadScalarFields     => LoadDMFTFreeEnergyScalarFields
        procedure :: LoadObjectFields     => LoadDMFTFreeEnergyObjectFields
        procedure :: DisconnectObjectFields => DisconnectDMFTFreeEnergyObjectFields
        procedure :: IsEqual              => IsDMFTFreeEnergyEqual
        procedure :: AssignmentOperator   => AssignmentOperatorDMFTFreeEnergy
        procedure :: clear                => ClearDMFTFreeEnergy
        procedure :: init => InitDMFTFreeEnergy
#ifndef __GFORTRAN__
        final     :: FinalizeDMFTFreeEnergy
#endif
        procedure :: GetDmftfreeenergyTemperaturesExtents
        procedure :: GetDmftfreeenergyInternal_energiesExtents

    end type

    type, extends(SolverState) :: DMFTState
        type(LocalOmegaFunction)  ::  sig
        type(LocalOmegaFunction)  ::  hyb
        type(LocalOmegaFunction)  ::  GImp
        integer(kind=int32)  ::  num_freqs =  0

        real(kind=dp)  ::  sign =  0.0d0

        real(kind=dp)  ::  measurement_time =  0.0d0



        contains
        procedure :: AllocateObjectFields => AllocateDMFTStateObjectFields
        procedure :: ResetSectionFields   => ResetDMFTStateSectionFields
        procedure :: StoreScalarFields    => StoreDMFTStateScalarFields
        procedure :: StoreObjectFields    => StoreDMFTStateObjectFields
        procedure :: LoadScalarFields     => LoadDMFTStateScalarFields
        procedure :: LoadObjectFields     => LoadDMFTStateObjectFields
        procedure :: DisconnectObjectFields => DisconnectDMFTStateObjectFields
        procedure :: IsEqual              => IsDMFTStateEqual
        procedure :: AssignmentOperator   => AssignmentOperatorDMFTState
        procedure :: clear                => ClearDMFTState
        procedure :: init => InitDMFTState
#ifndef __GFORTRAN__
        final     :: FinalizeDMFTState
#endif

    end type

    type, extends(SolverProblem) :: ImpurityProblem
        type(string)  ::  os_dir
        integer(kind=int32)  ::  num_freqs =  0

        type(string)  ::  hamiltonian_approximation
        real(kind=dp)  ::  max_sampling_energy =  real(10.000000,kind=16)

        integer(kind=int32)  ::  two_body_size =  0

        type(vector_complex)  ::  two_body
        complex(kind=dp),pointer :: two_body_(:)
        type(LocalOmegaFunction)  ::  hyb


        contains
        procedure :: AllocateObjectFields => AllocateImpurityProblemObjectFields
        procedure :: ResetSectionFields   => ResetImpurityProblemSectionFields
        procedure :: StoreScalarFields    => StoreImpurityProblemScalarFields
        procedure :: StoreObjectFields    => StoreImpurityProblemObjectFields
        procedure :: LoadScalarFields     => LoadImpurityProblemScalarFields
        procedure :: LoadObjectFields     => LoadImpurityProblemObjectFields
        procedure :: DisconnectObjectFields => DisconnectImpurityProblemObjectFields
        procedure :: IsEqual              => IsImpurityProblemEqual
        procedure :: AssignmentOperator   => AssignmentOperatorImpurityProblem
        procedure :: clear                => ClearImpurityProblem
        procedure :: init => InitImpurityProblem
#ifndef __GFORTRAN__
        final     :: FinalizeImpurityProblem
#endif
        procedure :: GetImpurityproblemTwo_bodyExtents

    end type

    type, extends(SolverSolution) :: ImpurityMeasurement
        integer(kind=int32)  ::  num_freqs =  0

        integer(kind=int32)  ::  dim =  0

        integer(kind=int32)  ::  num_si =  0

        integer(kind=int32)  ::  num_orbs =  0

        integer(kind=int32)  ::  num_x =  int(20,kind=int32)

        type(cube_complex)  ::  RhoImp
        complex(kind=dp),pointer :: RhoImp_(:,:,:)
        type(LocalOmegaFunction)  ::  GImp
        type(LocalOmegaFunction)  ::  Sigma
        type(LocalOmegaFunction)  ::  OccSusc
        type(array4_complex)  ::  CoVariance
        complex(kind=dp),pointer :: CoVariance_(:,:,:,:)
        type(LocalOmegaFunction)  ::  GAux
        real(kind=dp)  ::  sign =  0.0d0

        real(kind=dp)  ::  lnZ =  0.0d0

        real(kind=dp)  ::  k =  0.0d0

        integer(kind=int32)  ::  num_samples =  0

        type(vector_real)  ::  expansion_histogram
        real(kind=dp),pointer :: expansion_histogram_(:)
        real(kind=dp)  ::  relative_error =  0.0d0



        contains
        procedure :: AllocateObjectFields => AllocateImpurityMeasurementObjectFields
        procedure :: ResetSectionFields   => ResetImpurityMeasurementSectionFields
        procedure :: StoreScalarFields    => StoreImpurityMeasurementScalarFields
        procedure :: StoreObjectFields    => StoreImpurityMeasurementObjectFields
        procedure :: LoadScalarFields     => LoadImpurityMeasurementScalarFields
        procedure :: LoadObjectFields     => LoadImpurityMeasurementObjectFields
        procedure :: DisconnectObjectFields => DisconnectImpurityMeasurementObjectFields
        procedure :: IsEqual              => IsImpurityMeasurementEqual
        procedure :: AssignmentOperator   => AssignmentOperatorImpurityMeasurement
        procedure :: clear                => ClearImpurityMeasurement
        procedure :: init => InitImpurityMeasurement
#ifndef __GFORTRAN__
        final     :: FinalizeImpurityMeasurement
#endif
        procedure :: GetImpuritymeasurementRhoimpExtents
        procedure :: GetImpuritymeasurementCovarianceExtents
        procedure :: GetImpuritymeasurementExpansion_histogramExtents

    end type

    contains
        subroutine InitDMFTFreeEnergy(self)
                class(DMFTFreeEnergy), intent(inout) :: self
                call self%InitPersistent()
                self%num_temperatures =  0
                self%entropy =  0.0d0
                self%result =  0.0d0
                self%ready = .False.
        end subroutine
        subroutine StoreDMFTFreeEnergyObjectFields(self)
                class(DMFTFreeEnergy), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%temperatures%StoreObject(ps, gid,  'temperatures')
                call self%internal_energies%StoreObject(ps, gid,  'internal_energies')
        end subroutine
        subroutine LoadDMFTFreeEnergyObjectFields(self)
                class(DMFTFreeEnergy), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%temperatures%LoadObject(ps, gid,  'temperatures')
                call self%internal_energies%LoadObject(ps, gid,  'internal_energies')
        end subroutine
        subroutine ResetDMFTFreeEnergySectionFields(self)
                class(DMFTFreeEnergy), intent(inout) :: self
                self%temperatures_ => self%temperatures%GetWithExtents(self%GetDMFTFreeEnergytemperaturesExtents())
                self%internal_energies_ => self%internal_energies%GetWithExtents(self%GetDMFTFreeEnergyinternal_energiesExtents())
        end subroutine
        subroutine DisconnectDMFTFreeEnergyObjectFields(self)
                class(DMFTFreeEnergy), intent(inout) :: self
               type(iterator) :: iter
                call self%temperatures%DisconnectFromStore()
                call self%internal_energies%DisconnectFromStore()
        end subroutine
        subroutine StoreDMFTFreeEnergyScalarFields(self)
                class(DMFTFreeEnergy), intent(inout) :: self
                call self%write('num_temperatures', self%num_temperatures)
                call self%write('entropy', self%entropy)
                call self%write('result', self%result)
                call self%write('ready', self%ready)
        end subroutine
        subroutine LoadDMFTFreeEnergyScalarFields(self)
                class(DMFTFreeEnergy), intent(inout) :: self
                call self%read('num_temperatures', self%num_temperatures)
                call self%read('entropy', self%entropy)
                call self%read('result', self%result)
                call self%read('ready', self%ready)
        end subroutine
        subroutine FinalizeDMFTFreeEnergy(self)
               type(DMFTFreeEnergy), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearDMFTFreeEnergy(self)
                class(DMFTFreeEnergy), intent(inout) :: self
                type(DMFTFreeEnergy), save :: empty
                self = empty
        end subroutine
        pure elemental function IsDMFTFreeEnergyEqual(lhs, rhs) result(iseq)
                class(DMFTFreeEnergy), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (DMFTFreeEnergy)
                       iseq = iseq .and. (lhs%num_temperatures == rhs%num_temperatures)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%temperatures == rhs%temperatures)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%internal_energies == rhs%internal_energies)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%entropy == rhs%entropy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%result == rhs%result)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ready .eqv. rhs%ready)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorDMFTFreeEnergy(lhs, rhs)
                class(DMFTFreeEnergy), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (DMFTFreeEnergy)
                       lhs%num_temperatures = rhs%num_temperatures
                       lhs%temperatures = rhs%temperatures
                       lhs%internal_energies = rhs%internal_energies
                       lhs%entropy = rhs%entropy
                       lhs%result = rhs%result
                       lhs%ready = rhs%ready
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetDmftfreeenergyTemperaturesExtents(self) result(res)
                class(DMFTFreeEnergy), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_temperatures)]
        end function
        function GetDmftfreeenergyInternal_energiesExtents(self) result(res)
                class(DMFTFreeEnergy), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_temperatures)]
        end function
        subroutine AllocateDMFTFreeEnergyObjectFields(self)
                class(DMFTFreeEnergy), intent(inout) :: self
                call self%temperatures%init(int(self%num_temperatures))
                call self%internal_energies%init(int(self%num_temperatures))
        end subroutine


        subroutine InitDMFTState(self)
                class(DMFTState), intent(inout) :: self
                call self%SolverState%Init()
                call self%sig%InitPersistent()
                call self%hyb%InitPersistent()
                call self%GImp%InitPersistent()
                self%num_freqs =  0
                self%sign =  0.0d0
                self%measurement_time =  0.0d0
        end subroutine
        subroutine StoreDMFTStateObjectFields(self)
                class(DMFTState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverState%StoreObjectFields()
                call self%sig%store(ps%FetchSubGroup(gid,  'sig'))
                call self%hyb%store(ps%FetchSubGroup(gid,  'hyb'))
                call self%GImp%store(ps%FetchSubGroup(gid,  'GImp'))
        end subroutine
        subroutine LoadDMFTStateObjectFields(self)
                class(DMFTState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverState%LoadObjectFields()
                call self%sig%load(ps%FetchSubGroup(gid,  'sig'))
                call self%hyb%load(ps%FetchSubGroup(gid,  'hyb'))
                call self%GImp%load(ps%FetchSubGroup(gid,  'GImp'))
        end subroutine
        subroutine ResetDMFTStateSectionFields(self)
                class(DMFTState), intent(inout) :: self
                call self%SolverState%ResetSectionFields()
        end subroutine
        subroutine DisconnectDMFTStateObjectFields(self)
                class(DMFTState), intent(inout) :: self
               type(iterator) :: iter
                call self%SolverState%DisconnectObjectFields()
                call self%sig%disconnect()
                call self%hyb%disconnect()
                call self%GImp%disconnect()
        end subroutine
        subroutine StoreDMFTStateScalarFields(self)
                class(DMFTState), intent(inout) :: self
                call self%SolverState%StoreScalarFields()
                call self%write('num_freqs', self%num_freqs)
                call self%write('sign', self%sign)
                call self%write('measurement_time', self%measurement_time)
        end subroutine
        subroutine LoadDMFTStateScalarFields(self)
                class(DMFTState), intent(inout) :: self
                call self%SolverState%LoadScalarFields()
                call self%read('num_freqs', self%num_freqs)
                call self%read('sign', self%sign)
                call self%read('measurement_time', self%measurement_time)
        end subroutine
        subroutine FinalizeDMFTState(self)
               type(DMFTState), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearDMFTState(self)
                class(DMFTState), intent(inout) :: self
                type(DMFTState), save :: empty
                self = empty
        end subroutine
        pure elemental function IsDMFTStateEqual(lhs, rhs) result(iseq)
                class(DMFTState), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (DMFTState)
                       iseq = iseq .and. (lhs%SolverState == rhs%SolverState)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sig == rhs%sig)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hyb == rhs%hyb)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%GImp == rhs%GImp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_freqs == rhs%num_freqs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sign == rhs%sign)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%measurement_time == rhs%measurement_time)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorDMFTState(lhs, rhs)
                class(DMFTState), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (DMFTState)
                       lhs%SolverState = rhs%SolverState
                       lhs%sig = rhs%sig
                       lhs%hyb = rhs%hyb
                       lhs%GImp = rhs%GImp
                       lhs%num_freqs = rhs%num_freqs
                       lhs%sign = rhs%sign
                       lhs%measurement_time = rhs%measurement_time
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateDMFTStateObjectFields(self)
                class(DMFTState), intent(inout) :: self
                call self%SolverState%AllocateObjectFields()
        end subroutine


        subroutine InitImpurityProblem(self)
                class(ImpurityProblem), intent(inout) :: self
                call self%SolverProblem%Init()
                self%os_dir = "./Impurity"
                self%num_freqs =  0
                self%hamiltonian_approximation = "ising"
                self%max_sampling_energy =  real(10.000000,kind=16)
                self%two_body_size =  0
                call self%hyb%InitPersistent()
        end subroutine
        subroutine StoreImpurityProblemObjectFields(self)
                class(ImpurityProblem), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverProblem%StoreObjectFields()
                call self%two_body%StoreObject(ps, gid,  'two_body')
                call self%hyb%store(ps%FetchSubGroup(gid,  'hyb'))
        end subroutine
        subroutine LoadImpurityProblemObjectFields(self)
                class(ImpurityProblem), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverProblem%LoadObjectFields()
                call self%two_body%LoadObject(ps, gid,  'two_body')
                call self%hyb%load(ps%FetchSubGroup(gid,  'hyb'))
        end subroutine
        subroutine ResetImpurityProblemSectionFields(self)
                class(ImpurityProblem), intent(inout) :: self
                call self%SolverProblem%ResetSectionFields()
                self%two_body_ => self%two_body%GetWithExtents(self%GetImpurityProblemtwo_bodyExtents())
        end subroutine
        subroutine DisconnectImpurityProblemObjectFields(self)
                class(ImpurityProblem), intent(inout) :: self
               type(iterator) :: iter
                call self%SolverProblem%DisconnectObjectFields()
                call self%two_body%DisconnectFromStore()
                call self%hyb%disconnect()
        end subroutine
        subroutine StoreImpurityProblemScalarFields(self)
                class(ImpurityProblem), intent(inout) :: self
                call self%SolverProblem%StoreScalarFields()
                call self%write('os_dir', self%os_dir)
                call self%write('num_freqs', self%num_freqs)
                call self%write('hamiltonian_approximation', self%hamiltonian_approximation)
                call self%write('max_sampling_energy', self%max_sampling_energy)
                call self%write('two_body_size', self%two_body_size)
        end subroutine
        subroutine LoadImpurityProblemScalarFields(self)
                class(ImpurityProblem), intent(inout) :: self
                call self%SolverProblem%LoadScalarFields()
                call self%read('os_dir', self%os_dir)
                call self%read('num_freqs', self%num_freqs)
                call self%read('hamiltonian_approximation', self%hamiltonian_approximation)
                call self%read('max_sampling_energy', self%max_sampling_energy)
                call self%read('two_body_size', self%two_body_size)
        end subroutine
        subroutine FinalizeImpurityProblem(self)
               type(ImpurityProblem), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearImpurityProblem(self)
                class(ImpurityProblem), intent(inout) :: self
                type(ImpurityProblem), save :: empty
                self = empty
        end subroutine
        pure elemental function IsImpurityProblemEqual(lhs, rhs) result(iseq)
                class(ImpurityProblem), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ImpurityProblem)
                       iseq = iseq .and. (lhs%SolverProblem == rhs%SolverProblem)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%os_dir == rhs%os_dir)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_freqs == rhs%num_freqs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hamiltonian_approximation == rhs%hamiltonian_approximation)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_sampling_energy == rhs%max_sampling_energy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%two_body_size == rhs%two_body_size)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%two_body == rhs%two_body)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hyb == rhs%hyb)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorImpurityProblem(lhs, rhs)
                class(ImpurityProblem), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ImpurityProblem)
                       lhs%SolverProblem = rhs%SolverProblem
                       lhs%os_dir = rhs%os_dir
                       lhs%num_freqs = rhs%num_freqs
                       lhs%hamiltonian_approximation = rhs%hamiltonian_approximation
                       lhs%max_sampling_energy = rhs%max_sampling_energy
                       lhs%two_body_size = rhs%two_body_size
                       lhs%two_body = rhs%two_body
                       lhs%hyb = rhs%hyb
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetImpurityproblemTwo_bodyExtents(self) result(res)
                class(ImpurityProblem), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%two_body_size)]
        end function
        subroutine AllocateImpurityProblemObjectFields(self)
                class(ImpurityProblem), intent(inout) :: self
                call self%SolverProblem%AllocateObjectFields()
                call self%two_body%init(int(self%two_body_size))
        end subroutine


        subroutine InitImpurityMeasurement(self)
                class(ImpurityMeasurement), intent(inout) :: self
                call self%SolverSolution%Init()
                self%num_freqs =  0
                self%dim =  0
                self%num_si =  0
                self%num_orbs =  0
                self%num_x =  int(20,kind=int32)
                call self%GImp%InitPersistent()
                call self%Sigma%InitPersistent()
                call self%OccSusc%InitPersistent()
                call self%GAux%InitPersistent()
                self%sign =  0.0d0
                self%lnZ =  0.0d0
                self%k =  0.0d0
                self%num_samples =  0
                self%relative_error =  0.0d0
        end subroutine
        subroutine StoreImpurityMeasurementObjectFields(self)
                class(ImpurityMeasurement), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverSolution%StoreObjectFields()
                call self%RhoImp%StoreObject(ps, gid,  'RhoImp')
                call self%GImp%store(ps%FetchSubGroup(gid,  'GImp'))
                call self%Sigma%store(ps%FetchSubGroup(gid,  'Sigma'))
                call self%OccSusc%store(ps%FetchSubGroup(gid,  'OccSusc'))
                call self%CoVariance%StoreObject(ps, gid,  'CoVariance')
                call self%GAux%store(ps%FetchSubGroup(gid,  'GAux'))
                call self%expansion_histogram%StoreObject(ps, gid,  'expansion_histogram')
        end subroutine
        subroutine LoadImpurityMeasurementObjectFields(self)
                class(ImpurityMeasurement), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%SolverSolution%LoadObjectFields()
                call self%RhoImp%LoadObject(ps, gid,  'RhoImp')
                call self%GImp%load(ps%FetchSubGroup(gid,  'GImp'))
                call self%Sigma%load(ps%FetchSubGroup(gid,  'Sigma'))
                call self%OccSusc%load(ps%FetchSubGroup(gid,  'OccSusc'))
                call self%CoVariance%LoadObject(ps, gid,  'CoVariance')
                call self%GAux%load(ps%FetchSubGroup(gid,  'GAux'))
                call self%expansion_histogram%LoadObject(ps, gid,  'expansion_histogram')
        end subroutine
        subroutine ResetImpurityMeasurementSectionFields(self)
                class(ImpurityMeasurement), intent(inout) :: self
                call self%SolverSolution%ResetSectionFields()
                self%RhoImp_ => self%RhoImp%GetWithExtents(self%GetImpurityMeasurementRhoImpExtents())
                self%CoVariance_ => self%CoVariance%GetWithExtents(self%GetImpurityMeasurementCoVarianceExtents())
                self%expansion_histogram_ => self%expansion_histogram%GetWithExtents(self%GetImpurityMeasurementexpansion_histogramExtents())
        end subroutine
        subroutine DisconnectImpurityMeasurementObjectFields(self)
                class(ImpurityMeasurement), intent(inout) :: self
               type(iterator) :: iter
                call self%SolverSolution%DisconnectObjectFields()
                call self%RhoImp%DisconnectFromStore()
                call self%GImp%disconnect()
                call self%Sigma%disconnect()
                call self%OccSusc%disconnect()
                call self%CoVariance%DisconnectFromStore()
                call self%GAux%disconnect()
                call self%expansion_histogram%DisconnectFromStore()
        end subroutine
        subroutine StoreImpurityMeasurementScalarFields(self)
                class(ImpurityMeasurement), intent(inout) :: self
                call self%SolverSolution%StoreScalarFields()
                call self%write('num_freqs', self%num_freqs)
                call self%write('dim', self%dim)
                call self%write('num_si', self%num_si)
                call self%write('num_orbs', self%num_orbs)
                call self%write('num_x', self%num_x)
                call self%write('sign', self%sign)
                call self%write('lnZ', self%lnZ)
                call self%write('k', self%k)
                call self%write('num_samples', self%num_samples)
                call self%write('relative_error', self%relative_error)
        end subroutine
        subroutine LoadImpurityMeasurementScalarFields(self)
                class(ImpurityMeasurement), intent(inout) :: self
                call self%SolverSolution%LoadScalarFields()
                call self%read('num_freqs', self%num_freqs)
                call self%read('dim', self%dim)
                call self%read('num_si', self%num_si)
                call self%read('num_orbs', self%num_orbs)
                call self%read('num_x', self%num_x)
                call self%read('sign', self%sign)
                call self%read('lnZ', self%lnZ)
                call self%read('k', self%k)
                call self%read('num_samples', self%num_samples)
                call self%read('relative_error', self%relative_error)
        end subroutine
        subroutine FinalizeImpurityMeasurement(self)
               type(ImpurityMeasurement), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearImpurityMeasurement(self)
                class(ImpurityMeasurement), intent(inout) :: self
                type(ImpurityMeasurement), save :: empty
                self = empty
        end subroutine
        pure elemental function IsImpurityMeasurementEqual(lhs, rhs) result(iseq)
                class(ImpurityMeasurement), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ImpurityMeasurement)
                       iseq = iseq .and. (lhs%SolverSolution == rhs%SolverSolution)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_freqs == rhs%num_freqs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dim == rhs%dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_orbs == rhs%num_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_x == rhs%num_x)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%RhoImp == rhs%RhoImp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%GImp == rhs%GImp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Sigma == rhs%Sigma)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%OccSusc == rhs%OccSusc)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%CoVariance == rhs%CoVariance)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%GAux == rhs%GAux)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sign == rhs%sign)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%lnZ == rhs%lnZ)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%k == rhs%k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_samples == rhs%num_samples)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%expansion_histogram == rhs%expansion_histogram)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%relative_error == rhs%relative_error)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorImpurityMeasurement(lhs, rhs)
                class(ImpurityMeasurement), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ImpurityMeasurement)
                       lhs%SolverSolution = rhs%SolverSolution
                       lhs%num_freqs = rhs%num_freqs
                       lhs%dim = rhs%dim
                       lhs%num_si = rhs%num_si
                       lhs%num_orbs = rhs%num_orbs
                       lhs%num_x = rhs%num_x
                       lhs%RhoImp = rhs%RhoImp
                       lhs%GImp = rhs%GImp
                       lhs%Sigma = rhs%Sigma
                       lhs%OccSusc = rhs%OccSusc
                       lhs%CoVariance = rhs%CoVariance
                       lhs%GAux = rhs%GAux
                       lhs%sign = rhs%sign
                       lhs%lnZ = rhs%lnZ
                       lhs%k = rhs%k
                       lhs%num_samples = rhs%num_samples
                       lhs%expansion_histogram = rhs%expansion_histogram
                       lhs%relative_error = rhs%relative_error
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetImpuritymeasurementRhoimpExtents(self) result(res)
                class(ImpurityMeasurement), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        function GetImpuritymeasurementCovarianceExtents(self) result(res)
                class(ImpurityMeasurement), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%num_x),extent(1,self%num_x),extent(1,self%num_orbs),extent(1,self%num_si)]
        end function
        function GetImpuritymeasurementExpansion_histogramExtents(self) result(res)
                class(ImpurityMeasurement), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_samples)]
        end function
        subroutine AllocateImpurityMeasurementObjectFields(self)
                class(ImpurityMeasurement), intent(inout) :: self
                call self%SolverSolution%AllocateObjectFields()
                call self%RhoImp%init(int(self%dim),int(self%dim),int(self%num_si))
                call self%CoVariance%init(int(self%num_x),int(self%num_x),int(self%num_orbs),int(self%num_si))
                call self%expansion_histogram%init(int(self%num_samples))
        end subroutine



end module
