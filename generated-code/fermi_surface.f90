
module fermi_surface
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use cube


    implicit none
    public

    type, extends(persistent) :: LowEnergyHamiltonian
        integer(kind=int32)  ::  min_band =  0

        integer(kind=int32)  ::  max_band =  0

        integer(kind=int32)  ::  num_k_irr =  0

        integer(kind=int32)  ::  num_si =  0

        type(string)  ::  name
        type(cube_real)  ::  eps
        real(kind=dp),pointer :: eps_(:,:,:)
        type(cube_real)  ::  scalar
        real(kind=dp),pointer :: scalar_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateLowEnergyHamiltonianObjectFields
        procedure :: ResetSectionFields   => ResetLowEnergyHamiltonianSectionFields
        procedure :: StoreScalarFields    => StoreLowEnergyHamiltonianScalarFields
        procedure :: StoreObjectFields    => StoreLowEnergyHamiltonianObjectFields
        procedure :: LoadScalarFields     => LoadLowEnergyHamiltonianScalarFields
        procedure :: LoadObjectFields     => LoadLowEnergyHamiltonianObjectFields
        procedure :: DisconnectObjectFields => DisconnectLowEnergyHamiltonianObjectFields
        procedure :: IsEqual              => IsLowEnergyHamiltonianEqual
        procedure :: AssignmentOperator   => AssignmentOperatorLowEnergyHamiltonian
        procedure :: clear                => ClearLowEnergyHamiltonian
        procedure :: init => InitLowEnergyHamiltonian
#ifndef __GFORTRAN__
        final     :: FinalizeLowEnergyHamiltonian
#endif
        procedure :: GetLowenergyhamiltonianEpsExtents
        procedure :: GetLowenergyhamiltonianScalarExtents

    end type
    interface
         function WriteFRMSFI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    contains
        subroutine InitLowEnergyHamiltonian(self)
                class(LowEnergyHamiltonian), intent(inout) :: self
                call self%InitPersistent()
                self%min_band =  0
                self%max_band =  0
                self%num_k_irr =  0
                self%num_si =  0
        end subroutine
        subroutine StoreLowEnergyHamiltonianObjectFields(self)
                class(LowEnergyHamiltonian), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%eps%StoreObject(ps, gid,  'eps')
                call self%scalar%StoreObject(ps, gid,  'scalar')
        end subroutine
        subroutine LoadLowEnergyHamiltonianObjectFields(self)
                class(LowEnergyHamiltonian), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%eps%LoadObject(ps, gid,  'eps')
                call self%scalar%LoadObject(ps, gid,  'scalar')
        end subroutine
        subroutine ResetLowEnergyHamiltonianSectionFields(self)
                class(LowEnergyHamiltonian), intent(inout) :: self
                self%eps_ => self%eps%GetWithExtents(self%GetLowEnergyHamiltonianepsExtents())
                self%scalar_ => self%scalar%GetWithExtents(self%GetLowEnergyHamiltonianscalarExtents())
        end subroutine
        subroutine DisconnectLowEnergyHamiltonianObjectFields(self)
                class(LowEnergyHamiltonian), intent(inout) :: self
               type(iterator) :: iter
                call self%eps%DisconnectFromStore()
                call self%scalar%DisconnectFromStore()
        end subroutine
        subroutine StoreLowEnergyHamiltonianScalarFields(self)
                class(LowEnergyHamiltonian), intent(inout) :: self
                call self%write('min_band', self%min_band)
                call self%write('max_band', self%max_band)
                call self%write('num_k_irr', self%num_k_irr)
                call self%write('num_si', self%num_si)
                call self%write('name', self%name)
        end subroutine
        subroutine LoadLowEnergyHamiltonianScalarFields(self)
                class(LowEnergyHamiltonian), intent(inout) :: self
                call self%read('min_band', self%min_band)
                call self%read('max_band', self%max_band)
                call self%read('num_k_irr', self%num_k_irr)
                call self%read('num_si', self%num_si)
                call self%read('name', self%name)
        end subroutine
        subroutine FinalizeLowEnergyHamiltonian(self)
               type(LowEnergyHamiltonian), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearLowEnergyHamiltonian(self)
                class(LowEnergyHamiltonian), intent(inout) :: self
                type(LowEnergyHamiltonian), save :: empty
                self = empty
        end subroutine
        pure elemental function IsLowEnergyHamiltonianEqual(lhs, rhs) result(iseq)
                class(LowEnergyHamiltonian), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (LowEnergyHamiltonian)
                       iseq = iseq .and. (lhs%min_band == rhs%min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_band == rhs%max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_irr == rhs%num_k_irr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%name == rhs%name)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%eps == rhs%eps)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%scalar == rhs%scalar)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorLowEnergyHamiltonian(lhs, rhs)
                class(LowEnergyHamiltonian), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (LowEnergyHamiltonian)
                       lhs%min_band = rhs%min_band
                       lhs%max_band = rhs%max_band
                       lhs%num_k_irr = rhs%num_k_irr
                       lhs%num_si = rhs%num_si
                       lhs%name = rhs%name
                       lhs%eps = rhs%eps
                       lhs%scalar = rhs%scalar
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetLowenergyhamiltonianEpsExtents(self) result(res)
                class(LowEnergyHamiltonian), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(self%min_band,self%max_band),extent(1,self%num_k_irr),extent(1,self%num_si)]
        end function
        function GetLowenergyhamiltonianScalarExtents(self) result(res)
                class(LowEnergyHamiltonian), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(self%min_band,self%max_band),extent(1,self%num_k_irr),extent(1,self%num_si)]
        end function
        subroutine AllocateLowEnergyHamiltonianObjectFields(self)
                class(LowEnergyHamiltonian), intent(inout) :: self
                call self%eps%init(int(self%max_band- (self%min_band) + 1),int(self%num_k_irr),int(self%num_si))
                call self%scalar%init(int(self%max_band- (self%min_band) + 1),int(self%num_k_irr),int(self%num_si))
        end subroutine


        subroutine WriteFRMSFWrapper(ret, imp_fp, selfpath) bind(C,name='fermi_surface_mp_writefrmsfwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(WriteFRMSFI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine


end module
