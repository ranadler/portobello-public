
module FlapwMBPT
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use array4
    use array5
    use array7
    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: Control
        integer(kind=int32)  ::  iter_dft =  int(80,kind=int32)

        integer(kind=int32)  ::  iter_hf =  int(0,kind=int32)

        integer(kind=int32)  ::  iter_gw =  int(0,kind=int32)

        integer(kind=int32)  ::  iter_qp =  int(0,kind=int32)

        integer(kind=int32)  ::  iter_psi =  int(0,kind=int32)

        integer(kind=int32)  ::  iter_bsp =  int(0,kind=int32)

        integer(kind=int32)  ::  restart_begin =  int(0,kind=int32)

        integer(kind=int32)  ::  restart_end =  int(0,kind=int32)

        real(kind=dp)  ::  admix =  real(0.050000,kind=16)

        real(kind=dp)  ::  adspin =  real(0.700000,kind=16)

        real(kind=dp)  ::  adm_gw =  real(0.300000,kind=16)

        real(kind=dp)  ::  acc_it_gw =  real(0.600000,kind=16)

        integer(kind=int32)  ::  iexch =  int(5,kind=int32)

        real(kind=dp)  ::  scal_spin =  real(1.000000,kind=16)

        integer(kind=int32)  ::  psi_fncl_use =  int(0,kind=int32)

        integer(kind=int32)  ::  nproc_tau =  int(1,kind=int32)

        integer(kind=int32)  ::  nproc_k =  int(1,kind=int32)

        integer(kind=int32)  ::  nproc_pbr =  int(1,kind=int32)

        integer(kind=int32)  ::  irel =  int(1,kind=int32)

        integer(kind=int32)  ::  irel_core =  int(1,kind=int32)

        real(kind=dp)  ::  clight =  real(274.074000,kind=16)

        logical  ::  rel_interst = .False.

        real(kind=dp)  ::  temperature =  real(900.000000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateControlObjectFields
        procedure :: ResetSectionFields   => ResetControlSectionFields
        procedure :: StoreScalarFields    => StoreControlScalarFields
        procedure :: StoreObjectFields    => StoreControlObjectFields
        procedure :: LoadScalarFields     => LoadControlScalarFields
        procedure :: LoadObjectFields     => LoadControlObjectFields
        procedure :: DisconnectObjectFields => DisconnectControlObjectFields
        procedure :: IsEqual              => IsControlEqual
        procedure :: AssignmentOperator   => AssignmentOperatorControl
        procedure :: clear                => ClearControl
        procedure :: init => InitControl
#ifndef __GFORTRAN__
        final     :: FinalizeControl
#endif

    end type

    type, extends(persistent) :: AtomicData
        type(string)  ::  txtel
        real(kind=dp)  ::  z =  0.0d0

        real(kind=dp)  ::  z_dop =  real(0.000000,kind=16)

        real(kind=dp)  ::  magn_shift =  real(0.000000,kind=16)

        real(kind=dp)  ::  smt =  0.0d0

        real(kind=dp)  ::  h =  real(0.012000,kind=16)

        integer(kind=int32)  ::  nrad =  int(1200,kind=int32)

        integer(kind=int32)  ::  lmb =  0

        integer(kind=int32)  ::  lmpb =  0

        integer(kind=int32)  ::  maxntle =  int(10,kind=int32)

        type(vector_int)  ::  lim_pb_mt
        integer(kind=int32),pointer :: lim_pb_mt_(:)
        type(vector_int)  ::  lim_pb_mt_red
        integer(kind=int32),pointer :: lim_pb_mt_red_(:)
        type(vector_int)  ::  ntle
        integer(kind=int32),pointer :: ntle_(:)
        type(matrix_int)  ::  augm
        integer(kind=int32),pointer :: augm_(:,:)
        type(matrix_real)  ::  atoc
        real(kind=dp),pointer :: atoc_(:,:)
        type(matrix_real)  ::  ptnl
        real(kind=dp),pointer :: ptnl_(:,:)
        type(matrix_int)  ::  correlated
        integer(kind=int32),pointer :: correlated_(:,:)
        type(matrix_real)  ::  idmd
        real(kind=dp),pointer :: idmd_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateAtomicDataObjectFields
        procedure :: ResetSectionFields   => ResetAtomicDataSectionFields
        procedure :: StoreScalarFields    => StoreAtomicDataScalarFields
        procedure :: StoreObjectFields    => StoreAtomicDataObjectFields
        procedure :: LoadScalarFields     => LoadAtomicDataScalarFields
        procedure :: LoadObjectFields     => LoadAtomicDataObjectFields
        procedure :: DisconnectObjectFields => DisconnectAtomicDataObjectFields
        procedure :: IsEqual              => IsAtomicDataEqual
        procedure :: AssignmentOperator   => AssignmentOperatorAtomicData
        procedure :: clear                => ClearAtomicData
        procedure :: init => InitAtomicData
#ifndef __GFORTRAN__
        final     :: FinalizeAtomicData
#endif
        procedure :: GetAtomicdataLim_pb_mtExtents
        procedure :: GetAtomicdataLim_pb_mt_redExtents
        procedure :: GetAtomicdataNtleExtents
        procedure :: GetAtomicdataAugmExtents
        procedure :: GetAtomicdataAtocExtents
        procedure :: GetAtomicdataPtnlExtents
        procedure :: GetAtomicdataCorrelatedExtents
        procedure :: GetAtomicdataIdmdExtents

    end type

    type, extends(persistent) :: Structure
        real(kind=dp)  ::  par =  real(1.000000,kind=16)

        real(kind=dp)  ::  b_a =  real(1.000000,kind=16)

        real(kind=dp)  ::  c_a =  real(1.000000,kind=16)

        integer(kind=int32)  ::  nsort =  0

        integer(kind=int32)  ::  natom =  0

        integer(kind=int32)  ::  istruc =  int(1,kind=int32)

        type(vector_real)  ::  a
        real(kind=dp),pointer :: a_(:)
        type(vector_real)  ::  b
        real(kind=dp),pointer :: b_(:)
        type(vector_real)  ::  c
        real(kind=dp),pointer :: c_(:)
        type(matrix_real)  ::  tau
        real(kind=dp),pointer :: tau_(:,:)
        type(vector_int)  ::  isA
        integer(kind=int32),pointer :: isA_(:)
        type(AtomicData), allocatable  ::  ad(:)


        contains
        procedure :: AllocateObjectFields => AllocateStructureObjectFields
        procedure :: ResetSectionFields   => ResetStructureSectionFields
        procedure :: StoreScalarFields    => StoreStructureScalarFields
        procedure :: StoreObjectFields    => StoreStructureObjectFields
        procedure :: LoadScalarFields     => LoadStructureScalarFields
        procedure :: LoadObjectFields     => LoadStructureObjectFields
        procedure :: DisconnectObjectFields => DisconnectStructureObjectFields
        procedure :: IsEqual              => IsStructureEqual
        procedure :: AssignmentOperator   => AssignmentOperatorStructure
        procedure :: clear                => ClearStructure
        procedure :: init => InitStructure
#ifndef __GFORTRAN__
        final     :: FinalizeStructure
#endif
        procedure :: GetStructureAExtents
        procedure :: GetStructureBExtents
        procedure :: GetStructureCExtents
        procedure :: GetStructureTauExtents
        procedure :: GetStructureIsaExtents

    end type

    type, extends(persistent) :: Generator
        integer(kind=int32)  ::  sign =  int(1,kind=int32)

        type(string)  ::  kind
        type(vector_real)  ::  v
        real(kind=dp),pointer :: v_(:)
        integer(kind=int32)  ::  n =  0

        type(vector_real)  ::  t
        real(kind=dp),pointer :: t_(:)


        contains
        procedure :: AllocateObjectFields => AllocateGeneratorObjectFields
        procedure :: ResetSectionFields   => ResetGeneratorSectionFields
        procedure :: StoreScalarFields    => StoreGeneratorScalarFields
        procedure :: StoreObjectFields    => StoreGeneratorObjectFields
        procedure :: LoadScalarFields     => LoadGeneratorScalarFields
        procedure :: LoadObjectFields     => LoadGeneratorObjectFields
        procedure :: DisconnectObjectFields => DisconnectGeneratorObjectFields
        procedure :: IsEqual              => IsGeneratorEqual
        procedure :: AssignmentOperator   => AssignmentOperatorGenerator
        procedure :: clear                => ClearGenerator
        procedure :: init => InitGenerator
#ifndef __GFORTRAN__
        final     :: FinalizeGenerator
#endif
        procedure :: GetGeneratorVExtents
        procedure :: GetGeneratorTExtents

    end type

    type, extends(persistent) :: SpacegroupGens
        integer(kind=int32)  ::  num_generators =  0

        type(Generator), allocatable  ::  gens(:)
        type(cube_real)  ::  rots
        real(kind=dp),pointer :: rots_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateSpacegroupGensObjectFields
        procedure :: ResetSectionFields   => ResetSpacegroupGensSectionFields
        procedure :: StoreScalarFields    => StoreSpacegroupGensScalarFields
        procedure :: StoreObjectFields    => StoreSpacegroupGensObjectFields
        procedure :: LoadScalarFields     => LoadSpacegroupGensScalarFields
        procedure :: LoadObjectFields     => LoadSpacegroupGensObjectFields
        procedure :: DisconnectObjectFields => DisconnectSpacegroupGensObjectFields
        procedure :: IsEqual              => IsSpacegroupGensEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSpacegroupGens
        procedure :: clear                => ClearSpacegroupGens
        procedure :: init => InitSpacegroupGens
#ifndef __GFORTRAN__
        final     :: FinalizeSpacegroupGens
#endif
        procedure :: GetSpacegroupgensRotsExtents

    end type

    type, extends(persistent) :: KGrid
        type(vector_int)  ::  ndiv
        integer(kind=int32),pointer :: ndiv_(:)
        type(vector_int)  ::  ndiv_c
        integer(kind=int32),pointer :: ndiv_c_(:)
        logical  ::  metal = .False.

        integer(kind=int32)  ::  n_k_div =  int(9,kind=int32)

        type(string)  ::  k_line


        contains
        procedure :: AllocateObjectFields => AllocateKGridObjectFields
        procedure :: ResetSectionFields   => ResetKGridSectionFields
        procedure :: StoreScalarFields    => StoreKGridScalarFields
        procedure :: StoreObjectFields    => StoreKGridObjectFields
        procedure :: LoadScalarFields     => LoadKGridScalarFields
        procedure :: LoadObjectFields     => LoadKGridObjectFields
        procedure :: DisconnectObjectFields => DisconnectKGridObjectFields
        procedure :: IsEqual              => IsKGridEqual
        procedure :: AssignmentOperator   => AssignmentOperatorKGrid
        procedure :: clear                => ClearKGrid
        procedure :: init => InitKGrid
#ifndef __GFORTRAN__
        final     :: FinalizeKGrid
#endif
        procedure :: GetKgridNdivExtents
        procedure :: GetKgridNdiv_cExtents

    end type

    type, extends(persistent) :: RealSpaceMeshes
        type(vector_int)  ::  nrdiv
        integer(kind=int32),pointer :: nrdiv_(:)
        type(vector_int)  ::  mdiv
        integer(kind=int32),pointer :: mdiv_(:)
        type(vector_int)  ::  nrdiv_red
        integer(kind=int32),pointer :: nrdiv_red_(:)


        contains
        procedure :: AllocateObjectFields => AllocateRealSpaceMeshesObjectFields
        procedure :: ResetSectionFields   => ResetRealSpaceMeshesSectionFields
        procedure :: StoreScalarFields    => StoreRealSpaceMeshesScalarFields
        procedure :: StoreObjectFields    => StoreRealSpaceMeshesObjectFields
        procedure :: LoadScalarFields     => LoadRealSpaceMeshesScalarFields
        procedure :: LoadObjectFields     => LoadRealSpaceMeshesObjectFields
        procedure :: DisconnectObjectFields => DisconnectRealSpaceMeshesObjectFields
        procedure :: IsEqual              => IsRealSpaceMeshesEqual
        procedure :: AssignmentOperator   => AssignmentOperatorRealSpaceMeshes
        procedure :: clear                => ClearRealSpaceMeshes
        procedure :: init => InitRealSpaceMeshes
#ifndef __GFORTRAN__
        final     :: FinalizeRealSpaceMeshes
#endif
        procedure :: GetRealspacemeshesNrdivExtents
        procedure :: GetRealspacemeshesMdivExtents
        procedure :: GetRealspacemeshesNrdiv_redExtents

    end type

    type, extends(persistent) :: Basis
        real(kind=dp)  ::  cut_lapw_ratio =  real(0.610000,kind=16)

        real(kind=dp)  ::  cut_pb_ratio =  real(0.980000,kind=16)

        real(kind=dp)  ::  eps_pb =  real(0.001000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateBasisObjectFields
        procedure :: ResetSectionFields   => ResetBasisSectionFields
        procedure :: StoreScalarFields    => StoreBasisScalarFields
        procedure :: StoreObjectFields    => StoreBasisObjectFields
        procedure :: LoadScalarFields     => LoadBasisScalarFields
        procedure :: LoadObjectFields     => LoadBasisObjectFields
        procedure :: DisconnectObjectFields => DisconnectBasisObjectFields
        procedure :: IsEqual              => IsBasisEqual
        procedure :: AssignmentOperator   => AssignmentOperatorBasis
        procedure :: clear                => ClearBasis
        procedure :: init => InitBasis
#ifndef __GFORTRAN__
        final     :: FinalizeBasis
#endif

    end type

    type, extends(persistent) :: Zones
        integer(kind=int32)  ::  nbndf =  int(0,kind=int32)

        type(vector_int)  ::  nbndf_bnd
        integer(kind=int32),pointer :: nbndf_bnd_(:)


        contains
        procedure :: AllocateObjectFields => AllocateZonesObjectFields
        procedure :: ResetSectionFields   => ResetZonesSectionFields
        procedure :: StoreScalarFields    => StoreZonesScalarFields
        procedure :: StoreObjectFields    => StoreZonesObjectFields
        procedure :: LoadScalarFields     => LoadZonesScalarFields
        procedure :: LoadObjectFields     => LoadZonesObjectFields
        procedure :: DisconnectObjectFields => DisconnectZonesObjectFields
        procedure :: IsEqual              => IsZonesEqual
        procedure :: AssignmentOperator   => AssignmentOperatorZones
        procedure :: clear                => ClearZones
        procedure :: init => InitZones
#ifndef __GFORTRAN__
        final     :: FinalizeZones
#endif
        procedure :: GetZonesNbndf_bndExtents

    end type

    type, extends(persistent) :: BandPlot
        integer(kind=int32)  ::  n_k_div =  int(10,kind=int32)



        contains
        procedure :: AllocateObjectFields => AllocateBandPlotObjectFields
        procedure :: ResetSectionFields   => ResetBandPlotSectionFields
        procedure :: StoreScalarFields    => StoreBandPlotScalarFields
        procedure :: StoreObjectFields    => StoreBandPlotObjectFields
        procedure :: LoadScalarFields     => LoadBandPlotScalarFields
        procedure :: LoadObjectFields     => LoadBandPlotObjectFields
        procedure :: DisconnectObjectFields => DisconnectBandPlotObjectFields
        procedure :: IsEqual              => IsBandPlotEqual
        procedure :: AssignmentOperator   => AssignmentOperatorBandPlot
        procedure :: clear                => ClearBandPlot
        procedure :: init => InitBandPlot
#ifndef __GFORTRAN__
        final     :: FinalizeBandPlot
#endif

    end type

    type, extends(persistent) :: DensityOfStates
        real(kind=dp)  ::  emindos =  real(2.000000,kind=16)

        real(kind=dp)  ::  emaxdos =  real(2.000000,kind=16)

        integer(kind=int32)  ::  ndos =  int(600,kind=int32)

        real(kind=dp)  ::  e_small =  real(0.005000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateDensityOfStatesObjectFields
        procedure :: ResetSectionFields   => ResetDensityOfStatesSectionFields
        procedure :: StoreScalarFields    => StoreDensityOfStatesScalarFields
        procedure :: StoreObjectFields    => StoreDensityOfStatesObjectFields
        procedure :: LoadScalarFields     => LoadDensityOfStatesScalarFields
        procedure :: LoadObjectFields     => LoadDensityOfStatesObjectFields
        procedure :: DisconnectObjectFields => DisconnectDensityOfStatesObjectFields
        procedure :: IsEqual              => IsDensityOfStatesEqual
        procedure :: AssignmentOperator   => AssignmentOperatorDensityOfStates
        procedure :: clear                => ClearDensityOfStates
        procedure :: init => InitDensityOfStates
#ifndef __GFORTRAN__
        final     :: FinalizeDensityOfStates
#endif

    end type

    type, extends(persistent) :: MultiSCF
        real(kind=dp)  ::  v_v0 =  real(1.000000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateMultiSCFObjectFields
        procedure :: ResetSectionFields   => ResetMultiSCFSectionFields
        procedure :: StoreScalarFields    => StoreMultiSCFScalarFields
        procedure :: StoreObjectFields    => StoreMultiSCFObjectFields
        procedure :: LoadScalarFields     => LoadMultiSCFScalarFields
        procedure :: LoadObjectFields     => LoadMultiSCFObjectFields
        procedure :: DisconnectObjectFields => DisconnectMultiSCFObjectFields
        procedure :: IsEqual              => IsMultiSCFEqual
        procedure :: AssignmentOperator   => AssignmentOperatorMultiSCF
        procedure :: clear                => ClearMultiSCF
        procedure :: init => InitMultiSCF
#ifndef __GFORTRAN__
        final     :: FinalizeMultiSCF
#endif

    end type

    type, extends(persistent) :: Magnetism
        real(kind=dp)  ::  b_extval =  real(0.000000,kind=16)

        integer(kind=int32)  ::  iter_h_ext =  int(0,kind=int32)

        type(vector_real)  ::  b_ext
        real(kind=dp),pointer :: b_ext_(:)


        contains
        procedure :: AllocateObjectFields => AllocateMagnetismObjectFields
        procedure :: ResetSectionFields   => ResetMagnetismSectionFields
        procedure :: StoreScalarFields    => StoreMagnetismScalarFields
        procedure :: StoreObjectFields    => StoreMagnetismObjectFields
        procedure :: LoadScalarFields     => LoadMagnetismScalarFields
        procedure :: LoadObjectFields     => LoadMagnetismObjectFields
        procedure :: DisconnectObjectFields => DisconnectMagnetismObjectFields
        procedure :: IsEqual              => IsMagnetismEqual
        procedure :: AssignmentOperator   => AssignmentOperatorMagnetism
        procedure :: clear                => ClearMagnetism
        procedure :: init => InitMagnetism
#ifndef __GFORTRAN__
        final     :: FinalizeMagnetism
#endif
        procedure :: GetMagnetismB_extExtents

    end type

    type, extends(persistent) :: Coulomb
        real(kind=dp)  ::  eps_coul =  real(0.000100,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateCoulombObjectFields
        procedure :: ResetSectionFields   => ResetCoulombSectionFields
        procedure :: StoreScalarFields    => StoreCoulombScalarFields
        procedure :: StoreObjectFields    => StoreCoulombObjectFields
        procedure :: LoadScalarFields     => LoadCoulombScalarFields
        procedure :: LoadObjectFields     => LoadCoulombObjectFields
        procedure :: DisconnectObjectFields => DisconnectCoulombObjectFields
        procedure :: IsEqual              => IsCoulombEqual
        procedure :: AssignmentOperator   => AssignmentOperatorCoulomb
        procedure :: clear                => ClearCoulomb
        procedure :: init => InitCoulomb
#ifndef __GFORTRAN__
        final     :: FinalizeCoulomb
#endif

    end type

    type, extends(persistent) :: TauMesh
        integer(kind=int32)  ::  n_tau =  int(46,kind=int32)

        real(kind=dp)  ::  exp_tau_gw =  real(4.000000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateTauMeshObjectFields
        procedure :: ResetSectionFields   => ResetTauMeshSectionFields
        procedure :: StoreScalarFields    => StoreTauMeshScalarFields
        procedure :: StoreObjectFields    => StoreTauMeshObjectFields
        procedure :: LoadScalarFields     => LoadTauMeshScalarFields
        procedure :: LoadObjectFields     => LoadTauMeshObjectFields
        procedure :: DisconnectObjectFields => DisconnectTauMeshObjectFields
        procedure :: IsEqual              => IsTauMeshEqual
        procedure :: AssignmentOperator   => AssignmentOperatorTauMesh
        procedure :: clear                => ClearTauMesh
        procedure :: init => InitTauMesh
#ifndef __GFORTRAN__
        final     :: FinalizeTauMesh
#endif

    end type

    type, extends(persistent) :: OmegaMesh
        integer(kind=int32)  ::  n_omega_exa =  int(10,kind=int32)

        integer(kind=int32)  ::  n_omega_asy =  int(36,kind=int32)

        real(kind=dp)  ::  omega_max =  real(50.000000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateOmegaMeshObjectFields
        procedure :: ResetSectionFields   => ResetOmegaMeshSectionFields
        procedure :: StoreScalarFields    => StoreOmegaMeshScalarFields
        procedure :: StoreObjectFields    => StoreOmegaMeshObjectFields
        procedure :: LoadScalarFields     => LoadOmegaMeshScalarFields
        procedure :: LoadObjectFields     => LoadOmegaMeshObjectFields
        procedure :: DisconnectObjectFields => DisconnectOmegaMeshObjectFields
        procedure :: IsEqual              => IsOmegaMeshEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOmegaMesh
        procedure :: clear                => ClearOmegaMesh
        procedure :: init => InitOmegaMesh
#ifndef __GFORTRAN__
        final     :: FinalizeOmegaMesh
#endif

    end type

    type, extends(persistent) :: NuMesh
        integer(kind=int32)  ::  n_nu_exa =  int(10,kind=int32)

        integer(kind=int32)  ::  n_nu_geom =  int(30,kind=int32)

        integer(kind=int32)  ::  n_nu_asy =  int(6,kind=int32)

        real(kind=dp)  ::  nu_geom =  real(100.000000,kind=16)

        real(kind=dp)  ::  nu_max =  real(400.000000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateNuMeshObjectFields
        procedure :: ResetSectionFields   => ResetNuMeshSectionFields
        procedure :: StoreScalarFields    => StoreNuMeshScalarFields
        procedure :: StoreObjectFields    => StoreNuMeshObjectFields
        procedure :: LoadScalarFields     => LoadNuMeshScalarFields
        procedure :: LoadObjectFields     => LoadNuMeshObjectFields
        procedure :: DisconnectObjectFields => DisconnectNuMeshObjectFields
        procedure :: IsEqual              => IsNuMeshEqual
        procedure :: AssignmentOperator   => AssignmentOperatorNuMesh
        procedure :: clear                => ClearNuMesh
        procedure :: init => InitNuMesh
#ifndef __GFORTRAN__
        final     :: FinalizeNuMesh
#endif

    end type

    type, extends(persistent) :: AFMSymmetry
        real(kind=dp)  ::  magmom =  0.0d0

        integer(kind=int32)  ::  site =  0

        integer(kind=int32)  ::  opposite_site =  0

        type(matrix_real)  ::  op
        real(kind=dp),pointer :: op_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateAFMSymmetryObjectFields
        procedure :: ResetSectionFields   => ResetAFMSymmetrySectionFields
        procedure :: StoreScalarFields    => StoreAFMSymmetryScalarFields
        procedure :: StoreObjectFields    => StoreAFMSymmetryObjectFields
        procedure :: LoadScalarFields     => LoadAFMSymmetryScalarFields
        procedure :: LoadObjectFields     => LoadAFMSymmetryObjectFields
        procedure :: DisconnectObjectFields => DisconnectAFMSymmetryObjectFields
        procedure :: IsEqual              => IsAFMSymmetryEqual
        procedure :: AssignmentOperator   => AssignmentOperatorAFMSymmetry
        procedure :: clear                => ClearAFMSymmetry
        procedure :: init => InitAFMSymmetry
#ifndef __GFORTRAN__
        final     :: FinalizeAFMSymmetry
#endif
        procedure :: GetAfmsymmetryOpExtents

    end type

    type, extends(persistent) :: RhobustaExtra
        logical  ::  has_magnetic_field = .False.

        logical  ::  is_corr_spin_polarized = .False.

        logical  ::  is_dft_spin_polarized = .False.

        logical  ::  local_off_diagonals = .False.

        logical  ::  simplify_diagonal = .False.



        contains
        procedure :: AllocateObjectFields => AllocateRhobustaExtraObjectFields
        procedure :: ResetSectionFields   => ResetRhobustaExtraSectionFields
        procedure :: StoreScalarFields    => StoreRhobustaExtraScalarFields
        procedure :: StoreObjectFields    => StoreRhobustaExtraObjectFields
        procedure :: LoadScalarFields     => LoadRhobustaExtraScalarFields
        procedure :: LoadObjectFields     => LoadRhobustaExtraObjectFields
        procedure :: DisconnectObjectFields => DisconnectRhobustaExtraObjectFields
        procedure :: IsEqual              => IsRhobustaExtraEqual
        procedure :: AssignmentOperator   => AssignmentOperatorRhobustaExtra
        procedure :: clear                => ClearRhobustaExtra
        procedure :: init => InitRhobustaExtra
#ifndef __GFORTRAN__
        final     :: FinalizeRhobustaExtra
#endif

    end type

    type, extends(persistent) :: AFMExtra
        integer(kind=int32)  ::  num_afm =  int(0,kind=int32)

        type(AFMSymmetry), allocatable  ::  afm_symm(:)


        contains
        procedure :: AllocateObjectFields => AllocateAFMExtraObjectFields
        procedure :: ResetSectionFields   => ResetAFMExtraSectionFields
        procedure :: StoreScalarFields    => StoreAFMExtraScalarFields
        procedure :: StoreObjectFields    => StoreAFMExtraObjectFields
        procedure :: LoadScalarFields     => LoadAFMExtraScalarFields
        procedure :: LoadObjectFields     => LoadAFMExtraObjectFields
        procedure :: DisconnectObjectFields => DisconnectAFMExtraObjectFields
        procedure :: IsEqual              => IsAFMExtraEqual
        procedure :: AssignmentOperator   => AssignmentOperatorAFMExtra
        procedure :: clear                => ClearAFMExtra
        procedure :: init => InitAFMExtra
#ifndef __GFORTRAN__
        final     :: FinalizeAFMExtra
#endif

    end type

    type, extends(persistent) :: Input
        type(string)  ::  text
        type(string)  ::  allfile
        type(Control)  ::  ctrl
        type(Structure)  ::  strct
        type(SpacegroupGens)  ::  sgg
        type(RealSpaceMeshes)  ::  rsm
        type(Basis)  ::  bas
        type(Zones)  ::  zns
        type(BandPlot)  ::  bp
        type(DensityOfStates)  ::  dos
        type(KGrid)  ::  kg
        type(MultiSCF)  ::  mscf
        type(Magnetism)  ::  mag
        type(Coulomb)  ::  coul
        type(TauMesh)  ::  tmesh
        type(OmegaMesh)  ::  omesh
        type(NuMesh)  ::  nmesh


        contains
        procedure :: AllocateObjectFields => AllocateInputObjectFields
        procedure :: ResetSectionFields   => ResetInputSectionFields
        procedure :: StoreScalarFields    => StoreInputScalarFields
        procedure :: StoreObjectFields    => StoreInputObjectFields
        procedure :: LoadScalarFields     => LoadInputScalarFields
        procedure :: LoadObjectFields     => LoadInputObjectFields
        procedure :: DisconnectObjectFields => DisconnectInputObjectFields
        procedure :: IsEqual              => IsInputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorInput
        procedure :: clear                => ClearInput
        procedure :: init => InitInput
#ifndef __GFORTRAN__
        final     :: FinalizeInput
#endif

    end type

    type, extends(persistent) :: ExpansionSizes
        integer(kind=int32)  ::  npnt =  0

        integer(kind=int32)  ::  nbndf =  0

        integer(kind=int32)  ::  nspin =  0

        integer(kind=int32)  ::  nbasmpw =  0

        integer(kind=int32)  ::  nfun =  0



        contains
        procedure :: AllocateObjectFields => AllocateExpansionSizesObjectFields
        procedure :: ResetSectionFields   => ResetExpansionSizesSectionFields
        procedure :: StoreScalarFields    => StoreExpansionSizesScalarFields
        procedure :: StoreObjectFields    => StoreExpansionSizesObjectFields
        procedure :: LoadScalarFields     => LoadExpansionSizesScalarFields
        procedure :: LoadObjectFields     => LoadExpansionSizesObjectFields
        procedure :: DisconnectObjectFields => DisconnectExpansionSizesObjectFields
        procedure :: IsEqual              => IsExpansionSizesEqual
        procedure :: AssignmentOperator   => AssignmentOperatorExpansionSizes
        procedure :: clear                => ClearExpansionSizes
        procedure :: init => InitExpansionSizes
#ifndef __GFORTRAN__
        final     :: FinalizeExpansionSizes
#endif

    end type

    type, extends(ExpansionSizes) :: Sizes
        integer(kind=int32)  ::  ncormax =  0

        integer(kind=int32)  ::  nspin_0 =  0

        integer(kind=int32)  ::  nsort =  0

        integer(kind=int32)  ::  maxmtcor =  0

        integer(kind=int32)  ::  maxel =  0

        integer(kind=int32)  ::  natom =  0

        integer(kind=int32)  ::  maxwf =  0

        integer(kind=int32)  ::  nlmb =  0

        integer(kind=int32)  ::  maxntle =  0

        integer(kind=int32)  ::  maxnrad =  0

        logical  ::  has_gf0 = .False.



        contains
        procedure :: AllocateObjectFields => AllocateSizesObjectFields
        procedure :: ResetSectionFields   => ResetSizesSectionFields
        procedure :: StoreScalarFields    => StoreSizesScalarFields
        procedure :: StoreObjectFields    => StoreSizesObjectFields
        procedure :: LoadScalarFields     => LoadSizesScalarFields
        procedure :: LoadObjectFields     => LoadSizesObjectFields
        procedure :: DisconnectObjectFields => DisconnectSizesObjectFields
        procedure :: IsEqual              => IsSizesEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSizes
        procedure :: clear                => ClearSizes
        procedure :: init => InitSizes
#ifndef __GFORTRAN__
        final     :: FinalizeSizes
#endif

    end type

    type, extends(persistent) :: ElDensity
        integer(kind=int32)  ::  maxmt =  0

        integer(kind=int32)  ::  nspin_0 =  int(1,kind=int32)

        integer(kind=int32)  ::  maxmtb =  int(1,kind=int32)

        integer(kind=int32)  ::  nplwro =  int(1,kind=int32)

        integer(kind=int32)  ::  nrmax =  int(0,kind=int32)

        integer(kind=int32)  ::  nsort =  int(0,kind=int32)

        type(vector_real)  ::  ro
        real(kind=dp),pointer :: ro_(:)
        type(matrix_complex)  ::  rointr
        complex(kind=dp),pointer :: rointr_(:,:)
        type(vector_real)  ::  spmt
        real(kind=dp),pointer :: spmt_(:)
        type(matrix_complex)  ::  spintr
        complex(kind=dp),pointer :: spintr_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateElDensityObjectFields
        procedure :: ResetSectionFields   => ResetElDensitySectionFields
        procedure :: StoreScalarFields    => StoreElDensityScalarFields
        procedure :: StoreObjectFields    => StoreElDensityObjectFields
        procedure :: LoadScalarFields     => LoadElDensityScalarFields
        procedure :: LoadObjectFields     => LoadElDensityObjectFields
        procedure :: DisconnectObjectFields => DisconnectElDensityObjectFields
        procedure :: IsEqual              => IsElDensityEqual
        procedure :: AssignmentOperator   => AssignmentOperatorElDensity
        procedure :: clear                => ClearElDensity
        procedure :: init => InitElDensity
#ifndef __GFORTRAN__
        final     :: FinalizeElDensity
#endif
        procedure :: GetEldensityRoExtents
        procedure :: GetEldensityRointrExtents
        procedure :: GetEldensitySpmtExtents
        procedure :: GetEldensitySpintrExtents

    end type

    type, extends(persistent) :: ElDensityWithCore
        integer(kind=int32)  ::  nrmax =  int(0,kind=int32)

        integer(kind=int32)  ::  nsort =  int(0,kind=int32)

        integer(kind=int32)  ::  nspin_0 =  int(1,kind=int32)

        type(cube_complex)  ::  ro_core
        complex(kind=dp),pointer :: ro_core_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateElDensityWithCoreObjectFields
        procedure :: ResetSectionFields   => ResetElDensityWithCoreSectionFields
        procedure :: StoreScalarFields    => StoreElDensityWithCoreScalarFields
        procedure :: StoreObjectFields    => StoreElDensityWithCoreObjectFields
        procedure :: LoadScalarFields     => LoadElDensityWithCoreScalarFields
        procedure :: LoadObjectFields     => LoadElDensityWithCoreObjectFields
        procedure :: DisconnectObjectFields => DisconnectElDensityWithCoreObjectFields
        procedure :: IsEqual              => IsElDensityWithCoreEqual
        procedure :: AssignmentOperator   => AssignmentOperatorElDensityWithCore
        procedure :: clear                => ClearElDensityWithCore
        procedure :: init => InitElDensityWithCore
#ifndef __GFORTRAN__
        final     :: FinalizeElDensityWithCore
#endif
        procedure :: GetEldensitywithcoreRo_coreExtents

    end type

    type, extends(Sizes) :: Image
        real(kind=dp)  ::  chem_pot =  0.0d0

        type(cube_real)  ::  e_core
        real(kind=dp),pointer :: e_core_(:,:,:)
        type(vector_real)  ::  pcor
        real(kind=dp),pointer :: pcor_(:)
        type(vector_real)  ::  qcor
        real(kind=dp),pointer :: qcor_(:)
        type(matrix_int)  ::  n_bnd
        integer(kind=int32),pointer :: n_bnd_(:,:)
        type(cube_real)  ::  e_bnd
        real(kind=dp),pointer :: e_bnd_(:,:,:)
        type(array4_complex)  ::  g_loc_0
        complex(kind=dp),pointer :: g_loc_0_(:,:,:,:)
        type(matrix_real)  ::  gfun
        real(kind=dp),pointer :: gfun_(:,:)
        type(array5_real)  ::  p_f
        real(kind=dp),pointer :: p_f_(:,:,:,:,:)
        type(array5_real)  ::  pd_f
        real(kind=dp),pointer :: pd_f_(:,:,:,:,:)
        type(array5_real)  ::  pd2_f
        real(kind=dp),pointer :: pd2_f_(:,:,:,:,:)
        type(array5_real)  ::  q_f
        real(kind=dp),pointer :: q_f_(:,:,:,:,:)
        type(array5_real)  ::  qd_f
        real(kind=dp),pointer :: qd_f_(:,:,:,:,:)
        type(array5_real)  ::  qd2_f
        real(kind=dp),pointer :: qd2_f_(:,:,:,:,:)
        type(matrix_real)  ::  gfund
        real(kind=dp),pointer :: gfund_(:,:)
        type(array4_real)  ::  eny
        real(kind=dp),pointer :: eny_(:,:,:,:)
        type(array7_real)  ::  ffsmt
        real(kind=dp),pointer :: ffsmt_(:,:,:,:,:,:,:)
        type(ElDensity)  ::  rho


        contains
        procedure :: AllocateObjectFields => AllocateImageObjectFields
        procedure :: ResetSectionFields   => ResetImageSectionFields
        procedure :: StoreScalarFields    => StoreImageScalarFields
        procedure :: StoreObjectFields    => StoreImageObjectFields
        procedure :: LoadScalarFields     => LoadImageScalarFields
        procedure :: LoadObjectFields     => LoadImageObjectFields
        procedure :: DisconnectObjectFields => DisconnectImageObjectFields
        procedure :: IsEqual              => IsImageEqual
        procedure :: AssignmentOperator   => AssignmentOperatorImage
        procedure :: clear                => ClearImage
        procedure :: init => InitImage
#ifndef __GFORTRAN__
        final     :: FinalizeImage
#endif
        procedure :: GetImageE_coreExtents
        procedure :: GetImagePcorExtents
        procedure :: GetImageQcorExtents
        procedure :: GetImageN_bndExtents
        procedure :: GetImageE_bndExtents
        procedure :: GetImageG_loc_0Extents
        procedure :: GetImageGfunExtents
        procedure :: GetImageP_fExtents
        procedure :: GetImagePd_fExtents
        procedure :: GetImagePd2_fExtents
        procedure :: GetImageQ_fExtents
        procedure :: GetImageQd_fExtents
        procedure :: GetImageQd2_fExtents
        procedure :: GetImageGfundExtents
        procedure :: GetImageEnyExtents
        procedure :: GetImageFfsmtExtents

    end type

    type, extends(ExpansionSizes) :: FullEVB
        type(array4_complex)  ::  evb
        complex(kind=dp),pointer :: evb_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateFullEVBObjectFields
        procedure :: ResetSectionFields   => ResetFullEVBSectionFields
        procedure :: StoreScalarFields    => StoreFullEVBScalarFields
        procedure :: StoreObjectFields    => StoreFullEVBObjectFields
        procedure :: LoadScalarFields     => LoadFullEVBScalarFields
        procedure :: LoadObjectFields     => LoadFullEVBObjectFields
        procedure :: DisconnectObjectFields => DisconnectFullEVBObjectFields
        procedure :: IsEqual              => IsFullEVBEqual
        procedure :: AssignmentOperator   => AssignmentOperatorFullEVB
        procedure :: clear                => ClearFullEVB
        procedure :: init => InitFullEVB
#ifndef __GFORTRAN__
        final     :: FinalizeFullEVB
#endif
        procedure :: GetFullevbEvbExtents

    end type

    type, extends(ExpansionSizes) :: FullZB
        type(array4_complex)  ::  zb
        complex(kind=dp),pointer :: zb_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateFullZBObjectFields
        procedure :: ResetSectionFields   => ResetFullZBSectionFields
        procedure :: StoreScalarFields    => StoreFullZBScalarFields
        procedure :: StoreObjectFields    => StoreFullZBObjectFields
        procedure :: LoadScalarFields     => LoadFullZBScalarFields
        procedure :: LoadObjectFields     => LoadFullZBObjectFields
        procedure :: DisconnectObjectFields => DisconnectFullZBObjectFields
        procedure :: IsEqual              => IsFullZBEqual
        procedure :: AssignmentOperator   => AssignmentOperatorFullZB
        procedure :: clear                => ClearFullZB
        procedure :: init => InitFullZB
#ifndef __GFORTRAN__
        final     :: FinalizeFullZB
#endif
        procedure :: GetFullzbZbExtents

    end type

    type, extends(ExpansionSizes) :: FullGF0
        type(array4_complex)  ::  gf0
        complex(kind=dp),pointer :: gf0_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateFullGF0ObjectFields
        procedure :: ResetSectionFields   => ResetFullGF0SectionFields
        procedure :: StoreScalarFields    => StoreFullGF0ScalarFields
        procedure :: StoreObjectFields    => StoreFullGF0ObjectFields
        procedure :: LoadScalarFields     => LoadFullGF0ScalarFields
        procedure :: LoadObjectFields     => LoadFullGF0ObjectFields
        procedure :: DisconnectObjectFields => DisconnectFullGF0ObjectFields
        procedure :: IsEqual              => IsFullGF0Equal
        procedure :: AssignmentOperator   => AssignmentOperatorFullGF0
        procedure :: clear                => ClearFullGF0
        procedure :: init => InitFullGF0
#ifndef __GFORTRAN__
        final     :: FinalizeFullGF0
#endif
        procedure :: GetFullgf0Gf0Extents

    end type

    contains
        subroutine InitControl(self)
                class(Control), intent(inout) :: self
                call self%InitPersistent()
                self%iter_dft =  int(80,kind=int32)
                self%iter_hf =  int(0,kind=int32)
                self%iter_gw =  int(0,kind=int32)
                self%iter_qp =  int(0,kind=int32)
                self%iter_psi =  int(0,kind=int32)
                self%iter_bsp =  int(0,kind=int32)
                self%restart_begin =  int(0,kind=int32)
                self%restart_end =  int(0,kind=int32)
                self%admix =  real(0.050000,kind=16)
                self%adspin =  real(0.700000,kind=16)
                self%adm_gw =  real(0.300000,kind=16)
                self%acc_it_gw =  real(0.600000,kind=16)
                self%iexch =  int(5,kind=int32)
                self%scal_spin =  real(1.000000,kind=16)
                self%psi_fncl_use =  int(0,kind=int32)
                self%nproc_tau =  int(1,kind=int32)
                self%nproc_k =  int(1,kind=int32)
                self%nproc_pbr =  int(1,kind=int32)
                self%irel =  int(1,kind=int32)
                self%irel_core =  int(1,kind=int32)
                self%clight =  real(274.074000,kind=16)
                self%rel_interst = .False.
                self%temperature =  real(900.000000,kind=16)
        end subroutine
        subroutine StoreControlObjectFields(self)
                class(Control), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadControlObjectFields(self)
                class(Control), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetControlSectionFields(self)
                class(Control), intent(inout) :: self
        end subroutine
        subroutine DisconnectControlObjectFields(self)
                class(Control), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreControlScalarFields(self)
                class(Control), intent(inout) :: self
                call self%write('iter_dft', self%iter_dft)
                call self%write('iter_hf', self%iter_hf)
                call self%write('iter_gw', self%iter_gw)
                call self%write('iter_qp', self%iter_qp)
                call self%write('iter_psi', self%iter_psi)
                call self%write('iter_bsp', self%iter_bsp)
                call self%write('restart_begin', self%restart_begin)
                call self%write('restart_end', self%restart_end)
                call self%write('admix', self%admix)
                call self%write('adspin', self%adspin)
                call self%write('adm_gw', self%adm_gw)
                call self%write('acc_it_gw', self%acc_it_gw)
                call self%write('iexch', self%iexch)
                call self%write('scal_spin', self%scal_spin)
                call self%write('psi_fncl_use', self%psi_fncl_use)
                call self%write('nproc_tau', self%nproc_tau)
                call self%write('nproc_k', self%nproc_k)
                call self%write('nproc_pbr', self%nproc_pbr)
                call self%write('irel', self%irel)
                call self%write('irel_core', self%irel_core)
                call self%write('clight', self%clight)
                call self%write('rel_interst', self%rel_interst)
                call self%write('temperature', self%temperature)
        end subroutine
        subroutine LoadControlScalarFields(self)
                class(Control), intent(inout) :: self
                call self%read('iter_dft', self%iter_dft)
                call self%read('iter_hf', self%iter_hf)
                call self%read('iter_gw', self%iter_gw)
                call self%read('iter_qp', self%iter_qp)
                call self%read('iter_psi', self%iter_psi)
                call self%read('iter_bsp', self%iter_bsp)
                call self%read('restart_begin', self%restart_begin)
                call self%read('restart_end', self%restart_end)
                call self%read('admix', self%admix)
                call self%read('adspin', self%adspin)
                call self%read('adm_gw', self%adm_gw)
                call self%read('acc_it_gw', self%acc_it_gw)
                call self%read('iexch', self%iexch)
                call self%read('scal_spin', self%scal_spin)
                call self%read('psi_fncl_use', self%psi_fncl_use)
                call self%read('nproc_tau', self%nproc_tau)
                call self%read('nproc_k', self%nproc_k)
                call self%read('nproc_pbr', self%nproc_pbr)
                call self%read('irel', self%irel)
                call self%read('irel_core', self%irel_core)
                call self%read('clight', self%clight)
                call self%read('rel_interst', self%rel_interst)
                call self%read('temperature', self%temperature)
        end subroutine
        subroutine FinalizeControl(self)
               type(Control), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearControl(self)
                class(Control), intent(inout) :: self
                type(Control), save :: empty
                self = empty
        end subroutine
        pure elemental function IsControlEqual(lhs, rhs) result(iseq)
                class(Control), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Control)
                       iseq = iseq .and. (lhs%iter_dft == rhs%iter_dft)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%iter_hf == rhs%iter_hf)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%iter_gw == rhs%iter_gw)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%iter_qp == rhs%iter_qp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%iter_psi == rhs%iter_psi)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%iter_bsp == rhs%iter_bsp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%restart_begin == rhs%restart_begin)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%restart_end == rhs%restart_end)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%admix == rhs%admix)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%adspin == rhs%adspin)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%adm_gw == rhs%adm_gw)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%acc_it_gw == rhs%acc_it_gw)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%iexch == rhs%iexch)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%scal_spin == rhs%scal_spin)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%psi_fncl_use == rhs%psi_fncl_use)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nproc_tau == rhs%nproc_tau)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nproc_k == rhs%nproc_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nproc_pbr == rhs%nproc_pbr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%irel == rhs%irel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%irel_core == rhs%irel_core)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%clight == rhs%clight)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rel_interst .eqv. rhs%rel_interst)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%temperature == rhs%temperature)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorControl(lhs, rhs)
                class(Control), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Control)
                       lhs%iter_dft = rhs%iter_dft
                       lhs%iter_hf = rhs%iter_hf
                       lhs%iter_gw = rhs%iter_gw
                       lhs%iter_qp = rhs%iter_qp
                       lhs%iter_psi = rhs%iter_psi
                       lhs%iter_bsp = rhs%iter_bsp
                       lhs%restart_begin = rhs%restart_begin
                       lhs%restart_end = rhs%restart_end
                       lhs%admix = rhs%admix
                       lhs%adspin = rhs%adspin
                       lhs%adm_gw = rhs%adm_gw
                       lhs%acc_it_gw = rhs%acc_it_gw
                       lhs%iexch = rhs%iexch
                       lhs%scal_spin = rhs%scal_spin
                       lhs%psi_fncl_use = rhs%psi_fncl_use
                       lhs%nproc_tau = rhs%nproc_tau
                       lhs%nproc_k = rhs%nproc_k
                       lhs%nproc_pbr = rhs%nproc_pbr
                       lhs%irel = rhs%irel
                       lhs%irel_core = rhs%irel_core
                       lhs%clight = rhs%clight
                       lhs%rel_interst = rhs%rel_interst
                       lhs%temperature = rhs%temperature
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateControlObjectFields(self)
                class(Control), intent(inout) :: self
        end subroutine


        subroutine InitAtomicData(self)
                class(AtomicData), intent(inout) :: self
                call self%InitPersistent()
                self%z =  0.0d0
                self%z_dop =  real(0.000000,kind=16)
                self%magn_shift =  real(0.000000,kind=16)
                self%smt =  0.0d0
                self%h =  real(0.012000,kind=16)
                self%nrad =  int(1200,kind=int32)
                self%lmb =  0
                self%lmpb =  0
                self%maxntle =  int(10,kind=int32)
        end subroutine
        subroutine StoreAtomicDataObjectFields(self)
                class(AtomicData), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%lim_pb_mt%StoreObject(ps, gid,  'lim_pb_mt')
                call self%lim_pb_mt_red%StoreObject(ps, gid,  'lim_pb_mt_red')
                call self%ntle%StoreObject(ps, gid,  'ntle')
                call self%augm%StoreObject(ps, gid,  'augm')
                call self%atoc%StoreObject(ps, gid,  'atoc')
                call self%ptnl%StoreObject(ps, gid,  'ptnl')
                call self%correlated%StoreObject(ps, gid,  'correlated')
                call self%idmd%StoreObject(ps, gid,  'idmd')
        end subroutine
        subroutine LoadAtomicDataObjectFields(self)
                class(AtomicData), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%lim_pb_mt%LoadObject(ps, gid,  'lim_pb_mt')
                call self%lim_pb_mt_red%LoadObject(ps, gid,  'lim_pb_mt_red')
                call self%ntle%LoadObject(ps, gid,  'ntle')
                call self%augm%LoadObject(ps, gid,  'augm')
                call self%atoc%LoadObject(ps, gid,  'atoc')
                call self%ptnl%LoadObject(ps, gid,  'ptnl')
                call self%correlated%LoadObject(ps, gid,  'correlated')
                call self%idmd%LoadObject(ps, gid,  'idmd')
        end subroutine
        subroutine ResetAtomicDataSectionFields(self)
                class(AtomicData), intent(inout) :: self
                self%lim_pb_mt_ => self%lim_pb_mt%GetWithExtents(self%GetAtomicDatalim_pb_mtExtents())
                self%lim_pb_mt_red_ => self%lim_pb_mt_red%GetWithExtents(self%GetAtomicDatalim_pb_mt_redExtents())
                self%ntle_ => self%ntle%GetWithExtents(self%GetAtomicDatantleExtents())
                self%augm_ => self%augm%GetWithExtents(self%GetAtomicDataaugmExtents())
                self%atoc_ => self%atoc%GetWithExtents(self%GetAtomicDataatocExtents())
                self%ptnl_ => self%ptnl%GetWithExtents(self%GetAtomicDataptnlExtents())
                self%correlated_ => self%correlated%GetWithExtents(self%GetAtomicDatacorrelatedExtents())
                self%idmd_ => self%idmd%GetWithExtents(self%GetAtomicDataidmdExtents())
        end subroutine
        subroutine DisconnectAtomicDataObjectFields(self)
                class(AtomicData), intent(inout) :: self
               type(iterator) :: iter
                call self%lim_pb_mt%DisconnectFromStore()
                call self%lim_pb_mt_red%DisconnectFromStore()
                call self%ntle%DisconnectFromStore()
                call self%augm%DisconnectFromStore()
                call self%atoc%DisconnectFromStore()
                call self%ptnl%DisconnectFromStore()
                call self%correlated%DisconnectFromStore()
                call self%idmd%DisconnectFromStore()
        end subroutine
        subroutine StoreAtomicDataScalarFields(self)
                class(AtomicData), intent(inout) :: self
                call self%write('txtel', self%txtel)
                call self%write('z', self%z)
                call self%write('z_dop', self%z_dop)
                call self%write('magn_shift', self%magn_shift)
                call self%write('smt', self%smt)
                call self%write('h', self%h)
                call self%write('nrad', self%nrad)
                call self%write('lmb', self%lmb)
                call self%write('lmpb', self%lmpb)
                call self%write('maxntle', self%maxntle)
        end subroutine
        subroutine LoadAtomicDataScalarFields(self)
                class(AtomicData), intent(inout) :: self
                call self%read('txtel', self%txtel)
                call self%read('z', self%z)
                call self%read('z_dop', self%z_dop)
                call self%read('magn_shift', self%magn_shift)
                call self%read('smt', self%smt)
                call self%read('h', self%h)
                call self%read('nrad', self%nrad)
                call self%read('lmb', self%lmb)
                call self%read('lmpb', self%lmpb)
                call self%read('maxntle', self%maxntle)
        end subroutine
        subroutine FinalizeAtomicData(self)
               type(AtomicData), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearAtomicData(self)
                class(AtomicData), intent(inout) :: self
                type(AtomicData), save :: empty
                self = empty
        end subroutine
        pure elemental function IsAtomicDataEqual(lhs, rhs) result(iseq)
                class(AtomicData), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (AtomicData)
                       iseq = iseq .and. (lhs%txtel == rhs%txtel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%z == rhs%z)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%z_dop == rhs%z_dop)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%magn_shift == rhs%magn_shift)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%smt == rhs%smt)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%h == rhs%h)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrad == rhs%nrad)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%lmb == rhs%lmb)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%lmpb == rhs%lmpb)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxntle == rhs%maxntle)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%lim_pb_mt == rhs%lim_pb_mt)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%lim_pb_mt_red == rhs%lim_pb_mt_red)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ntle == rhs%ntle)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%augm == rhs%augm)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%atoc == rhs%atoc)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ptnl == rhs%ptnl)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%correlated == rhs%correlated)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%idmd == rhs%idmd)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorAtomicData(lhs, rhs)
                class(AtomicData), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (AtomicData)
                       lhs%txtel = rhs%txtel
                       lhs%z = rhs%z
                       lhs%z_dop = rhs%z_dop
                       lhs%magn_shift = rhs%magn_shift
                       lhs%smt = rhs%smt
                       lhs%h = rhs%h
                       lhs%nrad = rhs%nrad
                       lhs%lmb = rhs%lmb
                       lhs%lmpb = rhs%lmpb
                       lhs%maxntle = rhs%maxntle
                       lhs%lim_pb_mt = rhs%lim_pb_mt
                       lhs%lim_pb_mt_red = rhs%lim_pb_mt_red
                       lhs%ntle = rhs%ntle
                       lhs%augm = rhs%augm
                       lhs%atoc = rhs%atoc
                       lhs%ptnl = rhs%ptnl
                       lhs%correlated = rhs%correlated
                       lhs%idmd = rhs%idmd
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetAtomicdataLim_pb_mtExtents(self) result(res)
                class(AtomicData), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(0,self%lmpb)]
        end function
        function GetAtomicdataLim_pb_mt_redExtents(self) result(res)
                class(AtomicData), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(0,self%lmpb)]
        end function
        function GetAtomicdataNtleExtents(self) result(res)
                class(AtomicData), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(0,self%lmb)]
        end function
        function GetAtomicdataAugmExtents(self) result(res)
                class(AtomicData), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%maxntle),extent(0,self%lmb)]
        end function
        function GetAtomicdataAtocExtents(self) result(res)
                class(AtomicData), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%maxntle),extent(0,self%lmb)]
        end function
        function GetAtomicdataPtnlExtents(self) result(res)
                class(AtomicData), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%maxntle),extent(0,self%lmb)]
        end function
        function GetAtomicdataCorrelatedExtents(self) result(res)
                class(AtomicData), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%maxntle),extent(0,self%lmb)]
        end function
        function GetAtomicdataIdmdExtents(self) result(res)
                class(AtomicData), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%maxntle),extent(0,self%lmb)]
        end function
        subroutine AllocateAtomicDataObjectFields(self)
                class(AtomicData), intent(inout) :: self
                call self%lim_pb_mt%init(int(self%lmpb- (0) + 1))
                call self%lim_pb_mt_red%init(int(self%lmpb- (0) + 1))
                call self%ntle%init(int(self%lmb- (0) + 1))
                call self%augm%init(int(self%maxntle),int(self%lmb- (0) + 1))
                call self%atoc%init(int(self%maxntle),int(self%lmb- (0) + 1))
                call self%ptnl%init(int(self%maxntle),int(self%lmb- (0) + 1))
                call self%correlated%init(int(self%maxntle),int(self%lmb- (0) + 1))
                call self%idmd%init(int(self%maxntle),int(self%lmb- (0) + 1))
        end subroutine


        subroutine InitStructure(self)
                class(Structure), intent(inout) :: self
                call self%InitPersistent()
                self%par =  real(1.000000,kind=16)
                self%b_a =  real(1.000000,kind=16)
                self%c_a =  real(1.000000,kind=16)
                self%nsort =  0
                self%natom =  0
                self%istruc =  int(1,kind=int32)
        end subroutine
        subroutine StoreStructureObjectFields(self)
                class(Structure), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%a%StoreObject(ps, gid,  'a')
                call self%b%StoreObject(ps, gid,  'b')
                call self%c%StoreObject(ps, gid,  'c')
                call self%tau%StoreObject(ps, gid,  'tau')
                call self%isA%StoreObject(ps, gid,  'isA')

                call iter%Init(lbound(self%ad), ubound(self%ad))
                do while (.not. iter%Done())
                    call self%ad(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'ad', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadStructureObjectFields(self)
                class(Structure), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%a%LoadObject(ps, gid,  'a')
                call self%b%LoadObject(ps, gid,  'b')
                call self%c%LoadObject(ps, gid,  'c')
                call self%tau%LoadObject(ps, gid,  'tau')
                call self%isA%LoadObject(ps, gid,  'isA')

                allocate(self%ad(int(1):int(self%nsort)))
                call iter%Init(lbound(self%ad), ubound(self%ad))
                do while (.not. iter%Done())
                    call self%ad(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'ad', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetStructureSectionFields(self)
                class(Structure), intent(inout) :: self
                self%a_ => self%a%GetWithExtents(self%GetStructureaExtents())
                self%b_ => self%b%GetWithExtents(self%GetStructurebExtents())
                self%c_ => self%c%GetWithExtents(self%GetStructurecExtents())
                self%tau_ => self%tau%GetWithExtents(self%GetStructuretauExtents())
                self%isA_ => self%isA%GetWithExtents(self%GetStructureisAExtents())
        end subroutine
        subroutine DisconnectStructureObjectFields(self)
                class(Structure), intent(inout) :: self
               type(iterator) :: iter
                call self%a%DisconnectFromStore()
                call self%b%DisconnectFromStore()
                call self%c%DisconnectFromStore()
                call self%tau%DisconnectFromStore()
                call self%isA%DisconnectFromStore()

                call iter%Init(lbound(self%ad), ubound(self%ad))
                do while (.not. iter%Done())
                    call self%ad(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreStructureScalarFields(self)
                class(Structure), intent(inout) :: self
                call self%write('par', self%par)
                call self%write('b_a', self%b_a)
                call self%write('c_a', self%c_a)
                call self%write('nsort', self%nsort)
                call self%write('natom', self%natom)
                call self%write('istruc', self%istruc)
        end subroutine
        subroutine LoadStructureScalarFields(self)
                class(Structure), intent(inout) :: self
                call self%read('par', self%par)
                call self%read('b_a', self%b_a)
                call self%read('c_a', self%c_a)
                call self%read('nsort', self%nsort)
                call self%read('natom', self%natom)
                call self%read('istruc', self%istruc)
        end subroutine
        subroutine FinalizeStructure(self)
               type(Structure), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearStructure(self)
                class(Structure), intent(inout) :: self
                type(Structure), save :: empty
                self = empty
        end subroutine
        pure elemental function IsStructureEqual(lhs, rhs) result(iseq)
                class(Structure), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Structure)
                       iseq = iseq .and. (lhs%par == rhs%par)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%b_a == rhs%b_a)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%c_a == rhs%c_a)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nsort == rhs%nsort)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%natom == rhs%natom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%istruc == rhs%istruc)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%a == rhs%a)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%b == rhs%b)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%c == rhs%c)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%tau == rhs%tau)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%isA == rhs%isA)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%ad) .eqv. allocated(rhs%ad))
                       if (.not. iseq) return
                       if (allocated(lhs%ad)) then
                           iseq = iseq .and. all(shape(lhs%ad) == shape(rhs%ad))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%ad(:) == rhs%ad(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorStructure(lhs, rhs)
                class(Structure), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Structure)
                       lhs%par = rhs%par
                       lhs%b_a = rhs%b_a
                       lhs%c_a = rhs%c_a
                       lhs%nsort = rhs%nsort
                       lhs%natom = rhs%natom
                       lhs%istruc = rhs%istruc
                       lhs%a = rhs%a
                       lhs%b = rhs%b
                       lhs%c = rhs%c
                       lhs%tau = rhs%tau
                       lhs%isA = rhs%isA
                       lhs%ad = rhs%ad
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetStructureAExtents(self) result(res)
                class(Structure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetStructureBExtents(self) result(res)
                class(Structure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetStructureCExtents(self) result(res)
                class(Structure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetStructureTauExtents(self) result(res)
                class(Structure), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%natom)]
        end function
        function GetStructureIsaExtents(self) result(res)
                class(Structure), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%natom)]
        end function
        subroutine AllocateStructureObjectFields(self)
                class(Structure), intent(inout) :: self
                call self%a%init(int(3))
                call self%b%init(int(3))
                call self%c%init(int(3))
                call self%tau%init(int(3),int(self%natom))
                call self%isA%init(int(self%natom))
                allocate(self%ad(int(1):int(self%nsort)))
        end subroutine


        subroutine InitGenerator(self)
                class(Generator), intent(inout) :: self
                call self%InitPersistent()
                self%sign =  int(1,kind=int32)
                self%kind = "R"
                self%n =  0
        end subroutine
        subroutine StoreGeneratorObjectFields(self)
                class(Generator), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%v%StoreObject(ps, gid,  'v')
                call self%t%StoreObject(ps, gid,  't')
        end subroutine
        subroutine LoadGeneratorObjectFields(self)
                class(Generator), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%v%LoadObject(ps, gid,  'v')
                call self%t%LoadObject(ps, gid,  't')
        end subroutine
        subroutine ResetGeneratorSectionFields(self)
                class(Generator), intent(inout) :: self
                self%v_ => self%v%GetWithExtents(self%GetGeneratorvExtents())
                self%t_ => self%t%GetWithExtents(self%GetGeneratortExtents())
        end subroutine
        subroutine DisconnectGeneratorObjectFields(self)
                class(Generator), intent(inout) :: self
               type(iterator) :: iter
                call self%v%DisconnectFromStore()
                call self%t%DisconnectFromStore()
        end subroutine
        subroutine StoreGeneratorScalarFields(self)
                class(Generator), intent(inout) :: self
                call self%write('sign', self%sign)
                call self%write('kind', self%kind)
                call self%write('n', self%n)
        end subroutine
        subroutine LoadGeneratorScalarFields(self)
                class(Generator), intent(inout) :: self
                call self%read('sign', self%sign)
                call self%read('kind', self%kind)
                call self%read('n', self%n)
        end subroutine
        subroutine FinalizeGenerator(self)
               type(Generator), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearGenerator(self)
                class(Generator), intent(inout) :: self
                type(Generator), save :: empty
                self = empty
        end subroutine
        pure elemental function IsGeneratorEqual(lhs, rhs) result(iseq)
                class(Generator), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Generator)
                       iseq = iseq .and. (lhs%sign == rhs%sign)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%kind == rhs%kind)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%v == rhs%v)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%n == rhs%n)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%t == rhs%t)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorGenerator(lhs, rhs)
                class(Generator), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Generator)
                       lhs%sign = rhs%sign
                       lhs%kind = rhs%kind
                       lhs%v = rhs%v
                       lhs%n = rhs%n
                       lhs%t = rhs%t
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetGeneratorVExtents(self) result(res)
                class(Generator), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetGeneratorTExtents(self) result(res)
                class(Generator), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        subroutine AllocateGeneratorObjectFields(self)
                class(Generator), intent(inout) :: self
                call self%v%init(int(3))
                call self%t%init(int(3))
        end subroutine


        subroutine InitSpacegroupGens(self)
                class(SpacegroupGens), intent(inout) :: self
                call self%InitPersistent()
                self%num_generators =  0
        end subroutine
        subroutine StoreSpacegroupGensObjectFields(self)
                class(SpacegroupGens), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                call iter%Init(lbound(self%gens), ubound(self%gens))
                do while (.not. iter%Done())
                    call self%gens(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'gens', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
                call self%rots%StoreObject(ps, gid,  'rots')
        end subroutine
        subroutine LoadSpacegroupGensObjectFields(self)
                class(SpacegroupGens), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                allocate(self%gens(int(1):int(self%num_generators)))
                call iter%Init(lbound(self%gens), ubound(self%gens))
                do while (.not. iter%Done())
                    call self%gens(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'gens', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
                call self%rots%LoadObject(ps, gid,  'rots')
        end subroutine
        subroutine ResetSpacegroupGensSectionFields(self)
                class(SpacegroupGens), intent(inout) :: self
                self%rots_ => self%rots%GetWithExtents(self%GetSpacegroupGensrotsExtents())
        end subroutine
        subroutine DisconnectSpacegroupGensObjectFields(self)
                class(SpacegroupGens), intent(inout) :: self
               type(iterator) :: iter

                call iter%Init(lbound(self%gens), ubound(self%gens))
                do while (.not. iter%Done())
                    call self%gens(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
                call self%rots%DisconnectFromStore()
        end subroutine
        subroutine StoreSpacegroupGensScalarFields(self)
                class(SpacegroupGens), intent(inout) :: self
                call self%write('num_generators', self%num_generators)
        end subroutine
        subroutine LoadSpacegroupGensScalarFields(self)
                class(SpacegroupGens), intent(inout) :: self
                call self%read('num_generators', self%num_generators)
        end subroutine
        subroutine FinalizeSpacegroupGens(self)
               type(SpacegroupGens), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSpacegroupGens(self)
                class(SpacegroupGens), intent(inout) :: self
                type(SpacegroupGens), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSpacegroupGensEqual(lhs, rhs) result(iseq)
                class(SpacegroupGens), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (SpacegroupGens)
                       iseq = iseq .and. (lhs%num_generators == rhs%num_generators)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%gens) .eqv. allocated(rhs%gens))
                       if (.not. iseq) return
                       if (allocated(lhs%gens)) then
                           iseq = iseq .and. all(shape(lhs%gens) == shape(rhs%gens))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%gens(:) == rhs%gens(:))
                        end if
                       iseq = iseq .and. (lhs%rots == rhs%rots)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSpacegroupGens(lhs, rhs)
                class(SpacegroupGens), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (SpacegroupGens)
                       lhs%num_generators = rhs%num_generators
                       lhs%gens = rhs%gens
                       lhs%rots = rhs%rots
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetSpacegroupgensRotsExtents(self) result(res)
                class(SpacegroupGens), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,3),extent(1,3),extent(1,self%num_generators)]
        end function
        subroutine AllocateSpacegroupGensObjectFields(self)
                class(SpacegroupGens), intent(inout) :: self
                allocate(self%gens(int(1):int(self%num_generators)))
                call self%rots%init(int(3),int(3),int(self%num_generators))
        end subroutine


        subroutine InitKGrid(self)
                class(KGrid), intent(inout) :: self
                call self%InitPersistent()
                self%metal = .False.
                self%n_k_div =  int(9,kind=int32)
                self%k_line = "010"
        end subroutine
        subroutine StoreKGridObjectFields(self)
                class(KGrid), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ndiv%StoreObject(ps, gid,  'ndiv')
                call self%ndiv_c%StoreObject(ps, gid,  'ndiv_c')
        end subroutine
        subroutine LoadKGridObjectFields(self)
                class(KGrid), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ndiv%LoadObject(ps, gid,  'ndiv')
                call self%ndiv_c%LoadObject(ps, gid,  'ndiv_c')
        end subroutine
        subroutine ResetKGridSectionFields(self)
                class(KGrid), intent(inout) :: self
                self%ndiv_ => self%ndiv%GetWithExtents(self%GetKGridndivExtents())
                self%ndiv_c_ => self%ndiv_c%GetWithExtents(self%GetKGridndiv_cExtents())
        end subroutine
        subroutine DisconnectKGridObjectFields(self)
                class(KGrid), intent(inout) :: self
               type(iterator) :: iter
                call self%ndiv%DisconnectFromStore()
                call self%ndiv_c%DisconnectFromStore()
        end subroutine
        subroutine StoreKGridScalarFields(self)
                class(KGrid), intent(inout) :: self
                call self%write('metal', self%metal)
                call self%write('n_k_div', self%n_k_div)
                call self%write('k_line', self%k_line)
        end subroutine
        subroutine LoadKGridScalarFields(self)
                class(KGrid), intent(inout) :: self
                call self%read('metal', self%metal)
                call self%read('n_k_div', self%n_k_div)
                call self%read('k_line', self%k_line)
        end subroutine
        subroutine FinalizeKGrid(self)
               type(KGrid), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearKGrid(self)
                class(KGrid), intent(inout) :: self
                type(KGrid), save :: empty
                self = empty
        end subroutine
        pure elemental function IsKGridEqual(lhs, rhs) result(iseq)
                class(KGrid), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (KGrid)
                       iseq = iseq .and. (lhs%ndiv == rhs%ndiv)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ndiv_c == rhs%ndiv_c)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%metal .eqv. rhs%metal)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%n_k_div == rhs%n_k_div)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%k_line == rhs%k_line)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorKGrid(lhs, rhs)
                class(KGrid), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (KGrid)
                       lhs%ndiv = rhs%ndiv
                       lhs%ndiv_c = rhs%ndiv_c
                       lhs%metal = rhs%metal
                       lhs%n_k_div = rhs%n_k_div
                       lhs%k_line = rhs%k_line
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetKgridNdivExtents(self) result(res)
                class(KGrid), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetKgridNdiv_cExtents(self) result(res)
                class(KGrid), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        subroutine AllocateKGridObjectFields(self)
                class(KGrid), intent(inout) :: self
                call self%ndiv%init(int(3))
                call self%ndiv_c%init(int(3))
        end subroutine


        subroutine InitRealSpaceMeshes(self)
                class(RealSpaceMeshes), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreRealSpaceMeshesObjectFields(self)
                class(RealSpaceMeshes), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%nrdiv%StoreObject(ps, gid,  'nrdiv')
                call self%mdiv%StoreObject(ps, gid,  'mdiv')
                call self%nrdiv_red%StoreObject(ps, gid,  'nrdiv_red')
        end subroutine
        subroutine LoadRealSpaceMeshesObjectFields(self)
                class(RealSpaceMeshes), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%nrdiv%LoadObject(ps, gid,  'nrdiv')
                call self%mdiv%LoadObject(ps, gid,  'mdiv')
                call self%nrdiv_red%LoadObject(ps, gid,  'nrdiv_red')
        end subroutine
        subroutine ResetRealSpaceMeshesSectionFields(self)
                class(RealSpaceMeshes), intent(inout) :: self
                self%nrdiv_ => self%nrdiv%GetWithExtents(self%GetRealSpaceMeshesnrdivExtents())
                self%mdiv_ => self%mdiv%GetWithExtents(self%GetRealSpaceMeshesmdivExtents())
                self%nrdiv_red_ => self%nrdiv_red%GetWithExtents(self%GetRealSpaceMeshesnrdiv_redExtents())
        end subroutine
        subroutine DisconnectRealSpaceMeshesObjectFields(self)
                class(RealSpaceMeshes), intent(inout) :: self
               type(iterator) :: iter
                call self%nrdiv%DisconnectFromStore()
                call self%mdiv%DisconnectFromStore()
                call self%nrdiv_red%DisconnectFromStore()
        end subroutine
        subroutine StoreRealSpaceMeshesScalarFields(self)
                class(RealSpaceMeshes), intent(inout) :: self
        end subroutine
        subroutine LoadRealSpaceMeshesScalarFields(self)
                class(RealSpaceMeshes), intent(inout) :: self
        end subroutine
        subroutine FinalizeRealSpaceMeshes(self)
               type(RealSpaceMeshes), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearRealSpaceMeshes(self)
                class(RealSpaceMeshes), intent(inout) :: self
                type(RealSpaceMeshes), save :: empty
                self = empty
        end subroutine
        pure elemental function IsRealSpaceMeshesEqual(lhs, rhs) result(iseq)
                class(RealSpaceMeshes), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (RealSpaceMeshes)
                       iseq = iseq .and. (lhs%nrdiv == rhs%nrdiv)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%mdiv == rhs%mdiv)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrdiv_red == rhs%nrdiv_red)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorRealSpaceMeshes(lhs, rhs)
                class(RealSpaceMeshes), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (RealSpaceMeshes)
                       lhs%nrdiv = rhs%nrdiv
                       lhs%mdiv = rhs%mdiv
                       lhs%nrdiv_red = rhs%nrdiv_red
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetRealspacemeshesNrdivExtents(self) result(res)
                class(RealSpaceMeshes), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetRealspacemeshesMdivExtents(self) result(res)
                class(RealSpaceMeshes), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        function GetRealspacemeshesNrdiv_redExtents(self) result(res)
                class(RealSpaceMeshes), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        subroutine AllocateRealSpaceMeshesObjectFields(self)
                class(RealSpaceMeshes), intent(inout) :: self
                call self%nrdiv%init(int(3))
                call self%mdiv%init(int(3))
                call self%nrdiv_red%init(int(3))
        end subroutine


        subroutine InitBasis(self)
                class(Basis), intent(inout) :: self
                call self%InitPersistent()
                self%cut_lapw_ratio =  real(0.610000,kind=16)
                self%cut_pb_ratio =  real(0.980000,kind=16)
                self%eps_pb =  real(0.001000,kind=16)
        end subroutine
        subroutine StoreBasisObjectFields(self)
                class(Basis), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadBasisObjectFields(self)
                class(Basis), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetBasisSectionFields(self)
                class(Basis), intent(inout) :: self
        end subroutine
        subroutine DisconnectBasisObjectFields(self)
                class(Basis), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreBasisScalarFields(self)
                class(Basis), intent(inout) :: self
                call self%write('cut_lapw_ratio', self%cut_lapw_ratio)
                call self%write('cut_pb_ratio', self%cut_pb_ratio)
                call self%write('eps_pb', self%eps_pb)
        end subroutine
        subroutine LoadBasisScalarFields(self)
                class(Basis), intent(inout) :: self
                call self%read('cut_lapw_ratio', self%cut_lapw_ratio)
                call self%read('cut_pb_ratio', self%cut_pb_ratio)
                call self%read('eps_pb', self%eps_pb)
        end subroutine
        subroutine FinalizeBasis(self)
               type(Basis), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearBasis(self)
                class(Basis), intent(inout) :: self
                type(Basis), save :: empty
                self = empty
        end subroutine
        pure elemental function IsBasisEqual(lhs, rhs) result(iseq)
                class(Basis), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Basis)
                       iseq = iseq .and. (lhs%cut_lapw_ratio == rhs%cut_lapw_ratio)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%cut_pb_ratio == rhs%cut_pb_ratio)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%eps_pb == rhs%eps_pb)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorBasis(lhs, rhs)
                class(Basis), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Basis)
                       lhs%cut_lapw_ratio = rhs%cut_lapw_ratio
                       lhs%cut_pb_ratio = rhs%cut_pb_ratio
                       lhs%eps_pb = rhs%eps_pb
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateBasisObjectFields(self)
                class(Basis), intent(inout) :: self
        end subroutine


        subroutine InitZones(self)
                class(Zones), intent(inout) :: self
                call self%InitPersistent()
                self%nbndf =  int(0,kind=int32)
        end subroutine
        subroutine StoreZonesObjectFields(self)
                class(Zones), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%nbndf_bnd%StoreObject(ps, gid,  'nbndf_bnd')
        end subroutine
        subroutine LoadZonesObjectFields(self)
                class(Zones), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%nbndf_bnd%LoadObject(ps, gid,  'nbndf_bnd')
        end subroutine
        subroutine ResetZonesSectionFields(self)
                class(Zones), intent(inout) :: self
                self%nbndf_bnd_ => self%nbndf_bnd%GetWithExtents(self%GetZonesnbndf_bndExtents())
        end subroutine
        subroutine DisconnectZonesObjectFields(self)
                class(Zones), intent(inout) :: self
               type(iterator) :: iter
                call self%nbndf_bnd%DisconnectFromStore()
        end subroutine
        subroutine StoreZonesScalarFields(self)
                class(Zones), intent(inout) :: self
                call self%write('nbndf', self%nbndf)
        end subroutine
        subroutine LoadZonesScalarFields(self)
                class(Zones), intent(inout) :: self
                call self%read('nbndf', self%nbndf)
        end subroutine
        subroutine FinalizeZones(self)
               type(Zones), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearZones(self)
                class(Zones), intent(inout) :: self
                type(Zones), save :: empty
                self = empty
        end subroutine
        pure elemental function IsZonesEqual(lhs, rhs) result(iseq)
                class(Zones), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Zones)
                       iseq = iseq .and. (lhs%nbndf == rhs%nbndf)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nbndf_bnd == rhs%nbndf_bnd)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorZones(lhs, rhs)
                class(Zones), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Zones)
                       lhs%nbndf = rhs%nbndf
                       lhs%nbndf_bnd = rhs%nbndf_bnd
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetZonesNbndf_bndExtents(self) result(res)
                class(Zones), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,2)]
        end function
        subroutine AllocateZonesObjectFields(self)
                class(Zones), intent(inout) :: self
                call self%nbndf_bnd%init(int(2))
        end subroutine


        subroutine InitBandPlot(self)
                class(BandPlot), intent(inout) :: self
                call self%InitPersistent()
                self%n_k_div =  int(10,kind=int32)
        end subroutine
        subroutine StoreBandPlotObjectFields(self)
                class(BandPlot), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadBandPlotObjectFields(self)
                class(BandPlot), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetBandPlotSectionFields(self)
                class(BandPlot), intent(inout) :: self
        end subroutine
        subroutine DisconnectBandPlotObjectFields(self)
                class(BandPlot), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreBandPlotScalarFields(self)
                class(BandPlot), intent(inout) :: self
                call self%write('n_k_div', self%n_k_div)
        end subroutine
        subroutine LoadBandPlotScalarFields(self)
                class(BandPlot), intent(inout) :: self
                call self%read('n_k_div', self%n_k_div)
        end subroutine
        subroutine FinalizeBandPlot(self)
               type(BandPlot), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearBandPlot(self)
                class(BandPlot), intent(inout) :: self
                type(BandPlot), save :: empty
                self = empty
        end subroutine
        pure elemental function IsBandPlotEqual(lhs, rhs) result(iseq)
                class(BandPlot), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (BandPlot)
                       iseq = iseq .and. (lhs%n_k_div == rhs%n_k_div)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorBandPlot(lhs, rhs)
                class(BandPlot), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (BandPlot)
                       lhs%n_k_div = rhs%n_k_div
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateBandPlotObjectFields(self)
                class(BandPlot), intent(inout) :: self
        end subroutine


        subroutine InitDensityOfStates(self)
                class(DensityOfStates), intent(inout) :: self
                call self%InitPersistent()
                self%emindos =  real(2.000000,kind=16)
                self%emaxdos =  real(2.000000,kind=16)
                self%ndos =  int(600,kind=int32)
                self%e_small =  real(0.005000,kind=16)
        end subroutine
        subroutine StoreDensityOfStatesObjectFields(self)
                class(DensityOfStates), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadDensityOfStatesObjectFields(self)
                class(DensityOfStates), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetDensityOfStatesSectionFields(self)
                class(DensityOfStates), intent(inout) :: self
        end subroutine
        subroutine DisconnectDensityOfStatesObjectFields(self)
                class(DensityOfStates), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreDensityOfStatesScalarFields(self)
                class(DensityOfStates), intent(inout) :: self
                call self%write('emindos', self%emindos)
                call self%write('emaxdos', self%emaxdos)
                call self%write('ndos', self%ndos)
                call self%write('e_small', self%e_small)
        end subroutine
        subroutine LoadDensityOfStatesScalarFields(self)
                class(DensityOfStates), intent(inout) :: self
                call self%read('emindos', self%emindos)
                call self%read('emaxdos', self%emaxdos)
                call self%read('ndos', self%ndos)
                call self%read('e_small', self%e_small)
        end subroutine
        subroutine FinalizeDensityOfStates(self)
               type(DensityOfStates), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearDensityOfStates(self)
                class(DensityOfStates), intent(inout) :: self
                type(DensityOfStates), save :: empty
                self = empty
        end subroutine
        pure elemental function IsDensityOfStatesEqual(lhs, rhs) result(iseq)
                class(DensityOfStates), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (DensityOfStates)
                       iseq = iseq .and. (lhs%emindos == rhs%emindos)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%emaxdos == rhs%emaxdos)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ndos == rhs%ndos)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%e_small == rhs%e_small)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorDensityOfStates(lhs, rhs)
                class(DensityOfStates), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (DensityOfStates)
                       lhs%emindos = rhs%emindos
                       lhs%emaxdos = rhs%emaxdos
                       lhs%ndos = rhs%ndos
                       lhs%e_small = rhs%e_small
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateDensityOfStatesObjectFields(self)
                class(DensityOfStates), intent(inout) :: self
        end subroutine


        subroutine InitMultiSCF(self)
                class(MultiSCF), intent(inout) :: self
                call self%InitPersistent()
                self%v_v0 =  real(1.000000,kind=16)
        end subroutine
        subroutine StoreMultiSCFObjectFields(self)
                class(MultiSCF), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadMultiSCFObjectFields(self)
                class(MultiSCF), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetMultiSCFSectionFields(self)
                class(MultiSCF), intent(inout) :: self
        end subroutine
        subroutine DisconnectMultiSCFObjectFields(self)
                class(MultiSCF), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreMultiSCFScalarFields(self)
                class(MultiSCF), intent(inout) :: self
                call self%write('v_v0', self%v_v0)
        end subroutine
        subroutine LoadMultiSCFScalarFields(self)
                class(MultiSCF), intent(inout) :: self
                call self%read('v_v0', self%v_v0)
        end subroutine
        subroutine FinalizeMultiSCF(self)
               type(MultiSCF), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearMultiSCF(self)
                class(MultiSCF), intent(inout) :: self
                type(MultiSCF), save :: empty
                self = empty
        end subroutine
        pure elemental function IsMultiSCFEqual(lhs, rhs) result(iseq)
                class(MultiSCF), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (MultiSCF)
                       iseq = iseq .and. (lhs%v_v0 == rhs%v_v0)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorMultiSCF(lhs, rhs)
                class(MultiSCF), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (MultiSCF)
                       lhs%v_v0 = rhs%v_v0
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateMultiSCFObjectFields(self)
                class(MultiSCF), intent(inout) :: self
        end subroutine


        subroutine InitMagnetism(self)
                class(Magnetism), intent(inout) :: self
                call self%InitPersistent()
                self%b_extval =  real(0.000000,kind=16)
                self%iter_h_ext =  int(0,kind=int32)
        end subroutine
        subroutine StoreMagnetismObjectFields(self)
                class(Magnetism), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%b_ext%StoreObject(ps, gid,  'b_ext')
        end subroutine
        subroutine LoadMagnetismObjectFields(self)
                class(Magnetism), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%b_ext%LoadObject(ps, gid,  'b_ext')
        end subroutine
        subroutine ResetMagnetismSectionFields(self)
                class(Magnetism), intent(inout) :: self
                self%b_ext_ => self%b_ext%GetWithExtents(self%GetMagnetismb_extExtents())
        end subroutine
        subroutine DisconnectMagnetismObjectFields(self)
                class(Magnetism), intent(inout) :: self
               type(iterator) :: iter
                call self%b_ext%DisconnectFromStore()
        end subroutine
        subroutine StoreMagnetismScalarFields(self)
                class(Magnetism), intent(inout) :: self
                call self%write('b_extval', self%b_extval)
                call self%write('iter_h_ext', self%iter_h_ext)
        end subroutine
        subroutine LoadMagnetismScalarFields(self)
                class(Magnetism), intent(inout) :: self
                call self%read('b_extval', self%b_extval)
                call self%read('iter_h_ext', self%iter_h_ext)
        end subroutine
        subroutine FinalizeMagnetism(self)
               type(Magnetism), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearMagnetism(self)
                class(Magnetism), intent(inout) :: self
                type(Magnetism), save :: empty
                self = empty
        end subroutine
        pure elemental function IsMagnetismEqual(lhs, rhs) result(iseq)
                class(Magnetism), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Magnetism)
                       iseq = iseq .and. (lhs%b_extval == rhs%b_extval)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%iter_h_ext == rhs%iter_h_ext)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%b_ext == rhs%b_ext)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorMagnetism(lhs, rhs)
                class(Magnetism), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Magnetism)
                       lhs%b_extval = rhs%b_extval
                       lhs%iter_h_ext = rhs%iter_h_ext
                       lhs%b_ext = rhs%b_ext
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetMagnetismB_extExtents(self) result(res)
                class(Magnetism), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,3)]
        end function
        subroutine AllocateMagnetismObjectFields(self)
                class(Magnetism), intent(inout) :: self
                call self%b_ext%init(int(3))
        end subroutine


        subroutine InitCoulomb(self)
                class(Coulomb), intent(inout) :: self
                call self%InitPersistent()
                self%eps_coul =  real(0.000100,kind=16)
        end subroutine
        subroutine StoreCoulombObjectFields(self)
                class(Coulomb), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadCoulombObjectFields(self)
                class(Coulomb), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetCoulombSectionFields(self)
                class(Coulomb), intent(inout) :: self
        end subroutine
        subroutine DisconnectCoulombObjectFields(self)
                class(Coulomb), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreCoulombScalarFields(self)
                class(Coulomb), intent(inout) :: self
                call self%write('eps_coul', self%eps_coul)
        end subroutine
        subroutine LoadCoulombScalarFields(self)
                class(Coulomb), intent(inout) :: self
                call self%read('eps_coul', self%eps_coul)
        end subroutine
        subroutine FinalizeCoulomb(self)
               type(Coulomb), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearCoulomb(self)
                class(Coulomb), intent(inout) :: self
                type(Coulomb), save :: empty
                self = empty
        end subroutine
        pure elemental function IsCoulombEqual(lhs, rhs) result(iseq)
                class(Coulomb), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Coulomb)
                       iseq = iseq .and. (lhs%eps_coul == rhs%eps_coul)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorCoulomb(lhs, rhs)
                class(Coulomb), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Coulomb)
                       lhs%eps_coul = rhs%eps_coul
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateCoulombObjectFields(self)
                class(Coulomb), intent(inout) :: self
        end subroutine


        subroutine InitTauMesh(self)
                class(TauMesh), intent(inout) :: self
                call self%InitPersistent()
                self%n_tau =  int(46,kind=int32)
                self%exp_tau_gw =  real(4.000000,kind=16)
        end subroutine
        subroutine StoreTauMeshObjectFields(self)
                class(TauMesh), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadTauMeshObjectFields(self)
                class(TauMesh), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetTauMeshSectionFields(self)
                class(TauMesh), intent(inout) :: self
        end subroutine
        subroutine DisconnectTauMeshObjectFields(self)
                class(TauMesh), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreTauMeshScalarFields(self)
                class(TauMesh), intent(inout) :: self
                call self%write('n_tau', self%n_tau)
                call self%write('exp_tau_gw', self%exp_tau_gw)
        end subroutine
        subroutine LoadTauMeshScalarFields(self)
                class(TauMesh), intent(inout) :: self
                call self%read('n_tau', self%n_tau)
                call self%read('exp_tau_gw', self%exp_tau_gw)
        end subroutine
        subroutine FinalizeTauMesh(self)
               type(TauMesh), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearTauMesh(self)
                class(TauMesh), intent(inout) :: self
                type(TauMesh), save :: empty
                self = empty
        end subroutine
        pure elemental function IsTauMeshEqual(lhs, rhs) result(iseq)
                class(TauMesh), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (TauMesh)
                       iseq = iseq .and. (lhs%n_tau == rhs%n_tau)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%exp_tau_gw == rhs%exp_tau_gw)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorTauMesh(lhs, rhs)
                class(TauMesh), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (TauMesh)
                       lhs%n_tau = rhs%n_tau
                       lhs%exp_tau_gw = rhs%exp_tau_gw
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateTauMeshObjectFields(self)
                class(TauMesh), intent(inout) :: self
        end subroutine


        subroutine InitOmegaMesh(self)
                class(OmegaMesh), intent(inout) :: self
                call self%InitPersistent()
                self%n_omega_exa =  int(10,kind=int32)
                self%n_omega_asy =  int(36,kind=int32)
                self%omega_max =  real(50.000000,kind=16)
        end subroutine
        subroutine StoreOmegaMeshObjectFields(self)
                class(OmegaMesh), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadOmegaMeshObjectFields(self)
                class(OmegaMesh), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetOmegaMeshSectionFields(self)
                class(OmegaMesh), intent(inout) :: self
        end subroutine
        subroutine DisconnectOmegaMeshObjectFields(self)
                class(OmegaMesh), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreOmegaMeshScalarFields(self)
                class(OmegaMesh), intent(inout) :: self
                call self%write('n_omega_exa', self%n_omega_exa)
                call self%write('n_omega_asy', self%n_omega_asy)
                call self%write('omega_max', self%omega_max)
        end subroutine
        subroutine LoadOmegaMeshScalarFields(self)
                class(OmegaMesh), intent(inout) :: self
                call self%read('n_omega_exa', self%n_omega_exa)
                call self%read('n_omega_asy', self%n_omega_asy)
                call self%read('omega_max', self%omega_max)
        end subroutine
        subroutine FinalizeOmegaMesh(self)
               type(OmegaMesh), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOmegaMesh(self)
                class(OmegaMesh), intent(inout) :: self
                type(OmegaMesh), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOmegaMeshEqual(lhs, rhs) result(iseq)
                class(OmegaMesh), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (OmegaMesh)
                       iseq = iseq .and. (lhs%n_omega_exa == rhs%n_omega_exa)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%n_omega_asy == rhs%n_omega_asy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega_max == rhs%omega_max)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOmegaMesh(lhs, rhs)
                class(OmegaMesh), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (OmegaMesh)
                       lhs%n_omega_exa = rhs%n_omega_exa
                       lhs%n_omega_asy = rhs%n_omega_asy
                       lhs%omega_max = rhs%omega_max
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateOmegaMeshObjectFields(self)
                class(OmegaMesh), intent(inout) :: self
        end subroutine


        subroutine InitNuMesh(self)
                class(NuMesh), intent(inout) :: self
                call self%InitPersistent()
                self%n_nu_exa =  int(10,kind=int32)
                self%n_nu_geom =  int(30,kind=int32)
                self%n_nu_asy =  int(6,kind=int32)
                self%nu_geom =  real(100.000000,kind=16)
                self%nu_max =  real(400.000000,kind=16)
        end subroutine
        subroutine StoreNuMeshObjectFields(self)
                class(NuMesh), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadNuMeshObjectFields(self)
                class(NuMesh), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetNuMeshSectionFields(self)
                class(NuMesh), intent(inout) :: self
        end subroutine
        subroutine DisconnectNuMeshObjectFields(self)
                class(NuMesh), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreNuMeshScalarFields(self)
                class(NuMesh), intent(inout) :: self
                call self%write('n_nu_exa', self%n_nu_exa)
                call self%write('n_nu_geom', self%n_nu_geom)
                call self%write('n_nu_asy', self%n_nu_asy)
                call self%write('nu_geom', self%nu_geom)
                call self%write('nu_max', self%nu_max)
        end subroutine
        subroutine LoadNuMeshScalarFields(self)
                class(NuMesh), intent(inout) :: self
                call self%read('n_nu_exa', self%n_nu_exa)
                call self%read('n_nu_geom', self%n_nu_geom)
                call self%read('n_nu_asy', self%n_nu_asy)
                call self%read('nu_geom', self%nu_geom)
                call self%read('nu_max', self%nu_max)
        end subroutine
        subroutine FinalizeNuMesh(self)
               type(NuMesh), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearNuMesh(self)
                class(NuMesh), intent(inout) :: self
                type(NuMesh), save :: empty
                self = empty
        end subroutine
        pure elemental function IsNuMeshEqual(lhs, rhs) result(iseq)
                class(NuMesh), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (NuMesh)
                       iseq = iseq .and. (lhs%n_nu_exa == rhs%n_nu_exa)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%n_nu_geom == rhs%n_nu_geom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%n_nu_asy == rhs%n_nu_asy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nu_geom == rhs%nu_geom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nu_max == rhs%nu_max)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorNuMesh(lhs, rhs)
                class(NuMesh), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (NuMesh)
                       lhs%n_nu_exa = rhs%n_nu_exa
                       lhs%n_nu_geom = rhs%n_nu_geom
                       lhs%n_nu_asy = rhs%n_nu_asy
                       lhs%nu_geom = rhs%nu_geom
                       lhs%nu_max = rhs%nu_max
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateNuMeshObjectFields(self)
                class(NuMesh), intent(inout) :: self
        end subroutine


        subroutine InitAFMSymmetry(self)
                class(AFMSymmetry), intent(inout) :: self
                call self%InitPersistent()
                self%magmom =  0.0d0
                self%site =  0
                self%opposite_site =  0
        end subroutine
        subroutine StoreAFMSymmetryObjectFields(self)
                class(AFMSymmetry), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%op%StoreObject(ps, gid,  'op')
        end subroutine
        subroutine LoadAFMSymmetryObjectFields(self)
                class(AFMSymmetry), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%op%LoadObject(ps, gid,  'op')
        end subroutine
        subroutine ResetAFMSymmetrySectionFields(self)
                class(AFMSymmetry), intent(inout) :: self
                self%op_ => self%op%GetWithExtents(self%GetAFMSymmetryopExtents())
        end subroutine
        subroutine DisconnectAFMSymmetryObjectFields(self)
                class(AFMSymmetry), intent(inout) :: self
               type(iterator) :: iter
                call self%op%DisconnectFromStore()
        end subroutine
        subroutine StoreAFMSymmetryScalarFields(self)
                class(AFMSymmetry), intent(inout) :: self
                call self%write('magmom', self%magmom)
                call self%write('site', self%site)
                call self%write('opposite_site', self%opposite_site)
        end subroutine
        subroutine LoadAFMSymmetryScalarFields(self)
                class(AFMSymmetry), intent(inout) :: self
                call self%read('magmom', self%magmom)
                call self%read('site', self%site)
                call self%read('opposite_site', self%opposite_site)
        end subroutine
        subroutine FinalizeAFMSymmetry(self)
               type(AFMSymmetry), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearAFMSymmetry(self)
                class(AFMSymmetry), intent(inout) :: self
                type(AFMSymmetry), save :: empty
                self = empty
        end subroutine
        pure elemental function IsAFMSymmetryEqual(lhs, rhs) result(iseq)
                class(AFMSymmetry), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (AFMSymmetry)
                       iseq = iseq .and. (lhs%magmom == rhs%magmom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%site == rhs%site)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%opposite_site == rhs%opposite_site)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%op == rhs%op)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorAFMSymmetry(lhs, rhs)
                class(AFMSymmetry), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (AFMSymmetry)
                       lhs%magmom = rhs%magmom
                       lhs%site = rhs%site
                       lhs%opposite_site = rhs%opposite_site
                       lhs%op = rhs%op
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetAfmsymmetryOpExtents(self) result(res)
                class(AFMSymmetry), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,4),extent(1,4)]
        end function
        subroutine AllocateAFMSymmetryObjectFields(self)
                class(AFMSymmetry), intent(inout) :: self
                call self%op%init(int(4),int(4))
        end subroutine


        subroutine InitRhobustaExtra(self)
                class(RhobustaExtra), intent(inout) :: self
                call self%InitPersistent()
                self%has_magnetic_field = .False.
                self%is_corr_spin_polarized = .False.
                self%is_dft_spin_polarized = .False.
                self%local_off_diagonals = .False.
                self%simplify_diagonal = .False.
        end subroutine
        subroutine StoreRhobustaExtraObjectFields(self)
                class(RhobustaExtra), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadRhobustaExtraObjectFields(self)
                class(RhobustaExtra), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetRhobustaExtraSectionFields(self)
                class(RhobustaExtra), intent(inout) :: self
        end subroutine
        subroutine DisconnectRhobustaExtraObjectFields(self)
                class(RhobustaExtra), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreRhobustaExtraScalarFields(self)
                class(RhobustaExtra), intent(inout) :: self
                call self%write('has_magnetic_field', self%has_magnetic_field)
                call self%write('is_corr_spin_polarized', self%is_corr_spin_polarized)
                call self%write('is_dft_spin_polarized', self%is_dft_spin_polarized)
                call self%write('local_off_diagonals', self%local_off_diagonals)
                call self%write('simplify_diagonal', self%simplify_diagonal)
        end subroutine
        subroutine LoadRhobustaExtraScalarFields(self)
                class(RhobustaExtra), intent(inout) :: self
                call self%read('has_magnetic_field', self%has_magnetic_field)
                call self%read('is_corr_spin_polarized', self%is_corr_spin_polarized)
                call self%read('is_dft_spin_polarized', self%is_dft_spin_polarized)
                call self%read('local_off_diagonals', self%local_off_diagonals)
                call self%read('simplify_diagonal', self%simplify_diagonal)
        end subroutine
        subroutine FinalizeRhobustaExtra(self)
               type(RhobustaExtra), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearRhobustaExtra(self)
                class(RhobustaExtra), intent(inout) :: self
                type(RhobustaExtra), save :: empty
                self = empty
        end subroutine
        pure elemental function IsRhobustaExtraEqual(lhs, rhs) result(iseq)
                class(RhobustaExtra), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (RhobustaExtra)
                       iseq = iseq .and. (lhs%has_magnetic_field .eqv. rhs%has_magnetic_field)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%is_corr_spin_polarized .eqv. rhs%is_corr_spin_polarized)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%is_dft_spin_polarized .eqv. rhs%is_dft_spin_polarized)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%local_off_diagonals .eqv. rhs%local_off_diagonals)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%simplify_diagonal .eqv. rhs%simplify_diagonal)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorRhobustaExtra(lhs, rhs)
                class(RhobustaExtra), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (RhobustaExtra)
                       lhs%has_magnetic_field = rhs%has_magnetic_field
                       lhs%is_corr_spin_polarized = rhs%is_corr_spin_polarized
                       lhs%is_dft_spin_polarized = rhs%is_dft_spin_polarized
                       lhs%local_off_diagonals = rhs%local_off_diagonals
                       lhs%simplify_diagonal = rhs%simplify_diagonal
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateRhobustaExtraObjectFields(self)
                class(RhobustaExtra), intent(inout) :: self
        end subroutine


        subroutine InitAFMExtra(self)
                class(AFMExtra), intent(inout) :: self
                call self%InitPersistent()
                self%num_afm =  int(0,kind=int32)
        end subroutine
        subroutine StoreAFMExtraObjectFields(self)
                class(AFMExtra), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                call iter%Init(lbound(self%afm_symm), ubound(self%afm_symm))
                do while (.not. iter%Done())
                    call self%afm_symm(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'afm_symm', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadAFMExtraObjectFields(self)
                class(AFMExtra), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                allocate(self%afm_symm(int(1):int(self%num_afm)))
                call iter%Init(lbound(self%afm_symm), ubound(self%afm_symm))
                do while (.not. iter%Done())
                    call self%afm_symm(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'afm_symm', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetAFMExtraSectionFields(self)
                class(AFMExtra), intent(inout) :: self
        end subroutine
        subroutine DisconnectAFMExtraObjectFields(self)
                class(AFMExtra), intent(inout) :: self
               type(iterator) :: iter

                call iter%Init(lbound(self%afm_symm), ubound(self%afm_symm))
                do while (.not. iter%Done())
                    call self%afm_symm(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreAFMExtraScalarFields(self)
                class(AFMExtra), intent(inout) :: self
                call self%write('num_afm', self%num_afm)
        end subroutine
        subroutine LoadAFMExtraScalarFields(self)
                class(AFMExtra), intent(inout) :: self
                call self%read('num_afm', self%num_afm)
        end subroutine
        subroutine FinalizeAFMExtra(self)
               type(AFMExtra), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearAFMExtra(self)
                class(AFMExtra), intent(inout) :: self
                type(AFMExtra), save :: empty
                self = empty
        end subroutine
        pure elemental function IsAFMExtraEqual(lhs, rhs) result(iseq)
                class(AFMExtra), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (AFMExtra)
                       iseq = iseq .and. (lhs%num_afm == rhs%num_afm)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%afm_symm) .eqv. allocated(rhs%afm_symm))
                       if (.not. iseq) return
                       if (allocated(lhs%afm_symm)) then
                           iseq = iseq .and. all(shape(lhs%afm_symm) == shape(rhs%afm_symm))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%afm_symm(:) == rhs%afm_symm(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorAFMExtra(lhs, rhs)
                class(AFMExtra), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (AFMExtra)
                       lhs%num_afm = rhs%num_afm
                       lhs%afm_symm = rhs%afm_symm
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateAFMExtraObjectFields(self)
                class(AFMExtra), intent(inout) :: self
                allocate(self%afm_symm(int(1):int(self%num_afm)))
        end subroutine


        subroutine InitInput(self)
                class(Input), intent(inout) :: self
                call self%InitPersistent()
                call self%ctrl%InitPersistent()
                call self%strct%InitPersistent()
                call self%sgg%InitPersistent()
                call self%rsm%InitPersistent()
                call self%bas%InitPersistent()
                call self%zns%InitPersistent()
                call self%bp%InitPersistent()
                call self%dos%InitPersistent()
                call self%kg%InitPersistent()
                call self%mscf%InitPersistent()
                call self%mag%InitPersistent()
                call self%coul%InitPersistent()
                call self%tmesh%InitPersistent()
                call self%omesh%InitPersistent()
                call self%nmesh%InitPersistent()
        end subroutine
        subroutine StoreInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ctrl%store(ps%FetchSubGroup(gid,  'ctrl'))
                call self%strct%store(ps%FetchSubGroup(gid,  'strct'))
                call self%sgg%store(ps%FetchSubGroup(gid,  'sgg'))
                call self%rsm%store(ps%FetchSubGroup(gid,  'rsm'))
                call self%bas%store(ps%FetchSubGroup(gid,  'bas'))
                call self%zns%store(ps%FetchSubGroup(gid,  'zns'))
                call self%bp%store(ps%FetchSubGroup(gid,  'bp'))
                call self%dos%store(ps%FetchSubGroup(gid,  'dos'))
                call self%kg%store(ps%FetchSubGroup(gid,  'kg'))
                call self%mscf%store(ps%FetchSubGroup(gid,  'mscf'))
                call self%mag%store(ps%FetchSubGroup(gid,  'mag'))
                call self%coul%store(ps%FetchSubGroup(gid,  'coul'))
                call self%tmesh%store(ps%FetchSubGroup(gid,  'tmesh'))
                call self%omesh%store(ps%FetchSubGroup(gid,  'omesh'))
                call self%nmesh%store(ps%FetchSubGroup(gid,  'nmesh'))
        end subroutine
        subroutine LoadInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ctrl%load(ps%FetchSubGroup(gid,  'ctrl'))
                call self%strct%load(ps%FetchSubGroup(gid,  'strct'))
                call self%sgg%load(ps%FetchSubGroup(gid,  'sgg'))
                call self%rsm%load(ps%FetchSubGroup(gid,  'rsm'))
                call self%bas%load(ps%FetchSubGroup(gid,  'bas'))
                call self%zns%load(ps%FetchSubGroup(gid,  'zns'))
                call self%bp%load(ps%FetchSubGroup(gid,  'bp'))
                call self%dos%load(ps%FetchSubGroup(gid,  'dos'))
                call self%kg%load(ps%FetchSubGroup(gid,  'kg'))
                call self%mscf%load(ps%FetchSubGroup(gid,  'mscf'))
                call self%mag%load(ps%FetchSubGroup(gid,  'mag'))
                call self%coul%load(ps%FetchSubGroup(gid,  'coul'))
                call self%tmesh%load(ps%FetchSubGroup(gid,  'tmesh'))
                call self%omesh%load(ps%FetchSubGroup(gid,  'omesh'))
                call self%nmesh%load(ps%FetchSubGroup(gid,  'nmesh'))
        end subroutine
        subroutine ResetInputSectionFields(self)
                class(Input), intent(inout) :: self
        end subroutine
        subroutine DisconnectInputObjectFields(self)
                class(Input), intent(inout) :: self
               type(iterator) :: iter
                call self%ctrl%disconnect()
                call self%strct%disconnect()
                call self%sgg%disconnect()
                call self%rsm%disconnect()
                call self%bas%disconnect()
                call self%zns%disconnect()
                call self%bp%disconnect()
                call self%dos%disconnect()
                call self%kg%disconnect()
                call self%mscf%disconnect()
                call self%mag%disconnect()
                call self%coul%disconnect()
                call self%tmesh%disconnect()
                call self%omesh%disconnect()
                call self%nmesh%disconnect()
        end subroutine
        subroutine StoreInputScalarFields(self)
                class(Input), intent(inout) :: self
                call self%write('text', self%text)
                call self%write('allfile', self%allfile)
        end subroutine
        subroutine LoadInputScalarFields(self)
                class(Input), intent(inout) :: self
                call self%read('text', self%text)
                call self%read('allfile', self%allfile)
        end subroutine
        subroutine FinalizeInput(self)
               type(Input), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearInput(self)
                class(Input), intent(inout) :: self
                type(Input), save :: empty
                self = empty
        end subroutine
        pure elemental function IsInputEqual(lhs, rhs) result(iseq)
                class(Input), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Input)
                       iseq = iseq .and. (lhs%text == rhs%text)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%allfile == rhs%allfile)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ctrl == rhs%ctrl)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%strct == rhs%strct)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sgg == rhs%sgg)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rsm == rhs%rsm)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%bas == rhs%bas)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%zns == rhs%zns)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%bp == rhs%bp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dos == rhs%dos)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%kg == rhs%kg)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%mscf == rhs%mscf)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%mag == rhs%mag)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%coul == rhs%coul)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%tmesh == rhs%tmesh)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omesh == rhs%omesh)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nmesh == rhs%nmesh)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorInput(lhs, rhs)
                class(Input), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Input)
                       lhs%text = rhs%text
                       lhs%allfile = rhs%allfile
                       lhs%ctrl = rhs%ctrl
                       lhs%strct = rhs%strct
                       lhs%sgg = rhs%sgg
                       lhs%rsm = rhs%rsm
                       lhs%bas = rhs%bas
                       lhs%zns = rhs%zns
                       lhs%bp = rhs%bp
                       lhs%dos = rhs%dos
                       lhs%kg = rhs%kg
                       lhs%mscf = rhs%mscf
                       lhs%mag = rhs%mag
                       lhs%coul = rhs%coul
                       lhs%tmesh = rhs%tmesh
                       lhs%omesh = rhs%omesh
                       lhs%nmesh = rhs%nmesh
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateInputObjectFields(self)
                class(Input), intent(inout) :: self
        end subroutine


        subroutine InitExpansionSizes(self)
                class(ExpansionSizes), intent(inout) :: self
                call self%InitPersistent()
                self%npnt =  0
                self%nbndf =  0
                self%nspin =  0
                self%nbasmpw =  0
                self%nfun =  0
        end subroutine
        subroutine StoreExpansionSizesObjectFields(self)
                class(ExpansionSizes), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadExpansionSizesObjectFields(self)
                class(ExpansionSizes), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetExpansionSizesSectionFields(self)
                class(ExpansionSizes), intent(inout) :: self
        end subroutine
        subroutine DisconnectExpansionSizesObjectFields(self)
                class(ExpansionSizes), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreExpansionSizesScalarFields(self)
                class(ExpansionSizes), intent(inout) :: self
                call self%write('npnt', self%npnt)
                call self%write('nbndf', self%nbndf)
                call self%write('nspin', self%nspin)
                call self%write('nbasmpw', self%nbasmpw)
                call self%write('nfun', self%nfun)
        end subroutine
        subroutine LoadExpansionSizesScalarFields(self)
                class(ExpansionSizes), intent(inout) :: self
                call self%read('npnt', self%npnt)
                call self%read('nbndf', self%nbndf)
                call self%read('nspin', self%nspin)
                call self%read('nbasmpw', self%nbasmpw)
                call self%read('nfun', self%nfun)
        end subroutine
        subroutine FinalizeExpansionSizes(self)
               type(ExpansionSizes), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearExpansionSizes(self)
                class(ExpansionSizes), intent(inout) :: self
                type(ExpansionSizes), save :: empty
                self = empty
        end subroutine
        pure elemental function IsExpansionSizesEqual(lhs, rhs) result(iseq)
                class(ExpansionSizes), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ExpansionSizes)
                       iseq = iseq .and. (lhs%npnt == rhs%npnt)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nbndf == rhs%nbndf)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nspin == rhs%nspin)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nbasmpw == rhs%nbasmpw)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nfun == rhs%nfun)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorExpansionSizes(lhs, rhs)
                class(ExpansionSizes), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ExpansionSizes)
                       lhs%npnt = rhs%npnt
                       lhs%nbndf = rhs%nbndf
                       lhs%nspin = rhs%nspin
                       lhs%nbasmpw = rhs%nbasmpw
                       lhs%nfun = rhs%nfun
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateExpansionSizesObjectFields(self)
                class(ExpansionSizes), intent(inout) :: self
        end subroutine


        subroutine InitSizes(self)
                class(Sizes), intent(inout) :: self
                call self%ExpansionSizes%Init()
                self%ncormax =  0
                self%nspin_0 =  0
                self%nsort =  0
                self%maxmtcor =  0
                self%maxel =  0
                self%natom =  0
                self%maxwf =  0
                self%nlmb =  0
                self%maxntle =  0
                self%maxnrad =  0
                self%has_gf0 = .False.
        end subroutine
        subroutine StoreSizesObjectFields(self)
                class(Sizes), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ExpansionSizes%StoreObjectFields()
        end subroutine
        subroutine LoadSizesObjectFields(self)
                class(Sizes), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ExpansionSizes%LoadObjectFields()
        end subroutine
        subroutine ResetSizesSectionFields(self)
                class(Sizes), intent(inout) :: self
                call self%ExpansionSizes%ResetSectionFields()
        end subroutine
        subroutine DisconnectSizesObjectFields(self)
                class(Sizes), intent(inout) :: self
               type(iterator) :: iter
                call self%ExpansionSizes%DisconnectObjectFields()
        end subroutine
        subroutine StoreSizesScalarFields(self)
                class(Sizes), intent(inout) :: self
                call self%ExpansionSizes%StoreScalarFields()
                call self%write('ncormax', self%ncormax)
                call self%write('nspin_0', self%nspin_0)
                call self%write('nsort', self%nsort)
                call self%write('maxmtcor', self%maxmtcor)
                call self%write('maxel', self%maxel)
                call self%write('natom', self%natom)
                call self%write('maxwf', self%maxwf)
                call self%write('nlmb', self%nlmb)
                call self%write('maxntle', self%maxntle)
                call self%write('maxnrad', self%maxnrad)
                call self%write('has_gf0', self%has_gf0)
        end subroutine
        subroutine LoadSizesScalarFields(self)
                class(Sizes), intent(inout) :: self
                call self%ExpansionSizes%LoadScalarFields()
                call self%read('ncormax', self%ncormax)
                call self%read('nspin_0', self%nspin_0)
                call self%read('nsort', self%nsort)
                call self%read('maxmtcor', self%maxmtcor)
                call self%read('maxel', self%maxel)
                call self%read('natom', self%natom)
                call self%read('maxwf', self%maxwf)
                call self%read('nlmb', self%nlmb)
                call self%read('maxntle', self%maxntle)
                call self%read('maxnrad', self%maxnrad)
                call self%read('has_gf0', self%has_gf0)
        end subroutine
        subroutine FinalizeSizes(self)
               type(Sizes), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSizes(self)
                class(Sizes), intent(inout) :: self
                type(Sizes), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSizesEqual(lhs, rhs) result(iseq)
                class(Sizes), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Sizes)
                       iseq = iseq .and. (lhs%ExpansionSizes == rhs%ExpansionSizes)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ncormax == rhs%ncormax)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nspin_0 == rhs%nspin_0)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nsort == rhs%nsort)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxmtcor == rhs%maxmtcor)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxel == rhs%maxel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%natom == rhs%natom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxwf == rhs%maxwf)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nlmb == rhs%nlmb)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxntle == rhs%maxntle)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxnrad == rhs%maxnrad)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%has_gf0 .eqv. rhs%has_gf0)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSizes(lhs, rhs)
                class(Sizes), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Sizes)
                       lhs%ExpansionSizes = rhs%ExpansionSizes
                       lhs%ncormax = rhs%ncormax
                       lhs%nspin_0 = rhs%nspin_0
                       lhs%nsort = rhs%nsort
                       lhs%maxmtcor = rhs%maxmtcor
                       lhs%maxel = rhs%maxel
                       lhs%natom = rhs%natom
                       lhs%maxwf = rhs%maxwf
                       lhs%nlmb = rhs%nlmb
                       lhs%maxntle = rhs%maxntle
                       lhs%maxnrad = rhs%maxnrad
                       lhs%has_gf0 = rhs%has_gf0
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateSizesObjectFields(self)
                class(Sizes), intent(inout) :: self
                call self%ExpansionSizes%AllocateObjectFields()
        end subroutine


        subroutine InitElDensity(self)
                class(ElDensity), intent(inout) :: self
                call self%InitPersistent()
                self%maxmt =  0
                self%nspin_0 =  int(1,kind=int32)
                self%maxmtb =  int(1,kind=int32)
                self%nplwro =  int(1,kind=int32)
                self%nrmax =  int(0,kind=int32)
                self%nsort =  int(0,kind=int32)
        end subroutine
        subroutine StoreElDensityObjectFields(self)
                class(ElDensity), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ro%StoreObject(ps, gid,  'ro')
                call self%rointr%StoreObject(ps, gid,  'rointr')
                call self%spmt%StoreObject(ps, gid,  'spmt')
                call self%spintr%StoreObject(ps, gid,  'spintr')
        end subroutine
        subroutine LoadElDensityObjectFields(self)
                class(ElDensity), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ro%LoadObject(ps, gid,  'ro')
                call self%rointr%LoadObject(ps, gid,  'rointr')
                call self%spmt%LoadObject(ps, gid,  'spmt')
                call self%spintr%LoadObject(ps, gid,  'spintr')
        end subroutine
        subroutine ResetElDensitySectionFields(self)
                class(ElDensity), intent(inout) :: self
                self%ro_ => self%ro%GetWithExtents(self%GetElDensityroExtents())
                self%rointr_ => self%rointr%GetWithExtents(self%GetElDensityrointrExtents())
                self%spmt_ => self%spmt%GetWithExtents(self%GetElDensityspmtExtents())
                self%spintr_ => self%spintr%GetWithExtents(self%GetElDensityspintrExtents())
        end subroutine
        subroutine DisconnectElDensityObjectFields(self)
                class(ElDensity), intent(inout) :: self
               type(iterator) :: iter
                call self%ro%DisconnectFromStore()
                call self%rointr%DisconnectFromStore()
                call self%spmt%DisconnectFromStore()
                call self%spintr%DisconnectFromStore()
        end subroutine
        subroutine StoreElDensityScalarFields(self)
                class(ElDensity), intent(inout) :: self
                call self%write('maxmt', self%maxmt)
                call self%write('nspin_0', self%nspin_0)
                call self%write('maxmtb', self%maxmtb)
                call self%write('nplwro', self%nplwro)
                call self%write('nrmax', self%nrmax)
                call self%write('nsort', self%nsort)
        end subroutine
        subroutine LoadElDensityScalarFields(self)
                class(ElDensity), intent(inout) :: self
                call self%read('maxmt', self%maxmt)
                call self%read('nspin_0', self%nspin_0)
                call self%read('maxmtb', self%maxmtb)
                call self%read('nplwro', self%nplwro)
                call self%read('nrmax', self%nrmax)
                call self%read('nsort', self%nsort)
        end subroutine
        subroutine FinalizeElDensity(self)
               type(ElDensity), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearElDensity(self)
                class(ElDensity), intent(inout) :: self
                type(ElDensity), save :: empty
                self = empty
        end subroutine
        pure elemental function IsElDensityEqual(lhs, rhs) result(iseq)
                class(ElDensity), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ElDensity)
                       iseq = iseq .and. (lhs%maxmt == rhs%maxmt)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nspin_0 == rhs%nspin_0)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxmtb == rhs%maxmtb)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nplwro == rhs%nplwro)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrmax == rhs%nrmax)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nsort == rhs%nsort)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ro == rhs%ro)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rointr == rhs%rointr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%spmt == rhs%spmt)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%spintr == rhs%spintr)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorElDensity(lhs, rhs)
                class(ElDensity), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ElDensity)
                       lhs%maxmt = rhs%maxmt
                       lhs%nspin_0 = rhs%nspin_0
                       lhs%maxmtb = rhs%maxmtb
                       lhs%nplwro = rhs%nplwro
                       lhs%nrmax = rhs%nrmax
                       lhs%nsort = rhs%nsort
                       lhs%ro = rhs%ro
                       lhs%rointr = rhs%rointr
                       lhs%spmt = rhs%spmt
                       lhs%spintr = rhs%spintr
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetEldensityRoExtents(self) result(res)
                class(ElDensity), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%maxmt)]
        end function
        function GetEldensityRointrExtents(self) result(res)
                class(ElDensity), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%nplwro),extent(1,self%nspin_0)]
        end function
        function GetEldensitySpmtExtents(self) result(res)
                class(ElDensity), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%maxmtb)]
        end function
        function GetEldensitySpintrExtents(self) result(res)
                class(ElDensity), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%nplwro)]
        end function
        subroutine AllocateElDensityObjectFields(self)
                class(ElDensity), intent(inout) :: self
                call self%ro%init(int(self%maxmt))
                call self%rointr%init(int(self%nplwro),int(self%nspin_0))
                call self%spmt%init(int(self%maxmtb))
                call self%spintr%init(int(3),int(self%nplwro))
        end subroutine


        subroutine InitElDensityWithCore(self)
                class(ElDensityWithCore), intent(inout) :: self
                call self%InitPersistent()
                self%nrmax =  int(0,kind=int32)
                self%nsort =  int(0,kind=int32)
                self%nspin_0 =  int(1,kind=int32)
        end subroutine
        subroutine StoreElDensityWithCoreObjectFields(self)
                class(ElDensityWithCore), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ro_core%StoreObject(ps, gid,  'ro_core')
        end subroutine
        subroutine LoadElDensityWithCoreObjectFields(self)
                class(ElDensityWithCore), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ro_core%LoadObject(ps, gid,  'ro_core')
        end subroutine
        subroutine ResetElDensityWithCoreSectionFields(self)
                class(ElDensityWithCore), intent(inout) :: self
                self%ro_core_ => self%ro_core%GetWithExtents(self%GetElDensityWithCorero_coreExtents())
        end subroutine
        subroutine DisconnectElDensityWithCoreObjectFields(self)
                class(ElDensityWithCore), intent(inout) :: self
               type(iterator) :: iter
                call self%ro_core%DisconnectFromStore()
        end subroutine
        subroutine StoreElDensityWithCoreScalarFields(self)
                class(ElDensityWithCore), intent(inout) :: self
                call self%write('nrmax', self%nrmax)
                call self%write('nsort', self%nsort)
                call self%write('nspin_0', self%nspin_0)
        end subroutine
        subroutine LoadElDensityWithCoreScalarFields(self)
                class(ElDensityWithCore), intent(inout) :: self
                call self%read('nrmax', self%nrmax)
                call self%read('nsort', self%nsort)
                call self%read('nspin_0', self%nspin_0)
        end subroutine
        subroutine FinalizeElDensityWithCore(self)
               type(ElDensityWithCore), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearElDensityWithCore(self)
                class(ElDensityWithCore), intent(inout) :: self
                type(ElDensityWithCore), save :: empty
                self = empty
        end subroutine
        pure elemental function IsElDensityWithCoreEqual(lhs, rhs) result(iseq)
                class(ElDensityWithCore), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ElDensityWithCore)
                       iseq = iseq .and. (lhs%nrmax == rhs%nrmax)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nsort == rhs%nsort)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nspin_0 == rhs%nspin_0)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ro_core == rhs%ro_core)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorElDensityWithCore(lhs, rhs)
                class(ElDensityWithCore), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ElDensityWithCore)
                       lhs%nrmax = rhs%nrmax
                       lhs%nsort = rhs%nsort
                       lhs%nspin_0 = rhs%nspin_0
                       lhs%ro_core = rhs%ro_core
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetEldensitywithcoreRo_coreExtents(self) result(res)
                class(ElDensityWithCore), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(0,self%nrmax),extent(1,self%nsort),extent(1,self%nspin_0)]
        end function
        subroutine AllocateElDensityWithCoreObjectFields(self)
                class(ElDensityWithCore), intent(inout) :: self
                call self%ro_core%init(int(self%nrmax- (0) + 1),int(self%nsort),int(self%nspin_0))
        end subroutine


        subroutine InitImage(self)
                class(Image), intent(inout) :: self
                call self%Sizes%Init()
                self%chem_pot =  0.0d0
                call self%rho%InitPersistent()
        end subroutine
        subroutine StoreImageObjectFields(self)
                class(Image), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Sizes%StoreObjectFields()
                call self%e_core%StoreObject(ps, gid,  'e_core')
                call self%pcor%StoreObject(ps, gid,  'pcor')
                call self%qcor%StoreObject(ps, gid,  'qcor')
                call self%n_bnd%StoreObject(ps, gid,  'n_bnd')
                call self%e_bnd%StoreObject(ps, gid,  'e_bnd')
                call self%g_loc_0%StoreObject(ps, gid,  'g_loc_0')
                call self%gfun%StoreObject(ps, gid,  'gfun')
                call self%p_f%StoreObject(ps, gid,  'p_f')
                call self%pd_f%StoreObject(ps, gid,  'pd_f')
                call self%pd2_f%StoreObject(ps, gid,  'pd2_f')
                call self%q_f%StoreObject(ps, gid,  'q_f')
                call self%qd_f%StoreObject(ps, gid,  'qd_f')
                call self%qd2_f%StoreObject(ps, gid,  'qd2_f')
                call self%gfund%StoreObject(ps, gid,  'gfund')
                call self%eny%StoreObject(ps, gid,  'eny')
                call self%ffsmt%StoreObject(ps, gid,  'ffsmt')
                call self%rho%store(ps%FetchSubGroup(gid,  'rho'))
        end subroutine
        subroutine LoadImageObjectFields(self)
                class(Image), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Sizes%LoadObjectFields()
                call self%e_core%LoadObject(ps, gid,  'e_core')
                call self%pcor%LoadObject(ps, gid,  'pcor')
                call self%qcor%LoadObject(ps, gid,  'qcor')
                call self%n_bnd%LoadObject(ps, gid,  'n_bnd')
                call self%e_bnd%LoadObject(ps, gid,  'e_bnd')
                call self%g_loc_0%LoadObject(ps, gid,  'g_loc_0')
                call self%gfun%LoadObject(ps, gid,  'gfun')
                call self%p_f%LoadObject(ps, gid,  'p_f')
                call self%pd_f%LoadObject(ps, gid,  'pd_f')
                call self%pd2_f%LoadObject(ps, gid,  'pd2_f')
                call self%q_f%LoadObject(ps, gid,  'q_f')
                call self%qd_f%LoadObject(ps, gid,  'qd_f')
                call self%qd2_f%LoadObject(ps, gid,  'qd2_f')
                call self%gfund%LoadObject(ps, gid,  'gfund')
                call self%eny%LoadObject(ps, gid,  'eny')
                call self%ffsmt%LoadObject(ps, gid,  'ffsmt')
                call self%rho%load(ps%FetchSubGroup(gid,  'rho'))
        end subroutine
        subroutine ResetImageSectionFields(self)
                class(Image), intent(inout) :: self
                call self%Sizes%ResetSectionFields()
                self%e_core_ => self%e_core%GetWithExtents(self%GetImagee_coreExtents())
                self%pcor_ => self%pcor%GetWithExtents(self%GetImagepcorExtents())
                self%qcor_ => self%qcor%GetWithExtents(self%GetImageqcorExtents())
                self%n_bnd_ => self%n_bnd%GetWithExtents(self%GetImagen_bndExtents())
                self%e_bnd_ => self%e_bnd%GetWithExtents(self%GetImagee_bndExtents())
                self%g_loc_0_ => self%g_loc_0%GetWithExtents(self%GetImageg_loc_0Extents())
                self%gfun_ => self%gfun%GetWithExtents(self%GetImagegfunExtents())
                self%p_f_ => self%p_f%GetWithExtents(self%GetImagep_fExtents())
                self%pd_f_ => self%pd_f%GetWithExtents(self%GetImagepd_fExtents())
                self%pd2_f_ => self%pd2_f%GetWithExtents(self%GetImagepd2_fExtents())
                self%q_f_ => self%q_f%GetWithExtents(self%GetImageq_fExtents())
                self%qd_f_ => self%qd_f%GetWithExtents(self%GetImageqd_fExtents())
                self%qd2_f_ => self%qd2_f%GetWithExtents(self%GetImageqd2_fExtents())
                self%gfund_ => self%gfund%GetWithExtents(self%GetImagegfundExtents())
                self%eny_ => self%eny%GetWithExtents(self%GetImageenyExtents())
                self%ffsmt_ => self%ffsmt%GetWithExtents(self%GetImageffsmtExtents())
        end subroutine
        subroutine DisconnectImageObjectFields(self)
                class(Image), intent(inout) :: self
               type(iterator) :: iter
                call self%Sizes%DisconnectObjectFields()
                call self%e_core%DisconnectFromStore()
                call self%pcor%DisconnectFromStore()
                call self%qcor%DisconnectFromStore()
                call self%n_bnd%DisconnectFromStore()
                call self%e_bnd%DisconnectFromStore()
                call self%g_loc_0%DisconnectFromStore()
                call self%gfun%DisconnectFromStore()
                call self%p_f%DisconnectFromStore()
                call self%pd_f%DisconnectFromStore()
                call self%pd2_f%DisconnectFromStore()
                call self%q_f%DisconnectFromStore()
                call self%qd_f%DisconnectFromStore()
                call self%qd2_f%DisconnectFromStore()
                call self%gfund%DisconnectFromStore()
                call self%eny%DisconnectFromStore()
                call self%ffsmt%DisconnectFromStore()
                call self%rho%disconnect()
        end subroutine
        subroutine StoreImageScalarFields(self)
                class(Image), intent(inout) :: self
                call self%Sizes%StoreScalarFields()
                call self%write('chem_pot', self%chem_pot)
        end subroutine
        subroutine LoadImageScalarFields(self)
                class(Image), intent(inout) :: self
                call self%Sizes%LoadScalarFields()
                call self%read('chem_pot', self%chem_pot)
        end subroutine
        subroutine FinalizeImage(self)
               type(Image), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearImage(self)
                class(Image), intent(inout) :: self
                type(Image), save :: empty
                self = empty
        end subroutine
        pure elemental function IsImageEqual(lhs, rhs) result(iseq)
                class(Image), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Image)
                       iseq = iseq .and. (lhs%Sizes == rhs%Sizes)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%chem_pot == rhs%chem_pot)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%e_core == rhs%e_core)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%pcor == rhs%pcor)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%qcor == rhs%qcor)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%n_bnd == rhs%n_bnd)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%e_bnd == rhs%e_bnd)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%g_loc_0 == rhs%g_loc_0)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%gfun == rhs%gfun)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%p_f == rhs%p_f)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%pd_f == rhs%pd_f)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%pd2_f == rhs%pd2_f)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%q_f == rhs%q_f)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%qd_f == rhs%qd_f)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%qd2_f == rhs%qd2_f)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%gfund == rhs%gfund)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%eny == rhs%eny)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ffsmt == rhs%ffsmt)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rho == rhs%rho)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorImage(lhs, rhs)
                class(Image), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Image)
                       lhs%Sizes = rhs%Sizes
                       lhs%chem_pot = rhs%chem_pot
                       lhs%e_core = rhs%e_core
                       lhs%pcor = rhs%pcor
                       lhs%qcor = rhs%qcor
                       lhs%n_bnd = rhs%n_bnd
                       lhs%e_bnd = rhs%e_bnd
                       lhs%g_loc_0 = rhs%g_loc_0
                       lhs%gfun = rhs%gfun
                       lhs%p_f = rhs%p_f
                       lhs%pd_f = rhs%pd_f
                       lhs%pd2_f = rhs%pd2_f
                       lhs%q_f = rhs%q_f
                       lhs%qd_f = rhs%qd_f
                       lhs%qd2_f = rhs%qd2_f
                       lhs%gfund = rhs%gfund
                       lhs%eny = rhs%eny
                       lhs%ffsmt = rhs%ffsmt
                       lhs%rho = rhs%rho
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetImageE_coreExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%ncormax),extent(1,self%nspin_0),extent(1,self%nsort)]
        end function
        function GetImagePcorExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%maxmtcor)]
        end function
        function GetImageQcorExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%maxmtcor)]
        end function
        function GetImageN_bndExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%npnt),extent(1,self%nspin_0)]
        end function
        function GetImageE_bndExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%nbndf),extent(1,self%npnt),extent(1,self%nspin_0)]
        end function
        function GetImageG_loc_0Extents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%maxel),extent(1,self%maxel),extent(1,self%natom),extent(1,self%nspin_0)]
        end function
        function GetImageGfunExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%maxwf),extent(1,self%nspin)]
        end function
        function GetImageP_fExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(0,self%maxnrad),extent(1,self%maxntle),extent(1,self%nlmb),extent(1,self%nsort),extent(1,self%nspin)]
        end function
        function GetImagePd_fExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(0,self%maxnrad),extent(1,self%maxntle),extent(1,self%nlmb),extent(1,self%nsort),extent(1,self%nspin)]
        end function
        function GetImagePd2_fExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(0,self%maxnrad),extent(1,self%maxntle),extent(1,self%nlmb),extent(1,self%nsort),extent(1,self%nspin)]
        end function
        function GetImageQ_fExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(0,self%maxnrad),extent(1,self%maxntle),extent(1,self%nlmb),extent(1,self%nsort),extent(1,self%nspin)]
        end function
        function GetImageQd_fExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(0,self%maxnrad),extent(1,self%maxntle),extent(1,self%nlmb),extent(1,self%nsort),extent(1,self%nspin)]
        end function
        function GetImageQd2_fExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(0,self%maxnrad),extent(1,self%maxntle),extent(1,self%nlmb),extent(1,self%nsort),extent(1,self%nspin)]
        end function
        function GetImageGfundExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%maxwf),extent(1,self%nspin)]
        end function
        function GetImageEnyExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%maxntle),extent(1,self%nlmb),extent(1,self%nsort),extent(1,self%nspin_0)]
        end function
        function GetImageFfsmtExtents(self) result(res)
                class(Image), intent(inout) :: self
                type(extent) :: res(7)
                res = [extent(1,2),extent(1,2),extent(1,self%maxntle),extent(1,self%maxntle),extent(1,self%nlmb),extent(1,self%nsort),extent(1,self%nspin)]
        end function
        subroutine AllocateImageObjectFields(self)
                class(Image), intent(inout) :: self
                call self%Sizes%AllocateObjectFields()
                call self%e_core%init(int(self%ncormax),int(self%nspin_0),int(self%nsort))
                call self%pcor%init(int(self%maxmtcor))
                call self%qcor%init(int(self%maxmtcor))
                call self%n_bnd%init(int(self%npnt),int(self%nspin_0))
                call self%e_bnd%init(int(self%nbndf),int(self%npnt),int(self%nspin_0))
                call self%g_loc_0%init(int(self%maxel),int(self%maxel),int(self%natom),int(self%nspin_0))
                call self%gfun%init(int(self%maxwf),int(self%nspin))
                call self%p_f%init(int(self%maxnrad- (0) + 1),int(self%maxntle),int(self%nlmb),int(self%nsort),int(self%nspin))
                call self%pd_f%init(int(self%maxnrad- (0) + 1),int(self%maxntle),int(self%nlmb),int(self%nsort),int(self%nspin))
                call self%pd2_f%init(int(self%maxnrad- (0) + 1),int(self%maxntle),int(self%nlmb),int(self%nsort),int(self%nspin))
                call self%q_f%init(int(self%maxnrad- (0) + 1),int(self%maxntle),int(self%nlmb),int(self%nsort),int(self%nspin))
                call self%qd_f%init(int(self%maxnrad- (0) + 1),int(self%maxntle),int(self%nlmb),int(self%nsort),int(self%nspin))
                call self%qd2_f%init(int(self%maxnrad- (0) + 1),int(self%maxntle),int(self%nlmb),int(self%nsort),int(self%nspin))
                call self%gfund%init(int(self%maxwf),int(self%nspin))
                call self%eny%init(int(self%maxntle),int(self%nlmb),int(self%nsort),int(self%nspin_0))
                call self%ffsmt%init(int(2),int(2),int(self%maxntle),int(self%maxntle),int(self%nlmb),int(self%nsort),int(self%nspin))
        end subroutine


        subroutine InitFullEVB(self)
                class(FullEVB), intent(inout) :: self
                call self%ExpansionSizes%Init()
        end subroutine
        subroutine StoreFullEVBObjectFields(self)
                class(FullEVB), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ExpansionSizes%StoreObjectFields()
                call self%evb%StoreObject(ps, gid,  'evb')
        end subroutine
        subroutine LoadFullEVBObjectFields(self)
                class(FullEVB), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ExpansionSizes%LoadObjectFields()
                call self%evb%LoadObject(ps, gid,  'evb')
        end subroutine
        subroutine ResetFullEVBSectionFields(self)
                class(FullEVB), intent(inout) :: self
                call self%ExpansionSizes%ResetSectionFields()
                self%evb_ => self%evb%GetWithExtents(self%GetFullEVBevbExtents())
        end subroutine
        subroutine DisconnectFullEVBObjectFields(self)
                class(FullEVB), intent(inout) :: self
               type(iterator) :: iter
                call self%ExpansionSizes%DisconnectObjectFields()
                call self%evb%DisconnectFromStore()
        end subroutine
        subroutine StoreFullEVBScalarFields(self)
                class(FullEVB), intent(inout) :: self
                call self%ExpansionSizes%StoreScalarFields()
        end subroutine
        subroutine LoadFullEVBScalarFields(self)
                class(FullEVB), intent(inout) :: self
                call self%ExpansionSizes%LoadScalarFields()
        end subroutine
        subroutine FinalizeFullEVB(self)
               type(FullEVB), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearFullEVB(self)
                class(FullEVB), intent(inout) :: self
                type(FullEVB), save :: empty
                self = empty
        end subroutine
        pure elemental function IsFullEVBEqual(lhs, rhs) result(iseq)
                class(FullEVB), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (FullEVB)
                       iseq = iseq .and. (lhs%ExpansionSizes == rhs%ExpansionSizes)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%evb == rhs%evb)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorFullEVB(lhs, rhs)
                class(FullEVB), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (FullEVB)
                       lhs%ExpansionSizes = rhs%ExpansionSizes
                       lhs%evb = rhs%evb
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetFullevbEvbExtents(self) result(res)
                class(FullEVB), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%nbasmpw),extent(1,self%nbndf),extent(1,self%npnt),extent(1,self%nspin)]
        end function
        subroutine AllocateFullEVBObjectFields(self)
                class(FullEVB), intent(inout) :: self
                call self%ExpansionSizes%AllocateObjectFields()
                call self%evb%init(int(self%nbasmpw),int(self%nbndf),int(self%npnt),int(self%nspin))
        end subroutine


        subroutine InitFullZB(self)
                class(FullZB), intent(inout) :: self
                call self%ExpansionSizes%Init()
        end subroutine
        subroutine StoreFullZBObjectFields(self)
                class(FullZB), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ExpansionSizes%StoreObjectFields()
                call self%zb%StoreObject(ps, gid,  'zb')
        end subroutine
        subroutine LoadFullZBObjectFields(self)
                class(FullZB), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ExpansionSizes%LoadObjectFields()
                call self%zb%LoadObject(ps, gid,  'zb')
        end subroutine
        subroutine ResetFullZBSectionFields(self)
                class(FullZB), intent(inout) :: self
                call self%ExpansionSizes%ResetSectionFields()
                self%zb_ => self%zb%GetWithExtents(self%GetFullZBzbExtents())
        end subroutine
        subroutine DisconnectFullZBObjectFields(self)
                class(FullZB), intent(inout) :: self
               type(iterator) :: iter
                call self%ExpansionSizes%DisconnectObjectFields()
                call self%zb%DisconnectFromStore()
        end subroutine
        subroutine StoreFullZBScalarFields(self)
                class(FullZB), intent(inout) :: self
                call self%ExpansionSizes%StoreScalarFields()
        end subroutine
        subroutine LoadFullZBScalarFields(self)
                class(FullZB), intent(inout) :: self
                call self%ExpansionSizes%LoadScalarFields()
        end subroutine
        subroutine FinalizeFullZB(self)
               type(FullZB), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearFullZB(self)
                class(FullZB), intent(inout) :: self
                type(FullZB), save :: empty
                self = empty
        end subroutine
        pure elemental function IsFullZBEqual(lhs, rhs) result(iseq)
                class(FullZB), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (FullZB)
                       iseq = iseq .and. (lhs%ExpansionSizes == rhs%ExpansionSizes)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%zb == rhs%zb)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorFullZB(lhs, rhs)
                class(FullZB), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (FullZB)
                       lhs%ExpansionSizes = rhs%ExpansionSizes
                       lhs%zb = rhs%zb
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetFullzbZbExtents(self) result(res)
                class(FullZB), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%nfun),extent(1,self%nbndf),extent(1,self%npnt),extent(1,self%nspin)]
        end function
        subroutine AllocateFullZBObjectFields(self)
                class(FullZB), intent(inout) :: self
                call self%ExpansionSizes%AllocateObjectFields()
                call self%zb%init(int(self%nfun),int(self%nbndf),int(self%npnt),int(self%nspin))
        end subroutine


        subroutine InitFullGF0(self)
                class(FullGF0), intent(inout) :: self
                call self%ExpansionSizes%Init()
        end subroutine
        subroutine StoreFullGF0ObjectFields(self)
                class(FullGF0), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ExpansionSizes%StoreObjectFields()
                call self%gf0%StoreObject(ps, gid,  'gf0')
        end subroutine
        subroutine LoadFullGF0ObjectFields(self)
                class(FullGF0), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ExpansionSizes%LoadObjectFields()
                call self%gf0%LoadObject(ps, gid,  'gf0')
        end subroutine
        subroutine ResetFullGF0SectionFields(self)
                class(FullGF0), intent(inout) :: self
                call self%ExpansionSizes%ResetSectionFields()
                self%gf0_ => self%gf0%GetWithExtents(self%GetFullGF0gf0Extents())
        end subroutine
        subroutine DisconnectFullGF0ObjectFields(self)
                class(FullGF0), intent(inout) :: self
               type(iterator) :: iter
                call self%ExpansionSizes%DisconnectObjectFields()
                call self%gf0%DisconnectFromStore()
        end subroutine
        subroutine StoreFullGF0ScalarFields(self)
                class(FullGF0), intent(inout) :: self
                call self%ExpansionSizes%StoreScalarFields()
        end subroutine
        subroutine LoadFullGF0ScalarFields(self)
                class(FullGF0), intent(inout) :: self
                call self%ExpansionSizes%LoadScalarFields()
        end subroutine
        subroutine FinalizeFullGF0(self)
               type(FullGF0), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearFullGF0(self)
                class(FullGF0), intent(inout) :: self
                type(FullGF0), save :: empty
                self = empty
        end subroutine
        pure elemental function IsFullGF0Equal(lhs, rhs) result(iseq)
                class(FullGF0), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (FullGF0)
                       iseq = iseq .and. (lhs%ExpansionSizes == rhs%ExpansionSizes)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%gf0 == rhs%gf0)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorFullGF0(lhs, rhs)
                class(FullGF0), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (FullGF0)
                       lhs%ExpansionSizes = rhs%ExpansionSizes
                       lhs%gf0 = rhs%gf0
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetFullgf0Gf0Extents(self) result(res)
                class(FullGF0), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%nbndf),extent(1,self%nbndf),extent(1,self%npnt),extent(1,self%nspin)]
        end function
        subroutine AllocateFullGF0ObjectFields(self)
                class(FullGF0), intent(inout) :: self
                call self%ExpansionSizes%AllocateObjectFields()
                call self%gf0%init(int(self%nbndf),int(self%nbndf),int(self%npnt),int(self%nspin))
        end subroutine



end module
