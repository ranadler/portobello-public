
module Rho
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use array4
    use cube


    implicit none
    public

    type, extends(persistent) :: RhoInfo
        type(string)  ::  location


        contains
        procedure :: AllocateObjectFields => AllocateRhoInfoObjectFields
        procedure :: ResetSectionFields   => ResetRhoInfoSectionFields
        procedure :: StoreScalarFields    => StoreRhoInfoScalarFields
        procedure :: StoreObjectFields    => StoreRhoInfoObjectFields
        procedure :: LoadScalarFields     => LoadRhoInfoScalarFields
        procedure :: LoadObjectFields     => LoadRhoInfoObjectFields
        procedure :: DisconnectObjectFields => DisconnectRhoInfoObjectFields
        procedure :: IsEqual              => IsRhoInfoEqual
        procedure :: AssignmentOperator   => AssignmentOperatorRhoInfo
        procedure :: clear                => ClearRhoInfo
        procedure :: init => InitRhoInfo
#ifndef __GFORTRAN__
        final     :: FinalizeRhoInfo
#endif

    end type

    type, extends(persistent) :: RhoInBands
        integer(kind=int32)  ::  min_band =  0

        integer(kind=int32)  ::  max_band =  0

        integer(kind=int32)  ::  num_k =  0

        integer(kind=int32)  ::  num_si =  0

        type(array4_complex)  ::  rho
        complex(kind=dp),pointer :: rho_(:,:,:,:)
        type(cube_complex)  ::  Hqp
        complex(kind=dp),pointer :: Hqp_(:,:,:)
        type(cube_complex)  ::  Z
        complex(kind=dp),pointer :: Z_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateRhoInBandsObjectFields
        procedure :: ResetSectionFields   => ResetRhoInBandsSectionFields
        procedure :: StoreScalarFields    => StoreRhoInBandsScalarFields
        procedure :: StoreObjectFields    => StoreRhoInBandsObjectFields
        procedure :: LoadScalarFields     => LoadRhoInBandsScalarFields
        procedure :: LoadObjectFields     => LoadRhoInBandsObjectFields
        procedure :: DisconnectObjectFields => DisconnectRhoInBandsObjectFields
        procedure :: IsEqual              => IsRhoInBandsEqual
        procedure :: AssignmentOperator   => AssignmentOperatorRhoInBands
        procedure :: clear                => ClearRhoInBands
        procedure :: init => InitRhoInBands
#ifndef __GFORTRAN__
        final     :: FinalizeRhoInBands
#endif
        procedure :: GetRhoinbandsRhoExtents
        procedure :: GetRhoinbandsHqpExtents
        procedure :: GetRhoinbandsZExtents

    end type

    contains
        subroutine InitRhoInfo(self)
                class(RhoInfo), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreRhoInfoObjectFields(self)
                class(RhoInfo), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadRhoInfoObjectFields(self)
                class(RhoInfo), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetRhoInfoSectionFields(self)
                class(RhoInfo), intent(inout) :: self
        end subroutine
        subroutine DisconnectRhoInfoObjectFields(self)
                class(RhoInfo), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreRhoInfoScalarFields(self)
                class(RhoInfo), intent(inout) :: self
                call self%write('location', self%location)
        end subroutine
        subroutine LoadRhoInfoScalarFields(self)
                class(RhoInfo), intent(inout) :: self
                call self%read('location', self%location)
        end subroutine
        subroutine FinalizeRhoInfo(self)
               type(RhoInfo), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearRhoInfo(self)
                class(RhoInfo), intent(inout) :: self
                type(RhoInfo), save :: empty
                self = empty
        end subroutine
        pure elemental function IsRhoInfoEqual(lhs, rhs) result(iseq)
                class(RhoInfo), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (RhoInfo)
                       iseq = iseq .and. (lhs%location == rhs%location)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorRhoInfo(lhs, rhs)
                class(RhoInfo), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (RhoInfo)
                       lhs%location = rhs%location
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateRhoInfoObjectFields(self)
                class(RhoInfo), intent(inout) :: self
        end subroutine


        subroutine InitRhoInBands(self)
                class(RhoInBands), intent(inout) :: self
                call self%InitPersistent()
                self%min_band =  0
                self%max_band =  0
                self%num_k =  0
                self%num_si =  0
        end subroutine
        subroutine StoreRhoInBandsObjectFields(self)
                class(RhoInBands), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%rho%StoreObject(ps, gid,  'rho')
                call self%Hqp%StoreObject(ps, gid,  'Hqp')
                call self%Z%StoreObject(ps, gid,  'Z')
        end subroutine
        subroutine LoadRhoInBandsObjectFields(self)
                class(RhoInBands), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%rho%LoadObject(ps, gid,  'rho')
                call self%Hqp%LoadObject(ps, gid,  'Hqp')
                call self%Z%LoadObject(ps, gid,  'Z')
        end subroutine
        subroutine ResetRhoInBandsSectionFields(self)
                class(RhoInBands), intent(inout) :: self
                self%rho_ => self%rho%GetWithExtents(self%GetRhoInBandsrhoExtents())
                self%Hqp_ => self%Hqp%GetWithExtents(self%GetRhoInBandsHqpExtents())
                self%Z_ => self%Z%GetWithExtents(self%GetRhoInBandsZExtents())
        end subroutine
        subroutine DisconnectRhoInBandsObjectFields(self)
                class(RhoInBands), intent(inout) :: self
               type(iterator) :: iter
                call self%rho%DisconnectFromStore()
                call self%Hqp%DisconnectFromStore()
                call self%Z%DisconnectFromStore()
        end subroutine
        subroutine StoreRhoInBandsScalarFields(self)
                class(RhoInBands), intent(inout) :: self
                call self%write('min_band', self%min_band)
                call self%write('max_band', self%max_band)
                call self%write('num_k', self%num_k)
                call self%write('num_si', self%num_si)
        end subroutine
        subroutine LoadRhoInBandsScalarFields(self)
                class(RhoInBands), intent(inout) :: self
                call self%read('min_band', self%min_band)
                call self%read('max_band', self%max_band)
                call self%read('num_k', self%num_k)
                call self%read('num_si', self%num_si)
        end subroutine
        subroutine FinalizeRhoInBands(self)
               type(RhoInBands), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearRhoInBands(self)
                class(RhoInBands), intent(inout) :: self
                type(RhoInBands), save :: empty
                self = empty
        end subroutine
        pure elemental function IsRhoInBandsEqual(lhs, rhs) result(iseq)
                class(RhoInBands), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (RhoInBands)
                       iseq = iseq .and. (lhs%min_band == rhs%min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_band == rhs%max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rho == rhs%rho)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Hqp == rhs%Hqp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Z == rhs%Z)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorRhoInBands(lhs, rhs)
                class(RhoInBands), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (RhoInBands)
                       lhs%min_band = rhs%min_band
                       lhs%max_band = rhs%max_band
                       lhs%num_k = rhs%num_k
                       lhs%num_si = rhs%num_si
                       lhs%rho = rhs%rho
                       lhs%Hqp = rhs%Hqp
                       lhs%Z = rhs%Z
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetRhoinbandsRhoExtents(self) result(res)
                class(RhoInBands), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(self%min_band,self%max_band),extent(self%min_band,self%max_band),extent(1,self%num_k),extent(1,self%num_si)]
        end function
        function GetRhoinbandsHqpExtents(self) result(res)
                class(RhoInBands), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(self%min_band,self%max_band),extent(1,self%num_k),extent(1,self%num_si)]
        end function
        function GetRhoinbandsZExtents(self) result(res)
                class(RhoInBands), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(self%min_band,self%max_band),extent(1,self%num_k),extent(1,self%num_si)]
        end function
        subroutine AllocateRhoInBandsObjectFields(self)
                class(RhoInBands), intent(inout) :: self
                call self%rho%init(int(self%max_band- (self%min_band) + 1),int(self%max_band- (self%min_band) + 1),int(self%num_k),int(self%num_si))
                call self%Hqp%init(int(self%max_band- (self%min_band) + 1),int(self%num_k),int(self%num_si))
                call self%Z%init(int(self%max_band- (self%min_band) + 1),int(self%num_k),int(self%num_si))
        end subroutine



end module
