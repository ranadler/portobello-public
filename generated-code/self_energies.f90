
module self_energies
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use array4
    use array5
    use vector


    implicit none
    public

    type, extends(persistent) :: LocalOmegaFunction
        logical  ::  isReal = .False.

        integer(kind=int32)  ::  num_omega =  0

        integer(kind=int32)  ::  num_orbs =  0

        integer(kind=int32)  ::  num_si =  0

        type(vector_real)  ::  omega
        real(kind=dp),pointer :: omega_(:)
        type(array4_complex)  ::  M
        complex(kind=dp),pointer :: M_(:,:,:,:)
        type(array4_real)  ::  moments
        real(kind=dp),pointer :: moments_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateLocalOmegaFunctionObjectFields
        procedure :: ResetSectionFields   => ResetLocalOmegaFunctionSectionFields
        procedure :: StoreScalarFields    => StoreLocalOmegaFunctionScalarFields
        procedure :: StoreObjectFields    => StoreLocalOmegaFunctionObjectFields
        procedure :: LoadScalarFields     => LoadLocalOmegaFunctionScalarFields
        procedure :: LoadObjectFields     => LoadLocalOmegaFunctionObjectFields
        procedure :: DisconnectObjectFields => DisconnectLocalOmegaFunctionObjectFields
        procedure :: IsEqual              => IsLocalOmegaFunctionEqual
        procedure :: AssignmentOperator   => AssignmentOperatorLocalOmegaFunction
        procedure :: clear                => ClearLocalOmegaFunction
        procedure :: init => InitLocalOmegaFunction
#ifndef __GFORTRAN__
        final     :: FinalizeLocalOmegaFunction
#endif
        procedure :: GetLocalomegafunctionOmegaExtents
        procedure :: GetLocalomegafunctionMExtents
        procedure :: GetLocalomegafunctionMomentsExtents

    end type

    type, extends(persistent) :: OmegaMesh
        integer(kind=int32)  ::  n_omega =  0

        type(vector_real)  ::  omega
        real(kind=dp),pointer :: omega_(:)
        type(vector_int)  ::  index
        integer(kind=int32),pointer :: index_(:)


        contains
        procedure :: AllocateObjectFields => AllocateOmegaMeshObjectFields
        procedure :: ResetSectionFields   => ResetOmegaMeshSectionFields
        procedure :: StoreScalarFields    => StoreOmegaMeshScalarFields
        procedure :: StoreObjectFields    => StoreOmegaMeshObjectFields
        procedure :: LoadScalarFields     => LoadOmegaMeshScalarFields
        procedure :: LoadObjectFields     => LoadOmegaMeshObjectFields
        procedure :: DisconnectObjectFields => DisconnectOmegaMeshObjectFields
        procedure :: IsEqual              => IsOmegaMeshEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOmegaMesh
        procedure :: clear                => ClearOmegaMesh
        procedure :: init => InitOmegaMesh
#ifndef __GFORTRAN__
        final     :: FinalizeOmegaMesh
#endif
        procedure :: GetOmegameshOmegaExtents
        procedure :: GetOmegameshIndexExtents

    end type
    interface
         function GetOmegaMeshI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    type, extends(persistent) :: LocalOmegaFunctionInfo
        type(string)  ::  location


        contains
        procedure :: AllocateObjectFields => AllocateLocalOmegaFunctionInfoObjectFields
        procedure :: ResetSectionFields   => ResetLocalOmegaFunctionInfoSectionFields
        procedure :: StoreScalarFields    => StoreLocalOmegaFunctionInfoScalarFields
        procedure :: StoreObjectFields    => StoreLocalOmegaFunctionInfoObjectFields
        procedure :: LoadScalarFields     => LoadLocalOmegaFunctionInfoScalarFields
        procedure :: LoadObjectFields     => LoadLocalOmegaFunctionInfoObjectFields
        procedure :: DisconnectObjectFields => DisconnectLocalOmegaFunctionInfoObjectFields
        procedure :: IsEqual              => IsLocalOmegaFunctionInfoEqual
        procedure :: AssignmentOperator   => AssignmentOperatorLocalOmegaFunctionInfo
        procedure :: clear                => ClearLocalOmegaFunctionInfo
        procedure :: init => InitLocalOmegaFunctionInfo
#ifndef __GFORTRAN__
        final     :: FinalizeLocalOmegaFunctionInfo
#endif

    end type

    type, extends(OmegaMesh) :: OmegaFunctionInBands
        logical  ::  isReal = .False.

        integer(kind=int32)  ::  min_band =  0

        integer(kind=int32)  ::  max_band =  0

        integer(kind=int32)  ::  num_k_irr =  0

        integer(kind=int32)  ::  num_si =  0

        type(array5_complex)  ::  M
        complex(kind=dp),pointer :: M_(:,:,:,:,:)
        type(array4_complex)  ::  Minf
        complex(kind=dp),pointer :: Minf_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateOmegaFunctionInBandsObjectFields
        procedure :: ResetSectionFields   => ResetOmegaFunctionInBandsSectionFields
        procedure :: StoreScalarFields    => StoreOmegaFunctionInBandsScalarFields
        procedure :: StoreObjectFields    => StoreOmegaFunctionInBandsObjectFields
        procedure :: LoadScalarFields     => LoadOmegaFunctionInBandsScalarFields
        procedure :: LoadObjectFields     => LoadOmegaFunctionInBandsObjectFields
        procedure :: DisconnectObjectFields => DisconnectOmegaFunctionInBandsObjectFields
        procedure :: IsEqual              => IsOmegaFunctionInBandsEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOmegaFunctionInBands
        procedure :: clear                => ClearOmegaFunctionInBands
        procedure :: init => InitOmegaFunctionInBands
#ifndef __GFORTRAN__
        final     :: FinalizeOmegaFunctionInBands
#endif
        procedure :: GetOmegafunctioninbandsMExtents
        procedure :: GetOmegafunctioninbandsMinfExtents

    end type

    contains
        subroutine InitLocalOmegaFunction(self)
                class(LocalOmegaFunction), intent(inout) :: self
                call self%InitPersistent()
                self%isReal = .False.
                self%num_omega =  0
                self%num_orbs =  0
                self%num_si =  0
        end subroutine
        subroutine StoreLocalOmegaFunctionObjectFields(self)
                class(LocalOmegaFunction), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omega%StoreObject(ps, gid,  'omega')
                call self%M%StoreObject(ps, gid,  'M')
                call self%moments%StoreObject(ps, gid,  'moments')
        end subroutine
        subroutine LoadLocalOmegaFunctionObjectFields(self)
                class(LocalOmegaFunction), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omega%LoadObject(ps, gid,  'omega')
                call self%M%LoadObject(ps, gid,  'M')
                call self%moments%LoadObject(ps, gid,  'moments')
        end subroutine
        subroutine ResetLocalOmegaFunctionSectionFields(self)
                class(LocalOmegaFunction), intent(inout) :: self
                self%omega_ => self%omega%GetWithExtents(self%GetLocalOmegaFunctionomegaExtents())
                self%M_ => self%M%GetWithExtents(self%GetLocalOmegaFunctionMExtents())
                self%moments_ => self%moments%GetWithExtents(self%GetLocalOmegaFunctionmomentsExtents())
        end subroutine
        subroutine DisconnectLocalOmegaFunctionObjectFields(self)
                class(LocalOmegaFunction), intent(inout) :: self
               type(iterator) :: iter
                call self%omega%DisconnectFromStore()
                call self%M%DisconnectFromStore()
                call self%moments%DisconnectFromStore()
        end subroutine
        subroutine StoreLocalOmegaFunctionScalarFields(self)
                class(LocalOmegaFunction), intent(inout) :: self
                call self%write('isReal', self%isReal)
                call self%write('num_omega', self%num_omega)
                call self%write('num_orbs', self%num_orbs)
                call self%write('num_si', self%num_si)
        end subroutine
        subroutine LoadLocalOmegaFunctionScalarFields(self)
                class(LocalOmegaFunction), intent(inout) :: self
                call self%read('isReal', self%isReal)
                call self%read('num_omega', self%num_omega)
                call self%read('num_orbs', self%num_orbs)
                call self%read('num_si', self%num_si)
        end subroutine
        subroutine FinalizeLocalOmegaFunction(self)
               type(LocalOmegaFunction), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearLocalOmegaFunction(self)
                class(LocalOmegaFunction), intent(inout) :: self
                type(LocalOmegaFunction), save :: empty
                self = empty
        end subroutine
        pure elemental function IsLocalOmegaFunctionEqual(lhs, rhs) result(iseq)
                class(LocalOmegaFunction), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (LocalOmegaFunction)
                       iseq = iseq .and. (lhs%isReal .eqv. rhs%isReal)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_omega == rhs%num_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_orbs == rhs%num_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega == rhs%omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M == rhs%M)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%moments == rhs%moments)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorLocalOmegaFunction(lhs, rhs)
                class(LocalOmegaFunction), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (LocalOmegaFunction)
                       lhs%isReal = rhs%isReal
                       lhs%num_omega = rhs%num_omega
                       lhs%num_orbs = rhs%num_orbs
                       lhs%num_si = rhs%num_si
                       lhs%omega = rhs%omega
                       lhs%M = rhs%M
                       lhs%moments = rhs%moments
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetLocalomegafunctionOmegaExtents(self) result(res)
                class(LocalOmegaFunction), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_omega)]
        end function
        function GetLocalomegafunctionMExtents(self) result(res)
                class(LocalOmegaFunction), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%num_omega),extent(1,self%num_orbs),extent(1,self%num_orbs),extent(1,self%num_si)]
        end function
        function GetLocalomegafunctionMomentsExtents(self) result(res)
                class(LocalOmegaFunction), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,3),extent(1,self%num_orbs),extent(1,self%num_orbs),extent(1,self%num_si)]
        end function
        subroutine AllocateLocalOmegaFunctionObjectFields(self)
                class(LocalOmegaFunction), intent(inout) :: self
                call self%omega%init(int(self%num_omega))
                call self%M%init(int(self%num_omega),int(self%num_orbs),int(self%num_orbs),int(self%num_si))
                call self%moments%init(int(3),int(self%num_orbs),int(self%num_orbs),int(self%num_si))
        end subroutine


        subroutine InitOmegaMesh(self)
                class(OmegaMesh), intent(inout) :: self
                call self%InitPersistent()
                self%n_omega =  0
        end subroutine
        subroutine StoreOmegaMeshObjectFields(self)
                class(OmegaMesh), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omega%StoreObject(ps, gid,  'omega')
                call self%index%StoreObject(ps, gid,  'index')
        end subroutine
        subroutine LoadOmegaMeshObjectFields(self)
                class(OmegaMesh), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omega%LoadObject(ps, gid,  'omega')
                call self%index%LoadObject(ps, gid,  'index')
        end subroutine
        subroutine ResetOmegaMeshSectionFields(self)
                class(OmegaMesh), intent(inout) :: self
                self%omega_ => self%omega%GetWithExtents(self%GetOmegaMeshomegaExtents())
                self%index_ => self%index%GetWithExtents(self%GetOmegaMeshindexExtents())
        end subroutine
        subroutine DisconnectOmegaMeshObjectFields(self)
                class(OmegaMesh), intent(inout) :: self
               type(iterator) :: iter
                call self%omega%DisconnectFromStore()
                call self%index%DisconnectFromStore()
        end subroutine
        subroutine StoreOmegaMeshScalarFields(self)
                class(OmegaMesh), intent(inout) :: self
                call self%write('n_omega', self%n_omega)
        end subroutine
        subroutine LoadOmegaMeshScalarFields(self)
                class(OmegaMesh), intent(inout) :: self
                call self%read('n_omega', self%n_omega)
        end subroutine
        subroutine FinalizeOmegaMesh(self)
               type(OmegaMesh), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOmegaMesh(self)
                class(OmegaMesh), intent(inout) :: self
                type(OmegaMesh), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOmegaMeshEqual(lhs, rhs) result(iseq)
                class(OmegaMesh), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (OmegaMesh)
                       iseq = iseq .and. (lhs%n_omega == rhs%n_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega == rhs%omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%index == rhs%index)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOmegaMesh(lhs, rhs)
                class(OmegaMesh), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (OmegaMesh)
                       lhs%n_omega = rhs%n_omega
                       lhs%omega = rhs%omega
                       lhs%index = rhs%index
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetOmegameshOmegaExtents(self) result(res)
                class(OmegaMesh), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(0,self%n_omega)]
        end function
        function GetOmegameshIndexExtents(self) result(res)
                class(OmegaMesh), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(0,self%n_omega)]
        end function
        subroutine AllocateOmegaMeshObjectFields(self)
                class(OmegaMesh), intent(inout) :: self
                call self%omega%init(int(self%n_omega- (0) + 1))
                call self%index%init(int(self%n_omega- (0) + 1))
        end subroutine


        subroutine GetOmegaMeshWrapper(ret, imp_fp, selfpath) bind(C,name='self_energies_mp_getomegameshwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(GetOmegaMeshI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine InitLocalOmegaFunctionInfo(self)
                class(LocalOmegaFunctionInfo), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreLocalOmegaFunctionInfoObjectFields(self)
                class(LocalOmegaFunctionInfo), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadLocalOmegaFunctionInfoObjectFields(self)
                class(LocalOmegaFunctionInfo), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetLocalOmegaFunctionInfoSectionFields(self)
                class(LocalOmegaFunctionInfo), intent(inout) :: self
        end subroutine
        subroutine DisconnectLocalOmegaFunctionInfoObjectFields(self)
                class(LocalOmegaFunctionInfo), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreLocalOmegaFunctionInfoScalarFields(self)
                class(LocalOmegaFunctionInfo), intent(inout) :: self
                call self%write('location', self%location)
        end subroutine
        subroutine LoadLocalOmegaFunctionInfoScalarFields(self)
                class(LocalOmegaFunctionInfo), intent(inout) :: self
                call self%read('location', self%location)
        end subroutine
        subroutine FinalizeLocalOmegaFunctionInfo(self)
               type(LocalOmegaFunctionInfo), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearLocalOmegaFunctionInfo(self)
                class(LocalOmegaFunctionInfo), intent(inout) :: self
                type(LocalOmegaFunctionInfo), save :: empty
                self = empty
        end subroutine
        pure elemental function IsLocalOmegaFunctionInfoEqual(lhs, rhs) result(iseq)
                class(LocalOmegaFunctionInfo), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (LocalOmegaFunctionInfo)
                       iseq = iseq .and. (lhs%location == rhs%location)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorLocalOmegaFunctionInfo(lhs, rhs)
                class(LocalOmegaFunctionInfo), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (LocalOmegaFunctionInfo)
                       lhs%location = rhs%location
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateLocalOmegaFunctionInfoObjectFields(self)
                class(LocalOmegaFunctionInfo), intent(inout) :: self
        end subroutine


        subroutine InitOmegaFunctionInBands(self)
                class(OmegaFunctionInBands), intent(inout) :: self
                call self%OmegaMesh%Init()
                self%isReal = .False.
                self%min_band =  0
                self%max_band =  0
                self%num_k_irr =  0
                self%num_si =  0
        end subroutine
        subroutine StoreOmegaFunctionInBandsObjectFields(self)
                class(OmegaFunctionInBands), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%OmegaMesh%StoreObjectFields()
                call self%M%StoreObject(ps, gid,  'M')
                call self%Minf%StoreObject(ps, gid,  'Minf')
        end subroutine
        subroutine LoadOmegaFunctionInBandsObjectFields(self)
                class(OmegaFunctionInBands), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%OmegaMesh%LoadObjectFields()
                call self%M%LoadObject(ps, gid,  'M')
                call self%Minf%LoadObject(ps, gid,  'Minf')
        end subroutine
        subroutine ResetOmegaFunctionInBandsSectionFields(self)
                class(OmegaFunctionInBands), intent(inout) :: self
                call self%OmegaMesh%ResetSectionFields()
                self%M_ => self%M%GetWithExtents(self%GetOmegaFunctionInBandsMExtents())
                self%Minf_ => self%Minf%GetWithExtents(self%GetOmegaFunctionInBandsMinfExtents())
        end subroutine
        subroutine DisconnectOmegaFunctionInBandsObjectFields(self)
                class(OmegaFunctionInBands), intent(inout) :: self
               type(iterator) :: iter
                call self%OmegaMesh%DisconnectObjectFields()
                call self%M%DisconnectFromStore()
                call self%Minf%DisconnectFromStore()
        end subroutine
        subroutine StoreOmegaFunctionInBandsScalarFields(self)
                class(OmegaFunctionInBands), intent(inout) :: self
                call self%OmegaMesh%StoreScalarFields()
                call self%write('isReal', self%isReal)
                call self%write('min_band', self%min_band)
                call self%write('max_band', self%max_band)
                call self%write('num_k_irr', self%num_k_irr)
                call self%write('num_si', self%num_si)
        end subroutine
        subroutine LoadOmegaFunctionInBandsScalarFields(self)
                class(OmegaFunctionInBands), intent(inout) :: self
                call self%OmegaMesh%LoadScalarFields()
                call self%read('isReal', self%isReal)
                call self%read('min_band', self%min_band)
                call self%read('max_band', self%max_band)
                call self%read('num_k_irr', self%num_k_irr)
                call self%read('num_si', self%num_si)
        end subroutine
        subroutine FinalizeOmegaFunctionInBands(self)
               type(OmegaFunctionInBands), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOmegaFunctionInBands(self)
                class(OmegaFunctionInBands), intent(inout) :: self
                type(OmegaFunctionInBands), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOmegaFunctionInBandsEqual(lhs, rhs) result(iseq)
                class(OmegaFunctionInBands), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (OmegaFunctionInBands)
                       iseq = iseq .and. (lhs%OmegaMesh == rhs%OmegaMesh)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%isReal .eqv. rhs%isReal)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%min_band == rhs%min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_band == rhs%max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_irr == rhs%num_k_irr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M == rhs%M)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Minf == rhs%Minf)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOmegaFunctionInBands(lhs, rhs)
                class(OmegaFunctionInBands), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (OmegaFunctionInBands)
                       lhs%OmegaMesh = rhs%OmegaMesh
                       lhs%isReal = rhs%isReal
                       lhs%min_band = rhs%min_band
                       lhs%max_band = rhs%max_band
                       lhs%num_k_irr = rhs%num_k_irr
                       lhs%num_si = rhs%num_si
                       lhs%M = rhs%M
                       lhs%Minf = rhs%Minf
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetOmegafunctioninbandsMExtents(self) result(res)
                class(OmegaFunctionInBands), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(1,self%max_band - self%min_band + 1),extent(1,self%max_band - self%min_band + 1),extent(0,self%n_omega),extent(1,self%num_k_irr),extent(1,self%num_si)]
        end function
        function GetOmegafunctioninbandsMinfExtents(self) result(res)
                class(OmegaFunctionInBands), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%max_band - self%min_band + 1),extent(1,self%max_band - self%min_band + 1),extent(1,self%num_k_irr),extent(1,self%num_si)]
        end function
        subroutine AllocateOmegaFunctionInBandsObjectFields(self)
                class(OmegaFunctionInBands), intent(inout) :: self
                call self%OmegaMesh%AllocateObjectFields()
                call self%M%init(int(self%max_band - self%min_band + 1),int(self%max_band - self%min_band + 1),int(self%n_omega- (0) + 1),int(self%num_k_irr),int(self%num_si))
                call self%Minf%init(int(self%max_band - self%min_band + 1),int(self%max_band - self%min_band + 1),int(self%num_k_irr),int(self%num_si))
        end subroutine



end module
