
module models
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use atomic_orbitals
    use vector


    implicit none
    public

    type, extends(AtomicShell) :: ModelShell
        type(vector_int)  ::  correlated_orb_indices
        integer(kind=int32),pointer :: correlated_orb_indices_(:)


        contains
        procedure :: AllocateObjectFields => AllocateModelShellObjectFields
        procedure :: ResetSectionFields   => ResetModelShellSectionFields
        procedure :: StoreScalarFields    => StoreModelShellScalarFields
        procedure :: StoreObjectFields    => StoreModelShellObjectFields
        procedure :: LoadScalarFields     => LoadModelShellScalarFields
        procedure :: LoadObjectFields     => LoadModelShellObjectFields
        procedure :: DisconnectObjectFields => DisconnectModelShellObjectFields
        procedure :: IsEqual              => IsModelShellEqual
        procedure :: AssignmentOperator   => AssignmentOperatorModelShell
        procedure :: clear                => ClearModelShell
        procedure :: init => InitModelShell
#ifndef __GFORTRAN__
        final     :: FinalizeModelShell
#endif
        procedure :: GetModelshellCorrelated_orb_indicesExtents

    end type

    type, extends(persistent) :: ModelState
        type(string)  ::  model_type


        contains
        procedure :: AllocateObjectFields => AllocateModelStateObjectFields
        procedure :: ResetSectionFields   => ResetModelStateSectionFields
        procedure :: StoreScalarFields    => StoreModelStateScalarFields
        procedure :: StoreObjectFields    => StoreModelStateObjectFields
        procedure :: LoadScalarFields     => LoadModelStateScalarFields
        procedure :: LoadObjectFields     => LoadModelStateObjectFields
        procedure :: DisconnectObjectFields => DisconnectModelStateObjectFields
        procedure :: IsEqual              => IsModelStateEqual
        procedure :: AssignmentOperator   => AssignmentOperatorModelState
        procedure :: clear                => ClearModelState
        procedure :: init => InitModelState
#ifndef __GFORTRAN__
        final     :: FinalizeModelState
#endif

    end type

    contains
        subroutine InitModelShell(self)
                class(ModelShell), intent(inout) :: self
                call self%AtomicShell%Init()
        end subroutine
        subroutine StoreModelShellObjectFields(self)
                class(ModelShell), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%AtomicShell%StoreObjectFields()
                call self%correlated_orb_indices%StoreObject(ps, gid,  'correlated_orb_indices')
        end subroutine
        subroutine LoadModelShellObjectFields(self)
                class(ModelShell), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%AtomicShell%LoadObjectFields()
                call self%correlated_orb_indices%LoadObject(ps, gid,  'correlated_orb_indices')
        end subroutine
        subroutine ResetModelShellSectionFields(self)
                class(ModelShell), intent(inout) :: self
                call self%AtomicShell%ResetSectionFields()
                self%correlated_orb_indices_ => self%correlated_orb_indices%GetWithExtents(self%GetModelShellcorrelated_orb_indicesExtents())
        end subroutine
        subroutine DisconnectModelShellObjectFields(self)
                class(ModelShell), intent(inout) :: self
               type(iterator) :: iter
                call self%AtomicShell%DisconnectObjectFields()
                call self%correlated_orb_indices%DisconnectFromStore()
        end subroutine
        subroutine StoreModelShellScalarFields(self)
                class(ModelShell), intent(inout) :: self
                call self%AtomicShell%StoreScalarFields()
        end subroutine
        subroutine LoadModelShellScalarFields(self)
                class(ModelShell), intent(inout) :: self
                call self%AtomicShell%LoadScalarFields()
        end subroutine
        subroutine FinalizeModelShell(self)
               type(ModelShell), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearModelShell(self)
                class(ModelShell), intent(inout) :: self
                type(ModelShell), save :: empty
                self = empty
        end subroutine
        pure elemental function IsModelShellEqual(lhs, rhs) result(iseq)
                class(ModelShell), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ModelShell)
                       iseq = iseq .and. (lhs%AtomicShell == rhs%AtomicShell)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%correlated_orb_indices == rhs%correlated_orb_indices)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorModelShell(lhs, rhs)
                class(ModelShell), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ModelShell)
                       lhs%AtomicShell = rhs%AtomicShell
                       lhs%correlated_orb_indices = rhs%correlated_orb_indices
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetModelshellCorrelated_orb_indicesExtents(self) result(res)
                class(ModelShell), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%dim)]
        end function
        subroutine AllocateModelShellObjectFields(self)
                class(ModelShell), intent(inout) :: self
                call self%AtomicShell%AllocateObjectFields()
                call self%correlated_orb_indices%init(int(self%dim))
        end subroutine


        subroutine InitModelState(self)
                class(ModelState), intent(inout) :: self
                call self%InitPersistent()
                self%model_type = "wannier90"
        end subroutine
        subroutine StoreModelStateObjectFields(self)
                class(ModelState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadModelStateObjectFields(self)
                class(ModelState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetModelStateSectionFields(self)
                class(ModelState), intent(inout) :: self
        end subroutine
        subroutine DisconnectModelStateObjectFields(self)
                class(ModelState), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreModelStateScalarFields(self)
                class(ModelState), intent(inout) :: self
                call self%write('model_type', self%model_type)
        end subroutine
        subroutine LoadModelStateScalarFields(self)
                class(ModelState), intent(inout) :: self
                call self%read('model_type', self%model_type)
        end subroutine
        subroutine FinalizeModelState(self)
               type(ModelState), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearModelState(self)
                class(ModelState), intent(inout) :: self
                type(ModelState), save :: empty
                self = empty
        end subroutine
        pure elemental function IsModelStateEqual(lhs, rhs) result(iseq)
                class(ModelState), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ModelState)
                       iseq = iseq .and. (lhs%model_type == rhs%model_type)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorModelState(lhs, rhs)
                class(ModelState), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ModelState)
                       lhs%model_type = rhs%model_type
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateModelStateObjectFields(self)
                class(ModelState), intent(inout) :: self
        end subroutine



end module
