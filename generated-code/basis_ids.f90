
module basis_ids
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter



    implicit none
    public

    type, extends(persistent) :: BasisIds
        integer(kind=int32)  ::  muffin_tin =  int(0,kind=int32)

        integer(kind=int32)  ::  bands =  int(0,kind=int32)

        integer(kind=int32)  ::  correlated_orbitals =  int(0,kind=int32)

        integer(kind=int32)  ::  k_mesh =  int(0,kind=int32)

        integer(kind=int32)  ::  omega_mesh =  int(0,kind=int32)



        contains
        procedure :: AllocateObjectFields => AllocateBasisIdsObjectFields
        procedure :: ResetSectionFields   => ResetBasisIdsSectionFields
        procedure :: StoreScalarFields    => StoreBasisIdsScalarFields
        procedure :: StoreObjectFields    => StoreBasisIdsObjectFields
        procedure :: LoadScalarFields     => LoadBasisIdsScalarFields
        procedure :: LoadObjectFields     => LoadBasisIdsObjectFields
        procedure :: DisconnectObjectFields => DisconnectBasisIdsObjectFields
        procedure :: IsEqual              => IsBasisIdsEqual
        procedure :: AssignmentOperator   => AssignmentOperatorBasisIds
        procedure :: clear                => ClearBasisIds
        procedure :: init => InitBasisIds
#ifndef __GFORTRAN__
        final     :: FinalizeBasisIds
#endif

    end type

    contains
        subroutine InitBasisIds(self)
                class(BasisIds), intent(inout) :: self
                call self%InitPersistent()
                self%muffin_tin =  int(0,kind=int32)
                self%bands =  int(0,kind=int32)
                self%correlated_orbitals =  int(0,kind=int32)
                self%k_mesh =  int(0,kind=int32)
                self%omega_mesh =  int(0,kind=int32)
        end subroutine
        subroutine StoreBasisIdsObjectFields(self)
                class(BasisIds), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadBasisIdsObjectFields(self)
                class(BasisIds), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetBasisIdsSectionFields(self)
                class(BasisIds), intent(inout) :: self
        end subroutine
        subroutine DisconnectBasisIdsObjectFields(self)
                class(BasisIds), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreBasisIdsScalarFields(self)
                class(BasisIds), intent(inout) :: self
                call self%write('muffin_tin', self%muffin_tin)
                call self%write('bands', self%bands)
                call self%write('correlated_orbitals', self%correlated_orbitals)
                call self%write('k_mesh', self%k_mesh)
                call self%write('omega_mesh', self%omega_mesh)
        end subroutine
        subroutine LoadBasisIdsScalarFields(self)
                class(BasisIds), intent(inout) :: self
                call self%read('muffin_tin', self%muffin_tin)
                call self%read('bands', self%bands)
                call self%read('correlated_orbitals', self%correlated_orbitals)
                call self%read('k_mesh', self%k_mesh)
                call self%read('omega_mesh', self%omega_mesh)
        end subroutine
        subroutine FinalizeBasisIds(self)
               type(BasisIds), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearBasisIds(self)
                class(BasisIds), intent(inout) :: self
                type(BasisIds), save :: empty
                self = empty
        end subroutine
        pure elemental function IsBasisIdsEqual(lhs, rhs) result(iseq)
                class(BasisIds), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (BasisIds)
                       iseq = iseq .and. (lhs%muffin_tin == rhs%muffin_tin)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%bands == rhs%bands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%correlated_orbitals == rhs%correlated_orbitals)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%k_mesh == rhs%k_mesh)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega_mesh == rhs%omega_mesh)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorBasisIds(lhs, rhs)
                class(BasisIds), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (BasisIds)
                       lhs%muffin_tin = rhs%muffin_tin
                       lhs%bands = rhs%bands
                       lhs%correlated_orbitals = rhs%correlated_orbitals
                       lhs%k_mesh = rhs%k_mesh
                       lhs%omega_mesh = rhs%omega_mesh
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateBasisIdsObjectFields(self)
                class(BasisIds), intent(inout) :: self
        end subroutine



end module
