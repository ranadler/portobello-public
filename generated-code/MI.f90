
module MI
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter



    implicit none
    public

    type, extends(persistent) :: ImpurityProblem
        type(string)  ::  file_name
        type(string)  ::  description


        contains
        procedure :: AllocateObjectFields => AllocateImpurityProblemObjectFields
        procedure :: ResetSectionFields   => ResetImpurityProblemSectionFields
        procedure :: StoreScalarFields    => StoreImpurityProblemScalarFields
        procedure :: StoreObjectFields    => StoreImpurityProblemObjectFields
        procedure :: LoadScalarFields     => LoadImpurityProblemScalarFields
        procedure :: LoadObjectFields     => LoadImpurityProblemObjectFields
        procedure :: DisconnectObjectFields => DisconnectImpurityProblemObjectFields
        procedure :: IsEqual              => IsImpurityProblemEqual
        procedure :: AssignmentOperator   => AssignmentOperatorImpurityProblem
        procedure :: clear                => ClearImpurityProblem
        procedure :: init => InitImpurityProblem
#ifndef __GFORTRAN__
        final     :: FinalizeImpurityProblem
#endif

    end type

    type, extends(persistent) :: MultiImpuritySupervisor
        integer(kind=int32)  ::  num_impurities =  0

        logical  ::  ready = .False.

        type(string)  ::  solver_class
        type(string)  ::  solver_module
        type(ImpurityProblem), allocatable  ::  impurity_problem(:)


        contains
        procedure :: AllocateObjectFields => AllocateMultiImpuritySupervisorObjectFields
        procedure :: ResetSectionFields   => ResetMultiImpuritySupervisorSectionFields
        procedure :: StoreScalarFields    => StoreMultiImpuritySupervisorScalarFields
        procedure :: StoreObjectFields    => StoreMultiImpuritySupervisorObjectFields
        procedure :: LoadScalarFields     => LoadMultiImpuritySupervisorScalarFields
        procedure :: LoadObjectFields     => LoadMultiImpuritySupervisorObjectFields
        procedure :: DisconnectObjectFields => DisconnectMultiImpuritySupervisorObjectFields
        procedure :: IsEqual              => IsMultiImpuritySupervisorEqual
        procedure :: AssignmentOperator   => AssignmentOperatorMultiImpuritySupervisor
        procedure :: clear                => ClearMultiImpuritySupervisor
        procedure :: init => InitMultiImpuritySupervisor
#ifndef __GFORTRAN__
        final     :: FinalizeMultiImpuritySupervisor
#endif

    end type

    contains
        subroutine InitImpurityProblem(self)
                class(ImpurityProblem), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreImpurityProblemObjectFields(self)
                class(ImpurityProblem), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadImpurityProblemObjectFields(self)
                class(ImpurityProblem), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetImpurityProblemSectionFields(self)
                class(ImpurityProblem), intent(inout) :: self
        end subroutine
        subroutine DisconnectImpurityProblemObjectFields(self)
                class(ImpurityProblem), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreImpurityProblemScalarFields(self)
                class(ImpurityProblem), intent(inout) :: self
                call self%write('file_name', self%file_name)
                call self%write('description', self%description)
        end subroutine
        subroutine LoadImpurityProblemScalarFields(self)
                class(ImpurityProblem), intent(inout) :: self
                call self%read('file_name', self%file_name)
                call self%read('description', self%description)
        end subroutine
        subroutine FinalizeImpurityProblem(self)
               type(ImpurityProblem), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearImpurityProblem(self)
                class(ImpurityProblem), intent(inout) :: self
                type(ImpurityProblem), save :: empty
                self = empty
        end subroutine
        pure elemental function IsImpurityProblemEqual(lhs, rhs) result(iseq)
                class(ImpurityProblem), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ImpurityProblem)
                       iseq = iseq .and. (lhs%file_name == rhs%file_name)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%description == rhs%description)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorImpurityProblem(lhs, rhs)
                class(ImpurityProblem), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ImpurityProblem)
                       lhs%file_name = rhs%file_name
                       lhs%description = rhs%description
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateImpurityProblemObjectFields(self)
                class(ImpurityProblem), intent(inout) :: self
        end subroutine


        subroutine InitMultiImpuritySupervisor(self)
                class(MultiImpuritySupervisor), intent(inout) :: self
                call self%InitPersistent()
                self%num_impurities =  0
                self%ready = .False.
        end subroutine
        subroutine StoreMultiImpuritySupervisorObjectFields(self)
                class(MultiImpuritySupervisor), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                call iter%Init(lbound(self%impurity_problem), ubound(self%impurity_problem))
                do while (.not. iter%Done())
                    call self%impurity_problem(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'impurity_problem', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadMultiImpuritySupervisorObjectFields(self)
                class(MultiImpuritySupervisor), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                allocate(self%impurity_problem(int(1):int(self%num_impurities)))
                call iter%Init(lbound(self%impurity_problem), ubound(self%impurity_problem))
                do while (.not. iter%Done())
                    call self%impurity_problem(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'impurity_problem', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetMultiImpuritySupervisorSectionFields(self)
                class(MultiImpuritySupervisor), intent(inout) :: self
        end subroutine
        subroutine DisconnectMultiImpuritySupervisorObjectFields(self)
                class(MultiImpuritySupervisor), intent(inout) :: self
               type(iterator) :: iter

                call iter%Init(lbound(self%impurity_problem), ubound(self%impurity_problem))
                do while (.not. iter%Done())
                    call self%impurity_problem(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreMultiImpuritySupervisorScalarFields(self)
                class(MultiImpuritySupervisor), intent(inout) :: self
                call self%write('num_impurities', self%num_impurities)
                call self%write('ready', self%ready)
                call self%write('solver_class', self%solver_class)
                call self%write('solver_module', self%solver_module)
        end subroutine
        subroutine LoadMultiImpuritySupervisorScalarFields(self)
                class(MultiImpuritySupervisor), intent(inout) :: self
                call self%read('num_impurities', self%num_impurities)
                call self%read('ready', self%ready)
                call self%read('solver_class', self%solver_class)
                call self%read('solver_module', self%solver_module)
        end subroutine
        subroutine FinalizeMultiImpuritySupervisor(self)
               type(MultiImpuritySupervisor), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearMultiImpuritySupervisor(self)
                class(MultiImpuritySupervisor), intent(inout) :: self
                type(MultiImpuritySupervisor), save :: empty
                self = empty
        end subroutine
        pure elemental function IsMultiImpuritySupervisorEqual(lhs, rhs) result(iseq)
                class(MultiImpuritySupervisor), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (MultiImpuritySupervisor)
                       iseq = iseq .and. (lhs%num_impurities == rhs%num_impurities)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ready .eqv. rhs%ready)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%solver_class == rhs%solver_class)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%solver_module == rhs%solver_module)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%impurity_problem) .eqv. allocated(rhs%impurity_problem))
                       if (.not. iseq) return
                       if (allocated(lhs%impurity_problem)) then
                           iseq = iseq .and. all(shape(lhs%impurity_problem) == shape(rhs%impurity_problem))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%impurity_problem(:) == rhs%impurity_problem(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorMultiImpuritySupervisor(lhs, rhs)
                class(MultiImpuritySupervisor), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (MultiImpuritySupervisor)
                       lhs%num_impurities = rhs%num_impurities
                       lhs%ready = rhs%ready
                       lhs%solver_class = rhs%solver_class
                       lhs%solver_module = rhs%solver_module
                       lhs%impurity_problem = rhs%impurity_problem
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateMultiImpuritySupervisorObjectFields(self)
                class(MultiImpuritySupervisor), intent(inout) :: self
                allocate(self%impurity_problem(int(1):int(self%num_impurities)))
        end subroutine



end module
