
module maxent
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: Input
        integer(kind=int32)  ::  num_x =  0

        real(kind=dp)  ::  beta =  0.0d0

        integer(kind=int32)  ::  nomega =  0

        integer(kind=int32)  ::  FERMIONIC_TAU_KERNEL =  int(1,kind=int32)

        integer(kind=int32)  ::  FERMIONIC_IOMEGA_KERNEL =  int(2,kind=int32)

        integer(kind=int32)  ::  BOSONIC_TAU_KERNEL =  int(3,kind=int32)

        integer(kind=int32)  ::  BOSONIC_IOMEGA_KERNEL =  int(4,kind=int32)

        integer(kind=int32)  ::  kernel_choice =  int(1,kind=int32)

        type(matrix_complex)  ::  kernel
        complex(kind=dp),pointer :: kernel_(:,:)
        real(kind=dp)  ::  normalization =  real(1.000000,kind=16)

        type(vector_real)  ::  omega
        real(kind=dp),pointer :: omega_(:)
        type(vector_real)  ::  d_omega
        real(kind=dp),pointer :: d_omega_(:)
        type(matrix_real)  ::  dlda
        real(kind=dp),pointer :: dlda_(:,:)
        type(vector_real)  ::  model
        real(kind=dp),pointer :: model_(:)
        type(vector_real)  ::  Dp
        real(kind=dp),pointer :: Dp_(:)
        type(vector_real)  ::  Dm
        real(kind=dp),pointer :: Dm_(:)
        type(vector_complex)  ::  signal
        complex(kind=dp),pointer :: signal_(:)
        type(vector_real)  ::  invVar
        real(kind=dp),pointer :: invVar_(:)
        type(vector_complex)  ::  X
        complex(kind=dp),pointer :: X_(:)


        contains
        procedure :: AllocateObjectFields => AllocateInputObjectFields
        procedure :: ResetSectionFields   => ResetInputSectionFields
        procedure :: StoreScalarFields    => StoreInputScalarFields
        procedure :: StoreObjectFields    => StoreInputObjectFields
        procedure :: LoadScalarFields     => LoadInputScalarFields
        procedure :: LoadObjectFields     => LoadInputObjectFields
        procedure :: DisconnectObjectFields => DisconnectInputObjectFields
        procedure :: IsEqual              => IsInputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorInput
        procedure :: clear                => ClearInput
        procedure :: init => InitInput
#ifndef __GFORTRAN__
        final     :: FinalizeInput
#endif
        procedure :: GetInputKernelExtents
        procedure :: GetInputOmegaExtents
        procedure :: GetInputD_omegaExtents
        procedure :: GetInputDldaExtents
        procedure :: GetInputModelExtents
        procedure :: GetInputDpExtents
        procedure :: GetInputDmExtents
        procedure :: GetInputSignalExtents
        procedure :: GetInputInvvarExtents
        procedure :: GetInputXExtents

    end type
    interface
         function CompleteSetupI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    type, extends(persistent) :: Workspace
        integer(kind=int32)  ::  num_x =  0

        integer(kind=int32)  ::  nomega =  0

        real(kind=dp)  ::  alpha =  real(1000.000000,kind=16)

        real(kind=dp)  ::  rfac =  real(1.000000,kind=16)

        real(kind=dp)  ::  temp =  real(10.000000,kind=16)

        type(vector_real)  ::  A
        real(kind=dp),pointer :: A_(:)
        type(vector_real)  ::  Ap
        real(kind=dp),pointer :: Ap_(:)
        type(vector_real)  ::  Am
        real(kind=dp),pointer :: Am_(:)
        type(vector_real)  ::  CA
        real(kind=dp),pointer :: CA_(:)
        type(vector_real)  ::  dA
        real(kind=dp),pointer :: dA_(:)
        type(vector_real)  ::  dAp
        real(kind=dp),pointer :: dAp_(:)
        type(vector_real)  ::  dAm
        real(kind=dp),pointer :: dAm_(:)
        type(vector_real)  ::  dCA
        real(kind=dp),pointer :: dCA_(:)
        type(vector_complex)  ::  signal1
        complex(kind=dp),pointer :: signal1_(:)
        type(vector_complex)  ::  signal2
        complex(kind=dp),pointer :: signal2_(:)
        real(kind=dp)  ::  entropy =  0.0d0

        real(kind=dp)  ::  tr =  0.0d0



        contains
        procedure :: AllocateObjectFields => AllocateWorkspaceObjectFields
        procedure :: ResetSectionFields   => ResetWorkspaceSectionFields
        procedure :: StoreScalarFields    => StoreWorkspaceScalarFields
        procedure :: StoreObjectFields    => StoreWorkspaceObjectFields
        procedure :: LoadScalarFields     => LoadWorkspaceScalarFields
        procedure :: LoadObjectFields     => LoadWorkspaceObjectFields
        procedure :: DisconnectObjectFields => DisconnectWorkspaceObjectFields
        procedure :: IsEqual              => IsWorkspaceEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWorkspace
        procedure :: clear                => ClearWorkspace
        procedure :: init => InitWorkspace
#ifndef __GFORTRAN__
        final     :: FinalizeWorkspace
#endif
        procedure :: GetWorkspaceAExtents
        procedure :: GetWorkspaceApExtents
        procedure :: GetWorkspaceAmExtents
        procedure :: GetWorkspaceCaExtents
        procedure :: GetWorkspaceDaExtents
        procedure :: GetWorkspaceDapExtents
        procedure :: GetWorkspaceDamExtents
        procedure :: GetWorkspaceDcaExtents
        procedure :: GetWorkspaceSignal1Extents
        procedure :: GetWorkspaceSignal2Extents

    end type
    interface
         function MaxentI(selfpath, num_steps) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                integer(kind=c_int),intent(in),value :: num_steps
         end function
    end interface

    interface
         function MaxentOffDiagI(selfpath, num_steps) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                integer(kind=c_int),intent(in),value :: num_steps
         end function
    end interface


    contains
        subroutine InitInput(self)
                class(Input), intent(inout) :: self
                call self%InitPersistent()
                self%num_x =  0
                self%beta =  0.0d0
                self%nomega =  0
                self%FERMIONIC_TAU_KERNEL =  int(1,kind=int32)
                self%FERMIONIC_IOMEGA_KERNEL =  int(2,kind=int32)
                self%BOSONIC_TAU_KERNEL =  int(3,kind=int32)
                self%BOSONIC_IOMEGA_KERNEL =  int(4,kind=int32)
                self%kernel_choice =  int(1,kind=int32)
                self%normalization =  real(1.000000,kind=16)
        end subroutine
        subroutine StoreInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%kernel%StoreObject(ps, gid,  'kernel')
                call self%omega%StoreObject(ps, gid,  'omega')
                call self%d_omega%StoreObject(ps, gid,  'd_omega')
                call self%dlda%StoreObject(ps, gid,  'dlda')
                call self%model%StoreObject(ps, gid,  'model')
                call self%Dp%StoreObject(ps, gid,  'Dp')
                call self%Dm%StoreObject(ps, gid,  'Dm')
                call self%signal%StoreObject(ps, gid,  'signal')
                call self%invVar%StoreObject(ps, gid,  'invVar')
                call self%X%StoreObject(ps, gid,  'X')
        end subroutine
        subroutine LoadInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%kernel%LoadObject(ps, gid,  'kernel')
                call self%omega%LoadObject(ps, gid,  'omega')
                call self%d_omega%LoadObject(ps, gid,  'd_omega')
                call self%dlda%LoadObject(ps, gid,  'dlda')
                call self%model%LoadObject(ps, gid,  'model')
                call self%Dp%LoadObject(ps, gid,  'Dp')
                call self%Dm%LoadObject(ps, gid,  'Dm')
                call self%signal%LoadObject(ps, gid,  'signal')
                call self%invVar%LoadObject(ps, gid,  'invVar')
                call self%X%LoadObject(ps, gid,  'X')
        end subroutine
        subroutine ResetInputSectionFields(self)
                class(Input), intent(inout) :: self
                self%kernel_ => self%kernel%GetWithExtents(self%GetInputkernelExtents())
                self%omega_ => self%omega%GetWithExtents(self%GetInputomegaExtents())
                self%d_omega_ => self%d_omega%GetWithExtents(self%GetInputd_omegaExtents())
                self%dlda_ => self%dlda%GetWithExtents(self%GetInputdldaExtents())
                self%model_ => self%model%GetWithExtents(self%GetInputmodelExtents())
                self%Dp_ => self%Dp%GetWithExtents(self%GetInputDpExtents())
                self%Dm_ => self%Dm%GetWithExtents(self%GetInputDmExtents())
                self%signal_ => self%signal%GetWithExtents(self%GetInputsignalExtents())
                self%invVar_ => self%invVar%GetWithExtents(self%GetInputinvVarExtents())
                self%X_ => self%X%GetWithExtents(self%GetInputXExtents())
        end subroutine
        subroutine DisconnectInputObjectFields(self)
                class(Input), intent(inout) :: self
               type(iterator) :: iter
                call self%kernel%DisconnectFromStore()
                call self%omega%DisconnectFromStore()
                call self%d_omega%DisconnectFromStore()
                call self%dlda%DisconnectFromStore()
                call self%model%DisconnectFromStore()
                call self%Dp%DisconnectFromStore()
                call self%Dm%DisconnectFromStore()
                call self%signal%DisconnectFromStore()
                call self%invVar%DisconnectFromStore()
                call self%X%DisconnectFromStore()
        end subroutine
        subroutine StoreInputScalarFields(self)
                class(Input), intent(inout) :: self
                call self%write('num_x', self%num_x)
                call self%write('beta', self%beta)
                call self%write('nomega', self%nomega)
                call self%write('FERMIONIC_TAU_KERNEL', self%FERMIONIC_TAU_KERNEL)
                call self%write('FERMIONIC_IOMEGA_KERNEL', self%FERMIONIC_IOMEGA_KERNEL)
                call self%write('BOSONIC_TAU_KERNEL', self%BOSONIC_TAU_KERNEL)
                call self%write('BOSONIC_IOMEGA_KERNEL', self%BOSONIC_IOMEGA_KERNEL)
                call self%write('kernel_choice', self%kernel_choice)
                call self%write('normalization', self%normalization)
        end subroutine
        subroutine LoadInputScalarFields(self)
                class(Input), intent(inout) :: self
                call self%read('num_x', self%num_x)
                call self%read('beta', self%beta)
                call self%read('nomega', self%nomega)
                call self%read('FERMIONIC_TAU_KERNEL', self%FERMIONIC_TAU_KERNEL)
                call self%read('FERMIONIC_IOMEGA_KERNEL', self%FERMIONIC_IOMEGA_KERNEL)
                call self%read('BOSONIC_TAU_KERNEL', self%BOSONIC_TAU_KERNEL)
                call self%read('BOSONIC_IOMEGA_KERNEL', self%BOSONIC_IOMEGA_KERNEL)
                call self%read('kernel_choice', self%kernel_choice)
                call self%read('normalization', self%normalization)
        end subroutine
        subroutine FinalizeInput(self)
               type(Input), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearInput(self)
                class(Input), intent(inout) :: self
                type(Input), save :: empty
                self = empty
        end subroutine
        pure elemental function IsInputEqual(lhs, rhs) result(iseq)
                class(Input), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Input)
                       iseq = iseq .and. (lhs%num_x == rhs%num_x)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%beta == rhs%beta)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nomega == rhs%nomega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%FERMIONIC_TAU_KERNEL == rhs%FERMIONIC_TAU_KERNEL)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%FERMIONIC_IOMEGA_KERNEL == rhs%FERMIONIC_IOMEGA_KERNEL)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%BOSONIC_TAU_KERNEL == rhs%BOSONIC_TAU_KERNEL)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%BOSONIC_IOMEGA_KERNEL == rhs%BOSONIC_IOMEGA_KERNEL)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%kernel_choice == rhs%kernel_choice)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%kernel == rhs%kernel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%normalization == rhs%normalization)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega == rhs%omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%d_omega == rhs%d_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dlda == rhs%dlda)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%model == rhs%model)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Dp == rhs%Dp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Dm == rhs%Dm)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%signal == rhs%signal)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%invVar == rhs%invVar)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%X == rhs%X)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorInput(lhs, rhs)
                class(Input), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Input)
                       lhs%num_x = rhs%num_x
                       lhs%beta = rhs%beta
                       lhs%nomega = rhs%nomega
                       lhs%FERMIONIC_TAU_KERNEL = rhs%FERMIONIC_TAU_KERNEL
                       lhs%FERMIONIC_IOMEGA_KERNEL = rhs%FERMIONIC_IOMEGA_KERNEL
                       lhs%BOSONIC_TAU_KERNEL = rhs%BOSONIC_TAU_KERNEL
                       lhs%BOSONIC_IOMEGA_KERNEL = rhs%BOSONIC_IOMEGA_KERNEL
                       lhs%kernel_choice = rhs%kernel_choice
                       lhs%kernel = rhs%kernel
                       lhs%normalization = rhs%normalization
                       lhs%omega = rhs%omega
                       lhs%d_omega = rhs%d_omega
                       lhs%dlda = rhs%dlda
                       lhs%model = rhs%model
                       lhs%Dp = rhs%Dp
                       lhs%Dm = rhs%Dm
                       lhs%signal = rhs%signal
                       lhs%invVar = rhs%invVar
                       lhs%X = rhs%X
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetInputKernelExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(-self%nomega,self%nomega),extent(1,self%num_x)]
        end function
        function GetInputOmegaExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetInputD_omegaExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetInputDldaExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,2 * self%nomega + 1),extent(1,2 * self%nomega + 1)]
        end function
        function GetInputModelExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetInputDpExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetInputDmExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetInputSignalExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_x)]
        end function
        function GetInputInvvarExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_x)]
        end function
        function GetInputXExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_x)]
        end function
        subroutine AllocateInputObjectFields(self)
                class(Input), intent(inout) :: self
                call self%kernel%init(int(self%nomega- (-self%nomega) + 1),int(self%num_x))
                call self%omega%init(int(self%nomega- (-self%nomega) + 1))
                call self%d_omega%init(int(self%nomega- (-self%nomega) + 1))
                call self%dlda%init(int(2 * self%nomega + 1),int(2 * self%nomega + 1))
                call self%model%init(int(self%nomega- (-self%nomega) + 1))
                call self%Dp%init(int(self%nomega- (-self%nomega) + 1))
                call self%Dm%init(int(self%nomega- (-self%nomega) + 1))
                call self%signal%init(int(self%num_x))
                call self%invVar%init(int(self%num_x))
                call self%X%init(int(self%num_x))
        end subroutine


        subroutine CompleteSetupWrapper(ret, imp_fp, selfpath) bind(C,name='maxent_mp_completesetupwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(CompleteSetupI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine InitWorkspace(self)
                class(Workspace), intent(inout) :: self
                call self%InitPersistent()
                self%num_x =  0
                self%nomega =  0
                self%alpha =  real(1000.000000,kind=16)
                self%rfac =  real(1.000000,kind=16)
                self%temp =  real(10.000000,kind=16)
                self%entropy =  0.0d0
                self%tr =  0.0d0
        end subroutine
        subroutine StoreWorkspaceObjectFields(self)
                class(Workspace), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%A%StoreObject(ps, gid,  'A')
                call self%Ap%StoreObject(ps, gid,  'Ap')
                call self%Am%StoreObject(ps, gid,  'Am')
                call self%CA%StoreObject(ps, gid,  'CA')
                call self%dA%StoreObject(ps, gid,  'dA')
                call self%dAp%StoreObject(ps, gid,  'dAp')
                call self%dAm%StoreObject(ps, gid,  'dAm')
                call self%dCA%StoreObject(ps, gid,  'dCA')
                call self%signal1%StoreObject(ps, gid,  'signal1')
                call self%signal2%StoreObject(ps, gid,  'signal2')
        end subroutine
        subroutine LoadWorkspaceObjectFields(self)
                class(Workspace), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%A%LoadObject(ps, gid,  'A')
                call self%Ap%LoadObject(ps, gid,  'Ap')
                call self%Am%LoadObject(ps, gid,  'Am')
                call self%CA%LoadObject(ps, gid,  'CA')
                call self%dA%LoadObject(ps, gid,  'dA')
                call self%dAp%LoadObject(ps, gid,  'dAp')
                call self%dAm%LoadObject(ps, gid,  'dAm')
                call self%dCA%LoadObject(ps, gid,  'dCA')
                call self%signal1%LoadObject(ps, gid,  'signal1')
                call self%signal2%LoadObject(ps, gid,  'signal2')
        end subroutine
        subroutine ResetWorkspaceSectionFields(self)
                class(Workspace), intent(inout) :: self
                self%A_ => self%A%GetWithExtents(self%GetWorkspaceAExtents())
                self%Ap_ => self%Ap%GetWithExtents(self%GetWorkspaceApExtents())
                self%Am_ => self%Am%GetWithExtents(self%GetWorkspaceAmExtents())
                self%CA_ => self%CA%GetWithExtents(self%GetWorkspaceCAExtents())
                self%dA_ => self%dA%GetWithExtents(self%GetWorkspacedAExtents())
                self%dAp_ => self%dAp%GetWithExtents(self%GetWorkspacedApExtents())
                self%dAm_ => self%dAm%GetWithExtents(self%GetWorkspacedAmExtents())
                self%dCA_ => self%dCA%GetWithExtents(self%GetWorkspacedCAExtents())
                self%signal1_ => self%signal1%GetWithExtents(self%GetWorkspacesignal1Extents())
                self%signal2_ => self%signal2%GetWithExtents(self%GetWorkspacesignal2Extents())
        end subroutine
        subroutine DisconnectWorkspaceObjectFields(self)
                class(Workspace), intent(inout) :: self
               type(iterator) :: iter
                call self%A%DisconnectFromStore()
                call self%Ap%DisconnectFromStore()
                call self%Am%DisconnectFromStore()
                call self%CA%DisconnectFromStore()
                call self%dA%DisconnectFromStore()
                call self%dAp%DisconnectFromStore()
                call self%dAm%DisconnectFromStore()
                call self%dCA%DisconnectFromStore()
                call self%signal1%DisconnectFromStore()
                call self%signal2%DisconnectFromStore()
        end subroutine
        subroutine StoreWorkspaceScalarFields(self)
                class(Workspace), intent(inout) :: self
                call self%write('num_x', self%num_x)
                call self%write('nomega', self%nomega)
                call self%write('alpha', self%alpha)
                call self%write('rfac', self%rfac)
                call self%write('temp', self%temp)
                call self%write('entropy', self%entropy)
                call self%write('tr', self%tr)
        end subroutine
        subroutine LoadWorkspaceScalarFields(self)
                class(Workspace), intent(inout) :: self
                call self%read('num_x', self%num_x)
                call self%read('nomega', self%nomega)
                call self%read('alpha', self%alpha)
                call self%read('rfac', self%rfac)
                call self%read('temp', self%temp)
                call self%read('entropy', self%entropy)
                call self%read('tr', self%tr)
        end subroutine
        subroutine FinalizeWorkspace(self)
               type(Workspace), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWorkspace(self)
                class(Workspace), intent(inout) :: self
                type(Workspace), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWorkspaceEqual(lhs, rhs) result(iseq)
                class(Workspace), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Workspace)
                       iseq = iseq .and. (lhs%num_x == rhs%num_x)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nomega == rhs%nomega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%alpha == rhs%alpha)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rfac == rhs%rfac)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%temp == rhs%temp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%A == rhs%A)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Ap == rhs%Ap)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Am == rhs%Am)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%CA == rhs%CA)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dA == rhs%dA)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dAp == rhs%dAp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dAm == rhs%dAm)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dCA == rhs%dCA)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%signal1 == rhs%signal1)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%signal2 == rhs%signal2)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%entropy == rhs%entropy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%tr == rhs%tr)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWorkspace(lhs, rhs)
                class(Workspace), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Workspace)
                       lhs%num_x = rhs%num_x
                       lhs%nomega = rhs%nomega
                       lhs%alpha = rhs%alpha
                       lhs%rfac = rhs%rfac
                       lhs%temp = rhs%temp
                       lhs%A = rhs%A
                       lhs%Ap = rhs%Ap
                       lhs%Am = rhs%Am
                       lhs%CA = rhs%CA
                       lhs%dA = rhs%dA
                       lhs%dAp = rhs%dAp
                       lhs%dAm = rhs%dAm
                       lhs%dCA = rhs%dCA
                       lhs%signal1 = rhs%signal1
                       lhs%signal2 = rhs%signal2
                       lhs%entropy = rhs%entropy
                       lhs%tr = rhs%tr
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetWorkspaceAExtents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetWorkspaceApExtents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetWorkspaceAmExtents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetWorkspaceCaExtents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetWorkspaceDaExtents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetWorkspaceDapExtents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetWorkspaceDamExtents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetWorkspaceDcaExtents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%nomega,self%nomega)]
        end function
        function GetWorkspaceSignal1Extents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_x)]
        end function
        function GetWorkspaceSignal2Extents(self) result(res)
                class(Workspace), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_x)]
        end function
        subroutine AllocateWorkspaceObjectFields(self)
                class(Workspace), intent(inout) :: self
                call self%A%init(int(self%nomega- (-self%nomega) + 1))
                call self%Ap%init(int(self%nomega- (-self%nomega) + 1))
                call self%Am%init(int(self%nomega- (-self%nomega) + 1))
                call self%CA%init(int(self%nomega- (-self%nomega) + 1))
                call self%dA%init(int(self%nomega- (-self%nomega) + 1))
                call self%dAp%init(int(self%nomega- (-self%nomega) + 1))
                call self%dAm%init(int(self%nomega- (-self%nomega) + 1))
                call self%dCA%init(int(self%nomega- (-self%nomega) + 1))
                call self%signal1%init(int(self%num_x))
                call self%signal2%init(int(self%num_x))
        end subroutine


        subroutine MaxentWrapper(ret, imp_fp, selfpath, num_steps) bind(C,name='maxent_mp_maxentwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                integer(kind=c_int),intent(in),value :: num_steps
            procedure(MaxentI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), num_steps)
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine MaxentOffDiagWrapper(ret, imp_fp, selfpath, num_steps) bind(C,name='maxent_mp_maxentoffdiagwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                integer(kind=c_int),intent(in),value :: num_steps
            procedure(MaxentOffDiagI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), num_steps)
            ret(1)= NewCStr(res%chars())
        end subroutine


end module
