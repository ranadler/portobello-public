
module example
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use base_types
    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(ComputationInput) :: ExampleInput
        type(string)  ::  full_path


        contains
        procedure :: AllocateObjectFields => AllocateExampleInputObjectFields
        procedure :: ResetSectionFields   => ResetExampleInputSectionFields
        procedure :: StoreScalarFields    => StoreExampleInputScalarFields
        procedure :: StoreObjectFields    => StoreExampleInputObjectFields
        procedure :: LoadScalarFields     => LoadExampleInputScalarFields
        procedure :: LoadObjectFields     => LoadExampleInputObjectFields
        procedure :: DisconnectObjectFields => DisconnectExampleInputObjectFields
        procedure :: IsEqual              => IsExampleInputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorExampleInput
        procedure :: clear                => ClearExampleInput
        procedure :: init => InitExampleInput
        final     :: FinalizeExampleInput

    end type
    interface
         function SomeBasicCalculationI(selfpath, inp) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                integer(kind=c_int),intent(in),value :: inp
         end function
    end interface


    type, extends(ComputationOutput) :: Orbitals
        integer(kind=int32)  ::  num_atoms =  0

        integer(kind=int32)  ::  num_bands =  0

        integer(kind=int32)  ::  num_orbs =  0

        type(vector_int)  ::  nearest_atom
        integer(kind=int32),pointer :: nearest_atom_(:)
        type(matrix_real)  ::  distance_to_nearest_atom
        real(kind=dp),pointer :: distance_to_nearest_atom_(:,:)
        type(matrix_real)  ::  centers
        real(kind=dp),pointer :: centers_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateOrbitalsObjectFields
        procedure :: ResetSectionFields   => ResetOrbitalsSectionFields
        procedure :: StoreScalarFields    => StoreOrbitalsScalarFields
        procedure :: StoreObjectFields    => StoreOrbitalsObjectFields
        procedure :: LoadScalarFields     => LoadOrbitalsScalarFields
        procedure :: LoadObjectFields     => LoadOrbitalsObjectFields
        procedure :: DisconnectObjectFields => DisconnectOrbitalsObjectFields
        procedure :: IsEqual              => IsOrbitalsEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOrbitals
        procedure :: clear                => ClearOrbitals
        procedure :: init => InitOrbitals
        final     :: FinalizeOrbitals
        procedure :: GetOrbitalsNearest_atomExtents
        procedure :: GetOrbitalsDistance_to_nearest_atomExtents
        procedure :: GetOrbitalsCentersExtents

    end type

    type, extends(Orbitals) :: ExampleOutput
        integer(kind=int32)  ::  num_k =  0

        type(ExampleInput)  ::  input
        type(matrix_real)  ::  k_lat
        real(kind=dp),pointer :: k_lat_(:,:)
        type(cube_complex)  ::  V
        complex(kind=dp),pointer :: V_(:,:,:)
        type(ExampleInput), allocatable  ::  inp_per_orb(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateExampleOutputObjectFields
        procedure :: ResetSectionFields   => ResetExampleOutputSectionFields
        procedure :: StoreScalarFields    => StoreExampleOutputScalarFields
        procedure :: StoreObjectFields    => StoreExampleOutputObjectFields
        procedure :: LoadScalarFields     => LoadExampleOutputScalarFields
        procedure :: LoadObjectFields     => LoadExampleOutputObjectFields
        procedure :: DisconnectObjectFields => DisconnectExampleOutputObjectFields
        procedure :: IsEqual              => IsExampleOutputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorExampleOutput
        procedure :: clear                => ClearExampleOutput
        procedure :: init => InitExampleOutput
        final     :: FinalizeExampleOutput
        procedure :: GetExampleoutputK_latExtents
        procedure :: GetExampleoutputVExtents

    end type
    interface
         function ComputeI(selfpath, low_energy, high_energy, i1, other) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                real(kind=c_float),intent(in),value :: low_energy
                real(kind=c_float),intent(in),value :: high_energy
                integer(kind=c_int),intent(in),value :: i1
                 type(string), intent(in)            :: other
         end function
    end interface


    type, extends(persistent) :: Simple
        integer(kind=int32)  ::  m =  0

        integer(kind=int32)  ::  n =  0

        type(matrix_real)  ::  d
        real(kind=dp),pointer :: d_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateSimpleObjectFields
        procedure :: ResetSectionFields   => ResetSimpleSectionFields
        procedure :: StoreScalarFields    => StoreSimpleScalarFields
        procedure :: StoreObjectFields    => StoreSimpleObjectFields
        procedure :: LoadScalarFields     => LoadSimpleScalarFields
        procedure :: LoadObjectFields     => LoadSimpleObjectFields
        procedure :: DisconnectObjectFields => DisconnectSimpleObjectFields
        procedure :: IsEqual              => IsSimpleEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSimple
        procedure :: clear                => ClearSimple
        procedure :: init => InitSimple
        final     :: FinalizeSimple
        procedure :: GetSimpleDExtents

    end type
    interface
         function SimpleCallI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    contains
        subroutine InitExampleInput(self)
                class(ExampleInput), intent(inout) :: self
                call self%ComputationInput%Init()
        end subroutine
        subroutine StoreExampleInputObjectFields(self)
                class(ExampleInput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ComputationInput%StoreObjectFields()
        end subroutine
        subroutine LoadExampleInputObjectFields(self)
                class(ExampleInput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ComputationInput%LoadObjectFields()
        end subroutine
        subroutine ResetExampleInputSectionFields(self)
                class(ExampleInput), intent(inout) :: self
                call self%ComputationInput%ResetSectionFields()
        end subroutine
        subroutine DisconnectExampleInputObjectFields(self)
                class(ExampleInput), intent(inout) :: self
               type(iterator) :: iter
                call self%ComputationInput%DisconnectObjectFields()
        end subroutine
        subroutine StoreExampleInputScalarFields(self)
                class(ExampleInput), intent(inout) :: self
                call self%ComputationInput%StoreScalarFields()
                call self%write('full_path', self%full_path)
        end subroutine
        subroutine LoadExampleInputScalarFields(self)
                class(ExampleInput), intent(inout) :: self
                call self%ComputationInput%LoadScalarFields()
                call self%read('full_path', self%full_path)
        end subroutine
        subroutine FinalizeExampleInput(self)
               type(ExampleInput), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearExampleInput(self)
                class(ExampleInput), intent(inout) :: self
                type(ExampleInput), save :: empty
                self = empty
        end subroutine
        pure elemental function IsExampleInputEqual(lhs, rhs) result(iseq)
                class(ExampleInput), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ExampleInput)
                       iseq = iseq .and. (lhs%ComputationInput == rhs%ComputationInput)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%full_path == rhs%full_path)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorExampleInput(lhs, rhs)
                class(ExampleInput), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ExampleInput)
                       lhs%ComputationInput = rhs%ComputationInput
                       lhs%full_path = rhs%full_path
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateExampleInputObjectFields(self)
                class(ExampleInput), intent(inout) :: self
                call self%ComputationInput%AllocateObjectFields()
        end subroutine


        subroutine SomeBasicCalculationWrapper(ret, imp_fp, selfpath, inp) bind(C,name='example_mp_somebasiccalculationwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                integer(kind=c_int),intent(in),value :: inp
            procedure(SomeBasicCalculationI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), inp)
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine InitOrbitals(self)
                class(Orbitals), intent(inout) :: self
                call self%ComputationOutput%Init()
                self%num_atoms =  0
                self%num_bands =  0
                self%num_orbs =  0
        end subroutine
        subroutine StoreOrbitalsObjectFields(self)
                class(Orbitals), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ComputationOutput%StoreObjectFields()
                call self%nearest_atom%StoreObject(ps, gid,  'nearest_atom')
                call self%distance_to_nearest_atom%StoreObject(ps, gid,  'distance_to_nearest_atom')
                call self%centers%StoreObject(ps, gid,  'centers')
        end subroutine
        subroutine LoadOrbitalsObjectFields(self)
                class(Orbitals), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ComputationOutput%LoadObjectFields()
                call self%nearest_atom%LoadObject(ps, gid,  'nearest_atom')
                call self%distance_to_nearest_atom%LoadObject(ps, gid,  'distance_to_nearest_atom')
                call self%centers%LoadObject(ps, gid,  'centers')
        end subroutine
        subroutine ResetOrbitalsSectionFields(self)
                class(Orbitals), intent(inout) :: self
                call self%ComputationOutput%ResetSectionFields()
                self%nearest_atom_ => self%nearest_atom%GetWithExtents(self%GetOrbitalsnearest_atomExtents())
                self%distance_to_nearest_atom_ => self%distance_to_nearest_atom%GetWithExtents(self%GetOrbitalsdistance_to_nearest_atomExtents())
                self%centers_ => self%centers%GetWithExtents(self%GetOrbitalscentersExtents())
        end subroutine
        subroutine DisconnectOrbitalsObjectFields(self)
                class(Orbitals), intent(inout) :: self
               type(iterator) :: iter
                call self%ComputationOutput%DisconnectObjectFields()
                call self%nearest_atom%DisconnectFromStore()
                call self%distance_to_nearest_atom%DisconnectFromStore()
                call self%centers%DisconnectFromStore()
        end subroutine
        subroutine StoreOrbitalsScalarFields(self)
                class(Orbitals), intent(inout) :: self
                call self%ComputationOutput%StoreScalarFields()
                call self%write('num_atoms', self%num_atoms)
                call self%write('num_bands', self%num_bands)
                call self%write('num_orbs', self%num_orbs)
        end subroutine
        subroutine LoadOrbitalsScalarFields(self)
                class(Orbitals), intent(inout) :: self
                call self%ComputationOutput%LoadScalarFields()
                call self%read('num_atoms', self%num_atoms)
                call self%read('num_bands', self%num_bands)
                call self%read('num_orbs', self%num_orbs)
        end subroutine
        subroutine FinalizeOrbitals(self)
               type(Orbitals), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOrbitals(self)
                class(Orbitals), intent(inout) :: self
                type(Orbitals), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOrbitalsEqual(lhs, rhs) result(iseq)
                class(Orbitals), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Orbitals)
                       iseq = iseq .and. (lhs%ComputationOutput == rhs%ComputationOutput)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_atoms == rhs%num_atoms)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_bands == rhs%num_bands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_orbs == rhs%num_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nearest_atom == rhs%nearest_atom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%distance_to_nearest_atom == rhs%distance_to_nearest_atom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%centers == rhs%centers)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOrbitals(lhs, rhs)
                class(Orbitals), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Orbitals)
                       lhs%ComputationOutput = rhs%ComputationOutput
                       lhs%num_atoms = rhs%num_atoms
                       lhs%num_bands = rhs%num_bands
                       lhs%num_orbs = rhs%num_orbs
                       lhs%nearest_atom = rhs%nearest_atom
                       lhs%distance_to_nearest_atom = rhs%distance_to_nearest_atom
                       lhs%centers = rhs%centers
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetOrbitalsNearest_atomExtents(self) result(res)
                class(Orbitals), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_orbs)]
        end function
        function GetOrbitalsDistance_to_nearest_atomExtents(self) result(res)
                class(Orbitals), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_orbs)]
        end function
        function GetOrbitalsCentersExtents(self) result(res)
                class(Orbitals), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_orbs)]
        end function
        subroutine AllocateOrbitalsObjectFields(self)
                class(Orbitals), intent(inout) :: self
                call self%ComputationOutput%AllocateObjectFields()
                call self%nearest_atom%init(int(self%num_orbs))
                call self%distance_to_nearest_atom%init(int(3),int(self%num_orbs))
                call self%centers%init(int(3),int(self%num_orbs))
        end subroutine


        subroutine InitExampleOutput(self)
                class(ExampleOutput), intent(inout) :: self
                call self%Orbitals%Init()
                self%num_k =  0
                call self%input%InitPersistent()
        end subroutine
        subroutine StoreExampleOutputObjectFields(self)
                class(ExampleOutput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Orbitals%StoreObjectFields()
                call self%input%store(ps%FetchSubGroup(gid,  'input'))
                call self%k_lat%StoreObject(ps, gid,  'k_lat')
                call self%V%StoreObject(ps, gid,  'V')

                call iter%Init(lbound(self%inp_per_orb), ubound(self%inp_per_orb))
                do while (.not. iter%Done())
                    call self%inp_per_orb(iter%i(1),iter%i(2))%store( &
                                ps%FetchIndexInSubGroup(gid, 'inp_per_orb', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadExampleOutputObjectFields(self)
                class(ExampleOutput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Orbitals%LoadObjectFields()
                call self%input%load(ps%FetchSubGroup(gid,  'input'))
                call self%k_lat%LoadObject(ps, gid,  'k_lat')
                call self%V%LoadObject(ps, gid,  'V')

                allocate(self%inp_per_orb(int(1):int(self%num_orbs),int(1):int(self%num_atoms)))
                call iter%Init(lbound(self%inp_per_orb), ubound(self%inp_per_orb))
                do while (.not. iter%Done())
                    call self%inp_per_orb(iter%i(1),iter%i(2))%load( &
                                ps%FetchIndexInSubGroup(gid, 'inp_per_orb', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetExampleOutputSectionFields(self)
                class(ExampleOutput), intent(inout) :: self
                call self%Orbitals%ResetSectionFields()
                self%k_lat_ => self%k_lat%GetWithExtents(self%GetExampleOutputk_latExtents())
                self%V_ => self%V%GetWithExtents(self%GetExampleOutputVExtents())
        end subroutine
        subroutine DisconnectExampleOutputObjectFields(self)
                class(ExampleOutput), intent(inout) :: self
               type(iterator) :: iter
                call self%Orbitals%DisconnectObjectFields()
                call self%input%disconnect()
                call self%k_lat%DisconnectFromStore()
                call self%V%DisconnectFromStore()

                call iter%Init(lbound(self%inp_per_orb), ubound(self%inp_per_orb))
                do while (.not. iter%Done())
                    call self%inp_per_orb(iter%i(1),iter%i(2))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreExampleOutputScalarFields(self)
                class(ExampleOutput), intent(inout) :: self
                call self%Orbitals%StoreScalarFields()
                call self%write('num_k', self%num_k)
        end subroutine
        subroutine LoadExampleOutputScalarFields(self)
                class(ExampleOutput), intent(inout) :: self
                call self%Orbitals%LoadScalarFields()
                call self%read('num_k', self%num_k)
        end subroutine
        subroutine FinalizeExampleOutput(self)
               type(ExampleOutput), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearExampleOutput(self)
                class(ExampleOutput), intent(inout) :: self
                type(ExampleOutput), save :: empty
                self = empty
        end subroutine
        pure elemental function IsExampleOutputEqual(lhs, rhs) result(iseq)
                class(ExampleOutput), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ExampleOutput)
                       iseq = iseq .and. (lhs%Orbitals == rhs%Orbitals)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%input == rhs%input)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%k_lat == rhs%k_lat)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%V == rhs%V)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%inp_per_orb) .eqv. allocated(rhs%inp_per_orb))
                       if (.not. iseq) return
                       if (allocated(lhs%inp_per_orb)) then
                           iseq = iseq .and. all(shape(lhs%inp_per_orb) == shape(rhs%inp_per_orb))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%inp_per_orb(:,:) == rhs%inp_per_orb(:,:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorExampleOutput(lhs, rhs)
                class(ExampleOutput), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ExampleOutput)
                       lhs%Orbitals = rhs%Orbitals
                       lhs%num_k = rhs%num_k
                       lhs%input = rhs%input
                       lhs%k_lat = rhs%k_lat
                       lhs%V = rhs%V
                       lhs%inp_per_orb = rhs%inp_per_orb
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetExampleoutputK_latExtents(self) result(res)
                class(ExampleOutput), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_k)]
        end function
        function GetExampleoutputVExtents(self) result(res)
                class(ExampleOutput), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_bands),extent(1,self%num_orbs),extent(1,self%num_k)]
        end function
        subroutine AllocateExampleOutputObjectFields(self)
                class(ExampleOutput), intent(inout) :: self
                call self%Orbitals%AllocateObjectFields()
                call self%k_lat%init(int(3),int(self%num_k))
                call self%V%init(int(self%num_bands),int(self%num_orbs),int(self%num_k))
                allocate(self%inp_per_orb(int(1):int(self%num_orbs),int(1):int(self%num_atoms)))
        end subroutine


        subroutine ComputeWrapper(ret, imp_fp, selfpath, low_energy, high_energy, i1, other) bind(C,name='example_mp_computewrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                real(kind=c_float),intent(in),value :: low_energy
                real(kind=c_float),intent(in),value :: high_energy
                integer(kind=c_int),intent(in),value :: i1
                type(c_ptr),intent(in),value        :: other
            procedure(ComputeI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), low_energy, high_energy, i1, FromCStr(other))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine InitSimple(self)
                class(Simple), intent(inout) :: self
                call self%InitPersistent()
                self%m =  0
                self%n =  0
        end subroutine
        subroutine StoreSimpleObjectFields(self)
                class(Simple), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%d%StoreObject(ps, gid,  'd')
        end subroutine
        subroutine LoadSimpleObjectFields(self)
                class(Simple), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%d%LoadObject(ps, gid,  'd')
        end subroutine
        subroutine ResetSimpleSectionFields(self)
                class(Simple), intent(inout) :: self
                self%d_ => self%d%GetWithExtents(self%GetSimpledExtents())
        end subroutine
        subroutine DisconnectSimpleObjectFields(self)
                class(Simple), intent(inout) :: self
               type(iterator) :: iter
                call self%d%DisconnectFromStore()
        end subroutine
        subroutine StoreSimpleScalarFields(self)
                class(Simple), intent(inout) :: self
                call self%write('m', self%m)
                call self%write('n', self%n)
        end subroutine
        subroutine LoadSimpleScalarFields(self)
                class(Simple), intent(inout) :: self
                call self%read('m', self%m)
                call self%read('n', self%n)
        end subroutine
        subroutine FinalizeSimple(self)
               type(Simple), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSimple(self)
                class(Simple), intent(inout) :: self
                type(Simple), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSimpleEqual(lhs, rhs) result(iseq)
                class(Simple), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Simple)
                       iseq = iseq .and. (lhs%m == rhs%m)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%n == rhs%n)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%d == rhs%d)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSimple(lhs, rhs)
                class(Simple), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Simple)
                       lhs%m = rhs%m
                       lhs%n = rhs%n
                       lhs%d = rhs%d
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetSimpleDExtents(self) result(res)
                class(Simple), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%m),extent(1,self%n)]
        end function
        subroutine AllocateSimpleObjectFields(self)
                class(Simple), intent(inout) :: self
                call self%d%init(int(self%m),int(self%n))
        end subroutine


        subroutine SimpleCallWrapper(ret, imp_fp, selfpath) bind(C,name='example_mp_simplecallwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(SimpleCallI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine


end module
