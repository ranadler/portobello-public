
module ARPES
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use array5
    use vector


    implicit none
    public

    type, extends(persistent) :: label
        type(string)  ::  text


        contains
        procedure :: AllocateObjectFields => AllocatelabelObjectFields
        procedure :: ResetSectionFields   => ResetlabelSectionFields
        procedure :: StoreScalarFields    => StorelabelScalarFields
        procedure :: StoreObjectFields    => StorelabelObjectFields
        procedure :: LoadScalarFields     => LoadlabelScalarFields
        procedure :: LoadObjectFields     => LoadlabelObjectFields
        procedure :: DisconnectObjectFields => DisconnectlabelObjectFields
        procedure :: IsEqual              => IslabelEqual
        procedure :: AssignmentOperator   => AssignmentOperatorlabel
        procedure :: clear                => Clearlabel
        procedure :: init => Initlabel
#ifndef __GFORTRAN__
        final     :: Finalizelabel
#endif

    end type

    type, extends(persistent) :: HSKP
        integer(kind=int32)  ::  npoints =  0

        type(label), allocatable  ::  labels(:)


        contains
        procedure :: AllocateObjectFields => AllocateHSKPObjectFields
        procedure :: ResetSectionFields   => ResetHSKPSectionFields
        procedure :: StoreScalarFields    => StoreHSKPScalarFields
        procedure :: StoreObjectFields    => StoreHSKPObjectFields
        procedure :: LoadScalarFields     => LoadHSKPScalarFields
        procedure :: LoadObjectFields     => LoadHSKPObjectFields
        procedure :: DisconnectObjectFields => DisconnectHSKPObjectFields
        procedure :: IsEqual              => IsHSKPEqual
        procedure :: AssignmentOperator   => AssignmentOperatorHSKP
        procedure :: clear                => ClearHSKP
        procedure :: init => InitHSKP
#ifndef __GFORTRAN__
        final     :: FinalizeHSKP
#endif

    end type

    type, extends(persistent) :: ARPES
        type(string)  ::  title
        integer(kind=int32)  ::  num_equivalents =  0

        integer(kind=int32)  ::  num_directions =  0

        integer(kind=int32)  ::  num_values =  0

        integer(kind=int32)  ::  nx =  0

        integer(kind=int32)  ::  ny =  0

        type(array5_real)  ::  Z
        real(kind=dp),pointer :: Z_(:,:,:,:,:)
        type(vector_real)  ::  values
        real(kind=dp),pointer :: values_(:)
        type(HSKP)  ::  hskp


        contains
        procedure :: AllocateObjectFields => AllocateARPESObjectFields
        procedure :: ResetSectionFields   => ResetARPESSectionFields
        procedure :: StoreScalarFields    => StoreARPESScalarFields
        procedure :: StoreObjectFields    => StoreARPESObjectFields
        procedure :: LoadScalarFields     => LoadARPESScalarFields
        procedure :: LoadObjectFields     => LoadARPESObjectFields
        procedure :: DisconnectObjectFields => DisconnectARPESObjectFields
        procedure :: IsEqual              => IsARPESEqual
        procedure :: AssignmentOperator   => AssignmentOperatorARPES
        procedure :: clear                => ClearARPES
        procedure :: init => InitARPES
#ifndef __GFORTRAN__
        final     :: FinalizeARPES
#endif
        procedure :: GetArpesZExtents
        procedure :: GetArpesValuesExtents

    end type

    type, extends(ARPES) :: PathARPES
        logical  ::  is_plane = .False.

        type(vector_real)  ::  omegas
        real(kind=dp),pointer :: omegas_(:)
        type(string)  ::  xlabel
        type(string)  ::  ylabel


        contains
        procedure :: AllocateObjectFields => AllocatePathARPESObjectFields
        procedure :: ResetSectionFields   => ResetPathARPESSectionFields
        procedure :: StoreScalarFields    => StorePathARPESScalarFields
        procedure :: StoreObjectFields    => StorePathARPESObjectFields
        procedure :: LoadScalarFields     => LoadPathARPESScalarFields
        procedure :: LoadObjectFields     => LoadPathARPESObjectFields
        procedure :: DisconnectObjectFields => DisconnectPathARPESObjectFields
        procedure :: IsEqual              => IsPathARPESEqual
        procedure :: AssignmentOperator   => AssignmentOperatorPathARPES
        procedure :: clear                => ClearPathARPES
        procedure :: init => InitPathARPES
#ifndef __GFORTRAN__
        final     :: FinalizePathARPES
#endif
        procedure :: GetPatharpesOmegasExtents

    end type

    type, extends(ARPES) :: PlaneARPES
        logical  ::  is_plane = .False.

        real(kind=dp)  ::  width =  0.0d0

        type(string)  ::  xlabel
        type(string)  ::  ylabel


        contains
        procedure :: AllocateObjectFields => AllocatePlaneARPESObjectFields
        procedure :: ResetSectionFields   => ResetPlaneARPESSectionFields
        procedure :: StoreScalarFields    => StorePlaneARPESScalarFields
        procedure :: StoreObjectFields    => StorePlaneARPESObjectFields
        procedure :: LoadScalarFields     => LoadPlaneARPESScalarFields
        procedure :: LoadObjectFields     => LoadPlaneARPESObjectFields
        procedure :: DisconnectObjectFields => DisconnectPlaneARPESObjectFields
        procedure :: IsEqual              => IsPlaneARPESEqual
        procedure :: AssignmentOperator   => AssignmentOperatorPlaneARPES
        procedure :: clear                => ClearPlaneARPES
        procedure :: init => InitPlaneARPES
#ifndef __GFORTRAN__
        final     :: FinalizePlaneARPES
#endif

    end type

    contains
        subroutine Initlabel(self)
                class(label), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StorelabelObjectFields(self)
                class(label), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadlabelObjectFields(self)
                class(label), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetlabelSectionFields(self)
                class(label), intent(inout) :: self
        end subroutine
        subroutine DisconnectlabelObjectFields(self)
                class(label), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StorelabelScalarFields(self)
                class(label), intent(inout) :: self
                call self%write('text', self%text)
        end subroutine
        subroutine LoadlabelScalarFields(self)
                class(label), intent(inout) :: self
                call self%read('text', self%text)
        end subroutine
        subroutine Finalizelabel(self)
               type(label), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine Clearlabel(self)
                class(label), intent(inout) :: self
                type(label), save :: empty
                self = empty
        end subroutine
        pure elemental function IslabelEqual(lhs, rhs) result(iseq)
                class(label), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (label)
                       iseq = iseq .and. (lhs%text == rhs%text)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorlabel(lhs, rhs)
                class(label), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (label)
                       lhs%text = rhs%text
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocatelabelObjectFields(self)
                class(label), intent(inout) :: self
        end subroutine


        subroutine InitHSKP(self)
                class(HSKP), intent(inout) :: self
                call self%InitPersistent()
                self%npoints =  0
        end subroutine
        subroutine StoreHSKPObjectFields(self)
                class(HSKP), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                call iter%Init(lbound(self%labels), ubound(self%labels))
                do while (.not. iter%Done())
                    call self%labels(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'labels', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadHSKPObjectFields(self)
                class(HSKP), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                allocate(self%labels(int(1):int(self%npoints)))
                call iter%Init(lbound(self%labels), ubound(self%labels))
                do while (.not. iter%Done())
                    call self%labels(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'labels', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetHSKPSectionFields(self)
                class(HSKP), intent(inout) :: self
        end subroutine
        subroutine DisconnectHSKPObjectFields(self)
                class(HSKP), intent(inout) :: self
               type(iterator) :: iter

                call iter%Init(lbound(self%labels), ubound(self%labels))
                do while (.not. iter%Done())
                    call self%labels(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreHSKPScalarFields(self)
                class(HSKP), intent(inout) :: self
                call self%write('npoints', self%npoints)
        end subroutine
        subroutine LoadHSKPScalarFields(self)
                class(HSKP), intent(inout) :: self
                call self%read('npoints', self%npoints)
        end subroutine
        subroutine FinalizeHSKP(self)
               type(HSKP), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearHSKP(self)
                class(HSKP), intent(inout) :: self
                type(HSKP), save :: empty
                self = empty
        end subroutine
        pure elemental function IsHSKPEqual(lhs, rhs) result(iseq)
                class(HSKP), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (HSKP)
                       iseq = iseq .and. (lhs%npoints == rhs%npoints)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%labels) .eqv. allocated(rhs%labels))
                       if (.not. iseq) return
                       if (allocated(lhs%labels)) then
                           iseq = iseq .and. all(shape(lhs%labels) == shape(rhs%labels))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%labels(:) == rhs%labels(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorHSKP(lhs, rhs)
                class(HSKP), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (HSKP)
                       lhs%npoints = rhs%npoints
                       lhs%labels = rhs%labels
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateHSKPObjectFields(self)
                class(HSKP), intent(inout) :: self
                allocate(self%labels(int(1):int(self%npoints)))
        end subroutine


        subroutine InitARPES(self)
                class(ARPES), intent(inout) :: self
                call self%InitPersistent()
                self%num_equivalents =  0
                self%num_directions =  0
                self%num_values =  0
                self%nx =  0
                self%ny =  0
                call self%hskp%InitPersistent()
        end subroutine
        subroutine StoreARPESObjectFields(self)
                class(ARPES), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Z%StoreObject(ps, gid,  'Z')
                call self%values%StoreObject(ps, gid,  'values')
                call self%hskp%store(ps%FetchSubGroup(gid,  'hskp'))
        end subroutine
        subroutine LoadARPESObjectFields(self)
                class(ARPES), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Z%LoadObject(ps, gid,  'Z')
                call self%values%LoadObject(ps, gid,  'values')
                call self%hskp%load(ps%FetchSubGroup(gid,  'hskp'))
        end subroutine
        subroutine ResetARPESSectionFields(self)
                class(ARPES), intent(inout) :: self
                self%Z_ => self%Z%GetWithExtents(self%GetARPESZExtents())
                self%values_ => self%values%GetWithExtents(self%GetARPESvaluesExtents())
        end subroutine
        subroutine DisconnectARPESObjectFields(self)
                class(ARPES), intent(inout) :: self
               type(iterator) :: iter
                call self%Z%DisconnectFromStore()
                call self%values%DisconnectFromStore()
                call self%hskp%disconnect()
        end subroutine
        subroutine StoreARPESScalarFields(self)
                class(ARPES), intent(inout) :: self
                call self%write('title', self%title)
                call self%write('num_equivalents', self%num_equivalents)
                call self%write('num_directions', self%num_directions)
                call self%write('num_values', self%num_values)
                call self%write('nx', self%nx)
                call self%write('ny', self%ny)
        end subroutine
        subroutine LoadARPESScalarFields(self)
                class(ARPES), intent(inout) :: self
                call self%read('title', self%title)
                call self%read('num_equivalents', self%num_equivalents)
                call self%read('num_directions', self%num_directions)
                call self%read('num_values', self%num_values)
                call self%read('nx', self%nx)
                call self%read('ny', self%ny)
        end subroutine
        subroutine FinalizeARPES(self)
               type(ARPES), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearARPES(self)
                class(ARPES), intent(inout) :: self
                type(ARPES), save :: empty
                self = empty
        end subroutine
        pure elemental function IsARPESEqual(lhs, rhs) result(iseq)
                class(ARPES), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (ARPES)
                       iseq = iseq .and. (lhs%title == rhs%title)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_equivalents == rhs%num_equivalents)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_directions == rhs%num_directions)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_values == rhs%num_values)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nx == rhs%nx)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ny == rhs%ny)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Z == rhs%Z)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%values == rhs%values)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hskp == rhs%hskp)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorARPES(lhs, rhs)
                class(ARPES), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (ARPES)
                       lhs%title = rhs%title
                       lhs%num_equivalents = rhs%num_equivalents
                       lhs%num_directions = rhs%num_directions
                       lhs%num_values = rhs%num_values
                       lhs%nx = rhs%nx
                       lhs%ny = rhs%ny
                       lhs%Z = rhs%Z
                       lhs%values = rhs%values
                       lhs%hskp = rhs%hskp
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetArpesZExtents(self) result(res)
                class(ARPES), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(1,self%num_equivalents),extent(1,self%num_directions),extent(1,self%num_values),extent(1,self%nx),extent(1,self%ny)]
        end function
        function GetArpesValuesExtents(self) result(res)
                class(ARPES), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_values)]
        end function
        subroutine AllocateARPESObjectFields(self)
                class(ARPES), intent(inout) :: self
                call self%Z%init(int(self%num_equivalents),int(self%num_directions),int(self%num_values),int(self%nx),int(self%ny))
                call self%values%init(int(self%num_values))
        end subroutine


        subroutine InitPathARPES(self)
                class(PathARPES), intent(inout) :: self
                call self%ARPES%Init()
                self%is_plane = .False.
                self%xlabel = "k"
                self%ylabel = "\omega"
        end subroutine
        subroutine StorePathARPESObjectFields(self)
                class(PathARPES), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ARPES%StoreObjectFields()
                call self%omegas%StoreObject(ps, gid,  'omegas')
        end subroutine
        subroutine LoadPathARPESObjectFields(self)
                class(PathARPES), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ARPES%LoadObjectFields()
                call self%omegas%LoadObject(ps, gid,  'omegas')
        end subroutine
        subroutine ResetPathARPESSectionFields(self)
                class(PathARPES), intent(inout) :: self
                call self%ARPES%ResetSectionFields()
                self%omegas_ => self%omegas%GetWithExtents(self%GetPathARPESomegasExtents())
        end subroutine
        subroutine DisconnectPathARPESObjectFields(self)
                class(PathARPES), intent(inout) :: self
               type(iterator) :: iter
                call self%ARPES%DisconnectObjectFields()
                call self%omegas%DisconnectFromStore()
        end subroutine
        subroutine StorePathARPESScalarFields(self)
                class(PathARPES), intent(inout) :: self
                call self%ARPES%StoreScalarFields()
                call self%write('is_plane', self%is_plane)
                call self%write('xlabel', self%xlabel)
                call self%write('ylabel', self%ylabel)
        end subroutine
        subroutine LoadPathARPESScalarFields(self)
                class(PathARPES), intent(inout) :: self
                call self%ARPES%LoadScalarFields()
                call self%read('is_plane', self%is_plane)
                call self%read('xlabel', self%xlabel)
                call self%read('ylabel', self%ylabel)
        end subroutine
        subroutine FinalizePathARPES(self)
               type(PathARPES), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearPathARPES(self)
                class(PathARPES), intent(inout) :: self
                type(PathARPES), save :: empty
                self = empty
        end subroutine
        pure elemental function IsPathARPESEqual(lhs, rhs) result(iseq)
                class(PathARPES), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (PathARPES)
                       iseq = iseq .and. (lhs%ARPES == rhs%ARPES)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%is_plane .eqv. rhs%is_plane)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omegas == rhs%omegas)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%xlabel == rhs%xlabel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ylabel == rhs%ylabel)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorPathARPES(lhs, rhs)
                class(PathARPES), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (PathARPES)
                       lhs%ARPES = rhs%ARPES
                       lhs%is_plane = rhs%is_plane
                       lhs%omegas = rhs%omegas
                       lhs%xlabel = rhs%xlabel
                       lhs%ylabel = rhs%ylabel
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetPatharpesOmegasExtents(self) result(res)
                class(PathARPES), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%ny)]
        end function
        subroutine AllocatePathARPESObjectFields(self)
                class(PathARPES), intent(inout) :: self
                call self%ARPES%AllocateObjectFields()
                call self%omegas%init(int(self%ny))
        end subroutine


        subroutine InitPlaneARPES(self)
                class(PlaneARPES), intent(inout) :: self
                call self%ARPES%Init()
                self%is_plane = .False.
                self%width =  0.0d0
        end subroutine
        subroutine StorePlaneARPESObjectFields(self)
                class(PlaneARPES), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ARPES%StoreObjectFields()
        end subroutine
        subroutine LoadPlaneARPESObjectFields(self)
                class(PlaneARPES), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%ARPES%LoadObjectFields()
        end subroutine
        subroutine ResetPlaneARPESSectionFields(self)
                class(PlaneARPES), intent(inout) :: self
                call self%ARPES%ResetSectionFields()
        end subroutine
        subroutine DisconnectPlaneARPESObjectFields(self)
                class(PlaneARPES), intent(inout) :: self
               type(iterator) :: iter
                call self%ARPES%DisconnectObjectFields()
        end subroutine
        subroutine StorePlaneARPESScalarFields(self)
                class(PlaneARPES), intent(inout) :: self
                call self%ARPES%StoreScalarFields()
                call self%write('is_plane', self%is_plane)
                call self%write('width', self%width)
                call self%write('xlabel', self%xlabel)
                call self%write('ylabel', self%ylabel)
        end subroutine
        subroutine LoadPlaneARPESScalarFields(self)
                class(PlaneARPES), intent(inout) :: self
                call self%ARPES%LoadScalarFields()
                call self%read('is_plane', self%is_plane)
                call self%read('width', self%width)
                call self%read('xlabel', self%xlabel)
                call self%read('ylabel', self%ylabel)
        end subroutine
        subroutine FinalizePlaneARPES(self)
               type(PlaneARPES), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearPlaneARPES(self)
                class(PlaneARPES), intent(inout) :: self
                type(PlaneARPES), save :: empty
                self = empty
        end subroutine
        pure elemental function IsPlaneARPESEqual(lhs, rhs) result(iseq)
                class(PlaneARPES), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (PlaneARPES)
                       iseq = iseq .and. (lhs%ARPES == rhs%ARPES)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%is_plane .eqv. rhs%is_plane)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%width == rhs%width)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%xlabel == rhs%xlabel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ylabel == rhs%ylabel)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorPlaneARPES(lhs, rhs)
                class(PlaneARPES), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (PlaneARPES)
                       lhs%ARPES = rhs%ARPES
                       lhs%is_plane = rhs%is_plane
                       lhs%width = rhs%width
                       lhs%xlabel = rhs%xlabel
                       lhs%ylabel = rhs%ylabel
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocatePlaneARPESObjectFields(self)
                class(PlaneARPES), intent(inout) :: self
                call self%ARPES%AllocateObjectFields()
        end subroutine



end module
