
module FlapwMBPT_basis
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use array4
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: MuffinTin
        integer(kind=int32)  ::  num_muffin_orbs =  0

        integer(kind=int32)  ::  num_radial_functions =  0

        integer(kind=int32)  ::  nrad =  0

        type(vector_int)  ::  lapw2self
        integer(kind=int32),pointer :: lapw2self_(:)
        type(matrix_real)  ::  radial_functions
        real(kind=dp),pointer :: radial_functions_(:,:)
        type(matrix_real)  ::  radial_functions2
        real(kind=dp),pointer :: radial_functions2_(:,:)
        type(vector_real)  ::  radial_mesh
        real(kind=dp),pointer :: radial_mesh_(:)
        type(vector_real)  ::  dr_mesh
        real(kind=dp),pointer :: dr_mesh_(:)


        contains
        procedure :: AllocateObjectFields => AllocateMuffinTinObjectFields
        procedure :: ResetSectionFields   => ResetMuffinTinSectionFields
        procedure :: StoreScalarFields    => StoreMuffinTinScalarFields
        procedure :: StoreObjectFields    => StoreMuffinTinObjectFields
        procedure :: LoadScalarFields     => LoadMuffinTinScalarFields
        procedure :: LoadObjectFields     => LoadMuffinTinObjectFields
        procedure :: DisconnectObjectFields => DisconnectMuffinTinObjectFields
        procedure :: IsEqual              => IsMuffinTinEqual
        procedure :: AssignmentOperator   => AssignmentOperatorMuffinTin
        procedure :: clear                => ClearMuffinTin
        procedure :: init => InitMuffinTin
#ifndef __GFORTRAN__
        final     :: FinalizeMuffinTin
#endif
        procedure :: GetMuffintinLapw2selfExtents
        procedure :: GetMuffintinRadial_functionsExtents
        procedure :: GetMuffintinRadial_functions2Extents
        procedure :: GetMuffintinRadial_meshExtents
        procedure :: GetMuffintinDr_meshExtents

    end type

    type, extends(persistent) :: MuffinTinBasis
        integer(kind=int32)  ::  nrel =  0

        integer(kind=int32)  ::  nsort =  0

        integer(kind=int32)  ::  natom =  0

        integer(kind=int32)  ::  nops =  0

        type(matrix_int)  ::  atom_to_equiv_under_op
        integer(kind=int32),pointer :: atom_to_equiv_under_op_(:,:)
        type(MuffinTin), allocatable  ::  tins(:)


        contains
        procedure :: AllocateObjectFields => AllocateMuffinTinBasisObjectFields
        procedure :: ResetSectionFields   => ResetMuffinTinBasisSectionFields
        procedure :: StoreScalarFields    => StoreMuffinTinBasisScalarFields
        procedure :: StoreObjectFields    => StoreMuffinTinBasisObjectFields
        procedure :: LoadScalarFields     => LoadMuffinTinBasisScalarFields
        procedure :: LoadObjectFields     => LoadMuffinTinBasisObjectFields
        procedure :: DisconnectObjectFields => DisconnectMuffinTinBasisObjectFields
        procedure :: IsEqual              => IsMuffinTinBasisEqual
        procedure :: AssignmentOperator   => AssignmentOperatorMuffinTinBasis
        procedure :: clear                => ClearMuffinTinBasis
        procedure :: init => InitMuffinTinBasis
#ifndef __GFORTRAN__
        final     :: FinalizeMuffinTinBasis
#endif
        procedure :: GetMuffintinbasisAtom_to_equiv_under_opExtents

    end type
    interface
         function GetMtBasisI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    type, extends(persistent) :: InterstitialBasis
        integer(kind=int32)  ::  max_num_pw =  0

        integer(kind=int32)  ::  max_g_basis =  0

        integer(kind=int32)  ::  nrel =  0

        integer(kind=int32)  ::  num_expanded =  0

        integer(kind=int32)  ::  num_k_all =  0

        integer(kind=int32)  ::  num_k =  0

        integer(kind=int32)  ::  num_si =  0

        type(matrix_real)  ::  reciprical_lattice_vectors
        real(kind=dp),pointer :: reciprical_lattice_vectors_(:,:)
        type(matrix_int)  ::  vector_index
        integer(kind=int32),pointer :: vector_index_(:,:)
        type(vector_int)  ::  num_vectors_at_k
        integer(kind=int32),pointer :: num_vectors_at_k_(:)
        type(array4_complex)  ::  A
        complex(kind=dp),pointer :: A_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateInterstitialBasisObjectFields
        procedure :: ResetSectionFields   => ResetInterstitialBasisSectionFields
        procedure :: StoreScalarFields    => StoreInterstitialBasisScalarFields
        procedure :: StoreObjectFields    => StoreInterstitialBasisObjectFields
        procedure :: LoadScalarFields     => LoadInterstitialBasisScalarFields
        procedure :: LoadObjectFields     => LoadInterstitialBasisObjectFields
        procedure :: DisconnectObjectFields => DisconnectInterstitialBasisObjectFields
        procedure :: IsEqual              => IsInterstitialBasisEqual
        procedure :: AssignmentOperator   => AssignmentOperatorInterstitialBasis
        procedure :: clear                => ClearInterstitialBasis
        procedure :: init => InitInterstitialBasis
#ifndef __GFORTRAN__
        final     :: FinalizeInterstitialBasis
#endif
        procedure :: GetInterstitialbasisReciprical_lattice_vectorsExtents
        procedure :: GetInterstitialbasisVector_indexExtents
        procedure :: GetInterstitialbasisNum_vectors_at_kExtents
        procedure :: GetInterstitialbasisAExtents

    end type
    interface
         function GetInterstitialBasisI(selfpath, min_band, max_band) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
         end function
    end interface


    contains
        subroutine InitMuffinTin(self)
                class(MuffinTin), intent(inout) :: self
                call self%InitPersistent()
                self%num_muffin_orbs =  0
                self%num_radial_functions =  0
                self%nrad =  0
        end subroutine
        subroutine StoreMuffinTinObjectFields(self)
                class(MuffinTin), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%lapw2self%StoreObject(ps, gid,  'lapw2self')
                call self%radial_functions%StoreObject(ps, gid,  'radial_functions')
                call self%radial_functions2%StoreObject(ps, gid,  'radial_functions2')
                call self%radial_mesh%StoreObject(ps, gid,  'radial_mesh')
                call self%dr_mesh%StoreObject(ps, gid,  'dr_mesh')
        end subroutine
        subroutine LoadMuffinTinObjectFields(self)
                class(MuffinTin), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%lapw2self%LoadObject(ps, gid,  'lapw2self')
                call self%radial_functions%LoadObject(ps, gid,  'radial_functions')
                call self%radial_functions2%LoadObject(ps, gid,  'radial_functions2')
                call self%radial_mesh%LoadObject(ps, gid,  'radial_mesh')
                call self%dr_mesh%LoadObject(ps, gid,  'dr_mesh')
        end subroutine
        subroutine ResetMuffinTinSectionFields(self)
                class(MuffinTin), intent(inout) :: self
                self%lapw2self_ => self%lapw2self%GetWithExtents(self%GetMuffinTinlapw2selfExtents())
                self%radial_functions_ => self%radial_functions%GetWithExtents(self%GetMuffinTinradial_functionsExtents())
                self%radial_functions2_ => self%radial_functions2%GetWithExtents(self%GetMuffinTinradial_functions2Extents())
                self%radial_mesh_ => self%radial_mesh%GetWithExtents(self%GetMuffinTinradial_meshExtents())
                self%dr_mesh_ => self%dr_mesh%GetWithExtents(self%GetMuffinTindr_meshExtents())
        end subroutine
        subroutine DisconnectMuffinTinObjectFields(self)
                class(MuffinTin), intent(inout) :: self
               type(iterator) :: iter
                call self%lapw2self%DisconnectFromStore()
                call self%radial_functions%DisconnectFromStore()
                call self%radial_functions2%DisconnectFromStore()
                call self%radial_mesh%DisconnectFromStore()
                call self%dr_mesh%DisconnectFromStore()
        end subroutine
        subroutine StoreMuffinTinScalarFields(self)
                class(MuffinTin), intent(inout) :: self
                call self%write('num_muffin_orbs', self%num_muffin_orbs)
                call self%write('num_radial_functions', self%num_radial_functions)
                call self%write('nrad', self%nrad)
        end subroutine
        subroutine LoadMuffinTinScalarFields(self)
                class(MuffinTin), intent(inout) :: self
                call self%read('num_muffin_orbs', self%num_muffin_orbs)
                call self%read('num_radial_functions', self%num_radial_functions)
                call self%read('nrad', self%nrad)
        end subroutine
        subroutine FinalizeMuffinTin(self)
               type(MuffinTin), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearMuffinTin(self)
                class(MuffinTin), intent(inout) :: self
                type(MuffinTin), save :: empty
                self = empty
        end subroutine
        pure elemental function IsMuffinTinEqual(lhs, rhs) result(iseq)
                class(MuffinTin), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (MuffinTin)
                       iseq = iseq .and. (lhs%num_muffin_orbs == rhs%num_muffin_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_radial_functions == rhs%num_radial_functions)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrad == rhs%nrad)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%lapw2self == rhs%lapw2self)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%radial_functions == rhs%radial_functions)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%radial_functions2 == rhs%radial_functions2)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%radial_mesh == rhs%radial_mesh)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dr_mesh == rhs%dr_mesh)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorMuffinTin(lhs, rhs)
                class(MuffinTin), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (MuffinTin)
                       lhs%num_muffin_orbs = rhs%num_muffin_orbs
                       lhs%num_radial_functions = rhs%num_radial_functions
                       lhs%nrad = rhs%nrad
                       lhs%lapw2self = rhs%lapw2self
                       lhs%radial_functions = rhs%radial_functions
                       lhs%radial_functions2 = rhs%radial_functions2
                       lhs%radial_mesh = rhs%radial_mesh
                       lhs%dr_mesh = rhs%dr_mesh
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetMuffintinLapw2selfExtents(self) result(res)
                class(MuffinTin), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_muffin_orbs)]
        end function
        function GetMuffintinRadial_functionsExtents(self) result(res)
                class(MuffinTin), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_radial_functions),extent(1,self%nrad)]
        end function
        function GetMuffintinRadial_functions2Extents(self) result(res)
                class(MuffinTin), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_radial_functions),extent(1,self%nrad)]
        end function
        function GetMuffintinRadial_meshExtents(self) result(res)
                class(MuffinTin), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%nrad)]
        end function
        function GetMuffintinDr_meshExtents(self) result(res)
                class(MuffinTin), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%nrad)]
        end function
        subroutine AllocateMuffinTinObjectFields(self)
                class(MuffinTin), intent(inout) :: self
                call self%lapw2self%init(int(self%num_muffin_orbs))
                call self%radial_functions%init(int(self%num_radial_functions),int(self%nrad))
                call self%radial_functions2%init(int(self%num_radial_functions),int(self%nrad))
                call self%radial_mesh%init(int(self%nrad))
                call self%dr_mesh%init(int(self%nrad))
        end subroutine


        subroutine InitMuffinTinBasis(self)
                class(MuffinTinBasis), intent(inout) :: self
                call self%InitPersistent()
                self%nrel =  0
                self%nsort =  0
                self%natom =  0
                self%nops =  0
        end subroutine
        subroutine StoreMuffinTinBasisObjectFields(self)
                class(MuffinTinBasis), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%atom_to_equiv_under_op%StoreObject(ps, gid,  'atom_to_equiv_under_op')

                call iter%Init(lbound(self%tins), ubound(self%tins))
                do while (.not. iter%Done())
                    call self%tins(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'tins', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadMuffinTinBasisObjectFields(self)
                class(MuffinTinBasis), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%atom_to_equiv_under_op%LoadObject(ps, gid,  'atom_to_equiv_under_op')

                allocate(self%tins(int(1):int(self%nsort)))
                call iter%Init(lbound(self%tins), ubound(self%tins))
                do while (.not. iter%Done())
                    call self%tins(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'tins', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetMuffinTinBasisSectionFields(self)
                class(MuffinTinBasis), intent(inout) :: self
                self%atom_to_equiv_under_op_ => self%atom_to_equiv_under_op%GetWithExtents(self%GetMuffinTinBasisatom_to_equiv_under_opExtents())
        end subroutine
        subroutine DisconnectMuffinTinBasisObjectFields(self)
                class(MuffinTinBasis), intent(inout) :: self
               type(iterator) :: iter
                call self%atom_to_equiv_under_op%DisconnectFromStore()

                call iter%Init(lbound(self%tins), ubound(self%tins))
                do while (.not. iter%Done())
                    call self%tins(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreMuffinTinBasisScalarFields(self)
                class(MuffinTinBasis), intent(inout) :: self
                call self%write('nrel', self%nrel)
                call self%write('nsort', self%nsort)
                call self%write('natom', self%natom)
                call self%write('nops', self%nops)
        end subroutine
        subroutine LoadMuffinTinBasisScalarFields(self)
                class(MuffinTinBasis), intent(inout) :: self
                call self%read('nrel', self%nrel)
                call self%read('nsort', self%nsort)
                call self%read('natom', self%natom)
                call self%read('nops', self%nops)
        end subroutine
        subroutine FinalizeMuffinTinBasis(self)
               type(MuffinTinBasis), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearMuffinTinBasis(self)
                class(MuffinTinBasis), intent(inout) :: self
                type(MuffinTinBasis), save :: empty
                self = empty
        end subroutine
        pure elemental function IsMuffinTinBasisEqual(lhs, rhs) result(iseq)
                class(MuffinTinBasis), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (MuffinTinBasis)
                       iseq = iseq .and. (lhs%nrel == rhs%nrel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nsort == rhs%nsort)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%natom == rhs%natom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nops == rhs%nops)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%atom_to_equiv_under_op == rhs%atom_to_equiv_under_op)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%tins) .eqv. allocated(rhs%tins))
                       if (.not. iseq) return
                       if (allocated(lhs%tins)) then
                           iseq = iseq .and. all(shape(lhs%tins) == shape(rhs%tins))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%tins(:) == rhs%tins(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorMuffinTinBasis(lhs, rhs)
                class(MuffinTinBasis), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (MuffinTinBasis)
                       lhs%nrel = rhs%nrel
                       lhs%nsort = rhs%nsort
                       lhs%natom = rhs%natom
                       lhs%nops = rhs%nops
                       lhs%atom_to_equiv_under_op = rhs%atom_to_equiv_under_op
                       lhs%tins = rhs%tins
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetMuffintinbasisAtom_to_equiv_under_opExtents(self) result(res)
                class(MuffinTinBasis), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%natom),extent(1,self%nops)]
        end function
        subroutine AllocateMuffinTinBasisObjectFields(self)
                class(MuffinTinBasis), intent(inout) :: self
                call self%atom_to_equiv_under_op%init(int(self%natom),int(self%nops))
                allocate(self%tins(int(1):int(self%nsort)))
        end subroutine


        subroutine GetMtBasisWrapper(ret, imp_fp, selfpath) bind(C,name='flapwmbpt_basis_mp_getmtbasiswrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(GetMtBasisI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine InitInterstitialBasis(self)
                class(InterstitialBasis), intent(inout) :: self
                call self%InitPersistent()
                self%max_num_pw =  0
                self%max_g_basis =  0
                self%nrel =  0
                self%num_expanded =  0
                self%num_k_all =  0
                self%num_k =  0
                self%num_si =  0
        end subroutine
        subroutine StoreInterstitialBasisObjectFields(self)
                class(InterstitialBasis), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%reciprical_lattice_vectors%StoreObject(ps, gid,  'reciprical_lattice_vectors')
                call self%vector_index%StoreObject(ps, gid,  'vector_index')
                call self%num_vectors_at_k%StoreObject(ps, gid,  'num_vectors_at_k')
                call self%A%StoreObject(ps, gid,  'A')
        end subroutine
        subroutine LoadInterstitialBasisObjectFields(self)
                class(InterstitialBasis), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%reciprical_lattice_vectors%LoadObject(ps, gid,  'reciprical_lattice_vectors')
                call self%vector_index%LoadObject(ps, gid,  'vector_index')
                call self%num_vectors_at_k%LoadObject(ps, gid,  'num_vectors_at_k')
                call self%A%LoadObject(ps, gid,  'A')
        end subroutine
        subroutine ResetInterstitialBasisSectionFields(self)
                class(InterstitialBasis), intent(inout) :: self
                self%reciprical_lattice_vectors_ => self%reciprical_lattice_vectors%GetWithExtents(self%GetInterstitialBasisreciprical_lattice_vectorsExtents())
                self%vector_index_ => self%vector_index%GetWithExtents(self%GetInterstitialBasisvector_indexExtents())
                self%num_vectors_at_k_ => self%num_vectors_at_k%GetWithExtents(self%GetInterstitialBasisnum_vectors_at_kExtents())
                self%A_ => self%A%GetWithExtents(self%GetInterstitialBasisAExtents())
        end subroutine
        subroutine DisconnectInterstitialBasisObjectFields(self)
                class(InterstitialBasis), intent(inout) :: self
               type(iterator) :: iter
                call self%reciprical_lattice_vectors%DisconnectFromStore()
                call self%vector_index%DisconnectFromStore()
                call self%num_vectors_at_k%DisconnectFromStore()
                call self%A%DisconnectFromStore()
        end subroutine
        subroutine StoreInterstitialBasisScalarFields(self)
                class(InterstitialBasis), intent(inout) :: self
                call self%write('max_num_pw', self%max_num_pw)
                call self%write('max_g_basis', self%max_g_basis)
                call self%write('nrel', self%nrel)
                call self%write('num_expanded', self%num_expanded)
                call self%write('num_k_all', self%num_k_all)
                call self%write('num_k', self%num_k)
                call self%write('num_si', self%num_si)
        end subroutine
        subroutine LoadInterstitialBasisScalarFields(self)
                class(InterstitialBasis), intent(inout) :: self
                call self%read('max_num_pw', self%max_num_pw)
                call self%read('max_g_basis', self%max_g_basis)
                call self%read('nrel', self%nrel)
                call self%read('num_expanded', self%num_expanded)
                call self%read('num_k_all', self%num_k_all)
                call self%read('num_k', self%num_k)
                call self%read('num_si', self%num_si)
        end subroutine
        subroutine FinalizeInterstitialBasis(self)
               type(InterstitialBasis), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearInterstitialBasis(self)
                class(InterstitialBasis), intent(inout) :: self
                type(InterstitialBasis), save :: empty
                self = empty
        end subroutine
        pure elemental function IsInterstitialBasisEqual(lhs, rhs) result(iseq)
                class(InterstitialBasis), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (InterstitialBasis)
                       iseq = iseq .and. (lhs%max_num_pw == rhs%max_num_pw)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_g_basis == rhs%max_g_basis)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrel == rhs%nrel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_expanded == rhs%num_expanded)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_all == rhs%num_k_all)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%reciprical_lattice_vectors == rhs%reciprical_lattice_vectors)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%vector_index == rhs%vector_index)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_vectors_at_k == rhs%num_vectors_at_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%A == rhs%A)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorInterstitialBasis(lhs, rhs)
                class(InterstitialBasis), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (InterstitialBasis)
                       lhs%max_num_pw = rhs%max_num_pw
                       lhs%max_g_basis = rhs%max_g_basis
                       lhs%nrel = rhs%nrel
                       lhs%num_expanded = rhs%num_expanded
                       lhs%num_k_all = rhs%num_k_all
                       lhs%num_k = rhs%num_k
                       lhs%num_si = rhs%num_si
                       lhs%reciprical_lattice_vectors = rhs%reciprical_lattice_vectors
                       lhs%vector_index = rhs%vector_index
                       lhs%num_vectors_at_k = rhs%num_vectors_at_k
                       lhs%A = rhs%A
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetInterstitialbasisReciprical_lattice_vectorsExtents(self) result(res)
                class(InterstitialBasis), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%max_g_basis)]
        end function
        function GetInterstitialbasisVector_indexExtents(self) result(res)
                class(InterstitialBasis), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%max_num_pw),extent(1,self%num_k_all)]
        end function
        function GetInterstitialbasisNum_vectors_at_kExtents(self) result(res)
                class(InterstitialBasis), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_k)]
        end function
        function GetInterstitialbasisAExtents(self) result(res)
                class(InterstitialBasis), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%max_num_pw * self%nrel),extent(1,self%num_expanded),extent(1,self%num_k),extent(1,self%num_si)]
        end function
        subroutine AllocateInterstitialBasisObjectFields(self)
                class(InterstitialBasis), intent(inout) :: self
                call self%reciprical_lattice_vectors%init(int(3),int(self%max_g_basis))
                call self%vector_index%init(int(self%max_num_pw),int(self%num_k_all))
                call self%num_vectors_at_k%init(int(self%num_k))
                call self%A%init(int(self%max_num_pw * self%nrel),int(self%num_expanded),int(self%num_k),int(self%num_si))
        end subroutine


        subroutine GetInterstitialBasisWrapper(ret, imp_fp, selfpath, min_band, max_band) bind(C,name='flapwmbpt_basis_mp_getinterstitialbasiswrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                integer(kind=c_int),intent(in),value :: min_band
                integer(kind=c_int),intent(in),value :: max_band
            procedure(GetInterstitialBasisI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), min_band, max_band)
            ret(1)= NewCStr(res%chars())
        end subroutine


end module
