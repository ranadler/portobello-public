
module atomic_orbitals
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use LAPW
    use array4
    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: OrbitalLabels
        type(string)  ::  short_label
        type(string)  ::  latex_label


        contains
        procedure :: AllocateObjectFields => AllocateOrbitalLabelsObjectFields
        procedure :: ResetSectionFields   => ResetOrbitalLabelsSectionFields
        procedure :: StoreScalarFields    => StoreOrbitalLabelsScalarFields
        procedure :: StoreObjectFields    => StoreOrbitalLabelsObjectFields
        procedure :: LoadScalarFields     => LoadOrbitalLabelsScalarFields
        procedure :: LoadObjectFields     => LoadOrbitalLabelsObjectFields
        procedure :: DisconnectObjectFields => DisconnectOrbitalLabelsObjectFields
        procedure :: IsEqual              => IsOrbitalLabelsEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOrbitalLabels
        procedure :: clear                => ClearOrbitalLabels
        procedure :: init => InitOrbitalLabels
#ifndef __GFORTRAN__
        final     :: FinalizeOrbitalLabels
#endif

    end type

    type, extends(persistent) :: AtomicShell
        integer(kind=int32)  ::  atom_number =  0

        integer(kind=int32)  ::  l =  0

        integer(kind=int32)  ::  lapw_kind =  0

        integer(kind=int32)  ::  num_equivalents =  0

        integer(kind=int32)  ::  num_anti_equivalents =  0

        integer(kind=int32)  ::  cluster_size =  int(1,kind=int32)

        logical  ::  is_correlated = .False.

        integer(kind=int32)  ::  nrel =  int(1,kind=int32)

        integer(kind=int32)  ::  dim =  0

        integer(kind=int32)  ::  start_index =  int(0,kind=int32)

        integer(kind=int32)  ::  end_index =  int(0,kind=int32)

        type(matrix_int)  ::  raw_rep
        integer(kind=int32),pointer :: raw_rep_(:,:)
        type(matrix_int)  ::  rep
        integer(kind=int32),pointer :: rep_(:,:)
        type(matrix_complex)  ::  basis
        complex(kind=dp),pointer :: basis_(:,:)
        type(OrbitalLabels), allocatable  ::  labels(:)
        type(vector_int)  ::  lapw_indices
        integer(kind=int32),pointer :: lapw_indices_(:)
        type(vector_int)  ::  det_signs
        integer(kind=int32),pointer :: det_signs_(:)
        type(cube_real)  ::  rotations
        real(kind=dp),pointer :: rotations_(:,:,:)
        type(matrix_real)  ::  translations
        real(kind=dp),pointer :: translations_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateAtomicShellObjectFields
        procedure :: ResetSectionFields   => ResetAtomicShellSectionFields
        procedure :: StoreScalarFields    => StoreAtomicShellScalarFields
        procedure :: StoreObjectFields    => StoreAtomicShellObjectFields
        procedure :: LoadScalarFields     => LoadAtomicShellScalarFields
        procedure :: LoadObjectFields     => LoadAtomicShellObjectFields
        procedure :: DisconnectObjectFields => DisconnectAtomicShellObjectFields
        procedure :: IsEqual              => IsAtomicShellEqual
        procedure :: AssignmentOperator   => AssignmentOperatorAtomicShell
        procedure :: clear                => ClearAtomicShell
        procedure :: init => InitAtomicShell
#ifndef __GFORTRAN__
        final     :: FinalizeAtomicShell
#endif
        procedure :: GetAtomicshellRaw_repExtents
        procedure :: GetAtomicshellRepExtents
        procedure :: GetAtomicshellBasisExtents
        procedure :: GetAtomicshellLapw_indicesExtents
        procedure :: GetAtomicshellDet_signsExtents
        procedure :: GetAtomicshellRotationsExtents
        procedure :: GetAtomicshellTranslationsExtents

    end type

    type, extends(persistent) :: SelectedOrbitals
        integer(kind=int32)  ::  num_muffin_orbs =  0

        integer(kind=int32)  ::  num_shells =  0

        integer(kind=int32)  ::  total_dim =  0

        type(string)  ::  subspace_expr
        real(kind=dp)  ::  exclusion_margin =  real(2.000000,kind=16)

        type(matrix_complex)  ::  V
        complex(kind=dp),pointer :: V_(:,:)
        type(AtomicShell), allocatable  ::  shells(:)


        contains
        procedure :: AllocateObjectFields => AllocateSelectedOrbitalsObjectFields
        procedure :: ResetSectionFields   => ResetSelectedOrbitalsSectionFields
        procedure :: StoreScalarFields    => StoreSelectedOrbitalsScalarFields
        procedure :: StoreObjectFields    => StoreSelectedOrbitalsObjectFields
        procedure :: LoadScalarFields     => LoadSelectedOrbitalsScalarFields
        procedure :: LoadObjectFields     => LoadSelectedOrbitalsObjectFields
        procedure :: DisconnectObjectFields => DisconnectSelectedOrbitalsObjectFields
        procedure :: IsEqual              => IsSelectedOrbitalsEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSelectedOrbitals
        procedure :: clear                => ClearSelectedOrbitals
        procedure :: init => InitSelectedOrbitals
#ifndef __GFORTRAN__
        final     :: FinalizeSelectedOrbitals
#endif
        procedure :: GetSelectedorbitalsVExtents

    end type

    type, extends(persistent) :: OrbitalsBandProjector
        integer(kind=int32)  ::  num_k_all =  0

        integer(kind=int32)  ::  num_orbs =  0

        integer(kind=int32)  ::  min_band =  0

        integer(kind=int32)  ::  max_band =  0

        integer(kind=int32)  ::  num_si =  int(1,kind=int32)

        type(array4_complex)  ::  P
        complex(kind=dp),pointer :: P_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateOrbitalsBandProjectorObjectFields
        procedure :: ResetSectionFields   => ResetOrbitalsBandProjectorSectionFields
        procedure :: StoreScalarFields    => StoreOrbitalsBandProjectorScalarFields
        procedure :: StoreObjectFields    => StoreOrbitalsBandProjectorObjectFields
        procedure :: LoadScalarFields     => LoadOrbitalsBandProjectorScalarFields
        procedure :: LoadObjectFields     => LoadOrbitalsBandProjectorObjectFields
        procedure :: DisconnectObjectFields => DisconnectOrbitalsBandProjectorObjectFields
        procedure :: IsEqual              => IsOrbitalsBandProjectorEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOrbitalsBandProjector
        procedure :: clear                => ClearOrbitalsBandProjector
        procedure :: init => InitOrbitalsBandProjector
#ifndef __GFORTRAN__
        final     :: FinalizeOrbitalsBandProjector
#endif
        procedure :: GetOrbitalsbandprojectorPExtents

    end type

    type, extends(Expansion) :: OrbitalMTProjector


        contains
        procedure :: AllocateObjectFields => AllocateOrbitalMTProjectorObjectFields
        procedure :: ResetSectionFields   => ResetOrbitalMTProjectorSectionFields
        procedure :: StoreScalarFields    => StoreOrbitalMTProjectorScalarFields
        procedure :: StoreObjectFields    => StoreOrbitalMTProjectorObjectFields
        procedure :: LoadScalarFields     => LoadOrbitalMTProjectorScalarFields
        procedure :: LoadObjectFields     => LoadOrbitalMTProjectorObjectFields
        procedure :: DisconnectObjectFields => DisconnectOrbitalMTProjectorObjectFields
        procedure :: IsEqual              => IsOrbitalMTProjectorEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOrbitalMTProjector
        procedure :: clear                => ClearOrbitalMTProjector
        procedure :: init => InitOrbitalMTProjector
#ifndef __GFORTRAN__
        final     :: FinalizeOrbitalMTProjector
#endif

    end type

    contains
        subroutine InitOrbitalLabels(self)
                class(OrbitalLabels), intent(inout) :: self
                call self%InitPersistent()
        end subroutine
        subroutine StoreOrbitalLabelsObjectFields(self)
                class(OrbitalLabels), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine LoadOrbitalLabelsObjectFields(self)
                class(OrbitalLabels), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
        end subroutine
        subroutine ResetOrbitalLabelsSectionFields(self)
                class(OrbitalLabels), intent(inout) :: self
        end subroutine
        subroutine DisconnectOrbitalLabelsObjectFields(self)
                class(OrbitalLabels), intent(inout) :: self
               type(iterator) :: iter
        end subroutine
        subroutine StoreOrbitalLabelsScalarFields(self)
                class(OrbitalLabels), intent(inout) :: self
                call self%write('short_label', self%short_label)
                call self%write('latex_label', self%latex_label)
        end subroutine
        subroutine LoadOrbitalLabelsScalarFields(self)
                class(OrbitalLabels), intent(inout) :: self
                call self%read('short_label', self%short_label)
                call self%read('latex_label', self%latex_label)
        end subroutine
        subroutine FinalizeOrbitalLabels(self)
               type(OrbitalLabels), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOrbitalLabels(self)
                class(OrbitalLabels), intent(inout) :: self
                type(OrbitalLabels), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOrbitalLabelsEqual(lhs, rhs) result(iseq)
                class(OrbitalLabels), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (OrbitalLabels)
                       iseq = iseq .and. (lhs%short_label == rhs%short_label)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%latex_label == rhs%latex_label)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOrbitalLabels(lhs, rhs)
                class(OrbitalLabels), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (OrbitalLabels)
                       lhs%short_label = rhs%short_label
                       lhs%latex_label = rhs%latex_label
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateOrbitalLabelsObjectFields(self)
                class(OrbitalLabels), intent(inout) :: self
        end subroutine


        subroutine InitAtomicShell(self)
                class(AtomicShell), intent(inout) :: self
                call self%InitPersistent()
                self%atom_number =  0
                self%l =  0
                self%lapw_kind =  0
                self%num_equivalents =  0
                self%num_anti_equivalents =  0
                self%cluster_size =  int(1,kind=int32)
                self%is_correlated = .False.
                self%nrel =  int(1,kind=int32)
                self%dim =  0
                self%start_index =  int(0,kind=int32)
                self%end_index =  int(0,kind=int32)
        end subroutine
        subroutine StoreAtomicShellObjectFields(self)
                class(AtomicShell), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%raw_rep%StoreObject(ps, gid,  'raw_rep')
                call self%rep%StoreObject(ps, gid,  'rep')
                call self%basis%StoreObject(ps, gid,  'basis')

                call iter%Init(lbound(self%labels), ubound(self%labels))
                do while (.not. iter%Done())
                    call self%labels(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'labels', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
                call self%lapw_indices%StoreObject(ps, gid,  'lapw_indices')
                call self%det_signs%StoreObject(ps, gid,  'det_signs')
                call self%rotations%StoreObject(ps, gid,  'rotations')
                call self%translations%StoreObject(ps, gid,  'translations')
        end subroutine
        subroutine LoadAtomicShellObjectFields(self)
                class(AtomicShell), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%raw_rep%LoadObject(ps, gid,  'raw_rep')
                call self%rep%LoadObject(ps, gid,  'rep')
                call self%basis%LoadObject(ps, gid,  'basis')

                allocate(self%labels(int(1):int(self%dim)))
                call iter%Init(lbound(self%labels), ubound(self%labels))
                do while (.not. iter%Done())
                    call self%labels(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'labels', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
                call self%lapw_indices%LoadObject(ps, gid,  'lapw_indices')
                call self%det_signs%LoadObject(ps, gid,  'det_signs')
                call self%rotations%LoadObject(ps, gid,  'rotations')
                call self%translations%LoadObject(ps, gid,  'translations')
        end subroutine
        subroutine ResetAtomicShellSectionFields(self)
                class(AtomicShell), intent(inout) :: self
                self%raw_rep_ => self%raw_rep%GetWithExtents(self%GetAtomicShellraw_repExtents())
                self%rep_ => self%rep%GetWithExtents(self%GetAtomicShellrepExtents())
                self%basis_ => self%basis%GetWithExtents(self%GetAtomicShellbasisExtents())
                self%lapw_indices_ => self%lapw_indices%GetWithExtents(self%GetAtomicShelllapw_indicesExtents())
                self%det_signs_ => self%det_signs%GetWithExtents(self%GetAtomicShelldet_signsExtents())
                self%rotations_ => self%rotations%GetWithExtents(self%GetAtomicShellrotationsExtents())
                self%translations_ => self%translations%GetWithExtents(self%GetAtomicShelltranslationsExtents())
        end subroutine
        subroutine DisconnectAtomicShellObjectFields(self)
                class(AtomicShell), intent(inout) :: self
               type(iterator) :: iter
                call self%raw_rep%DisconnectFromStore()
                call self%rep%DisconnectFromStore()
                call self%basis%DisconnectFromStore()

                call iter%Init(lbound(self%labels), ubound(self%labels))
                do while (.not. iter%Done())
                    call self%labels(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
                call self%lapw_indices%DisconnectFromStore()
                call self%det_signs%DisconnectFromStore()
                call self%rotations%DisconnectFromStore()
                call self%translations%DisconnectFromStore()
        end subroutine
        subroutine StoreAtomicShellScalarFields(self)
                class(AtomicShell), intent(inout) :: self
                call self%write('atom_number', self%atom_number)
                call self%write('l', self%l)
                call self%write('lapw_kind', self%lapw_kind)
                call self%write('num_equivalents', self%num_equivalents)
                call self%write('num_anti_equivalents', self%num_anti_equivalents)
                call self%write('cluster_size', self%cluster_size)
                call self%write('is_correlated', self%is_correlated)
                call self%write('nrel', self%nrel)
                call self%write('dim', self%dim)
                call self%write('start_index', self%start_index)
                call self%write('end_index', self%end_index)
        end subroutine
        subroutine LoadAtomicShellScalarFields(self)
                class(AtomicShell), intent(inout) :: self
                call self%read('atom_number', self%atom_number)
                call self%read('l', self%l)
                call self%read('lapw_kind', self%lapw_kind)
                call self%read('num_equivalents', self%num_equivalents)
                call self%read('num_anti_equivalents', self%num_anti_equivalents)
                call self%read('cluster_size', self%cluster_size)
                call self%read('is_correlated', self%is_correlated)
                call self%read('nrel', self%nrel)
                call self%read('dim', self%dim)
                call self%read('start_index', self%start_index)
                call self%read('end_index', self%end_index)
        end subroutine
        subroutine FinalizeAtomicShell(self)
               type(AtomicShell), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearAtomicShell(self)
                class(AtomicShell), intent(inout) :: self
                type(AtomicShell), save :: empty
                self = empty
        end subroutine
        pure elemental function IsAtomicShellEqual(lhs, rhs) result(iseq)
                class(AtomicShell), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (AtomicShell)
                       iseq = iseq .and. (lhs%atom_number == rhs%atom_number)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%l == rhs%l)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%lapw_kind == rhs%lapw_kind)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_equivalents == rhs%num_equivalents)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_anti_equivalents == rhs%num_anti_equivalents)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%cluster_size == rhs%cluster_size)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%is_correlated .eqv. rhs%is_correlated)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrel == rhs%nrel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dim == rhs%dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%start_index == rhs%start_index)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%end_index == rhs%end_index)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%raw_rep == rhs%raw_rep)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rep == rhs%rep)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%basis == rhs%basis)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%labels) .eqv. allocated(rhs%labels))
                       if (.not. iseq) return
                       if (allocated(lhs%labels)) then
                           iseq = iseq .and. all(shape(lhs%labels) == shape(rhs%labels))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%labels(:) == rhs%labels(:))
                        end if
                       iseq = iseq .and. (lhs%lapw_indices == rhs%lapw_indices)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%det_signs == rhs%det_signs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rotations == rhs%rotations)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%translations == rhs%translations)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorAtomicShell(lhs, rhs)
                class(AtomicShell), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (AtomicShell)
                       lhs%atom_number = rhs%atom_number
                       lhs%l = rhs%l
                       lhs%lapw_kind = rhs%lapw_kind
                       lhs%num_equivalents = rhs%num_equivalents
                       lhs%num_anti_equivalents = rhs%num_anti_equivalents
                       lhs%cluster_size = rhs%cluster_size
                       lhs%is_correlated = rhs%is_correlated
                       lhs%nrel = rhs%nrel
                       lhs%dim = rhs%dim
                       lhs%start_index = rhs%start_index
                       lhs%end_index = rhs%end_index
                       lhs%raw_rep = rhs%raw_rep
                       lhs%rep = rhs%rep
                       lhs%basis = rhs%basis
                       lhs%labels = rhs%labels
                       lhs%lapw_indices = rhs%lapw_indices
                       lhs%det_signs = rhs%det_signs
                       lhs%rotations = rhs%rotations
                       lhs%translations = rhs%translations
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetAtomicshellRaw_repExtents(self) result(res)
                class(AtomicShell), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        function GetAtomicshellRepExtents(self) result(res)
                class(AtomicShell), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        function GetAtomicshellBasisExtents(self) result(res)
                class(AtomicShell), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        function GetAtomicshellLapw_indicesExtents(self) result(res)
                class(AtomicShell), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%dim * self%num_equivalents + self%dim * self%num_anti_equivalents)]
        end function
        function GetAtomicshellDet_signsExtents(self) result(res)
                class(AtomicShell), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_equivalents + self%num_anti_equivalents)]
        end function
        function GetAtomicshellRotationsExtents(self) result(res)
                class(AtomicShell), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_equivalents + self%num_anti_equivalents),extent(1,self%dim),extent(1,self%dim)]
        end function
        function GetAtomicshellTranslationsExtents(self) result(res)
                class(AtomicShell), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_equivalents + self%num_anti_equivalents)]
        end function
        subroutine AllocateAtomicShellObjectFields(self)
                class(AtomicShell), intent(inout) :: self
                call self%raw_rep%init(int(self%dim),int(self%dim))
                call self%rep%init(int(self%dim),int(self%dim))
                call self%basis%init(int(self%dim),int(self%dim))
                allocate(self%labels(int(1):int(self%dim)))
                call self%lapw_indices%init(int(self%dim * self%num_equivalents + self%dim * self%num_anti_equivalents))
                call self%det_signs%init(int(self%num_equivalents + self%num_anti_equivalents))
                call self%rotations%init(int(self%num_equivalents + self%num_anti_equivalents),int(self%dim),int(self%dim))
                call self%translations%init(int(3),int(self%num_equivalents + self%num_anti_equivalents))
        end subroutine


        subroutine InitSelectedOrbitals(self)
                class(SelectedOrbitals), intent(inout) :: self
                call self%InitPersistent()
                self%num_muffin_orbs =  0
                self%num_shells =  0
                self%total_dim =  0
                self%exclusion_margin =  real(2.000000,kind=16)
        end subroutine
        subroutine StoreSelectedOrbitalsObjectFields(self)
                class(SelectedOrbitals), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%V%StoreObject(ps, gid,  'V')

                call iter%Init(lbound(self%shells), ubound(self%shells))
                do while (.not. iter%Done())
                    call self%shells(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'shells', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadSelectedOrbitalsObjectFields(self)
                class(SelectedOrbitals), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%V%LoadObject(ps, gid,  'V')

                allocate(self%shells(int(1):int(self%num_shells)))
                call iter%Init(lbound(self%shells), ubound(self%shells))
                do while (.not. iter%Done())
                    call self%shells(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'shells', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetSelectedOrbitalsSectionFields(self)
                class(SelectedOrbitals), intent(inout) :: self
                self%V_ => self%V%GetWithExtents(self%GetSelectedOrbitalsVExtents())
        end subroutine
        subroutine DisconnectSelectedOrbitalsObjectFields(self)
                class(SelectedOrbitals), intent(inout) :: self
               type(iterator) :: iter
                call self%V%DisconnectFromStore()

                call iter%Init(lbound(self%shells), ubound(self%shells))
                do while (.not. iter%Done())
                    call self%shells(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreSelectedOrbitalsScalarFields(self)
                class(SelectedOrbitals), intent(inout) :: self
                call self%write('num_muffin_orbs', self%num_muffin_orbs)
                call self%write('num_shells', self%num_shells)
                call self%write('total_dim', self%total_dim)
                call self%write('subspace_expr', self%subspace_expr)
                call self%write('exclusion_margin', self%exclusion_margin)
        end subroutine
        subroutine LoadSelectedOrbitalsScalarFields(self)
                class(SelectedOrbitals), intent(inout) :: self
                call self%read('num_muffin_orbs', self%num_muffin_orbs)
                call self%read('num_shells', self%num_shells)
                call self%read('total_dim', self%total_dim)
                call self%read('subspace_expr', self%subspace_expr)
                call self%read('exclusion_margin', self%exclusion_margin)
        end subroutine
        subroutine FinalizeSelectedOrbitals(self)
               type(SelectedOrbitals), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSelectedOrbitals(self)
                class(SelectedOrbitals), intent(inout) :: self
                type(SelectedOrbitals), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSelectedOrbitalsEqual(lhs, rhs) result(iseq)
                class(SelectedOrbitals), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (SelectedOrbitals)
                       iseq = iseq .and. (lhs%num_muffin_orbs == rhs%num_muffin_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_shells == rhs%num_shells)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%total_dim == rhs%total_dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%subspace_expr == rhs%subspace_expr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%exclusion_margin == rhs%exclusion_margin)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%V == rhs%V)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%shells) .eqv. allocated(rhs%shells))
                       if (.not. iseq) return
                       if (allocated(lhs%shells)) then
                           iseq = iseq .and. all(shape(lhs%shells) == shape(rhs%shells))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%shells(:) == rhs%shells(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSelectedOrbitals(lhs, rhs)
                class(SelectedOrbitals), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (SelectedOrbitals)
                       lhs%num_muffin_orbs = rhs%num_muffin_orbs
                       lhs%num_shells = rhs%num_shells
                       lhs%total_dim = rhs%total_dim
                       lhs%subspace_expr = rhs%subspace_expr
                       lhs%exclusion_margin = rhs%exclusion_margin
                       lhs%V = rhs%V
                       lhs%shells = rhs%shells
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetSelectedorbitalsVExtents(self) result(res)
                class(SelectedOrbitals), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_muffin_orbs),extent(1,self%total_dim)]
        end function
        subroutine AllocateSelectedOrbitalsObjectFields(self)
                class(SelectedOrbitals), intent(inout) :: self
                call self%V%init(int(self%num_muffin_orbs),int(self%total_dim))
                allocate(self%shells(int(1):int(self%num_shells)))
        end subroutine


        subroutine InitOrbitalsBandProjector(self)
                class(OrbitalsBandProjector), intent(inout) :: self
                call self%InitPersistent()
                self%num_k_all =  0
                self%num_orbs =  0
                self%min_band =  0
                self%max_band =  0
                self%num_si =  int(1,kind=int32)
        end subroutine
        subroutine StoreOrbitalsBandProjectorObjectFields(self)
                class(OrbitalsBandProjector), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%P%StoreObject(ps, gid,  'P')
        end subroutine
        subroutine LoadOrbitalsBandProjectorObjectFields(self)
                class(OrbitalsBandProjector), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%P%LoadObject(ps, gid,  'P')
        end subroutine
        subroutine ResetOrbitalsBandProjectorSectionFields(self)
                class(OrbitalsBandProjector), intent(inout) :: self
                self%P_ => self%P%GetWithExtents(self%GetOrbitalsBandProjectorPExtents())
        end subroutine
        subroutine DisconnectOrbitalsBandProjectorObjectFields(self)
                class(OrbitalsBandProjector), intent(inout) :: self
               type(iterator) :: iter
                call self%P%DisconnectFromStore()
        end subroutine
        subroutine StoreOrbitalsBandProjectorScalarFields(self)
                class(OrbitalsBandProjector), intent(inout) :: self
                call self%write('num_k_all', self%num_k_all)
                call self%write('num_orbs', self%num_orbs)
                call self%write('min_band', self%min_band)
                call self%write('max_band', self%max_band)
                call self%write('num_si', self%num_si)
        end subroutine
        subroutine LoadOrbitalsBandProjectorScalarFields(self)
                class(OrbitalsBandProjector), intent(inout) :: self
                call self%read('num_k_all', self%num_k_all)
                call self%read('num_orbs', self%num_orbs)
                call self%read('min_band', self%min_band)
                call self%read('max_band', self%max_band)
                call self%read('num_si', self%num_si)
        end subroutine
        subroutine FinalizeOrbitalsBandProjector(self)
               type(OrbitalsBandProjector), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOrbitalsBandProjector(self)
                class(OrbitalsBandProjector), intent(inout) :: self
                type(OrbitalsBandProjector), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOrbitalsBandProjectorEqual(lhs, rhs) result(iseq)
                class(OrbitalsBandProjector), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (OrbitalsBandProjector)
                       iseq = iseq .and. (lhs%num_k_all == rhs%num_k_all)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_orbs == rhs%num_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%min_band == rhs%min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_band == rhs%max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%P == rhs%P)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOrbitalsBandProjector(lhs, rhs)
                class(OrbitalsBandProjector), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (OrbitalsBandProjector)
                       lhs%num_k_all = rhs%num_k_all
                       lhs%num_orbs = rhs%num_orbs
                       lhs%min_band = rhs%min_band
                       lhs%max_band = rhs%max_band
                       lhs%num_si = rhs%num_si
                       lhs%P = rhs%P
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetOrbitalsbandprojectorPExtents(self) result(res)
                class(OrbitalsBandProjector), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%max_band - self%min_band + 1),extent(1,self%num_orbs),extent(1,self%num_k_all),extent(1,self%num_si)]
        end function
        subroutine AllocateOrbitalsBandProjectorObjectFields(self)
                class(OrbitalsBandProjector), intent(inout) :: self
                call self%P%init(int(self%max_band - self%min_band + 1),int(self%num_orbs),int(self%num_k_all),int(self%num_si))
        end subroutine


        subroutine InitOrbitalMTProjector(self)
                class(OrbitalMTProjector), intent(inout) :: self
                call self%Expansion%Init()
        end subroutine
        subroutine StoreOrbitalMTProjectorObjectFields(self)
                class(OrbitalMTProjector), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Expansion%StoreObjectFields()
        end subroutine
        subroutine LoadOrbitalMTProjectorObjectFields(self)
                class(OrbitalMTProjector), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Expansion%LoadObjectFields()
        end subroutine
        subroutine ResetOrbitalMTProjectorSectionFields(self)
                class(OrbitalMTProjector), intent(inout) :: self
                call self%Expansion%ResetSectionFields()
        end subroutine
        subroutine DisconnectOrbitalMTProjectorObjectFields(self)
                class(OrbitalMTProjector), intent(inout) :: self
               type(iterator) :: iter
                call self%Expansion%DisconnectObjectFields()
        end subroutine
        subroutine StoreOrbitalMTProjectorScalarFields(self)
                class(OrbitalMTProjector), intent(inout) :: self
                call self%Expansion%StoreScalarFields()
        end subroutine
        subroutine LoadOrbitalMTProjectorScalarFields(self)
                class(OrbitalMTProjector), intent(inout) :: self
                call self%Expansion%LoadScalarFields()
        end subroutine
        subroutine FinalizeOrbitalMTProjector(self)
               type(OrbitalMTProjector), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOrbitalMTProjector(self)
                class(OrbitalMTProjector), intent(inout) :: self
                type(OrbitalMTProjector), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOrbitalMTProjectorEqual(lhs, rhs) result(iseq)
                class(OrbitalMTProjector), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (OrbitalMTProjector)
                       iseq = iseq .and. (lhs%Expansion == rhs%Expansion)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOrbitalMTProjector(lhs, rhs)
                class(OrbitalMTProjector), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (OrbitalMTProjector)
                       lhs%Expansion = rhs%Expansion
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateOrbitalMTProjectorObjectFields(self)
                class(OrbitalMTProjector), intent(inout) :: self
                call self%Expansion%AllocateObjectFields()
        end subroutine



end module
