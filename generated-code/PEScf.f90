
module PEScf
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use FlapwMBPT_interface
    use atomic_orbitals
    use base_types
    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: Histogram
        integer(kind=int32)  ::  num_configs =  0

        type(vector_real)  ::  p
        real(kind=dp),pointer :: p_(:)
        type(vector_int)  ::  N
        integer(kind=int32),pointer :: N_(:)
        type(vector_real)  ::  energy
        real(kind=dp),pointer :: energy_(:)
        type(vector_real)  ::  Jz
        real(kind=dp),pointer :: Jz_(:)


        contains
        procedure :: AllocateObjectFields => AllocateHistogramObjectFields
        procedure :: ResetSectionFields   => ResetHistogramSectionFields
        procedure :: StoreScalarFields    => StoreHistogramScalarFields
        procedure :: StoreObjectFields    => StoreHistogramObjectFields
        procedure :: LoadScalarFields     => LoadHistogramScalarFields
        procedure :: LoadObjectFields     => LoadHistogramObjectFields
        procedure :: DisconnectObjectFields => DisconnectHistogramObjectFields
        procedure :: IsEqual              => IsHistogramEqual
        procedure :: AssignmentOperator   => AssignmentOperatorHistogram
        procedure :: clear                => ClearHistogram
        procedure :: init => InitHistogram
#ifndef __GFORTRAN__
        final     :: FinalizeHistogram
#endif
        procedure :: GetHistogramPExtents
        procedure :: GetHistogramNExtents
        procedure :: GetHistogramEnergyExtents
        procedure :: GetHistogramJzExtents

    end type

    type, extends(persistent) :: SolverState
        integer(kind=int32)  ::  super_iter =  int(1,kind=int32)

        integer(kind=int32)  ::  sub_iter =  int(0,kind=int32)

        integer(kind=int32)  ::  num_equivalents =  0

        integer(kind=int32)  ::  num_anti_equivalents =  int(0,kind=int32)

        integer(kind=int32)  ::  dim =  0

        integer(kind=int32)  ::  num_si =  0

        integer(kind=int32)  ::  nrel =  int(1,kind=int32)

        type(string)  ::  double_counting_method
        real(kind=dp)  ::  nominal_dc =  0.0d0

        type(cube_complex)  ::  DC
        complex(kind=dp),pointer :: DC_(:,:,:)
        real(kind=dp)  ::  initial_spin_polarization =  0.0d0

        type(string)  ::  state
        real(kind=dp)  ::  mu =  0.0d0

        real(kind=dp)  ::  U =  0.0d0

        real(kind=dp)  ::  J =  0.0d0

        real(kind=dp)  ::  beta =  0.0d0

        real(kind=dp)  ::  Nimp =  0.0d0

        real(kind=dp)  ::  Nlatt =  0.0d0

        logical  ::  ising = .False.

        logical  ::  kanamori = .False.

        type(matrix_int)  ::  rep
        integer(kind=int32),pointer :: rep_(:,:)
        integer(kind=int32)  ::  num_unique_orbs =  0

        type(Window)  ::  proj_energy_window
        integer(kind=int32)  ::  window_min_band =  int(1,kind=int32)

        integer(kind=int32)  ::  window_max_band =  int(1,kind=int32)

        real(kind=dp)  ::  energyImp =  0.0d0

        real(kind=dp)  ::  NN =  0.0d0

        type(cube_complex)  ::  RhoImp
        complex(kind=dp),pointer :: RhoImp_(:,:,:)
        real(kind=dp)  ::  energyBands =  0.0d0

        type(Histogram)  ::  hist
        type(Convergence)  ::  cvg
        real(kind=dp)  ::  quality =  real(0.000000,kind=16)

        type(Window)  ::  NRange


        contains
        procedure :: AllocateObjectFields => AllocateSolverStateObjectFields
        procedure :: ResetSectionFields   => ResetSolverStateSectionFields
        procedure :: StoreScalarFields    => StoreSolverStateScalarFields
        procedure :: StoreObjectFields    => StoreSolverStateObjectFields
        procedure :: LoadScalarFields     => LoadSolverStateScalarFields
        procedure :: LoadObjectFields     => LoadSolverStateObjectFields
        procedure :: DisconnectObjectFields => DisconnectSolverStateObjectFields
        procedure :: IsEqual              => IsSolverStateEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSolverState
        procedure :: clear                => ClearSolverState
        procedure :: init => InitSolverState
#ifndef __GFORTRAN__
        final     :: FinalizeSolverState
#endif
        procedure :: GetSolverstateDcExtents
        procedure :: GetSolverstateRepExtents
        procedure :: GetSolverstateRhoimpExtents

    end type

    type, extends(persistent) :: SolverProblem
        type(string)  ::  self_path
        logical  ::  isReal = .False.

        real(kind=dp)  ::  beta =  0.0d0

        real(kind=dp)  ::  mu =  0.0d0

        real(kind=dp)  ::  U =  0.0d0

        real(kind=dp)  ::  J =  0.0d0

        real(kind=dp)  ::  Uprime =  0.0d0

        real(kind=dp)  ::  F0 =  0.0d0

        real(kind=dp)  ::  F2 =  0.0d0

        real(kind=dp)  ::  F4 =  0.0d0

        integer(kind=int32)  ::  dim =  0

        integer(kind=int32)  ::  num_si =  0

        integer(kind=int32)  ::  nrel =  0

        type(cube_complex)  ::  E
        complex(kind=dp),pointer :: E_(:,:,:)
        type(AtomicShell)  ::  subshell
        integer(kind=int32)  ::  correlated_orbitals_id =  0



        contains
        procedure :: AllocateObjectFields => AllocateSolverProblemObjectFields
        procedure :: ResetSectionFields   => ResetSolverProblemSectionFields
        procedure :: StoreScalarFields    => StoreSolverProblemScalarFields
        procedure :: StoreObjectFields    => StoreSolverProblemObjectFields
        procedure :: LoadScalarFields     => LoadSolverProblemScalarFields
        procedure :: LoadObjectFields     => LoadSolverProblemObjectFields
        procedure :: DisconnectObjectFields => DisconnectSolverProblemObjectFields
        procedure :: IsEqual              => IsSolverProblemEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSolverProblem
        procedure :: clear                => ClearSolverProblem
        procedure :: init => InitSolverProblem
#ifndef __GFORTRAN__
        final     :: FinalizeSolverProblem
#endif
        procedure :: GetSolverproblemEExtents

    end type

    type, extends(persistent) :: SolverSolution
        real(kind=dp)  ::  E =  0.0d0

        real(kind=dp)  ::  N =  0.0d0

        real(kind=dp)  ::  NN =  0.0d0

        type(Histogram)  ::  hist
        real(kind=dp)  ::  quality =  real(0.000000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateSolverSolutionObjectFields
        procedure :: ResetSectionFields   => ResetSolverSolutionSectionFields
        procedure :: StoreScalarFields    => StoreSolverSolutionScalarFields
        procedure :: StoreObjectFields    => StoreSolverSolutionObjectFields
        procedure :: LoadScalarFields     => LoadSolverSolutionScalarFields
        procedure :: LoadObjectFields     => LoadSolverSolutionObjectFields
        procedure :: DisconnectObjectFields => DisconnectSolverSolutionObjectFields
        procedure :: IsEqual              => IsSolverSolutionEqual
        procedure :: AssignmentOperator   => AssignmentOperatorSolverSolution
        procedure :: clear                => ClearSolverSolution
        procedure :: init => InitSolverSolution
#ifndef __GFORTRAN__
        final     :: FinalizeSolverSolution
#endif

    end type

    contains
        subroutine InitHistogram(self)
                class(Histogram), intent(inout) :: self
                call self%InitPersistent()
                self%num_configs =  0
        end subroutine
        subroutine StoreHistogramObjectFields(self)
                class(Histogram), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%p%StoreObject(ps, gid,  'p')
                call self%N%StoreObject(ps, gid,  'N')
                call self%energy%StoreObject(ps, gid,  'energy')
                call self%Jz%StoreObject(ps, gid,  'Jz')
        end subroutine
        subroutine LoadHistogramObjectFields(self)
                class(Histogram), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%p%LoadObject(ps, gid,  'p')
                call self%N%LoadObject(ps, gid,  'N')
                call self%energy%LoadObject(ps, gid,  'energy')
                call self%Jz%LoadObject(ps, gid,  'Jz')
        end subroutine
        subroutine ResetHistogramSectionFields(self)
                class(Histogram), intent(inout) :: self
                self%p_ => self%p%GetWithExtents(self%GetHistogrampExtents())
                self%N_ => self%N%GetWithExtents(self%GetHistogramNExtents())
                self%energy_ => self%energy%GetWithExtents(self%GetHistogramenergyExtents())
                self%Jz_ => self%Jz%GetWithExtents(self%GetHistogramJzExtents())
        end subroutine
        subroutine DisconnectHistogramObjectFields(self)
                class(Histogram), intent(inout) :: self
               type(iterator) :: iter
                call self%p%DisconnectFromStore()
                call self%N%DisconnectFromStore()
                call self%energy%DisconnectFromStore()
                call self%Jz%DisconnectFromStore()
        end subroutine
        subroutine StoreHistogramScalarFields(self)
                class(Histogram), intent(inout) :: self
                call self%write('num_configs', self%num_configs)
        end subroutine
        subroutine LoadHistogramScalarFields(self)
                class(Histogram), intent(inout) :: self
                call self%read('num_configs', self%num_configs)
        end subroutine
        subroutine FinalizeHistogram(self)
               type(Histogram), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearHistogram(self)
                class(Histogram), intent(inout) :: self
                type(Histogram), save :: empty
                self = empty
        end subroutine
        pure elemental function IsHistogramEqual(lhs, rhs) result(iseq)
                class(Histogram), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Histogram)
                       iseq = iseq .and. (lhs%num_configs == rhs%num_configs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%p == rhs%p)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%N == rhs%N)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%energy == rhs%energy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Jz == rhs%Jz)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorHistogram(lhs, rhs)
                class(Histogram), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Histogram)
                       lhs%num_configs = rhs%num_configs
                       lhs%p = rhs%p
                       lhs%N = rhs%N
                       lhs%energy = rhs%energy
                       lhs%Jz = rhs%Jz
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetHistogramPExtents(self) result(res)
                class(Histogram), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_configs)]
        end function
        function GetHistogramNExtents(self) result(res)
                class(Histogram), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_configs)]
        end function
        function GetHistogramEnergyExtents(self) result(res)
                class(Histogram), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_configs)]
        end function
        function GetHistogramJzExtents(self) result(res)
                class(Histogram), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_configs)]
        end function
        subroutine AllocateHistogramObjectFields(self)
                class(Histogram), intent(inout) :: self
                call self%p%init(int(self%num_configs))
                call self%N%init(int(self%num_configs))
                call self%energy%init(int(self%num_configs))
                call self%Jz%init(int(self%num_configs))
        end subroutine


        subroutine InitSolverState(self)
                class(SolverState), intent(inout) :: self
                call self%InitPersistent()
                self%super_iter =  int(1,kind=int32)
                self%sub_iter =  int(0,kind=int32)
                self%num_equivalents =  0
                self%num_anti_equivalents =  int(0,kind=int32)
                self%dim =  0
                self%num_si =  0
                self%nrel =  int(1,kind=int32)
                self%nominal_dc =  0.0d0
                self%initial_spin_polarization =  0.0d0
                self%state = "start"
                self%mu =  0.0d0
                self%U =  0.0d0
                self%J =  0.0d0
                self%beta =  0.0d0
                self%Nimp =  0.0d0
                self%Nlatt =  0.0d0
                self%ising = .False.
                self%kanamori = .False.
                self%num_unique_orbs =  0
                call self%proj_energy_window%InitPersistent()
                self%window_min_band =  int(1,kind=int32)
                self%window_max_band =  int(1,kind=int32)
                self%energyImp =  0.0d0
                self%NN =  0.0d0
                self%energyBands =  0.0d0
                call self%hist%InitPersistent()
                call self%cvg%InitPersistent()
                self%quality =  real(0.000000,kind=16)
                call self%NRange%InitPersistent()
        end subroutine
        subroutine StoreSolverStateObjectFields(self)
                class(SolverState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%DC%StoreObject(ps, gid,  'DC')
                call self%rep%StoreObject(ps, gid,  'rep')
                call self%proj_energy_window%store(ps%FetchSubGroup(gid,  'proj_energy_window'))
                call self%RhoImp%StoreObject(ps, gid,  'RhoImp')
                call self%hist%store(ps%FetchSubGroup(gid,  'hist'))
                call self%cvg%store(ps%FetchSubGroup(gid,  'cvg'))
                call self%NRange%store(ps%FetchSubGroup(gid,  'NRange'))
        end subroutine
        subroutine LoadSolverStateObjectFields(self)
                class(SolverState), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%DC%LoadObject(ps, gid,  'DC')
                call self%rep%LoadObject(ps, gid,  'rep')
                call self%proj_energy_window%load(ps%FetchSubGroup(gid,  'proj_energy_window'))
                call self%RhoImp%LoadObject(ps, gid,  'RhoImp')
                call self%hist%load(ps%FetchSubGroup(gid,  'hist'))
                call self%cvg%load(ps%FetchSubGroup(gid,  'cvg'))
                call self%NRange%load(ps%FetchSubGroup(gid,  'NRange'))
        end subroutine
        subroutine ResetSolverStateSectionFields(self)
                class(SolverState), intent(inout) :: self
                self%DC_ => self%DC%GetWithExtents(self%GetSolverStateDCExtents())
                self%rep_ => self%rep%GetWithExtents(self%GetSolverStaterepExtents())
                self%RhoImp_ => self%RhoImp%GetWithExtents(self%GetSolverStateRhoImpExtents())
        end subroutine
        subroutine DisconnectSolverStateObjectFields(self)
                class(SolverState), intent(inout) :: self
               type(iterator) :: iter
                call self%DC%DisconnectFromStore()
                call self%rep%DisconnectFromStore()
                call self%proj_energy_window%disconnect()
                call self%RhoImp%DisconnectFromStore()
                call self%hist%disconnect()
                call self%cvg%disconnect()
                call self%NRange%disconnect()
        end subroutine
        subroutine StoreSolverStateScalarFields(self)
                class(SolverState), intent(inout) :: self
                call self%write('super_iter', self%super_iter)
                call self%write('sub_iter', self%sub_iter)
                call self%write('num_equivalents', self%num_equivalents)
                call self%write('num_anti_equivalents', self%num_anti_equivalents)
                call self%write('dim', self%dim)
                call self%write('num_si', self%num_si)
                call self%write('nrel', self%nrel)
                call self%write('double_counting_method', self%double_counting_method)
                call self%write('nominal_dc', self%nominal_dc)
                call self%write('initial_spin_polarization', self%initial_spin_polarization)
                call self%write('state', self%state)
                call self%write('mu', self%mu)
                call self%write('U', self%U)
                call self%write('J', self%J)
                call self%write('beta', self%beta)
                call self%write('Nimp', self%Nimp)
                call self%write('Nlatt', self%Nlatt)
                call self%write('ising', self%ising)
                call self%write('kanamori', self%kanamori)
                call self%write('num_unique_orbs', self%num_unique_orbs)
                call self%write('window_min_band', self%window_min_band)
                call self%write('window_max_band', self%window_max_band)
                call self%write('energyImp', self%energyImp)
                call self%write('NN', self%NN)
                call self%write('energyBands', self%energyBands)
                call self%write('quality', self%quality)
        end subroutine
        subroutine LoadSolverStateScalarFields(self)
                class(SolverState), intent(inout) :: self
                call self%read('super_iter', self%super_iter)
                call self%read('sub_iter', self%sub_iter)
                call self%read('num_equivalents', self%num_equivalents)
                call self%read('num_anti_equivalents', self%num_anti_equivalents)
                call self%read('dim', self%dim)
                call self%read('num_si', self%num_si)
                call self%read('nrel', self%nrel)
                call self%read('double_counting_method', self%double_counting_method)
                call self%read('nominal_dc', self%nominal_dc)
                call self%read('initial_spin_polarization', self%initial_spin_polarization)
                call self%read('state', self%state)
                call self%read('mu', self%mu)
                call self%read('U', self%U)
                call self%read('J', self%J)
                call self%read('beta', self%beta)
                call self%read('Nimp', self%Nimp)
                call self%read('Nlatt', self%Nlatt)
                call self%read('ising', self%ising)
                call self%read('kanamori', self%kanamori)
                call self%read('num_unique_orbs', self%num_unique_orbs)
                call self%read('window_min_band', self%window_min_band)
                call self%read('window_max_band', self%window_max_band)
                call self%read('energyImp', self%energyImp)
                call self%read('NN', self%NN)
                call self%read('energyBands', self%energyBands)
                call self%read('quality', self%quality)
        end subroutine
        subroutine FinalizeSolverState(self)
               type(SolverState), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSolverState(self)
                class(SolverState), intent(inout) :: self
                type(SolverState), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSolverStateEqual(lhs, rhs) result(iseq)
                class(SolverState), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (SolverState)
                       iseq = iseq .and. (lhs%super_iter == rhs%super_iter)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sub_iter == rhs%sub_iter)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_equivalents == rhs%num_equivalents)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_anti_equivalents == rhs%num_anti_equivalents)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dim == rhs%dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrel == rhs%nrel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%double_counting_method == rhs%double_counting_method)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nominal_dc == rhs%nominal_dc)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%DC == rhs%DC)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%initial_spin_polarization == rhs%initial_spin_polarization)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%state == rhs%state)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%mu == rhs%mu)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%U == rhs%U)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%J == rhs%J)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%beta == rhs%beta)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Nimp == rhs%Nimp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Nlatt == rhs%Nlatt)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ising .eqv. rhs%ising)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%kanamori .eqv. rhs%kanamori)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%rep == rhs%rep)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_unique_orbs == rhs%num_unique_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%proj_energy_window == rhs%proj_energy_window)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%window_min_band == rhs%window_min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%window_max_band == rhs%window_max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%energyImp == rhs%energyImp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%NN == rhs%NN)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%RhoImp == rhs%RhoImp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%energyBands == rhs%energyBands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hist == rhs%hist)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%cvg == rhs%cvg)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%quality == rhs%quality)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%NRange == rhs%NRange)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSolverState(lhs, rhs)
                class(SolverState), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (SolverState)
                       lhs%super_iter = rhs%super_iter
                       lhs%sub_iter = rhs%sub_iter
                       lhs%num_equivalents = rhs%num_equivalents
                       lhs%num_anti_equivalents = rhs%num_anti_equivalents
                       lhs%dim = rhs%dim
                       lhs%num_si = rhs%num_si
                       lhs%nrel = rhs%nrel
                       lhs%double_counting_method = rhs%double_counting_method
                       lhs%nominal_dc = rhs%nominal_dc
                       lhs%DC = rhs%DC
                       lhs%initial_spin_polarization = rhs%initial_spin_polarization
                       lhs%state = rhs%state
                       lhs%mu = rhs%mu
                       lhs%U = rhs%U
                       lhs%J = rhs%J
                       lhs%beta = rhs%beta
                       lhs%Nimp = rhs%Nimp
                       lhs%Nlatt = rhs%Nlatt
                       lhs%ising = rhs%ising
                       lhs%kanamori = rhs%kanamori
                       lhs%rep = rhs%rep
                       lhs%num_unique_orbs = rhs%num_unique_orbs
                       lhs%proj_energy_window = rhs%proj_energy_window
                       lhs%window_min_band = rhs%window_min_band
                       lhs%window_max_band = rhs%window_max_band
                       lhs%energyImp = rhs%energyImp
                       lhs%NN = rhs%NN
                       lhs%RhoImp = rhs%RhoImp
                       lhs%energyBands = rhs%energyBands
                       lhs%hist = rhs%hist
                       lhs%cvg = rhs%cvg
                       lhs%quality = rhs%quality
                       lhs%NRange = rhs%NRange
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetSolverstateDcExtents(self) result(res)
                class(SolverState), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        function GetSolverstateRepExtents(self) result(res)
                class(SolverState), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%dim),extent(1,self%dim)]
        end function
        function GetSolverstateRhoimpExtents(self) result(res)
                class(SolverState), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        subroutine AllocateSolverStateObjectFields(self)
                class(SolverState), intent(inout) :: self
                call self%DC%init(int(self%dim),int(self%dim),int(self%num_si))
                call self%rep%init(int(self%dim),int(self%dim))
                call self%RhoImp%init(int(self%dim),int(self%dim),int(self%num_si))
        end subroutine


        subroutine InitSolverProblem(self)
                class(SolverProblem), intent(inout) :: self
                call self%InitPersistent()
                self%isReal = .False.
                self%beta =  0.0d0
                self%mu =  0.0d0
                self%U =  0.0d0
                self%J =  0.0d0
                self%Uprime =  0.0d0
                self%F0 =  0.0d0
                self%F2 =  0.0d0
                self%F4 =  0.0d0
                self%dim =  0
                self%num_si =  0
                self%nrel =  0
                call self%subshell%InitPersistent()
                self%correlated_orbitals_id =  0
        end subroutine
        subroutine StoreSolverProblemObjectFields(self)
                class(SolverProblem), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%E%StoreObject(ps, gid,  'E')
                call self%subshell%store(ps%FetchSubGroup(gid,  'subshell'))
        end subroutine
        subroutine LoadSolverProblemObjectFields(self)
                class(SolverProblem), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%E%LoadObject(ps, gid,  'E')
                call self%subshell%load(ps%FetchSubGroup(gid,  'subshell'))
        end subroutine
        subroutine ResetSolverProblemSectionFields(self)
                class(SolverProblem), intent(inout) :: self
                self%E_ => self%E%GetWithExtents(self%GetSolverProblemEExtents())
        end subroutine
        subroutine DisconnectSolverProblemObjectFields(self)
                class(SolverProblem), intent(inout) :: self
               type(iterator) :: iter
                call self%E%DisconnectFromStore()
                call self%subshell%disconnect()
        end subroutine
        subroutine StoreSolverProblemScalarFields(self)
                class(SolverProblem), intent(inout) :: self
                call self%write('self_path', self%self_path)
                call self%write('isReal', self%isReal)
                call self%write('beta', self%beta)
                call self%write('mu', self%mu)
                call self%write('U', self%U)
                call self%write('J', self%J)
                call self%write('Uprime', self%Uprime)
                call self%write('F0', self%F0)
                call self%write('F2', self%F2)
                call self%write('F4', self%F4)
                call self%write('dim', self%dim)
                call self%write('num_si', self%num_si)
                call self%write('nrel', self%nrel)
                call self%write('correlated_orbitals_id', self%correlated_orbitals_id)
        end subroutine
        subroutine LoadSolverProblemScalarFields(self)
                class(SolverProblem), intent(inout) :: self
                call self%read('self_path', self%self_path)
                call self%read('isReal', self%isReal)
                call self%read('beta', self%beta)
                call self%read('mu', self%mu)
                call self%read('U', self%U)
                call self%read('J', self%J)
                call self%read('Uprime', self%Uprime)
                call self%read('F0', self%F0)
                call self%read('F2', self%F2)
                call self%read('F4', self%F4)
                call self%read('dim', self%dim)
                call self%read('num_si', self%num_si)
                call self%read('nrel', self%nrel)
                call self%read('correlated_orbitals_id', self%correlated_orbitals_id)
        end subroutine
        subroutine FinalizeSolverProblem(self)
               type(SolverProblem), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSolverProblem(self)
                class(SolverProblem), intent(inout) :: self
                type(SolverProblem), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSolverProblemEqual(lhs, rhs) result(iseq)
                class(SolverProblem), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (SolverProblem)
                       iseq = iseq .and. (lhs%self_path == rhs%self_path)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%isReal .eqv. rhs%isReal)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%beta == rhs%beta)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%mu == rhs%mu)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%U == rhs%U)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%J == rhs%J)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Uprime == rhs%Uprime)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%F0 == rhs%F0)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%F2 == rhs%F2)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%F4 == rhs%F4)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dim == rhs%dim)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrel == rhs%nrel)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%E == rhs%E)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%subshell == rhs%subshell)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%correlated_orbitals_id == rhs%correlated_orbitals_id)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSolverProblem(lhs, rhs)
                class(SolverProblem), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (SolverProblem)
                       lhs%self_path = rhs%self_path
                       lhs%isReal = rhs%isReal
                       lhs%beta = rhs%beta
                       lhs%mu = rhs%mu
                       lhs%U = rhs%U
                       lhs%J = rhs%J
                       lhs%Uprime = rhs%Uprime
                       lhs%F0 = rhs%F0
                       lhs%F2 = rhs%F2
                       lhs%F4 = rhs%F4
                       lhs%dim = rhs%dim
                       lhs%num_si = rhs%num_si
                       lhs%nrel = rhs%nrel
                       lhs%E = rhs%E
                       lhs%subshell = rhs%subshell
                       lhs%correlated_orbitals_id = rhs%correlated_orbitals_id
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetSolverproblemEExtents(self) result(res)
                class(SolverProblem), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%dim),extent(1,self%dim),extent(1,self%num_si)]
        end function
        subroutine AllocateSolverProblemObjectFields(self)
                class(SolverProblem), intent(inout) :: self
                call self%E%init(int(self%dim),int(self%dim),int(self%num_si))
        end subroutine


        subroutine InitSolverSolution(self)
                class(SolverSolution), intent(inout) :: self
                call self%InitPersistent()
                self%E =  0.0d0
                self%N =  0.0d0
                self%NN =  0.0d0
                call self%hist%InitPersistent()
                self%quality =  real(0.000000,kind=16)
        end subroutine
        subroutine StoreSolverSolutionObjectFields(self)
                class(SolverSolution), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%hist%store(ps%FetchSubGroup(gid,  'hist'))
        end subroutine
        subroutine LoadSolverSolutionObjectFields(self)
                class(SolverSolution), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%hist%load(ps%FetchSubGroup(gid,  'hist'))
        end subroutine
        subroutine ResetSolverSolutionSectionFields(self)
                class(SolverSolution), intent(inout) :: self
        end subroutine
        subroutine DisconnectSolverSolutionObjectFields(self)
                class(SolverSolution), intent(inout) :: self
               type(iterator) :: iter
                call self%hist%disconnect()
        end subroutine
        subroutine StoreSolverSolutionScalarFields(self)
                class(SolverSolution), intent(inout) :: self
                call self%write('E', self%E)
                call self%write('N', self%N)
                call self%write('NN', self%NN)
                call self%write('quality', self%quality)
        end subroutine
        subroutine LoadSolverSolutionScalarFields(self)
                class(SolverSolution), intent(inout) :: self
                call self%read('E', self%E)
                call self%read('N', self%N)
                call self%read('NN', self%NN)
                call self%read('quality', self%quality)
        end subroutine
        subroutine FinalizeSolverSolution(self)
               type(SolverSolution), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearSolverSolution(self)
                class(SolverSolution), intent(inout) :: self
                type(SolverSolution), save :: empty
                self = empty
        end subroutine
        pure elemental function IsSolverSolutionEqual(lhs, rhs) result(iseq)
                class(SolverSolution), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (SolverSolution)
                       iseq = iseq .and. (lhs%E == rhs%E)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%N == rhs%N)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%NN == rhs%NN)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%hist == rhs%hist)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%quality == rhs%quality)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorSolverSolution(lhs, rhs)
                class(SolverSolution), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (SolverSolution)
                       lhs%E = rhs%E
                       lhs%N = rhs%N
                       lhs%NN = rhs%NN
                       lhs%hist = rhs%hist
                       lhs%quality = rhs%quality
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateSolverSolutionObjectFields(self)
                class(SolverSolution), intent(inout) :: self
        end subroutine



end module
