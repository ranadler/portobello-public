
module energy
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use cube
    use matrix


    implicit none
    public

    type, extends(persistent) :: FreeEnergy
        integer(kind=int32)  ::  min_band =  0

        integer(kind=int32)  ::  max_band =  0

        integer(kind=int32)  ::  num_k_irr =  0

        integer(kind=int32)  ::  num_si =  0

        integer(kind=int32)  ::  num_reps =  int(1,kind=int32)

        integer(kind=int32)  ::  num_atoms =  int(1,kind=int32)

        type(cube_real)  ::  Hqp
        real(kind=dp),pointer :: Hqp_(:,:,:)
        type(matrix_real)  ::  weight
        real(kind=dp),pointer :: weight_(:,:)
        real(kind=dp)  ::  corr_energy_term =  real(0.000000,kind=16)

        real(kind=dp)  ::  Fx =  real(0.000000,kind=16)

        real(kind=dp)  ::  Fc =  real(0.000000,kind=16)

        real(kind=dp)  ::  z_vnucl =  real(0.000000,kind=16)

        real(kind=dp)  ::  ro_vh_new =  real(0.000000,kind=16)

        real(kind=dp)  ::  exch_dft =  real(0.000000,kind=16)

        real(kind=dp)  ::  e_coul =  real(0.000000,kind=16)

        real(kind=dp)  ::  tr_vxc_valence =  real(0.000000,kind=16)

        real(kind=dp)  ::  tr_vh_valence =  real(0.000000,kind=16)

        real(kind=dp)  ::  total_energy_core =  real(0.000000,kind=16)

        real(kind=dp)  ::  e_h_core =  real(0.000000,kind=16)

        real(kind=dp)  ::  e_xc_core =  real(0.000000,kind=16)

        real(kind=dp)  ::  core_energy =  real(0.000000,kind=16)

        real(kind=dp)  ::  muN =  real(0.000000,kind=16)

        real(kind=dp)  ::  correlated_term_ry =  real(0.000000,kind=16)

        real(kind=dp)  ::  trLogHloc =  real(0.000000,kind=16)

        real(kind=dp)  ::  trLogHlocCorr =  real(0.000000,kind=16)

        real(kind=dp)  ::  muRy =  0.0d0

        real(kind=dp)  ::  N =  0.0d0

        real(kind=dp)  ::  Nqp =  0.0d0

        real(kind=dp)  ::  result =  real(0.000000,kind=16)



        contains
        procedure :: AllocateObjectFields => AllocateFreeEnergyObjectFields
        procedure :: ResetSectionFields   => ResetFreeEnergySectionFields
        procedure :: StoreScalarFields    => StoreFreeEnergyScalarFields
        procedure :: StoreObjectFields    => StoreFreeEnergyObjectFields
        procedure :: LoadScalarFields     => LoadFreeEnergyScalarFields
        procedure :: LoadObjectFields     => LoadFreeEnergyObjectFields
        procedure :: DisconnectObjectFields => DisconnectFreeEnergyObjectFields
        procedure :: IsEqual              => IsFreeEnergyEqual
        procedure :: AssignmentOperator   => AssignmentOperatorFreeEnergy
        procedure :: clear                => ClearFreeEnergy
        procedure :: init => InitFreeEnergy
#ifndef __GFORTRAN__
        final     :: FinalizeFreeEnergy
#endif
        procedure :: GetFreeenergyHqpExtents
        procedure :: GetFreeenergyWeightExtents

    end type
    interface
         function CalculateFreeEnergyDFTI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function CalculateFreeEnergyGutzI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface

    interface
         function CalculateFreeEnergyDMFTI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    contains
        subroutine InitFreeEnergy(self)
                class(FreeEnergy), intent(inout) :: self
                call self%InitPersistent()
                self%min_band =  0
                self%max_band =  0
                self%num_k_irr =  0
                self%num_si =  0
                self%num_reps =  int(1,kind=int32)
                self%num_atoms =  int(1,kind=int32)
                self%corr_energy_term =  real(0.000000,kind=16)
                self%Fx =  real(0.000000,kind=16)
                self%Fc =  real(0.000000,kind=16)
                self%z_vnucl =  real(0.000000,kind=16)
                self%ro_vh_new =  real(0.000000,kind=16)
                self%exch_dft =  real(0.000000,kind=16)
                self%e_coul =  real(0.000000,kind=16)
                self%tr_vxc_valence =  real(0.000000,kind=16)
                self%tr_vh_valence =  real(0.000000,kind=16)
                self%total_energy_core =  real(0.000000,kind=16)
                self%e_h_core =  real(0.000000,kind=16)
                self%e_xc_core =  real(0.000000,kind=16)
                self%core_energy =  real(0.000000,kind=16)
                self%muN =  real(0.000000,kind=16)
                self%correlated_term_ry =  real(0.000000,kind=16)
                self%trLogHloc =  real(0.000000,kind=16)
                self%trLogHlocCorr =  real(0.000000,kind=16)
                self%muRy =  0.0d0
                self%N =  0.0d0
                self%Nqp =  0.0d0
                self%result =  real(0.000000,kind=16)
        end subroutine
        subroutine StoreFreeEnergyObjectFields(self)
                class(FreeEnergy), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Hqp%StoreObject(ps, gid,  'Hqp')
                call self%weight%StoreObject(ps, gid,  'weight')
        end subroutine
        subroutine LoadFreeEnergyObjectFields(self)
                class(FreeEnergy), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Hqp%LoadObject(ps, gid,  'Hqp')
                call self%weight%LoadObject(ps, gid,  'weight')
        end subroutine
        subroutine ResetFreeEnergySectionFields(self)
                class(FreeEnergy), intent(inout) :: self
                self%Hqp_ => self%Hqp%GetWithExtents(self%GetFreeEnergyHqpExtents())
                self%weight_ => self%weight%GetWithExtents(self%GetFreeEnergyweightExtents())
        end subroutine
        subroutine DisconnectFreeEnergyObjectFields(self)
                class(FreeEnergy), intent(inout) :: self
               type(iterator) :: iter
                call self%Hqp%DisconnectFromStore()
                call self%weight%DisconnectFromStore()
        end subroutine
        subroutine StoreFreeEnergyScalarFields(self)
                class(FreeEnergy), intent(inout) :: self
                call self%write('min_band', self%min_band)
                call self%write('max_band', self%max_band)
                call self%write('num_k_irr', self%num_k_irr)
                call self%write('num_si', self%num_si)
                call self%write('num_reps', self%num_reps)
                call self%write('num_atoms', self%num_atoms)
                call self%write('corr_energy_term', self%corr_energy_term)
                call self%write('Fx', self%Fx)
                call self%write('Fc', self%Fc)
                call self%write('z_vnucl', self%z_vnucl)
                call self%write('ro_vh_new', self%ro_vh_new)
                call self%write('exch_dft', self%exch_dft)
                call self%write('e_coul', self%e_coul)
                call self%write('tr_vxc_valence', self%tr_vxc_valence)
                call self%write('tr_vh_valence', self%tr_vh_valence)
                call self%write('total_energy_core', self%total_energy_core)
                call self%write('e_h_core', self%e_h_core)
                call self%write('e_xc_core', self%e_xc_core)
                call self%write('core_energy', self%core_energy)
                call self%write('muN', self%muN)
                call self%write('correlated_term_ry', self%correlated_term_ry)
                call self%write('trLogHloc', self%trLogHloc)
                call self%write('trLogHlocCorr', self%trLogHlocCorr)
                call self%write('muRy', self%muRy)
                call self%write('N', self%N)
                call self%write('Nqp', self%Nqp)
                call self%write('result', self%result)
        end subroutine
        subroutine LoadFreeEnergyScalarFields(self)
                class(FreeEnergy), intent(inout) :: self
                call self%read('min_band', self%min_band)
                call self%read('max_band', self%max_band)
                call self%read('num_k_irr', self%num_k_irr)
                call self%read('num_si', self%num_si)
                call self%read('num_reps', self%num_reps)
                call self%read('num_atoms', self%num_atoms)
                call self%read('corr_energy_term', self%corr_energy_term)
                call self%read('Fx', self%Fx)
                call self%read('Fc', self%Fc)
                call self%read('z_vnucl', self%z_vnucl)
                call self%read('ro_vh_new', self%ro_vh_new)
                call self%read('exch_dft', self%exch_dft)
                call self%read('e_coul', self%e_coul)
                call self%read('tr_vxc_valence', self%tr_vxc_valence)
                call self%read('tr_vh_valence', self%tr_vh_valence)
                call self%read('total_energy_core', self%total_energy_core)
                call self%read('e_h_core', self%e_h_core)
                call self%read('e_xc_core', self%e_xc_core)
                call self%read('core_energy', self%core_energy)
                call self%read('muN', self%muN)
                call self%read('correlated_term_ry', self%correlated_term_ry)
                call self%read('trLogHloc', self%trLogHloc)
                call self%read('trLogHlocCorr', self%trLogHlocCorr)
                call self%read('muRy', self%muRy)
                call self%read('N', self%N)
                call self%read('Nqp', self%Nqp)
                call self%read('result', self%result)
        end subroutine
        subroutine FinalizeFreeEnergy(self)
               type(FreeEnergy), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearFreeEnergy(self)
                class(FreeEnergy), intent(inout) :: self
                type(FreeEnergy), save :: empty
                self = empty
        end subroutine
        pure elemental function IsFreeEnergyEqual(lhs, rhs) result(iseq)
                class(FreeEnergy), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (FreeEnergy)
                       iseq = iseq .and. (lhs%min_band == rhs%min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_band == rhs%max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_irr == rhs%num_k_irr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_reps == rhs%num_reps)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_atoms == rhs%num_atoms)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Hqp == rhs%Hqp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%weight == rhs%weight)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%corr_energy_term == rhs%corr_energy_term)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Fx == rhs%Fx)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Fc == rhs%Fc)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%z_vnucl == rhs%z_vnucl)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ro_vh_new == rhs%ro_vh_new)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%exch_dft == rhs%exch_dft)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%e_coul == rhs%e_coul)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%tr_vxc_valence == rhs%tr_vxc_valence)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%tr_vh_valence == rhs%tr_vh_valence)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%total_energy_core == rhs%total_energy_core)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%e_h_core == rhs%e_h_core)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%e_xc_core == rhs%e_xc_core)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%core_energy == rhs%core_energy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%muN == rhs%muN)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%correlated_term_ry == rhs%correlated_term_ry)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%trLogHloc == rhs%trLogHloc)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%trLogHlocCorr == rhs%trLogHlocCorr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%muRy == rhs%muRy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%N == rhs%N)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Nqp == rhs%Nqp)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%result == rhs%result)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorFreeEnergy(lhs, rhs)
                class(FreeEnergy), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (FreeEnergy)
                       lhs%min_band = rhs%min_band
                       lhs%max_band = rhs%max_band
                       lhs%num_k_irr = rhs%num_k_irr
                       lhs%num_si = rhs%num_si
                       lhs%num_reps = rhs%num_reps
                       lhs%num_atoms = rhs%num_atoms
                       lhs%Hqp = rhs%Hqp
                       lhs%weight = rhs%weight
                       lhs%corr_energy_term = rhs%corr_energy_term
                       lhs%Fx = rhs%Fx
                       lhs%Fc = rhs%Fc
                       lhs%z_vnucl = rhs%z_vnucl
                       lhs%ro_vh_new = rhs%ro_vh_new
                       lhs%exch_dft = rhs%exch_dft
                       lhs%e_coul = rhs%e_coul
                       lhs%tr_vxc_valence = rhs%tr_vxc_valence
                       lhs%tr_vh_valence = rhs%tr_vh_valence
                       lhs%total_energy_core = rhs%total_energy_core
                       lhs%e_h_core = rhs%e_h_core
                       lhs%e_xc_core = rhs%e_xc_core
                       lhs%core_energy = rhs%core_energy
                       lhs%muN = rhs%muN
                       lhs%correlated_term_ry = rhs%correlated_term_ry
                       lhs%trLogHloc = rhs%trLogHloc
                       lhs%trLogHlocCorr = rhs%trLogHlocCorr
                       lhs%muRy = rhs%muRy
                       lhs%N = rhs%N
                       lhs%Nqp = rhs%Nqp
                       lhs%result = rhs%result
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetFreeenergyHqpExtents(self) result(res)
                class(FreeEnergy), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(self%min_band,self%max_band),extent(1,self%num_k_irr),extent(1,self%num_si)]
        end function
        function GetFreeenergyWeightExtents(self) result(res)
                class(FreeEnergy), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_reps),extent(1,self%num_k_irr)]
        end function
        subroutine AllocateFreeEnergyObjectFields(self)
                class(FreeEnergy), intent(inout) :: self
                call self%Hqp%init(int(self%max_band- (self%min_band) + 1),int(self%num_k_irr),int(self%num_si))
                call self%weight%init(int(self%num_reps),int(self%num_k_irr))
        end subroutine


        subroutine CalculateFreeEnergyDFTWrapper(ret, imp_fp, selfpath) bind(C,name='energy_mp_calculatefreeenergydftwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(CalculateFreeEnergyDFTI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine CalculateFreeEnergyGutzWrapper(ret, imp_fp, selfpath) bind(C,name='energy_mp_calculatefreeenergygutzwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(CalculateFreeEnergyGutzI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine CalculateFreeEnergyDMFTWrapper(ret, imp_fp, selfpath) bind(C,name='energy_mp_calculatefreeenergydmftwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(CalculateFreeEnergyDMFTI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine


end module
