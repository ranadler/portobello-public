
module wannier
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use atomic_orbitals
    use base_types
    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(OrbitalsBandProjector) :: WannierInput
        integer(kind=int32)  ::  num_k_full =  0

        type(Window)  ::  dis_window
        type(string)  ::  location_of_partial_bs_all_k
        type(cube_complex)  ::  A
        complex(kind=dp),pointer :: A_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateWannierInputObjectFields
        procedure :: ResetSectionFields   => ResetWannierInputSectionFields
        procedure :: StoreScalarFields    => StoreWannierInputScalarFields
        procedure :: StoreObjectFields    => StoreWannierInputObjectFields
        procedure :: LoadScalarFields     => LoadWannierInputScalarFields
        procedure :: LoadObjectFields     => LoadWannierInputObjectFields
        procedure :: DisconnectObjectFields => DisconnectWannierInputObjectFields
        procedure :: IsEqual              => IsWannierInputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWannierInput
        procedure :: clear                => ClearWannierInput
        procedure :: init => InitWannierInput
#ifndef __GFORTRAN__
        final     :: FinalizeWannierInput
#endif
        procedure :: GetWannierinputAExtents

    end type
    interface
         function ComputeI(selfpath, outputpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
                 type(string), intent(in)            :: outputpath
         end function
    end interface


    type, extends(OrbitalsBandProjector) :: WannierOutput
        type(vector_int)  ::  nearest_atom
        integer(kind=int32),pointer :: nearest_atom_(:)
        type(matrix_real)  ::  distance_to_nearest_atom
        real(kind=dp),pointer :: distance_to_nearest_atom_(:,:)
        type(matrix_real)  ::  centers
        real(kind=dp),pointer :: centers_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateWannierOutputObjectFields
        procedure :: ResetSectionFields   => ResetWannierOutputSectionFields
        procedure :: StoreScalarFields    => StoreWannierOutputScalarFields
        procedure :: StoreObjectFields    => StoreWannierOutputObjectFields
        procedure :: LoadScalarFields     => LoadWannierOutputScalarFields
        procedure :: LoadObjectFields     => LoadWannierOutputObjectFields
        procedure :: DisconnectObjectFields => DisconnectWannierOutputObjectFields
        procedure :: IsEqual              => IsWannierOutputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWannierOutput
        procedure :: clear                => ClearWannierOutput
        procedure :: init => InitWannierOutput
#ifndef __GFORTRAN__
        final     :: FinalizeWannierOutput
#endif
        procedure :: GetWannieroutputNearest_atomExtents
        procedure :: GetWannieroutputDistance_to_nearest_atomExtents
        procedure :: GetWannieroutputCentersExtents

    end type
    interface
         function WriteXSFI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    contains
        subroutine InitWannierInput(self)
                class(WannierInput), intent(inout) :: self
                call self%OrbitalsBandProjector%Init()
                self%num_k_full =  0
                call self%dis_window%InitPersistent()
        end subroutine
        subroutine StoreWannierInputObjectFields(self)
                class(WannierInput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%OrbitalsBandProjector%StoreObjectFields()
                call self%dis_window%store(ps%FetchSubGroup(gid,  'dis_window'))
                call self%A%StoreObject(ps, gid,  'A')
        end subroutine
        subroutine LoadWannierInputObjectFields(self)
                class(WannierInput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%OrbitalsBandProjector%LoadObjectFields()
                call self%dis_window%load(ps%FetchSubGroup(gid,  'dis_window'))
                call self%A%LoadObject(ps, gid,  'A')
        end subroutine
        subroutine ResetWannierInputSectionFields(self)
                class(WannierInput), intent(inout) :: self
                call self%OrbitalsBandProjector%ResetSectionFields()
                self%A_ => self%A%GetWithExtents(self%GetWannierInputAExtents())
        end subroutine
        subroutine DisconnectWannierInputObjectFields(self)
                class(WannierInput), intent(inout) :: self
               type(iterator) :: iter
                call self%OrbitalsBandProjector%DisconnectObjectFields()
                call self%dis_window%disconnect()
                call self%A%DisconnectFromStore()
        end subroutine
        subroutine StoreWannierInputScalarFields(self)
                class(WannierInput), intent(inout) :: self
                call self%OrbitalsBandProjector%StoreScalarFields()
                call self%write('num_k_full', self%num_k_full)
                call self%write('location_of_partial_bs_all_k', self%location_of_partial_bs_all_k)
        end subroutine
        subroutine LoadWannierInputScalarFields(self)
                class(WannierInput), intent(inout) :: self
                call self%OrbitalsBandProjector%LoadScalarFields()
                call self%read('num_k_full', self%num_k_full)
                call self%read('location_of_partial_bs_all_k', self%location_of_partial_bs_all_k)
        end subroutine
        subroutine FinalizeWannierInput(self)
               type(WannierInput), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWannierInput(self)
                class(WannierInput), intent(inout) :: self
                type(WannierInput), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWannierInputEqual(lhs, rhs) result(iseq)
                class(WannierInput), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (WannierInput)
                       iseq = iseq .and. (lhs%OrbitalsBandProjector == rhs%OrbitalsBandProjector)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_full == rhs%num_k_full)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dis_window == rhs%dis_window)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%location_of_partial_bs_all_k == rhs%location_of_partial_bs_all_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%A == rhs%A)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWannierInput(lhs, rhs)
                class(WannierInput), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (WannierInput)
                       lhs%OrbitalsBandProjector = rhs%OrbitalsBandProjector
                       lhs%num_k_full = rhs%num_k_full
                       lhs%dis_window = rhs%dis_window
                       lhs%location_of_partial_bs_all_k = rhs%location_of_partial_bs_all_k
                       lhs%A = rhs%A
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetWannierinputAExtents(self) result(res)
                class(WannierInput), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%max_band - self%min_band + 1),extent(1,self%num_orbs),extent(1,self%num_k_full)]
        end function
        subroutine AllocateWannierInputObjectFields(self)
                class(WannierInput), intent(inout) :: self
                call self%OrbitalsBandProjector%AllocateObjectFields()
                call self%A%init(int(self%max_band - self%min_band + 1),int(self%num_orbs),int(self%num_k_full))
        end subroutine


        subroutine ComputeWrapper(ret, imp_fp, selfpath, outputpath) bind(C,name='wannier_mp_computewrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
                type(c_ptr),intent(in),value        :: outputpath
            procedure(ComputeI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath), FromCStr(outputpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine InitWannierOutput(self)
                class(WannierOutput), intent(inout) :: self
                call self%OrbitalsBandProjector%Init()
        end subroutine
        subroutine StoreWannierOutputObjectFields(self)
                class(WannierOutput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%OrbitalsBandProjector%StoreObjectFields()
                call self%nearest_atom%StoreObject(ps, gid,  'nearest_atom')
                call self%distance_to_nearest_atom%StoreObject(ps, gid,  'distance_to_nearest_atom')
                call self%centers%StoreObject(ps, gid,  'centers')
        end subroutine
        subroutine LoadWannierOutputObjectFields(self)
                class(WannierOutput), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%OrbitalsBandProjector%LoadObjectFields()
                call self%nearest_atom%LoadObject(ps, gid,  'nearest_atom')
                call self%distance_to_nearest_atom%LoadObject(ps, gid,  'distance_to_nearest_atom')
                call self%centers%LoadObject(ps, gid,  'centers')
        end subroutine
        subroutine ResetWannierOutputSectionFields(self)
                class(WannierOutput), intent(inout) :: self
                call self%OrbitalsBandProjector%ResetSectionFields()
                self%nearest_atom_ => self%nearest_atom%GetWithExtents(self%GetWannierOutputnearest_atomExtents())
                self%distance_to_nearest_atom_ => self%distance_to_nearest_atom%GetWithExtents(self%GetWannierOutputdistance_to_nearest_atomExtents())
                self%centers_ => self%centers%GetWithExtents(self%GetWannierOutputcentersExtents())
        end subroutine
        subroutine DisconnectWannierOutputObjectFields(self)
                class(WannierOutput), intent(inout) :: self
               type(iterator) :: iter
                call self%OrbitalsBandProjector%DisconnectObjectFields()
                call self%nearest_atom%DisconnectFromStore()
                call self%distance_to_nearest_atom%DisconnectFromStore()
                call self%centers%DisconnectFromStore()
        end subroutine
        subroutine StoreWannierOutputScalarFields(self)
                class(WannierOutput), intent(inout) :: self
                call self%OrbitalsBandProjector%StoreScalarFields()
        end subroutine
        subroutine LoadWannierOutputScalarFields(self)
                class(WannierOutput), intent(inout) :: self
                call self%OrbitalsBandProjector%LoadScalarFields()
        end subroutine
        subroutine FinalizeWannierOutput(self)
               type(WannierOutput), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWannierOutput(self)
                class(WannierOutput), intent(inout) :: self
                type(WannierOutput), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWannierOutputEqual(lhs, rhs) result(iseq)
                class(WannierOutput), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (WannierOutput)
                       iseq = iseq .and. (lhs%OrbitalsBandProjector == rhs%OrbitalsBandProjector)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nearest_atom == rhs%nearest_atom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%distance_to_nearest_atom == rhs%distance_to_nearest_atom)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%centers == rhs%centers)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWannierOutput(lhs, rhs)
                class(WannierOutput), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (WannierOutput)
                       lhs%OrbitalsBandProjector = rhs%OrbitalsBandProjector
                       lhs%nearest_atom = rhs%nearest_atom
                       lhs%distance_to_nearest_atom = rhs%distance_to_nearest_atom
                       lhs%centers = rhs%centers
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetWannieroutputNearest_atomExtents(self) result(res)
                class(WannierOutput), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_orbs)]
        end function
        function GetWannieroutputDistance_to_nearest_atomExtents(self) result(res)
                class(WannierOutput), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_orbs)]
        end function
        function GetWannieroutputCentersExtents(self) result(res)
                class(WannierOutput), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_orbs)]
        end function
        subroutine AllocateWannierOutputObjectFields(self)
                class(WannierOutput), intent(inout) :: self
                call self%OrbitalsBandProjector%AllocateObjectFields()
                call self%nearest_atom%init(int(self%num_orbs))
                call self%distance_to_nearest_atom%init(int(3),int(self%num_orbs))
                call self%centers%init(int(3),int(self%num_orbs))
        end subroutine


        subroutine WriteXSFWrapper(ret, imp_fp, selfpath) bind(C,name='wannier_mp_writexsfwrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(WriteXSFI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine


end module
