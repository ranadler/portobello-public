
module LAPW
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use array4
    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: PWOverlap
        integer(kind=int32)  ::  num_pw =  0

        type(matrix_complex)  ::  overlap
        complex(kind=dp),pointer :: overlap_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocatePWOverlapObjectFields
        procedure :: ResetSectionFields   => ResetPWOverlapSectionFields
        procedure :: StoreScalarFields    => StorePWOverlapScalarFields
        procedure :: StoreObjectFields    => StorePWOverlapObjectFields
        procedure :: LoadScalarFields     => LoadPWOverlapScalarFields
        procedure :: LoadObjectFields     => LoadPWOverlapObjectFields
        procedure :: DisconnectObjectFields => DisconnectPWOverlapObjectFields
        procedure :: IsEqual              => IsPWOverlapEqual
        procedure :: AssignmentOperator   => AssignmentOperatorPWOverlap
        procedure :: clear                => ClearPWOverlap
        procedure :: init => InitPWOverlap
#ifndef __GFORTRAN__
        final     :: FinalizePWOverlap
#endif
        procedure :: GetPwoverlapOverlapExtents

    end type

    type, extends(persistent) :: Basis
        integer(kind=int32)  ::  num_muffin_orbs =  0

        integer(kind=int32)  ::  num_k_all =  0

        integer(kind=int32)  ::  num_k_irr =  0

        integer(kind=int32)  ::  num_k_shard =  0

        integer(kind=int32)  ::  num_si =  int(1,kind=int32)

        integer(kind=int32)  ::  nrel =  int(1,kind=int32)

        type(PWOverlap), allocatable  ::  pw_overlap(:)
        type(vector_int)  ::  atom_number
        integer(kind=int32),pointer :: atom_number_(:)
        type(vector_int)  ::  n
        integer(kind=int32),pointer :: n_(:)
        type(vector_int)  ::  l
        integer(kind=int32),pointer :: l_(:)
        type(vector_int)  ::  m
        integer(kind=int32),pointer :: m_(:)
        type(vector_int)  ::  correlated
        integer(kind=int32),pointer :: correlated_(:)
        type(vector_int)  ::  kind
        integer(kind=int32),pointer :: kind_(:)
        type(matrix_real)  ::  mo_overlap
        real(kind=dp),pointer :: mo_overlap_(:,:)
        type(vector_real)  ::  weight
        real(kind=dp),pointer :: weight_(:)
        type(matrix_real)  ::  KPoints
        real(kind=dp),pointer :: KPoints_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateBasisObjectFields
        procedure :: ResetSectionFields   => ResetBasisSectionFields
        procedure :: StoreScalarFields    => StoreBasisScalarFields
        procedure :: StoreObjectFields    => StoreBasisObjectFields
        procedure :: LoadScalarFields     => LoadBasisScalarFields
        procedure :: LoadObjectFields     => LoadBasisObjectFields
        procedure :: DisconnectObjectFields => DisconnectBasisObjectFields
        procedure :: IsEqual              => IsBasisEqual
        procedure :: AssignmentOperator   => AssignmentOperatorBasis
        procedure :: clear                => ClearBasis
        procedure :: init => InitBasis
#ifndef __GFORTRAN__
        final     :: FinalizeBasis
#endif
        procedure :: GetBasisAtom_numberExtents
        procedure :: GetBasisNExtents
        procedure :: GetBasisLExtents
        procedure :: GetBasisMExtents
        procedure :: GetBasisCorrelatedExtents
        procedure :: GetBasisKindExtents
        procedure :: GetBasisMo_overlapExtents
        procedure :: GetBasisWeightExtents
        procedure :: GetBasisKpointsExtents

    end type

    type, extends(persistent) :: MTAtomInfo
        integer(kind=int32)  ::  num_orbitals =  int(0,kind=int32)

        type(vector_real)  ::  l
        real(kind=dp),pointer :: l_(:)
        type(vector_real)  ::  energy
        real(kind=dp),pointer :: energy_(:)
        type(vector_real)  ::  core_overlap
        real(kind=dp),pointer :: core_overlap_(:)


        contains
        procedure :: AllocateObjectFields => AllocateMTAtomInfoObjectFields
        procedure :: ResetSectionFields   => ResetMTAtomInfoSectionFields
        procedure :: StoreScalarFields    => StoreMTAtomInfoScalarFields
        procedure :: StoreObjectFields    => StoreMTAtomInfoObjectFields
        procedure :: LoadScalarFields     => LoadMTAtomInfoScalarFields
        procedure :: LoadObjectFields     => LoadMTAtomInfoObjectFields
        procedure :: DisconnectObjectFields => DisconnectMTAtomInfoObjectFields
        procedure :: IsEqual              => IsMTAtomInfoEqual
        procedure :: AssignmentOperator   => AssignmentOperatorMTAtomInfo
        procedure :: clear                => ClearMTAtomInfo
        procedure :: init => InitMTAtomInfo
#ifndef __GFORTRAN__
        final     :: FinalizeMTAtomInfo
#endif
        procedure :: GetMtatominfoLExtents
        procedure :: GetMtatominfoEnergyExtents
        procedure :: GetMtatominfoCore_overlapExtents

    end type

    type, extends(persistent) :: BasisMTInfo
        integer(kind=int32)  ::  num_unique_atoms =  0

        type(MTAtomInfo), allocatable  ::  atoms(:)


        contains
        procedure :: AllocateObjectFields => AllocateBasisMTInfoObjectFields
        procedure :: ResetSectionFields   => ResetBasisMTInfoSectionFields
        procedure :: StoreScalarFields    => StoreBasisMTInfoScalarFields
        procedure :: StoreObjectFields    => StoreBasisMTInfoObjectFields
        procedure :: LoadScalarFields     => LoadBasisMTInfoScalarFields
        procedure :: LoadObjectFields     => LoadBasisMTInfoObjectFields
        procedure :: DisconnectObjectFields => DisconnectBasisMTInfoObjectFields
        procedure :: IsEqual              => IsBasisMTInfoEqual
        procedure :: AssignmentOperator   => AssignmentOperatorBasisMTInfo
        procedure :: clear                => ClearBasisMTInfo
        procedure :: init => InitBasisMTInfo
#ifndef __GFORTRAN__
        final     :: FinalizeBasisMTInfo
#endif

    end type

    type, extends(persistent) :: Expansion
        logical  ::  is_sharded = .False.

        integer(kind=int32)  ::  shard_offset =  int(0,kind=int32)

        integer(kind=int32)  ::  num_k =  0

        integer(kind=int32)  ::  num_expanded =  0

        integer(kind=int32)  ::  num_si =  int(1,kind=int32)

        integer(kind=int32)  ::  max_num_pw =  0

        integer(kind=int32)  ::  num_muffin_orbs =  0

        type(array4_complex)  ::  Z
        complex(kind=dp),pointer :: Z_(:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateExpansionObjectFields
        procedure :: ResetSectionFields   => ResetExpansionSectionFields
        procedure :: StoreScalarFields    => StoreExpansionScalarFields
        procedure :: StoreObjectFields    => StoreExpansionObjectFields
        procedure :: LoadScalarFields     => LoadExpansionScalarFields
        procedure :: LoadObjectFields     => LoadExpansionObjectFields
        procedure :: DisconnectObjectFields => DisconnectExpansionObjectFields
        procedure :: IsEqual              => IsExpansionEqual
        procedure :: AssignmentOperator   => AssignmentOperatorExpansion
        procedure :: clear                => ClearExpansion
        procedure :: init => InitExpansion
#ifndef __GFORTRAN__
        final     :: FinalizeExpansion
#endif
        procedure :: GetExpansionZExtents

    end type

    type, extends(persistent) :: KPath
        integer(kind=int32)  ::  num_k =  0

        type(matrix_real)  ::  k
        real(kind=dp),pointer :: k_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateKPathObjectFields
        procedure :: ResetSectionFields   => ResetKPathSectionFields
        procedure :: StoreScalarFields    => StoreKPathScalarFields
        procedure :: StoreObjectFields    => StoreKPathObjectFields
        procedure :: LoadScalarFields     => LoadKPathScalarFields
        procedure :: LoadObjectFields     => LoadKPathObjectFields
        procedure :: DisconnectObjectFields => DisconnectKPathObjectFields
        procedure :: IsEqual              => IsKPathEqual
        procedure :: AssignmentOperator   => AssignmentOperatorKPath
        procedure :: clear                => ClearKPath
        procedure :: init => InitKPath
#ifndef __GFORTRAN__
        final     :: FinalizeKPath
#endif
        procedure :: GetKpathKExtents

    end type

    type, extends(Expansion) :: BandStructure
        type(matrix_int)  ::  num_bands
        integer(kind=int32),pointer :: num_bands_(:,:)
        type(cube_real)  ::  energy
        real(kind=dp),pointer :: energy_(:,:,:)
        type(array4_complex)  ::  Occ
        complex(kind=dp),pointer :: Occ_(:,:,:,:)
        real(kind=dp)  ::  chemical_potential =  0.0d0



        contains
        procedure :: AllocateObjectFields => AllocateBandStructureObjectFields
        procedure :: ResetSectionFields   => ResetBandStructureSectionFields
        procedure :: StoreScalarFields    => StoreBandStructureScalarFields
        procedure :: StoreObjectFields    => StoreBandStructureObjectFields
        procedure :: LoadScalarFields     => LoadBandStructureScalarFields
        procedure :: LoadObjectFields     => LoadBandStructureObjectFields
        procedure :: DisconnectObjectFields => DisconnectBandStructureObjectFields
        procedure :: IsEqual              => IsBandStructureEqual
        procedure :: AssignmentOperator   => AssignmentOperatorBandStructure
        procedure :: clear                => ClearBandStructure
        procedure :: init => InitBandStructure
#ifndef __GFORTRAN__
        final     :: FinalizeBandStructure
#endif
        procedure :: GetBandstructureNum_bandsExtents
        procedure :: GetBandstructureEnergyExtents
        procedure :: GetBandstructureOccExtents

    end type

    type, extends(persistent) :: EquivalentAtomSymmetries
        integer(kind=int32)  ::  num_atoms =  0

        type(vector_int)  ::  g
        integer(kind=int32),pointer :: g_(:)


        contains
        procedure :: AllocateObjectFields => AllocateEquivalentAtomSymmetriesObjectFields
        procedure :: ResetSectionFields   => ResetEquivalentAtomSymmetriesSectionFields
        procedure :: StoreScalarFields    => StoreEquivalentAtomSymmetriesScalarFields
        procedure :: StoreObjectFields    => StoreEquivalentAtomSymmetriesObjectFields
        procedure :: LoadScalarFields     => LoadEquivalentAtomSymmetriesScalarFields
        procedure :: LoadObjectFields     => LoadEquivalentAtomSymmetriesObjectFields
        procedure :: DisconnectObjectFields => DisconnectEquivalentAtomSymmetriesObjectFields
        procedure :: IsEqual              => IsEquivalentAtomSymmetriesEqual
        procedure :: AssignmentOperator   => AssignmentOperatorEquivalentAtomSymmetries
        procedure :: clear                => ClearEquivalentAtomSymmetries
        procedure :: init => InitEquivalentAtomSymmetries
#ifndef __GFORTRAN__
        final     :: FinalizeEquivalentAtomSymmetries
#endif
        procedure :: GetEquivalentatomsymmetriesGExtents

    end type

    contains
        subroutine InitPWOverlap(self)
                class(PWOverlap), intent(inout) :: self
                call self%InitPersistent()
                self%num_pw =  0
        end subroutine
        subroutine StorePWOverlapObjectFields(self)
                class(PWOverlap), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%overlap%StoreObject(ps, gid,  'overlap')
        end subroutine
        subroutine LoadPWOverlapObjectFields(self)
                class(PWOverlap), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%overlap%LoadObject(ps, gid,  'overlap')
        end subroutine
        subroutine ResetPWOverlapSectionFields(self)
                class(PWOverlap), intent(inout) :: self
                self%overlap_ => self%overlap%GetWithExtents(self%GetPWOverlapoverlapExtents())
        end subroutine
        subroutine DisconnectPWOverlapObjectFields(self)
                class(PWOverlap), intent(inout) :: self
               type(iterator) :: iter
                call self%overlap%DisconnectFromStore()
        end subroutine
        subroutine StorePWOverlapScalarFields(self)
                class(PWOverlap), intent(inout) :: self
                call self%write('num_pw', self%num_pw)
        end subroutine
        subroutine LoadPWOverlapScalarFields(self)
                class(PWOverlap), intent(inout) :: self
                call self%read('num_pw', self%num_pw)
        end subroutine
        subroutine FinalizePWOverlap(self)
               type(PWOverlap), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearPWOverlap(self)
                class(PWOverlap), intent(inout) :: self
                type(PWOverlap), save :: empty
                self = empty
        end subroutine
        pure elemental function IsPWOverlapEqual(lhs, rhs) result(iseq)
                class(PWOverlap), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (PWOverlap)
                       iseq = iseq .and. (lhs%num_pw == rhs%num_pw)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%overlap == rhs%overlap)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorPWOverlap(lhs, rhs)
                class(PWOverlap), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (PWOverlap)
                       lhs%num_pw = rhs%num_pw
                       lhs%overlap = rhs%overlap
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetPwoverlapOverlapExtents(self) result(res)
                class(PWOverlap), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_pw),extent(1,self%num_pw)]
        end function
        subroutine AllocatePWOverlapObjectFields(self)
                class(PWOverlap), intent(inout) :: self
                call self%overlap%init(int(self%num_pw),int(self%num_pw))
        end subroutine


        subroutine InitBasis(self)
                class(Basis), intent(inout) :: self
                call self%InitPersistent()
                self%num_muffin_orbs =  0
                self%num_k_all =  0
                self%num_k_irr =  0
                self%num_k_shard =  0
                self%num_si =  int(1,kind=int32)
                self%nrel =  int(1,kind=int32)
        end subroutine
        subroutine StoreBasisObjectFields(self)
                class(Basis), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                call iter%Init(lbound(self%pw_overlap), ubound(self%pw_overlap))
                do while (.not. iter%Done())
                    call self%pw_overlap(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'pw_overlap', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
                call self%atom_number%StoreObject(ps, gid,  'atom_number')
                call self%n%StoreObject(ps, gid,  'n')
                call self%l%StoreObject(ps, gid,  'l')
                call self%m%StoreObject(ps, gid,  'm')
                call self%correlated%StoreObject(ps, gid,  'correlated')
                call self%kind%StoreObject(ps, gid,  'kind')
                call self%mo_overlap%StoreObject(ps, gid,  'mo_overlap')
                call self%weight%StoreObject(ps, gid,  'weight')
                call self%KPoints%StoreObject(ps, gid,  'KPoints')
        end subroutine
        subroutine LoadBasisObjectFields(self)
                class(Basis), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                allocate(self%pw_overlap(int(1):int(self%num_k_irr)))
                call iter%Init(lbound(self%pw_overlap), ubound(self%pw_overlap))
                do while (.not. iter%Done())
                    call self%pw_overlap(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'pw_overlap', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
                call self%atom_number%LoadObject(ps, gid,  'atom_number')
                call self%n%LoadObject(ps, gid,  'n')
                call self%l%LoadObject(ps, gid,  'l')
                call self%m%LoadObject(ps, gid,  'm')
                call self%correlated%LoadObject(ps, gid,  'correlated')
                call self%kind%LoadObject(ps, gid,  'kind')
                call self%mo_overlap%LoadObject(ps, gid,  'mo_overlap')
                call self%weight%LoadObject(ps, gid,  'weight')
                call self%KPoints%LoadObject(ps, gid,  'KPoints')
        end subroutine
        subroutine ResetBasisSectionFields(self)
                class(Basis), intent(inout) :: self
                self%atom_number_ => self%atom_number%GetWithExtents(self%GetBasisatom_numberExtents())
                self%n_ => self%n%GetWithExtents(self%GetBasisnExtents())
                self%l_ => self%l%GetWithExtents(self%GetBasislExtents())
                self%m_ => self%m%GetWithExtents(self%GetBasismExtents())
                self%correlated_ => self%correlated%GetWithExtents(self%GetBasiscorrelatedExtents())
                self%kind_ => self%kind%GetWithExtents(self%GetBasiskindExtents())
                self%mo_overlap_ => self%mo_overlap%GetWithExtents(self%GetBasismo_overlapExtents())
                self%weight_ => self%weight%GetWithExtents(self%GetBasisweightExtents())
                self%KPoints_ => self%KPoints%GetWithExtents(self%GetBasisKPointsExtents())
        end subroutine
        subroutine DisconnectBasisObjectFields(self)
                class(Basis), intent(inout) :: self
               type(iterator) :: iter

                call iter%Init(lbound(self%pw_overlap), ubound(self%pw_overlap))
                do while (.not. iter%Done())
                    call self%pw_overlap(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
                call self%atom_number%DisconnectFromStore()
                call self%n%DisconnectFromStore()
                call self%l%DisconnectFromStore()
                call self%m%DisconnectFromStore()
                call self%correlated%DisconnectFromStore()
                call self%kind%DisconnectFromStore()
                call self%mo_overlap%DisconnectFromStore()
                call self%weight%DisconnectFromStore()
                call self%KPoints%DisconnectFromStore()
        end subroutine
        subroutine StoreBasisScalarFields(self)
                class(Basis), intent(inout) :: self
                call self%write('num_muffin_orbs', self%num_muffin_orbs)
                call self%write('num_k_all', self%num_k_all)
                call self%write('num_k_irr', self%num_k_irr)
                call self%write('num_k_shard', self%num_k_shard)
                call self%write('num_si', self%num_si)
                call self%write('nrel', self%nrel)
        end subroutine
        subroutine LoadBasisScalarFields(self)
                class(Basis), intent(inout) :: self
                call self%read('num_muffin_orbs', self%num_muffin_orbs)
                call self%read('num_k_all', self%num_k_all)
                call self%read('num_k_irr', self%num_k_irr)
                call self%read('num_k_shard', self%num_k_shard)
                call self%read('num_si', self%num_si)
                call self%read('nrel', self%nrel)
        end subroutine
        subroutine FinalizeBasis(self)
               type(Basis), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearBasis(self)
                class(Basis), intent(inout) :: self
                type(Basis), save :: empty
                self = empty
        end subroutine
        pure elemental function IsBasisEqual(lhs, rhs) result(iseq)
                class(Basis), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Basis)
                       iseq = iseq .and. (lhs%num_muffin_orbs == rhs%num_muffin_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_all == rhs%num_k_all)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_irr == rhs%num_k_irr)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k_shard == rhs%num_k_shard)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%nrel == rhs%nrel)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%pw_overlap) .eqv. allocated(rhs%pw_overlap))
                       if (.not. iseq) return
                       if (allocated(lhs%pw_overlap)) then
                           iseq = iseq .and. all(shape(lhs%pw_overlap) == shape(rhs%pw_overlap))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%pw_overlap(:) == rhs%pw_overlap(:))
                        end if
                       iseq = iseq .and. (lhs%atom_number == rhs%atom_number)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%n == rhs%n)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%l == rhs%l)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%m == rhs%m)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%correlated == rhs%correlated)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%kind == rhs%kind)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%mo_overlap == rhs%mo_overlap)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%weight == rhs%weight)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%KPoints == rhs%KPoints)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorBasis(lhs, rhs)
                class(Basis), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Basis)
                       lhs%num_muffin_orbs = rhs%num_muffin_orbs
                       lhs%num_k_all = rhs%num_k_all
                       lhs%num_k_irr = rhs%num_k_irr
                       lhs%num_k_shard = rhs%num_k_shard
                       lhs%num_si = rhs%num_si
                       lhs%nrel = rhs%nrel
                       lhs%pw_overlap = rhs%pw_overlap
                       lhs%atom_number = rhs%atom_number
                       lhs%n = rhs%n
                       lhs%l = rhs%l
                       lhs%m = rhs%m
                       lhs%correlated = rhs%correlated
                       lhs%kind = rhs%kind
                       lhs%mo_overlap = rhs%mo_overlap
                       lhs%weight = rhs%weight
                       lhs%KPoints = rhs%KPoints
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetBasisAtom_numberExtents(self) result(res)
                class(Basis), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_muffin_orbs)]
        end function
        function GetBasisNExtents(self) result(res)
                class(Basis), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_muffin_orbs)]
        end function
        function GetBasisLExtents(self) result(res)
                class(Basis), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_muffin_orbs)]
        end function
        function GetBasisMExtents(self) result(res)
                class(Basis), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_muffin_orbs)]
        end function
        function GetBasisCorrelatedExtents(self) result(res)
                class(Basis), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_muffin_orbs)]
        end function
        function GetBasisKindExtents(self) result(res)
                class(Basis), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_muffin_orbs)]
        end function
        function GetBasisMo_overlapExtents(self) result(res)
                class(Basis), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_muffin_orbs),extent(1,self%num_muffin_orbs)]
        end function
        function GetBasisWeightExtents(self) result(res)
                class(Basis), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_k_shard)]
        end function
        function GetBasisKpointsExtents(self) result(res)
                class(Basis), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_k_all),extent(1,3)]
        end function
        subroutine AllocateBasisObjectFields(self)
                class(Basis), intent(inout) :: self
                allocate(self%pw_overlap(int(1):int(self%num_k_irr)))
                call self%atom_number%init(int(self%num_muffin_orbs))
                call self%n%init(int(self%num_muffin_orbs))
                call self%l%init(int(self%num_muffin_orbs))
                call self%m%init(int(self%num_muffin_orbs))
                call self%correlated%init(int(self%num_muffin_orbs))
                call self%kind%init(int(self%num_muffin_orbs))
                call self%mo_overlap%init(int(self%num_muffin_orbs),int(self%num_muffin_orbs))
                call self%weight%init(int(self%num_k_shard))
                call self%KPoints%init(int(self%num_k_all),int(3))
        end subroutine


        subroutine InitMTAtomInfo(self)
                class(MTAtomInfo), intent(inout) :: self
                call self%InitPersistent()
                self%num_orbitals =  int(0,kind=int32)
        end subroutine
        subroutine StoreMTAtomInfoObjectFields(self)
                class(MTAtomInfo), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%l%StoreObject(ps, gid,  'l')
                call self%energy%StoreObject(ps, gid,  'energy')
                call self%core_overlap%StoreObject(ps, gid,  'core_overlap')
        end subroutine
        subroutine LoadMTAtomInfoObjectFields(self)
                class(MTAtomInfo), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%l%LoadObject(ps, gid,  'l')
                call self%energy%LoadObject(ps, gid,  'energy')
                call self%core_overlap%LoadObject(ps, gid,  'core_overlap')
        end subroutine
        subroutine ResetMTAtomInfoSectionFields(self)
                class(MTAtomInfo), intent(inout) :: self
                self%l_ => self%l%GetWithExtents(self%GetMTAtomInfolExtents())
                self%energy_ => self%energy%GetWithExtents(self%GetMTAtomInfoenergyExtents())
                self%core_overlap_ => self%core_overlap%GetWithExtents(self%GetMTAtomInfocore_overlapExtents())
        end subroutine
        subroutine DisconnectMTAtomInfoObjectFields(self)
                class(MTAtomInfo), intent(inout) :: self
               type(iterator) :: iter
                call self%l%DisconnectFromStore()
                call self%energy%DisconnectFromStore()
                call self%core_overlap%DisconnectFromStore()
        end subroutine
        subroutine StoreMTAtomInfoScalarFields(self)
                class(MTAtomInfo), intent(inout) :: self
                call self%write('num_orbitals', self%num_orbitals)
        end subroutine
        subroutine LoadMTAtomInfoScalarFields(self)
                class(MTAtomInfo), intent(inout) :: self
                call self%read('num_orbitals', self%num_orbitals)
        end subroutine
        subroutine FinalizeMTAtomInfo(self)
               type(MTAtomInfo), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearMTAtomInfo(self)
                class(MTAtomInfo), intent(inout) :: self
                type(MTAtomInfo), save :: empty
                self = empty
        end subroutine
        pure elemental function IsMTAtomInfoEqual(lhs, rhs) result(iseq)
                class(MTAtomInfo), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (MTAtomInfo)
                       iseq = iseq .and. (lhs%num_orbitals == rhs%num_orbitals)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%l == rhs%l)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%energy == rhs%energy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%core_overlap == rhs%core_overlap)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorMTAtomInfo(lhs, rhs)
                class(MTAtomInfo), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (MTAtomInfo)
                       lhs%num_orbitals = rhs%num_orbitals
                       lhs%l = rhs%l
                       lhs%energy = rhs%energy
                       lhs%core_overlap = rhs%core_overlap
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetMtatominfoLExtents(self) result(res)
                class(MTAtomInfo), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_orbitals)]
        end function
        function GetMtatominfoEnergyExtents(self) result(res)
                class(MTAtomInfo), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_orbitals)]
        end function
        function GetMtatominfoCore_overlapExtents(self) result(res)
                class(MTAtomInfo), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_orbitals)]
        end function
        subroutine AllocateMTAtomInfoObjectFields(self)
                class(MTAtomInfo), intent(inout) :: self
                call self%l%init(int(self%num_orbitals))
                call self%energy%init(int(self%num_orbitals))
                call self%core_overlap%init(int(self%num_orbitals))
        end subroutine


        subroutine InitBasisMTInfo(self)
                class(BasisMTInfo), intent(inout) :: self
                call self%InitPersistent()
                self%num_unique_atoms =  0
        end subroutine
        subroutine StoreBasisMTInfoObjectFields(self)
                class(BasisMTInfo), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                call iter%Init(lbound(self%atoms), ubound(self%atoms))
                do while (.not. iter%Done())
                    call self%atoms(iter%i(1))%store( &
                                ps%FetchIndexInSubGroup(gid, 'atoms', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine LoadBasisMTInfoObjectFields(self)
                class(BasisMTInfo), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()

                allocate(self%atoms(int(1):int(self%num_unique_atoms)))
                call iter%Init(lbound(self%atoms), ubound(self%atoms))
                do while (.not. iter%Done())
                    call self%atoms(iter%i(1))%load( &
                                ps%FetchIndexInSubGroup(gid, 'atoms', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
        end subroutine
        subroutine ResetBasisMTInfoSectionFields(self)
                class(BasisMTInfo), intent(inout) :: self
        end subroutine
        subroutine DisconnectBasisMTInfoObjectFields(self)
                class(BasisMTInfo), intent(inout) :: self
               type(iterator) :: iter

                call iter%Init(lbound(self%atoms), ubound(self%atoms))
                do while (.not. iter%Done())
                    call self%atoms(iter%i(1))%disconnect()
                    call iter%Inc()
                end do
        end subroutine
        subroutine StoreBasisMTInfoScalarFields(self)
                class(BasisMTInfo), intent(inout) :: self
                call self%write('num_unique_atoms', self%num_unique_atoms)
        end subroutine
        subroutine LoadBasisMTInfoScalarFields(self)
                class(BasisMTInfo), intent(inout) :: self
                call self%read('num_unique_atoms', self%num_unique_atoms)
        end subroutine
        subroutine FinalizeBasisMTInfo(self)
               type(BasisMTInfo), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearBasisMTInfo(self)
                class(BasisMTInfo), intent(inout) :: self
                type(BasisMTInfo), save :: empty
                self = empty
        end subroutine
        pure elemental function IsBasisMTInfoEqual(lhs, rhs) result(iseq)
                class(BasisMTInfo), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (BasisMTInfo)
                       iseq = iseq .and. (lhs%num_unique_atoms == rhs%num_unique_atoms)
                       if (.not. iseq) return

                       iseq = iseq .and. (allocated(lhs%atoms) .eqv. allocated(rhs%atoms))
                       if (.not. iseq) return
                       if (allocated(lhs%atoms)) then
                           iseq = iseq .and. all(shape(lhs%atoms) == shape(rhs%atoms))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%atoms(:) == rhs%atoms(:))
                        end if
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorBasisMTInfo(lhs, rhs)
                class(BasisMTInfo), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (BasisMTInfo)
                       lhs%num_unique_atoms = rhs%num_unique_atoms
                       lhs%atoms = rhs%atoms
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        subroutine AllocateBasisMTInfoObjectFields(self)
                class(BasisMTInfo), intent(inout) :: self
                allocate(self%atoms(int(1):int(self%num_unique_atoms)))
        end subroutine


        subroutine InitExpansion(self)
                class(Expansion), intent(inout) :: self
                call self%InitPersistent()
                self%is_sharded = .False.
                self%shard_offset =  int(0,kind=int32)
                self%num_k =  0
                self%num_expanded =  0
                self%num_si =  int(1,kind=int32)
                self%max_num_pw =  0
                self%num_muffin_orbs =  0
        end subroutine
        subroutine StoreExpansionObjectFields(self)
                class(Expansion), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Z%StoreObject(ps, gid,  'Z')
        end subroutine
        subroutine LoadExpansionObjectFields(self)
                class(Expansion), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Z%LoadObject(ps, gid,  'Z')
        end subroutine
        subroutine ResetExpansionSectionFields(self)
                class(Expansion), intent(inout) :: self
                self%Z_ => self%Z%GetWithExtents(self%GetExpansionZExtents())
        end subroutine
        subroutine DisconnectExpansionObjectFields(self)
                class(Expansion), intent(inout) :: self
               type(iterator) :: iter
                call self%Z%DisconnectFromStore()
        end subroutine
        subroutine StoreExpansionScalarFields(self)
                class(Expansion), intent(inout) :: self
                call self%write('is_sharded', self%is_sharded)
                call self%write('shard_offset', self%shard_offset)
                call self%write('num_k', self%num_k)
                call self%write('num_expanded', self%num_expanded)
                call self%write('num_si', self%num_si)
                call self%write('max_num_pw', self%max_num_pw)
                call self%write('num_muffin_orbs', self%num_muffin_orbs)
        end subroutine
        subroutine LoadExpansionScalarFields(self)
                class(Expansion), intent(inout) :: self
                call self%read('is_sharded', self%is_sharded)
                call self%read('shard_offset', self%shard_offset)
                call self%read('num_k', self%num_k)
                call self%read('num_expanded', self%num_expanded)
                call self%read('num_si', self%num_si)
                call self%read('max_num_pw', self%max_num_pw)
                call self%read('num_muffin_orbs', self%num_muffin_orbs)
        end subroutine
        subroutine FinalizeExpansion(self)
               type(Expansion), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearExpansion(self)
                class(Expansion), intent(inout) :: self
                type(Expansion), save :: empty
                self = empty
        end subroutine
        pure elemental function IsExpansionEqual(lhs, rhs) result(iseq)
                class(Expansion), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Expansion)
                       iseq = iseq .and. (lhs%is_sharded .eqv. rhs%is_sharded)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%shard_offset == rhs%shard_offset)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_expanded == rhs%num_expanded)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_num_pw == rhs%max_num_pw)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_muffin_orbs == rhs%num_muffin_orbs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Z == rhs%Z)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorExpansion(lhs, rhs)
                class(Expansion), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Expansion)
                       lhs%is_sharded = rhs%is_sharded
                       lhs%shard_offset = rhs%shard_offset
                       lhs%num_k = rhs%num_k
                       lhs%num_expanded = rhs%num_expanded
                       lhs%num_si = rhs%num_si
                       lhs%max_num_pw = rhs%max_num_pw
                       lhs%num_muffin_orbs = rhs%num_muffin_orbs
                       lhs%Z = rhs%Z
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetExpansionZExtents(self) result(res)
                class(Expansion), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%num_muffin_orbs),extent(1,self%num_expanded),extent(1,self%num_k),extent(1,self%num_si)]
        end function
        subroutine AllocateExpansionObjectFields(self)
                class(Expansion), intent(inout) :: self
                call self%Z%init(int(self%num_muffin_orbs),int(self%num_expanded),int(self%num_k),int(self%num_si))
        end subroutine


        subroutine InitKPath(self)
                class(KPath), intent(inout) :: self
                call self%InitPersistent()
                self%num_k =  0
        end subroutine
        subroutine StoreKPathObjectFields(self)
                class(KPath), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%k%StoreObject(ps, gid,  'k')
        end subroutine
        subroutine LoadKPathObjectFields(self)
                class(KPath), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%k%LoadObject(ps, gid,  'k')
        end subroutine
        subroutine ResetKPathSectionFields(self)
                class(KPath), intent(inout) :: self
                self%k_ => self%k%GetWithExtents(self%GetKPathkExtents())
        end subroutine
        subroutine DisconnectKPathObjectFields(self)
                class(KPath), intent(inout) :: self
               type(iterator) :: iter
                call self%k%DisconnectFromStore()
        end subroutine
        subroutine StoreKPathScalarFields(self)
                class(KPath), intent(inout) :: self
                call self%write('num_k', self%num_k)
        end subroutine
        subroutine LoadKPathScalarFields(self)
                class(KPath), intent(inout) :: self
                call self%read('num_k', self%num_k)
        end subroutine
        subroutine FinalizeKPath(self)
               type(KPath), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearKPath(self)
                class(KPath), intent(inout) :: self
                type(KPath), save :: empty
                self = empty
        end subroutine
        pure elemental function IsKPathEqual(lhs, rhs) result(iseq)
                class(KPath), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (KPath)
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%k == rhs%k)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorKPath(lhs, rhs)
                class(KPath), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (KPath)
                       lhs%num_k = rhs%num_k
                       lhs%k = rhs%k
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetKpathKExtents(self) result(res)
                class(KPath), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_k)]
        end function
        subroutine AllocateKPathObjectFields(self)
                class(KPath), intent(inout) :: self
                call self%k%init(int(3),int(self%num_k))
        end subroutine


        subroutine InitBandStructure(self)
                class(BandStructure), intent(inout) :: self
                call self%Expansion%Init()
                self%chemical_potential =  0.0d0
        end subroutine
        subroutine StoreBandStructureObjectFields(self)
                class(BandStructure), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Expansion%StoreObjectFields()
                call self%num_bands%StoreObject(ps, gid,  'num_bands')
                call self%energy%StoreObject(ps, gid,  'energy')
                call self%Occ%StoreObject(ps, gid,  'Occ')
        end subroutine
        subroutine LoadBandStructureObjectFields(self)
                class(BandStructure), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Expansion%LoadObjectFields()
                call self%num_bands%LoadObject(ps, gid,  'num_bands')
                call self%energy%LoadObject(ps, gid,  'energy')
                call self%Occ%LoadObject(ps, gid,  'Occ')
        end subroutine
        subroutine ResetBandStructureSectionFields(self)
                class(BandStructure), intent(inout) :: self
                call self%Expansion%ResetSectionFields()
                self%num_bands_ => self%num_bands%GetWithExtents(self%GetBandStructurenum_bandsExtents())
                self%energy_ => self%energy%GetWithExtents(self%GetBandStructureenergyExtents())
                self%Occ_ => self%Occ%GetWithExtents(self%GetBandStructureOccExtents())
        end subroutine
        subroutine DisconnectBandStructureObjectFields(self)
                class(BandStructure), intent(inout) :: self
               type(iterator) :: iter
                call self%Expansion%DisconnectObjectFields()
                call self%num_bands%DisconnectFromStore()
                call self%energy%DisconnectFromStore()
                call self%Occ%DisconnectFromStore()
        end subroutine
        subroutine StoreBandStructureScalarFields(self)
                class(BandStructure), intent(inout) :: self
                call self%Expansion%StoreScalarFields()
                call self%write('chemical_potential', self%chemical_potential)
        end subroutine
        subroutine LoadBandStructureScalarFields(self)
                class(BandStructure), intent(inout) :: self
                call self%Expansion%LoadScalarFields()
                call self%read('chemical_potential', self%chemical_potential)
        end subroutine
        subroutine FinalizeBandStructure(self)
               type(BandStructure), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearBandStructure(self)
                class(BandStructure), intent(inout) :: self
                type(BandStructure), save :: empty
                self = empty
        end subroutine
        pure elemental function IsBandStructureEqual(lhs, rhs) result(iseq)
                class(BandStructure), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (BandStructure)
                       iseq = iseq .and. (lhs%Expansion == rhs%Expansion)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_bands == rhs%num_bands)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%energy == rhs%energy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Occ == rhs%Occ)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%chemical_potential == rhs%chemical_potential)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorBandStructure(lhs, rhs)
                class(BandStructure), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (BandStructure)
                       lhs%Expansion = rhs%Expansion
                       lhs%num_bands = rhs%num_bands
                       lhs%energy = rhs%energy
                       lhs%Occ = rhs%Occ
                       lhs%chemical_potential = rhs%chemical_potential
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetBandstructureNum_bandsExtents(self) result(res)
                class(BandStructure), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_k),extent(1,self%num_si)]
        end function
        function GetBandstructureEnergyExtents(self) result(res)
                class(BandStructure), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_expanded),extent(1,self%num_k),extent(1,self%num_si)]
        end function
        function GetBandstructureOccExtents(self) result(res)
                class(BandStructure), intent(inout) :: self
                type(extent) :: res(4)
                res = [extent(1,self%num_expanded),extent(1,self%num_expanded),extent(1,self%num_k),extent(1,self%num_si)]
        end function
        subroutine AllocateBandStructureObjectFields(self)
                class(BandStructure), intent(inout) :: self
                call self%Expansion%AllocateObjectFields()
                call self%num_bands%init(int(self%num_k),int(self%num_si))
                call self%energy%init(int(self%num_expanded),int(self%num_k),int(self%num_si))
                call self%Occ%init(int(self%num_expanded),int(self%num_expanded),int(self%num_k),int(self%num_si))
        end subroutine


        subroutine InitEquivalentAtomSymmetries(self)
                class(EquivalentAtomSymmetries), intent(inout) :: self
                call self%InitPersistent()
                self%num_atoms =  0
        end subroutine
        subroutine StoreEquivalentAtomSymmetriesObjectFields(self)
                class(EquivalentAtomSymmetries), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%g%StoreObject(ps, gid,  'g')
        end subroutine
        subroutine LoadEquivalentAtomSymmetriesObjectFields(self)
                class(EquivalentAtomSymmetries), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%g%LoadObject(ps, gid,  'g')
        end subroutine
        subroutine ResetEquivalentAtomSymmetriesSectionFields(self)
                class(EquivalentAtomSymmetries), intent(inout) :: self
                self%g_ => self%g%GetWithExtents(self%GetEquivalentAtomSymmetriesgExtents())
        end subroutine
        subroutine DisconnectEquivalentAtomSymmetriesObjectFields(self)
                class(EquivalentAtomSymmetries), intent(inout) :: self
               type(iterator) :: iter
                call self%g%DisconnectFromStore()
        end subroutine
        subroutine StoreEquivalentAtomSymmetriesScalarFields(self)
                class(EquivalentAtomSymmetries), intent(inout) :: self
                call self%write('num_atoms', self%num_atoms)
        end subroutine
        subroutine LoadEquivalentAtomSymmetriesScalarFields(self)
                class(EquivalentAtomSymmetries), intent(inout) :: self
                call self%read('num_atoms', self%num_atoms)
        end subroutine
        subroutine FinalizeEquivalentAtomSymmetries(self)
               type(EquivalentAtomSymmetries), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearEquivalentAtomSymmetries(self)
                class(EquivalentAtomSymmetries), intent(inout) :: self
                type(EquivalentAtomSymmetries), save :: empty
                self = empty
        end subroutine
        pure elemental function IsEquivalentAtomSymmetriesEqual(lhs, rhs) result(iseq)
                class(EquivalentAtomSymmetries), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (EquivalentAtomSymmetries)
                       iseq = iseq .and. (lhs%num_atoms == rhs%num_atoms)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%g == rhs%g)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorEquivalentAtomSymmetries(lhs, rhs)
                class(EquivalentAtomSymmetries), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (EquivalentAtomSymmetries)
                       lhs%num_atoms = rhs%num_atoms
                       lhs%g = rhs%g
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetEquivalentatomsymmetriesGExtents(self) result(res)
                class(EquivalentAtomSymmetries), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_atoms)]
        end function
        subroutine AllocateEquivalentAtomSymmetriesObjectFields(self)
                class(EquivalentAtomSymmetries), intent(inout) :: self
                call self%g%init(int(self%num_atoms))
        end subroutine



end module
