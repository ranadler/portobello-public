
module Transport
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: Input
        real(kind=dp)  ::  Temperature =  real(1.000000,kind=16)

        integer(kind=int32)  ::  maxpower =  int(2,kind=int32)

        logical  ::  symmetrize_bz = .False.

        logical  ::  interband = .False.

        real(kind=dp)  ::  gamma =  real(0.000500,kind=16)

        real(kind=dp)  ::  gamma_correlated =  real(0.000500,kind=16)

        real(kind=dp)  ::  delta =  real(0.001000,kind=16)

        real(kind=dp)  ::  omega_max =  real(0.150000,kind=16)

        real(kind=dp)  ::  energy_window =  real(0.100000,kind=16)

        integer(kind=int32)  ::  min_band =  int(1,kind=int32)

        integer(kind=int32)  ::  max_band =  int(1,kind=int32)

        integer(kind=int32)  ::  num_omega =  int(0,kind=int32)

        logical  ::  run_optics = .False.

        integer(kind=int32)  ::  num_dirs =  int(3,kind=int32)

        type(matrix_real)  ::  dirs
        real(kind=dp),pointer :: dirs_(:,:)


        contains
        procedure :: AllocateObjectFields => AllocateInputObjectFields
        procedure :: ResetSectionFields   => ResetInputSectionFields
        procedure :: StoreScalarFields    => StoreInputScalarFields
        procedure :: StoreObjectFields    => StoreInputObjectFields
        procedure :: LoadScalarFields     => LoadInputScalarFields
        procedure :: LoadObjectFields     => LoadInputObjectFields
        procedure :: DisconnectObjectFields => DisconnectInputObjectFields
        procedure :: IsEqual              => IsInputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorInput
        procedure :: clear                => ClearInput
        procedure :: init => InitInput
        final     :: FinalizeInput
        procedure :: GetInputDirsExtents

    end type
    interface
         function ComputeFromPortobelloI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    type, extends(persistent) :: WorkArea
        integer(kind=int32)  ::  num_dirs =  int(3,kind=int32)

        integer(kind=int32)  ::  maxpower =  int(2,kind=int32)

        integer(kind=int32)  ::  num_omega =  0

        type(cube_real)  ::  total
        real(kind=dp),pointer :: total_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateWorkAreaObjectFields
        procedure :: ResetSectionFields   => ResetWorkAreaSectionFields
        procedure :: StoreScalarFields    => StoreWorkAreaScalarFields
        procedure :: StoreObjectFields    => StoreWorkAreaObjectFields
        procedure :: LoadScalarFields     => LoadWorkAreaScalarFields
        procedure :: LoadObjectFields     => LoadWorkAreaObjectFields
        procedure :: DisconnectObjectFields => DisconnectWorkAreaObjectFields
        procedure :: IsEqual              => IsWorkAreaEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWorkArea
        procedure :: clear                => ClearWorkArea
        procedure :: init => InitWorkArea
        final     :: FinalizeWorkArea
        procedure :: GetWorkareaTotalExtents

    end type

    type, extends(Input) :: Output
        type(vector_real)  ::  omega
        real(kind=dp),pointer :: omega_(:)
        type(cube_real)  ::  A
        real(kind=dp),pointer :: A_(:,:,:)
        type(vector_real)  ::  sigma
        real(kind=dp),pointer :: sigma_(:)
        type(vector_real)  ::  A1
        real(kind=dp),pointer :: A1_(:)
        type(vector_real)  ::  A2
        real(kind=dp),pointer :: A2_(:)
        type(vector_real)  ::  Seebeck
        real(kind=dp),pointer :: Seebeck_(:)
        type(vector_real)  ::  kappa
        real(kind=dp),pointer :: kappa_(:)
        type(vector_real)  ::  ZT
        real(kind=dp),pointer :: ZT_(:)


        contains
        procedure :: AllocateObjectFields => AllocateOutputObjectFields
        procedure :: ResetSectionFields   => ResetOutputSectionFields
        procedure :: StoreScalarFields    => StoreOutputScalarFields
        procedure :: StoreObjectFields    => StoreOutputObjectFields
        procedure :: LoadScalarFields     => LoadOutputScalarFields
        procedure :: LoadObjectFields     => LoadOutputObjectFields
        procedure :: DisconnectObjectFields => DisconnectOutputObjectFields
        procedure :: IsEqual              => IsOutputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOutput
        procedure :: clear                => ClearOutput
        procedure :: init => InitOutput
        final     :: FinalizeOutput
        procedure :: GetOutputOmegaExtents
        procedure :: GetOutputAExtents
        procedure :: GetOutputSigmaExtents
        procedure :: GetOutputA1Extents
        procedure :: GetOutputA2Extents
        procedure :: GetOutputSeebeckExtents
        procedure :: GetOutputKappaExtents
        procedure :: GetOutputZtExtents

    end type

    contains
        subroutine InitInput(self)
                class(Input), intent(inout) :: self
                call self%InitPersistent()
                self%Temperature =  real(1.000000,kind=16)
                self%maxpower =  int(2,kind=int32)
                self%symmetrize_bz = .False.
                self%interband = .False.
                self%gamma =  real(0.000500,kind=16)
                self%gamma_correlated =  real(0.000500,kind=16)
                self%delta =  real(0.001000,kind=16)
                self%omega_max =  real(0.150000,kind=16)
                self%energy_window =  real(0.100000,kind=16)
                self%min_band =  int(1,kind=int32)
                self%max_band =  int(1,kind=int32)
                self%num_omega =  int(0,kind=int32)
                self%run_optics = .False.
                self%num_dirs =  int(3,kind=int32)
        end subroutine
        subroutine StoreInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%dirs%StoreObject(ps, gid,  'dirs')
        end subroutine
        subroutine LoadInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%dirs%LoadObject(ps, gid,  'dirs')
        end subroutine
        subroutine ResetInputSectionFields(self)
                class(Input), intent(inout) :: self
                self%dirs_ => self%dirs%GetWithExtents(self%GetInputdirsExtents())
        end subroutine
        subroutine DisconnectInputObjectFields(self)
                class(Input), intent(inout) :: self
               type(iterator) :: iter
                call self%dirs%DisconnectFromStore()
        end subroutine
        subroutine StoreInputScalarFields(self)
                class(Input), intent(inout) :: self
                call self%write('Temperature', self%Temperature)
                call self%write('maxpower', self%maxpower)
                call self%write('symmetrize_bz', self%symmetrize_bz)
                call self%write('interband', self%interband)
                call self%write('gamma', self%gamma)
                call self%write('gamma_correlated', self%gamma_correlated)
                call self%write('delta', self%delta)
                call self%write('omega_max', self%omega_max)
                call self%write('energy_window', self%energy_window)
                call self%write('min_band', self%min_band)
                call self%write('max_band', self%max_band)
                call self%write('num_omega', self%num_omega)
                call self%write('run_optics', self%run_optics)
                call self%write('num_dirs', self%num_dirs)
        end subroutine
        subroutine LoadInputScalarFields(self)
                class(Input), intent(inout) :: self
                call self%read('Temperature', self%Temperature)
                call self%read('maxpower', self%maxpower)
                call self%read('symmetrize_bz', self%symmetrize_bz)
                call self%read('interband', self%interband)
                call self%read('gamma', self%gamma)
                call self%read('gamma_correlated', self%gamma_correlated)
                call self%read('delta', self%delta)
                call self%read('omega_max', self%omega_max)
                call self%read('energy_window', self%energy_window)
                call self%read('min_band', self%min_band)
                call self%read('max_band', self%max_band)
                call self%read('num_omega', self%num_omega)
                call self%read('run_optics', self%run_optics)
                call self%read('num_dirs', self%num_dirs)
        end subroutine
        subroutine FinalizeInput(self)
               type(Input), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearInput(self)
                class(Input), intent(inout) :: self
                type(Input), save :: empty
                self = empty
        end subroutine
        pure elemental function IsInputEqual(lhs, rhs) result(iseq)
                class(Input), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Input)
                       iseq = iseq .and. (lhs%Temperature == rhs%Temperature)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxpower == rhs%maxpower)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%symmetrize_bz .eqv. rhs%symmetrize_bz)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%interband .eqv. rhs%interband)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%gamma == rhs%gamma)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%gamma_correlated == rhs%gamma_correlated)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%delta == rhs%delta)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega_max == rhs%omega_max)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%energy_window == rhs%energy_window)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%min_band == rhs%min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_band == rhs%max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_omega == rhs%num_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%run_optics .eqv. rhs%run_optics)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_dirs == rhs%num_dirs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dirs == rhs%dirs)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorInput(lhs, rhs)
                class(Input), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Input)
                       lhs%Temperature = rhs%Temperature
                       lhs%maxpower = rhs%maxpower
                       lhs%symmetrize_bz = rhs%symmetrize_bz
                       lhs%interband = rhs%interband
                       lhs%gamma = rhs%gamma
                       lhs%gamma_correlated = rhs%gamma_correlated
                       lhs%delta = rhs%delta
                       lhs%omega_max = rhs%omega_max
                       lhs%energy_window = rhs%energy_window
                       lhs%min_band = rhs%min_band
                       lhs%max_band = rhs%max_band
                       lhs%num_omega = rhs%num_omega
                       lhs%run_optics = rhs%run_optics
                       lhs%num_dirs = rhs%num_dirs
                       lhs%dirs = rhs%dirs
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetInputDirsExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,3),extent(1,self%num_dirs)]
        end function
        subroutine AllocateInputObjectFields(self)
                class(Input), intent(inout) :: self
                call self%dirs%init(int(3),int(self%num_dirs))
        end subroutine


        subroutine ComputeFromPortobelloWrapper(ret, imp_fp, selfpath) bind(C,name='transport_mp_computefromportobellowrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(ComputeFromPortobelloI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine InitWorkArea(self)
                class(WorkArea), intent(inout) :: self
                call self%InitPersistent()
                self%num_dirs =  int(3,kind=int32)
                self%maxpower =  int(2,kind=int32)
                self%num_omega =  0
        end subroutine
        subroutine StoreWorkAreaObjectFields(self)
                class(WorkArea), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%total%StoreObject(ps, gid,  'total')
        end subroutine
        subroutine LoadWorkAreaObjectFields(self)
                class(WorkArea), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%total%LoadObject(ps, gid,  'total')
        end subroutine
        subroutine ResetWorkAreaSectionFields(self)
                class(WorkArea), intent(inout) :: self
                self%total_ => self%total%GetWithExtents(self%GetWorkAreatotalExtents())
        end subroutine
        subroutine DisconnectWorkAreaObjectFields(self)
                class(WorkArea), intent(inout) :: self
               type(iterator) :: iter
                call self%total%DisconnectFromStore()
        end subroutine
        subroutine StoreWorkAreaScalarFields(self)
                class(WorkArea), intent(inout) :: self
                call self%write('num_dirs', self%num_dirs)
                call self%write('maxpower', self%maxpower)
                call self%write('num_omega', self%num_omega)
        end subroutine
        subroutine LoadWorkAreaScalarFields(self)
                class(WorkArea), intent(inout) :: self
                call self%read('num_dirs', self%num_dirs)
                call self%read('maxpower', self%maxpower)
                call self%read('num_omega', self%num_omega)
        end subroutine
        subroutine FinalizeWorkArea(self)
               type(WorkArea), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWorkArea(self)
                class(WorkArea), intent(inout) :: self
                type(WorkArea), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWorkAreaEqual(lhs, rhs) result(iseq)
                class(WorkArea), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (WorkArea)
                       iseq = iseq .and. (lhs%num_dirs == rhs%num_dirs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxpower == rhs%maxpower)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_omega == rhs%num_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%total == rhs%total)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWorkArea(lhs, rhs)
                class(WorkArea), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (WorkArea)
                       lhs%num_dirs = rhs%num_dirs
                       lhs%maxpower = rhs%maxpower
                       lhs%num_omega = rhs%num_omega
                       lhs%total = rhs%total
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetWorkareaTotalExtents(self) result(res)
                class(WorkArea), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_dirs),extent(0,self%maxpower),extent(0,self%num_omega)]
        end function
        subroutine AllocateWorkAreaObjectFields(self)
                class(WorkArea), intent(inout) :: self
                call self%total%init(int(self%num_dirs),int(self%maxpower- (0) + 1),int(self%num_omega- (0) + 1))
        end subroutine


        subroutine InitOutput(self)
                class(Output), intent(inout) :: self
                call self%Input%Init()
        end subroutine
        subroutine StoreOutputObjectFields(self)
                class(Output), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Input%StoreObjectFields()
                call self%omega%StoreObject(ps, gid,  'omega')
                call self%A%StoreObject(ps, gid,  'A')
                call self%sigma%StoreObject(ps, gid,  'sigma')
                call self%A1%StoreObject(ps, gid,  'A1')
                call self%A2%StoreObject(ps, gid,  'A2')
                call self%Seebeck%StoreObject(ps, gid,  'Seebeck')
                call self%kappa%StoreObject(ps, gid,  'kappa')
                call self%ZT%StoreObject(ps, gid,  'ZT')
        end subroutine
        subroutine LoadOutputObjectFields(self)
                class(Output), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Input%LoadObjectFields()
                call self%omega%LoadObject(ps, gid,  'omega')
                call self%A%LoadObject(ps, gid,  'A')
                call self%sigma%LoadObject(ps, gid,  'sigma')
                call self%A1%LoadObject(ps, gid,  'A1')
                call self%A2%LoadObject(ps, gid,  'A2')
                call self%Seebeck%LoadObject(ps, gid,  'Seebeck')
                call self%kappa%LoadObject(ps, gid,  'kappa')
                call self%ZT%LoadObject(ps, gid,  'ZT')
        end subroutine
        subroutine ResetOutputSectionFields(self)
                class(Output), intent(inout) :: self
                call self%Input%ResetSectionFields()
                self%omega_ => self%omega%GetWithExtents(self%GetOutputomegaExtents())
                self%A_ => self%A%GetWithExtents(self%GetOutputAExtents())
                self%sigma_ => self%sigma%GetWithExtents(self%GetOutputsigmaExtents())
                self%A1_ => self%A1%GetWithExtents(self%GetOutputA1Extents())
                self%A2_ => self%A2%GetWithExtents(self%GetOutputA2Extents())
                self%Seebeck_ => self%Seebeck%GetWithExtents(self%GetOutputSeebeckExtents())
                self%kappa_ => self%kappa%GetWithExtents(self%GetOutputkappaExtents())
                self%ZT_ => self%ZT%GetWithExtents(self%GetOutputZTExtents())
        end subroutine
        subroutine DisconnectOutputObjectFields(self)
                class(Output), intent(inout) :: self
               type(iterator) :: iter
                call self%Input%DisconnectObjectFields()
                call self%omega%DisconnectFromStore()
                call self%A%DisconnectFromStore()
                call self%sigma%DisconnectFromStore()
                call self%A1%DisconnectFromStore()
                call self%A2%DisconnectFromStore()
                call self%Seebeck%DisconnectFromStore()
                call self%kappa%DisconnectFromStore()
                call self%ZT%DisconnectFromStore()
        end subroutine
        subroutine StoreOutputScalarFields(self)
                class(Output), intent(inout) :: self
                call self%Input%StoreScalarFields()
        end subroutine
        subroutine LoadOutputScalarFields(self)
                class(Output), intent(inout) :: self
                call self%Input%LoadScalarFields()
        end subroutine
        subroutine FinalizeOutput(self)
               type(Output), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOutput(self)
                class(Output), intent(inout) :: self
                type(Output), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOutputEqual(lhs, rhs) result(iseq)
                class(Output), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Output)
                       iseq = iseq .and. (lhs%Input == rhs%Input)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega == rhs%omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%A == rhs%A)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%sigma == rhs%sigma)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%A1 == rhs%A1)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%A2 == rhs%A2)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%Seebeck == rhs%Seebeck)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%kappa == rhs%kappa)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%ZT == rhs%ZT)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOutput(lhs, rhs)
                class(Output), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Output)
                       lhs%Input = rhs%Input
                       lhs%omega = rhs%omega
                       lhs%A = rhs%A
                       lhs%sigma = rhs%sigma
                       lhs%A1 = rhs%A1
                       lhs%A2 = rhs%A2
                       lhs%Seebeck = rhs%Seebeck
                       lhs%kappa = rhs%kappa
                       lhs%ZT = rhs%ZT
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetOutputOmegaExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(0,self%num_omega)]
        end function
        function GetOutputAExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(0,self%num_omega),extent(1,self%num_dirs),extent(0,self%maxpower)]
        end function
        function GetOutputSigmaExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_dirs)]
        end function
        function GetOutputA1Extents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_dirs)]
        end function
        function GetOutputA2Extents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_dirs)]
        end function
        function GetOutputSeebeckExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_dirs)]
        end function
        function GetOutputKappaExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_dirs)]
        end function
        function GetOutputZtExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(1,self%num_dirs)]
        end function
        subroutine AllocateOutputObjectFields(self)
                class(Output), intent(inout) :: self
                call self%Input%AllocateObjectFields()
                call self%omega%init(int(self%num_omega- (0) + 1))
                call self%A%init(int(self%num_omega- (0) + 1),int(self%num_dirs),int(self%maxpower- (0) + 1))
                call self%sigma%init(int(self%num_dirs))
                call self%A1%init(int(self%num_dirs))
                call self%A2%init(int(self%num_dirs))
                call self%Seebeck%init(int(self%num_dirs))
                call self%kappa%init(int(self%num_dirs))
                call self%ZT%init(int(self%num_dirs))
        end subroutine



end module
