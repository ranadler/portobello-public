
module TransportOptics
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

    use array5
    use cube
    use matrix
    use vector


    implicit none
    public

    type, extends(persistent) :: Velocities
        integer(kind=int32)  ::  num_expanded =  0

        integer(kind=int32)  ::  num_si =  0

        integer(kind=int32)  ::  num_k =  0

        type(array5_complex)  ::  data
        complex(kind=dp),pointer :: data_(:,:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateVelocitiesObjectFields
        procedure :: ResetSectionFields   => ResetVelocitiesSectionFields
        procedure :: StoreScalarFields    => StoreVelocitiesScalarFields
        procedure :: StoreObjectFields    => StoreVelocitiesObjectFields
        procedure :: LoadScalarFields     => LoadVelocitiesScalarFields
        procedure :: LoadObjectFields     => LoadVelocitiesObjectFields
        procedure :: DisconnectObjectFields => DisconnectVelocitiesObjectFields
        procedure :: IsEqual              => IsVelocitiesEqual
        procedure :: AssignmentOperator   => AssignmentOperatorVelocities
        procedure :: clear                => ClearVelocities
        procedure :: init => InitVelocities
#ifndef __GFORTRAN__
        final     :: FinalizeVelocities
#endif
        procedure :: GetVelocitiesDataExtents

    end type

    type, extends(persistent) :: Input
        real(kind=dp)  ::  Temperature =  real(1.000000,kind=16)

        integer(kind=int32)  ::  num_omega =  int(1,kind=int32)

        logical  ::  symmetrize_bz = .False.

        real(kind=dp)  ::  gamm =  real(0.000500,kind=16)

        real(kind=dp)  ::  gammc =  real(0.000500,kind=16)

        real(kind=dp)  ::  delta =  real(0.001000,kind=16)

        real(kind=dp)  ::  omega_max =  real(0.150000,kind=16)

        integer(kind=int32)  ::  max_omega_mesh =  int(0,kind=int32)

        integer(kind=int32)  ::  active_omega_points =  int(1,kind=int32)

        integer(kind=int32)  ::  min_band =  int(1,kind=int32)

        integer(kind=int32)  ::  max_band =  int(1,kind=int32)

        type(string)  ::  momentum_kind
        logical  ::  test_velocity = .False.

        integer(kind=int32)  ::  num_dirs =  int(3,kind=int32)

        type(cube_real)  ::  dirs
        real(kind=dp),pointer :: dirs_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateInputObjectFields
        procedure :: ResetSectionFields   => ResetInputSectionFields
        procedure :: StoreScalarFields    => StoreInputScalarFields
        procedure :: StoreObjectFields    => StoreInputObjectFields
        procedure :: LoadScalarFields     => LoadInputScalarFields
        procedure :: LoadObjectFields     => LoadInputObjectFields
        procedure :: DisconnectObjectFields => DisconnectInputObjectFields
        procedure :: IsEqual              => IsInputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorInput
        procedure :: clear                => ClearInput
        procedure :: init => InitInput
#ifndef __GFORTRAN__
        final     :: FinalizeInput
#endif
        procedure :: GetInputDirsExtents

    end type
    interface
         function ComputeFromPortobelloI(selfpath) result(res)
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(string), intent(in) :: selfpath
         end function
    end interface


    type, extends(persistent) :: GreensFunction
        integer(kind=int32)  ::  max_omega =  0

        integer(kind=int32)  ::  num_basis =  0

        integer(kind=int32)  ::  num_correlated =  0

        type(vector_real)  ::  w
        real(kind=dp),pointer :: w_(:)
        type(vector_real)  ::  a
        real(kind=dp),pointer :: a_(:)
        type(vector_real)  ::  b
        real(kind=dp),pointer :: b_(:)
        type(matrix_complex)  ::  HPlus
        complex(kind=dp),pointer :: HPlus_(:,:)
        type(cube_complex)  ::  L
        complex(kind=dp),pointer :: L_(:,:,:)
        type(cube_complex)  ::  R
        complex(kind=dp),pointer :: R_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateGreensFunctionObjectFields
        procedure :: ResetSectionFields   => ResetGreensFunctionSectionFields
        procedure :: StoreScalarFields    => StoreGreensFunctionScalarFields
        procedure :: StoreObjectFields    => StoreGreensFunctionObjectFields
        procedure :: LoadScalarFields     => LoadGreensFunctionScalarFields
        procedure :: LoadObjectFields     => LoadGreensFunctionObjectFields
        procedure :: DisconnectObjectFields => DisconnectGreensFunctionObjectFields
        procedure :: IsEqual              => IsGreensFunctionEqual
        procedure :: AssignmentOperator   => AssignmentOperatorGreensFunction
        procedure :: clear                => ClearGreensFunction
        procedure :: init => InitGreensFunction
#ifndef __GFORTRAN__
        final     :: FinalizeGreensFunction
#endif
        procedure :: GetGreensfunctionWExtents
        procedure :: GetGreensfunctionAExtents
        procedure :: GetGreensfunctionBExtents
        procedure :: GetGreensfunctionHplusExtents
        procedure :: GetGreensfunctionLExtents
        procedure :: GetGreensfunctionRExtents

    end type

    type, extends(persistent) :: WorkArea
        integer(kind=int32)  ::  num_dirs =  int(3,kind=int32)

        integer(kind=int32)  ::  maxpower =  int(2,kind=int32)

        integer(kind=int32)  ::  num_omega =  0

        type(cube_real)  ::  total
        real(kind=dp),pointer :: total_(:,:,:)
        real(kind=dp)  ::  total_weight =  0.0d0



        contains
        procedure :: AllocateObjectFields => AllocateWorkAreaObjectFields
        procedure :: ResetSectionFields   => ResetWorkAreaSectionFields
        procedure :: StoreScalarFields    => StoreWorkAreaScalarFields
        procedure :: StoreObjectFields    => StoreWorkAreaObjectFields
        procedure :: LoadScalarFields     => LoadWorkAreaScalarFields
        procedure :: LoadObjectFields     => LoadWorkAreaObjectFields
        procedure :: DisconnectObjectFields => DisconnectWorkAreaObjectFields
        procedure :: IsEqual              => IsWorkAreaEqual
        procedure :: AssignmentOperator   => AssignmentOperatorWorkArea
        procedure :: clear                => ClearWorkArea
        procedure :: init => InitWorkArea
#ifndef __GFORTRAN__
        final     :: FinalizeWorkArea
#endif
        procedure :: GetWorkareaTotalExtents

    end type

    type, extends(persistent) :: IntegrandForDebug
        integer(kind=int32)  ::  max_omega =  0

        type(vector_real)  ::  omega
        real(kind=dp),pointer :: omega_(:)
        type(vector_complex)  ::  G
        complex(kind=dp),pointer :: G_(:)


        contains
        procedure :: AllocateObjectFields => AllocateIntegrandForDebugObjectFields
        procedure :: ResetSectionFields   => ResetIntegrandForDebugSectionFields
        procedure :: StoreScalarFields    => StoreIntegrandForDebugScalarFields
        procedure :: StoreObjectFields    => StoreIntegrandForDebugObjectFields
        procedure :: LoadScalarFields     => LoadIntegrandForDebugScalarFields
        procedure :: LoadObjectFields     => LoadIntegrandForDebugObjectFields
        procedure :: DisconnectObjectFields => DisconnectIntegrandForDebugObjectFields
        procedure :: IsEqual              => IsIntegrandForDebugEqual
        procedure :: AssignmentOperator   => AssignmentOperatorIntegrandForDebug
        procedure :: clear                => ClearIntegrandForDebug
        procedure :: init => InitIntegrandForDebug
#ifndef __GFORTRAN__
        final     :: FinalizeIntegrandForDebug
#endif
        procedure :: GetIntegrandfordebugOmegaExtents
        procedure :: GetIntegrandfordebugGExtents

    end type

    type, extends(persistent) :: EmbeddedSelfEnergyOnRealOmega
        integer(kind=int32)  ::  min_band =  0

        integer(kind=int32)  ::  max_band =  0

        integer(kind=int32)  ::  shard_start =  0

        integer(kind=int32)  ::  shard_end =  0

        integer(kind=int32)  ::  num_si =  0

        integer(kind=int32)  ::  max_omega =  0

        type(vector_real)  ::  omegaRy
        real(kind=dp),pointer :: omegaRy_(:)
        type(vector_int)  ::  actual_k
        integer(kind=int32),pointer :: actual_k_(:)
        type(array5_complex)  ::  M
        complex(kind=dp),pointer :: M_(:,:,:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateEmbeddedSelfEnergyOnRealOmegaObjectFields
        procedure :: ResetSectionFields   => ResetEmbeddedSelfEnergyOnRealOmegaSectionFields
        procedure :: StoreScalarFields    => StoreEmbeddedSelfEnergyOnRealOmegaScalarFields
        procedure :: StoreObjectFields    => StoreEmbeddedSelfEnergyOnRealOmegaObjectFields
        procedure :: LoadScalarFields     => LoadEmbeddedSelfEnergyOnRealOmegaScalarFields
        procedure :: LoadObjectFields     => LoadEmbeddedSelfEnergyOnRealOmegaObjectFields
        procedure :: DisconnectObjectFields => DisconnectEmbeddedSelfEnergyOnRealOmegaObjectFields
        procedure :: IsEqual              => IsEmbeddedSelfEnergyOnRealOmegaEqual
        procedure :: AssignmentOperator   => AssignmentOperatorEmbeddedSelfEnergyOnRealOmega
        procedure :: clear                => ClearEmbeddedSelfEnergyOnRealOmega
        procedure :: init => InitEmbeddedSelfEnergyOnRealOmega
#ifndef __GFORTRAN__
        final     :: FinalizeEmbeddedSelfEnergyOnRealOmega
#endif
        procedure :: GetEmbeddedselfenergyonrealomegaOmegaryExtents
        procedure :: GetEmbeddedselfenergyonrealomegaActual_kExtents
        procedure :: GetEmbeddedselfenergyonrealomegaMExtents

    end type

    type, extends(Input) :: Output
        integer(kind=int32)  ::  maxpower =  int(2,kind=int32)

        type(vector_real)  ::  omega
        real(kind=dp),pointer :: omega_(:)
        type(cube_real)  ::  A
        real(kind=dp),pointer :: A_(:,:,:)


        contains
        procedure :: AllocateObjectFields => AllocateOutputObjectFields
        procedure :: ResetSectionFields   => ResetOutputSectionFields
        procedure :: StoreScalarFields    => StoreOutputScalarFields
        procedure :: StoreObjectFields    => StoreOutputObjectFields
        procedure :: LoadScalarFields     => LoadOutputScalarFields
        procedure :: LoadObjectFields     => LoadOutputObjectFields
        procedure :: DisconnectObjectFields => DisconnectOutputObjectFields
        procedure :: IsEqual              => IsOutputEqual
        procedure :: AssignmentOperator   => AssignmentOperatorOutput
        procedure :: clear                => ClearOutput
        procedure :: init => InitOutput
#ifndef __GFORTRAN__
        final     :: FinalizeOutput
#endif
        procedure :: GetOutputOmegaExtents
        procedure :: GetOutputAExtents

    end type

    contains
        subroutine InitVelocities(self)
                class(Velocities), intent(inout) :: self
                call self%InitPersistent()
                self%num_expanded =  0
                self%num_si =  0
                self%num_k =  0
        end subroutine
        subroutine StoreVelocitiesObjectFields(self)
                class(Velocities), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%data%StoreObject(ps, gid,  'data')
        end subroutine
        subroutine LoadVelocitiesObjectFields(self)
                class(Velocities), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%data%LoadObject(ps, gid,  'data')
        end subroutine
        subroutine ResetVelocitiesSectionFields(self)
                class(Velocities), intent(inout) :: self
                self%data_ => self%data%GetWithExtents(self%GetVelocitiesdataExtents())
        end subroutine
        subroutine DisconnectVelocitiesObjectFields(self)
                class(Velocities), intent(inout) :: self
               type(iterator) :: iter
                call self%data%DisconnectFromStore()
        end subroutine
        subroutine StoreVelocitiesScalarFields(self)
                class(Velocities), intent(inout) :: self
                call self%write('num_expanded', self%num_expanded)
                call self%write('num_si', self%num_si)
                call self%write('num_k', self%num_k)
        end subroutine
        subroutine LoadVelocitiesScalarFields(self)
                class(Velocities), intent(inout) :: self
                call self%read('num_expanded', self%num_expanded)
                call self%read('num_si', self%num_si)
                call self%read('num_k', self%num_k)
        end subroutine
        subroutine FinalizeVelocities(self)
               type(Velocities), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearVelocities(self)
                class(Velocities), intent(inout) :: self
                type(Velocities), save :: empty
                self = empty
        end subroutine
        pure elemental function IsVelocitiesEqual(lhs, rhs) result(iseq)
                class(Velocities), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Velocities)
                       iseq = iseq .and. (lhs%num_expanded == rhs%num_expanded)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_k == rhs%num_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%data == rhs%data)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorVelocities(lhs, rhs)
                class(Velocities), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Velocities)
                       lhs%num_expanded = rhs%num_expanded
                       lhs%num_si = rhs%num_si
                       lhs%num_k = rhs%num_k
                       lhs%data = rhs%data
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetVelocitiesDataExtents(self) result(res)
                class(Velocities), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(1,self%num_k),extent(1,self%num_si),extent(1,self%num_expanded),extent(1,self%num_expanded),extent(1,3)]
        end function
        subroutine AllocateVelocitiesObjectFields(self)
                class(Velocities), intent(inout) :: self
                call self%data%init(int(self%num_k),int(self%num_si),int(self%num_expanded),int(self%num_expanded),int(3))
        end subroutine


        subroutine InitInput(self)
                class(Input), intent(inout) :: self
                call self%InitPersistent()
                self%Temperature =  real(1.000000,kind=16)
                self%num_omega =  int(1,kind=int32)
                self%symmetrize_bz = .False.
                self%gamm =  real(0.000500,kind=16)
                self%gammc =  real(0.000500,kind=16)
                self%delta =  real(0.001000,kind=16)
                self%omega_max =  real(0.150000,kind=16)
                self%max_omega_mesh =  int(0,kind=int32)
                self%active_omega_points =  int(1,kind=int32)
                self%min_band =  int(1,kind=int32)
                self%max_band =  int(1,kind=int32)
                self%momentum_kind = "gradient"
                self%test_velocity = .False.
                self%num_dirs =  int(3,kind=int32)
        end subroutine
        subroutine StoreInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%dirs%StoreObject(ps, gid,  'dirs')
        end subroutine
        subroutine LoadInputObjectFields(self)
                class(Input), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%dirs%LoadObject(ps, gid,  'dirs')
        end subroutine
        subroutine ResetInputSectionFields(self)
                class(Input), intent(inout) :: self
                self%dirs_ => self%dirs%GetWithExtents(self%GetInputdirsExtents())
        end subroutine
        subroutine DisconnectInputObjectFields(self)
                class(Input), intent(inout) :: self
               type(iterator) :: iter
                call self%dirs%DisconnectFromStore()
        end subroutine
        subroutine StoreInputScalarFields(self)
                class(Input), intent(inout) :: self
                call self%write('Temperature', self%Temperature)
                call self%write('num_omega', self%num_omega)
                call self%write('symmetrize_bz', self%symmetrize_bz)
                call self%write('gamm', self%gamm)
                call self%write('gammc', self%gammc)
                call self%write('delta', self%delta)
                call self%write('omega_max', self%omega_max)
                call self%write('max_omega_mesh', self%max_omega_mesh)
                call self%write('active_omega_points', self%active_omega_points)
                call self%write('min_band', self%min_band)
                call self%write('max_band', self%max_band)
                call self%write('momentum_kind', self%momentum_kind)
                call self%write('test_velocity', self%test_velocity)
                call self%write('num_dirs', self%num_dirs)
        end subroutine
        subroutine LoadInputScalarFields(self)
                class(Input), intent(inout) :: self
                call self%read('Temperature', self%Temperature)
                call self%read('num_omega', self%num_omega)
                call self%read('symmetrize_bz', self%symmetrize_bz)
                call self%read('gamm', self%gamm)
                call self%read('gammc', self%gammc)
                call self%read('delta', self%delta)
                call self%read('omega_max', self%omega_max)
                call self%read('max_omega_mesh', self%max_omega_mesh)
                call self%read('active_omega_points', self%active_omega_points)
                call self%read('min_band', self%min_band)
                call self%read('max_band', self%max_band)
                call self%read('momentum_kind', self%momentum_kind)
                call self%read('test_velocity', self%test_velocity)
                call self%read('num_dirs', self%num_dirs)
        end subroutine
        subroutine FinalizeInput(self)
               type(Input), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearInput(self)
                class(Input), intent(inout) :: self
                type(Input), save :: empty
                self = empty
        end subroutine
        pure elemental function IsInputEqual(lhs, rhs) result(iseq)
                class(Input), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Input)
                       iseq = iseq .and. (lhs%Temperature == rhs%Temperature)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_omega == rhs%num_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%symmetrize_bz .eqv. rhs%symmetrize_bz)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%gamm == rhs%gamm)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%gammc == rhs%gammc)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%delta == rhs%delta)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega_max == rhs%omega_max)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_omega_mesh == rhs%max_omega_mesh)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%active_omega_points == rhs%active_omega_points)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%min_band == rhs%min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_band == rhs%max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%momentum_kind == rhs%momentum_kind)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%test_velocity .eqv. rhs%test_velocity)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_dirs == rhs%num_dirs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%dirs == rhs%dirs)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorInput(lhs, rhs)
                class(Input), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Input)
                       lhs%Temperature = rhs%Temperature
                       lhs%num_omega = rhs%num_omega
                       lhs%symmetrize_bz = rhs%symmetrize_bz
                       lhs%gamm = rhs%gamm
                       lhs%gammc = rhs%gammc
                       lhs%delta = rhs%delta
                       lhs%omega_max = rhs%omega_max
                       lhs%max_omega_mesh = rhs%max_omega_mesh
                       lhs%active_omega_points = rhs%active_omega_points
                       lhs%min_band = rhs%min_band
                       lhs%max_band = rhs%max_band
                       lhs%momentum_kind = rhs%momentum_kind
                       lhs%test_velocity = rhs%test_velocity
                       lhs%num_dirs = rhs%num_dirs
                       lhs%dirs = rhs%dirs
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetInputDirsExtents(self) result(res)
                class(Input), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,3),extent(1,3),extent(1,self%num_dirs)]
        end function
        subroutine AllocateInputObjectFields(self)
                class(Input), intent(inout) :: self
                call self%dirs%init(int(3),int(3),int(self%num_dirs))
        end subroutine


        subroutine ComputeFromPortobelloWrapper(ret, imp_fp, selfpath) bind(C,name='transportoptics_mp_computefromportobellowrapper_')
                use iso_c_binding
                use stringifor
                use hdf5_base
                type(string) :: res
                type(c_ptr),intent(inout),dimension(1) :: ret
                type(c_funptr),intent(in),value        :: imp_fp
                type(c_ptr),intent(in),value           :: selfpath
            procedure(ComputeFromPortobelloI), pointer :: fort_imp_fp
            call c_f_procpointer(imp_fp, fort_imp_fp)
            res = fort_imp_fp( FromCStr(selfpath))
            ret(1)= NewCStr(res%chars())
        end subroutine

        subroutine InitGreensFunction(self)
                class(GreensFunction), intent(inout) :: self
                call self%InitPersistent()
                self%max_omega =  0
                self%num_basis =  0
                self%num_correlated =  0
        end subroutine
        subroutine StoreGreensFunctionObjectFields(self)
                class(GreensFunction), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%w%StoreObject(ps, gid,  'w')
                call self%a%StoreObject(ps, gid,  'a')
                call self%b%StoreObject(ps, gid,  'b')
                call self%HPlus%StoreObject(ps, gid,  'HPlus')
                call self%L%StoreObject(ps, gid,  'L')
                call self%R%StoreObject(ps, gid,  'R')
        end subroutine
        subroutine LoadGreensFunctionObjectFields(self)
                class(GreensFunction), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%w%LoadObject(ps, gid,  'w')
                call self%a%LoadObject(ps, gid,  'a')
                call self%b%LoadObject(ps, gid,  'b')
                call self%HPlus%LoadObject(ps, gid,  'HPlus')
                call self%L%LoadObject(ps, gid,  'L')
                call self%R%LoadObject(ps, gid,  'R')
        end subroutine
        subroutine ResetGreensFunctionSectionFields(self)
                class(GreensFunction), intent(inout) :: self
                self%w_ => self%w%GetWithExtents(self%GetGreensFunctionwExtents())
                self%a_ => self%a%GetWithExtents(self%GetGreensFunctionaExtents())
                self%b_ => self%b%GetWithExtents(self%GetGreensFunctionbExtents())
                self%HPlus_ => self%HPlus%GetWithExtents(self%GetGreensFunctionHPlusExtents())
                self%L_ => self%L%GetWithExtents(self%GetGreensFunctionLExtents())
                self%R_ => self%R%GetWithExtents(self%GetGreensFunctionRExtents())
        end subroutine
        subroutine DisconnectGreensFunctionObjectFields(self)
                class(GreensFunction), intent(inout) :: self
               type(iterator) :: iter
                call self%w%DisconnectFromStore()
                call self%a%DisconnectFromStore()
                call self%b%DisconnectFromStore()
                call self%HPlus%DisconnectFromStore()
                call self%L%DisconnectFromStore()
                call self%R%DisconnectFromStore()
        end subroutine
        subroutine StoreGreensFunctionScalarFields(self)
                class(GreensFunction), intent(inout) :: self
                call self%write('max_omega', self%max_omega)
                call self%write('num_basis', self%num_basis)
                call self%write('num_correlated', self%num_correlated)
        end subroutine
        subroutine LoadGreensFunctionScalarFields(self)
                class(GreensFunction), intent(inout) :: self
                call self%read('max_omega', self%max_omega)
                call self%read('num_basis', self%num_basis)
                call self%read('num_correlated', self%num_correlated)
        end subroutine
        subroutine FinalizeGreensFunction(self)
               type(GreensFunction), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearGreensFunction(self)
                class(GreensFunction), intent(inout) :: self
                type(GreensFunction), save :: empty
                self = empty
        end subroutine
        pure elemental function IsGreensFunctionEqual(lhs, rhs) result(iseq)
                class(GreensFunction), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (GreensFunction)
                       iseq = iseq .and. (lhs%max_omega == rhs%max_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_basis == rhs%num_basis)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_correlated == rhs%num_correlated)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%w == rhs%w)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%a == rhs%a)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%b == rhs%b)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%HPlus == rhs%HPlus)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%L == rhs%L)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%R == rhs%R)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorGreensFunction(lhs, rhs)
                class(GreensFunction), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (GreensFunction)
                       lhs%max_omega = rhs%max_omega
                       lhs%num_basis = rhs%num_basis
                       lhs%num_correlated = rhs%num_correlated
                       lhs%w = rhs%w
                       lhs%a = rhs%a
                       lhs%b = rhs%b
                       lhs%HPlus = rhs%HPlus
                       lhs%L = rhs%L
                       lhs%R = rhs%R
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetGreensfunctionWExtents(self) result(res)
                class(GreensFunction), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%max_omega,self%max_omega)]
        end function
        function GetGreensfunctionAExtents(self) result(res)
                class(GreensFunction), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%max_omega,self%max_omega)]
        end function
        function GetGreensfunctionBExtents(self) result(res)
                class(GreensFunction), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%max_omega,self%max_omega)]
        end function
        function GetGreensfunctionHplusExtents(self) result(res)
                class(GreensFunction), intent(inout) :: self
                type(extent) :: res(2)
                res = [extent(1,self%num_basis),extent(-self%max_omega,self%max_omega)]
        end function
        function GetGreensfunctionLExtents(self) result(res)
                class(GreensFunction), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_correlated),extent(1,self%num_correlated),extent(-self%max_omega,self%max_omega)]
        end function
        function GetGreensfunctionRExtents(self) result(res)
                class(GreensFunction), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_correlated),extent(1,self%num_correlated),extent(-self%max_omega,self%max_omega)]
        end function
        subroutine AllocateGreensFunctionObjectFields(self)
                class(GreensFunction), intent(inout) :: self
                call self%w%init(int(self%max_omega- (-self%max_omega) + 1))
                call self%a%init(int(self%max_omega- (-self%max_omega) + 1))
                call self%b%init(int(self%max_omega- (-self%max_omega) + 1))
                call self%HPlus%init(int(self%num_basis),int(self%max_omega- (-self%max_omega) + 1))
                call self%L%init(int(self%num_correlated),int(self%num_correlated),int(self%max_omega- (-self%max_omega) + 1))
                call self%R%init(int(self%num_correlated),int(self%num_correlated),int(self%max_omega- (-self%max_omega) + 1))
        end subroutine


        subroutine InitWorkArea(self)
                class(WorkArea), intent(inout) :: self
                call self%InitPersistent()
                self%num_dirs =  int(3,kind=int32)
                self%maxpower =  int(2,kind=int32)
                self%num_omega =  0
                self%total_weight =  0.0d0
        end subroutine
        subroutine StoreWorkAreaObjectFields(self)
                class(WorkArea), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%total%StoreObject(ps, gid,  'total')
        end subroutine
        subroutine LoadWorkAreaObjectFields(self)
                class(WorkArea), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%total%LoadObject(ps, gid,  'total')
        end subroutine
        subroutine ResetWorkAreaSectionFields(self)
                class(WorkArea), intent(inout) :: self
                self%total_ => self%total%GetWithExtents(self%GetWorkAreatotalExtents())
        end subroutine
        subroutine DisconnectWorkAreaObjectFields(self)
                class(WorkArea), intent(inout) :: self
               type(iterator) :: iter
                call self%total%DisconnectFromStore()
        end subroutine
        subroutine StoreWorkAreaScalarFields(self)
                class(WorkArea), intent(inout) :: self
                call self%write('num_dirs', self%num_dirs)
                call self%write('maxpower', self%maxpower)
                call self%write('num_omega', self%num_omega)
                call self%write('total_weight', self%total_weight)
        end subroutine
        subroutine LoadWorkAreaScalarFields(self)
                class(WorkArea), intent(inout) :: self
                call self%read('num_dirs', self%num_dirs)
                call self%read('maxpower', self%maxpower)
                call self%read('num_omega', self%num_omega)
                call self%read('total_weight', self%total_weight)
        end subroutine
        subroutine FinalizeWorkArea(self)
               type(WorkArea), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearWorkArea(self)
                class(WorkArea), intent(inout) :: self
                type(WorkArea), save :: empty
                self = empty
        end subroutine
        pure elemental function IsWorkAreaEqual(lhs, rhs) result(iseq)
                class(WorkArea), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (WorkArea)
                       iseq = iseq .and. (lhs%num_dirs == rhs%num_dirs)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxpower == rhs%maxpower)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_omega == rhs%num_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%total == rhs%total)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%total_weight == rhs%total_weight)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorWorkArea(lhs, rhs)
                class(WorkArea), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (WorkArea)
                       lhs%num_dirs = rhs%num_dirs
                       lhs%maxpower = rhs%maxpower
                       lhs%num_omega = rhs%num_omega
                       lhs%total = rhs%total
                       lhs%total_weight = rhs%total_weight
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetWorkareaTotalExtents(self) result(res)
                class(WorkArea), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(1,self%num_dirs),extent(0,self%maxpower),extent(0,self%num_omega)]
        end function
        subroutine AllocateWorkAreaObjectFields(self)
                class(WorkArea), intent(inout) :: self
                call self%total%init(int(self%num_dirs),int(self%maxpower- (0) + 1),int(self%num_omega- (0) + 1))
        end subroutine


        subroutine InitIntegrandForDebug(self)
                class(IntegrandForDebug), intent(inout) :: self
                call self%InitPersistent()
                self%max_omega =  0
        end subroutine
        subroutine StoreIntegrandForDebugObjectFields(self)
                class(IntegrandForDebug), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omega%StoreObject(ps, gid,  'omega')
                call self%G%StoreObject(ps, gid,  'G')
        end subroutine
        subroutine LoadIntegrandForDebugObjectFields(self)
                class(IntegrandForDebug), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omega%LoadObject(ps, gid,  'omega')
                call self%G%LoadObject(ps, gid,  'G')
        end subroutine
        subroutine ResetIntegrandForDebugSectionFields(self)
                class(IntegrandForDebug), intent(inout) :: self
                self%omega_ => self%omega%GetWithExtents(self%GetIntegrandForDebugomegaExtents())
                self%G_ => self%G%GetWithExtents(self%GetIntegrandForDebugGExtents())
        end subroutine
        subroutine DisconnectIntegrandForDebugObjectFields(self)
                class(IntegrandForDebug), intent(inout) :: self
               type(iterator) :: iter
                call self%omega%DisconnectFromStore()
                call self%G%DisconnectFromStore()
        end subroutine
        subroutine StoreIntegrandForDebugScalarFields(self)
                class(IntegrandForDebug), intent(inout) :: self
                call self%write('max_omega', self%max_omega)
        end subroutine
        subroutine LoadIntegrandForDebugScalarFields(self)
                class(IntegrandForDebug), intent(inout) :: self
                call self%read('max_omega', self%max_omega)
        end subroutine
        subroutine FinalizeIntegrandForDebug(self)
               type(IntegrandForDebug), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearIntegrandForDebug(self)
                class(IntegrandForDebug), intent(inout) :: self
                type(IntegrandForDebug), save :: empty
                self = empty
        end subroutine
        pure elemental function IsIntegrandForDebugEqual(lhs, rhs) result(iseq)
                class(IntegrandForDebug), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (IntegrandForDebug)
                       iseq = iseq .and. (lhs%max_omega == rhs%max_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega == rhs%omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%G == rhs%G)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorIntegrandForDebug(lhs, rhs)
                class(IntegrandForDebug), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (IntegrandForDebug)
                       lhs%max_omega = rhs%max_omega
                       lhs%omega = rhs%omega
                       lhs%G = rhs%G
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetIntegrandfordebugOmegaExtents(self) result(res)
                class(IntegrandForDebug), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%max_omega,self%max_omega)]
        end function
        function GetIntegrandfordebugGExtents(self) result(res)
                class(IntegrandForDebug), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%max_omega,self%max_omega)]
        end function
        subroutine AllocateIntegrandForDebugObjectFields(self)
                class(IntegrandForDebug), intent(inout) :: self
                call self%omega%init(int(self%max_omega- (-self%max_omega) + 1))
                call self%G%init(int(self%max_omega- (-self%max_omega) + 1))
        end subroutine


        subroutine InitEmbeddedSelfEnergyOnRealOmega(self)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                call self%InitPersistent()
                self%min_band =  0
                self%max_band =  0
                self%shard_start =  0
                self%shard_end =  0
                self%num_si =  0
                self%max_omega =  0
        end subroutine
        subroutine StoreEmbeddedSelfEnergyOnRealOmegaObjectFields(self)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omegaRy%StoreObject(ps, gid,  'omegaRy')
                call self%actual_k%StoreObject(ps, gid,  'actual_k')
                call self%M%StoreObject(ps, gid,  'M')
        end subroutine
        subroutine LoadEmbeddedSelfEnergyOnRealOmegaObjectFields(self)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%omegaRy%LoadObject(ps, gid,  'omegaRy')
                call self%actual_k%LoadObject(ps, gid,  'actual_k')
                call self%M%LoadObject(ps, gid,  'M')
        end subroutine
        subroutine ResetEmbeddedSelfEnergyOnRealOmegaSectionFields(self)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                self%omegaRy_ => self%omegaRy%GetWithExtents(self%GetEmbeddedSelfEnergyOnRealOmegaomegaRyExtents())
                self%actual_k_ => self%actual_k%GetWithExtents(self%GetEmbeddedSelfEnergyOnRealOmegaactual_kExtents())
                self%M_ => self%M%GetWithExtents(self%GetEmbeddedSelfEnergyOnRealOmegaMExtents())
        end subroutine
        subroutine DisconnectEmbeddedSelfEnergyOnRealOmegaObjectFields(self)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
               type(iterator) :: iter
                call self%omegaRy%DisconnectFromStore()
                call self%actual_k%DisconnectFromStore()
                call self%M%DisconnectFromStore()
        end subroutine
        subroutine StoreEmbeddedSelfEnergyOnRealOmegaScalarFields(self)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                call self%write('min_band', self%min_band)
                call self%write('max_band', self%max_band)
                call self%write('shard_start', self%shard_start)
                call self%write('shard_end', self%shard_end)
                call self%write('num_si', self%num_si)
                call self%write('max_omega', self%max_omega)
        end subroutine
        subroutine LoadEmbeddedSelfEnergyOnRealOmegaScalarFields(self)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                call self%read('min_band', self%min_band)
                call self%read('max_band', self%max_band)
                call self%read('shard_start', self%shard_start)
                call self%read('shard_end', self%shard_end)
                call self%read('num_si', self%num_si)
                call self%read('max_omega', self%max_omega)
        end subroutine
        subroutine FinalizeEmbeddedSelfEnergyOnRealOmega(self)
               type(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearEmbeddedSelfEnergyOnRealOmega(self)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                type(EmbeddedSelfEnergyOnRealOmega), save :: empty
                self = empty
        end subroutine
        pure elemental function IsEmbeddedSelfEnergyOnRealOmegaEqual(lhs, rhs) result(iseq)
                class(EmbeddedSelfEnergyOnRealOmega), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (EmbeddedSelfEnergyOnRealOmega)
                       iseq = iseq .and. (lhs%min_band == rhs%min_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_band == rhs%max_band)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%shard_start == rhs%shard_start)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%shard_end == rhs%shard_end)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%num_si == rhs%num_si)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%max_omega == rhs%max_omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omegaRy == rhs%omegaRy)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%actual_k == rhs%actual_k)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%M == rhs%M)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorEmbeddedSelfEnergyOnRealOmega(lhs, rhs)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (EmbeddedSelfEnergyOnRealOmega)
                       lhs%min_band = rhs%min_band
                       lhs%max_band = rhs%max_band
                       lhs%shard_start = rhs%shard_start
                       lhs%shard_end = rhs%shard_end
                       lhs%num_si = rhs%num_si
                       lhs%max_omega = rhs%max_omega
                       lhs%omegaRy = rhs%omegaRy
                       lhs%actual_k = rhs%actual_k
                       lhs%M = rhs%M
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetEmbeddedselfenergyonrealomegaOmegaryExtents(self) result(res)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(-self%max_omega,self%max_omega)]
        end function
        function GetEmbeddedselfenergyonrealomegaActual_kExtents(self) result(res)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(self%shard_start,self%shard_end)]
        end function
        function GetEmbeddedselfenergyonrealomegaMExtents(self) result(res)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                type(extent) :: res(5)
                res = [extent(1,self%max_band - self%min_band + 1),extent(1,self%max_band - self%min_band + 1),extent(-self%max_omega,self%max_omega),extent(self%shard_start,self%shard_end),extent(1,self%num_si)]
        end function
        subroutine AllocateEmbeddedSelfEnergyOnRealOmegaObjectFields(self)
                class(EmbeddedSelfEnergyOnRealOmega), intent(inout) :: self
                call self%omegaRy%init(int(self%max_omega- (-self%max_omega) + 1))
                call self%actual_k%init(int(self%shard_end- (self%shard_start) + 1))
                call self%M%init(int(self%max_band - self%min_band + 1),int(self%max_band - self%min_band + 1),int(self%max_omega- (-self%max_omega) + 1),int(self%shard_end- (self%shard_start) + 1),int(self%num_si))
        end subroutine


        subroutine InitOutput(self)
                class(Output), intent(inout) :: self
                call self%Input%Init()
                self%maxpower =  int(2,kind=int32)
        end subroutine
        subroutine StoreOutputObjectFields(self)
                class(Output), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Input%StoreObjectFields()
                call self%omega%StoreObject(ps, gid,  'omega')
                call self%A%StoreObject(ps, gid,  'A')
        end subroutine
        subroutine LoadOutputObjectFields(self)
                class(Output), intent(inout) :: self
                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
                call self%Input%LoadObjectFields()
                call self%omega%LoadObject(ps, gid,  'omega')
                call self%A%LoadObject(ps, gid,  'A')
        end subroutine
        subroutine ResetOutputSectionFields(self)
                class(Output), intent(inout) :: self
                call self%Input%ResetSectionFields()
                self%omega_ => self%omega%GetWithExtents(self%GetOutputomegaExtents())
                self%A_ => self%A%GetWithExtents(self%GetOutputAExtents())
        end subroutine
        subroutine DisconnectOutputObjectFields(self)
                class(Output), intent(inout) :: self
               type(iterator) :: iter
                call self%Input%DisconnectObjectFields()
                call self%omega%DisconnectFromStore()
                call self%A%DisconnectFromStore()
        end subroutine
        subroutine StoreOutputScalarFields(self)
                class(Output), intent(inout) :: self
                call self%Input%StoreScalarFields()
                call self%write('maxpower', self%maxpower)
        end subroutine
        subroutine LoadOutputScalarFields(self)
                class(Output), intent(inout) :: self
                call self%Input%LoadScalarFields()
                call self%read('maxpower', self%maxpower)
        end subroutine
        subroutine FinalizeOutput(self)
               type(Output), intent(inout) :: self
               call self%disconnect()
        end subroutine
        subroutine ClearOutput(self)
                class(Output), intent(inout) :: self
                type(Output), save :: empty
                self = empty
        end subroutine
        pure elemental function IsOutputEqual(lhs, rhs) result(iseq)
                class(Output), intent(in) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                logical :: iseq
                iseq = .True.
                select type(rhs)
                   type is (Output)
                       iseq = iseq .and. (lhs%Input == rhs%Input)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%maxpower == rhs%maxpower)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%omega == rhs%omega)
                       if (.not. iseq) return
                       iseq = iseq .and. (lhs%A == rhs%A)
                       if (.not. iseq) return
                   class default
                       iseq = .False.
                 end select
        end function
        subroutine AssignmentOperatorOutput(lhs, rhs)
                class(Output), intent(inout) :: lhs
                class(ObjectContainer), intent(in) :: rhs
                select type(rhs)
                   type is (Output)
                       lhs%Input = rhs%Input
                       lhs%maxpower = rhs%maxpower
                       lhs%omega = rhs%omega
                       lhs%A = rhs%A
                       call lhs%ResetSectionFields()
                   class default
                 end select
        end subroutine
        function GetOutputOmegaExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(1)
                res = [extent(0,self%num_omega)]
        end function
        function GetOutputAExtents(self) result(res)
                class(Output), intent(inout) :: self
                type(extent) :: res(3)
                res = [extent(0,self%num_omega),extent(1,self%num_dirs),extent(0,self%maxpower)]
        end function
        subroutine AllocateOutputObjectFields(self)
                class(Output), intent(inout) :: self
                call self%Input%AllocateObjectFields()
                call self%omega%init(int(self%num_omega- (0) + 1))
                call self%A%init(int(self%num_omega- (0) + 1),int(self%num_dirs),int(self%maxpower- (0) + 1))
        end subroutine



end module
