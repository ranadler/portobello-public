#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/Type.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include "clang/AST/CXXInheritance.h"
#include "clang/AST/DeclCXX.h"
#include "clang/AST/TemplateBase.h"
#include "clang/AST/DeclTemplate.h"
#include "clang/AST/Decl.h"
#include "clang/AST/Expr.h"
#include "clang/AST/Stmt.h"
#include "clang/AST/Type.h"
#include "clang/AST/Attr.h"
#include "clang/Basic/IdentifierTable.h"
#include <iostream>
#include <fstream>
#include <streambuf>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <algorithm>

#include <initializer_list>

#include "Jinja2CppLight.h"

using namespace Jinja2CppLight;
using namespace clang;
using namespace std;

static string header = R"(\

    #include <initializer_list>

    struct ___MARKER1___ {};

    class complex {};

    struct string {
      char* content;
      string(int max_len) {}
      string(const char* str) {}
    };

    #define UNITS(X,Y) string X##_Units{#Y}


    // the following are not used any more
    #define IN_FORTRAN constexpr static const char *IN_FORTRAN_[]=
    #define IN_PYTHON  constexpr static const char *IN_PYTHON_[]=
    #define IN_CPP     constexpr static const char *IN_CPP_ARRAY_[]=


    struct datetime {};

    struct range {
        range(int len) {};
        range(int low, int high) {};
    };

    struct extent : public range {
        int dummy;
        extent(int low, int high) : range(low,high+1) {};
    };

    // this is for user-defined array types
    template <class T>
    struct array {
      array<T>(const std::initializer_list<range> ranges) {};
    };

    template <class T>
    struct vector {
      vector<T>(const std::initializer_list<range> ranges) {};
    };

    template<class T>
    struct matrix {
       matrix<T>(const std::initializer_list<range> ranges) {};
    };

    template<class T>
    struct cube {
       cube<T>(const std::initializer_list<range>& ranges) {};
    };

    template<class T>
    struct array4 {
       array4<T>(const std::initializer_list<range>& ranges) {};
    };

    template<class T>
    struct array5 {
       array5<T>(const std::initializer_list<range>& ranges) {};
    };

    template<class T>
    struct array6 {
       array6<T>(const std::initializer_list<range>& ranges) {};
    };


    template<class T>
    struct array7 {
       array7<T>(const std::initializer_list<range>& ranges) {};
    };


    template <class T>
    class list {
    };

    template <class K, class V>
    class map {};

    template <class T>
    class set {};

    class BusObject {};  // base class for all Bus classes

    struct ___MARKER2___ {};

)";


set<string> builtin_structures{
    "complex", "string", "datetime", "range", "extent", "vector", "matrix",
    "cube", "array4", "array5", "array6","array7","list", "map", "set"
};

// all other instantiations are not built-in - they are user defined types
// which should be declared in the hierarchy
set<string> builtin_template_types{"int", "float", "class complex"};

struct DDClass;
struct DDField;
struct Initializer;


class GenVisitor {
public:
    virtual void StartClass(DDClass* cls) {}
    virtual void EndClass(DDClass* cls)   {}


    virtual void StartField(DDField* fld) {}
    virtual void EndField(DDField* fld)   {}

    virtual void GenInit(Initializer* init, DDField* fld) {}
};


struct TypeInformation {
    string name;
    const string& generate();
};

struct DDBase {
    string name;
};


struct Node {
    string oper;
    string leaf;
    bool is_member{false};

    vector<shared_ptr<Node>> children;


    const string to_string(const string& this_qualifier) {
        if (oper != "") {
            if (children.size() > 1) {
                return children[0]->to_string(this_qualifier) + " " + oper + " " + children[1]->to_string(this_qualifier);
            } else {
                return oper +  children[0]->to_string(this_qualifier);
            }
        } else {
            if (is_member)
                return this_qualifier + leaf;
            else
                return leaf;
        }
    }

    const string PythonLength() {
      if (oper == ":") {
            if (children.size() > 1) {
                return "int(" + children[1]->to_string("self.") + "- (" + children[0]->to_string("self.") + ") + 1)";
            } else {
                return  "int(" + children[1]->to_string("self.") + ")";
            }
        } else if (oper == "::") {
            if (children.size() > 1) {
                return "int(" +children[1]->to_string("self.") + "- (" + children[0]->to_string("self.") + "))";
            } else {
                return "int(" +children[0]->to_string("self.") + "-1)";
            }
        } else {
            return "int(" +children[0]->to_string("self.")+ ")";
        }
    }

    const string FortranString(int kind) {
        if (kind == 1 || kind == 3) {
            if (oper == ":") {
                if (children.size() > 1) {
                    return GetExtent(kind, children[0]->to_string("self%"), children[1]->to_string("self%"));
                } else {
                    return GetExtent(kind, "1", children[0]->to_string("self%"));
                }
            } else if (oper == "::") {
                if (children.size() > 1) {
                    return GetExtent(kind, children[0]->to_string("self%"), children[1]->to_string("self%") + "-1");
                } else {
                    return GetExtent(kind, "1", children[0]->to_string("self%") + "- 1");
                }
            } else {
                return GetExtent(kind, "1",  children[0]->to_string("self%"));
            }
        } else if (kind == 2) {
            // not range - this is a counter
            if (oper == ":") {
                if (children.size() > 1) {
                    return "int(" + children[1]->to_string("self%") + "- (" + children[0]->to_string("self%") + ") + 1)";
                } else {
                    return  "int(" + children[1]->to_string("self%") + ")";
                }
            } else if (oper == "::") {
                if (children.size() > 1) {
                    return "int(" +children[1]->to_string("self%") + "- (" + children[0]->to_string("self%") + "))";
                } else {
                    return "int(" +children[0]->to_string("self%") + "-1)";
                }
            } else {
                return "int(" +children[0]->to_string("self%")+ ")";
            }

        }
    }

    private:
    string GetExtent(int kind, const string& lower, const string& upper) {
        if (kind == 1)
            return "extent(" + lower + ","+ upper + ")";
        else // kind = 3
            return "int(" + lower + "):int(" + upper + ")";
    }
};

// Initializer typically represents a range, where the limit(s) are given by arithmetic expressions,
// which may include constants or valid names.
struct Initializer : public RecursiveASTVisitor<Initializer> {
    Initializer(ASTContext *c, Expr* e) {
        TraverseStmt(e);
        TrySetRoot();
    };

    bool shouldTraversePostOrder() const {return true;}
    bool shouldVisitImplicitCode() const {return true;}


    void TrySetRoot() {
        if (q.size() > 0) {
            for (auto node : q)
                root.children.push_back(node);
            if (is_inclusive)
                root.oper = ":";
            else
                root.oper = "::";
            if (q.size() == 1)
                root.oper = "value:";
            q.clear();
        }
    }

    bool VisitCXXConstructExpr(CXXConstructExpr* ce) {
        string cls = ce->getBestDynamicClassType()->getIdentifier()->getName().str();
        if (cls == "extent") {
            is_inclusive = true;
        } else {
            if (cls != "range")
                return true;
        }

        TrySetRoot();
        return true;
    }

    bool VisitImplicitCastExpr(ImplicitCastExpr* exp) {
        return true;
    }

    bool VisitCXXThisExpr(CXXThisExpr* exp) {
        return true;
    }

    bool VisitMemberExpr(MemberExpr* me) {
        shared_ptr<Node> node(new Node);
        node->is_member = true;
        node->leaf = me->getMemberDecl()->getIdentifier()->getName();
        q.push_back(node);
        return true;
    }

    bool VisitIntegerLiteral(IntegerLiteral* il) {
        shared_ptr<Node> node(new Node);
        node->leaf = to_string(il->getValue().getLimitedValue());
        q.push_back(node);
        return true;
    }

    bool VisitFloatingLiteral(FloatingLiteral *fl) {
        shared_ptr<Node> node(new Node);
        node->leaf = to_string(fl->getValueAsApproximateDouble());
        q.push_back(node);
        return true;
    }


    bool VisitUnaryOperator(UnaryOperator* uo) {
        shared_ptr<Node> node(new Node);
        node->oper = uo->getOpcodeStr(uo->getOpcode());
        node->children = {q.back()};
        q.pop_back();
        q.push_back(node);
        return true;
    }


    bool VisitBinaryOperator(BinaryOperator* bo) {
        shared_ptr<Node> node(new Node);
        node->oper = bo->getOpcodeStr(bo->getOpcode());
        auto last2 = q.back();
        q.pop_back();
        auto last = q.back();
        q.pop_back();
        node->children = {last, last2};
        q.push_back(node);
        return true;
    }

    Node root;
    bool is_inclusive{false};
    list<shared_ptr<Node>> q;
};

struct ParamDecl {
    ParamDecl(string name, string paramtype) : name(name), paramtype(paramtype) {}
    string name;
    string paramtype;
};



struct DDFunction : public RecursiveASTVisitor<DDFunction>, public DDBase {
    DDFunction(ASTContext *context, CXXMethodDecl* fd, bool& has_implementation) :
        returnType(fd->getReturnType().getAsString()) {
        name = fd->getDeclName().getAsString();
        is_static = fd->isStatic();
        is_const = fd->isConst();
        for (ParmVarDecl*  param : fd->parameters()) {
                params.push_back(ParamDecl(param->getDeclName().getAsString(),
                                           param->getType().getAsString()));
        }


        for (Attr* attr : fd-> getAttrs()) {
            SourceRange sr = attr->getRange();
            const string& text = Lexer::getSourceText(CharSourceRange::getCharRange(sr),
                                                      context->getSourceManager(),
                                                      LangOptions()).str();
            auto pos = text.find("FORTRAN,");
            if (pos != std::string::npos) {
                is_fortran = true;
                has_implementation = true;
                imp_module = text;
                imp_module = imp_module.erase(0, pos);
                imp_module = imp_module.erase(0, imp_module.find(",")+1);
                pos = imp_module.find("\"");
                imp_module = imp_module.erase(pos, pos+1);
                //cerr << "attribute:" << imp_module << endl;
            }
            pos = text.find("PYTHON:");
            if (pos != std::string::npos) {
                is_python = true;
            }
            pos = text.find("C++");
            if (pos != std::string::npos) {
                is_cpp = true;
            }
        }

    }

    bool is_fortran{false};
    bool is_cpp{false};
    bool is_python{false};
    bool is_static{false};
    bool is_const{false};
    string returnType;
    list<ParamDecl> params;
    string imp_module;
};

struct HasTempalteArgs {
    virtual const map<string, pair<string,string>>& GetTemplateArgs() = 0;
};

struct DDField :  public RecursiveASTVisitor<DDField>, public DDBase {

    DDField(ASTContext *c, HasTempalteArgs*dd, FieldDecl* fd) : context(c)  {
        this->name = fd->getDeclName().getAsString();
        this->type_name = fd->getType().getAsString();
        TraverseFieldDecl(fd);

        const Type* type_class = fd->getType().getTypePtr();

        if (type_class->isStructureOrClassType() && dd->GetTemplateArgs().count(type_name)) {
            //cerr << "found array instantiation " << type_name << endl;
            auto p = dd->GetTemplateArgs().at(type_name);
            this->is_objar = true;
            this->obj_type = p.second;
            this->name_space = p.first;  // it is collected at instantiation time
            return;
        }

        if (type_class->isStructureOrClassType() &&
            !type_class->isDependentType() ) {  // for now we are not handling template instantiations in general objects

            TagDecl* tag_decl = fd->getType().getTypePtr()->getAsTagDecl();
            CXXRecordDecl* rec_decl = fd->getType().getTypePtr()->getAsCXXRecordDecl();

            const string& qualfied_name = tag_decl->getQualifiedNameAsString();
            const string& unqualified_name = tag_decl->getUnderlyingDecl()->getNameAsString();

            string ns = qualfied_name;
            auto pos = ns.find("::" + unqualified_name);
            if (pos != std::string::npos) {
                ns = ns.erase(pos);
            }
            // TODO: check that ns does not contain an extra ::

            if (builtin_structures.count(qualfied_name) == 0) {
                this->is_child_obj = true;
                this->name_space = ns;
                this->obj_type = unqualified_name;
            }
        }
    }

    bool VisitInitListExpr(InitListExpr* ile) {
        this->ile = ile;
        for (int i =0; i< ile->getNumInits(); i++) {
             Expr* expr = ile->getInit(i);
             shared_ptr<Initializer> init(new Initializer(context, expr));
            initializers.push_back(init);
        }
        return false; // otherwise the init list will be traversed again
    }

    // this is an initialization string
    bool VisitStringLiteral(StringLiteral* sl) {
        //cout << "found string literal " << string(sl->getString()) << endl;
        init_string = shared_ptr<string>(new string(sl->getString()));
        return true;
    }

    // this is an initialization int
    bool VisitIntegerLiteral(IntegerLiteral* il) {
        //cout << "found int literal " << to_string() << endl;
        init_int = shared_ptr<long>(new long(il->getValue().getLimitedValue()));
        return true;
    }

    // this is an initialization float
    bool VisitFloatingLiteral(FloatingLiteral *fl) {
        //cout << "found float literal " << to_string() << endl;
        //fl->getValue()->toString(str);
        // TODO: this doesn't work for negative numbers for some reason ... fix it!
        init_float = shared_ptr<double>(new double(fl->getValueAsApproximateDouble()));
        //cerr << "found float literal " << *init_float << endl;
        return true;
    }

    void accept(GenVisitor *v) {
        for (auto init : initializers) {
            v->GenInit(init.get(), this);
        }
    }


    string type_name; // simplified for basic types, otherwise class name, or template name
    list<shared_ptr<Initializer>> initializers;
    shared_ptr<long> init_int;
    shared_ptr<double> init_float;
    shared_ptr<string> init_string;

    // this is for child object types
    bool is_child_obj{false};
    bool is_objar{false};  // if it is an object array with user-defined type (saved individually)
                           // if true, the obj type is defined in obj_type (and the namespace in name_space)
    string name_space;
    string obj_type;
private:
    InitListExpr* ile{nullptr};
    ASTContext *context;
};

class LangDefinitionAnalyzer : public RecursiveASTVisitor<LangDefinitionAnalyzer> {
public:

    bool VisitStringLiteral(StringLiteral *sl) {
        literals.insert(string(sl->getString()));
        //cerr << "literal: " << string(sl->getString()) << endl;
        return true;
    }

    set<string> literals;
};

struct DDClass : public RecursiveASTVisitor<DDClass>, public DDBase  {

    DDClass(ASTContext *c, HasTempalteArgs* dd, CXXRecordDecl* td, string& name_space) : context(c), dd(dd) {
        if (!td->isStruct()) {
            cerr << "Warning: definition " << td->getBeginLoc().printToString(c->getSourceManager()) <<
              " invalid - only structs allowed. Changing to 'struct'.\n";
        }

        this->name = td->getIdentifier()->getName();

        for (CXXRecordDecl::base_class_const_iterator iter = td->bases_begin(); iter!=td->bases_end(); iter++) {
            const CXXBaseSpecifier& base = *iter;
            AccessSpecifier as = base.getAccessSpecifier();
            if (as == AS_protected | as == AS_private) {
                cerr << "Warning: ignoring access specifier protected / private on line " <<
                        base.getBaseTypeLoc().printToString(c->getSourceManager()) << "\n";
            }

            string base_name = base.getType().getBaseTypeIdentifier()->getName().str();
            bases.push_back(base_name);
            // here also need to obtain the type's namespace, so that we add a dependence statement
            string ns = base.getType().getAsString();
            auto pos = ns.find("::" + base_name);
            if (pos != std::string::npos) {
                ns = ns.erase(pos);

                // this name starts with our own namespace
                if (ns.find("struct " + name_space) == 0)
                    ns = "";

                if (ns != "") {
                    namespaces.insert(ns);
                    //cerr << "adding:" << ns << endl;
                }
            }
        }
        TraverseDecl(td);
    }

    bool VisitCXXMethodDecl(CXXMethodDecl* fd) {
        shared_ptr<DDFunction> fun(new DDFunction(context, fd, has_external_implementation));
        functions.push_back(fun);

        return true;
    }

    bool VisitFieldDecl(FieldDecl* fd) {
        shared_ptr<DDField> field(new DDField(context, dd, fd));
        if (field->is_objar) has_objars = true;
        fields.push_back(field);
        return true;
    }

    void accept(GenVisitor *v) {
        for (auto fld : fields) {
            v->StartField(fld.get());
            fld->accept(v);
            v->EndField(fld.get());
        }
    }


    bool VisitVarDecl(VarDecl* vd) {
        if (vd->isConstexpr()) {
            LangDefinitionAnalyzer lda;
            lda.TraverseVarDecl(vd);
            //cerr << "literals: " << string(vd->getName()) << endl;
            methods_to_bind[string(vd->getName())] = lda.literals;
            return true;
        }
        return true;
    }

    bool hasBaseClass() {
        return bases.size() > 0;
    }

    string getFortranBaseClass() {
        return *bases.begin();
    }

    list<string> bases;
    set<string> namespaces;
    list<shared_ptr<DDField>> fields;
    string name_space;
    map<string, set<string>> methods_to_bind;
    list<shared_ptr<DDFunction>> functions;
    bool has_external_implementation{false};
    bool has_objars{false};
    HasTempalteArgs *dd;
private:
    ASTContext *context;
};



class CreateDataDefinition : public HasTempalteArgs,  public RecursiveASTVisitor<CreateDataDefinition> {
public:

    void setContext(ASTContext *context) { this->context = context; }

    virtual void HandleTranslationUnit(clang::ASTContext &c) {}

    bool shouldVisitTemplateInstantiations() const { return true; }

    // only one namespace is allowed in each of our files - for software engineering
    // reasons (it is easier to generate the code, since fortran cannot split one module
    // into multiple files).
    bool VisitNamespaceDecl(NamespaceDecl* nd) {
        // the last one will be set correctly (first ones come from the includes, such as std, __stuff)
        name_space = string(nd->getIdentifier()->getName());
        return true;
    }

    bool VisitCXXRecordDecl(CXXRecordDecl* td) {
        SourceLocation sl = td->getSourceRange().getBegin();
        auto input_file =  context->getSourceManager().getFilename(sl).str();
        if (input_file != "input.cc")
            return true;

        string name = td->getIdentifier()->getName().str();
        if (name == "___MARKER1___") {
            passed_marker1 = true;
        } else if (name == "___MARKER2___") {
            passed_marker2 = true;
        } else {
            if (passed_marker2) {
                shared_ptr<DDClass> cls(new DDClass(context, this, td, name_space));
                classes.push_back(cls);
                class_map[name] = cls;
                cls->name_space = name_space;
            }
        }
        return true;
    }

    bool VisitClassTemplateDecl(ClassTemplateDecl *dec) {
        TemplateParameterList* tpl = dec->getTemplateParameters();
        for (int i =0; i < tpl->size(); i++) {
            NamedDecl* param = tpl->getParam(i);
            //cout << "       template param:" << param->getNameAsString() << "\n";
        }
        return true;
    }

    bool VisitClassTemplateSpecializationDecl(ClassTemplateSpecializationDecl* dec) {
        // this is where the template type is instantiated
        //cout << "    in template specialization of: " << dec->getDeclName().getAsString() << "\n";
        const TemplateArgumentList& args = dec->getTemplateArgs();
        //for (int i =0; i < args.size(); i++) {
        //    const TemplateArgument& arg = args[i];
        //    cerr <<  "              template arg: "  << arg.getAsType().getAsString() << "\n";
        //}
        //cerr << "canonical name: " << string(dec->getCanonicalDecl()->getName()) << endl;

        if (!passed_marker1 || args.size() > 1 || args.size() == 0)
            return true;

        if (args[0].getAsType().getBaseTypeIdentifier() == nullptr)
            return true;

        if (builtin_template_types.count(args[0].getAsType().getAsString()) == 0) {

            string base_name = args[0].getAsType().getBaseTypeIdentifier()->getName().str();

            // here also need to obtain the type's namespace, so that we add a dependence statement
            string ns = args[0].getAsType().getAsString();
            auto pos = ns.find("::" + base_name);
            if (pos != std::string::npos) {
                ns = ns.erase(pos);

                pos = ns.find("struct ");
                if (pos != std::string::npos)
                    ns = ns.erase(0, pos+7);
            }


            string inst_type =  dec->getDeclName().getAsString() + "<" +
                                args[0].getAsType().getAsString() +
                                ">";

            template_types[inst_type] = make_pair(ns, base_name);

        }
        return true;
    }

    void accept(GenVisitor *v) {
        for (auto cls : classes) {
            v->StartClass(cls.get());
            cls->accept(v);
            v->EndClass(cls.get());
        }
    }
    virtual const map<string, pair<string,string>>& GetTemplateArgs() {
        return template_types;
    }
private:
    bool passed_marker1{false};
    bool passed_marker2{false};
    ASTContext *context;
public:
    list< shared_ptr<DDClass> > classes;
    map<string, shared_ptr<DDClass>> class_map;
    string name_space;
    map<string, pair<string,string>> template_types;
};

class VisitingConsumer : public clang::ASTConsumer {
public:
    explicit VisitingConsumer(CreateDataDefinition* dd) : dd(dd) {}

protected:
    void HandleTranslationUnit(clang::ASTContext &context) override {
        dd->setContext(&context);
        dd->TraverseDecl(context.getTranslationUnitDecl());
    }

private:
    CreateDataDefinition* dd;
};

class RunVisitingConsumer : public clang::ASTFrontendAction {
public:
    RunVisitingConsumer(CreateDataDefinition* dd) : dd(dd) {}
protected:
    unique_ptr<clang::ASTConsumer> CreateASTConsumer(clang::CompilerInstance &Compiler, llvm::StringRef InFile) override {
        return unique_ptr<clang::ASTConsumer>(new VisitingConsumer(dd));
    }

private:
    CreateDataDefinition* dd;
};


/// -------------------------------------------------------------------------------

///
///                          code generation
///
/// -------------------------------------------------------------------------------


static string capitalize(string name) {
    if( !name.empty() )  {
        name[0] = std::toupper( name[0] );

        for( std::size_t i = 1 ; i < name.length() ; ++i )
            name[i] = std::tolower( name[i] );
    }
    return name;
}



struct GenTypeInfo {
    GenTypeInfo(const string& str) {
        type_def = str;
    }

    bool is_emtpy() const { return type_def == ""; }

    virtual const string get_type_def() const { return type_def;}

    virtual const string get_parameter_def() const { return ""; }

    virtual const string get_initializer_def(const DDField* fld, bool also_scalar_objects) const { return ""; }

    virtual const string get_PY_initializer_def(const DDField* fld) const { return ""; }

    virtual bool has_section() {return false;}

    virtual GenTypeInfo* get_element_type() { return nullptr; }

    virtual const string get_slicer() const { return ""; }

    virtual bool is_scalar() { return true; }

    virtual int get_dimension() { return 1; }

    virtual int is_transient() { return false; }

    virtual string fortran_module_name() { return ""; };

    virtual string get_numpy_element_type() { return ""; };

    virtual string get_numpy_type_hint() { return ""; }

    string type_def;
};


struct ScalarType : public GenTypeInfo {
    ScalarType(const string& str) : GenTypeInfo(str) {}

    virtual const string get_initializer_def(const DDField* fld, bool also_scalar_objects) const override {

            if ((type_def == "type(string)") && also_scalar_objects) {
                if (fld->init_string.get() != nullptr) {
                 return " = \"" + *fld->init_string + "\"\n";
                }
            } else if (type_def == "integer(kind=int32)") {
                if (fld->init_int.get() != nullptr) {
                    return " =  int(" + to_string(*fld->init_int) + ",kind=int32)\n";
                } else
                    return " =  0\n";

            } else if (type_def == "real(kind=dp)") {
                if (fld->init_float.get() != nullptr) {
                    return " =  real(" + to_string(*fld->init_float) + ",kind=16)\n";
                } else
                    return " =  0.0d0\n";
            } else if (type_def == "complex(kind=dp)") {
                // TODO: copy non-default  complex initializer
                return " =  (0.0d0, 0.0d0)\n";
            } else if (type_def == "logical") {
                // TODO: copy non-default logical initializer
                return " = .False.\n";
            }


        return "";
    }


    virtual const string get_PY_initializer_def(const DDField* fld) const override {
            if (fld->is_child_obj) {
                return " = " + fld->obj_type + "()\n";
            }

            if ((type_def == "type(string)")) {
                if (fld->init_string.get() != nullptr) {
                 return " = \"" + *fld->init_string + "\"\n";
                } else {
                    return " = \"\"\n";
                }
            } else if (type_def == "integer(kind=int32)") {
                if (fld->init_int.get() != nullptr) {
                    return " =  " + to_string(*fld->init_int);
                } else
                    return " =  0\n";

            } else if (type_def == "real(kind=dp)") {
                if (fld->init_float.get() != nullptr) {
                    return " = " +  to_string(*fld->init_float);
                } else
                    return " =  0.0\n";
            } else if (type_def == "complex(kind=dp)") {
                // TODO: copy non-default  complex initializer
                return " =  0.0 + 0.0j";
            } else if (type_def == "logical") {
                // TODO: copy non-default logical initializer
                return " = False\n";
            }


        return " = None";
    }

};

struct DatasetType : public GenTypeInfo {
    DatasetType(const string& name, const string& basetype, int dim, bool has_section = true) :
        dim(dim), has_section_(has_section), basetype(basetype), module_name(name),
        GenTypeInfo("type(" + name +  "_" +  basetype + ")") {
            if (this->basetype == "real") this->basetype = "float";
            if (this->basetype == "complex") this->basetype = "class complex";
        }

    virtual const string get_slicer() const {
        string slicer = ":";
        for (int i=0; i<dim-1; i++) {
            slicer += ",:";
        }
        return slicer;
    }

    virtual bool is_scalar() { return false; }

    virtual bool has_section() {return has_section_;}
    virtual GenTypeInfo* get_element_type();

    virtual int get_dimension() { return dim; }

    virtual string fortran_module_name() { return module_name; };

    virtual string get_numpy_element_type() {
        if (this->basetype == "float")
            return "np.float64";
        if (this->basetype == "int")
            return "np.int32";
        if (this->basetype == "class complex")
            return "np.complex128";
        return "?N?";
    }

    virtual string get_numpy_type_hint() {
        if (this->basetype == "float")
            return "Float64";
        if (this->basetype == "int")
            return "Int32";
        if (this->basetype == "class complex")
            return "Complex128";
        return "Any";
    }

    int dim;
    bool has_section_;
    string basetype;
    string module_name;
};


map<string, GenTypeInfo*>  typemap{
    {"int",                    new ScalarType("integer(kind=int32)")},

    {"float",                  new ScalarType("real(kind=dp)")},
    {"class complex",          new ScalarType("complex(kind=dp)")},
    {"struct string",          new ScalarType("type(string)")},
    {"_Bool",                   new ScalarType("logical")},
    {"struct datetime",        new ScalarType("type(datetime)")},

    {"vector<int>",            new DatasetType("vector", "int", 1)},
    {"vector<float>",          new DatasetType("vector", "real", 1)},
    {"vector<class complex>",  new DatasetType("vector", "complex", 1)},

    {"matrix<int>",            new DatasetType("matrix", "int", 2)},
    {"matrix<float>",          new DatasetType("matrix", "real", 2)},
    {"matrix<class complex>",  new DatasetType("matrix", "complex", 2)},

    {"cube<int>",            new DatasetType("cube", "int", 3)},
    {"cube<float>",          new DatasetType("cube", "real", 3)},
    {"cube<class complex>",  new DatasetType("cube", "complex", 3)},

    {"array4<int>",            new DatasetType("array4", "int", 4)},
    {"array4<float>",          new DatasetType("array4", "real", 4)},
    {"array4<class complex>",  new DatasetType("array4", "complex", 4)},

    {"array5<int>",            new DatasetType("array5", "int", 5)},
    {"array5<float>",          new DatasetType("array5", "real", 5)},
    {"array5<class complex>",  new DatasetType("array5", "complex", 5)},

    {"array6<int>",            new DatasetType("array6", "int", 6)},
    {"array6<float>",          new DatasetType("array6", "real", 6)},
    {"array6<class complex>",  new DatasetType("array6", "complex", 6)},

    {"array7<int>",            new DatasetType("array7", "int", 7)},
    {"array7<float>",          new DatasetType("array7", "real", 7)},
    {"array7<class complex>",  new DatasetType("array7", "complex", 7)},


};

GenTypeInfo* DatasetType::get_element_type() { return typemap.at(basetype); }


class FortranUseCollector : public GenVisitor {
public:
    FortranUseCollector(CreateDataDefinition* dd) {
        dd->accept(this);
    }


    void StartClass(DDClass* cls)  {
        if (cls->namespaces.size() > 0) {
            collected.insert(*cls->namespaces.begin());
            pydeps.insert(*cls->namespaces.begin());
        }
        my_name_space = cls->name_space;
    }

    void StartField(DDField* fld) {
        // first check for sub-object
        if (fld->is_child_obj && fld->name_space != "" && fld->name_space != my_name_space) {
            collected.insert(fld->name_space);
            pydeps.insert(fld->name_space);
            return;
        }
        if (typemap.count(fld->type_name) == 0)
            return;
        auto tyi = typemap.at(fld->type_name);
        if (tyi->is_scalar())
            return;
        collected.insert(tyi->fortran_module_name());
    }

    set<string> collected;
    set<string> pydeps;

private:
    string my_name_space;
};

string tolower( const string & str) {
    string ret = str;
    for( std::size_t i = 0 ; i < ret.length() ; ++i )
        ret[i] = std::tolower(ret[i]);
    return ret;
}

void GenFortranParameters(DDClass* cls, DDFunction* func, ostream&  outf, bool is_wrapper) {
    if (is_wrapper) {
        outf << "(ret, imp_fp, selfpath";  // return value is an array - for the C-python interface
    } else {
        outf << "(selfpath";  // return value is an array - for the C-python interface
    }
    for (auto param : func->params) {
       outf << ", ";
        outf << param.name;
        //cerr << param.paramtype << endl;
    }
    outf << ")";
    if (!is_wrapper) {
        outf << " result(res)";
    } else {
        outf << " bind(C,name='" << tolower( cls->name_space ) << "_mp_" <<  tolower( func->name ) << "wrapper_')";
    }
    outf << endl;

    // now write the body

    outf << "                use iso_c_binding" << endl;
    outf << "                use stringifor" << endl;
    outf << "                use hdf5_base" << endl;
    outf << "                type(string) :: res" << endl;
    if (is_wrapper) {
        outf << "                type(c_ptr),intent(inout),dimension(1) :: ret" << endl;
        outf << "                type(c_funptr),intent(in),value        :: imp_fp" << endl;
        outf << "                type(c_ptr),intent(in),value           :: selfpath" << endl;
   } else {
        //declare the result
        outf << "                type(string), intent(in) :: selfpath" << endl;
   }

   for (auto param : func->params) {
        if (param.paramtype == "int") {
            outf << "                integer(kind=c_int),intent(in),value :: " << param.name << endl;
        } else if (param.paramtype == "float") {
            outf << "                real(kind=c_float),intent(in),value :: " << param.name << endl;
        } else if (param.paramtype == "class complex") {
            // not supported yet - need to be passed as pointer, and converted
        } else if (param.paramtype == "const struct string &") {
            if (is_wrapper)
                outf << "                type(c_ptr),intent(in),value        :: " << param.name <<endl;
            else
                outf << "                 type(string), intent(in)            :: " << param.name << endl;
        } else if (param.paramtype == "class BusObject &") {

            if (is_wrapper) {
                outf << "                type(c_ptr),intent(in),value        :: " << param.name <<endl;
            } else
                outf << "                type(persistent), intent(in)        :: " << param.name << endl;
        }
   }

    // only wrapper has actual code
    if (is_wrapper) {
        outf << "            procedure(" << func->name << "I), pointer :: fort_imp_fp" << endl;
        outf << "            call c_f_procpointer(imp_fp, fort_imp_fp)" << endl;

        // make the call to the actual function
        outf << "            res = fort_imp_fp( FromCStr(selfpath)";

        for (auto param : func->params) {
            if (param.paramtype == "int" || param.paramtype == "float") {
                outf << ", " << param.name;
            } else if (param.paramtype == "class complex") {
                // not supported yet - need to be passed as pointer, and converted
            } else if (param.paramtype == "const struct string &") {
                outf << ", FromCStr(" << param.name << ")";
            } else if (param.paramtype == "class BusObject &") {
                outf << ", GetPersistentPath(" << param.name << ")";
            }
        }

        outf << ")" << endl;
        outf << "            ret(1)= NewCStr(res%chars())" << endl;
    }

}

void GenUserFunctions(DDClass* cls, ostream&  outf) {

    outf << endl;
    for (shared_ptr<DDFunction> func : cls->functions) {
        if (!func->is_fortran) continue;

        outf << "        subroutine " << func->name << "Wrapper";
        GenFortranParameters(cls, func.get(), outf, true);
        outf << "        end subroutine" << endl;
        outf << endl;
    }

}



class FortranDefGen : public GenVisitor {
public:

    FortranDefGen(CreateDataDefinition* dd, const map<string, string>* options,
                  const set<string>& modules, ofstream& outf) : dd(dd), options(options), outf(outf) {
        Template f_header( R"d(
module {{module_name}}
    use hdf5_base
    use stringifor
    use datetime_module
    use arrayiter

{{module_uses}}

    implicit none
    public
)d");
        f_header.setValue( "module_name", dd->name_space);
        string module_uses;
        for (auto module : modules) {
            module_uses += "    use " + module + "\n";
        }
        f_header.setValue("module_uses", module_uses);
        outf << f_header.render();
        dd->accept(this);
    }

    void StartClass(DDClass* cls)  {
        Template type_header( R"d(
    type{{extends_statement}}{{abstract}} :: {{type_name}}
)d");
        if (cls->bases.size() > 0) {
            type_header.setValue("extends_statement", string(", extends(") + cls->bases.front() + ")");
        } else {
            type_header.setValue("extends_statement",", extends(persistent)");
        }
        if (cls->has_external_implementation) {
            type_header.setValue("abstract", "");  //TODO: maybe it's better to generate abstract types, not stubs?
        } else {
            type_header.setValue("abstract", "");
        }
        type_header.setValue("type_name", cls->name);
        outf << type_header.render();
    }

    void EndClass(DDClass* cls) {
        Template methods( R"d(

        contains
        procedure :: AllocateObjectFields => Allocate{{type_name}}ObjectFields
        procedure :: ResetSectionFields   => Reset{{type_name}}SectionFields
        procedure :: StoreScalarFields    => Store{{type_name}}ScalarFields
        procedure :: StoreObjectFields    => Store{{type_name}}ObjectFields
        procedure :: LoadScalarFields     => Load{{type_name}}ScalarFields
        procedure :: LoadObjectFields     => Load{{type_name}}ObjectFields
        procedure :: DisconnectObjectFields => Disconnect{{type_name}}ObjectFields
        procedure :: IsEqual              => Is{{type_name}}Equal
        procedure :: AssignmentOperator   => AssignmentOperator{{type_name}}
        procedure :: clear                => Clear{{type_name}}
        procedure :: init => Init{{type_name}}
#ifndef __GFORTRAN__
        final     :: Finalize{{type_name}}
#endif
{{slicemethods}}
)d");


        methods.setValue("type_name", cls->name);
        auto slicemethods = GenSliceMethods(cls);
        methods.setValue("slicemethods", slicemethods);
        outf << methods.render();

        outf << "    end type\n";

        for (auto func : cls->functions) {
            if (func->is_fortran) {
                // this is the stub, to be re-implemented in the subclass
                outf <<  "    interface" << endl;

                // generate the stub that needs to be overriden
                outf << "         function " << func->name << "I";
                GenFortranParameters(cls, func.get(), outf, false);
                outf << "         end function" << endl;


                outf <<  "    end interface" << endl << endl;
            }
        }

    }


    struct SliceMethodsGenerator : GenVisitor {
        SliceMethodsGenerator(DDClass* cls): str(""), cls(cls) {};
        void StartField(DDField* fld) {
            if (typemap.count(fld->type_name) == 0)
                return;
            auto tyi = typemap.at(fld->type_name);
            if (tyi->is_emtpy() || !tyi->has_section())
                return;
            str += "        procedure :: Get" + capitalize(cls->name) + capitalize(fld->name) + "Extents\n";
        }

        string str{""};
        DDClass* cls;
    };

    string GenSliceMethods(DDClass* cls) {
        SliceMethodsGenerator gen(cls);
        cls->accept(&gen);
        return gen.str;
    }

    virtual const string getObjarSlicer(DDField* fld) const {
        string slicer = "(:";
        int dim = fld->initializers.size();
        for (int i=0; i<dim-1; i++) {
            slicer += ",:";
        }
        return slicer + ")";
    }


    void StartField(DDField* fld) {
        string template_str{
        "        " \
        "{{type_name}}{{is_paramter}}{{arraydef}}  ::  {{field_name}}{{dims}}{{initializer}}\n"};

        if ((typemap.count(fld->type_name) == 0) && !fld->is_child_obj && !fld->is_objar) {
            cerr << "Warning: field " << fld->name  << " is dropped because type " <<
                    fld->type_name << " is not recognized.\n";
            return;
        }


        if (fld->is_child_obj) {
            Template field_header(template_str);
            field_header.setValue("type_name", "type("+ fld->obj_type + ")");
            field_header.setValue("is_paramter", "");
            field_header.setValue("arraydef", "");
            field_header.setValue("dims", "");
            field_header.setValue("field_name", fld->name);
            field_header.setValue("initializer", "");
            outf << field_header.render();
        } else if (fld->is_objar) {
            Template field_header(template_str);
            field_header.setValue("type_name", "type("+ fld->obj_type + ")");
            field_header.setValue("is_paramter", "");
            field_header.setValue("arraydef", ", allocatable");
            field_header.setValue("dims", getObjarSlicer(fld));
            field_header.setValue("field_name", fld->name);
            field_header.setValue("initializer", "");
            outf << field_header.render();
        } else {
            // scalar or dataset
            auto tyi = typemap.at(fld->type_name);

            if (!tyi->is_emtpy()) {
                if (tyi->has_section()) {
                    template_str += "        " \
                                   "{{slice_def}},pointer :: {{field_name}}_({{slicer}})\n";
                }
                Template field_header(template_str);
                field_header.setValue("type_name", tyi->get_type_def());
                field_header.setValue("is_paramter", tyi->get_parameter_def());
                field_header.setValue("field_name", fld->name);
                field_header.setValue("arraydef", "");
                field_header.setValue("dims", "");
                field_header.setValue("initializer", tyi->get_initializer_def(fld,false));

                if (tyi->has_section()) {
                    field_header.setValue("slice_def", tyi->get_element_type()->get_type_def());
                    field_header.setValue("slicer", tyi->get_slicer());
                }

                outf << field_header.render();
            }
        }
    }

private:


    CreateDataDefinition* dd;
    ostream&  outf;

    const map<string, string>* options;
};


string GenerateInitializer(DDClass* cls) {

    string method_name = "Init{{type_name}}";

    struct MethodGenerator : public GenVisitor {

        void StartField(DDField* fld) {

            if (typemap.count(fld->type_name) == 0) {
                if (fld->is_child_obj) {
                    Template temp("                call self%{{field_name}}%InitPersistent()\n");
                    temp.setValue("field_name", fld->name);
                    str += temp.render();
                }
                return;
            }
            auto tyi = typemap.at(fld->type_name);
            if (!tyi->is_scalar())
                return;

            string init = tyi->get_initializer_def(fld, true);
            if (init != "")
                init = "                self%{{field_name}}" + init;
            Template temp(init);
            temp.setValue("field_name", fld->name);

            str += temp.render();
        }

        string str{""};
    };

    MethodGenerator mg;
    cls->accept(&mg);

    string base_call{""};
    if (cls->hasBaseClass())
        base_call += "                call self%" + cls->getFortranBaseClass() + "%" + method_name + "()\n";
    else
        base_call += "                call self%InitPersistent()\n";  // very important to initialize the base class

    Template baseTemplate(base_call);
    baseTemplate.setValue("type_name", "");

    Template temp{"        subroutine " + method_name +
                    "(self)\n" +
                    "                class({{type_name}}), intent(inout) :: self\n" +
                    baseTemplate.render() +
                    mg.str +
                    "        end subroutine\n"};
    temp.setValue("type_name", cls->name);
    return temp.render();
}

string GenObjarAllocExpression(DDField* fld) {

    struct MethodGenerator : public GenVisitor {
        void GenInit(Initializer* init, DDField* fld) {
            if (!fld->is_objar) return;
            ++init_index;
            if (init_index > 1)
                str += ",";
            str += init->root.FortranString(3);

        }
        string str{""};
        int init_index{0};
    };
    MethodGenerator mg;
    fld->accept(&mg);
    return mg.str;
}

string GetObjarIterIndex(DDField* fld) {

    struct MethodGenerator : public GenVisitor {
        void GenInit(Initializer* init, DDField* fld) {
            if (!fld->is_objar) return;
            ++init_index;
            if (init_index > 1)
                str += ",";
            str += "iter%i(" + to_string(init_index) + ")";

        }
        string str{""};
        int init_index{0};
    };
    MethodGenerator mg;
    fld->accept(&mg);
    return mg.str;
}


string GenSubroutine1(DDClass* cls,
                      const string& method_name,
                      const string& prelude,
                      const string& per_field,
                      const string& per_sub_object,
                      const string& per_objar,
                      bool choose_objects ) {// false = choose_scalars)

     struct MethodGenerator : public GenVisitor {
        MethodGenerator(const string& per_field,
                        const string& per_sub_obect,
                        const string& per_objar,
                        bool choose_objects,
                        DDClass* cls) :
                          choose_scalars(!choose_objects),
                          choose_objects(choose_objects),
                          per_sub_object(per_sub_obect),
                          per_objar(per_objar),
                          per_field(per_field), cls(cls) {}

        void StartField(DDField* fld) {
            if (typemap.count(fld->type_name) == 0) {
                if (fld->is_child_obj) {
                    if (choose_objects && per_sub_object != "") {
                        // code for objects
                    Template temp(per_sub_object);
                    temp.setValue("field_name", fld->name);
                    str += temp.render();
                    }
                } else if (fld->is_objar && per_objar != "") {
                    Template temp(per_objar);
                    temp.setValue("field_name", fld->name);
                    temp.setValue("alloc_expr", GenObjarAllocExpression(fld));  // like "3:self%num1, self%num2"
                    temp.setValue("iter_index", GetObjarIterIndex(fld)); // like "iter%i(1)";
                    temp.setValue("type_name", cls->name);
                    str += temp.render();
                }
                return;
            }
            auto tyi = typemap.at(fld->type_name);
            if (choose_scalars && !tyi->is_scalar())
                return;
            if (choose_objects && tyi->is_scalar())
                return;
            if (tyi->is_transient())
                return;
            Template temp(per_field);
            temp.setValue("field_name", fld->name);
            temp.setValue("type_name", cls->name);

            str += temp.render();
        }

        string str{""};
        bool choose_scalars, choose_objects;
        string per_field, per_sub_object, per_objar;
        DDClass* cls;
    };

    MethodGenerator mg(per_field, per_sub_object, per_objar, choose_objects, cls);
    cls->accept(&mg);

    string base_call{""};
    if (cls->hasBaseClass())
        base_call += "                call self%" + cls->getFortranBaseClass() + "%" + method_name + "()\n";
    Template baseTemplate(base_call);
    baseTemplate.setValue("type_name", "");

    Template temp{"        subroutine " + method_name + "(self)\n" +
                  "                class({{type_name}}), intent(inout) :: self\n" +
                    prelude + baseTemplate.render() +
                    mg.str +
                  "        end subroutine\n"};

    temp.setValue("type_name", cls->name);


    return temp.render();
}


// here we go over all fields, but there are 2 dummy parameters: left, right
string GetAssignmentOperator(DDClass* cls,
                      const string& method_name,
                      const string& prelude,
                      const string& repeated,
                      const string& inout_left,
                      const string& inout_right,
                      const string& ending) {

    struct MethodGenerator : public GenVisitor {
        MethodGenerator(const string& repeated) :
                          repeated(repeated) {}

        void StartField(DDField* fld) {
            // object fields are treated like scalars, with the class assignment operator available
            if (typemap.count(fld->type_name) == 0 && !fld->is_child_obj && !fld->is_objar)
                return;
            if (!fld->is_child_obj && !fld->is_objar) {
                auto tyi = typemap.at(fld->type_name);
                if (tyi->is_transient())
                    return;
            }
            // here we go over all fields
            Template temp(repeated);
            temp.setValue("field_name", fld->name);

            str += temp.render();
        }

        string str{""};
        string repeated;
    };

    MethodGenerator mg(repeated);
    cls->accept(&mg);

    string base_call{""};
    if (cls->hasBaseClass())
        base_call += "                       lhs%" + cls->getFortranBaseClass() +
                                       " = rhs%" + cls->getFortranBaseClass() + "\n";
    Template baseTemplate(base_call);
    baseTemplate.setValue("type_name", "");

    Template temp{"        subroutine " + method_name +
                    "(lhs, rhs)\n" +
                    "                class({{type_name}}), intent({{inout_left}}) :: lhs\n" +
                    "                class(ObjectContainer), intent({{inout_right}}) :: rhs\n" +
                    prelude + base_call +
                    mg.str + ending +
                    "        end subroutine\n"};

    temp.setValue("type_name", cls->name);
    temp.setValue("inout_left", inout_left);
    temp.setValue("inout_right", inout_right);


    return temp.render();
}

string GenArrayColonAll(DDField* fld) {

    struct MethodGenerator : public GenVisitor {
        void GenInit(Initializer* init, DDField* fld) {
            if (!fld->is_objar) return;
            ++init_index;
            if (init_index > 1)
                str += ",";
            str += ":";

        }
        string str{""};
        int init_index{0};
    };
    MethodGenerator mg;
    fld->accept(&mg);
    return mg.str;
}


// here we go over all fields, but there are 2 dummy parameters: left, right
string GenIsEq(DDClass* cls,
                      const string& prelude,
                      const string& repeated,
                      const string& ending) {

    struct MethodGenerator : public GenVisitor {
        MethodGenerator(const string& repeated) :
                          repeated(repeated) {}

        void StartField(DDField* fld) {
            // object fields are treated like scalars - using the class comparison
            if (typemap.count(fld->type_name) == 0 && !fld->is_child_obj  && !fld->is_objar) {
                return;
            }

            if (!fld->is_child_obj && !fld->is_objar) {
                auto tyi = typemap.at(fld->type_name);
                if (tyi->is_transient())
                    return;
            }

            if (fld->is_objar) {
                string comparison_str = R"d(
                       iseq = iseq .and. (allocated(lhs%{{field_name}}) .eqv. allocated(rhs%{{field_name}}))
                       if (.not. iseq) return
                       if (allocated(lhs%{{field_name}})) then
                           iseq = iseq .and. all(shape(lhs%{{field_name}}) == shape(rhs%{{field_name}}))
                           if (.not. iseq) return
                           iseq = iseq .and. all(lhs%{{field_name}}({{colon_all_array}}) == rhs%{{field_name}}({{colon_all_array}}))
                        end if
)d";

                Template temp(comparison_str);
                temp.setValue("field_name", fld->name);
                temp.setValue("colon_all_array", GenArrayColonAll(fld));

                str += temp.render();
                return;
            }


            // here we go over all fields
            Template temp(repeated);
            temp.setValue("field_name", fld->name);
            if (fld->type_name == "_Bool")
                temp.setValue("eqv", ".eqv.");
            else
                temp.setValue("eqv", "==");

            str += temp.render();
        }

        string str{""};
        string repeated;
    };

    MethodGenerator mg(repeated);
    cls->accept(&mg);

    string base_call{""};
    if (cls->hasBaseClass())
        base_call += "                       iseq = iseq .and. (lhs%" + cls->getFortranBaseClass() +
                     " == rhs%" + cls->getFortranBaseClass() + ")\n" +
                     "                       if (.not. iseq) return\n";
    Template baseTemplate(base_call);
    baseTemplate.setValue("type_name", "");

    Template temp{string("        pure elemental function Is{{type_name}}Equal") +
                    "(lhs, rhs) result(iseq)\n" +
                    "                class({{type_name}}), intent(in) :: lhs\n" +
                    "                class(ObjectContainer), intent(in) :: rhs\n" +
                    prelude + base_call +
                    mg.str + ending +
                    "        end function\n"};

    temp.setValue("type_name", cls->name);
    return temp.render();
}


string GenAllocateObjectFields(DDClass* cls) {

    struct MethodGenerator : public GenVisitor {
        void StartField(DDField* fld) {
            if (typemap.count(fld->type_name) == 0) {
                if (fld->is_objar) {
                    string allocate_field =
                    "                allocate(self%{{field_name}}({{alloc_expr}}))\n";

                    Template temp(allocate_field);
                    temp.setValue("field_name", fld->name);
                    temp.setValue("alloc_expr", GenObjarAllocExpression(fld));
                    str += temp.render();
                }
                return;
            }
            auto tyi = typemap.at(fld->type_name);
            if (tyi->is_scalar() || !tyi->has_section())
                return;
            if (tyi->is_transient())
                return;

            string allocate_field =
               "                call self%{{field_name}}%init(";

            Template temp(allocate_field);
            temp.setValue("field_name", fld->name);

            str += temp.render();
            init_index = 0;
        }


        void GenInit(Initializer* init, DDField* fld) {
            if (fld->is_objar) return;
            ++init_index;
            if (init_index > 1)
                str += ",";
            str += init->root.FortranString(2);

        }

        void EndField(DDField* fld) {

            if (typemap.count(fld->type_name) == 0)
                return;
            auto tyi = typemap.at(fld->type_name);
            if (tyi->is_scalar() || !tyi->has_section())
                return;
            if (tyi->is_transient())
                return;

            str += ")\n";
        }


        string str{""};
        int init_index;
    };




    MethodGenerator mg;
    cls->accept(&mg);
    string tmpstr = "        subroutine " + string("Allocate{{type_name}}ObjectFields") +
                    "(self)\n" +
                    "                class({{type_name}}), intent(inout) :: self\n";
    if (cls->hasBaseClass())
        tmpstr += "                call self%" + cls->getFortranBaseClass() + "%AllocateObjectFields()\n";

    tmpstr += mg.str + "        end subroutine\n";

    Template temp(tmpstr);
    temp.setValue("type_name", cls->name);
    return temp.render();
}



struct SliceMethodImpGenerator : GenVisitor {
    SliceMethodImpGenerator(DDClass* cls) : cls(cls) {}
    void StartField(DDField* fld) {
        if (typemap.count(fld->type_name) == 0)
            return;
        auto tyi = typemap.at(fld->type_name);
        if (tyi->is_emtpy() || !tyi->has_section())
            return;
        if (tyi->is_transient())
            return;
        str += "        function Get" + capitalize(cls->name) + capitalize(fld->name) + "Extents(self) result(res)\n" +
               "                class({{type_name}}), intent(inout) :: self\n" +
               "                type(extent) :: res(" + to_string(tyi->get_dimension())+  ")\n";

        str += "                res = [";
        init_index = 0;
    }


    void EndField(DDField* fld) {
        if (typemap.count(fld->type_name) == 0)
            return;
        auto tyi = typemap.at(fld->type_name);
        if (tyi->is_emtpy() || !tyi->has_section())
            return;
        if (tyi->is_transient())
            return;
        str += "]\n";
        str += "        end function\n";

    }

    void GenInit(Initializer* init, DDField* fld) {
        if (fld->is_objar)
            return;
        ++init_index;
        if (init_index > 1)
            str += ",";
        str += init->root.FortranString(1);

    }

    string str{""};
    int init_index{0};
    DDClass* cls;
};


class FortranProcGen : public GenVisitor {
public:

    FortranProcGen(CreateDataDefinition* dd, const map<string, string>* options, ofstream& outf) : dd(dd), options(options), outf(outf) {
        outf << "\n    contains" << endl;
        dd->accept(this);
        outf << "\nend module" << endl;
    }

    void StartClass(DDClass* cls)  {



        outf << GenerateInitializer(cls);

        outf << GenSubroutine1(
                    cls,
                    "Store{{type_name}}ObjectFields",
R"d(                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
)d",
"                call self%{{field_name}}%StoreObject(ps, gid,  '{{field_name}}')\n",
"                call self%{{field_name}}%store(ps%FetchSubGroup(gid,  '{{field_name}}'))\n",
R"d(
                call iter%Init(lbound(self%{{field_name}}), ubound(self%{{field_name}}))
                do while (.not. iter%Done())
                    call self%{{field_name}}({{iter_index}})%store( &
                                ps%FetchIndexInSubGroup(gid, '{{field_name}}', iter%StringIndex(), .True.))
                    call iter%Inc()
                end do
)d",
                    true);

        outf << GenSubroutine1(
                    cls,
                    "Load{{type_name}}ObjectFields",
R"d(                class(persistent_store),pointer :: ps
                integer(HID_T) :: gid
                type(iterator) :: iter
                ps  => self%GetPersistentStore()
                gid = self%GetGroupId()
)d",
"                call self%{{field_name}}%LoadObject(ps, gid,  '{{field_name}}')\n",
"                call self%{{field_name}}%load(ps%FetchSubGroup(gid,  '{{field_name}}'))\n",
R"d(
                allocate(self%{{field_name}}({{alloc_expr}}))
                call iter%Init(lbound(self%{{field_name}}), ubound(self%{{field_name}}))
                do while (.not. iter%Done())
                    call self%{{field_name}}({{iter_index}})%load( &
                                ps%FetchIndexInSubGroup(gid, '{{field_name}}', iter%StringIndex(), .False.))
                    call iter%Inc()
                end do
)d",
                    true);
  // TODO: load should not fetch the subgroup but only "get" it (and fail if it doesn't exist

        outf << GenSubroutine1(
                    cls,
                    "Reset{{type_name}}SectionFields",
                    "",
                    "                self%{{field_name}}_ => self%{{field_name}}%GetWithExtents(self%Get{{type_name}}{{field_name}}Extents())\n",
                    "",
                    "",
                    true);

        outf << GenSubroutine1(
                    cls,
                    "Disconnect{{type_name}}ObjectFields",
"               type(iterator) :: iter\n",
                    "                call self%{{field_name}}%DisconnectFromStore()\n",
                    "                call self%{{field_name}}%disconnect()\n",
R"d(
                call iter%Init(lbound(self%{{field_name}}), ubound(self%{{field_name}}))
                do while (.not. iter%Done())
                    call self%{{field_name}}({{iter_index}})%disconnect()
                    call iter%Inc()
                end do
)d",
                    true);


        outf << GenSubroutine1(
                cls,
                "Store{{type_name}}ScalarFields",
                "",
                "                call self%write('{{field_name}}', self%{{field_name}})\n",
                "",
                    "",
                false);


        outf << GenSubroutine1(
                cls,
                "Load{{type_name}}ScalarFields",
                "",
                "                call self%read('{{field_name}}', self%{{field_name}})\n",
                "",
                    "",
                false);


        // here there is no call to the base class' finalize
        Template finalize("        subroutine Finalize{{type_name}}(self)\n               type({{type_name}}), intent(inout) :: self\n"\
                          "               call self%disconnect()\n        end subroutine\n");
        finalize.setValue("type_name", cls->name);
        outf << finalize.render();


        Template ClearSubroutine(
            "        subroutine Clear{{type_name}}(self)\n" \
            "                class({{type_name}}), intent(inout) :: self\n" \
            "                type({{type_name}}), save :: empty\n" \
            "                self = empty\n" \
            "        end subroutine\n");
        ClearSubroutine.setValue("type_name", cls->name);
        outf << ClearSubroutine.render();


        outf << GenIsEq(
                cls,
                "                logical :: iseq\n"\
                "                iseq = .True.\n"\
                "                select type(rhs)\n"\
                "                   type is ({{type_name}})\n",

                "                       iseq = iseq .and. (lhs%{{field_name}} {{eqv}} rhs%{{field_name}})\n"\
                "                       if (.not. iseq) return\n",

                "                   class default\n"\
                "                       iseq = .False.\n"\
                "                 end select\n"
            );


        outf << GetAssignmentOperator(
                cls,
                "AssignmentOperator{{type_name}}",
                "                select type(rhs)\n"\
                "                   type is ({{type_name}})\n",
                "                       lhs%{{field_name}} = rhs%{{field_name}}\n",
                "inout",
                "in",
                "                       call lhs%ResetSectionFields()\n"\
                "                   class default\n"\
                "                 end select\n"
            );



       SliceMethodImpGenerator gen(cls);
       cls->accept(&gen);
       Template temp(gen.str);
       temp.setValue("type_name", cls->name);
       outf << temp.render();


       outf << GenAllocateObjectFields(cls);

       outf << endl;

       GenUserFunctions(cls,outf);
    }


private:
    CreateDataDefinition* dd;
    ostream&  outf;
    const map<string, string>* options;
};

//-----------------------------------------------------------
// little experiment - this code is unused

template<typename... T>
std::array<typename std::common_type<T...>::type, sizeof...(T)>
make_array(T &&...t) {
    return {std::forward<T>(t)...};
}


// --------------------------------------------------------------------
//
//                   Python Code Generation
//
//---------------------------------------------------------------------


map<string, string> pyAttributeTypes{
    {"int", "AttributeTypes.INT"},
    {"float", "AttributeTypes.FLOAT"},
    {"class complex", "AttributeTypes.COMPLEX"},
    {"struct string", "AttributeTypes.STRING"},
    {"_Bool", "AttributeTypes.BOOLEAN"},
    {"struct datetime", "AttributeTypes.DATE"},

};

class PyGen  : public GenVisitor {
public:

    PyGen(CreateDataDefinition* dd, const map<string, string>* options,
          const set<string>& modules,
          ofstream& outf) : dd(dd), options(options), outf(outf) {
        outf <<  R"d('''
This code was generated from a schema header file. (TODO: add name)
by program (TODO: add name)

@author: adler
'''

from portobello.bus.persistence import Persistent, Store, AttributeTypes
import numpy as np
import abc
from ctypes import c_int, c_float
from portobello.bus.fortran import Fortran
from nptyping import NDArray, Int32, Float64, Complex128
from typing import Any, Optional, Dict

)d";
        string module_uses;
        for (auto module : modules) {
            outf << "from portobello.generated." << module << " import *\n";
        }

        dd->accept(this);

        // write something at the end so that we know it's not truncated
        outf << "\n# this is the end of the generated file";
    }


    void StartClass(DDClass* cls)  {
        Template  class_def(R"d(
class {{clsname}}({{baseclass}}):
    def __init__(self, **kwargs):
        self.__InitMetadata__(False)
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __InitMetadata__(self, superclass):
        {{baseclass}}.__InitMetadata__(self, True)

)d");

        if (cls->hasBaseClass())
            class_def.setValue("baseclass", cls->getFortranBaseClass());
        else
            class_def.setValue("baseclass", "Persistent");
        class_def.setValue("clsname", cls->name);
        outf << class_def.render();

        //for (auto method: cls->methods_to_bind["IN_FORTRAN_"]) {
        //    cerr << "to bind:" << method << endl;
        //}
    }

    void EndClass(DDClass* cls) {
        outf <<  R"d(

)d";

        for (shared_ptr<DDFunction> func : cls->functions) {
            if (func->is_python) {
                // create an abstract method
                outf << "    @classmethod" << endl;
                outf << "    @abc.abstractmethod"  << endl;
                outf << "    def " << func->name << "(cls, self";
                for (auto param : func->params)
                   outf << ", " << param.name;
                outf << "):\n        return" << endl;
            } else {
                // generate python call to the wrapper
                outf << "    @classmethod" << endl;
                outf << "    def " << func->name << "(cls, selfpath";
                for (auto param : func->params)
                   outf << ", " << param.name;
                outf << "):" << endl;
                outf << "        fort = Fortran.Instance()" << endl;
                outf << "        return " <<
                    "fort.CallWrapper(\"" << cls->name_space << "\",\"" <<
                           func->imp_module << "\", \"" << func->name << "\",";
                outf << endl << "            selfpath";
                for (auto param : func->params) {
                   if (param.paramtype == "int")
                     outf << ", c_int(" << param.name << ")";
                   else if (param.paramtype == "float")
                     outf << ", c_float(" << param.name << ")";
                   else if (param.paramtype == "class complex") {
                      // not supported yet - need to be passed as pointer, and converted
                   } else if (param.paramtype == "const struct string &") {
                     outf << ", " << param.name;
                   } else if (param.paramtype == "class BusObject &") {
                     outf << ", GetPersistentPath(" << param.name << ")";
                   }
                }
                outf << ")" << endl;
            }
        }

    }

    string getPyShape(DDField* fld) {
        string shape = "\'(";
        int count = 0;
        for (auto init : fld->initializers) {
            count += 1;
            if (count != 1) shape += ", ";
            shape += init->root.PythonLength();
        }
        if (count ==1) shape += ",";
        shape+= ")\'";
        return shape;
    }
    
    // Add type hint, something like NDArray[(Any, Any, Any), Complex128] 
    string getPyTypeHint(DDField* fld, const string& type_hint) {
        string shape = "NDArray[(";
        int count = 0;
        for (auto init : fld->initializers) {
            count += 1;
            if (count != 1) shape += ", ";
            shape += "Any";
        }
        if (count ==1) shape += ",";
        shape+= "), " + type_hint + "]";
        return shape;
    }

    void StartField(DDField* fld) {
        if (fld->is_child_obj) {
            // this is the bode for objects
            string ns = (fld->name_space == "" ? "globals" : fld->name_space);
            outf <<
"        self._objects['" << fld->name << "'] = ('" << ns << "', '" <<
            fld->obj_type << "')"<< endl;

            outf <<
"        self." << fld->name << " = " << fld->obj_type << "()" << endl;
            return;
        }

        if (fld->is_objar) {
            string ns = (fld->name_space == "" ? "globals" : fld->name_space);
            outf <<
"        self._objars['" << fld->name << "'] = ( ('" << ns << "', '" <<
            fld->obj_type << "'), " << getPyShape(fld) << ") " << endl;

            outf <<
"        self." << fld->name << " : NDarray[(Any,), " << ns << "." << fld->obj_type << "]" << endl;

            outf <<
"        self." << fld->name << " = None" << endl;

            return;
        }

        if (typemap.count(fld->type_name) == 0) {
            return;
        }

        auto tyi = typemap.at(fld->type_name);

        if (tyi->is_scalar()) {
            auto py_type = pyAttributeTypes.at(fld->type_name);
            outf <<
"        self._attributes['" << fld->name << "'] = " << py_type << endl;
            outf <<
"        self." << fld->name  << tyi->get_PY_initializer_def(fld) << endl;
        } else {
            // this is for a dataset
            const string& np_type = tyi->get_numpy_element_type();
            const string& np_type_hint = tyi->get_numpy_type_hint();
            
            outf <<
"        self._datasets['" << fld->name << "'] = (" << np_type << ", "<< getPyShape(fld) << " )" << endl;

            outf <<
"        self." << fld->name << " : " << getPyTypeHint(fld, np_type_hint) << endl;

            outf <<
"        self." << fld->name << " = None" << endl;
        }
    }

private:
    CreateDataDefinition* dd;
    ostream&  outf;
    const map<string, string>* options;
};

auto usage_str = R"""(
  usage:
    schema2code <PORTOBELLO_ROOT> <list of header files to parse, for example schema/*.h>
  This will generate code under the portobello root dir for the selected list of header files.
)""";

int main(int argc, char **argv) {
    if (argc <=2) {
        cerr << usage_str;
        return 1;
    }

    auto basedir = string(argv[1]);

    for (int i=2; i<argc; i++) {
        CreateDataDefinition dd;
        {
            ifstream t(argv[i]);
            clang::tooling::runToolOnCodeWithArgs(
                unique_ptr<RunVisitingConsumer>(new RunVisitingConsumer(&dd)),
                header + string((istreambuf_iterator<char>(t)),istreambuf_iterator<char>()),
                vector<string>{"-std=c++17", "-fsyntax-only", "-I"+basedir+"/schema/"});
        }

        // this is the command line to see the syntax tree:
        // clang-5.0 -std=c++17 -Xclang -ast-dump -fsyntax-only -I. <filename>
        // make sure the file includes the debug header, __for_debug.h

        FortranUseCollector uses(&dd);
        auto module_name = dd.name_space;

        if (module_name == "") module_name = "globals";  // TODO: this could be a command line argument

        auto fortfile = basedir + "/generated-code/" + module_name + ".f90";
        ofstream fortran_fout(fortfile);
        cerr << "generating fortran file: " << fortfile << endl;
        FortranDefGen generate_code(&dd, {}, uses.collected, fortran_fout);
        FortranProcGen generate_procedures(&dd, {}, fortran_fout);
        fortran_fout.flush();

        string pyfile = basedir + "/portobello/generated/" + module_name + ".py";
        cerr << "generating python file: " << pyfile << endl;
        ofstream py_fout(pyfile);
        PyGen generate_python_module(&dd, {}, uses.pydeps, py_fout);
        py_fout.flush();
    }

}
