

module DFTPlusSigma

    use Rho, only: RhoInBands, RhoInfo
    implicit none


    private


    interface
        subroutine FortranCBForPreparingSigmaOrRho() bind(C)
            use iso_c_binding
        end subroutine
    end interface

    procedure(FortranCBForPreparingSigmaOrRho), pointer :: PrepareSigmaOrRho => null()

    integer :: num_valence_electrons = 0
    real*8  :: sum_valence_electrons = 0.0 ! current sum of fermi functions with current chemical potential
    logical :: has_correlated_rho = .False.

    public :: OneIteration
    public :: GetConvergenceImp,GetOmegaMeshImp,DFTPlusSigmaOneIterationImp,SetGreensFunction
    public :: PrepareSigmaOrRho, ReadSigmaFromDMFT, set_g, is_dft_spin_polarized
    public :: SetKohnShamBandsPlusSigmaXAndRotateSigc
    public :: MarkGutzwillerDensityIsReady

    real*8, allocatable :: eps(:,:,:), eps2(:), gutz_qp_weight(:,:,:) ! backup of e_band in our shard
    complex*16, allocatable :: Hqp(:,:,:)
    integer :: min_band, max_band
    logical :: is_dft_plus_g, is_dft_spin_polarized

contains

    subroutine SetQPEigenvalues()
        use manager_mod
        use parallel_mod
        use solid_mod
        use mpi
        type(RhoInBands),allocatable :: rhob
        type(RhoInfo),   allocatable :: rinfo
        integer :: ispin, ind_k, k, ierr, bnd, i, j

        if (.not. allocated(Hqp)) return ! not ready yet
        if (.not. allocated(eps)) then
            allocate(eps(nbndf, npnt, nspin_0))
        endif
        
        if (.not. allocated(qp_spectr_weight)) then
            allocate(qp_spectr_weight(nbndf,maxval(ndim3_k),nspin_0))
        endif

        if (.not. allocated(e_qp)) then
            allocate(e_qp(nbndf, npnt, nspin_0))
        endif
    
        allocate(eps2(nbndf))
        ! first keep backup of the e_bnd
        eps(:,:,:) = e_bnd(:,:,:) 
        e_bnd = 0.d0 ! necessary because we sum later over all shards, and do sorting only within ours
        e_qp = 0.0   ! same comment
        do ispin=1,nspin
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                ! only set in our shard
                ! merge-sort the epsilon and Hqp bands
                eps2(:) = 0.d0
                eps2(:min_band-1) = eps(:min_band-1, k,ispin) 
                eps2(min_band:n_bnd(k, ispin)+min_band-max_band-1) = eps(max_band+1:n_bnd(k, ispin), k,ispin) 
                i = 1
                j = 1
                do bnd=1,n_bnd(k,ispin)
                    if (j > max_band-min_band+1) then ! all Hqp matrix used
                        e_bnd(bnd, k, ispin) = eps2(i)
                        e_qp(bnd, k, ispin) = eps2(i)
                        qp_spectr_weight(bnd, ind_k, ispin) = 1.d0
                        i=i+1
                    else if (eps2(i) < dreal(Hqp(j, ind_k, ispin))) then 
                        e_bnd(bnd, k, ispin) = eps2(i)
                        e_qp(bnd, k, ispin) = eps2(i)
                        qp_spectr_weight(bnd, ind_k, ispin) = 1.d0
                        i=i+1
                    else
                        e_bnd(bnd, k, ispin) = dreal(Hqp(j, ind_k, ispin))
                        e_qp(bnd, k, ispin) = Hqp(j, ind_k, ispin)
                        qp_spectr_weight(bnd, ind_k, ispin) = gutz_qp_weight(j, ind_k, ispin)
                        j=j+1
                    end if
                end do
            end do
        end do
        ! and now add up all shards
        if(nproc_k/=1) then
            call MPI_BARRIER(MPI_COMM_WORLD, ierr)
            call MPI_ALLREDUCE(MPI_IN_PLACE, e_bnd, size(e_bnd), MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, ierr)
             ! e_qp - we sum them up just for tidiness - only the shard's values are used
            call MPI_ALLREDUCE(MPI_IN_PLACE, e_qp, size(e_qp), MPI_DOUBLE_COMPLEX, MPI_SUM, MPI_COMM_WORLD, ierr)
            call MPI_BARRIER(MPI_COMM_WORLD, ierr)
        endif
        deallocate(eps2)
    end subroutine

    subroutine UnSetQPEigenvalues()
        use manager_mod
        use parallel_mod
        use solid_mod
        e_bnd(:,:,:) = eps(:,:,:)
    end subroutine

    function GetConvergenceImp(selfpath) result(res)
        use stringifor
        use manager_mod
        use FlapwMBPT_interface, only: Convergence
        use RhoMixing, only: charge_convergence, mag_convergence

        type(string), intent(inout) :: selfpath
        type(string) :: res

        type(Convergence), allocatable :: cvg

        allocate(cvg)

        cvg%num_valence_electrons = num_valence_electrons
        cvg%sum_valence_electrons = sum_valence_electrons
        cvg%charge_convergence = charge_convergence
        cvg%mag_convergence = mag_convergence

        call cvg%store(selfpath%chars(),flush_opt=.True. )

        deallocate(cvg)

        res = selfpath
    end function

    function GetOmegaMeshImp(selfpath) result(res)
        use stringifor
        use self_energies
        use manager_mod

        type(string), intent(inout) :: selfpath
        type(string) :: res

        type(OmegaMesh), allocatable :: om

        allocate(om)

        om%n_omega = n_omega
        call om%allocate()
        om%omega_(0:n_omega) = w_omega(0:n_omega)
        om%index_(0:n_omega) = mats_num_omega(0:n_omega)

        call om%store(selfpath%chars(),flush_opt=.True. )

        deallocate(om)

        res = selfpath
    end function

    subroutine set_g(calculate_energy)
        use atom_mod
        use etot_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        implicit none
        logical :: correlation, calculate_energy
        character*8 :: way
        integer :: k,ispin,n,ind_tau,i_omega,i_tau,ind_k,i,it,ind_omega
        real*8 :: w_n,ta,cf(2)
        real*8, allocatable :: gx_tau(:,:)
        complex*16, allocatable :: gx_omega(:),tmp(:,:)
        correlation=.false.
        if(ubi==' qp'.or.ubi==' gw')  correlation=.true.
        call etot_gw_0
        allocate(tmp(nbndf,nbndf))
        if(correlation) then
            allocate(gc_omega(nbndf,nbndf,2,ndim3_omega))
            allocate(gc_tau(nbndf,nbndf,2,ndim3_tau))
        endif
        if(ubi==' gw') way='GW_based'
        if(ubi==' qp') way='QP_based'
        do ispin=1,nspin
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                n=n_bnd(k,ispin)
                allocate(gx_tau(n,2))
                call g_x_tau(ispin,k,gx_tau,betta_t,n,chem_pot)
                if(correlation) then
                    if(way=='GW_based') then
                        allocate(gx_omega(n))
                        gc_omega=0.d0
                        do ind_omega=1,ndim3_omega
                            i_omega=me3_tau*ndim3_omega+ind_omega-1
                            w_n=w_omega(i_omega)
                            ! ------ We temporarily place SIGMA_C into TMP --------------------
                            call ferm_unpack_omega(tmp,&
                                sig_c_omega(1,1,1,ind_omega,ind_k,ispin),&
                                n,nbndf,nbndf)
                            call g_x_omega(ispin,k,gx_omega,w_n,n,chem_pot)
                            call g_c_omega(0,gx_omega,tmp,n,nbndf)
                            call ferm_pack_omega(tmp,gc_omega(1,1,1,ind_omega),n,&
                                nbndf,nbndf)
                        enddo  !! over ind_omega
                        deallocate(gx_omega)
                        call from_omega_to_tau_baa(gc_omega,nbndf,gc_tau,nbndf,n)
                    else if(way=='QP_based') then
                        call qp_green(ispin,ind_k)
                    endif
                    call output_gc_band_tau(ispin,ind_k,nbndf)
                    call output_gc_band_omega(ispin,k,nbndf)
                    call output_gx_band_tau(ispin,k,nbndf)
                    call output_sigc_band_tau(ispin,ind_k,nbndf)
                endif
                ! ------ Contributions to E_tot --------------------------------------
                if (calculate_energy) call etot_gw_1(ispin,k,ind_k,gx_tau,n)

                if(correlation) then  !! QP:GWG
                    g_full(:,:,:,:,ind_k,ispin)=gc_tau
                    do ind_tau=1,ndim3_tau
                        i_tau=me3_tau*ndim3_tau+ind_tau-1
                        ta=tau_mesh(i_tau)
                        call g_x_tau(ispin,k,gx_tau,ta,n,chem_pot)
                        ta=betta_t-tau_mesh(i_tau)
                        call g_x_tau(ispin,k,gx_tau(1,2),ta,n,chem_pot)
                        do i=1,n
                            cf(1)=gx_tau(i,1)-gx_tau(i,2)
                            cf(2)=gx_tau(i,1)+gx_tau(i,2)
                            do it=1,2
                                g_full(i,i,it,ind_tau,ind_k,ispin)=&
                                    g_full(i,i,it,ind_tau,ind_k,ispin)+cf(it)
                            enddo
                        enddo
                    enddo  !! over ind_tau
                endif
                deallocate(gx_tau)
                if(correlation) then
                    allocate(gx_omega(n))
                    do ind_omega=1,ndim3_omega
                        i_omega=me3_tau*ndim3_omega+ind_omega-1
                        call g_x_omega(ispin,k,gx_omega,w_omega(i_omega),n,chem_pot)
                        call ferm_unpack_omega(tmp,gc_omega(1,1,1,ind_omega),n,nbndf,nbndf)
                        do i=1,n
                            tmp(i,i)=tmp(i,i)+gx_omega(i)
                        enddo
                        call ferm_pack_omega(tmp,gc_omega(1,1,1,ind_omega),n,nbndf,nbndf)
                    enddo
                    deallocate(gx_omega)
                endif
            enddo  !! over ind_k
        enddo  !! over ispin
        deallocate(tmp)
        if(correlation) deallocate(gc_omega,gc_tau)
        call timel('******* G_KS_TAU finished **********')
        ! ------------- Forming G_FULL(0-) --> G_FULL_0 ------------------------
        call green_0(correlation)
        ! ------------- Forming Local Green's Function -------------------------
        call g_mt_from_g_full(correlation)
    end subroutine



    subroutine MarkGutzwillerDensityIsReady()
        has_correlated_rho = .True.
        is_dft_plus_g = .True.
    end subroutine


    ! stitching point for gutzwiller
    subroutine g_mt_from_g_full(correlation)
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        use Rho, only: RhoInBands, RhoInfo
        implicit none
        logical, intent(in) :: correlation
        integer :: k,iatom,isort,ie,ind_k,n,ispin,i,j
        complex*16 :: wei
        complex*16, allocatable :: tmp(:,:),tmp1(:,:),tmp2(:,:)
        type(RhoInBands),allocatable :: rhob
        type(RhoInfo),   allocatable :: rinfo

        g_loc_0=(0.d0,0.d0)
        allocate(tmp(maxel,nbndf))
        allocate(tmp1(nbndf,nbndf))
        allocate(tmp2(nbndf,nbndf))

        ! Here we (potentially) read the rho in band representation (in a band window) from portobello (the bus)
        if (has_correlated_rho .and. is_dft_plus_g) then
            allocate(rhob)
            allocate(rinfo)
            call rinfo%load("./rho.core.h5:/info")
            call rhob%load(rinfo%location%chars())
            if (allocated(Hqp) .and. (max_band /= rhob%max_band .or. min_band /= rhob%min_band)) then 
                deallocate(Hqp)
                deallocate(gutz_qp_weight)
            end if
            if (.not. allocated(Hqp)) then 
                allocate(Hqp(rhob%max_band-rhob%min_band+1, ndim3_k(me3_k+1), nspin_0))
                allocate(gutz_qp_weight(rhob%max_band-rhob%min_band+1, ndim3_k(me3_k+1), nspin_0))
            end if
            Hqp(:,:,:) = rhob%Hqp_(:,:,:)
            gutz_qp_weight = rhob%Z_(:,:,:)
            max_band = rhob%max_band
            min_band = rhob%min_band
            
        end if

        do ispin=1,nspin
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                n=n_bnd(k,ispin)
                wei=dcmplx(wgt(k),0.d0)
                tmp1=(0.d0,0.d0)
                if(.not. correlation) then
                    do i=1,n
                        tmp1(i,i)=g_full_00(i,ind_k,ispin)
                    enddo
                    !   ------------------------------------------------------------------------
                    !   Here set the bands within the window => this is the crux of rhobusta
                    !   ------------------------------------------------------------------------
                    if (allocated(rhob)) then
                        tmp1(rhob%min_band:rhob%max_band, rhob%min_band:rhob%max_band) = &
                            rhob%rho_(:, :, ind_k, ispin)
                    end if
                else
                    tmp1(1:n,1:n)=g_full_0(1:n,1:n,ind_k,ispin)
                endif
                do iatom=1,natom
                    isort=is(iatom)
                    ie=io_lem(iatom)
                    call zgemm('n','n',lfunm(isort),n,n,(1.d0,0.d0),&
                        z_bnd(ie,1,ind_k,ispin),nfun,tmp1,nbndf,&
                        (0.d0,0.d0),tmp,maxel)
                    call zgemm('n','c',lfunm(isort),lfunm(isort),n,wei,tmp,&
                        maxel,z_bnd(ie,1,ind_k,ispin),nfun,(1.d0,0.d0),&
                        g_loc_0(1,1,iatom,ispin),maxel)
                enddo                 !! over iatom
            enddo                   !! over ind_k
        enddo                     !! over ispin
        deallocate(tmp,tmp1,tmp2)
        if(nproc_k/=1) call DGOP(g_loc_0,2*maxel**2*natom*nspin_0,'  +', comm_pnt)
        !     ------- Symmetrization -----------------------------------------------
        do ispin=1,nspin
            call symg_loc(g_loc_0(1,1,1,ispin))
            g_loc_0(:,:,:,ispin)=conjg(g_loc_0(:,:,:,ispin))
        enddo
        if (allocated(rhob)) deallocate(rhob)
        if (allocated(rinfo)) deallocate(rinfo)
    end


    ! copied for the Gutzwiller stitching point at g_mt_from_g_full
    subroutine set_g00
        use atom_mod
        use etot_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        implicit none
        logical :: correlation
        integer :: k,ispin,i,ind_k
        correlation=.false.
        if(ubi==' qp'.or.ubi==' gw') correlation=.true.
        if(ubi==' gw'.or.ubi==' qp') then
            if(ubi_0=='dft'.or.ubi_0==' hf') then
                e_qp=e_bnd
                chem_pot_qp=chem_pot
                z_qp=z_bnd
                ev_qp=ev_bnd
                q_qp=(0.d0,0.d0)
                do ispin=1,nspin
                    do ind_k=1,ndim3_k(me3_k+1)
                        k=n3_mpi_k(me3_k+1)+ind_k
                        do i=1,n_bnd(k,ispin)
                            q_qp(i,i,ind_k,ispin)=(1.d0,0.d0)
                        enddo
                    enddo
                enddo
            endif
        endif
        if(ubi==' gw'.and.ubi_0/=' gw') then
            do ispin=1,nspin
                do ind_k=1,ndim3_k(me3_k+1)
                    call set_g_tau_0(ispin,ind_k,0, g_full(1,1,1,1,ind_k,ispin))
                enddo
            enddo
        endif
        if(ubi==' qp'.and.ubi_0/=' qp'.and.ubi_0/=' gw') then
            do ispin=1,nspin
                do ind_k=1,ndim3_k(me3_k+1)
                    call set_g_tau_0(ispin,ind_k,0, g_full(1,1,1,1,ind_k,ispin))
                enddo
            enddo
        endif
        if((iter/=1)) then
            !     ------------- Forming G_FULL(0-) --> G_FULL_0 ------------------------
            call green_0(correlation)
            !     ------------- Forming Local Green's Function -------------------------
            call g_mt_from_g_full(correlation)
        endif
        !     ----------------------------------------------------------------------
    end

    subroutine ReadSigmaFromDMFT(location_of_sigma, seperate_high_freq)
        use atom_mod
        use etot_mod
        use manager_mod

        use parallel_mod
        use solid_mod
        use self_energies
        use dmft
        use penf_stringify

        type(string), intent(in) :: location_of_sigma
        logical, intent(in) :: seperate_high_freq
        !real*8 :: w_n
        integer :: ispin, ind_k, k, n, i_omega,  ind_omega, min_band, max_band, max_i_omega

        type(OmegaFunctionInBands), allocatable :: sigbb
        type(LocalOmegaFunctionInfo), allocatable ::sigbb_info
        integer :: num_bands
        complex(kind=dp), allocatable :: tmp(:,:)
        integer :: allocerr

        allocate(sigbb)
        allocate(sigbb_info)

        call sigbb_info%load(location_of_sigma%chars())

        call sigbb%load(sigbb_info%location%chars())

        !print *, "- after reading self energy from dmft, my #irr k's=", ndim3_k(me3_k+1)

        if (.not. allocated(sig_c_omega)) then
            allocate(sig_c_omega(nbndf,nbndf,2,ndim3_omega, ndim3_k(me3_k+1),nspin), stat=allocerr)
            if (allocerr /= 0) then
                print *, "***** could not allocate the self energy array ****", nbndf,nbndf,nspin,ndim3_omega, ndim3_k(me3_k+1),nspin
                print *, "Total required size in G:", nbndf*nbndf*nspin*ndim3_omega* ndim3_k(me3_k+1)*nspin * 32.0 / 1024/ 1024/1024
                call ENDING
            end if
        end if

        sig_c_omega = 0.d0
        sig_c_tau = 0.d0
        sigx_solid_k = 0.0d0

        num_bands = sigbb%max_band - sigbb%min_band  + 1
        min_band = sigbb%min_band
        max_band = sigbb%max_band

        !print *, " - bands in embedding: ",  min_band, max_band, "total", num_bands
        !print *, " --- num of kpoints:", ndim3_k(me3_k+1), me3_k

        allocate(tmp(nbndf, nbndf))

        do ispin=1,nspin
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                n=n_bnd(k,ispin)
                max_i_omega = me3_tau*ndim3_omega+ndim3_omega-1
                do ind_omega=1,ndim3_omega
                    i_omega=me3_tau*ndim3_omega+ind_omega-1
                    !w_n=w_omega(i_omega)

                    tmp(:,:) = (0.d0,0.d0)
                    tmp(min_band:max_band, min_band:max_band) = sigbb%M_(1:num_bands, 1:num_bands ,i_omega, ind_k, ispin)
                    if (seperate_high_freq) then
                        tmp(min_band:max_band, min_band:max_band) = tmp(min_band:max_band, min_band:max_band) -  sigbb%Minf_(1:num_bands, 1:num_bands, ind_k, ispin)
                    end if

                    call ferm_pack_omega(tmp(1,1), &   ! input complex
                                         sig_c_omega(1, 1, 1, ind_omega, ind_k, ispin), & !output real
                    n, nbndf, nbndf)   ! TODO: should the first number be n or nbdnf?
                end do

                if (seperate_high_freq) then
                    tmp(:,:) = (0.d0,0.d0)
                    tmp(min_band:max_band, min_band:max_band) = sigbb%Minf_(1:num_bands, 1:num_bands, ind_k, ispin)
                    call pack_hermit(tmp,sigx_solid_k(1,1,ind_k,ispin),n,nbndf,nbndf, 0.d0, 1.d0)
                end if

            end do
        end do

        deallocate(sigbb, tmp, sigbb_info)

    end subroutine



    function DFTPlusSigmaOneIterationImp(selfpath, sigma_location, checkpoint, search_mu, update_lapw_basis,&
            current_admix, save_potentials, mu_search_variant) result(res)
        use stringifor
        use iso_c_binding
        use manager_mod, only: iter, ubi, admix
        use flapw_image, only: StoreImage, WriteRho, core_rho_written

        type(string), intent(inout) :: selfpath, sigma_location, mu_search_variant
        type(string) :: res
        integer(kind=c_int), intent(in),value  :: checkpoint, search_mu, update_lapw_basis, save_potentials
        real(kind=c_float), intent(in), value :: current_admix
        integer :: ierr

        res = selfpath

        admix = current_admix

        if (.not. core_rho_written) call WriteRho()
        call OneIteration(sigma_location, search_mu==1, update_lapw_basis==1, .True., mu_search_variant)
        iter = 3  ! not a restart iter, and not the first, and we need to recalc g0

        ! for debug - plot current bs
        if (checkpoint /= 0) then
            call WriteRho()
            call StoreImage()
        end if

    end function

    subroutine register_prepare_sigma_or_rho_cb(cb) bind(C,name="dftplussigma_mp_register_prepare_sigma_or_rho_cb_")
        use iso_c_binding
        type(c_funptr),intent(in),value        :: cb
        call c_f_procpointer(cb, PrepareSigmaOrRho)
    end subroutine

    subroutine SetGreensFunction(location_of_sigma, search_mu, update_lapw_basis, save_potentials, mu_search_variant)
        use atom_mod
        use manager_mod

        use parallel_mod
        use solid_mod

        use stringifor
        use persistence
        use dmft
        implicit none

        logical, intent(in) :: search_mu, update_lapw_basis, save_potentials
        type(string), intent(in) :: location_of_sigma, mu_search_variant
        logical :: dft_plus_sig
        character(120) :: chars ! what is a better way to do this? gfort-9 crashes on assignment of the section, this is a hack.

        is_dft_plus_g = .false.
        dft_plus_sig = (location_of_sigma /= "")
        
        if (associated(PrepareSigmaOrRho))  then
            has_correlated_rho = .True.
            if (maswrk) print *, "- writing density matrix rho.h5:/bands"
            call PrepareSigmaOrRho ! here the bands are defined - we recompute the projector in the band basis and embed sigma
            if (.not. dft_plus_sig) is_dft_plus_g = .true.
        end if
        
        if (dft_plus_sig) then
            call ReadSigmaFromDMFT(location_of_sigma, .false.)   ! this reads sigma from the Portobello object to Kutepov's array
        end if

        if(iter==1) then
            chem_pot = minval(e_bnd(n_semic+1,:,1:nspin))+0.5d0
        endif

        ! save the lda vlaues
        if (.not. allocated(e_lda)) then
            allocate(e_lda(nbndf,npnt,nspin))
            e_lda=e_bnd
            chem_pot_lda=chem_pot
        end if

        !The final check is for the mpi=0 case where we start from DFT, 
        !    in which case iter=3 and yet we haven't embedded a rho yet
        if ( search_mu .and. (iter/=2) .and. (.not. is_dft_plus_g .or. allocated(Hqp)) ) then 
            if (dft_plus_sig .and. maswrk) print *, "- searching for mu, current value:", chem_pot

            if (is_dft_plus_g) then
                call SetQPEigenvalues()
                chars = mu_search_variant%chars()
                ubi = chars(1:3)
            end if

            call search_mu_0(chem_pot,nelec,chem_pot)

            if (is_dft_plus_g) then
                call UnSetQPEigenvalues()
                ubi = 'dft'
            end if

            if (.not. dft_plus_sig .and. maswrk) print *, "mu=",chem_pot
        end if

        if (dft_plus_sig .and. maswrk) print *, "- recalculating G(G0, Sigma, mu), mu=", chem_pot
        call set_g(.false.)

        call timel('******** Set_G finished ************')              
        ! -----------------------------------------------------------------
        if (update_lapw_basis) call seteny

        ! -----------------------------------------------------------------

        if(iter_dft<iter_max_scf.and.iter==iter_dft) then
            v_h_bb_old=v_h_bb
            if(ubi=='dft') then
                v_xc_bb_old=v_xc_bb
            else
                sigx_solid_k_old=sigx_solid_k
            endif
        endif
    end

    !!
    !! Corresponds to Kutepov's solid_scf
    !!
    subroutine OneIteration(location_of_sigma, search_mu, update_lapw_basis, save_potentials, mu_search_variant)
        use atom_mod
        use etot_mod
        use manager_mod
        use parallel_mod
        use solid_mod

        use stringifor
        use persistence
        use mpi

        use RhoMixing, only: mixro1_RHOBUSTA,charge_convergence
        use flapw_image, only: WriteRho

        implicit none
        type(string), intent(in) :: location_of_sigma, mu_search_variant
        logical, intent(in) :: search_mu, update_lapw_basis, save_potentials
        logical :: correlation
        real(kind=dp) :: old_chem_pot
        integer :: n, ierr

        correlation = (location_of_sigma /= "")

        if (allocated(g_full_0)) then
            if (correlation .and. maswrk) print *, "- using the correlated density matrix, with sigma"
        else
            if (correlation .and. maswrk)  print *, "- using the one-particle density matrix, with sigma"
        end if

        if(maswrk) write(iun,1010)iter
        ubi_0=ubi
        if(nspin/=nspin_1) nspmag=max(magn,nspin)
        ! ---------------------------------------------------------------------

        ! note on iteration numbers: we use only iter=1 for the first, 2 for a restart, and 3 for all others
        ! for restart (iter=2), this just happened in flapw_driver.f90 in Connect, so we can
        ! skip doing this.
        !if (iter /=2) then

        call set_g00
        !   this sets g_full_00, g_full to the LDA Green's function
        !      g_full is overriden by set_g
        !      How is g_full_00 used?
        !      looks like seteny is using it for the linearization energies only.
        !
        !    g_full_0 is what we use for occupancy later, and it is set in another call to
        !    greep_0(correlation=True) (this one with correlation) in set_g
        !    g_full_0 is also recovered from restart.

        !endif
        ! ---------------------------------------------------------------------
        call smultro ! calculates v_intr_h, vatmt from rho
        call vcoul  ! calculates v_mt_h, z_vnucl from: rho, vatmt
        call add_vxc ! calculates v_mt_xc, v_intr_xc
        call exchener(1,vxch) !! for old Density   ! calculates exchange-correlation cont. to energy
        call core_all
        !call output_ro_v   ! output them in Muffin basis - use this when debugging

        if(itest>1) then
            ! this prints a matching message between interstitial and muffin-tin
            text=' Matching : Coulon Potential!'
            call matching(0,v_mt_h,v_intr_h)
        endif

        !this belongs in SetGreensFunction, but we want to do it in "dft" mode, not "gw" (where a different algorithm is used)
        v_mt_h_old=v_mt_h
        ! don't recompute the bands for the 1st iteration of a restart, we just read it from the image
        call rad_eqs ! sets the ffhmt, which is used in band calculation (in set_h0) -> has to be called before bands
        ! We always do this. If seteny() is called and this is not done is the basis bad? Will it not converge?

        pv=0.d0
        if(irel==2) pvj=0.d0
        ! ----------- Full Potential; L=0 term is absent ---------------
        call vslli(2,2,v_mt_h,v_mt_xc,v_intr_h,v_intr_xc,0)

        call timel('******* vslli finished *************')
        call bands

        ! read the self energy here if it exists, into
        if (correlation) then
            ! ------------------------------------------------------------------
            ! THIS code feeds back the self-energy into the DFT
            ! we want 'set_g' to use the sig_c_omega, if it is available
            ! in future FlapwMBPT versions, make sure
            ! no code which uses the ubi is called in set_g
            ! other than set_g
            !!
            !!
            !  note that calling dft_loop here means that the LAPW states keep evolving
            !  (in r basis) through calls to solve the radial equations (rad_eqs) and
            !  adjusting the energy (seteny). Is this desirable?
            ubi = ' gw'
            iter_max_scf = iter + 2
            !-------------------------------------------------------------------
            old_chem_pot = chem_pot
        end if

        call SetGreensFunction(location_of_sigma, search_mu, update_lapw_basis, save_potentials, mu_search_variant)

        if (correlation) then
            ubi = 'dft'
            iter_max_scf = iter + 2
        end if

        call exchener(2,exch_dft)
        if (correlation .and. maswrk) then
            print *, "- re-calculating density with correlation, mu=", chem_pot
        end if

        call ROfull(correlation,1)

        call coulener(ro_vh_old)
        call smultro
        call vcoul
        call coulener(ro_vh_new)

        !     admix density for the next iteration
        call mixro1_RHOBUSTA

        ! make sure all processes have the same charge convergence, so that they don't disagree
        ! about when to stop
        if (goparr) call MPI_BCAST(charge_convergence, 1, MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)

        ! if no spin magnetization (no SDFT option) in DFT, symmetrize spin density
        if (.not. is_dft_spin_polarized .and.nspin > 1) then
            if (maswrk) print *, '- symmetrizing ρ to set m(r)=0'
            n=maxmt/2
            ro(1:n)=(ro(n+1:n+n) +ro(1:n))/2
            ro(n+1:n+n)=ro(1:n)
            rointr(:,1)=(rointr(:,2) + rointr(:,1))/2
            rointr(:,2)=rointr(:,1)
            ro_core(:,:,1)=(ro_core(:,:,2) + ro_core(:,:,1))/2
            ro_core(:,:,2)=ro_core(:,:,1)
        end if

        call WriteRho()

        ubi_0=ubi
        iter=iter+1

        !print *, iter, iter_dft, iter_max_scf

        call ubi_sumus(iter)

        1010  format(/'                iteration ',i3/)
        
    end


    subroutine SetKohnShamBandsPlusSigmaXAndRotateSigc
        ! from bands_sig_0
        ! e_bnds (dft) -> H_KS + self-energy moments bands 
        ! (plus rotate sig_c_omega into this basis)
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        implicit none
        logical :: correlation
        integer :: k,ndim,ispin,ind_omega,i,ind_k,k1,k2,n0
        complex*16, allocatable :: a(:,:,:),tmp(:,:,:),tmp1(:,:)
        correlation=.True.
        n0=nbndf
        allocate(tmp(n0,n0,2),a(n0,n0,npnt))
        do ispin=1,nspin
            k1=n3_mpi_k(me3_k+1)+1
            k2=n3_mpi_k(me3_k+1)+ndim3_k(me3_k+1)
            do k=1,k1-1
                e_bnd(:,k,ispin)=0.d0
            enddo
            do k=k2+1,npnt
                e_bnd(:,k,ispin)=0.d0
            enddo
            a=(0.d0,0.d0)
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                ndim=n_bnd(k,ispin)
                tmp=(0.d0,0.d0)
                
                !tmp = H_KS
                do i=1,ndim
                    tmp(i,i,1)=tmp(i,i,1)+dcmplx(e_bnd(i,k,ispin),0.d0)
                enddo
                
                !tmp = H_KS + Sigma_x
                call unpack_hermit(tmp,sigx_solid_k(1,1,ind_k,ispin),ndim,n0,n0,(1.d0,0.d0),(1.d0,0.d0))
                
                !e_bnd gets new eigenvalues of H_KS + Sigma_x
                a(1:ndim,1:ndim,k)=tmp(1:ndim,1:ndim,1)
                call eig_val_solver(ndim,n0,a(1,1,k),e_bnd(1,k,ispin))

                if(nproc_tau/=1) then
                    call brdcst(msgdbl,e_bnd(1,k,ispin),8*ndim,0,comm_tau_kk_pbr)
                    call brdcst(msgdbl,a(1,1,k),16*n0*n0,0,comm_tau_kk_pbr)
                endif

                !Flapw making some ficticious bands
                if(n0>ndim) e_bnd(n_bnd(k,ispin)+1:nbndf,k,ispin) = e_bnd(n_bnd(k,ispin),k,ispin)+0.2d0
                
            enddo  !! over ind_k    

            if(nproc_k/=1) call dgop(a,2*n0**2*npnt,'  +',comm_pnt)
            
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                ndim=n_bnd(k,ispin)
                allocate(tmp1(nfun,ndim))

          !c ------ Recalculation the V_H ------------------------------------
                call unpack_hermit(tmp,v_h_bb(1,1,ind_k,ispin),ndim,n0,n0,(0.d0,0.d0),(1.d0,0.d0))
                call zgemm('n','n',ndim,ndim,ndim,(1.d0,0.d0),tmp,n0,a(1,1,k),n0,(0.d0,0.d0),tmp1,ndim)
                call zgemm('c','n',ndim,ndim,ndim,(1.d0,0.d0),a(1,1,k),n0,tmp1,ndim,(0.d0,0.d0),tmp,n0)
                call pack_hermit(tmp,v_h_bb(1,1,ind_k,ispin),ndim,n0,n0,0.d0,1.d0)
          !c ------ Recalculation the V_XC ------------------------------------
                call unpack_hermit(tmp,v_xc_bb(1,1,ind_k,ispin),ndim,n0,n0,(0.d0,0.d0),(1.d0,0.d0))
                call zgemm('n','n',ndim,ndim,ndim,(1.d0,0.d0),tmp,n0,a(1,1,k),n0,(0.d0,0.d0),tmp1,ndim)
                call zgemm('c','n',ndim,ndim,ndim,(1.d0,0.d0),a(1,1,k),n0,tmp1,ndim,(0.d0,0.d0),tmp,n0)
                call pack_hermit(tmp,v_xc_bb(1,1,ind_k,ispin),ndim,n0,n0,0.d0,1.d0)
          !c ------ Recalculation the SIGX_SOLID_K --------------------------------
                call unpack_hermit(tmp,sigx_solid_k(1,1,ind_k,ispin),ndim,n0,n0,(0.d0,0.d0),(1.d0,0.d0))
                call zgemm('n','n',ndim,ndim,ndim,(1.d0,0.d0),tmp,n0,a(1,1,k),n0,(0.d0,0.d0),tmp1,ndim)
                call zgemm('c','n',ndim,ndim,ndim,(1.d0,0.d0),a(1,1,k),n0,tmp1,ndim,(0.d0,0.d0),tmp,n0)
                call pack_hermit(tmp,sigx_solid_k(1,1,ind_k,ispin),ndim,n0,n0,0.d0,1.d0)
          !c ------ Recalculation the SIG_c_omega --------------------------------
                do ind_omega=1,ndim3_omega
                    tmp = 0.0
                    tmp1 = 0.0
                    call ferm_unpack_omega(tmp,sig_c_omega(1,1,1,ind_omega,ind_k,ispin),ndim,n0,n0,3)
                    do i=1,2
                        call zgemm('n','n',ndim,ndim,ndim,(1.d0,0.d0),tmp(1,1,i),n0,a(1,1,k),n0,(0.d0,0.d0),tmp1,ndim)
                        call zgemm('c','n',ndim,ndim,ndim,(1.d0,0.d0),a(1,1,k),n0,tmp1,ndim,(0.d0,0.d0),tmp(1,1,i),n0)
                    enddo
                    sig_c_omega(:,:,:,ind_omega,ind_k,ispin)=0.d0
                    call ferm_pack_omega(tmp,sig_c_omega(1,1,1,ind_omega,ind_k,ispin),ndim,n0,n0)
                enddo

                deallocate(tmp1)
            enddo   !! over ind_k  

        enddo  !! over ispin        

        if(nproc_k/=1) then
            call dgop(e_bnd,nbndf*npnt*nspin,'  +',comm_pnt)
        endif

        deallocate(tmp,a)

        end
        

end module
