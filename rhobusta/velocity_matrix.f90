module velocity_matrix

! returns hermitian velocity matrices in rel case (which is =j) and nonrel case (which is -i*Grad)

    use atom_mod, only: gfun1

    implicit none
    private

    public :: VkMatrixNonrel, VkMatrixRel

contains

        ! from future version of Kutepov's code
    subroutine rotate_c(v0,v,u,key)
        use units_mod
        implicit none
        integer, intent(in) :: key
        real*8, intent(in) :: u(3,3)
        complex*16, intent(in) :: v0(3)
        complex*16, intent(out) :: v(3)
        integer :: i
        real*8 :: yg(3),yg1(3),r,s3,x0(3),x(3)
        do i=1,2
            if(i==1) x0=real(v0)
            if(i==2) x0=imag(v0)
            r=dot_product(x0,x0)
            if(r==0.d0) then
            x=0.d0
            else
            s3=sqrt(0.75d0/pi/r)
            yg(1)=s3*x0(2)
            yg(2)=s3*x0(3)
            yg(3)=s3*x0(1)
            if(key.eq.1)call prd1(yg,u,yg1) !!! inversional ig-operation
            if(key.eq.2)call prd2(yg,u,yg1) !!!      direct ig-operation
            x(1)=yg1(3)/s3
            x(2)=yg1(1)/s3
            x(3)=yg1(2)/s3
            endif
            if(i==1) v=x
            if(i==2) v=v+(0.d0,1.d0)*x
        enddo
    end

    ! make rotate_c safe for the 1st argument, which is changed through the computation if it is the same array as the output
    subroutine RotateComplex(v, ig)
        use solid_mod, only: u
          complex*16, intent(out) :: v(3)
          complex*16 :: saved_v0(3)
          integer :: ig
          saved_v0(:) = v(:)
          call rotate_c(saved_v0, v, u(2,ig), 2)
    end


    ! borrowed and modified from overlap_lapw (which is not likely to be wrong, because it is used everywhere)
    ! Note that we do not handle the case complex_ro, since it is not handled in other parts of the code either
    ! complex_ro seems to be related to magnetism in DFT, which we don't support
    subroutine pw_velocity_matrix(k, gb,pnt_,nbas,vel,indg, dont_use_symmetries)
        use manager_mod
        use solid_mod
        use units_mod
        implicit none
        integer, intent(in) :: nbas,indg(nbndtm), k
        real*8, intent(in) :: gb(3,nbas/nrel),pnt_(3)
        complex*16, intent(out) :: vel(nbas,nbas,3)
        logical, intent(in) :: dont_use_symmetries

        integer :: jgb,jgb0,igb,igb0,ia,ib,ic,ind,jp,jh,ipp,ih,nbndt, k0,ig
        real*8 :: gki(3),gkj(3),pi2a,c1,gj1,&
            gk0i(3),gk0j(3),&
            gj2,gj3,gj,gj22,gi1,gi2,gi3,gi,gij22,gi22,c05,c2,g2j,&
            enj,c2j,anormj,g2i,eni,c2i,anormi,pi2,brij,fs
        complex*16 psum(3), pdiff(3), zero, img, phase

        integer :: indgb_k0(nbndtm)

        vel=(0.d0,0.d0)
        zero=(0.0, 0.0)
        img=(0.0, 1.0)
        pi2=2.d0*pi
        pi2a=pi2/par
        c1=amegaint/amega
        nbndt=nbas/nrel
        if (dont_use_symmetries) then
            k0 = k
            indgb_k0(:) = indg(:)
        else
            k0=i_kref(k)
            ig = k_group(k)
            indgb_k0(:) = indgb(:,k0)
        endif
        ! we don't currently use this code for the non-rel-interst. case, but it should be ready
        ! if this routine is used as a substitute.
        if(irel.ne.2.or..not.rel_interst) then
            do jgb=1,nbndt
                jgb0=indg(jgb)
                gk0j = pi2a*(pnt(:,k0)+gbs(:,indgb_k0(jgb)))
                gj1=pnt_(1)+gb(1,jgb)
                gj2=pnt_(2)+gb(2,jgb)
                gj3=pnt_(3)+gb(3,jgb)
                gj=gj1*gj1+gj2*gj2+gj3*gj3
                gj22=pi2a**2*gj
                gj=sqrt(gj)
                do igb=1,nbndt
                    igb0=indg(igb)
                    gk0i = pi2a*(pnt(:,k0)+gbs(:,indgb_k0(igb)))
                    gi1=pnt_(1)+gb(1,igb)
                    gi2=pnt_(2)+gb(2,igb)
                    gi3=pnt_(3)+gb(3,igb)
                    gi=gi1*gi1+gi2*gi2+gi3*gi3
                    gij22=gj22
                    gi22=pi2a**2*gi
                    gij22=0.5d0*(gi22+gj22)
                    gi=sqrt(gi)
                    ia=igbs(1,jgb0)-igbs(1,igb0)
                    ib=igbs(2,jgb0)-igbs(2,igb0)
                    ic=igbs(3,jgb0)-igbs(3,igb0)
                    ind=indplw(ia,ib,ic)

                    fs = -pi2*dot_product((gk0j-gk0i),shift(:,k_group(k)))
                    phase = dcmplx(cos(fs),sin(fs))

                    if(igb.eq.jgb) then
                        vel(igb,jgb,:)=c1 * phase * pi2a*(pnt_+gb(:,jgb))
                    else
                        vel(igb,jgb,:)=sovr(ind) * phase * pi2a*(pnt_+gb(:,jgb))
                    end if
                    ! this is direct rotation (as the operator which rotates k's in inf_qnt)
                    if (k /= k0) then
                        call RotateComplex(vel(igb,jgb,:), ig)
                    end if

                    !     * For IREL==2 but no spin-orbit in inrestitial ************
                    if(irel==2) then
                        vel(nbndt+igb,nbndt+jgb,:)=vel(igb,jgb,:)
                    endif
                enddo          !!! over igb
            enddo             !!! over jgb
        else if(irel.eq.2) then
            c05=0.5d0*clight
            c2=clight*clight
            do jgb=1,nbndt
                jgb0=indg(jgb)            !=indgb(jgb, k), the gbs index
                gkj=pi2a*(pnt_+gb(:,jgb)) ! gb(:,jgb))=gbs(:, indg(jgb)), see how gb is initialized
                gk0j = pi2a*(pnt(:,k0)+gbs(:,indgb_k0(jgb)))
                g2j=dot_product(gkj,gkj)
                gj=sqrt(g2j)
                enj=c05*(sqrt(c2+4.d0*g2j)-clight)
                c2j=c2+enj
                anormj=c2j/dsqrt(c2j*c2j+c2*g2j)
                do igb=1,nbndt
                    igb0=indg(igb)
                    gki=pi2a*(pnt_+gb(:,igb))
                    gk0i = pi2a*(pnt(:,k0)+gbs(:,indgb_k0(igb)))
                    g2i=dot_product(gki,gki)
                    gi=sqrt(g2i)
                    eni=c05*(sqrt(c2+4.d0*g2i)-clight)
                    c2i=c2+eni
                    anormi=c2i/sqrt(c2i*c2i+c2*g2i) ! the normalization for i

                    ! find the plane wave that corresponds to the pair (p_j - p_i). p_i is the bra.
                    ia=igbs(1,jgb0)-igbs(1,igb0)
                    ib=igbs(2,jgb0)-igbs(2,igb0)
                    ic=igbs(3,jgb0)-igbs(3,igb0)
                    ind=indplw(ia,ib,ic)

                    ! us the plane wave index to find the integral of the real part over the interstitial (sovr, sovi)
                    ! this also includes the 1/V factor
                    if(igb.eq.jgb) then
                        brij=c1
                    else
                        brij=sovr(ind)
                    endif

                    fs = -pi2*dot_product((gk0j-gk0i),shift(:,k_group(k)))
                    phase = dcmplx(cos(fs),sin(fs))

                    ! both sum and difference are in the original k, we have to rotate the result in the end
                    psum  = phase*(gk0i/c2i + gk0j/c2j)*anormi*anormj*brij*c2
                    pdiff = phase*(gk0j/c2j - gk0i/c2i)*anormi*anormj*brij*c2

                    ! note - 1 is down spin, 2 is up spin
                    do jp=1,2
                        jh=(jp-1)*nbndt+jgb
                        do ipp=1,2
                            ih=(ipp-1)*nbndt+igb
                            if(ipp.eq.1.and.jp.eq.1) then !dn dn
                                vel(ih,jh,:)=&
                                       psum + img*[-pdiff(2), pdiff(1), zero]
                            else if(ipp.eq.1.and.jp.eq.2) then ! dn up
                                vel(ih,jh,:)=&
                                       [pdiff(3), img*pdiff(3), -pdiff(1)-img*pdiff(2)]
                            else if(ipp.eq.2.and.jp.eq.1) then ! up dn
                                vel(ih,jh,:)=&
                                       [-pdiff(3), img*pdiff(3), pdiff(1)-img*pdiff(2)]
                            else if(ipp.eq.2.and.jp.eq.2) then ! up up
                                vel(ih,jh,:)=&
                                       psum + img*[pdiff(2), -pdiff(1), zero]
                            endif

                            ! this is direct rotation (as the operator which rotates k's in inf_qnt)
                            if (k /= k0) then
                                call RotateComplex(vel(ih,jh,:),ig)
                            end if
                        enddo   !!! ip
                    enddo       !!! jp
                enddo           !!! igb
            enddo               !!! jgb
        endif                   !!! irel
    end subroutine


    subroutine VkMatrixRel(n0,n,nb, k,ispin,zb,ab, grad_bnd, k_permutation, dont_use_symmetries) ! here ispin == 1
        ! k_permutation: map from k+G to index in k-basis (indgb, indgb_kpath)
        ! use_symmetries: there is a subset of the list of k's which can generate the full set using the symmetry operations. i.e., IBZ -> BZ
        use atom_mod
        use manager_mod
        use solid_mod
        use units_mod
        implicit none

        integer, intent(in) :: n0,k,ispin, n, nb
        complex*16, intent(in) :: zb(nfun,n0),ab(nbasmpw,n0)
        complex*16, intent(out) :: grad_bnd(n,n,3)
        integer, intent(in) :: k_permutation(nbndtm)
        logical, intent(in) :: dont_use_symmetries

        complex*16, allocatable :: tmpWF(:,:), s3(:,:,:),tmp(:,:,:), uxyz(:,:,:)
        complex*16 :: zero, one, img, v1(3), v2(3), v3(3)
        real*8 :: pi2
        integer :: l,k0,i,j,li1, li, lm1,lm,iatom,isort,lf,lf1,nf,ind,mt1,mt,l1,ie1,j1,ie,ir,km1,km,jj,ii,ii1
        integer :: mj1, mj, ig
        real*8 :: J_1, J_2, vrad, dqdall

        real*8, allocatable ::  work(:)
        real*8, allocatable :: gb(:,:)

        grad_bnd = 0.0

        zero=(0.0, 0.0)
        one=(1.0, 0.0)
        img=(0.0, 1.0)

        pi2=2.d0*pi

        ! ------- Interstitial part -------------------------------------
        if (dont_use_symmetries) then
            k0=k
        else
            k0=i_kref(k)
            if (nb /= nbask(k0)) then
                print *, "Assertion Error: nb != nbask(k0) in subroutine VkMatrixNonRel"
                call exit()
            endif
        endif
        

        allocate(gb(3,nplwbas))
        do i=1,nb/nrel
            gb(:,i)=gbs(:,indgb(i,k))
        enddo

        allocate(s3(nb, nb,3))
        call pw_velocity_matrix(k, gb,pnt(1,k),nb,s3, k_permutation, dont_use_symmetries)
        deallocate(gb)

        allocate(tmpWF(nb,n))
        do i=1,3
            call zgemm('n','n',nb,n,nb,(1.d0,0.d0),s3(1,1,i),nb,ab,nbasmpw,(0.d0,0.d0),tmpWF,nb)
            call zgemm('c','n',n,n,nb,(1.d0,0.d0),ab,nbasmpw,tmpWF,nb,(0.d0,0.d0),grad_bnd(1,1,i),n)
        enddo
        deallocate(tmpWF,s3)


        ! MT Part

        v1 = [img, -one, zero]
        v2 = [img, one, zero]
        v3 = [zero, zero, img]

        if (k /= k0) then
            ! this is direct rotation (as the operator which rotates k's in inf_qnt)
            ! and as we rotate in the PW above
            ig = k_group(k)
            call RotateComplex(v1,ig)
            call RotateComplex(v2,ig)
            call RotateComplex(v3,ig)
        end if


        allocate(tmp(nfun,n,3))
        tmp = 0.0
        allocate(work(0:maxnrad))
        do iatom=1,natom
            isort=is(iatom)
            nf=lfunm(isort)

            ! Initialize the matrix between basis elements of this atom
            allocate(uxyz(nf,nf,3))
            uxyz=0.d0
            do lf1=1,lfun(isort) ! bra
                mt1=ind_wf(lf1,isort)
                li1=li_val(lf1,isort)
                l1=li1/2 ! this is the rel. indexing
                ie1=ie_val(lf1,isort)
                j1=in_val(lf1,isort) ! this is 1 or 1,2 for LOC or APW corespondingly
                ii1 = li_val(lf1,isort) - 2*l1
                if (ii1 == 0) then
                    ! kappa is l
                    J_1 = l1 - 0.5
                    ii1 = -1
                else
                    J_1 = l1 + 0.5
                    ii1 = 1
                end if

                do lf=1,lfun(isort)  ! ket
                    mt=ind_wf(lf,isort)
                    li = li_val(lf,isort)
                    l=li/2 ! this is the rel. indexing
                    ie=ie_val(lf,isort)
                    j=in_val(lf,isort)  ! this is 1 or 1,2 for LOC or APW correspondingly
                    ii = li_val(lf,isort) - 2*l
                    if (ii == 0) then
                        ! kappa is l
                        J_2 = l - 0.5
                        ii = -1
                    else
                        J_2 = l + 0.5
                        ii = 1
                    end if
                    jj = l+l+ii

                    ! only for the relevant pairs where the matrix is non zero
                    if((l1+1==l) .and. (J_1 == J_2)) then  ! -> J_1 = J_2 = 2*jj
                        ! radial integration (depends only on J and the functions)
                        work = 0.0
                        do ir=0,nrad(isort)
                            work(ir)=dr(ir,isort)*r(ir,isort)**2*\
                              (1.0/J_1*gfun(mt1+ir,ispin)*gfund(mt+ir,ispin) + 1.0/(J_1+1.0)*gfund(mt1+ir,ispin)*gfun(mt+ir,ispin))
                        enddo
                        Vrad=0.5*dqdall(h(isort),work,nrad(isort))

                        ! assign the angular integrals multiplied by Vrad

                        do mj=-jj,jj,nrel                  !ket
                            call getlimj(lm,l,ii,mj,li,1)
                            km=indbasa(j,ie,lm,isort)

                            do mj1=max(-jj,mj-2),min(jj,mj+2),nrel !bra
                                call getlimj(lm1,l1,ii1,mj1,li1,1)
                                km1 =indbasa(j1,ie1,lm1,isort)

                                if (mj1 == mj-2) then
                                    uxyz(km1,km,:) = Vrad * sqrt((J_1-mj/2.0+1)*(J_1+mj/2.0))*v1  !(i,-1,0)
                                    uxyz(km,km1,:) = conjg(uxyz(km1,km,:))
                                else if (mj1 == mj+2)  then
                                    uxyz(km1,km,:) = Vrad * sqrt((J_1+mj/2.0+1)*(J_1-mj/2.0))*v2 !(i,1,0)
                                    uxyz(km,km1,:) = conjg(uxyz(km1,km,:))
                                else if (mj1 == mj) then
                                    uxyz(km1,km,:) = Vrad * mj * v3!(0,0,i)
                                    uxyz(km,km1,:) = conjg(uxyz(km1,km,:))
                                end if
                            end do

                        end do !mj
                    endif
                end do

            end do  ! all basis elements of an atom

            ind=io_lem(iatom)
            do i=1,3
                call zgemm('n','n',nf,n,nf,(1.d0,0.d0),uxyz(1,1,i),nf, zb(ind,1),nfun,(0.d0,0.d0),tmp(ind,1,i),nfun)
            end do

            deallocate(uxyz)
        end do ! atoms

        do i=1,3
            call zgemm('c','n',n,n,nfun,(1.d0,0.d0),zb,nfun,tmp(1,1,i),nfun,(1.d0,0.d0),grad_bnd(1,1,i),n)
        end do

        ! deallocate stuff
        deallocate(tmp, work)

    end subroutine

    ! For the MT part, see https://www.questaal.org/docs/numerics/spherical_harmonics/#gradients-of-spherical-harmonics
    !   section "Gradients of Spherical harmonics", as well as the referenced book, 
    !   A.R.Edmonds, Angular Momentum in quantum Mechanics p. 79
    subroutine VkMatrixNonrel(n0,n,nb,k,ispin,zb,ab,grad_bnd, k_permutation, dont_use_symmetries)
        use atom_mod
        use manager_mod
        use solid_mod
        use units_mod
        implicit none
        integer, intent(in) :: n0,k,ispin,n,nb
        complex*16, intent(in) :: zb(nfun,n0),ab(nbasmpw,n0)
        complex*16, intent(out) :: grad_bnd(n,n,3)
        logical, intent(in) :: dont_use_symmetries
        integer, intent(in) :: k_permutation(nbndtm)
        integer :: l,k0,i,j,m,lm,iatom,isort,lf,lf1,nf,ind,mt1,mt,l1,ie1,j1,ie,ir,m1,l1m1,km1,km,jj,ig,am,lm2
        real*8 :: pi2a,v(3),a,b,dqdall,td,tr,tt,cr,pi2
        complex*16 :: cc
        real*8, allocatable :: ff(:,:),work(:),uxyz(:,:,:)
        complex*16, allocatable :: s(:,:),s3(:,:,:),uxyzc(:,:,:),c1(:),tmp(:,:,:)
        real*8, allocatable :: gb(:,:)
        ! ------- Interstitial part -------------------------------------

        if (dont_use_symmetries) then
            k0=k
        else
            k0=i_kref(k)
            if (nb /= nbask(k0)) then
                print *, "Assertion Error: nb != nbask(k0) in subroutine VkMatrixNonRel"
                call exit()
            endif
        endif

        grad_bnd = 0.d0
        pi2=2.d0*pi
        ig = k_group(k)

        allocate(gb(3,nplwbas))
        do i=1,nb/nrel
            gb(:,i)=gbs(:,k_permutation(i))
        enddo

        allocate(s3(nb, nb,3))
        call pw_velocity_matrix(k, gb,pnt(1,k),nb,s3,k_permutation, dont_use_symmetries)
        deallocate(gb)

        allocate(s(nb,n))
        do i=1,3
            call zgemm('n','n',nb,n,nb,(1.d0,0.d0),s3(1,1,i),nb,ab,nbasmpw,(0.d0,0.d0),s,nb)
            call zgemm('c','n',n,n,nb,(1.d0,0.d0),ab,nbasmpw,s,nb,(0.d0,0.d0),grad_bnd(1,1,i),n)
        enddo
        deallocate(s,s3)

        ! ------- MT part -------------------------------------
        cc=(0.d0,1.d0)/sqrt(2.d0)
        cr=1.d0/sqrt(2.d0)
        allocate(ff(limlb,6))
        lm=0
        do l=0,maxb
            a=2.0*(l+l+1)*(l+l+3)
            b=2.0*(l+l-1)*(l+l+1)
            do m=-l,l
                lm=lm+1
                ff(lm,1)=sqrt(dfloat(l+m+1)*(l+m+2)/a)  ! 1
                ff(lm,2)=-sqrt(dfloat(l-m)*(l-m-1)/b)   ! 1 b
                ff(lm,3)=sqrt(dfloat(l-m+1)*(l-m+2)/a)  !-1
                ff(lm,4)=-sqrt(dfloat(l+m)*(l+m-1)/b)   !-1 b
                ff(lm,5)=sqrt(2.0*(l+m+1)*(l-m+1)/a)    ! 0
                ff(lm,6)=sqrt(2.0*(l+m)*(l-m)/b)        ! 0 b
            enddo
        enddo
        allocate(tmp(nfun,n,3))
        allocate(work(0:maxnrad))
        do iatom=1,natom
            ind=io_lem(iatom)
            isort=is(iatom)
            nf=lfunm(isort)
            allocate(uxyz(nf,nf,3))
            uxyz=0.d0
            do lf1=1,lfun(isort)  !l1,mt1... are the ket
                mt1=ind_wf(lf1,isort)
                l1=li_val(lf1,isort)-1
                ie1=ie_val(lf1,isort)
                j1=in_val(lf1,isort)
                do lf=1,lfun(isort)  !l,mt... are the bra
                    mt=ind_wf(lf,isort)
                    l=li_val(lf,isort)-1
                    ie=ie_val(lf,isort)
                    j=in_val(lf,isort)
                    do ir=0,nrad(isort)
                        work(ir)=gfun(mt+ir,ispin)*gfun1(mt1+ir,ispin)*dr(ir,isort)*r(ir,isort)*r(ir,isort)
                    enddo
                    td=dqdall(h(isort),work,nrad(isort))
                    do ir=0,nrad(isort)
                        work(ir)=gfun(mt+ir,ispin)*gfun(mt1+ir,ispin)*dr(ir,isort)*r(ir,isort)
                    enddo
                    tr=dqdall(h(isort),work,nrad(isort))

                    if(l==l1+1) then ! bra:l ket:l-1
                        tt=td-l1*tr
                        do m1=-l1,l1
                            l1m1=l1*(l1+1)+m1+1
                            km1=indbasa(j1,ie1,l1m1,isort)
                            do m=-l,l
                                lm=l*(l+1)+m+1
                                km=indbasa(j,ie,lm,isort)
                                if(m==m1+1) uxyz(km,km1,1)=uxyz(km,km1,1)+tt*ff(l1m1,1)    !mu=m-m1=1
                                if(m==m1-1) uxyz(km,km1,2)=uxyz(km,km1,2)+tt*ff(l1m1,3)    !mu=-1
                                if(m==m1) uxyz(km,km1,3)=uxyz(km,km1,3)+tt*ff(l1m1,5)
                            enddo
                        enddo
                    endif
                    if(l==l1-1) then !bra:l, ket:l+1
                        tt=td+(l1+1)*tr
                        do m1=-l1,l1
                            l1m1=l1*(l1+1)+m1+1
                            km1=indbasa(j1,ie1,l1m1,isort)
                            do m=-l,l
                                lm=l*(l+1)+m+1
                                km=indbasa(j,ie,lm,isort)
                                if(m==m1+1) uxyz(km,km1,1)=uxyz(km,km1,1)+tt*ff(l1m1,2) !mu==m-m1=1
                                if(m==m1-1) uxyz(km,km1,2)=uxyz(km,km1,2)+tt*ff(l1m1,4) !mu=-1
                                if(m==m1) uxyz(km,km1,3)=uxyz(km,km1,3)+tt*ff(l1m1,6)
                            enddo
                        enddo
                    endif
                enddo   !! over lf
            enddo   !! over lf1

            ! now calculate gradient in the x, y z basis
            do km1=1,nf
                do km=1,nf
                    tt=(uxyz(km,km1,2)-uxyz(km,km1,1))*cr
                    uxyz(km,km1,2)=(-uxyz(km,km1,1)-uxyz(km,km1,2))*cr ! we will divide by i when setting the complex array
                    uxyz(km,km1,1)=tt
                enddo
            enddo

            ! so far, we got uxyz in Ylm SH basis (not RSH), now need to transform
            allocate(uxyzc(nf,nf,3))
            uxyzc=uxyz
            uxyzc(:,:,2) = uxyz(:,:,2)/(0.0,1.0) ! couldnt be done in a real array before

            if (k /= k0) then
                do km1=1,nf
                    do km=1,nf
                        ! this is direct rotation (as the operator which rotates k's in inf_qnt)
                        ! and as we rotate in the PW above
                        call RotateComplex(uxyzc(km,km1,:),ig)
                    enddo
                enddo
            end if

            ! following code tested: U.HVU with V=I it is indeed I
            allocate(c1(nf))
            do i=1,3
                do j=1,nf
                    c1=uxyzc(j,:,i)
                    do lf=1,lfun(isort)
                        l=li_val(lf,isort)-1
                        ie=ie_val(lf,isort)
                        jj=in_val(lf,isort)
                        do m=-l,-1
                            lm=l*(l+1)+m+1
                            km=indbasa(jj,ie,lm,isort)
                            lm=l*(l+1)-m+1
                            km1=indbasa(jj,ie,lm,isort)
                            uxyzc(j,km,i)=cc*(-c1(km)+(-1)**(-m)*c1(km1))
                        enddo
                        do m=1,l
                            lm=l*(l+1)-m+1
                            km=indbasa(jj,ie,lm,isort)
                            lm=l*(l+1)+m+1
                            km1=indbasa(jj,ie,lm,isort)
                            uxyzc(j,km1,i)=cr*(c1(km)+(-1)**m*c1(km1))
                        enddo
                    enddo
                enddo
                do j=1,nf
                    c1=uxyzc(:,j,i)
                    do lf=1,lfun(isort)
                        l=li_val(lf,isort)-1
                        ie=ie_val(lf,isort)
                        jj=in_val(lf,isort)
                        do m=-l,-1
                            lm=l*(l+1)+m+1
                            km=indbasa(jj,ie,lm,isort)
                            lm=l*(l+1)-m+1
                            km1=indbasa(jj,ie,lm,isort)
                            uxyzc(km,j,i)=cc*(c1(km)-(-1)**(-m)*c1(km1))
                        enddo
                        do m=1,l
                            lm=l*(l+1)-m+1
                            km=indbasa(jj,ie,lm,isort)
                            lm=l*(l+1)+m+1
                            km1=indbasa(jj,ie,lm,isort)
                            uxyzc(km1,j,i)=cr*(c1(km)+(-1)**m*c1(km1))
                        enddo
                    enddo
                enddo

                call zgemm('n','n',nf,n,nf,(1.d0,0.d0),uxyzc(1,1,i),nf, zb(ind,1),nfun,(0.d0,0.d0),tmp(ind,1,i),nfun)
            enddo   !! over i
            deallocate(uxyz,uxyzc,c1)
        enddo   !!! over iatom
        deallocate(ff,work)
        do i=1,3
            call zgemm('c','n',n,n,nfun,(1.d0,0.d0),zb,nfun,tmp(1,1,i),nfun,(1.d0,0.d0),grad_bnd(1,1,i),n)
        enddo
        deallocate(tmp)

        ! So far we calculated \Nabla, next we return the current, 1/(2mi) *(nabla_right - nabla_left)
        ! which is equal to -i/m * \Nabla (also on off-diagonal!)
        grad_bnd =grad_bnd * (0,-2) !  2 factor is for 1/m
    end

end module
