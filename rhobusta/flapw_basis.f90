module flapw_basis

    implicit none
    private :: GetRadialFunctions, GetMap, StoreMtBasis
    public :: GetMtBasisImp, GetInterstitialBasisImp, StoreInterstitialBasis

    include 'mpif.h'

  contains

    subroutine GetRadialFunctions(isort, mt)
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        
        use FlapwMBPT_basis, only: MuffinTin
        use stringifor
        use stringifor_string_t
        
        integer, intent(in) :: isort
        type(MuffinTin) :: mt

        integer :: ispin = 1
        integer :: ie1, mt1, ir
        
        do ie1 = 1,lfun(isort)
            mt1 = ind_wf(ie1,isort) 
            mt%radial_functions_(ie1,:) = gfun(mt1:mt1+nrad(isort), ispin)
            mt%radial_functions2_(ie1,:) = gfund(mt1:mt1+nrad(isort), ispin)
        end do

    end subroutine

    subroutine GetMap(isort, mt)
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        
        use FlapwMBPT_basis, only: MuffinTin
        use stringifor
        use stringifor_string_t
        
        integer, intent(in) :: isort
        type(MuffinTin) :: mt

        integer :: ispin = 1
        integer :: li, l, je, jn1, lm, mj, i, in1, kmj, lget, iatom
        
        ! from flapw_driver.f90
        do iatom=1,natom
            if (is(iatom) .ne. isort) cycle

            do lm=1,nrel*(lmb(isort)+1)**2
                if(irel.ne.2) then
                    l=lget(lm)
                    !mj=?
                    li=l+1
                else if(irel.eq.2) then
                    call getlimj(lm,l,i,mj,li,0)
                endif

                do je=1,ntle(l,isort)
                    in1=1
                    if(augm(je,l,isort)/='LOC') in1=2
                    do jn1=1,in1
                        kmj=io_lem(iatom)-1+indbasa(jn1,je,lm,isort)
                        mt%lapw2self_(kmj) = indfun0(jn1,je,li,isort) - 1
                    end do
                end do
            end do
        end do

    end subroutine

    subroutine StoreMtBasis(fname)
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        
        use FlapwMBPT_basis, only: MuffinTinBasis
        use stringifor
        use stringifor_string_t

        type(string), intent(in) :: fname
        type(MuffinTinBasis),   allocatable :: mtb
        integer :: isort, norb

        if (maswrk) then
            print *, " - collecting MT basis functions"
        endif

        allocate(mtb)

        mtb%nsort = nsort  
        mtb%nrel = nrel
        mtb%natom = natom
        mtb%nops = 96 !this is the size of ip -- not the real number of operations

        call mtb%allocate()
        mtb%atom_to_equiv_under_op_(:,:) = ip(:,:)

        do isort = 1, nsort

            
            mtb%tins(isort)%nrad = nrad(isort)+1
            mtb%tins(isort)%num_radial_functions = lfun(isort)
            mtb%tins(isort)%num_muffin_orbs = nfun

            call mtb%tins(isort)%allocate()

            mtb%tins(isort)%radial_mesh = r(:,isort)
            mtb%tins(isort)%dr_mesh = dr(:,isort)

            call GetRadialFunctions(isort, mtb%tins(isort))
            call GetMap(isort, mtb%tins(isort))

        end do

        call mtb%store(fname%chars(), flush_opt=.True.)

        deallocate(mtb)

        if (maswrk) then
            print *, " -- done"
        end if

    end subroutine

    function GetMtBasisImp(selfpath) result(res)
        use iso_c_binding
        use stringifor
        implicit none
        type(string), intent(inout) :: selfpath
        type(string) :: res
        
        res = ""
        call StoreMtBasis(selfpath)
    end function

    subroutine StoreInterstitialBasis(fname, num_k, nbask2store, indgb2store, ev_bnd2store, min_band, max_band)
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        
        use FlapwMBPT_basis, only: InterstitialBasis
        use stringifor
        use stringifor_string_t

        type(string), intent(in) :: fname
        integer, intent(in)  :: num_k,  nbask2store(num_k), indgb2store(nbndtm,num_k)
        complex*16, intent(in) :: ev_bnd2store(nbasmpw,nbndf,num_k,nspin_0)
        type(integer), intent(in) :: min_band, max_band
        type(InterstitialBasis),   allocatable :: int_bas
        integer :: i, k, ik, ispin, mink, maxk, shard_offset, upperBand, mb, ibnd

        if (maswrk) then
            print *, " - collecting interstitial basis functions"
        endif

        mb = max_band

        allocate(int_bas)

        int_bas%max_num_pw = nbasmpw/nrel
        int_bas%max_g_basis = maxplw
        int_bas%nrel = nrel
        int_bas%num_expanded = mb - min_band + 1
        int_bas%num_k = num_k  
        int_bas%num_si = nspin
        int_bas%num_k_all = nqdiv

        mink = 1
        maxk = num_k

        ! if memory is sharded, return only within the shard:
        if (size(ndim3_k) > 1) then
            shard_offset = n3_mpi_k(me3_k+1)
            int_bas%num_k = ndim3_k(me3_k+1)
            mink = shard_offset + 1
            maxk = shard_offset + int_bas%num_k
        end if

        call int_bas%allocate()

        do i=1,nbasmpw/nrel
            int_bas%reciprical_lattice_vectors_(:,i) = gbs(:,i)
        end do


        do ispin=1,nspin
            do k=mink, maxk
                ik = k-mink + 1
                int_bas%vector_index_(1:int_bas%num_expanded,ik)  = indgb2store(min_band:mb,ik)

                int_bas%num_vectors_at_k_(ik) = nbask2store(k)

                upperBand = min(n_bnd(k, ispin),mb)  ! n_bnd is not sharded
                do ibnd = min_band, upperBand

                    int_bas%A_(:,ibnd-min_band+1,ik,ispin) = ev_bnd(:,ibnd,k,ispin)

                enddo
            enddo
        enddo
        
        call int_bas%store(fname%chars(), flush_opt=.True.)

        deallocate(int_bas)

        if (maswrk) then
            print *, " -- done"
        endif

    end subroutine

    function GetInterstitialBasisImp(selfpath,min_band,max_band) result(res)
        use iso_c_binding
        use stringifor
        use solid_mod
        implicit none
        type(string), intent(inout) :: selfpath
        integer(kind=c_int), intent(in),value :: min_band, max_band
        type(string) :: res
        
        res = ""
        call StoreInterstitialBasis(selfpath,npnt, nbask, indgb, ev_bnd, min_band,max_band)
    end function


end module
