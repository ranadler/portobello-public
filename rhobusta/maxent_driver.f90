module maxent_driver

    use maxent, only: Input, Workspace

    implicit none


    type(Input),allocatable     :: inp
    type(Workspace),allocatable :: ws

contains


    real*8 function off_diag_entropy()
        implicit none
        ! locals
        integer :: i
        real*8 :: ent, eps
        ent=0.
        eps=1.E-12
        do i=-inp%nomega,inp%nomega
            if (ws%Ap_(i)>eps .and. inp%Dp_(i)>eps) then
                ent=ent-ws%Ap_(i)*log(ws%Ap_(i)/inp%Dp_(i))*inp%d_omega_(i)
            endif
            if (ws%Am_(i)>eps .and. inp%Dm_(i)>eps) then
                ent=ent-ws%Am_(i)*log(ws%Am_(i)/inp%Dp_(i))*inp%d_omega_(i)
            endif
        enddo
        off_diag_entropy=ent
        return
    end function

    real*8 function entropy()
        implicit none
        ! locals
        integer :: i
        real*8 :: ent, eps
        ent=0.
        eps=1.E-12
        do i=-inp%nomega,inp%nomega
            if (ws%A_(i)>eps .and. inp%model_(i)>eps) then
                ent=ent-ws%A_(i)*log(ws%A_(i)/inp%model_(i))*inp%d_omega_(i)
            endif
        enddo
        ENTROPY=ent
        return
    end function

    function CompleteSetupImp(selfpath) result(res)
        use stringifor
        !use m_constants, only: pi
        type(string), intent(inout) :: selfpath
        type(string) :: res
        real*8 ::  total, tau, omega, beta, tw, bw, x, pi
        integer ::j,i,iw,jw,it

        pi = 4 * datan(1d0)

        res = "./maxent.core.h5:/input/"
        if (.not. allocated(inp)) allocate(inp)
        if (.not. allocated(ws)) allocate(ws)

        call random_seed()

        call inp%load(res%chars())

        beta = inp%beta

        if (inp%kernel_choice == inp%FERMIONIC_IOMEGA_KERNEL) then
            do iw=-inp%nomega,inp%nomega
                do it=1,inp%num_x
                    x = inp%X_(it)
                    omega = inp%omega_(iw)
                    inp%kernel_(iw,it) = inp%d_omega_(iw) * 1/(x * (0,1)- omega)
                enddo
            enddo
        else if (inp%kernel_choice == inp%FERMIONIC_TAU_KERNEL) then  ! is fermion
            do iw=-inp%nomega,inp%nomega
                do it=1,inp%num_x
                    tau = inp%X_(it)
                    omega = inp%omega_(iw)
                    if (omega*beta<-50)then
                        inp%kernel_(iw,it) = inp%d_omega_(iw) * exp((beta-tau)*omega )
                    else
                        inp%kernel_(iw,it) = inp%d_omega_(iw) * exp(-tau*omega)/(1.+exp(-beta*omega))
                    endif
                enddo
            enddo
        else if (inp%kernel_choice == inp%BOSONIC_IOMEGA_KERNEL) then
            do iw=-inp%nomega,inp%nomega
                do it=1,inp%num_x
                    x = inp%X_(it)
                    omega = inp%omega_(iw)
                    inp%kernel_(iw,it) = inp%d_omega_(iw) * 2.0/pi * omega*omega/(x*x + omega*omega)
                enddo
            enddo
        else ! BOSONIC_TAU_KERNEL
            do iw=-inp%nomega,inp%nomega
                do it=1,inp%num_x
                    tau = inp%X_(it)
                    omega = inp%omega_(iw)
                    if (omega*beta<-50.)then
                        inp%kernel_(iw,it) = -0.5 * inp%d_omega_(iw) * omega * ( exp((beta-tau)*omega) + exp(tau*omega) )
                    else if(abs(omega)<1e-6) then
                        inp%kernel_(iw,it) = inp%d_omega_(iw)/beta
                    else
                        tw = exp(-tau*omega) + exp(-(beta-tau)*omega)
                        bw = 1.-exp(-beta*omega)
                        inp%kernel_(iw,it) = 0.5 * inp%d_omega_(iw) * omega * tw/bw
                    endif
                enddo
            enddo
        end if

        do j=1,2*inp%nomega+1
            do i=1,2*inp%nomega+1
                iw=i-inp%nomega-1
                jw=j-inp%nomega-1
                total=0.0
                do it=1,inp%num_x
                    total = total + conjg(inp%kernel_(iw,it))*inp%invVar_(it)*inp%kernel_(jw,it)
                enddo
                inp%dlda_(i,j)=total/(inp%d_omega_(iw)*inp%d_omega_(jw))
            enddo
        enddo

        call inp%store(res%chars(), flush_opt=.True.)
    end function


    subroutine InvertRealMatrix(ainv, a, n)
        implicit none
        real*8, intent(out):: ainv(n,n)
        real*8, intent(in) :: a(n,n)
        integer, intent(in):: n
        !
        integer, allocatable :: ipiv(:)
        real*8, allocatable  :: work(:)
        integer :: lwork, info
        !
        external dgetrf
        external dgetri
        !
        lwork = n*n
        allocate (work(lwork), ipiv(n))

        ! DGETRF computes an LU factorization of a general M-by-N matrix A using partial pivoting with row interchanges.
        ainv(:,:) = a(:,:)

        call dgetrf( n, n, ainv, n, ipiv, info )
        if(info.ne.0) then
            print *, 'ERROR: LU decomposition failed. INFO=', info
        endif

        !  DGETRI computes the inverse of a matrix using the LU factorization computed by DGETRF.
        call dgetri(n, ainv, n, ipiv, work, lwork, info)

        if (info.ne.0) then
            print *,  'ERROR: Matrix inversion failed. INFO=', info
        endif
        deallocate (work,ipiv)
    end subroutine


    real*8 function ComputeTrace()
        implicit none
        !
        ! computes : Tr( lam * (lam+alpha)^-1 )
        ! where      lam = sqrt(a(w1))*ker(w1,:)*invVar(:)*ker(w2,:)*sqrt(a(w2))/dw
        real*8  :: tr
        integer :: j, i, iw, jw, nw
        real*8, allocatable  :: lam(:,:),alam(:,:),alami(:,:)

        nw = inp%nomega
        allocate( lam(2*nw+1,2*nw+1), alam(2*nw+1,2*nw+1), alami(2*nw+1,2*nw+1) )

        do j=1,2*nw+1
            do i=1,2*nw+1
                iw=i-nw-1
                jw=j-nw-1
                !abs is for offdiag
                lam(i,j) = sqrt(abs(ws%A_(iw)))*inp%dlda_(i,j)*sqrt(abs(ws%A_(jw)))\
                    *sqrt(inp%d_omega_(iw)*inp%d_omega_(jw)) 
                            
                alam(i,j)=lam(i,j)
            enddo
            alam(j,j)=lam(j,j)+ws%alpha
        enddo

        call InvertRealMatrix(alami, alam, 2*nw+1)

        deallocate( alam )

        tr=0.
        do i=1,2*nw+1
            do j=1,2*nw+1
                tr=tr+lam(i,j)*alami(j,i)
            enddo
        enddo
        ComputeTrace = tr

        deallocate( lam, alami )
    end function


    function MaxentImp(selfpath, num_steps) result(res)
        use iso_c_binding
        use stringifor
        implicit none
        type(string), intent(inout) :: selfpath
        type(string) :: res
        integer(kind=c_int), value, intent(in)   :: num_steps ! number of annealing steps

        integer:: i,j1,j2,jj,acc,try
        real*8 :: am,eps,x
        real*8 :: dj1,dj2,x1,x2,wgt
        real*8 :: aa1,aa2,de,p,mm1,mm2,arat

        res = "./maxent.core.h5:/work/"
        call ws%load(res%chars())

        am = maxval(ws%A_)
        ws%dA_(:) = (ws%A_(:)/10.+0.01)*ws%rfac

        acc=0
        try=0
        eps=1.e-12

        do i=1,num_steps
            ! current signal(x)= A(w)*K(w,x)
            ws%signal1_ = matmul(ws%A_ , inp%kernel_)
            ! computing current chi2
            x1 = sum(inp%invVar_*abs(inp%signal_-ws%signal1_)**2)

            ! number of Metroolis steps equal to the number of frequency points
            do jj=1,2*inp%nomega+1
                ! finds two points (j1,j2) and the move for the spectra at these two points
                ! a(j1) -> a(j1)+dj1
                ! a(j2) -> a(j2)+dj2
                do
                    call random_number(x)
                    j1=int(x*(2*inp%nomega+1))-inp%nomega
                    call random_number(x)
                    j2=int(x*(2*inp%nomega))-inp%nomega
                    if (j2 >=j1) j2=j2+1   ! j2!=j1

                    do  ! need positive a(j1)+dj1
                        call random_number(x)
                        dj1=ws%dA_(j1)*(x-0.5)              ! dj1 -> -0.5*da(j1) ... 0.5*da(j1)
                        if (ws%A_(j1)+dj1>=0) exit
                    enddo
                    call random_number(x)
                    dj2=-dj1+0.05*ws%dA_(j2)*(x-0.5)    ! dj2 -> -dj1 + -0.025*da(j2) ... 0.025*da(j2)
                    if (ws%A_(j2)+dj2>=0) exit
                enddo

                if (ws%A_(j1)>0.1*am) try=try+1  ! trial steps
                ! computing chi2 for trial step
                ws%signal2_(:)=ws%signal1_(:) + dj1*inp%kernel_(j1,:) + dj2*inp%kernel_(j2,:)
                x2 = sum(inp%invVar_* (abs(inp%signal_-ws%signal2_))**2) ! chi2

                aa1=ws%A_(j1)+dj1
                aa2=ws%A_(j2)+dj2
                mm1=inp%model_(j1)
                mm2=inp%model_(j2)
                de=0.

                if (aa1>eps)       de=de-inp%d_omega_(j1)*aa1*log(aa1/mm1)
                if (ws%A_(j1)>eps) de=de+inp%d_omega_(j1)*ws%A_(j1)*log(ws%A_(j1)/mm1)
                if (aa2>eps)       de=de-inp%d_omega_(j2)*aa2*log(aa2/mm2)
                if (ws%A_(j2)>eps) de=de+inp%d_omega_(j2)*ws%A_(j2)*log(ws%A_(j2)/mm2)

                wgt=((x1-x2)/2.0 + ws%alpha*de)/ws%temp ! difference in chi2+alpha*S between trial and current configuration

                ! Metropolis probability
                if (wgt>0.d0) then
                    p=1.d0
                elseif (wgt>-100.d0) then
                    p=exp(wgt)
                else
                    p=0.
                endif
                ! accepting with Metropolis probability

                call random_number(x)
                if (x<p) then
                    if (ws%A_(j1)>0.1*am) acc=acc+1
                    ws%A_(j1)=aa1
                    ws%A_(j2)=aa2
                    ws%signal1_(:) = ws%signal2_(:)
                    x1=x2
                endif
            enddo

            if (mod(i,100) == 0) then
                arat=real(acc)/real(try)
                if (arat>0.1) then ! if accepting probability is large, reduce the size of future steps
                    if (ws%rfac<0.01) ws%rfac=ws%rfac*1.5
                else                  ! if accepting probability is small, increase the size of future steps
                    if (ws%rfac>0.001) ws%rfac=ws%rfac/1.5
                endif

                am = maxval(ws%A_)
                ws%dA_(:) = (0.1*ws%A_(:)+0.01*am)*ws%rfac

                if (mod(i,num_steps/10)==0) then
                    write(*,'(A,I7,2x,A,f12.5,2x,A,I10,2x)') 'ann-step=', i,'chi2=', x1, 'accepted=', acc
                endif

                ws%temp=ws%temp/1.5
                acc=0
                try=0
            endif
        enddo

        ws%entropy = entropy()
        ws%tr = ComputeTrace()

        call ws%store(res%chars(), .True.)
    end function

    function MaxentOffDiagImpDeprecated(selfpath, num_steps) result(res)
        use iso_c_binding
        use stringifor
        implicit none
        type(string), intent(inout) :: selfpath
        type(string) :: res
        integer(kind=c_int), value, intent(in)   :: num_steps ! number of annealing steps

        integer:: i,j1,j2,jj,acc,try
        real*8 :: am,eps,x
        real*8 :: dj1,dj2,x1,x2,wgt
        real*8 :: aa1,aa2,de,p,mm1,mm2,arat,num1,num1_old,num2,num2_old

        res = "./maxent.core.h5:/work/"
        call ws%load(res%chars())

        am = maxval(abs(ws%A_))
        ws%dA_(:) = (inp%Dm_(:)/10.+0.01)*ws%rfac

        acc=0
        try=0
        eps=1.e-12

        do i=1,num_steps
            ! current signal(x)= A(w)*K(w,x)
            ws%signal1_ = matmul(ws%A_ , inp%kernel_)
            ! computing current chi2
            x1 = sum(inp%invVar_*abs(inp%signal_-ws%signal1_)**2)

            ! number of Metroolis steps equal to the number of frequency points
            do jj=1,2*inp%nomega+1
                ! like diag only we let a be negative
                call random_number(x)
                j1=int(x*(2*inp%nomega+1))-inp%nomega
                call random_number(x)
                j2=int(x*(2*inp%nomega))-inp%nomega
                if (j2 >=j1) j2=j2+1  
                call random_number(x)
                dj1=ws%dA_(j1)*(x-0.5)           
                call random_number(x)
                dj2=-dj1+0.05*ws%dA_(j2)*(x-0.5)   

                if (abs(ws%A_(j1))>0.1*am) try=try+1  ! trial steps
                ! computing chi2 for trial step
                ws%signal2_(:)=ws%signal1_(:) + dj1*inp%kernel_(j1,:) + dj2*inp%kernel_(j2,:)
                x2 = sum(inp%invVar_* (abs(inp%signal_-ws%signal2_))**2) ! chi2

                aa1=ws%A_(j1)+dj1
                aa2=ws%A_(j2)+dj2
                mm1=2*inp%model_(j1)
                mm2=2*inp%model_(j2)

                num1 = aa1**2 + 4*mm1
                num1_old = ws%A_(j1)**2 + 4*mm1
                num2 = aa2**2 + 4*mm2
                num2_old = ws%A_(j2)**2 + 4*mm1

                de = num1 + num2 - num1_old - num2_old

                if (num1>eps)       de=de-inp%d_omega_(j1)*aa1*log(num1/mm1)
                if (num1_old>eps)   de=de+inp%d_omega_(j1)*ws%A_(j1)*log(num1_old/mm1)
                if (num2>eps)       de=de-inp%d_omega_(j2)*aa2*log(num2_old/mm2)
                if (num2_old>eps)   de=de+inp%d_omega_(j2)*ws%A_(j2)*log(num2_old/mm2)

                wgt=((x1-x2)/2.0 + ws%alpha*de)/ws%temp ! difference in chi2+alpha*S between trial and current configuration

                ! Metropolis probability
                if (wgt>0.d0) then
                    p=1.d0
                elseif (wgt>-100.d0) then
                    p=exp(wgt)
                else
                    p=0.
                endif
                ! accepting with Metropolis probability

                call random_number(x)
                if (x<p) then
                    if (abs(ws%A_(j1))>0.1*am) acc=acc+1
                    ws%A_(j1)=aa1
                    ws%A_(j2)=aa2
                    ws%signal1_(:) = ws%signal2_(:)
                    x1=x2
                endif
            enddo

            if (mod(i,100) == 0) then
                arat=real(acc)/real(try)
                if (arat>0.1) then ! if accepting probability is large, reduce the size of future steps
                    if (ws%rfac<0.01) ws%rfac=ws%rfac*1.5
                else                  ! if accepting probability is small, increase the size of future steps
                    if (ws%rfac>0.001) ws%rfac=ws%rfac/1.5
                endif

                am = maxval(ws%A_)
                ws%dA_(:) = (0.1*ws%A_(:)+0.01*am)*ws%rfac

                if (mod(i,num_steps/10)==0) then
                    write(*,'(A,I7,2x,A,f12.5,2x,A,I10,2x)') 'ann-step=', i,'chi2=', x1, 'accepted=', acc
                endif

                ws%temp=ws%temp/1.5
                acc=0
                try=0
            endif
        enddo

        ws%entropy = entropy()
        ws%tr = ComputeTrace()

        call ws%store(res%chars(), .True.)
    end function

    function MaxentOffDiagImp(selfpath, num_steps) result(res)
        use iso_c_binding
        use stringifor
        implicit none
        type(string), intent(inout) :: selfpath
        type(string) :: res
        integer(kind=c_int), value, intent(in)   :: num_steps ! number of annealing steps

        integer:: i,j1,j2,jj,acc,try
        real*8 :: am,amm,amp,eps,x
        real*8 :: dj1,dj2,x1,x2,wgt, sign
        real*8 :: aa1,aa2,de,p,mm1,mm2,arat

        logical :: is_positive_a

        res = "./maxent.core.h5:/work/"
        call ws%load(res%chars())


        amm = maxval(ws%Ap_(:))
        amp = maxval(ws%Am_(:))
        ws%dAp_(:) = (ws%Ap_(:)/10.+0.01)*ws%rfac
        ws%dAm_(:) = (ws%Am_(:)/10.+0.01)*ws%rfac

        acc=0
        try=0
        eps=1.e-12

        do i=1,num_steps
            ws%A_(:) = ws%Ap_(:) - ws%Am_(:)

            ! current signal(x)= A+(w)*K(w,x) - A-(w)*K(w,x)
            ws%signal1_ = matmul(ws%A_ , inp%kernel_)
            ! computing current chi2
            x1 = sum(inp%invVar_*abs(inp%signal_-ws%signal1_)**2)

            ! number of Metroolis steps equal to the number of frequency points
            do jj=1,4*inp%nomega+2
                ! finds two points (j1,j2) and the move for the spectra at these two points
                ! a(j1) -> a(j1)+dj1
                ! a(j2) -> a(j2)+dj2

                call random_number(x)
                if (x < 0.5) then
                    am = amm
                    ws%CA_(:) = ws%Am_(:)
                    ws%dCA_(:) = ws%dAm_(:)
                    inp%model_(:) = inp%Dm_(:)
                    is_positive_a = .false.
                    sign = 1
                else
                    am = amp
                    ws%CA_(:) = ws%Ap_(:)
                    ws%dCA_(:) = ws%dAp_(:)
                    inp%model_(:) = inp%Dp_(:)
                    is_positive_a = .true.
                    sign = 1
                endif

                do
                    call random_number(x)
                    j1=int(x*(2*inp%nomega+1))-inp%nomega
                    call random_number(x)
                    j2=int(x*(2*inp%nomega))-inp%nomega
                    if (j2 >=j1) j2=j2+1   ! j2!=j1

                    do  ! need positive a(j1)+dj1
                        call random_number(x)
                        dj1=ws%dCA_(j1)*(x-0.5)              ! dj1 -> -0.5*da(j1) ... 0.5*da(j1)
                        if (ws%CA_(j1)+dj1>=0) exit
                    enddo
                    call random_number(x)
                    dj2=-dj1+0.05*ws%dCA_(j2)*(x-0.5)    ! dj2 -> -dj1 + -0.025*da(j2) ... 0.025*da(j2)
                    if (ws%CA_(j2)+dj2>=0) exit
                enddo
                

                if (ws%CA_(j1)>0.1*am) try=try+1  ! trial steps
                ! computing chi2 for trial step
                if (is_positive_a) then
                    ws%signal2_(:)=ws%signal1_(:) + dj1*inp%kernel_(j1,:) + dj2*inp%kernel_(j2,:)
                else
                    ws%signal2_(:)=ws%signal1_(:) - dj1*inp%kernel_(j1,:) - dj2*inp%kernel_(j2,:)
                endif
                x2 = sum(inp%invVar_* (abs(inp%signal_-ws%signal2_))**2) ! chi2

                aa1=ws%CA_(j1)+dj1
                aa2=ws%CA_(j2)+dj2
                mm1=inp%model_(j1)
                mm2=inp%model_(j2)
                de=0.

                if (aa1>eps)            de=de-sign*inp%d_omega_(j1)*aa1*log(aa1/mm1)
                if (ws%CA_(j1)>eps)   de=de+sign*inp%d_omega_(j1)*ws%CA_(j1)*log(ws%CA_(j1)/mm1)
                if (aa2>eps)            de=de-sign*inp%d_omega_(j2)*aa2*log(aa2/mm2)
                if (ws%CA_(j2)>eps)   de=de+sign*inp%d_omega_(j2)*ws%CA_(j2)*log(ws%CA_(j2)/mm2)

                wgt=((x1-x2)/2.0 + ws%alpha*de)/ws%temp ! difference in chi2+alpha*S between trial and current configuration

                ! Metropolis probability
                if (wgt>0.d0) then
                    p=1.d0
                elseif (wgt>-100.d0) then
                    p=exp(wgt)
                else
                    p=0.
                endif
                ! accepting with Metropolis probability

                call random_number(x)
                if (x<p) then

                    if (ws%CA_(j1)>0.1*am) acc=acc+1
                    if (is_positive_a) then
                        ws%Ap_(j1)=aa1
                        ws%Ap_(j2)=aa2
                    else 
                        ws%Am_(j1)=aa1
                        ws%Am_(j2)=aa2
                    endif
                    ws%signal1_(:) = ws%signal2_(:)
                    x1=x2
                endif


            enddo

            if (mod(i,100) == 0) then
                arat=real(acc)/real(try)
                if (arat>0.1) then ! if accepting probability is large, reduce the size of future steps
                    if (ws%rfac<0.01) ws%rfac=ws%rfac*1.5
                else                  ! if accepting probability is small, increase the size of future steps
                    if (ws%rfac>0.001) ws%rfac=ws%rfac/1.5
                endif

                amp = maxval(ws%Ap_)
                amm = maxval(ws%Am_)

                ws%dAp_(:) = (0.1*ws%Ap_(:)/10.+0.01*amp)*ws%rfac
                ws%dAm_(:) = (0.1*ws%Am_(:)/10.+0.01*amm)*ws%rfac

                if (mod(i,num_steps/10)==0) then
                    write(*,'(A,I7,2x,A,f12.5,2x,A,I10,2x)') 'ann-step=', i,'chi2=', x1, 'accepted=', acc
                endif

                ws%temp=ws%temp/1.5
                acc=0
                try=0
            endif
        enddo

        ws%entropy = entropy()
        ws%tr = ComputeTrace()

        call ws%store(res%chars(), .True.)
    end function

end module
