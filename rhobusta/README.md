# Some basic FLAPW documentation

## DFT process and functions working on Global data structure

The following sequences starts with density (ro, rointr) and initializes the global variables that contain the potential:
| subroutine      | input  | output | Description     |
| :---        |    :----:   | :----: | :----          |
| smultro | |||
| vcoul   | |||
| add_vxc |

