
  ! this ugly macro is used for GFORTRAN to generate the l-value of buffer correctly.
  ! unfortunately it is not smart enough at the moment to understand array slices as l-values, and generates
  ! the wrong code. Instead, we supply the buffer directly to the MPI routine.
#define SendOrReceiveBuffer(direction, buffer, cnt, other, tag) \
  if (((direction) == 0).and. goparr) \
     call MPI_RECV(buffer, cnt, MPI_DOUBLE_COMPLEX, other, tag, comm_pnt, MPI_STATUS_IGNORE, ierror);\
  if (((direction) == 1).and. goparr) \
     call MPI_SEND(buffer, cnt, MPI_DOUBLE_COMPLEX, other, tag, comm_pnt,                    ierror)

! this is the original code, use it when the problem is fixed in GFORTRAN
!   subroutine SendOrReceiveBuffer(direction, buffer, cnt, other, tag)
!        !use mpi
!        use parallel_mod
!        integer, intent(in) :: direction, cnt, other, tag
!        type(*), dimension(..), intent(in) :: buffer
!        integer :: ierror
!
!        if (direction == 0) then
!            call MPI_RECV(buffer, cnt, MPI_DOUBLE_COMPLEX, other, tag, comm_pnt, MPI_STATUS_IGNORE, ierror)
!        else
!            call MPI_SEND(buffer, cnt, MPI_DOUBLE_COMPLEX, other, tag, comm_pnt,                    ierror)
!        end if
!    end subroutine


module flapw_image

    implicit none
    private
    public :: LoadImage, StoreImage, ReadRho, WriteRho, core_rho_written

    logical :: core_rho_written

    include 'mpif.h'

  contains

    function MergeOrDistributeBigData(direction, esizes, has_gf0) result(complete)
        !use mpi
        use FlapwMBPT, only: Image, FullEVB, FullZB, FullGF0,ExpansionSizes
        use parallel_mod
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod

        integer, intent(in) :: direction
        type(ExpansionSizes),intent(in) :: esizes
        logical, intent(in) :: has_gf0
        logical :: complete

        type(FullEVB), allocatable :: evb
        type(FullZB),  allocatable :: zb
        type(FullGF0),  allocatable :: gf0
        integer :: k1, k2, iproc, ispin, bsm, ierror

        complete = .True.

        ! the chunk of k's that belongs to our process
        k1 = n3_mpi_k(me3_k+1) + 1
        k2 = n3_mpi_k(me3_k+1) + ndim3_k(me3_k+1)

        if (goparr) call MPI_BARRIER(comm_pnt, ierror)
        ! Now store the big objects

        bsm = nbasmpw*nbndf
        if (maswrk) then
            allocate(evb)
            evb%ExpansionSizes = esizes
            call evb%allocate()
            if (direction == 0) evb%evb_(:,:,k1:k2,:) = ev_bnd(:,:,1:,:)
            if (direction == 1) then
                call evb%load('./flapw_image.h5:/')
                ev_bnd(:,:,1:,:) = evb%evb_(:,:,k1:k2,:)
            endif
            do iproc=2,nproc_k
                do ispin=1,nspin
                    SendOrReceiveBuffer(direction, evb%evb_(1:,1:,(n3_mpi_k(iproc) + 1):,ispin), ndim3_k(iproc)*bsm, iproc-1, ispin)
                end do
            end do
            if (direction == 0) call evb%store('./flapw_image.h5:/', flush_opt=.True.)
            if (goparr) call MPI_BARRIER(comm_pnt, ierror)
            deallocate(evb)
        else
            do ispin=1,nspin
                SendOrReceiveBuffer(1-direction, ev_bnd(1:,1:,1:,ispin), ndim3_k(me3_k+1)*bsm, 0, ispin)
            end do
            if (goparr) call MPI_BARRIER(comm_pnt, ierror)
        end if

        bsm = nfun*nbndf
        inquire(file='./overlaps.tmp.h5', exist=complete)
        if (maswrk) then
            allocate(zb)
            zb%ExpansionSizes = esizes
            call zb%allocate()
            if (direction == 0) zb%zb_(:,:,k1:k2,:) = z_bnd(:,:,1:,:)
            if (direction == 1) then
                if (complete) then 
                    call zb%load('./overlaps.tmp.h5:/')
                    z_bnd(:,:,1:,:) = zb%zb_(:,:,k1:k2,:)
                else
                    deallocate(zb)
                    return
                end if
            endif
            do iproc=2,nproc_k
                do ispin=1,nspin
                    SendOrReceiveBuffer(direction, zb%zb_(1:,1:,(n3_mpi_k(iproc) + 1):,ispin), ndim3_k(iproc)*bsm, iproc-1, ispin)
                end do
            end do
            if (direction == 0) call zb%store('./overlaps.tmp.h5:/', flush_opt=.True.)

            if (goparr) call MPI_BARRIER(comm_pnt, ierror)
            deallocate(zb)
        else
            if (.not. complete .and. direction ==1) return
            do ispin=1,nspin
                SendOrReceiveBuffer(1-direction, z_bnd(1:,1:,1:,ispin), ndim3_k(me3_k+1)*bsm, 0, ispin)
            end do
            if (goparr) call MPI_BARRIER(comm_pnt, ierror)
        end if
    end function

    subroutine WriteRho()
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod

        use FlapwMBPT, only: ElDensity, ElDensityWithCore
        implicit none


        type(ElDensity), allocatable :: rho
        type(ElDensityWithCore), allocatable :: rho_core

        allocate(rho)
        allocate(rho_core)
        rho%nspin_0 = nspin_0
        rho%maxmt = maxmt
        rho%nplwro = nplwro
        rho%maxmtb = maxmtb
        rho%nrmax = nrmax
        rho%nsort = nsort
        rho_core%nrmax = nrmax
        rho_core%nsort = nsort
        rho_core%nspin_0 = nspin_0

        call rho%allocate()
        call rho_core%allocate()
        rho%ro_ = ro
        rho%rointr_ = rointr
        rho_core%ro_core_ = ro_core

        if (magn == 2) then
            rho%spmt_ = spmt
            rho%spintr_ = spintr
        end if
        call rho%store("./el-density.core.h5:/", flush_opt=.True.)
        call rho_core%store("./el-density.core.h5:/", flush_opt=.True.)
        deallocate(rho,rho_core)
        core_rho_written = .True.
    end subroutine

    subroutine ReadRho()
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod

        use FlapwMBPT, only: ElDensity, ElDensityWithCore
        implicit none

        type(ElDensity), allocatable :: rho
        type(ElDensityWithCore), allocatable :: rho_core

        allocate(rho)

        if (.not. core_rho_written) return ! already read from image

        call rho%load("./el-density.core.h5:/")

        ro = rho%ro_
        if (rho%nrmax>0) then
            allocate(rho_core)
            call rho_core%load("./el-density.core.h5:/")
            !ro_core = rho_core%ro_core_
            deallocate(rho_core)
        endif 

        rointr = rho%rointr_
        if (magn == 2) then
            spmt = rho%spmt_
            spintr = rho%spintr_
        end if
        deallocate(rho)
    end subroutine


    function LoadImage() result(complete)
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        use FlapwMBPT, only: Image, FullEVB, FullZB, FullGF0, ExpansionSizes

        type(Image),   allocatable :: img
        type(ExpansionSizes) :: esizes
        logical has_gf0, complete

        allocate(img)

        call img%load('./flapw_image.h5:/')

        ! TODO: verify all sizes match the ones from the file!

        chem_pot = img%chem_pot
        !ubi_0 = img%ubi%chars()
        ubi_0 = 'dft'

        e_core = img%e_core_
        pcor = img%pcor_
        qcor = img%qcor_

        n_bnd = img%n_bnd_
        e_bnd = img%e_bnd_
        g_loc_0 = img%g_loc_0_

        gfun = img%gfun_
        p_f = img%p_f_
        pd_f = img%pd_f_
        pd2_f = img%pd2_f_
        q_f = img%q_f_
        qd_f = img%qd_f_
        qd2_f = img%qd2_f_


        esizes = img%ExpansionSizes
        has_gf0 = img%has_gf0

        if(irel>=1) img%gfund = img%gfund_
        eny = img%eny_
        ffsmt = img%ffsmt_

        ! read_ro
        ro = img%rho%ro_
        rointr = img%rho%rointr_

        if (magn == 2) then
            spmt = img%rho%spmt_
            spintr = img%rho%spintr_
        end if
        deallocate(img)

        complete = MergeOrDistributeBigData(1, esizes, has_gf0)


        ! TODO:
        ! unfortunately this should not be here, but it is homeless
        ! because it was in Andrey's original restart_solid.f
        ! and we don't want to do this at the caller

        pcor_old=pcor
        qcor_old=qcor
        gfun_old=gfun
        if(irel>=1) gfund_old=gfund


        core_rho_written = .False. ! we just got it from the image
        
        if (maswrk) print *, " -- done"
    end function

    subroutine StoreImage()
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        use FlapwMBPT, only: Image
        use stringifor
        use stringifor_string_t
        !use mpi

        use FlapwMBPT, only: Image, ExpansionSizes

        type(Image),   allocatable :: img
        type(ExpansionSizes) :: esizes
        character(len=3) :: tmps
        logical :: complete ! irrelevant

        if (maswrk) then

            print *, " - writing Fortran checkpoint"
            allocate(img)

            img%ncormax = ncormax
            img%nspin_0 = nspin_0
            img%nsort = nsort
            img%maxmtcor = maxmtcor
            img%has_gf0 = allocated(g_full_0)

            img%npnt = npnt
            img%nbndf = nbndf
            img%nspin = nspin
            img%nbasmpw = nbasmpw
            img%nfun = nfun

            img%maxel = maxel
            img%natom = natom

            img%maxwf = maxwf
            img%nlmb = nrel*maxb+1
            img%maxntle = maxntle
            img%maxnrad = maxnrad

            img%rho%nspin_0 = nspin_0
            img%rho%maxmt = maxmt
            img%rho%nplwro = nplwro
            img%rho%maxmtb = maxmtb

            esizes = img%ExpansionSizes

            img%chem_pot = chem_pot
            tmps = ubi

            call img%allocate()
            call img%rho%allocate()

            img%e_core_ = e_core
            img%pcor_ = pcor
            img%qcor_ = qcor

            img%n_bnd_ = n_bnd
            img%e_bnd_ = e_bnd
            img%g_loc_0_ = g_loc_0

            img%gfun_ = gfun
            img%p_f_ = p_f
            img%pd_f_ = pd_f
            img%pd2_f_ = pd2_f
            img%q_f_ = q_f
            img%qd_f_ = qd_f
            img%qd2_f_ = qd2_f

            if(irel>=1) img%gfund_ = gfund
            img%eny_ = eny
            img%ffsmt_ = ffsmt

            ! here we replicate write_ro

            img%rho%ro_ = ro
            img%rho%rointr_ = rointr

            if (magn == 2) then
                img%rho%spmt_ = spmt
                img%rho%spintr_ = spintr
            end if
            call img%store('./flapw_image.h5:/', flush_opt=.True.)

            deallocate(img)
        end if

        complete = MergeOrDistributeBigData(0, esizes, allocated(g_full_0))

        if (maswrk) print *, " -- done"

    end subroutine


end module
