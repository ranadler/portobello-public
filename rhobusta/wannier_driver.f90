

module wannier_driver

    use wannier
    use wannier_internal

    implicit none

    private

    type(Wannier90Workspace),target, allocatable :: w90
    type(wannierOutput), target, allocatable     :: wout

    public :: ComputeImp

contains

    subroutine setup(win, outputpath)
        use solid_mod
        use atom_mod
        use manager_mod
        use parallel_mod
        use mpi
        use units_mod, only: evolt, bohr, pi

        type(WannierInput), intent(inout) :: win
        type(string) :: outputpath

        character(30)  :: seed
        character(20)  :: atom_symbols(natom)

        integer :: iatom, ispin, ik, ia, ib, ic, mpierr

        ! output parameters from setup
        integer :: &
            proj_l(nbndf), proj_m(nbndf), proj_radial(nbndf),&
            proj_s(nbndf), &
            exclude_bands(nbndf)  ! not used since we never write exclusions
        real*8 :: proj_site(3,nbndf), proj_z(3, nbndf), proj_x(3, nbndf),&
                  proj_zona(nbndf), proj_s_qaxisx(3,nbndf)

        integer :: win_f

        type(BandStructure), pointer :: Ksbsp => null()
        type(Wannier90Workspace),pointer :: w90p

        real*8, allocatable :: v(:)
        allocate(v(3))

        allocate(w90)
        w90p => w90
        
        seed = '_wannier' ! used for the wannier output file
        if (maswrk) then
            print *, 'nspin_0 is ', nspin_0, ', this is not supposed to work with 2'
        endif
        ispin = 1

        w90%num_wann   = win%num_orbs
        w90%min_band  = win%min_band
        w90%max_band  = win%max_band
        w90%num_bands  = win%max_band - win%min_band + 1
        w90%dis_window = win%dis_window
        w90%num_k_all  = nqdiv

        ! we got all the bounds, including num_wann - now allocate the arrays
        call w90%allocate()

        ! NOICE!
        ! Assignment without (:,:) will produce wrong values, because it does
        !    not cast the single precision values to double precision and tries
        !    to assign the memory chunk.
        !    This seems like a bug in ifort18
        
        do ik=1, w90%num_k_all
            v = matmul(transpose(rbas), pnt(:,ik))
            w90%k_lat_(:,ik) = v(:)
        enddo

        do iatom=1, natom
            atom_symbols(iatom)=trim(txtel(is(iatom)))
        enddo

        if (maswrk) then
            call wannier_setup(& ! --------------------- input parameters first -----------------------------------
                seed, &   ! base file name - for output file
                ndiv,&    ! dimensions of the grid
                w90%num_k_all,&   ! num of k points
                rbas*bohr*par, & ! the lattice coords in angst., cartesian
                gbas/bohr*2*pi/par,& ! reciprocal, in units of angst, cartesian
                w90%k_lat_,&  !double(3,num_kpts), k-points lattice in cartesian coords
                w90%num_bands, &  ! total number of bands including semi-core states
                natom,& ! total number of atoms
                atom_symbols,& !character(len=20), dimension(num_atoms) elemental symbols of the atoms
                tau*par*bohr,& ! cartesian positions of the atoms in angstrom.
                .false.,&  ! only gamma point in the k-points
                .false., &  ! Haven't implemented spinful case yet (spinors = .true.)
                ! --------------------------------- Next - output parameters --------------------------------
                w90%overlap%nntot,&         ! nearest neighbors total for each k-point
                w90%nnlist_,&        ! integer(num_kpts,num_nnmax):
                w90%nncell_,&        ! integer(3,num_kpts,num_nnmax)
                w90%num_bands,&     ! integer number of bands used by the DFT (???)
                w90%num_wann,&      ! integer number of wannier functions to be extracted
                proj_site,&     ! double(3,nbndf)   !Projection function centre in crystallographic co-ordinates relative to the direct lattice vectors.
                proj_l,&        ! integer(num_bands_tot)
                proj_m,&        ! integer(num_bands_tot)
                proj_radial,&   ! integer(num_bands_tot) readial part of the projection function
                proj_z,&        ! double(3, num_bands_tot) axis Z
                proj_x,&        ! double(3, num_bands_tot) axis X
                proj_zona,&     ! double(num_bands_tot) z/a
                exclude_bands,& ! integer(num_bands_tot) list of bands to exclude from calc
                proj_s,&        ! integer(num_bands_tot) 1 or -1 denote proj. spin
                proj_s_qaxisx) ! double(3, num_bands_tot) spin quantisation axis
        endif

        if (nproc_k/=1) then
            call mpi_bcast(w90%overlap%nntot,1,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(w90%nnlist_,nqdiv*w90%num_nnmax,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(w90%nncell_,3*nqdiv*w90%num_nnmax,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(w90%num_bands,1,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(w90%num_wann,1,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(proj_site,3*nbndf,mpi_double_precision,0,mpi_comm_world,mpierr)
            call mpi_bcast(proj_l,nbndf,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(proj_m,nbndf,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(proj_radial,nbndf,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(proj_z,3*nbndf,mpi_double_precision,0,mpi_comm_world,mpierr)
            call mpi_bcast(proj_x,3*nbndf,mpi_double_precision,0,mpi_comm_world,mpierr)
            call mpi_bcast(proj_zona,nbndf,mpi_double_precision,0,mpi_comm_world,mpierr)
            call mpi_bcast(exclude_bands,nbndf,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(proj_s,nbndf,mpi_integer,0,mpi_comm_world,mpierr)
            call mpi_bcast(proj_s_qaxisx,3*nbndf,mpi_double_precision,0,mpi_comm_world,mpierr)
        endif

        ! note that nntot is set in the call above, so the call to allocate() is well defined
        w90%overlap%Wannier90Bounds%num_bands = w90%Wannier90Bounds%num_bands
        w90%overlap%Wannier90Bounds%num_wann = w90%Wannier90Bounds%num_wann
        w90%overlap%Wannier90Bounds%num_k_all = w90%Wannier90Bounds%num_k_all
        call w90%overlap%allocate()

        call run()

        deallocate(w90)

    contains

        subroutine run()
            use wannier_input_m
            use hdf5_base

            real*8 :: tempvec2(3)
            logical :: lwindow(w90%num_bands,w90%num_k_all)
            real*8  :: wann_spreads(w90%num_wann), total_spread(3)
            integer :: irk, ibnd, k, k0, iwan, ishell, ispin
            integer :: eig_fh, amn_fh, ierr

            integer :: j, i, m,indtmp, aa, bb, cc, ii, jj, tempint,indexb,ib,jbnd
            real*8 :: tempvec(3), kdottau, distance,temp_distance

            type(BandStructure),target, allocatable :: KSbs

            ispin = 1 ! TODO: should we do 2 spins? choose one of them?

            lwindow(:,:) = .True.  ! all bands that we pass are in the outer window. We filter before setting it up.

            allocate(KSbs)
            Ksbsp => KSbs
            call KSbs%load(win%location_of_partial_bs_all_k%chars())

            if (maswrk) then
                print *, "writing A matrix => wannier.amn"
                open(newunit = amn_fh, file = trim(seed)//'.amn')
                write(amn_fh,'(3i8)')
                write(amn_fh,'(3i8)') w90%num_bands, nqdiv, w90%num_wann
                do irk = 1, nqdiv
                    do iwan = 1, w90%num_wann
                        do ibnd = 1, w90%num_bands
                            write(amn_fh,'(3i8,2x,2F17.12)') ibnd, iwan, irk,&
                                win%A_(ibnd,iwan,i_kref(irk))
                        enddo
                    enddo
                enddo
                close(unit = amn_fh)
            endif 

            do k=1,w90%num_k_all
                w90%eigenvalues_(:,k) = (KSbs%energy_(:,i_kref(k),ispin) - KSbs%chemical_potential)*evolt/2.0d0 
            end do
            
            if (maswrk) then
                open(newunit=eig_fh, file=trim(seed) // '.eig', status="replace")
                do irk = 1, w90%num_k_all
                    do ibnd = 1,w90%num_bands
                        write(eig_fh,*) ibnd, irk, w90%eigenvalues_(ibnd, irk)
                    end do
                end do
                close(eig_fh)
            endif

            allocate(wout)
            wout%num_orbs = w90%num_wann
            wout%min_band = win%min_band
            wout%max_band = win%max_band
            wout%num_k_all = w90%num_k_all
            call wout%allocate()

            if (maswrk) then
                print *, "Calculating M matrix => wannier.mmn"
            endif
            
            call calc_m_matrix(w90p, Ksbsp, ispin, seed)

            deallocate(KSbs) ! free space before running Wannier

            if (maswrk) then
                print *, "Running Wannier..."
            endif
            
            if (maswrk) then
                call wannier_run(seed, &
                    ndiv,&
                    w90%num_k_all,&
                    rbas*par*bohr,&
                    gbas*2.0d0*pi/par/bohr,&
                    w90%k_lat_,&
                    w90%num_bands,&   ! not num_bands, because  "if (library) num_bands = num_bands - num_exclude_bands" is in parameters.f90. Sang removed it!
                    w90%num_wann, w90%overlap%nntot,natom,atom_symbols,&
                    tau*par*bohr,.false.,&
                    w90%overlap%M_(:,:,:,:),&
                    win%A_(:,:,:),&
                    w90%eigenvalues_(:,:),& 
                    !! ------------------------------- Now output ----------------------------------->
                    w90%U_,&    !! --- next are optional variables --->
                    w90%U_opt_,&
                    lwindow,&
                    wout%centers_,&
                    wann_spreads,total_spread)
            endif

            if (maswrk) then
                print *, "Finishing up..."
            endif
            
            if (nproc_k/=1) then
                call mpi_bcast(lwindow,w90%num_bands*nqdiv,  mpi_logical,0, mpi_comm_world,mpierr)
                call mpi_bcast(wout%centers_,3*w90%num_wann, mpi_double_precision,0, mpi_comm_world,mpierr)
                call mpi_bcast(wann_spreads,w90%num_wann, mpi_double_precision,0, mpi_comm_world,mpierr)
                call mpi_bcast(total_spread,3,  mpi_double_precision,0, mpi_comm_world,mpierr)
            endif

            ! associate the functions with nearest atom (by their centers)
            do ii=1, w90%num_wann
                distance=100.0d0
                do jj=1, natom
                    do aa=-ndiv(1)+1, ndiv(1)-1
                        do bb=-ndiv(2)+1, ndiv(2)-1
                            do cc=-ndiv(3)+1, ndiv(3)-1
                                tempvec=(tau(:,jj)+&
                                    rbas(:, 1)*aa+rbas(:,2)*bb+rbas(:,3)*cc)*par*bohr&
                                    -wout%centers_(:,ii)
                                temp_distance=dsqrt(sum(tempvec*tempvec))
                                if (distance .gt. temp_distance) then
                                    distance = temp_distance
                                    wout%nearest_atom_(ii)=jj
                                    wout%distance_to_nearest_atom_(:,ii)=tempvec
                                endif
                            enddo
                        enddo
                    enddo
                enddo
            enddo

            do irk=1,w90%num_k_all
                do j=1,w90%num_wann
                    tempvec2=matmul(transpose(gbas),wout%centers_(:,j)/(par*bohr)-tau(:,wout%nearest_atom_(j)))
                    kdottau=2.0d0*pi*sum(tempvec2*w90%k_lat_(:,irk))
                    do i=1,w90%num_wann
                        indtmp=0
                        do m=1,w90%num_bands
                            if (lwindow(m,irk)) then
                                indtmp=indtmp+1
                                ! TODO: handle the spin index
                                wout%P_(m,j,irk, 1)= wout%P_(m,j,irk, 1) + w90%U_opt_(indtmp,i,irk)&
                                                  * w90%U_(i,j,irk)
                            endif
                        enddo
                    enddo
                enddo
            enddo

            if (maswrk) then
                call wout%store(outputpath%chars())
            endif 
            deallocate(wout)

        end subroutine

        ! caluclate wout%expn%{A,Z}
        ! Adapted from Sang's code, without the K-resolution - now we use one k point to represent
        ! the expansion of the orbitals.
!        subroutine CalcExpansion(ispin)
!            use solid_mod
!            use atom_mod
!            use band_structure
!            implicit none
!
!            integer, intent(in) :: ispin
!            integer :: iq
!
!            wout%expn%num_k = 1
!            wout%expn%num_expanded = w90%num_wann
!            wout%expn%num_muffin_orbs = KSbsp%num_muffin_orbs
!            wout%expn%max_num_pw = KSbsp%max_num_pw
!            call wout%expn%allocate()
!
!            ! calculate lte_wan, gks_wan
!            do iq=1, w90%num_k_all
!                !TODO; use matmul instead, or the matrix operation *
!
!                call zgemm('n','n',nfun,w90%num_wann,w90%num_bands,(1.0d0,0.0d0),&
!                    KSbsp%Z_(1,1,iq,ispin),nfun,wout%P_(1,1,iq,ispin),w90%num_bands,&
!                    (1.0d0,0.0d0), wout%expn%Z_(1,1,1,ispin),nfun)  ! the 1+0j means this adds for all k
!
!                call zgemm('n','n',nbasmpw,w90%num_wann,w90%num_bands,(1.0d0,0.0d0),&
!                    KSbsp%A_(1,1,iq,ispin),nbasmpw,wout%P_(1,1,iq,ispin),w90%num_bands,&
!                    (1.0d0,0.0d0), wout%expn%A_(1,1,1,ispin),nbasmpw) ! the 1+0j means this adds for all k
!            enddo
!        end subroutine

    end subroutine



    !================================!
    subroutine wigner_seitz(nrpts, irvec)
        !================================!
        !     Calculates a grid of lattice vectors r that fall inside (and even
        !     on the surface of) the Wigner-Seitz supercell centered on the
        !     origin of the Bravais superlattice with primitive translations
        !     ndiv(1)*a_1, ndiv(2)*a_2, and ndiv(3)*a_3                       !
        !=======================================================================
        use solid_mod
        use manager_mod
        use units_mod
        use parallel_mod
        implicit none


        !     irvec(i,irpt)     The irpt-th Wigner-Seitz grid point has componen
        !     irvec(1:3,irpt) in the basis of the lattice vectors
        !     ndegen(irpt)      Weight of the irpt-th point is 1/ndegen(irpt)
        !     nrpts             number of Wigner-Seitz grid points

        integer, intent(out) :: nrpts,  irvec(3, 8*w90%num_k_all)
        integer :: ndegen(8*w90%num_k_all)

        integer       :: ndiff(3),rpt_origin
        double precision :: dist(125),tot,dist_min,real_metric(3,3)
        integer       :: n1,n2,n3,i1,i2,i3,icnt,i,j,ir

        ndegen=0
        irvec=0

        !$$$  if (timing_level>1.and.on_root)
        !$$$  $  call io_stopwatch('postw90_common: wigner_seitz',1)

        !     The Wannier functions live in a periodic supercell of the real spa
        !     cell. This supercell is ndiv(i) unit cells long along each primiti
        !     translation vector a_i of the unit cell
        !
        !     We loop over grid points r on a cell that is approx. 8 times
        !     larger than this "primitive supercell."
        !
        !     One of these points is in the W-S supercell if it is closer to R=0
        !     of the other points R (where R are the translation vectors of the
        !     supercell). In practice it is sufficient to inspect only 125 R-poi

        !     In the end, nrpts contains the total number of grid points that ha
        !     found in the Wigner-Seitz cell
        real_metric=matmul(transpose(rbas), rbas)*(par*bohr)**2

        nrpts = 0
        do n1 = -ndiv(1) , ndiv(1)
            do n2 = -ndiv(2), ndiv(2)
                do n3 = -ndiv(3),  ndiv(3)
                    !     Loop over the 125 points R. R=0 corresponds to i1=i2=i3=0,
                    !     or icnt=63
                    icnt = 0
                    do i1 = -2, 2
                        do i2 = -2, 2
                            do i3 = -2, 2
                                icnt = icnt + 1
                                !     Calculate distance squared |r-R|^2
                                ndiff(1) = n1 - i1 * ndiv(1)
                                ndiff(2) = n2 - i2 * ndiv(2)
                                ndiff(3) = n3 - i3 * ndiv(3)
                                dist(icnt) = 0.0d0
                                do i = 1, 3
                                    do j = 1, 3
                                        dist(icnt)=dist(icnt)+&
                                            dble(ndiff(i))*real_metric(i,j)*dble(ndiff(j))
                                    enddo
                                enddo
                            enddo
                        enddo
                    enddo
                    dist_min=minval(dist)
                    if (abs(dist(63) - dist_min ) .lt.1.0d-7) then ! R=0
                        nrpts = nrpts + 1
                        !$$$  if(.not. count_pts) then
                        ndegen(nrpts)=0
                        do i=1,125
                            if(abs(dist(i)-dist_min).lt.1.0d-7)&
                                ndegen(nrpts)=ndegen(nrpts)+1
                        end do
                        irvec(1, nrpts) = n1
                        irvec(2, nrpts) = n2
                        irvec(3, nrpts) = n3
                        !
                        !     Remember which grid point r is at the origin
                        !
                        if (n1==0 .and. n2==0 .and. n3==0) rpt_origin=nrpts
                        !$$$  endif
                    end if
                    !     n3
                enddo
                !     n2
            enddo
            !     n1
        enddo

        !     Check the "sum rule"
        tot = 0.0d0
        do ir = 1, nrpts
            !
            !     Corrects weights in Fourier sums for R-vectors on the boundary of
            !     W-S supercell
            !
            tot=tot+1.0d0/dble(ndegen(ir))
        enddo
        if (abs(tot-dble(ndiv(1)*ndiv(2)*ndiv(3))) > 1.0d-8)&
            call ending('ERROR in wigner_seitz: error in finding Wigner-Seitz points')

    end subroutine wigner_seitz



    ! adler: I think this is doing the wannier interpolation Vdagger_k * M * V_k
    subroutine from_band_to_wan_coarse_grid(matin,matout)

        use parallel_mod
        use solid_mod
        use units_mod
        implicit none

        complex*16,intent(in) :: matin(w90%num_bands,w90%num_bands,w90%num_k_all)
        complex*16,intent(out) :: matout(w90%num_wann,w90%num_wann,w90%num_k_all)

        integer :: ik
        complex*16 :: tempmat(w90%num_bands,w90%num_wann)

        matout=0.0d0

        ! TODO: handle spin
        do ik=1, w90%num_k_all
            tempmat=0.0d0
            call zgemm('n','n',w90%num_bands,w90%num_wann,w90%num_bands,(1.0d0,0.0d0),&
                matin(1,1,ik),w90%num_bands,wout%P_(1,1,ik, 1),w90%num_bands,&
                (0.0d0,0.0d0),tempmat,w90%num_bands)
            call zgemm('c','n',w90%num_wann,w90%num_wann,w90%num_bands,(1.0d0,0.0d0),&
                wout%P_(1,1,ik,1),w90%num_bands,tempmat,w90%num_bands,&
                (0.0d0,0.0d0),matout(1,1,ik),w90%num_wann)
        enddo
    end subroutine



    function ComputeImp(selfpath, outputpath) result(res)
        !use w90_comms, only: comms_setup,comms_end ! work around bug with no MPI in w90
        type(string) :: res
        type(string), intent(in) :: selfpath
        type(string), intent(in) :: outputpath
        type(WannierInput) :: self
        !integer(kind=c_int),value :: num_workers

        call self%load(selfpath%chars())
        
        ! need this one to setup up MPI first
        !call comms_setup 

        call setup(self, outputpath)

        !call comms_end don't want to terminate MPI in fortran anymore

        res = ""

    end function

end module

