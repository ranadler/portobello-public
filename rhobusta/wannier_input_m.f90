module wannier_input_m
    use LAPW, only:BandStructure
    use wannier, only:WannierOutput
implicit none

private

    type(BandStructure), pointer :: KSBsp

public :: calc_m_matrix

contains

!
! this code is roughly copied from Sangkook Choi's MatDelab ComWann
!


subroutine calc_m_matrix(w90, bs, ispin, seed)
    use atom_mod
    use solid_mod
    use wannier
    use wannier_internal
    use band_structure
    use mpi
	use parallel_mod
    implicit none
    ! pointer to the main workspace
    type(Wannier90Workspace),intent(inout), pointer :: w90
    type(BandStructure),intent(inout), pointer :: bs

    character(30), intent(in)  :: seed
    integer, intent(in) :: ispin
    integer :: ind_k,k,k0,kq,kq0,iq0,shiftk(3),num_irr_k
    double precision :: bvec(3), fifi_j(maxlfun,maxlfun,0:2*maxb,nsort)
    complex*16 :: ff(maxel,maxel,natom)
    complex*16, allocatable :: znew(:,:), znew1(:,:), anew(:,:), anew1(:,:), z_wan(:,:), a_wan(:,:), &
        ev_bnd_combined(:,:,:,:)

    KSBsp => bs

    allocate(znew(nfun,w90%num_bands))
    allocate(znew1(nfun,w90%num_bands))
    allocate(anew1(nbasmpw,w90%num_bands))
    allocate(anew(nbasmpw,w90%num_bands))
    allocate(z_wan(nfun, w90%num_bands))
    allocate(a_wan(nbasmpw, w90%num_bands))

    num_irr_k = sum(ndim_k(:))

    !Workers need ev_bnd on all irr_k points
    allocate(ev_bnd_combined(nbasmpw,nbndf,num_irr_k,1)) !1 for nspin
    ev_bnd_combined = 0.0d0
    do ind_k=1,ndim_k(me+1)
        k = n_mpi_k(me+1)+ind_k
        ev_bnd_combined(:,:,k,:) = ev_bnd(:,:,ind_k,:)
    enddo
    if (nproc_k/=1) then
        call mpi_allreduce_dcmplx(ev_bnd_combined, nbasmpw*nbndf*num_irr_k, mpi_sum, mpi_comm_world)
    endif

    do ind_k=1,ndim_kk(me+1) 
        k=n_mpi_kk(me+1)+ind_k
        k0 = i_kref(k)

        if (modulo(k,1) == 0) print *, " ... calculating M for k>=", k, "/", w90%num_k_all
        do iq0=0,w90%overlap%nntot          ! b vector
            if (iq0 .eq. 0) then
                bvec = 0.d0
                kq = k
            else
                kq=w90%nnlist_(k,iq0) ! the neighbor
                shiftk = w90%nncell_(:,k,iq0)
                bvec = matmul(gbas, w90%k_lat_(:,kq) + shiftk - w90%k_lat_(:,k))
            end if

            fifi_j=0.0d0
            ff    =0.0d0
            znew=0.0d0
            anew=0.0d0
            znew1=0.0d0
            anew1=0.0d0
            ! radial
            call fifi_j_prepare_onepoint(fifi_j,bvec,ispin)

            ! add angular
            call integral_band_pw_band_mt_wan(fifi_j,ff,bvec) ! ff is the output

            ! ---------- go from irr wedge to full BZ for A and Z!
            z_wan(:,:) = Ksbsp%Z_(:, :, k0, ispin)
            a_wan(:,:) = ev_bnd_combined(:, w90%min_band:w90%max_band, k0, ispin)

            call sym_z_0(znew,k,z_wan,w90%num_bands,k_group(k),pnt(1,k)) 
            call sym_a_2(anew,k,a_wan,w90%num_bands,k_group(k),k0)
            
            if (iq0 .eq. 0) then
                kq=k
            else
                kq=w90%nnlist_(k,iq0)
            endif
            kq0=i_kref(kq)

            z_wan(:,:) = Ksbsp%Z_(:, :, kq0, ispin)
            a_wan(:,:) = ev_bnd_combined(:, w90%min_band:w90%max_band, kq0, ispin)

            call sym_z_0(znew1,kq,z_wan,w90%num_bands,k_group(kq),pnt(1,kq))
            call sym_a_2(anew1,kq,a_wan,w90%num_bands,k_group(kq),kq0)   

            ! this calculates the 2 contributions and adds them
            if (iq0 .eq. 0) then
               call integral_band_pw_band_wan(w90, ff, znew,anew,znew1,anew1, &
                    w90%M0_(:,:,k), k, kq, [0,0,0], w90%num_bands)
            else
                call integral_band_pw_band_wan(w90, ff, znew,anew,znew1,anew1, &
                    w90%overlap%M_(:,:,iq0,k), k, kq, shiftk, w90%num_bands)
            end if

        enddo
    enddo

    ! this is for debug - it's supposed to be unity
    where (abs(w90%M0_) < 1.0d-10) w90%M0_ = 0

    if (nproc_k/=1) then
        call mpi_allreduce_dcmplx(w90%overlap%M_,w90%num_bands**2*nqdiv*w90%overlap%nntot, mpi_sum,mpi_comm_world)
    endif 

    if (maswrk) then
        call output(seed)
    endif

    deallocate(znew)
    deallocate(znew1)
    deallocate(anew1)
    deallocate(anew)
    deallocate(z_wan)
    deallocate(a_wan)
    deallocate(ev_bnd_combined)

    contains

    subroutine output(seed)
        integer :: mmn_fh, n, iband1, iband2, irk
        character(30)  :: seed

        open(newunit = mmn_fh, file = trim(seed) // ".mmn", form = 'formatted')
        write(mmn_fh,*)
        write(mmn_fh,'(3i8)') w90%num_bands, w90%num_k_all, w90%overlap%nntot
        do irk = 1, w90%num_k_all
            do n = 1,  w90%overlap%nntot
                write(mmn_fh,'(5i8)') irk, w90%nnlist_(irk,n), w90%nncell_(:,irk,n)
                do iband2 = 1,  w90%num_bands
                    do iband1 = 1, w90%num_bands
                        write(mmn_fh,'(2F17.12)') w90%overlap%M_(iband1,iband2,n,irk)
                     enddo
                enddo
            enddo
        enddo
        close(mmn_fh)

    end subroutine
end subroutine


! why integrate numerically - kutepov already has the overlaps ?
subroutine fifi_j_prepare_onepoint(fifi_j,pn,ispin)
    use atom_mod
    use manager_mod
    use solid_mod
    use units_mod
    implicit none
    integer,intent(in) :: ispin
    double precision, intent(in) :: pn(3)
    double precision, intent(out) :: fifi_j(maxlfun,maxlfun,0:2*maxb,nsort)
    integer :: isort,ir,l,mt,mt1,lf,lf1
    double precision :: pi4,rv,dqdall,c2,sqpi4,pp,pi2a,&
        bess(0:2*maxb,0:maxnrad),bessd(0:2*maxb),&
        work(0:maxnrad),work1(0:maxnrad), qq

    fifi_j=0.0d0

    pi4=4.d0*pi
    pi2a=(pi+pi)/par
    sqpi4=sqrt(pi4)
    c2=clight**2

    qq=sqrt(dot_product(pn,pn))*pi2a ! bvec

    do isort=1,nsort
        do ir=0,nrad(isort)
            rv=r(ir,isort)
            ! adler : this is coming from the spherical wave expansion of the
            ! plane wave exp(ib(r-R))
            call BESSR(qq,rv,bess(0,ir),bessd,2*lmb(isort))
            rv=rv*rv
            work(ir)=rv*dr(ir,isort)
        enddo
        do l=0,2*lmb(isort)
            do lf1=1,lfun(isort)
                mt1=ind_wf(lf1,isort)
                do lf=1,lfun(isort)
                    mt=ind_wf(lf,isort)
                    do ir=0,nrad(isort)
                        pp=gfun(mt+ir,ispin)*gfun(mt1+ir,ispin)
                        if(irel>=1) pp=pp+gfund(mt+ir,ispin)*gfund(mt1+ir,ispin)/c2
                        work1(ir)=work(ir)*bess(l,ir)*pp
                    enddo
                    ! adler: dqall does numeric integration
                    fifi_j(lf,lf1,l,isort)=pi4*dqdall(h(isort),work1,nrad(isort))
                enddo               !! over lf
            enddo                 !! over lf1
        enddo                   !! over l
    enddo                     !! over iatom
end subroutine


! is this the angular part of the integral?
subroutine integral_band_pw_band_mt_wan(fifi_j,ff,pn)
    use atom_mod
    use manager_mod
    use parallel_mod
    use solid_mod
    use units_mod
    implicit none
    double precision, intent(in) :: fifi_j(maxlfun,maxlfun,0:2*maxb,nsort),pn(3)
    complex*16, intent(out) :: ff(maxel,maxel,natom)

    integer :: iatom,isort,nl,ndimb,lm1,lm2,l1,l2,ii,&
        iii,indx,icg1,icg2,i2,mj1,mj2,i1,li1,li2,icg,lmi,lget,&
        l_i,nm1,nm2,ie1,km1,lf1,ie2,km2,lf2,in1,&
        jn2,in2,jn1
    double precision :: fas,q(3),qq,sqpi4,pi2
    complex*16 :: c1,c2
    double precision, allocatable :: ylm(:)

    allocate(ylm((2*maxb+1)**2))
    pi2=pi+pi
    sqpi4=sqrt(4.d0*pi)
    q=pn
    qq=sqrt(dot_product(q,q))

    if(qq.gt.1.d-9) then
        call sphharm(q(1)/qq,q(2)/qq,q(3)/qq,2*maxb,ylm)
    else
        ylm=0.d0
        ylm(1)=1.d0/sqpi4
    endif
    ff=(0.d0,0.d0)

    do iatom=1,natom
        isort=is(iatom)
        nl=lfunm(isort)
        ndimb=nrel*(lmb(isort)+1)**2
        fas=pi2*dot_product(pn,tau(:,iatom))   ! adler: what about a par factor? does it cancel btwn pn and tau?
        c1=exp(dcmplx(0.d0,-fas))
        do lm1=1,ndimb
            do lm2=1,ndimb
                if(irel<=1) then
                    l1=lget(lm1)
                    l2=lget(lm2)
                    ii = max0(lm1,lm2)
                    iii = min0(lm1,lm2)
                    indx = (ii*(ii-1))/2 + iii
                    icg1 = indxcg(indx)
                    icg2 = indxcg(indx+1) - 1
                else
                    call getlimj(lm2,l2,i2,mj2,li2,0)
                    call getlimj(lm1,l1,i1,mj1,li1,0)
                    icg1=indxcgr(lm1,lm2,1,0)
                    icg2=indxcgr(lm1,lm2,2,0)
                endif
                do icg = icg1, icg2
                    if(irel<=1) then
                        lmi = jcg(icg)  ! CG integrals   ! adler: this come from the expansion in spherical waves
                        l_i=lget(lmi)
                        c2=c1*dcmplx(0.d0,-1.d0)**l_i*ylm(lmi)*cg(icg)
                    else
                        lmi=jcgr(icg)
                        l_i=lget(lmi)
                        c2=c1*dcmplx(0.d0,-1.d0)**l_i*ylm(lmi)*cgr(icg)
                    endif
                    nm1=ntle(l1,isort)
                    nm2=ntle(l2,isort)
                    do ie1=1,nm1
                        in1=1
                        if(augm(ie1,l1,isort)/='LOC') in1=2
                        do jn1=1,in1
                            km1=indbasa(jn1,ie1,lm1,isort)
                            lf1=lf_isz(km1,isort)
                            do ie2=1,nm2
                                in2=1
                                if(augm(ie2,l2,isort)/='LOC') in2=2
                                do jn2=1,in2
                                    km2=indbasa(jn2,ie2,lm2,isort)
                                    lf2=lf_isz(km2,isort)
                                    ff(km1,km2,iatom)=&
                                        ff(km1,km2,iatom)+c2&
                                        *fifi_j(lf1,lf2,l_i,isort)
                                enddo
                            enddo         ! over ie2
                        enddo
                    enddo
                enddo               ! over icg
            enddo                 ! over lm2
        enddo                   ! over lm1
    enddo                     !!! over iatom
    deallocate(ylm)
end subroutine


! adler: znew, anew are for the 1st kpoint, znew1 anew1 are for the 2nd kpoint
subroutine integral_band_pw_band_wan(w90, ff, znew, anew, znew1, anew1, b_pw_b,k,kq,kq_shift, num_bands)
    use atom_mod
    use manager_mod
    use parallel_mod
    use solid_mod
    use units_mod
    use wannier_internal
    implicit none

    integer, intent(in) :: k,kq,kq_shift(3), num_bands
    type(Wannier90Workspace),intent(inout),pointer :: w90
    complex*16, intent(in) :: &
        znew(nfun,num_bands),&
        anew(nbasmpw,num_bands),&
        znew1(nfun,num_bands),&
        anew1(nbasmpw,num_bands),&
        ff(maxel,maxel,natom)
    complex*16, intent(inout) :: b_pw_b(num_bands,num_bands)
    integer :: j,iatom,isort,k0,kq0,ndimb,ind0,&
        jbas,jnd,ia,ib,ic,ibas,ind,ia1,&
        ib1,ic1,ibas1,ibas0,irl
    complex*16, allocatable :: s(:,:),tmp(:,:),tmp1(:,:)
    integer :: ispin

    ispin = 1

    !print *, "in integral_band_pw_band_wan", nbndtm, w90%num_bands, maxel
    allocate(s(nbndtm,nbndtm))
    allocate(tmp(nbndtm,w90%num_bands))
    allocate(tmp1(maxel,w90%num_bands))
    s=0.0d0

    b_pw_b=(0.d0,0.d0)

    k0=i_kref(k)
    kq0=i_kref(kq)
    !     ----------------- MT contribution --------------------------------
    do iatom=1,natom
        isort=is(iatom)
        ndimb=lfunm(isort)
        ind0=io_lem(iatom)
        tmp1=0.0d0

        !print *, iatom, znew1(ind0:ndimb,1:num_bands)
        !           row x column
        ! ff is   ndimb x ndimb
        ! z is    ndimb x num_bands 
        ! tmp1 is ndimb x num_bands
        call zgemm('n','n',ndimb,num_bands,ndimb, &
         (1.d0,0.d0),ff(1,1,iatom),maxel, &
         znew1(ind0,1),nfun,(0.d0,0.d0),tmp1,maxel)
        call zgemm('c','n',num_bands,num_bands,ndimb, &
         (1.d0,0.d0),znew(ind0,1),nfun, &
         tmp1,maxel,(1.d0,0.d0),b_pw_b, &
         num_bands)
    enddo                     !! over iatom
    !     ----------------- Interstitial contribution ----------------------
    do jbas=1,nbask(k0)/nrel
        jnd=indgb(jbas,k)
        ia=kq_shift(1)+igbs(1,jnd)
        ib=kq_shift(2)+igbs(2,jnd)
        ic=kq_shift(3)+igbs(3,jnd)
        do ibas=1,nbask(kq0)/nrel
            ind=indgb(ibas,kq)
            ia1=-ia+igbs(1,ind)
            ib1=-ib+igbs(2,ind)
            ic1=-ic+igbs(3,ind)
            j=indplw(ia1,ib1,ic1)
            if(complex_ro) then
                s(jbas,ibas)=dcmplx(sovr(j),sovi(j))  !adler: sovi is the plane wave overlap (flattened)
            else
                s(jbas,ibas)=dcmplx(sovr(j),0.d0)
            endif
        enddo
    enddo
    do irl=1,nrel
        ibas1=nbask(kq0)/nrel*(irl-1)
        ibas0=nbask(k0)/nrel*(irl-1)
        tmp=0.0d0

        call zgemm('n','n',nbask(k0)/nrel,num_bands,nbask(kq0)/nrel, &
         (1.d0,0.d0),s,nbndtm,anew1(ibas1+1,1),nbasmpw, &
         (0.d0,0.d0),tmp,nbndtm)
        call zgemm('c','n',num_bands,num_bands, &
         nbask(k0)/nrel,(1.d0,0.d0),anew(ibas0+1,1),nbasmpw, &
         tmp,nbndtm,(1.d0,0.d0),b_pw_b, &
         num_bands)
    enddo
    deallocate(s,tmp,tmp1)
end subroutine

subroutine sym_a_2(ev_new,k,ev_bnd0,neig,ig,k0)
      
    ! k0 here is always from NPNT set (not npnt)      
    !use comwann_mod
    use atom_mod
    use manager_mod
    use solid_mod
    use units_mod
    implicit none
    integer, intent(in) :: k,neig,ig,k0
    complex*16, intent(in) :: ev_bnd0(nbasmpw,neig)
    complex*16, intent(out) :: ev_new(nbasmpw,neig)
    integer :: j,ibnd,j0,j1,gbs_number,i1,i2,ii,i0
    double precision :: v(3),pi2,phase,gtild(3)
    complex*16 :: cc, ev_bnd1(nbasmpw,neig)
    if(k==k0) then
    ev_new=ev_bnd0
    return
    else
    ev_new=0.0d0
    endif
    pi2=pi+pi
    call rotate(pnt(1,k0),pnt(2,k0),pnt(3,k0),v(1),v(2),v(3), u(2,ig),2)
    gtild(:)=pnt(:,k)-v(:)
    !c     -------- Symmetrization into another q - point ----------------------

    if (irel .le. 1) then
    do j=1,nbask(k0)
        j0=indgb(j,k)         !! G in GBS-list
        v(:)=gbs(:,j0)+gtild(:)     !! G+G_A
        j1=gbs_number(v)      !! G+G_A in GBS-list
        j1=gbs_sym(j1,ig)     !! A^(-1)*(G+G_A) in GBS-list
        j1=iplf_bk(j1,k0)     !! A^(-1)*(G+G_A) in indgb list
        v(:)=pnt(:,k)+gbs(:,j0)  !! k+G
        phase=-pi2*dot_product(v,shift(:,ig))
        cc=dcmplx(cos(phase),sin(phase))
        do ibnd=1,neig
        ev_new(j,ibnd)=cc*ev_bnd0(j1,ibnd)
        enddo
    enddo                   !! over j
    else
    ev_bnd1=0.0d0
    do ii=1, nrel
        i0=(nbask(k0)/nrel)*(ii-1)
        do j=1,nbask(k0)/nrel
        j0=indgb(j,k)       !! G in GBS-list
        v(:)=gbs(:,j0)+gtild(:)   !! G+G_A
        j1=gbs_number(v)    !! G+G_A in GBS-list
        j1=gbs_sym(j1,ig)   !! A^(-1)*(G+G_A) in GBS-list
        j1=iplf_bk(j1,k0)
        v(:)=pnt(:,k)+gbs(:,j0) !! k+G
        phase=-pi2*dot_product(v,shift(:,ig))
        cc=dcmplx(cos(phase),sin(phase))
        do ibnd=1,neig
            ev_bnd1(j+i0,ibnd)=cc*ev_bnd0(j1+i0,ibnd)
        enddo
        enddo                 !! over j        
    enddo
    do j=1,nbask(k0)/nrel        
        i1=j
        i2=nbask(k0)/nrel+j
        do ibnd=1,neig
        ev_new(i1,ibnd)=conjg(uj(1,ig))*ev_bnd1(i1,ibnd) + conjg(uj(3,ig))*ev_bnd1(i2,ibnd)
        ev_new(i2,ibnd)=conjg(uj(2,ig))*ev_bnd1(i1,ibnd) + conjg(uj(4,ig))*ev_bnd1(i2,ibnd)
        enddo
    enddo                
    endif
 end subroutine
    
 subroutine sym_z_0(znew,k,z_bnd0,neig,ig,pt)
!    use comwann_mod
    use atom_mod
    use manager_mod
    use solid_mod
    use units_mod
    implicit none
    integer, intent(in) :: k,neig,ig
    double precision, intent(in) :: pt(3)
    complex*16, intent(in) :: z_bnd0(nfun,neig)
    complex*16,intent(out) :: znew(nfun,neig)
    integer :: ibnd,iatom,jatom,isort,ind0,jnd0,l,ie,m,k0, lm,km,lm0,li,ist,jj,ii,mj,ind1,in,jn
    double precision :: pi2,fas,v(3)
    complex*16 :: cf
    double precision, allocatable :: y0(:),y1(:),t0(:)
    complex*16, allocatable :: t0j(:),y0j(:)

    v=0.0d0

    k0=i_kref(k)
    if(k==k0) then
      znew=z_bnd0
      return
    endif
    if(irel<=1) allocate(y0(2*maxb+1),y1(2*maxb+1),t0(2*maxb+1))
    if(irel==2) allocate(t0j(2*maxb+2),y0j(2*maxb+2))
    pi2=pi+pi
    do ibnd=1,neig
      do iatom=1,natom
        jatom=ip(iatom,ig)
        isort=is(iatom)
        ind0=io_lem(iatom)-1  !! zero index for atom iatom
        jnd0=io_lem(jatom)-1
        if(irel<=1) then
          do l=0,lmb(isort)
            do ie=1,ntle(l,isort)
              in=1
              if(augm(ie,l,isort)/='LOC') in=2
              do jn=1,in
!     ------------- Real components ----------------------------------
                do m=-l,l
                  lm=l*(l+1)+m+1
                  km=indbasa(jn,ie,lm,isort)
                  lm0=l+m+1
                  t0(lm0)=dreal(z_bnd0(jnd0+km,ibnd))
                enddo
                call rotate1(t0,y0,l,u(1,ig),1)
!     ------------- Imaginary components ----------------------------------
                do m=-l,l
                  lm=l*(l+1)+m+1
                  km=indbasa(jn,ie,lm,isort)
                  lm0=l+m+1
                  t0(lm0)=dimag(z_bnd0(jnd0+km,ibnd))
                enddo
                call rotate1(t0,y1,l,u(1,ig),1)
                do m=-l,l
                  lm=l*(l+1)+m+1
                  km=indbasa(jn,ie,lm,isort)
                  lm0=l+m+1
                  znew(ind0+km,ibnd)=dcmplx(y0(lm0),y1(lm0))
                enddo
              enddo
            enddo             !! over ie
          enddo               !! over l
        else if(irel.eq.2) then
          li=0
          do l=0,lmb(isort)
            ist=-1
            if(l.eq.0) ist=1
            do ii=ist,1,2
              li=li+1
              jj=l+l+ii
              do ie=1,ntle(l,isort)
                in=1
                if(augm(ie,l,isort)/='LOC') in=2
                do jn=1,in
                  lm0=0
                  do mj=-jj,jj,2
                    lm0=lm0+1
                    call getlimj(lm,l,ii,mj,li,1)
                    km=indbasa(jn,ie,lm,isort)
                    t0j(lm0)=z_bnd0(jnd0+km,ibnd)
                  enddo
                  call rotate2(t0j,y0j,l,ii,uj(1,ig),1,1)
                  do mj=-jj,jj,2
                    call getlimj(lm,l,ii,mj,li,1)
                    km=indbasa(jn,ie,lm,isort)
                    lm0=(jj+mj)/2+1
                    znew(ind0+km,ibnd)=y0j(lm0)
                  enddo
                enddo
              enddo
            enddo
          enddo
        endif
 !    ---------------  VII-30-21 -----------------------------------------
        !if(iatom<natom) ind1=io_lem(iatom+1)-1 !! last index for iatom
        !if(iatom==natom) ind1=nfun !! last index for atom iatom
        !v(:)=tshift(:,iatom,ig)
        !fas=pi2*dot_product(pt,v)
        !cf=dcmplx(cos(fas),sin(fas))
        !znew(ind0+1:ind1,ibnd)=cf*znew(ind0+1:ind1,ibnd)
      enddo                   !! over iatom
    enddo                     !! over ibnd
    if(irel<=1) deallocate(y0,y1,t0)
    if(irel==2) deallocate(t0j,y0j)
 end subroutine

 SUBROUTINE mpi_allreduce_dcmplx(X,LENX,OP,comm)
    use mpi
    use parallel_mod
    implicit none

    integer,intent(in) :: lenx,op,comm
    complex*16, intent(inout) :: x(lenx)
    integer :: mxbuff
    complex*16,allocatable :: buff(:)
    integer :: i,npass,length,locx,ierror


    mxbuff=2048
    allocate(buff(mxbuff))
    NPASS = (LENX-1)/MXBUFF + 1
    LENGTH = MXBUFF
    LOCX = 1
    DO I=1,NPASS
      IF(I.EQ.NPASS) LENGTH = LENX - MXBUFF*(NPASS-1)
      CALL MPI_ALLREDUCE(X(LOCX),BUFF,LENGTH,MPI_double_complex,OP,comm,IERROR)
      if (ierror .ne. MPI_SUCCESS) then
        call ending
      endif
      CALL ZCOPY(LENGTH,BUFF,1,X(LOCX),1)
      LOCX = LOCX + MXBUFF
    enddo
    deallocate(buff)
    END

end module
