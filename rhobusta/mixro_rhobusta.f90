module RhoMixing

    implicit none

    real*8  :: charge_convergence = 0.0
    real*8  :: mag_convergence = 0.0

contains

    ! this is a copy of mixro1 in order to be able to save the convergence numbers
    subroutine mixro1_RHOBUSTA
        !*****************************************
        !*   admixture in the main scf loop      *
        !*****************************************
        use atom_mod
        use etot_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        use units_mod
        use FlapwMBPT, only: ElDensity
        use flapw_image, only: core_rho_written

        implicit none

        type(ElDensity), allocatable :: oldrho

        integer :: isort,lm,isym,mt,mt1,irad,mti,mti1,i,l,m,istar
        real*8 :: dz,dm,const,delrho,roout,roinp,ronew,spout,spinp,spnew, con,dqdall,ad
        complex*16 :: rooutc,roinpc,ronewc,spoutc,spinpc,spnewc
        real*8, allocatable :: work1(:)
        logical :: density_exists

        ! TODO: if it does not exist, just read it from the image file
        if (.not. core_rho_written) return

        allocate(oldrho)
        call oldrho%load('./el-density.core.h5:/')
        ad=admix
        if(ubi/='dft') call set_adm(ad)
        allocate(work1(0:maxnrad))

        dz=0.d0
        do isort=1,nsort
            isym=1
            ! define charge convergency between iterations
            mt=indmt(isym,isort,1)
            mt1=indmt(isym,isort,nspin)
            const=dfloat(nspin)/8.d0/pi
            do irad=0,nrad(isort)
                delrho=ro(mt+irad)+ro(mt1+irad)-oldrho%ro_(mt+irad)-oldrho%ro_(mt1+irad)
                work1(irad)=weight(irad,isort)*delrho*delrho
            enddo
            dz=dz+const*dqdall(h(isort),work1(0),nrad(isort))
        enddo   !!! over isort
        charge_convergence = dz
        if(maswrk) then
            write(iun,1010)dz
            print 1010,dz
        endif
        dm=0.d0
        do isort=1,nsort
            do lm=1,(lmpb(isort)+1)**2
                if(sym(lm,isort))then
                    isym=lmsym(lm,isort)
                    ! define charge convergency between iterations
                    mt=indmt(isym,isort,1)
                    mt1=indmt(isym,isort,nspin)
                    const=dfloat(nspin)/8.d0/pi
                    const=1.d0/4.d0/pi
                    do irad=0,nrad(isort)
                        delrho=(ro(mt1+irad)-ro(mt+irad))-(oldrho%ro_(mt1+irad)-oldrho%ro_(mt+irad))
                        work1(irad)=weight(irad,isort)*delrho*delrho
                    enddo
                    dm=dm+const*dqdall(h(isort),work1(0),nrad(isort))
                    ! admix new charge density with the old one
                    do irad=0,nrad(isort)
                        mti=mt+irad
                        mti1=mt1+irad
                        roout=(ro(mti)+ro(mti1))*dfloat(nspin)/2.d0
                        roinp=(oldrho%ro_(mti)+oldrho%ro_(mti1))*dfloat(nspin)/2.d0
                        ronew=ad*roout+(1.d0-ad)*roinp
                        if(nspin.eq.1) then
                            ro(mti)=ronew
                        else
                            spout=ro(mti1)-ro(mti)
                            spinp=oldrho%ro_(mti1)-oldrho%ro_(mti)
                            spnew=adspin*spout+(1.d0-adspin)*spinp
                            ro(mti1)=0.5d0*(ronew+spnew)
                            ro(mti)=0.5d0*(ronew-spnew)
                        endif
                    enddo   !!! over irad
                endif
            enddo   !!! over lm
            if(magn.eq.2) then
                do i=1,3
                    do l=0,lmpb(isort)
                        do m=-l,l
                            lm=l*(l+1)+m+1
                            if(symb(lm,i,isort))then
                                isym=lmsymb(lm,i,isort)
                                mt=indmtb(isym,isort)
                                if(l.eq.0) then
                                    do irad=0,nrad(isort)
                                        delrho=oldrho%spmt_(mt+irad)-spmt(mt+irad)
                                        work1(irad)=weight(irad,isort)*delrho*delrho
                                    enddo
                                    dm=dm+dqdall(h(isort),work1(0),nrad(isort))/4.d0/pi
                                endif
                                do irad=0,nrad(isort)
                                    spmt(mt+irad)=adspin*spmt(mt+irad)+(1.d0-adspin)*oldrho%spmt_(mt+irad)
                                enddo
                            endif
                        enddo    !!! over m
                    enddo    !!! over l
                enddo    !!! over i
            endif  !!! only for magn = 2
        enddo   !!! over isort
        con=dfloat(nspin)/2.d0
        do istar=1,nplwro
            rooutc=(rointr(istar,1)+rointr(istar,nspin))*con
            roinpc=(oldrho%rointr_(istar,1)+oldrho%rointr_(istar,nspin))*con
            ronewc=ad*rooutc+(1.d0-ad)*roinpc
            if(nspin.eq.1) then
                rointr(istar,1)=ronewc
            else
                spoutc=-rointr(istar,1)+rointr(istar,nspin)
                spinpc=-oldrho%rointr_(istar,1)+oldrho%rointr_(istar,nspin)
                spnewc=adspin*spoutc+(1.d0-adspin)*spinpc
                rointr(istar,1)=0.5d0*(ronewc+spnewc)
                rointr(istar,2)=0.5d0*(ronewc-spnewc)
            endif
        enddo
        if(magn.eq.2) then
            do i=1,3
                do istar=1,nplwro
                    spintr(i,istar)=spintr(i,istar)*adspin+(1.d0-adspin)*oldrho%spintr_(i,istar)
                enddo
            enddo
        endif  !!! only for magn = 2
        mag_convergence = dm
        if(maswrk) then
            if(magn.eq.2.or.nspin.eq.2)write(iun,1020)dm
            if(magn.eq.2.or.nspin.eq.2)print 1020,dm
        endif
        deallocate(work1, oldrho)

        1010  format(1x,'charge density self-consistency=',g14.7)
        1020  format(1x,'magnetization  self-consistency=',g14.7)
    end

end module
