module fermisurfer_driver
implicit none

    public::WriteFRMSFImp

contains

    function WriteFRMSFImp(selfpath) result(res)
        use stringifor
        use fermi_surface, only: LowEnergyHamiltonian
        use solid_mod
        use manager_mod
        use units_mod, only:pi, rydberg

        type(string), intent(inout) :: selfpath
        type(string) :: res

        type(LowEnergyHamiltonian), allocatable :: FE

        real(4) :: bvec1(3), bvec2(3), bvec3(3)
        real*8 :: qq(3)
        ! Reciprocal lattice vector
        integer :: nk1, nk2, nk3, iorb
        ! k-grid of each direction
        integer :: k_grid_type
        integer :: nbnd


        ! k-dependent quantity
        integer :: ik1, ik2, ik3, ibnd, fo = 10, k0

        allocate(FE)
        call FE%load('./fs.core.h5:/')

        nbnd = FE%max_band - FE%min_band + 1 ! The number of bands
        bvec1(:)=gbas(:,1)
        bvec2(:)=gbas(:,2)
        bvec3(:)=gbas(:,3)

        nk1 = ndiv(1)
        nk2 = ndiv(2)
        nk3 = ndiv(3)
        k_grid_type = 1


        ! this means that we are changing a global variable, and there should not
        ! be self-consistency loop iterations in the same process after this.


        open(fo, file = FE%name%chars() // ".frmsf")
        write(fo,*) nk1, nk2, nk3
        write(fo,*) k_grid_type
        write(fo,*) nbnd
        write(fo,*) bvec1(1:3)
        write(fo,*) bvec2(1:3)
        write(fo,*) bvec3(1:3)
        do ibnd = 1, nbnd
            do ik1 = 1, nk1
                do ik2 = 1, nk2
                    do ik3 = 1, nk3
                        qq = (ik3-1)*qb0(:,3) + (ik2-1)*qb0(:,2) + (ik1-1)*qb0(:,1)
                        call zone1_number(qq,rb0,ndiv,k0)
                        k0=index_k1(k0)
                        k0 = i_kref(k0)

                        !print *, k0
                        write(fo,*)   FE%eps_(FE%min_band+ibnd-1,k0,1)
                    end do
                end do
            end do
        end do

        do ibnd = 1, nbnd
            do ik1 = 1, nk1
                do ik2 = 1, nk2
                    do ik3 = 1, nk3
                        qq = (ik3-1)*qb0(:,3) + (ik2-1)*qb0(:,2) + (ik1-1)*qb0(:,1)
                        call zone1_number(qq,rb0,ndiv,k0)
                        k0=index_k1(k0)
                        k0 = i_kref(k0)

                        write(fo,*)  FE%scalar_(FE%min_band+ibnd-1,k0,1)
                    end do
                end do
            end do
        end do
        
        close(fo)

        deallocate(FE)

        res = ""

    end function

end module
