
module flapw_groups


    implicit none

    private

    public :: InitGroup, CalculateDjFromDl,bld_sh_trans

    interface
        subroutine FortranCBForPreparingDJ(num_ops, lmax) bind(C)
            use iso_c_binding
            integer(kind=c_int32_t), intent(in), value :: num_ops
            integer(kind=c_int32_t), intent(in), value :: lmax
        end subroutine
    end interface

    procedure(FortranCBForPreparingDJ), pointer :: PrepareDJ => null()

contains

	!! unused - delete after testing
    subroutine register_prepare_dj_cb(cb) bind(C,name="flapwgroups_mp_register_prepare_dj_cb_")
        use iso_c_binding
        type(c_funptr),intent(in),value        :: cb
        call c_f_procpointer(cb, PrepareDJ)
    end subroutine

	!! unused - delete after testing
    subroutine CalculateDjFromDl(lmm, rots, shifts, u_a, uj_a, isTheSpaceGroup)
        use penf_stringify
        use stringifor
        use symmetry, only:Bootstrap
        use manager_mod
        use parallel_mod
        use solid_mod, only: ngroup, gbas
        integer, intent(in) :: lmm
        real*8, intent(in)        :: rots(:,:,:), shifts(:,:)
        real*8, intent(inout)        :: u_a(:,:)
        complex*16, intent(inout) :: uj_a(:,:)
        logical, intent(in) :: isTheSpaceGroup

        type(string) :: istr


        type(Bootstrap), allocatable :: bs

        if (.not.associated(PrepareDJ)) then
            print *, "- skipping wigner matices for J basis"
            print *, "  this should not happen in relativistic runs"
            return
        end if

        allocate(bs)

        bs%num_ops = size(rots, 3) !size(u_a,2)
        bs%max_l = lmm
        bs%maxIndexDl = size(u_a,1)
        bs%maxIndexDj = size(uj_a,1)
        call bs%allocate()
        bs%rots_(:,:,:bs%num_ops) = rots(:,:,:bs%num_ops)
        bs%shifts_(:,:bs%num_ops)  = shifts(:,:bs%num_ops)
        bs%Dl_(:,1:bs%num_ops) = u_a(:,1:bs%num_ops)

        istr = trim(str(bs%num_ops,no_sign=.True.)) // "_" // trim(str(lmm,no_sign=.True.))

        call bs%store("./bootstrap.core.h5:/D"//istr,flush_opt=.True.)

        call PrepareDJ(bs%num_ops, lmm)

        call bs%load("./bootstrap.core.h5:/D"//istr)

        uj_a(:,1:bs%num_ops) = bs%Dj_(:,1:bs%num_ops)

        ! if it is the space group, make it available on this address
        if (isTheSpaceGroup) then
            call bs%store("./spacegroup.core.h5:/")
        end if

        deallocate(bs)

    end subroutine

    !
    ! adler:
    ! This program had to be adapted from FLAPWMBPT because we have to construct the group from the
    ! PyMatGen description, which Kutepov stopped doing after the CPC
    ! Since it initializes additional global variables, I had to copy the whole thing and adapt it.
    !
    ! it is adapter from the code called "group" and should be maintained if that code is changed.
    !
    subroutine InitGroup(lmm,jmm,maxw2,sgg)
        !***************************************************************
        !     Program for preparation of transformation matrices       *
        !     for spherical harmonics according to the point group.    *
        !     To find spherical harmonic which after applying the      *
        !     declared group operation will reduce to y(l) multiply    *
        !     vector y(m) to matrice u(m,m1).                          *
        !      input:                                                  *
        !        rbas - translational vectors                          *
        !        lmm - lmax i.e 0:s,1:p,2:d,3:f,etc                    *
        !        ngroup - number of group elements founded             *
        !        u - wigner"s matrices                                 *
        !        shift - shifts if group is non-symmorphic one         *
        !        inv = 0 no  inversion                                 *
        !        inv = 1 yes inversion, ifia=1                         *
        !        inv = 2 yes inversion, ifia=-1                        *
        !     note that:                                               *
        !            u    (l,g) = u(m'm,l,g) = u(iwig,g)               *
        !             m'm                                              *
        !     where iwig=l*(2*l-1)*(2*l+1)/3+(2*l+1)*(l+m)+l+m'+1      *
        !***************************************************************
        use manager_mod
        use parallel_mod
        use solid_mod
        use atom_mod
        use FlapwMBPT, only: SpacegroupGens
        use mpi
        implicit none

        integer, intent(in) :: lmm,jmm,maxw2
        type(SpacegroupGens),intent(inout) :: sgg
        integer :: ierror

        integer :: nt,ngen,ir,l,m,m1,iwig,ig,jatom,iatom,ia,ib,ic,ii,jj, single_ngroup
        real*8 :: x(3),x1(3),t(3),sd(3),dshift
        real*8, allocatable :: fac(:),su1(:,:)
        complex*16, allocatable :: su1_j(:,:)
        inv=0
        ifia = 1
        ! allocate temporary data
        allocate(fac(0:8*maxw2))
        call facc(fac,8*maxw2)
        allocate(su1(-lmm:lmm,-lmm:lmm))
        allocate(su1_j(-jmm:jmm,-jmm:jmm))

        if(maswrk) print *, "- preparing full symmetry group "
        call PrepareGroup(sgg, lmm, jmm)
        deallocate(fac, su1, su1_j)

        symmorph=.true.

        do ir=1,ngroup
            call zone0_cart(shift(1,ir),rbas,shift(1,ir))
            dshift=shift(1,ir)**2+shift(2,ir)**2+shift(3,ir)**2
            if(dshift.gt.1.d-5) symmorph=.false.   ! adler: this is wronge, luckily it is not used in the main process
            dshift=0.d0

            ! adler: I think this checks if this is a representation of inversion (=(-1)^l)
            do l=0,lmm
                do m=-l,l
                    do m1=-l,l
                        iwig=l*(2*l-1)*(2*l+1)/3+(2*l+1)*(l+m1)+l+m+1
                        if(m1 == m) then
                            dshift=dshift+abs(u(iwig,ir)-(-1.d0)**l)
                        else
                            dshift=dshift+abs(u(iwig,ir))
                        endif
                    enddo
                enddo
            enddo
            ! inversion
            if(dshift.lt.1.d-5 .and. inv /= 1) then
                inv = 1
                inv_num=ir
            endif
        enddo

        if (irel.eq.2) then
            if(maswrk) call CalculateDjFromDl(lmm, sgg%rots_, shift, u(:,:), uj(:,:), .True.)
            if (goparr) call brdcst(msgdbl,uj,size(uj)*16,master,MPI_COMM_WORLD)

            ! now double the group by multiplying by -1 on the second part's SU(2) transformations => as well as
            ! all the (single particle) D representations.
            ! note that the corresponding O(3) transformation in the 2nd part are all the same
            single_ngroup = ngroup
            ngroup = single_ngroup * 2
            u(:, single_ngroup+1:ngroup) = u(:,1:single_ngroup)
            uj(:, single_ngroup+1:ngroup) = -uj(:,1:single_ngroup)
            shift(:,single_ngroup+1:ngroup) = shift(:,1:single_ngroup)


        else
            if (maswrk) print *, "- skipping relativistic symmetries for a non-relativistic run"
        endif

        do ig=1,ngroup

            jat:do jatom=1,natom
                call rotate(tau(1,jatom),tau(2,jatom),tau(3,jatom),x(1),x(2),x(3),u(2,ig),2)  !!! = A*t^(-1)_a
                x=x+shift(:,ig)
                !print *, "shift=", shift(:,ig)
                do iatom=1,natom
                    x1=tau(:,iatom)-x
                    do ia=-3,3
                        do ib=-3,3
                            do ic=-3,3
                                t(:)=ia*rbas(:,1)+ib*rbas(:,2)+ic*rbas(:,3)
                                sd=t-x1
                                if(abs(sd(1))+abs(sd(2))+abs(sd(3))<1.0d-4) then
                                    ip(iatom,ig)=jatom
                                    cycle jat
                                endif
                            enddo
                        enddo
                    enddo
                enddo
                if(maswrk) then
                    write(iun,*)'Could not find jatom for ig =',ig,' iatom =',jatom
                    write(iun,*)tau(1,jatom),' => ',x(1)
                    write(iun,*)tau(2,jatom),' => ',x(2)
                    write(iun,*)tau(3,jatom),' => ',x(3)
                endif
                call ending
            enddo jat
        end do

        if(irel==2.and.magn==2) call check_b

        ! ----------------------------------------------------------------------
        ! inv is used in interstitial calculations
        invers=.false.
        if(inv.eq.1) invers=.true.
        if(maswrk) then
          write(iun,*)'Number of elements in symmetry group is ',ngroup
          if(inv.eq.1) write(iun,*)'Inversional operation is presented'
          if(inv.eq.2) write(iun,*)'Inversional operation is not presented'
          if(symmorph) write(iun,*)'The group is symmorphic'
          if(.not.symmorph) write(iun,*)'The group is not symmorphic'
        endif
        real_things=.false.
        complex_ro=.true.
        if(invers) then
          if(maxval(abs(shift(:,inv_num)))<1.d-6) complex_ro=.false.
        endif

        ! ------ Multiplication table -----------------------------------------
        call group_table

        call timel('**** GROUP finished ****************')
    end


    ! adapted from parsbl.F to accept sgg instead of parsing the string
    subroutine PrepareGroup(sgg,lmax,jmax)
        use manager_mod
        use parallel_mod
        use solid_mod
        use FlapwMBPT
        implicit none
        type(SpacegroupGens),intent(inout) :: sgg
        integer, intent(in) :: lmax, jmax
        integer :: iop
        real*8 ::  r(3,3), s(3,3)

        ngroup=sgg%num_generators ! number of generators
        do iop = 1, ngroup
            u(:,iop) = 0.d0
            uj(:,iop) = 0.d0

            u(1,iop) = 1.d0

            ! we store the cartesian version in a global variable
            u_opt(:,:,iop) = sgg%rots_(:,:,iop)
            s = u_opt(:,:,iop) !shortcut

            r(1,:) = [s(2,2), s(2,3), s(2,1)]
            r(2,:)=  [s(3,2), s(3,3), s(3,1)]
            r(3,:) = [s(1,2), s(1,3), s(1,1)]


            !r(:,:) = unpack(u(2:10,iop), mask, 0.0)

            !print *, "operation", iop, "Cartesian:"
            !print *, u_opt(:,:,iop)

            !print *, "wigner:"
            !print *, r(:,:)

            call bld_sh_trans(r, u(1,iop),lmax)
            ! adler: given u1, lmm (maximum l) builds:
            !  u(1,iop) - {R(l=0),R(l=1),...,R(l=lmax)}, rotation matrices in spherical harmonics
            !  uj(1,iop) - same format, but relativistic transformations which are complex

			!add the translation (which ahs to be cartesian coordinates)
			shift(:,iop) = sgg%gens(iop)%t_(:)
            
			! set the sign to 1, we account for the sign in the representation
            ! Kutepov's code used this as a representation of multiplication by inversion, but it is 
            ! inconsitstent about it
			ifia(iop)= 1
        enddo
    end

    subroutine PrepareGenRotRef(gen, dgen, djgen,lmax,jmax,su1,su1_j,fac)
        use manager_mod
        use parallel_mod
        use solid_mod
        use units_mod
        use FlapwMBPT
        implicit none
        integer :: k,l,m,m1,ist,ii,iwig0,jj,im,im1,ik
        integer*8 :: nrot, sgn, iwig
        integer, intent(in):: lmax,jmax
		type(Generator), intent(inout) :: gen
        real*8,intent(out) :: su1(-lmax:lmax,-lmax:lmax), dgen(:),fac(:)
        complex*16,intent(out) ::su1_j(jmax+1,jmax+1),djgen(:)
        real*8 :: v(3),sp,v12,teta,fi,om,ddot,su,ss,wign
        complex*16 :: wign_j,cu,suj
        sgn = gen%sign
        if (gen%kind == 'R') then
            nrot = gen%n
            v = gen%v_
            sp=ddot(3,v,1,v,1)
            sp=1.d0/dsqrt(sp)
            do k=1,3
                v(k)=v(k)*sp
            enddo
            v12=v(1)**2+v(2)**2
            if(v12 > 1.d-4) then
                teta=acos(v(3))
                fi=acos(v(1)/sqrt(v12))
                if(v(2)<0.d0) fi=-fi
            else
                teta=0.d0
                fi=0.d0
            endif
            om=2.d0*pi/nrot
            do l=0,lmax
                sgn = sgn * gen%sign
                do m=-l,l
                    do m1=-l,l
                        su1(m,m1)=wign(l,m,m1,fi,teta,0.d0,fac)
                    enddo
                enddo
                do m=-l,l
                    do m1=-l,l
                        iwig=l*(2*l-1)*(2*l+1)/3+(2*l+1)*(l+m1)+l+m+1
                        su=0.d0
                        do k=-l,l
                            ss=k*om
                            su=su+su1(m1,k)*(su1(m,k)*dcos(ss)+su1(m,-k)*dsin(ss))
                        enddo
                        dgen(iwig)=su * sgn
                    enddo
                enddo
            enddo
            if(irel == 2) then
                sgn = gen%sign
                do l=0,lmax
                    sgn = sgn * gen%sign
                    ist=-1
                    if(l == 0) ist=1
                    do ii=ist,1,2
                        iwig0=(8*l**3+12*l**2+10*l+3)/3+l*(2*l+1)*ii
                        jj=l+l+ii
                        do m=-jj,jj,2
                            im=(m+jj)/2+1
                            do m1=-jj,jj,2
                                im1=(m1+jj)/2+1
                                su1_j(im,im1)=wign_j(jj,m,m1,fi,teta,0.d0,fac)
                            enddo
                        enddo
                        do m=-jj,jj,2
                            im=(m+jj)/2+1
                            do m1=-jj,jj,2
                                im1=(m1+jj)/2+1
                                iwig=iwig0+(l+l+ii+1)*(m1+ii)/2+(ii+m)/2
                                suj=(0.d0,0.d0)
                                do k=-jj,jj,2
                                    ik=(k+jj)/2+1
                                    ss=k*om/2.d0
                                    cu=dcmplx(0.d0,ss)
                                    cu=exp(cu)
                                    suj=suj+dconjg(su1_j(im1,ik))*su1_j(im,ik)*cu
                                enddo
                                djgen(iwig)=suj * sgn
                            enddo
                        enddo
                    enddo
                enddo
            endif
        else if(gen%kind == 'M') then
			v = gen%v_
            sp=ddot(3,v,1,v,1)
            sp=1.d0/dsqrt(sp)
            do k=1,3
                v(k)=v(k)*sp
            enddo
            v12=v(1)**2+v(2)**2
            if(v12.gt.1.d-12) then
                teta=acos(v(3))
                fi=acos(v(1)/sqrt(v12))
                if(v(2).lt.0.d0) fi=-fi
            else
                teta=0.d0
                fi=0.d0
            endif
            do l=0,lmax
                sgn = sgn * gen%sign
                do m=-l,l
                    do m1=-l,l
                        su1(m,m1)=wign(l,m,m1,fi,teta,0.d0,fac)
                    enddo
                enddo
                do m=-l,l
                    do m1=-l,l
                        iwig=l*(2*l-1)*(2*l+1)/3+(2*l+1)*(l+m1)+l+m+1
                        su=0.d0
                        do k=-l,l
                            su=su+(-1.d0)**(l+k)*su1(m1,k)*su1(m,k)
                        enddo
                        dgen(iwig)=su * sgn
                    enddo
                enddo
            enddo
            if(irel == 2) then
                sgn = gen%sign
                do l=0,lmax
                    sgn = sgn * gen%sign
                    ist=-1
                    if(l == 0) ist=1
                    do ii=ist,1,2
                        iwig0=(8*l**3+12*l**2+10*l+3)/3+l*(2*l+1)*ii
                        jj=l+l+ii
                        do m=-jj,jj,2
                            im=(m+jj)/2+1
                            do m1=-jj,jj,2
                                im1=(m1+jj)/2+1
                                su1_j(im,im1)=wign_j(jj,m,m1,fi,teta,0.d0,fac)
                            enddo
                        enddo
                        do m=-jj,jj,2
                            im=(m+jj)/2+1
                            do m1=-jj,jj,2
                                im1=(m1+jj)/2+1
                                iwig=iwig0+(l+l+ii+1)*(m1+ii)/2+(ii+m)/2
                                suj=(0.d0,0.d0)
                                do k=-jj,jj,2
                                    ik=(k+jj)/2+1
                                    suj=suj+dconjg(su1_j(im1,ik))*su1_j(im,ik)&
                                        *(-1.d0)**(l+(k+1)/2)*(0.d0,1.d0)
                                enddo
                                djgen(iwig)=suj * sgn
                            enddo
                        enddo
                    enddo
                enddo
            endif
        else if(gen%kind == 'E') then !identity
            do l=0,lmax
                sgn = sgn * gen%sign
                do m=-l,l
                    do m1=-l,l
                        iwig=l*(2*l-1)*(2*l+1)/3+(2*l+1)*(l+m1)+l+m+1
                        if (m == m1)then
                            dgen(iwig)=sgn
                        else
                            dgen(iwig)=0.d0
                        endif
                    enddo
                enddo
            enddo
            if(irel == 2) then
                sgn = gen%sign
                do l=0,lmax
                    sgn = sgn * gen%sign
                    ist=-1
                    if(l == 0) ist=1
                    do ii=ist,1,2
                        iwig0=(8*l**3+12*l**2+10*l+3)/3+l*(2*l+1)*ii
                        jj=l+l+ii
                        do m=-jj,jj,2
                            do m1=-jj,jj,2
                                iwig=iwig0+(l+l+ii+1)*(m1+ii)/2+(ii+m)/2
                                if (m == m1)then
                                    ss=1.d0
                                else
                                    ss=0.d0
                                endif
                                djgen(iwig)=dcmplx(ss,0.d0) * sgn
                            enddo
                        enddo
                    enddo
                enddo
            endif
        endif
    end


 !-----------------------------------------------------------------------
    !
    subroutine bld_sh_trans(r, g,lmax)
        !
        ! The GW code needs to know how spherical harmonic functions transform
        ! under the symmetry operations associated with the spacegroup of the
        ! material. From libraries such a pymatgen the transformation matrices
        ! of the symmetry operations can be obtained easily. However, only the
        ! Ylm with l=1 transform according to them. This routine uses
        ! recursion relations by Ivanic et al. [1,2,3] to express the
        ! transformations for l>1 in terms of the transformations for l-1 and
        ! l=1.
        !
        ! [1] J. Ivanic, K. Ruedenberg, "Rotation matrices for real spherical
        !     harmonics. Direct determination by recursion", J. Phys. Chem. 100
        !     (1996) 6342-6347, doi: 10.1021/jp953350u
        !
        ! [2] J. Ivanic, K. Ruedenberg, "Errata: Rotation matrices for real
        !     spherical harmonics. Direct determination by recursion", J. Phys.
        !     Chem. 102 (1998) 9099-9100, doi: 10.1021/jp9833340
        !
        ! [3] C.H. Choi, J. Ivanic, M.S. Gordon, K. Ruedenberg, "Rapid and
        !     stable determination of rotation matrices between spherical
        !     harmonics by direct recursion", J. Chem. Phys. 111 (1999)
        !     8825-8831, doi: 10.1063/1.480229
        !
        use solid_mod,only:maxwig        ! for maxwig
        implicit none
        integer, intent(in):: lmax         ! Maximum l quantum number
        real*8,  intent(in):: r(-1:1,-1:1) ! The transformation for l=1
        real*8,  intent(out)::g(maxwig)    ! The transformations for
        ! l=0,...,lmax stored as a vector
        ! g={R(l=0),R(l=1),...,R(l=lmax)}
        !complex*16, intent(out)::g_j(maxwig) ! The transformations for
        ! l=0,...,lmax stored as a vector
        ! g_j={R(l=0),R(l=1),...,R(l=lmax)}
        ! These transformations are complex valued
        ! for the relativistic case where the basis
        ! functions are complex.
        !
        integer :: l            ! l quantum number
        integer :: m,m1         ! m and m' quantum numbers
        !
        !
        g(2:10) = pack(r(:,:), .true.)
 
        do l = 1, lmax-1
            !
            !       First do m1 = -l-1
            !
            !       Eq.(7.9c) of [2].
            !
            m1 = l
            do m = l+1, 2, -1
                g(iwig(l+1,-m,-m1-1))&
                    = clmm1(l+1,m,m1+1)*(r(0,-1)*g(iwig(l,-m,m1))&
                    +r(0,1)*g(iwig(l,-m,-m1)))&
                    + 0.5d0*dlmm1(l+1,m,m1+1)*(r(-1,-1)*g(iwig(l,m-1,m1))&
                    +r(-1,1)*g(iwig(l,m-1,-m1))&
                    +r(1,-1)*g(iwig(l,-m+1,m1))&
                    +r(1,1)*g(iwig(l,-m+1,-m1)))&
                    + 0.5d0*dlmm1(l+1,-m,m1+1)*(r(-1,-1)*g(iwig(l,m+1,m1))&
                    +r(-1,1)*g(iwig(l,m+1,-m1))&
                    -r(1,-1)*g(iwig(l,-m-1,m1))&
                    -r(1,1)*g(iwig(l,-m-1,-m1)))
            enddo
            m = 1
            g(iwig(l+1,-m,-m1-1))&
                = clmm1(l+1,m,m1+1)*(r(0,-1)*g(iwig(l,-m,m1))&
                +r(0,1)*g(iwig(l,-m,-m1)))&
                + sqrt(0.5d0)*dlmm1(l+1,m,m1+1)*(r(-1,-1)*g(iwig(l,m-1,m1))&
                +r(-1,1)*g(iwig(l,m-1,-m1)))&
                + 0.5d0*dlmm1(l+1,-m,m1+1)*(r(-1,-1)*g(iwig(l,m+1,m1))&
                +r(-1,1)*g(iwig(l,m+1,-m1))&
                -r(1,-1)*g(iwig(l,-m-1,m1))&
                -r(1,1)*g(iwig(l,-m-1,-m1)))
            !
            !       Eq.(7.9a) of [2].
            !
            g(iwig(l+1,0,-m1-1))&
                = clmm1(l+1,0,m1+1)*(r(0,-1)*g(iwig(l,0,m1))&
                +r(0,1)*g(iwig(l,0,-m1)))&
                - sqrt(0.5d0)*dlmm1(l+1,0,m1+1)*(r(1,-1)*g(iwig(l,1,m1))&
                +r(1,1)*g(iwig(l,1,-m1))&
                +r(-1,-1)*g(iwig(l,-1,m1))&
                +r(-1,1)*g(iwig(l,-1,-m1)))
            !
            !       Eq.(7.9b) of [2].
            !
            m = 1
            g(iwig(l+1,m,-m1-1))&
                = clmm1(l+1,m,m1+1)*(r(0,-1)*g(iwig(l,m,m1))&
                +r(0,1)*g(iwig(l,m,-m1)))&
                + sqrt(0.5d0)*dlmm1(l+1,m,m1+1)*(r(1,-1)*g(iwig(l,m-1,m1))&
                +r(1,1)*g(iwig(l,m-1,-m1)))&
                - 0.5d0*dlmm1(l+1,-m,m1+1)*(r(1,-1)*g(iwig(l,m+1,m1))&
                +r(1,1)*g(iwig(l,m+1,-m1))&
                +r(-1,-1)*g(iwig(l,-m-1,m1))&
                +r(-1,1)*g(iwig(l,-m-1,-m1)))
            do m = 2, l+1
                g(iwig(l+1,m,-m1-1))&
                    = clmm1(l+1,m,m1+1)*(r(0,-1)*g(iwig(l,m,m1))&
                    +r(0,1)*g(iwig(l,m,-m1)))&
                    + 0.5d0*dlmm1(l+1,m,m1+1)*(r(1,-1)*g(iwig(l,m-1,m1))&
                    +r(1,1)*g(iwig(l,m-1,-m1))&
                    -r(-1,-1)*g(iwig(l,-m+1,m1))&
                    -r(-1,1)*g(iwig(l,-m+1,-m1)))&
                    - 0.5d0*dlmm1(l+1,-m,m1+1)*(r(1,-1)*g(iwig(l,m+1,m1))&
                    +r(1,1)*g(iwig(l,m+1,-m1))&
                    +r(-1,-1)*g(iwig(l,-m-1,m1))&
                    +r(-1,1)*g(iwig(l,-m-1,-m1)))
            enddo
            !
            !       Now do m1 = -l, ..., l
            !
            do m1 = -l, l
                !
                !         Eq.(6.7) of [2]. Note that when m=l blmm1(l+1,-m,m1)=0.
                !
                do m = l+1, 2, -1
                    g(iwig(l+1,-m,m1)) &
                        = almm1(l+1,m,m1)*r(0,0)*g(iwig(l,-m,m1))&
                        + 0.5d0*blmm1(l+1,m,m1)*(r(-1,0)*g(iwig(l,m-1,m1))&
                        +r(1,0)*g(iwig(l,-m+1,m1)))&
                        + 0.5d0*blmm1(l+1,-m,m1)*(r(-1,0)*g(iwig(l,m+1,m1))&
                        -r(1,0)*g(iwig(l,-m-1,m1)))
                enddo
                m = 1
                g(iwig(l+1,-m,m1)) &
                    = almm1(l+1,m,m1)*r(0,0)*g(iwig(l,-m,m1))&
                    + sqrt(0.5d0)*blmm1(l+1,m,m1)*r(-1,0)*g(iwig(l,m-1,m1))&
                    + 0.5d0*blmm1(l+1,-m,m1)*(r(-1,0)*g(iwig(l,m+1,m1))&
                    -r(1,0)*g(iwig(l,-m-1,m1)))
                !
                !         Eq.(6.3) of [1]
                !
                g(iwig(l+1,0,m1))&
                    = almm1(l+1,0,m1)*r(0,0)*g(iwig(l,0,m1))&
                    - blmm1(l+1,0,m1)*(r(1,0)*g(iwig(l,1,m1))&
                    +r(-1,0)*g(iwig(l,-1,m1)))/sqrt(2.0d0)
                !
                !         Eq.(6.6) of [1]. Note that when m=l blmm1(l+1,-m,m1)=0.
                !
                m = 1
                g(iwig(l+1,m,m1))&
                    = almm1(l+1,m,m1)*r(0,0)*g(iwig(l,m,m1))&
                    + sqrt(0.5d0)*blmm1(l+1,m,m1)*r(1,0)*g(iwig(l,m-1,m1))&
                    - 0.5d0*blmm1(l+1,-m,m1)*(r(1,0)*g(iwig(l,m+1,m1))&
                    +r(-1,0)*g(iwig(l,-m-1,m1)))
                do m = 2, l+1
                    g(iwig(l+1,m,m1))&
                        = almm1(l+1,m,m1)*r(0,0)*g(iwig(l,m,m1))&
                        + 0.5d0*blmm1(l+1,m,m1)*(r(1,0)*g(iwig(l,m-1,m1))&
                        -r(-1,0)*g(iwig(l,-m+1,m1)))&
                        - 0.5d0*blmm1(l+1,-m,m1)*(r(1,0)*g(iwig(l,m+1,m1))&
                        +r(-1,0)*g(iwig(l,-m-1,m1)))
                enddo
            enddo
            !
            !       Finally do m1 = l+1
            !
            !       Eq.(7.8b) of [1]
            !
            m1 = l
            do m = l+1, 2, -1
                g(iwig(l+1,-m,m1+1))&
                    = clmm1(l+1,m,m1+1)*(r(0,1)*g(iwig(l,-m,m1))&
                    -r(0,-1)*g(iwig(l,-m,-m1)))&
                    + 0.5d0*dlmm1(l+1,m,m1+1)*(r(-1,1)*g(iwig(l,m-1,m1))&
                    -r(-1,-1)*g(iwig(l,m-1,-m1))&
                    +r(1,1)*g(iwig(l,-m+1,m1))&
                    -r(1,-1)*g(iwig(l,-m+1,-m1)))&
                    + 0.5d0*dlmm1(l+1,-m,m1+1)*(r(-1,1)*g(iwig(l,m+1,m1))&
                    -r(-1,-1)*g(iwig(l,m+1,-m1))&
                    -r(1,1)*g(iwig(l,-m-1,m1))&
                    +r(1,-1)*g(iwig(l,-m-1,-m1)))
            enddo
            m = 1
            g(iwig(l+1,-m,m1+1))&
                = clmm1(l+1,m,m1+1)*(r(0,1)*g(iwig(l,-m,m1))&
                -r(0,-1)*g(iwig(l,-m,-m1)))&
                + sqrt(0.5d0)*dlmm1(l+1,m,m1+1)*(r(-1,1)*g(iwig(l,m-1,m1))&
                -r(-1,-1)*g(iwig(l,m-1,-m1)))&
                + 0.5d0*dlmm1(l+1,-m,m1+1)*(r(-1,1)*g(iwig(l,m+1,m1))&
                -r(-1,-1)*g(iwig(l,m+1,-m1))&
                -r(1,1)*g(iwig(l,-m-1,m1))&
                +r(1,-1)*g(iwig(l,-m-1,-m1)))
            !
            !       Eq.(7.5) of [1]
            !
            g(iwig(l+1,0,m1+1))&
                = clmm1(l+1,0,m1+1)*(r(0,1)*g(iwig(l,0,m1))&
                -r(0,-1)*g(iwig(l,0,-m1)))&
                - sqrt(0.5d0)*dlmm1(l+1,0,m1+1)*(r(1,1)*g(iwig(l,1,m1))&
                -r(1,-1)*g(iwig(l,1,-m1))&
                +r(-1,1)*g(iwig(l,-1,m1))&
                -r(-1,-1)*g(iwig(l,-1,-m1)))
            !
            !       Eq.(7.8a) of [1]
            !
            m = 1
            g(iwig(l+1,m,m1+1))&
                = clmm1(l+1,m,m1+1)*(r(0,1)*g(iwig(l,m,m1))&
                -r(0,-1)*g(iwig(l,m,-m1)))&
                + sqrt(0.5d0)*dlmm1(l+1,m,m1+1)*(r(1,1)*g(iwig(l,m-1,m1))&
                -r(1,-1)*g(iwig(l,m-1,-m1)))&
                - 0.5d0*dlmm1(l+1,-m,m1+1)*(r(1,1)*g(iwig(l,m+1,m1))&
                -r(1,-1)*g(iwig(l,m+1,-m1))&
                +r(-1,1)*g(iwig(l,-m-1,m1))&
                -r(-1,-1)*g(iwig(l,-m-1,-m1)))
            do m = 2, l+1
                g(iwig(l+1,m,m1+1))&
                    = clmm1(l+1,m,m1+1)*(r(0,1)*g(iwig(l,m,m1))&
                    -r(0,-1)*g(iwig(l,m,-m1)))&
                    + 0.5d0*dlmm1(l+1,m,m1+1)*(r(1,1)*g(iwig(l,m-1,m1))&
                    -r(1,-1)*g(iwig(l,m-1,-m1))&
                    -r(-1,1)*g(iwig(l,-m+1,m1))&
                    +r(-1,-1)*g(iwig(l,-m+1,-m1)))&
                    - 0.5d0*dlmm1(l+1,-m,m1+1)*(r(1,1)*g(iwig(l,m+1,m1))&
                    -r(1,-1)*g(iwig(l,m+1,-m1))&
                    +r(-1,1)*g(iwig(l,-m-1,m1))&
                    -r(-1,-1)*g(iwig(l,-m-1,-m1)))
            enddo
        enddo
    end
    !
    !-----------------------------------------------------------------------
    !
    integer function iwig(l,m,m1)
        !
        ! The vector g stores the transformation matrices for all spherical
        ! harmonics up to l=lmax. The resulting indexing becomes non-trivial
        ! therefore it seemed reasonable to encapsulate it in a function of
        ! its own. The expression is essentially stolen from subroutine
        ! parsop [parsbl.F].
        !
        implicit none
        integer, intent(in) :: l  ! l quantum number
        integer, intent(in) :: m  ! m quantum number
        integer, intent(in) :: m1 ! m' quantum number
        !
        iwig = 1
        if ((m.gt.l).or.(-m.gt.l).or.(m1.gt.l).or.(-m1.gt.l)) return
        iwig = l*(2*l-1)*(2*l+1)/3+(2*l+1)*(l+m1)+l+m+1
        !
        return
    end
    !lmm1
    !-----------------------------------------------------------------------
    !
    real*8 function almm1(l,m,m1)
        !
        ! The a^l_{mm'} coefficient of Eq. (6.4) of [1].
        !
        ! [1] J. Ivanic, K. Ruedenberg, "Rotation matrices for real spherical
        !     harmonics. Direct determination by recursion", J. Phys. Chem. 100
        !     (1996) 6342-6347, doi: 10.1021/jp953350u
        !
        implicit none
        integer, intent(in) :: l  ! l quantum number
        integer, intent(in) :: m  ! m quantum number
        integer, intent(in) :: m1 ! m' quantum number
        !
        real*8 e ! enumerator
        real*8 d ! denominator
        !
        e = (l+m)*(l-m)
        d = (l+m1)*(l-m1)
        almm1 = sqrt(e/d)
        !
        return
    end
    !
    !-----------------------------------------------------------------------
    !
    real*8 function blmm1(l,m,m1)
        !
        ! The b^l_{mm'} coefficient of Eq. (6.5) of [1].
        !
        ! [1] J. Ivanic, K. Ruedenberg, "Rotation matrices for real spherical
        !     harmonics. Direct determination by recursion", J. Phys. Chem. 100
        !     (1996) 6342-6347, doi: 10.1021/jp953350u
        !
        implicit none
        integer, intent(in) :: l  ! l quantum number
        integer, intent(in) :: m  ! m quantum number
        integer, intent(in) :: m1 ! m' quantum number
        !
        real*8 e ! enumerator
        real*8 d ! denominator
        !
        e = (l+m)*(l+m-1)
        d = (l+m1)*(l-m1)
        blmm1 = sqrt(e/d)
        !
        return
    end
    !
    !-----------------------------------------------------------------------
    !
    real*8 function clmm1(l,m,m1)
        !
        ! The c^l_{mm'} coefficient of Eq. (7.6) of [1].
        !
        ! [1] J. Ivanic, K. Ruedenberg, "Rotation matrices for real spherical
        !     harmonics. Direct determination by recursion", J. Phys. Chem. 100
        !     (1996) 6342-6347, doi: 10.1021/jp953350u
        !
        implicit none
        integer, intent(in) :: l  ! l quantum number
        integer, intent(in) :: m  ! m quantum number
        integer, intent(in) :: m1 ! m' quantum number
        !
        real*8 e ! enumerator
        real*8 d ! denominator
        !
        e = (l+m)*(l-m)
        d = (l+m1)*(l+m1-1)
        clmm1 = sqrt(e/d)
        !
        return
    end
    !
    !-----------------------------------------------------------------------
    !
    real*8 function dlmm1(l,m,m1)
        !
        ! The d^l_{mm'} coefficient of Eq. (7.7) of [1].
        !
        ! [1] J. Ivanic, K. Ruedenberg, "Rotation matrices for real spherical
        !     harmonics. Direct determination by recursion", J. Phys. Chem. 100
        !     (1996) 6342-6347, doi: 10.1021/jp953350u
        !
        implicit none
        integer, intent(in) :: l  ! l quantum number
        integer, intent(in) :: m  ! m quantum number
        integer, intent(in) :: m1 ! m' quantum number
        !
        real*8 e ! enumerator
        real*8 d ! denominator
        !
        e = (l+m)*(l+m-1)
        d = (l+m1)*(l+m1-1)
        dlmm1 = sqrt(e/d)
        !
        return
    end
    !
    !-----------------------------------------------------------------------



end module
