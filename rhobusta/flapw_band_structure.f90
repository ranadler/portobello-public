
module band_structure

    use LAPW

    implicit none

    private

    public :: GetBandStructure, GetKpathBands



    !real*8, allocatable ::  VDC(:,:,:,:)


contains
!
!    subroutine ReCalculateBandPotentials()
!        use solid_mod
!        use atom_mod
!        use manager_mod
!        use parallel_mod
!        use etot_mod
!        integer:: ispin, ind_k, k
!
!        print *, "- re-calculating band-basis potentials, mu=", chem_pot
!
!        if (.not. allocated(VDC)) then
!            allocate(VDC(nbndf, nbndf, ndim3_k(me3_k+1), nspin_0))
!        end if
!
!        pv=0.d0
!	    if(irel==2) pvj=0.d0
!	    call vslli(2,2,v_mt_h,v_mt_xc,v_intr_h,v_intr_xc,0) ! only Hartree +XC
!	    do ispin=1,nspin
!	      do ind_k=1,ndim3_k(me3_k+1)
!	        k=n3_mpi_k(me3_k+1)+ind_k
!	        call v_bloch(VDC(1,1,ind_k,ispin),pnt(1,k),ispin, &
!                n_bnd(k,ispin),nbndf,nbask(k),indgb(1,k),&
!                ev_bnd(1,1,ind_k,ispin),z_bnd(1,1,ind_k,ispin), &
!                pv,pvj,war,wari)
!	      enddo
!	    enddo
!
!    end subroutine


    ! this is written following code in plot_bands_lapw
    subroutine GetKpathBands(res, res_gradient, kp, min_band, max_band, get_gradient)
        use LAPW
        use TransportOptics, only: Velocities
        use velocity_matrix, only: VkMatrixNonrel, VkMatrixRel
        use solid_mod
        use atom_mod
        use manager_mod, only:chem_pot, nspin, nrel, nspin_0, irel
        use parallel_mod
        use units_mod, only: pi
        use mpi

        type(BandStructure),intent(inout) :: res
        type(Velocities),intent(inout)    :: res_gradient
        type(KPath), intent(inout)        :: kp
        integer, intent(in) ::  min_band, max_band
        logical, intent(in) :: get_gradient
        integer :: ik, ispin, nbn, nbndt, idum, mband,ii
	    real*8 :: err_val,err_slo
	    real*8, allocatable :: evn(:)
	    complex*16, allocatable :: zbn(:,:),evbn(:,:),grad(:,:,:)
	    integer, allocatable  :: indgb_kpath(:,:), nbask_kpath(:), n_bnd_kpath(:,:)
        real*8 :: r3(3)
        integer :: ierr, rank, num_workers, shard_size, first_ik, last_ik, num_k, n

	    ! these are not used but inherited from Kutepov
        err_val=0.d0
        err_slo=0.d0

        if (goparr) then
            call MPI_COMM_RANK(MPI_COMM_WORLD, rank,   ierr)
            call MPI_COMM_SIZE(MPI_COMM_WORLD, num_workers,   ierr)
        else
            rank = 0
            num_workers = 1
        end if
        shard_size = int(kp%num_k / num_workers)
        first_ik = rank*shard_size+1
        last_ik = min(kp%num_k,rank*shard_size+shard_size)
        if (rank == num_workers-1) then 
            last_ik = kp%num_k 
        end if
        num_k = last_ik - first_ik + 1

        allocate(evn(nbndf))
        allocate(zbn(nfun,nbndf))
        allocate(evbn(nbasmpw,nbndf))

        allocate(indgb_kpath(nbndtm,num_k))
        allocate(nbask_kpath(num_k))
        indgb_kpath=0
        nbask_kpath=0

        res%num_expanded = max_band - min_band + 1
        res%num_si = nspin
        res%num_k  = num_k
        res%num_muffin_orbs = nfun
        res%max_num_pw = nbasmpw
        call res%allocate()

        res%chemical_potential=chem_pot
        res%is_sharded = num_workers > 1
        res%shard_offset = first_ik-1

        if (get_gradient) then
            res_gradient%num_expanded = max_band - min_band + 1
            res_gradient%num_si = nspin
            res_gradient%num_k  = num_k
            call res_gradient%allocate()
        end if

        ! calculate the path's indgb and nbask arrays, as Kutepov does in
        ! g_inside_cutoff.F, get_indgb.F for his paths
        ! as far as I can tell, this has no side effects at all,
        ! although in Kutepov's code the second call could produce nbndt that potentially
        ! increases the number of band from start. But here we don't care about a few
        ! extra high-energy bands.

        do ik=first_ik,last_ik
            call g_inside_cutoff(nplwbas,kp%k_(1,ik),cutoff,.false.,.false.,&
                                 idum,nbndt,nplwbas)
            nbask_kpath(ik-first_ik+1)=min(nrel*nbndt,nbndtm)
            !nbndtm=max(nbndtm,nbndt)
        enddo
        ! note: nbndtm parameter is (in)
        ! TODO: do this once?
        do ik=first_ik,last_ik
            call g_inside_cutoff(nplwbas,kp%k_(1,ik),cutoff,.true.,.true., &
                                 indgb_kpath(1,ik-first_ik+1),nbndt,nplwbas)
        enddo

        pv=0.d0
        if(irel==2) pvj=0.d0
        call vslli(2,2,v_mt_h,v_mt_xc,v_intr_h,v_intr_xc,0)

        do ispin=1,nspin
            do ik=first_ik, last_ik
              ! this gets all the bands for the k point
              err_val = 0.0
              err_slo = 0.0
              evn = 0.0
              nbn = 0.0
              zbn = 0.0
              evbn = 0.0

              ! This does most of the work
              ! note that this is the same code that gets called in DFT
              ! when creating the bands - it just needs the "current" potential
              ! (and therefore one has to restart DFT), and solves the KS equation
              ! for the given k (here - a kpoint on the path)
              call lda_k_point(ik,ispin,kp%k_(1,ik),err_val,err_slo,&
                               evn(1),nbn,zbn(1,1),evbn(1,1),&
                               indgb_kpath(1,ik-first_ik+1),nbask_kpath(ik-first_ik+1))

              ! we just want the band data between min and max in res
              mband = min(nbn, max_band, nbndf)
              if (mband <= nbndf .and. min_band <= nbndf .and. mband >=min_band) then
                  res%Z_(1:,1:mband-min_band+1,ik-first_ik+1,ispin) = zbn(1:,min_band:mband)
                  res%energy_(1:mband-min_band+1, ik-first_ik+1, ispin) = evn(min_band:mband)
                  res%num_bands_(ik-first_ik+1, ispin) = mband
                  ! note that res%occ_ should not be used -it is not copied

                  if (get_gradient) then
                    allocate(grad(nbn,nbn,3))
                    
                    if (nrel < 2 ) then
                        call VKMatrixNonrel(nbndf,nbn,nbask_kpath(ik-first_ik+1), ik,ispin, zbn, evbn, grad, indgb_kpath(1,ik-first_ik+1), .true.)
                    else
                        call VKMatrixRel(nbndf,nbn,nbask_kpath(ik-first_ik+1), ik,ispin, zbn, evbn, grad, indgb_kpath(1,ik-first_ik+1), .true.)
                    endif 
                    
                    res_gradient%data_(ik-first_ik+1, ispin, 1:mband-min_band+1, 1:mband-min_band+1,:) = grad(min_band:mband,min_band:mband,:)
                    deallocate(grad)
                  end if

              else
                  res%num_bands_(ik-first_ik+1, ispin) = 0
              end if
            end do
        end do

	    deallocate(zbn,evbn,evn)
	    deallocate(indgb_kpath,nbask_kpath)

    end subroutine

    ! constructs an expansion for solution on the whole BZ from the expansion for the solution
    ! on the irreducible BZ
    subroutine GetBandStructure(res, res_gradient, min_band, max_band, kk, get_gradient)
        use LAPW
        use solid_mod
        use atom_mod
        use manager_mod, only:chem_pot, nspin, nrel, nspin_0, irel
        use parallel_mod
        use TransportOptics, only: Velocities
        use velocity_matrix, only: VkMatrixNonrel, VkMatrixRel
        type(BandStructure),intent(inout) :: res
        type(Velocities),intent(inout)    :: res_gradient
        integer, intent(in) ::  min_band, max_band
        integer, intent(in) :: kk
        logical, intent(in) :: get_gradient
	    complex*16, allocatable :: grad(:,:,:)
        integer :: ispin, ibnd, k0, mb, mink, maxk, k, ik, upperBand, ind_k, off, n
        real(kind=dp) :: fact

        mb = max_band
        if (mb == 0)   mb = maxval(n_bnd(:,:))

        res%num_expanded = mb - min_band + 1

        res%num_si = nspin

        if (kk < 0) then
            res%num_k = npnt
            mink = 1
            maxk = npnt

            ! if memory is sharded, return only within the shard:
            if (size(ndim3_k) > 1) then
                res%is_sharded = .True.
                res%shard_offset = n3_mpi_k(me3_k+1)
                res%num_k = ndim3_k(me3_k+1)
                mink = res%shard_offset + 1
                maxk = res%shard_offset + res%num_k
            end if
        else
            res%num_k = 1
            mink = kk
            maxk = kk

            if (size(ndim3_k) > 1) then
                res%is_sharded = .True.
                res%shard_offset = n3_mpi_k(me3_k+1)
            end if
        end if

        res%num_muffin_orbs = nfun
        res%max_num_pw = nbasmpw

        call res%allocate()

        if (get_gradient) then
            res_gradient%num_expanded = mb - min_band + 1
            res_gradient%num_si = nspin
            res_gradient%num_k = res%num_k
            call res_gradient%allocate()
        end if

        res%chemical_potential = chem_pot

        fact = 2.0 / nspin  / nrel

        off = res%shard_offset

        do ispin=1,nspin
            do k=mink, maxk
                ik = k-mink + 1
                upperBand             = min(n_bnd(k, ispin),mb)  ! n_bnd is not sharded
                res%num_bands_(ik, ispin) = n_bnd(k, ispin)
                do ibnd = min_band, upperBand
                    res%energy_(ibnd-min_band+1, ik, ispin)     = e_bnd(ibnd, k, ispin)  ! e_bnd is not sharded
                    res%Z_(:,ibnd-min_band+1,ik, ispin)         = z_bnd(:,ibnd, k-off, ispin)
                end do
                if (allocated(g_full_0)) then
                    do ibnd = min_band, upperBand
                        res%Occ_(1:upperBand-min_band+1, ibnd-min_band+1, ik, ispin) = fact * g_full_0(min_band:upperBand, ibnd, k-off, ispin)
                    end do
                else
                    do ibnd = min_band, upperBand
                        res%Occ_(ibnd-min_band+1, ibnd-min_band+1, ik, ispin) = fact * g_full_00(ibnd, k-off, ispin)
                    end do
                endif

                if (get_gradient) then
                    n=n_bnd(k, ispin)
                    allocate(grad(n,n,3))
                    
                    if (nrel < 2 ) then
                        call VKMatrixNonrel(nbndf,n,nbask(ik),k,ispin, z_bnd, ev_bnd, grad, indgb(1,ik), .true.)
                    else
                        call VKMatrixRel(nbndf,n,nbask(ik),k,ispin, z_bnd, ev_bnd, grad, indgb(1,ik), .true.)
                    endif 
                    res_gradient%data_(ik, ispin, 1:upperBand-min_band+1, 1:upperBand-min_band+1,:) = grad(min_band:upperBand,min_band:upperBand,:)
                    deallocate(grad)
                  end if

            end do
        end do
    end subroutine

    ! this is currently unused
    subroutine CheckSolution(sol)
        use solid_mod
        use persistence
        use manager_mod
        use parallel_mod
        use FlapwMBPT_interface
        type(DFT),intent(inout) :: sol
        integer :: k,n, i,j !, ind_k
        complex(kind=dp),allocatable :: overlap(:,:)
        !real(kind=dp) :: s

        print *, "Checking orthonormality of basis - as is"
        do k=1,npnt ! irreducible
            n=n_bnd(k,1)
            allocate(overlap(n,n))
            overlap(:,:) = 0.d0
            call CalcOverlap(overlap,n,k)
            !call CalcOverlap2(overlap,n,1, k)  ! should loop ispin=1,nspin
            !            do i=1,n
            !                s = sqrt(abs(overlap(i,i)))
            !                overlap(i,:) = overlap(i,:) / s
            !                overlap(:,i) = overlap(:,i) / s
            !            end do
            do i=1,n
                do j=1,n
                    if (i/=j) then
                        if (abs(overlap(i,j)) > 1.0d-8) print *, "non orthogonal band for k=",k, i,j,overlap(i,j)
                    else
                        if (abs(overlap(i,i) - 1.d0) > 1.0d-8) print *, "non normalized band for k=",k, i,overlap(i,i)
                    end if
                end do
            end do
            deallocate(overlap)
        end do
        print *, "end checking orthonormality. num kpoints (irr):", npnt, minval(n_bnd(:,1))
    end subroutine


    ! unused - but works
    subroutine CalcOverlap2(overlap,nbnd,ispin,k)
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        use units_mod
        implicit none
        integer, intent(in) :: nbnd,ispin
        integer :: isort,iatom,l,ie,lm,km,i,lget,je,kmj,in2,jn,in1,jn1,mj,li,ib,jb,nb,k
        complex(kind=dp), intent(inout) :: overlap(nbnd,nbnd)
        real(kind=dp), allocatable :: gb(:,:)
        complex(kind=dp),allocatable :: tmp(:,:), plane_wave_overlap(:,:)

        overlap = 0.d0

        ! ------- MT contribution ------------------------------------------
        do iatom=1,natom
            isort=is(iatom)
            do lm=1,nrel*(lmb(isort)+1)**2
                if(irel.ne.2) then
                    l=lget(lm)
                    li=l+1
                else if(irel.eq.2) then
                    call getlimj(lm,l,i,mj,li,0)
                endif
                do je=1,ntle(l,isort)
                    in1=1
                    if(augm(je,l,isort)/='LOC') in1=2
                    do jn1=1,in1
                        kmj=io_lem(iatom)-1+indbasa(jn1,je,lm,isort)
                        do ie=1,ntle(l,isort)
                            in2=1
                            if(augm(ie,l,isort)/='LOC') in2=2
                            do jn=1,in2
                                km=io_lem(iatom)-1+indbasa(jn,ie,lm,isort)
                                do jb=1,nbnd
                                    do ib=1,nbnd
                                        !if (jn == jn1 .and. ie == je) then
                                        overlap(ib,jb) = overlap(ib,jb)+ &
                                            z_bnd(kmj,jb, k, ispin)* dconjg(z_bnd(km,ib, k, ispin)) * ffsmt(jn,jn1,ie,je,li,isort,ispin)
                                    enddo
                                enddo
                            enddo
                        enddo
                    enddo    !! over lm
                enddo  !! over iatom
            enddo   !! over ib

        enddo

        ! ------- Interstitial contribution ------------------------------
        nb=nbask(k)
        allocate(plane_wave_overlap(nb,nb))
        allocate(tmp(nb,nbnd))
        ! ------ We form the overlap matrix -----------------------------
        allocate(gb(3,nb))
        do i=1,nb
            gb(:,i)=gbs(:,indgb(i,k))
        enddo
        call overlap_lapw(gb,pnt(1,k),nb,plane_wave_overlap,indgb(1,k))
        call zgemm('n','n',nb,nbnd,nb,(1.d0,0.d0),plane_wave_overlap,nb,ev_bnd(1,1,k,ispin),&
            nbasmpw,(0.d0,0.d0),tmp,nb)
        call zgemm('c','n',nbnd,nbnd,nb,(1.d0,0.d0),ev_bnd(1,1,k,ispin),nbasmpw,&
            tmp,nb,(1.d0,0.d0),overlap,nbnd)  ! note the last 1 - it adds to overlap
        deallocate(plane_wave_overlap,tmp,gb)


    end subroutine


    ! unused - but works
    ! adapted from overl_bnd_spin - which calculates overlap between spin states
    ! this however calculates overlap between the same spins
    subroutine CalcOverlap(ov,n,k)
        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        use units_mod
        implicit none
        integer, intent(in) :: n,k
        complex*16, intent(out) :: ov(n,n)
        integer :: isort,iatom,l,ie,lm,km,i,lget,je,kmj,in,jn,in1,jn1,mj,li,ib,nb
        real*8, allocatable :: gb(:,:)
        complex*16, allocatable :: s(:,:),tmp(:,:)
        ! ------- MT contribution ------------------------------------------
        allocate(tmp(nfun,n))
        tmp=(0.d0,0.d0)
        do ib=1,n
            do iatom=1,natom
                isort=is(iatom)
                do lm=1,nrel*(lmb(isort)+1)**2
                    if(irel.ne.2) then
                        l=lget(lm)
                        li=l+1
                    else if(irel.eq.2) then
                        call getlimj(lm,l,i,mj,li,0)
                    endif
                    do je=1,ntle(l,isort)
                        in1=1
                        if(augm(je,l,isort)/='LOC') in1=2
                        do jn1=1,in1
                            kmj=io_lem(iatom)-1+indbasa(jn1,je,lm,isort)
                            do ie=1,ntle(l,isort)
                                in=1
                                if(augm(ie,l,isort)/='LOC') in=2
                                do jn=1,in
                                    km=io_lem(iatom)-1+indbasa(jn,ie,lm,isort)
                                    tmp(kmj,ib)=tmp(kmj,ib)+&
                                        z_bnd(km,ib,k,1)&
                                        *ffsmt(jn,jn1,ie,je,li,isort,1)
                                enddo
                            enddo
                        enddo
                    enddo
                enddo    !! over lm
            enddo  !! over iatom
        enddo   !! over ib
        call zgemm('c','n',n,n,nfun,(1.d0,0.d0),tmp,nfun,z_bnd(1,1,k,1),nfun,(0.d0,0.d0),ov,n)
        deallocate(tmp)
        ! ------- Interstitial contribution ------------------------------
        nb=nbask(k)
        allocate(s(nb,nb))
        allocate(tmp(nb,n))
        ! ------ We form the overlap matrix -----------------------------
        allocate(gb(3,nb))
        do i=1,nb
            gb(:,i)=gbs(:,indgb(i,k))
        enddo
        call overlap_lapw(gb,pnt(1,k),nb,s,indgb(1,k))
        call zgemm('n','n',nb,n,nb,(1.d0,0.d0),s,nb,ev_bnd(1,1,k,1),&
            nbasmpw,(0.d0,0.d0),tmp,nb)
        call zgemm('c','n',n,n,nb,(1.d0,0.d0),ev_bnd(1,1,k,1),nbasmpw,&
            tmp,nb,(1.d0,0.d0),ov,n)
        deallocate(s,tmp,gb)
    end subroutine

end module
