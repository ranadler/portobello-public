module flapw_driver

    use persistence, only: dp
    implicit none
    private

    public :: MainSubroutine
    public :: ConnectFlapwMBPTImp, DisconnectImp, GetDFTInfoImp, CreatePlotsImp,\
              GetLAPWBasisImp, GetKPathBandsAndGradientImp, GetKPathBandsImp, GetPartialBandStructureImp,\
              CalculateFreeEnergyDFTImp, CalculateFreeEnergyDMFTImp,CalculateFreeEnergyGutzImp,\
              GetEquivalentAtomSymmetriesImp, GetWignerMatricesImp, ReloadImp, GetPartialBandStructureAndGradientImp! so we can find these in the shared object in GCC

              !CalculateForceImp,

    real(kind=dp) :: actual_b_extval
contains

    ! not used - tested T=0 limit
    function free_energy_x0(de) result(fe0)
        use manager_mod
        implicit none
        real*8 :: de,x,fe0
        if(x<=0.0) then
            fe0 = de
        else 
            fe0 = 0
        end if 
        return
    end function

    subroutine SaveVksInBands()
        use stringifor
        use energy, only: FreeEnergy
        use solid_mod
        use manager_mod
        use etot_mod
        use persistence, only: dp
        use atom_mod
        use units_mod
        use parallel_mod
        use Rho, only: RhoInBands, RhoInfo
        implicit none
        integer :: isort, ispin, ind_k, k, i, vsli_choice, n
        v_xc_bb(:,:,:,:) = 0.0
        v_h_bb(:,:,:,:) = 0.0
     
        vsli_choice = 1 ! Start LM2 for FLAPW matrix elements. 1: all 2: only L>0
             
        ! calculate Hartree potential in band basis v_h_bb
        pv=0.d0
        if(irel==2) pvj=0.d0
        call vslli(0,vsli_choice,v_mt_h,v_mt_xc,v_intr_h,v_intr_xc) ! VCrystal + Vh form v_mt*, v_intr*
        do ispin=1,nspin
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                call v_bloch(v_h_bb(1,1,ind_k,ispin),pnt(1,k),ispin,&
                            n_bnd(k,ispin),nbndf,nbask(k),indgb(1,k),&
                            ev_bnd(1,1,ind_k,ispin),z_bnd(1,1,ind_k,ispin),&
                            pv,pvj,war,wari)
            
            end do
        end do

        ! calculate Hartree potential in band basis v_xc_bb
        pv=0.d0
        if(irel==2) pvj=0.d0
        call vslli(1,vsli_choice,v_mt_h,v_mt_xc,v_intr_h,v_intr_xc) !  Vxc form v_mt*, v_intr*
        do ispin=1,nspin
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                call v_bloch(v_xc_bb(1,1,ind_k,ispin),pnt(1,k),ispin,&
                            n_bnd(k,ispin),nbndf,nbask(k),indgb(1,k),&
                            ev_bnd(1,1,ind_k,ispin),z_bnd(1,1,ind_k,ispin),&
                            pv,pvj,war,wari)
            
            end do
        end do
    end subroutine


    ! uses the unmodified unmodified V_*_bb to calculate tr(Vks*rho)
    subroutine CalculateVksIntegrals(vxc_int,vh_int,is_correlated_density)         
        use stringifor
        use energy, only: FreeEnergy
        use solid_mod
        use manager_mod
        use etot_mod
        use persistence, only: dp
        use atom_mod
        use units_mod
        use parallel_mod
        use Rho, only: RhoInBands, RhoInfo
        implicit none
        logical, intent(in) :: is_correlated_density
        real(kind=dp), intent(out) :: vxc_int, vh_int
        integer :: isort, ispin, ind_k, k, i, vsli_choice, n
        real(kind=dp) :: const
        type(RhoInBands),allocatable :: rhob
        type(RhoInfo),   allocatable :: rinfo
        real*8, allocatable :: tmp1(:,:)

        vxc_int=0 
        vh_int=0

        if (is_correlated_density) then
            allocate(rinfo, rhob)
            call rinfo%load("./rho.core.h5:/info")
            call rhob%load(rinfo%location%chars())
        end if

        do ispin=1,nspin
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                const=2.d0*wgt(k)/dfloat(nspin*nrel)
                n = n_bnd(k,ispin)

                allocate(tmp1(n,n))
                if (.not. allocated(g_full_0)) then
                    tmp1=(0.d0,0.d0)
                    do i=1,n
                        tmp1(i,i)=g_full_00(i,ind_k,ispin)
                    enddo
                    if (is_correlated_density) then
                        tmp1(rhob%min_band:rhob%max_band, rhob%min_band:rhob%max_band) = rhob%rho_(:, :, ind_k, ispin)
                    endif
                else
                    tmp1(:n,:n) = g_full_0(:n,:n,ind_k, ispin)
                endif

                vh_int =vh_int + const* trace(v_h_bb(1:n,1:n,ind_k,ispin),tmp1(1:n,1:n), n)
                vxc_int =vxc_int + const* trace(v_xc_bb(1:n,1:n,ind_k,ispin),tmp1(1:n,1:n), n)

                deallocate(tmp1)
            enddo
        enddo

        if (allocated(rhob)) deallocate(rhob)
        if (allocated(rinfo)) deallocate(rinfo)

    end subroutine

    function trace(aM, bM, n) result(tt)
        implicit none
        integer, intent(in) :: n
        real*8, intent(in) :: aM(n,n),bM(n,n)
        real*8 :: tt
        integer :: i,j
        tt = 0.0

        do i=1,n
            do j=1,n
                tt = tt + aM(i,j)*bM(j,i)
          enddo
        enddo
    end function

    ! the 2nd and 3rd parameters are here for debugging only.
    !   rougly it should be that the integration by means of tr(rho*V_bands) + the corresponding core quantity
    !   should be equal to the integrated quantity integral(dr*V(r)rho(r))
    !   so that:
    !        vxch ~ vxc_int+gx_vxc_cor  
    !        ro_vh_new ~ vh_int+gx_vh_cor
    ! we prefer (as does Singh, and consistent with the math) to use the integrals of V(r) because they are much
    ! more stable numerically, as can be seen when you plot the quantities above.
    subroutine OutputFreeEnergyAndComponents(FE, vxc_int, vh_int)
        use stringifor
        use energy, only: FreeEnergy
        use solid_mod
        use manager_mod
        use etot_mod
        use persistence, only: dp
        use atom_mod
        use units_mod
        use parallel_mod
        use penf_stringify
        use energy, only: FreeEnergy
        implicit none

        real(kind=dp), intent(in) ::  vxc_int, vh_int
        real(kind=dp) :: free_energy, e_coul, vks_terms, corr_energy
        type(FreeEnergy), intent(inout) :: FE
        
        ! See Singh's book page 95 for this functional
        ! these are all integrals of potential (Vks(r)) elements, calculated in FLAPW code.
        e_coul = z_vnucl !this is E_hartree + E_crystal
        corr_energy = FE%corr_energy_term/rydberg
        vks_terms =  exch_dft - vxch   & ! exchange: E_xc - integral(V*rho))
                    +e_coul - 0.5d0 * ro_vh_new
                        
        free_energy = d_free_x + d_free_c & ! spin factor included
                    + vks_terms &
                    + ecor & ! spin factor included
                    + chem_pot*nelec  &
                    + corr_energy  

        print *, "free energy per volume: [Ry/a0^3]", free_energy / amega
        print *, "free energy per atom: [Ry]", free_energy / FE%num_atoms
         
        print *, "F = E -TS [Ry] =", free_energy

        FE%result = free_energy
        FE%Fx = d_free_x
        FE%Fc = d_free_c
        FE%exch_dft = exch_dft
        FE%e_coul = e_coul
        FE%z_vnucl = z_vnucl
        FE%ro_vh_new = ro_vh_new
        FE%tr_vxc_valence = vxc_int
        FE%tr_vh_valence = vh_int
        FE%e_h_core = gx_vh_cor
        FE%e_xc_core = gx_vxc_cor
        FE%core_energy = ecor
        FE%muN = chem_pot*nelec
        FE%correlated_term_ry = corr_energy
        FE%muRy = chem_pot
        FE%N = nelec

    end subroutine 

    function CalculateFreeEnergyDFTImp(selfpath) result(loc)
        use stringifor
        use energy, only: FreeEnergy
        use DFTPlusSigma, only: set_g
        use solid_mod
        use manager_mod
        use etot_mod
        use persistence, only: dp
        use atom_mod
        use units_mod
        use parallel_mod
        use iso_c_binding

        type(string),intent(in) :: selfpath
        type(string) :: loc

        type(FreeEnergy), allocatable :: FE
        integer :: isort
        integer :: k,ispin,n,ind_omega,i_omega,i_tau,ind_k,i,it,ind_tau
        real*8 :: const,free_energy_x, vxc_int,vh_int

        ubi_0='dft'
        ubi='dft'
        call etot_gw_0()
        call set_g(.false.) ! no change of basis to Gx, we sum up d_free_x below

        call smultro
        call vcoul
        call add_vxc 
        call coulener(ro_vh_new)

        call exchener(1,vxch)
        call exchener(2,exch_dft)

        do ispin=1,nspin
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                const=2.d0*wgt(k)/dfloat(nspin*nrel)
                n=n_bnd(k,ispin)
                do i=1,n
                    ! the minus sign of the -trlog(1+exp(-beta*(e-mu))) is already inside
                    d_free_x=d_free_x+const*free_energy_x(e_bnd(i,k,ispin)-chem_pot)
                end do
            end do 
        end do
        !!!! do not call etot_gw_2, it overrides d_free_x
        if(nproc_k/=1) then
            call DGOP(d_free_x,1,'  +',comm_pnt)
            call DGOP(vxc_int,1,'  +',comm_pnt)
            call DGOP(vh_int,1,'  +',comm_pnt)
        end if

        allocate(FE)
        call OutputFreeEnergyAndComponents(Fe=FE, vxc_int=vxch, vh_int=ro_vh_new)
        call FE%store('./energy.h5:/')

        deallocate(FE)
    end function

    function CalculateFreeEnergyGutzImp(selfpath) result(loc)
        use stringifor
        use energy, only: FreeEnergy
        use DFTPlusSigma, only: set_g,MarkGutzwillerDensityIsReady
        use solid_mod
        use manager_mod
        use etot_mod
        use persistence, only: dp
        use atom_mod
        use units_mod
        use parallel_mod
        use iso_c_binding

        type(string),intent(in) :: selfpath
        type(string) :: loc

        type(FreeEnergy), allocatable :: FE
        integer :: isort
        integer :: k,ispin,n,ind_omega,i_omega,i_tau,ind_k,i,it,ind_tau
        real*8 :: const,free_energy_x, vxc_int,vh_int, Nqp, fermi_dirac

        ubi_0='dft'
        ubi='dft'

        ! now this happens after vsli, bands
        call smultro
        call vcoul
        call add_vxc 
        call coulener(ro_vh_new)
        
        call SaveVksInBands()
        call CalculateVksIntegrals(vxc_int,vh_int,.true.) ! reads g from bus

        call exchener(1,vxch)
        call exchener(2,exch_dft)

        call MarkGutzwillerDensityIsReady()
        call set_g(.false.) ! no change of basis to Gx, we sum up d_free_x below

        call etot_gw_0()
        !call core_all ! calculate the core exchange, only for ubi='dft'
        !call ROfull(.false.,1) ! add code to to the local density
        
        allocate(FE)
        call FE%load('./energy.h5:/')

        ! this means that we are changing a global variable, and there should not
        ! be self-consistency loop iterations in the same process after this.
        
        e_bnd(FE%min_band:Fe%max_band,:,:) = FE%Hqp_(:,:,:)/rydberg

        Nqp = 0.d0
        do ispin=1,nspin
            do ind_k=1,ndim3_k(me3_k+1)
                k=n3_mpi_k(me3_k+1)+ind_k
                const=2.d0*wgt(k)/dfloat(nspin*nrel)
                n=n_bnd(k,ispin)
                do i=1,n
                    ! the minus sign of the -trlog(1+exp(-beta*(e-mu))) is already inside
                    d_free_x=d_free_x+const*free_energy_x(e_bnd(i,k,ispin)-chem_pot)
                    Nqp = Nqp + const*fermi_dirac(e_bnd(i,k,ispin)-chem_pot)
                end do
            end do 
        end do
        !!!! do not call etot_gw_2, it overrides d_free_x
        if(nproc_k/=1) then
            call DGOP(d_free_x,1,'  +',comm_pnt)
            call DGOP(vxc_int,1,'  +',comm_pnt)
            call DGOP(vh_int,1,'  +',comm_pnt)
            call DGOP(Nqp,1,'  +',comm_pnt)
        end if

        FE%Nqp = Nqp ! Nqp-N is a good measure of conversion

        ! in gutz,  vxc_int and vh_int are calculated out of etot_gw_1, because there, 
        ! only DFT or G(tau=0) densities are supported, whereas we need to calculate
        ! tr(Vks*rho) with the dft+gutzwiller density
        call OutputFreeEnergyAndComponents(Fe=FE, vxc_int=vxc_int, vh_int=vh_int)

        call FE%store('./energy.h5:/')

        deallocate(FE)
    end function


    function CalculateFreeEnergyDMFTImp(selfpath) result(loc)
        use stringifor
        use energy, only: FreeEnergy
        use solid_mod
        use manager_mod
        use etot_mod
        use persistence, only: dp
        use atom_mod
        use units_mod
        use parallel_mod
        use DFTPlusSigma, only: PrepareSigmaOrRho, ReadSigmaFromDMFT, set_g, SetKohnShamBandsPlusSigmaXAndRotateSigc
        use penf_stringify
        implicit none

        type(string),intent(in) :: selfpath
        type(string) :: loc
        type(string) :: istr
        type(FreeEnergy), allocatable :: FE

        integer :: isort, ispin, ind_k, k, i
        integer :: i_nu, i_tau, ind_tau

        allocate(FE)
        call FE%load('./energy.core.h5:/')

        call etot_gw_0 ! probably not necessary but for good measure we initialize all energies to 0

        ! add teh self energy
        istr = trim(str(me3_k,no_sign=.True.))
        loc = './self-energy-' // istr //'.core.h5:/info/'
        call ReadSigmaFromDMFT(loc,.true.)   ! this reads sigma from the Portobello object to Kutepov's array
        ! -------------------------------------------------------------- !

        !call core_all  is done by the restart
        call SaveVksInBands()

        ! sets the bands to bands+Sigma
        call SetKohnShamBandsPlusSigmaXAndRotateSigc 
        
        ubi = ' gw'
        call set_g(.true.)
        
        !call ROfull(correlation,1) ! should we do it before caluclating hte potentials? does not seem to make changes. ro already summed up with core.

        call smultro
        call vcoul
        call add_vxc 
        call coulener(ro_vh_new)
        call exchener(1,vxch)  !don't call here, we need the one before core is set
        call exchener(2,exch_dft)

        call core_all ! realculate energy levels and exchange energy

        if(nproc_k/=1) then
            call DGOP(d_free_x,1,'  +',comm_pnt)
            call DGOP(d_free_c,1,'  +',comm_pnt)
            ! these are -int(potential*rho), note the minus sign in their construction
            call DGOP(gc_vxc_val,1,'  +',comm_pnt)
            call DGOP(gc_vh_val,1,'  +',comm_pnt)

        end if
        if (maswrk) then
            ! the gc*_val sums are -int(V*rho), already containing the minus sign
            call OutputFreeEnergyAndComponents(FE=FE, vxc_int=-gc_vxc_val, vh_int=-gc_vh_val)
            call FE%store('./energy.h5:/', .True.)            
        endif
        deallocate(FE)
    end function


    function ReloadImp(selfpath)  result(res)
        use manager_mod
        use parallel_mod
        use solid_mod
        use atom_mod
        use stringifor
        use FlapwMBPT, only: Input, RhobustaExtra
        use flapw_groups
        use flapw_image, only: LoadImage, WriteRho, ReadRho
        use etot_mod, only: vxch

        type(string), intent(inout) :: selfpath
        type(string) :: res
        logical :: complete

        if(maswrk) print *, "- reading run from checkpoint"            !call restart_solid(1)
        complete = LoadImage()
        iter = 2  ! on iter 1 kutepov searches the mu
        iter_dft =  1      ! since  we start at 2
        iter_max_scf = iter_dft + 3  ! since  we start at 2

        call set_g00

        ! Now we make the same preparation as one would make for band calculation
        ! Basically all the  densities and the potentials are ready
        ! This is not necessary in general -  only for restart where plots are made along k-path
        ! but to simplify the code, we do it on every restart (remove if it is expensive)
        !call ReadRho() ! no need to read, we just read it from the image
        call smultro(0,ro,rointr,v_intr_h,vatmt) ! calculates v from rho
        call vcoul   ! calculates v
        call add_vxc
        call exchener(1,vxch) !! for old Density   ! calculates exchange-correlation cont. to energy
        call core_all
        call rad_eqs
        pv=0.d0
        if(irel==2) pvj=0.d0
        ! ----------- Full Potential; L=0 term is absent ---------------
        call vslli(2,2,v_mt_h,v_mt_xc,v_intr_h,v_intr_xc,0)
        if (.not. complete) then
            if(maswrk) print *, "- claculating z_bn (solving for bands)"
            if(maswrk) print *, "-   [copy the overlaps tmp file to skip this]"
            call bands ! also recalculates Z_BN
        end if
    end function

	subroutine tau_frequency_meshes_PORT
		!     Create TAU and frequency meshes -------------
			  use manager_mod
			  use parallel_mod
			  use units_mod
			  implicit none
			  integer :: ind_tau,i_tau,i_omega,i_nu
			real*8 :: h1,h12,h2
			real*8, allocatable :: tran(:,:,:)
		! ----------------- TAU mesh --------------------------------------- 
			allocate(tau_mesh(0:n_tau))
			call mesh_tau_cheb
		! --------- Here for derivatives at tau=beta ------------------------	
			allocate(df_dtau_0(0:2))
			  h1=tau_mesh(1)
			  h12=tau_mesh(2)
			  h2=h12-h1
			  df_dtau_0(0)=-(h1+h12)/h1/h12
			  df_dtau_0(1)=h12/h1/h2
			  df_dtau_0(2)=-h1/h2/h12
		! ----------------- FREQUENCY meshes --------------------------------
			  !call mesh_nu
		! ------------------------------------------------------------------      
			  call mesh_omega
		! --- Basic AB transformations TAU ---> OMEGA -----------------------
			  allocate(ab_omega_from_tau(ndim3_tau,0:n_omega_d,2))
			  ab_omega_from_tau=0.d0
			  allocate(tran(n_tau/2,1+n_omega_d,2))
			  call transf_ferm_from_tau(tran,n_omega_d+1,n_omega_d+1,&
			                           w_omega_d(0))
			  do ind_tau=1,ndim3_tau
				i_tau=me3_tau*ndim3_tau+ind_tau-1
				if(i_tau/=0) then
				  do i_omega=0,n_omega_d
					if(i_tau/=n_tau/2) ab_omega_from_tau(ind_tau,i_omega,1)=&
			                                         tran(i_tau,i_omega+1,1)
					ab_omega_from_tau(ind_tau,i_omega,2)=&
			                                         tran(i_tau,i_omega+1,2)
				  enddo
				endif
			  enddo
			  deallocate(tran)
		! --- Prepare General transformations TAU ---> NU -----------------------
			!   allocate(ab_nu_from_tau(ndim3_tau,0:n_nu_d,2))
			!   ab_nu_from_tau=0.d0
			!   allocate(tran(n_tau/2,1+n_nu_d,2))
			!   call transf_bos_from_tau(tran,n_nu_d+1,n_nu_d+1,w_nu_d(0))
			!   do ind_tau=1,ndim3_tau
			! 	i_tau=me3_tau*ndim3_tau+ind_tau-1
			! 	if(i_tau/=0) then
			! 	  do i_nu=0,n_nu_d
			! 		ab_nu_from_tau(ind_tau,i_nu,1)=tran(i_tau,i_nu+1,1)
			! 		if(i_tau/=n_tau/2) ab_nu_from_tau(ind_tau,i_nu,2)=&
			!                                          tran(i_tau,i_nu+1,2)
			! 	  enddo
			! 	endif
			!   enddo
			!   deallocate(tran)
			  call timel('**** TAU_FREQUNCY_MESHES finished **')
			  end

    subroutine SetShardsFromMPI()
        use mpi, only: MPI_COMM_SIZE, MPI_COMM_WORLD
        use manager_mod
        use parallel_mod
        integer :: ierror

        CALL MPI_COMM_RANK(MPI_COMM_WORLD,me,ierror)                      
        call MPI_COMM_SIZE(MPI_COMM_WORLD,nproc,ierror) 
    end subroutine

    ! this is a workaround around a problem which prevents running with spin polarization but B=0
    subroutine FakeInitializeNspin(extra)
        use manager_mod
        use parallel_mod
        use solid_mod
        use atom_mod
        use DFTPlusSigma, only: is_dft_spin_polarized
        use FlapwMBPT, only: Input, RhobustaExtra
        type(RhobustaExtra),intent(inout) :: extra

        is_dft_spin_polarized = .False.
        actual_b_extval = b_extval
        if (irel <=1) then
            ! we piggiback on iter_h_ext to signal nspin=2
            if (iter_h_ext/10000/=0) then
                nspin = 2
                nspin_0 = 2
                nspin_1 = 2
                b_extval = 1.0 ! trick afteread to set nspin_0=2
                if (.not. extra%is_dft_spin_polarized) then
                    if(maswrk) print *, "- running in spin-polarized mode, but with symmetrical spins"
                    magn = 1 ! this choise has no magnetic density in the funcitonal
                else
                    if(maswrk) print *, "- running in spin-polarized mode (broken spin symmetry + SDFT functional)"
                    magn = 2 ! full SDFT functional
                    !if (.not. allocated(lm_tild)) allocate(lm_tild(nrel_core*limlb1))
                    is_dft_spin_polarized = .True.
                end if
            else
                nspin = 1
                nspin_0 = 1
                nspin_1 = 1
                magn = 0 ! this choise has no magnetic density in the functional
            end if
        else if (irel == 2) then
            nspin = 1
            nspin_0 = 1
            nspin_1 = 1
            magn = 1 ! just to show this explicit choice here, it is in fact already set to 1
            magn_shift = 0.0 ! make sure the is no magnetic shift to let afteread.F switch to magn=2
            if(maswrk) print *, "- dft is fully-relativistic (nspin set to 1 and band number is doubled)"
        end if
    end subroutine

    subroutine UnfakeNspin()        
        use manager_mod
        use parallel_mod
        use solid_mod
        use atom_mod
        use DFTPlusSigma, only: is_dft_spin_polarized
        if (nspin == 2) then
            b_extval = actual_b_extval
        else
            ! no magnetic field in unpolarized or dirac dft
            iter_h_ext = 0.0 
            b_extval = 0.0
        end if
    end subroutine

    ! options: comma separated lower case strings:
    !          +MB, restart
    function ConnectFlapwMBPTImp(selfpath, inpfile, num_workers, options) result(res)
        use iso_c_binding
        use manager_mod
        use parallel_mod
        use solid_mod
        use atom_mod
        use stringifor
        use input_mod ! remove this
        use FlapwMBPT, only: Input, RhobustaExtra
        use flapw_groups
        use flapw_image, only: LoadImage, WriteRho, ReadRho
        use RhoMixing, only: charge_convergence
        use etot_mod, only: vxch, exch_dft, ro_vh_old, ro_vh_new
        use DFTPlusSigma, only: is_dft_spin_polarized

        type(string), intent(inout) :: selfpath
        type(string),intent(in) :: inpfile
        type(string),intent(in) :: options
        integer(kind=c_int),value :: num_workers
        type(string) :: res

        type(Input),allocatable :: inp
        type(RhobustaExtra),allocatable :: extra

        logical :: restart_opt,init_Gw,from_file

        integer :: maxbv,maxbj,maxw2, ind,saved_iter_gw

        type(string),allocatable :: opts(:)

		from_file=.False.

        call options%split(opts,',',10)
        init_Gw = getOption("+Gw")
        restart_opt = getOption("restart")
        
		! from beging                                                           
		master = 0                                   
        if (num_workers > 1) then   
            call SetShardsFromMPI               
        else
            me = 0
            nproc = 1
        end if

		if (nproc > 1) then
		   goparr = .TRUE.                                                
		else                                                              
			goparr = .FALSE.                                               
		end if                                                           
		maswrk = me == master

        mem_using=0.d0
        mem_max=0.d0
        mem_where='qft'
	    call msgset(msgdbl,msgint,msgchr) ! Kutepov's type-unsafe broadcast constants

        if(maswrk) print *, "- reading ini file " // inpfile
        allocate(inp,extra)
        call inp%Load(inpfile%chars())
        call extra%Load(inpfile%chars() // "rhobusta/")  ! contains the extra flags which we save in the same file

        if(maswrk) print *, "- finished reading input files"

        inp%ctrl%nproc_k = nproc
        if(maswrk) print *, "- number of DFT shards is", inp%ctrl%nproc_k

		if (from_file) then
			call init()
		else
        	call initFlapwMBPT(inp)   ! this is adapted from Kutepov to read from the structure	
            call FakeInitializeNspin(extra)
            call set_control
		end if

		interp_omega_d = 2 ! add to init

        if(maswrk) print *, "- done setting input parameters"
        call start_par_mb
        if(maswrk) print *, "- initializing tau, frequency meshes"
        call tau_frequency_meshes_PORT

        if(maswrk) print *, "- initializing geometry and group"

        saved_iter_gw = iter_gw
        if (init_Gw) iter_gw= 1 ! make it allocate some gw arrays
        call memory_solid(maxbv,maxbj,maxw2)
        if (init_Gw) iter_gw= saved_iter_gw
        call get_bz

     	call InitGroup(maxbv,maxbj,maxw2,inp%sgg)  ! this adapted to initialize from the supplied group

        if(maswrk) print *, "- Initilizing BZ and meshes"
        call r_mesh_checking(mdiv)
        call r_mesh_checking(nrdiv)
        call bravay

        if(maswrk) print *, "- full BZ size:", nqdiv
        if(maswrk) print *, "- Irr. BZ size:", npnt 

        call symlmm
        if(maxnsymb.eq.0) maxnsymb=1
		call get_tau

        if(maswrk) print *, "- initializing basis functions"
        call getmts
        call radmsh
        maxnrad=maxval(nrad)

        if(maswrk) print *, "- initializing scf data structures"
        call start_par_solid
        call getind

        !  adler: set iter_gw = 1 so that it allocates g_full_00, the single particle GF.
        !         for g_full_0, the correlated GF,
        !            allocated(g_full_0) iff init_Gw is set here.
		call init_vh      
        if (init_Gw) iter_gw= 1
        iter_max_scf = iter_dft+1
            call memory1_solid
        iter_gw = 0
        iter_max_scf = iter_dft

        if(maswrk) print *, "- initializing free atoms"
        !call scf_0  ! only unused vars?? Removed -keeping freeatom
		call freeatom
        call neighbor_atm

        ! adler : the following allocations belong in warp
        allocate(sovr(numplw))
        if(complex_ro) allocate(sovi(numplw))
        call warp


        if(maswrk) print *, "- initializing correlation functions"
        !------- Here for pair correlation function -------------------	
        call g_pair_print_prepare
		!----------------------------------------------------------------
        call memory_flapw
        call rkmaxtest

        if(maswrk) print *, "- semi core states: ", n_semic
        if(maswrk) print *, "- magn=", magn

        call UnfakeNspin()
     
        ! ------------------------------
        !    next section comes from solid_scf
        !    it is executed once, so I moved it here
        !-------------------------------
        maxqmt=maxel*(maxel+1)/2
        if(maswrk) print *, "- complex_ro=", complex_ro
        if(maswrk) print *, "- nspin, nspin_0", nspin, nspin_0
        if (.not. restart_opt) then
            ! adler: this should not happen on restart
            call rostart(r_atom,ro_atom)
            call WriteRho()
        end if

        call timel('***** rostart finished *************')
        !  *****  the loop of self-consistency *****
        call timel('*******   SCF started **************')
        iter=1

        deallocate(inp, extra)
        charge_convergence = 100000.0 ! not converged - at least one iteration necessary

        ! handle restart
        if (restart_opt) then
            res = ReloadImp(selfpath)
        else
            call ubi_sumus(iter)
        end if

        res = selfpath

    contains
        logical function getOption(str)
            character(*),intent(in) :: str
            integer :: i
            getOption = .False.
            do i=1,size(opts)
                if (opts(i) == str) then
                    getOption = .True.
                    return
                end if
            end do
        end function

    end function

    function DisconnectImp(selfpath)  result(res)
        use stringifor
        use parallel_mod
        type(string), intent(inout) :: selfpath
        type(string) :: res

        call memory_close
        if (goparr) call ENDING
        stop
    end function

    function GetDFTInfoImp(selfpath)  result(res)
        use stringifor
        type(string), intent(inout) :: selfpath
        type(string) :: res

        call PrepareExports(3)  ! we don't need more than f-orbitals for projectors
        res = "./dft.core.h5:/"

    end function



    function CreatePlotsImp(selfpath) result(res)
        use stringifor
        use self_energies
        use manager_mod
        use parallel_mod, only: maswrk

        type(string), intent(inout) :: selfpath
        type(string) :: res


        !call k_taylor_test

        if (maswrk) print *, "- plotting dos"
        call output_dos

        if (maswrk) print *, "- calculating dft occupation numbers"
        call occ_number
        if (maswrk) print *, "- calculating dft energy"
	    if(ubi/=' qp') call etot_gw_2

        if (maswrk) print *, "- plotting bands"
        call plot_bands_lapw

        res = selfpath
    end function

    ! code deduced from kutepov's Rotate1, which uses u - follows the same
    ! cryptic pattern, instead of trying to deconstruct it
    ! see the code in rotate1 in rotate.f90 (from the code of key=1, that is Matrix multiplication of a vector - we extract the matrix)
    subroutine InitWigDl(WigD, l, flatW)
        use persistence, only:dp
        real(kind=dp), intent(inout) :: WigD(:,:)
        real(kind=dp), intent(inout) :: flatW(:)
        integer, intent(in) :: l
        integer :: nls,lm,m,nls1,m1,l1m1,l1,l2m,ll2m
        l1=l+1
        l2m=l1+l
        ll2m=l2m*l
        nls=(l2m-2)*ll2m/3+l1+ll2m
        lm=0
        do m=-l,l
            lm=lm+1
            nls1=nls+m
            do m1=-l,l
                l1m1=l1+m1
                WigD(lm,l1m1)  = flatW(nls1+l2m*m1)
                if (abs(WigD(lm,l1m1)) < 1.0d-10) WigD(lm,l1m1)=0.d0
            enddo
        enddo
    end subroutine


    ! code deduced from kutepov's Rotate2, which uses u - follows the same
    ! cryptic pattern, instead of trying to deconstruct it
    ! see the code in rotate2 in rotate.f90 (from the code of key=1, key1=2, that is Matrix multiplication of a vector - we extract the matrix)
    subroutine InitWigDj(WigD, l, ii, flatW)  ! jj means 2j, which is either jj=2(l-0.5) or jj=2(l+0.5)
        use persistence, only:dp
        integer, intent(in) :: l,ii
        complex(kind=dp), intent(inout) :: WigD(2*l+ii+1,2*l+ii+1)
        complex(kind=dp), intent(inout) :: flatW(:)
        integer :: jj,iwig0,lm,m,nls1,l1m1,m1
        jj = 2*l+ii
        iwig0=(8*l**3+12*l**2+10*l+3)/3+l*(2*l+1)*ii
        lm=0
        do m=-jj,jj,2
            lm=lm+1
            nls1=iwig0+(ii+m)/2
            l1m1=0
            do m1=-jj,jj,2
                l1m1=l1m1+1
                WigD(lm,l1m1) = flatW(nls1+(2*l+ii+1)*(ii+m1)/2)
                if (abs(WigD(lm,l1m1)) < 1.0d-10) WigD(lm,l1m1)=0.d0
            enddo
        enddo
    end subroutine


    subroutine PrepareExports(max_l)
        use FlapwMBPT_interface
        !use symz
        use atom_mod
        use solid_mod
        use manager_mod, only: nspin, irel, nrel
        use parallel_mod

        integer, intent(in) :: max_l
        type(DFT), allocatable :: dftObj
        integer :: ig, isort, iatom

        allocate(dftObj)

        dftObj%num_k_all = nqdiv
        dftObj%num_k_irr = npnt
        dftObj%num_si = nspin
        dftObj%nrel = nrel

        call dftObj%allocate()

        dftObj%k_irr_(:) = i_kref(:)
        dftObj%ig_(:) = k_group(:)


        dftObj%st%max_l = max_l
        dftObj%st%num_atoms = natom
        dftObj%st%num_distinct = nsort

        dftObj%sg%num_ops = ngroup

        dftObj%num_bands = maxval(n_bnd(:,1))

        dftObj%is_sharded = size(ndim3_k) > 1
        if (dftObj%is_sharded) then
            dftObj%num_k = ndim3_k(me3_k+1)
            dftObj%shard_offset = n3_mpi_k(me3_k+1)
        else
            dftObj%num_k = dftObj%num_k_irr
            dftObj%shard_offset = 0
        end if

        call dftObj%st%allocate()
        call dftObj%sg%allocate()

        ! import the wigner matrices, if they are set
        do ig=1, dftObj%sg%num_ops
            ! this is just a 1x1 identity, but we add it for consistency
            call InitWigDl(dftObj%sg%WigD0_(:,:,ig), 0, u(:,ig))
            call InitWigDl(dftObj%sg%WigD1_(:,:,ig), 1, u(:,ig))
            call InitWigDl(dftObj%sg%WigD2_(:,:,ig), 2, u(:,ig))
            call InitWigDl(dftObj%sg%WigD3_(:,:,ig), 3, u(:,ig))
        end do

        if (irel == 2) then
            do ig=1, dftObj%sg%num_ops
                !!!!                                            L
                call InitWigDj(dftObj%sg%WigD0J_(1:2,1:2,ig),   0,  1, uj(:,ig))

                call InitWigDj(dftObj%sg%WigD1J_(1:2,1:2,ig),   1, -1, uj(:,ig))
                call InitWigDj(dftObj%sg%WigD1J_(3:6,3:6,ig),   1,  1, uj(:,ig))

                call InitWigDj(dftObj%sg%WigD2J_(1:4,1:4,ig),   2, -1, uj(:,ig))
                call InitWigDj(dftObj%sg%WigD2J_(5:10,5:10,ig), 2,  1, uj(:,ig))

                call InitWigDj(dftObj%sg%WigD3J_(1:6,1:6,ig),   3, -1, uj(:,ig))
                call InitWigDj(dftObj%sg%WigD3J_(7:14,7:14,ig), 3,  1, uj(:,ig))
            end do
        end if


        ! import thestructure

        ! import the space group
        do ig=1, dftObj%sg%num_ops
            ! this follows the code from Kutepov's rotate() code
            dftObj%sg%rots_(:,:, ig)  = reshape(u(2:,ig), [3,3])
        end do

        dftObj%sg%shifts = shift

        dftObj%st%xyz =  tau * par  ! TODO: convert to angstrom instead of bohr
        dftObj%st%distinct_number = is

        dftObj%st%a = rbas(:,1)     ! TODO: convert to angstrom
        dftObj%st%b = rbas(:,2)
        dftObj%st%c = rbas(:,3)

        dftObj%st%Z = z

        dftObj%num_valence_electrons = 0
	    do iatom=1,natom
	        isort=is(iatom)
            dftObj%num_valence_electrons = dftObj%num_valence_electrons + sum(atoc(:,:,isort))
        end do

        dftObj%st%max_l_per_distinct = lmb
        !

        !print *, "- before storing exports"
        call dftObj%Store("./dft.core.h5:/", flush_opt=.True.)
        !print *, "- after storing exports"

        deallocate(dftObj)
    end subroutine


    function GetLAPWBasisImp(selfpath) result(loc)
        use iso_c_binding
        use LAPW, only:BandStructure, Basis
        use stringifor
        use persistence, only: dp

        use solid_mod
        use atom_mod
        use manager_mod
        use parallel_mod


        type(string),intent(in) :: selfpath
        type(string) :: loc

        type(Basis), allocatable :: lapw_basis
        integer :: ispin = 1
        integer :: isort,iatom,l,ie,lm,km,i,lget,je,kmj,in2,jn,in1,jn1,mj,li,nb, k

        real(kind=dp), allocatable :: gb(:,:)

        allocate(lapw_basis)

        lapw_basis%num_muffin_orbs = nfun
        lapw_basis%num_k_irr = npnt
        lapw_basis%num_k_all = nqdiv
        lapw_basis%num_k_shard = ndim3_k(me3_k+1)
        lapw_basis%num_si = nspin
        lapw_basis%nrel = nrel

        !print *, "in ExportLAPWBasis, num_pw=",nbasmpw, "nfun=",nfun, "npnt=",npnt

        call lapw_basis%allocate()

        do iatom=1,natom
            isort=is(iatom)
            do lm=1,nrel*(lmb(isort)+1)**2
                if(irel.ne.2) then
                    l=lget(lm)
                    !mj=?
                    li=l+1
                else if(irel.eq.2) then
                    call getlimj(lm,l,i,mj,li,0)
                endif
                do je=1,ntle(l,isort)
                    in1=1
                    if(augm(je,l,isort)/='LOC') in1=2
                    do jn1=1,in1
                        kmj=io_lem(iatom)-1+indbasa(jn1,je,lm,isort)
                        lapw_basis%atom_number_(kmj) = iatom
                        lapw_basis%n_(kmj) = int(ptnl(je,l,isort,ispin))
                        lapw_basis%l_(kmj) = l
                        lapw_basis%m_(kmj) = 0
                        lapw_basis%kind_(kmj) = jn1
                        if (correlated(je,l,isort) =='Y') then
                            lapw_basis%correlated_(kmj)= 1
                        else
                            lapw_basis%correlated_(kmj)= 0
                        end if
                        if (augm(je,l,isort)=='LOC') lapw_basis%kind_(kmj)  = 0  ! This creates eh LOC kind, and enforces ASSUMPTION:LOC_ORBS
                        do ie=1,ntle(l,isort)
                            in2=1
                            if(augm(ie,l,isort)/='LOC') in2=2
                            do jn=1,in2
                                km=io_lem(iatom)-1+indbasa(jn,ie,lm,isort)
                                lapw_basis%mo_overlap_(kmj,km) = lapw_basis%mo_overlap_(kmj,km) + ffsmt(jn,jn1,ie,je,li,isort,ispin)
                            enddo
                        enddo
                    enddo
                enddo
            enddo    !! over lm
        enddo  !! over iatom


        ! ------- Interstitial overlap ------------------------------
        do k=1,npnt
            nb=nbask(k)  ! actual size of interstitial basis

            if (size(ndim3_k) > 1) then
                if (k>n3_mpi_k(me3_k+1) .and. k<=n3_mpi_k(me3_k+1)+ndim3_k(me3_k+1)) then
                    lapw_basis%weight_(k-n3_mpi_k(me3_k+1)) = k_star(k) * 1.0d0 / nqdiv
                end if
            else
                    lapw_basis%weight_(k) = k_star(k) * 1.0d0 / nqdiv
            end if


            lapw_basis%pw_overlap(k)%num_pw = nb
            call  lapw_basis%pw_overlap(k)%allocate()
            lapw_basis%pw_overlap(k)%overlap_(:,:) = 0.d0

            allocate(gb(3,nb))
            do i=1,nb/irel
                gb(:,i)=gbs(:,indgb(i,k))
            enddo
            ! fills up the first nb*nb rows and columnsin the overlap matrix
            call overlap_lapw(gb,pnt(1,k),nb,lapw_basis%pw_overlap(k)%overlap_(1:,1:),indgb(1,k))
            deallocate(gb)
        end do

        do k=1,nqdiv
            lapw_basis%KPoints_(k,:) = pnt(:,k)
        end do


        call lapw_basis%store(selfpath%chars(), flush_opt=.True.)

        deallocate(lapw_basis)


        loc = selfpath

    end function

    function GetBasisMTInfoImp(selfpath) result(res)
        use stringifor
        use iso_c_binding

        use atom_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        use units_mod
        use LAPW, only: BasisMTInfo

        type(string), intent(inout) :: selfpath
        type(string) :: res
        integer :: li, l, ie,  mtc,mt, itc, jtc,ir, isp, iatom, isort, idx
        real :: c_ov, c2, dqdall
        real*8, allocatable :: work(:)
        type(BasisMTInfo),allocatable :: bmti


        c2=clight*clight

        allocate(work(0:maxnrad))
        allocate(bmti)

        bmti%num_unique_atoms = nsort
        call bmti%allocate()


        do isort=1,nsort
            if (bmti%atoms(isort)%num_orbitals > 0) cycle

            bmti%atoms(isort)%num_orbitals = sum(ntle(1:,:))*nrel + sum(ntle(0,:))
            !print *, "num orbitals:", bmti%atoms(isort)%num_orbitals, nspin, nrel, lmb(isort)
            call bmti%atoms(isort)%allocate()
            idx = 0
            do isp=1,nspin

                do li=1,nrel*lmb(isort)+1 ! up to max l for this atom, * (2), +1 for l=0
                    if(irel.le.1) then
                        l=li-1
                    else
                        l=li/2
                    endif
                    mtc=ntle_cor(li,isort)  !! last core level for this l

                    do ie=1,ntle(l,isort)

                        mt=indfun0(1,ie,li,isort)
                        mt=ind_wf(mt,isort)
                        if(mtc==0) then
                            c_ov=0.d0
                        else
                            itc=indcor0(mtc,li,isort)
                            jtc=indcor(itc,isp,isort)
                            work(:) = 0.0
                            do ir=0,nrad(isort)
                                work(ir)=pcor(jtc+ir)*gfun(mt+ir,isp)
                                if(irel.ge.1) work(ir)=work(ir) &
                                    +qcor(jtc+ir)*gfund(mt+ir,isp)/c2
                                work(ir)=work(ir)*r(ir,isort)*dr(ir,isort)
                            enddo
                            c_ov=dqdall(h(isort),work,nrad(isort))
                        endif
                        idx = idx + 1
                        bmti%atoms(isort)%l_(idx) = l
                        bmti%atoms(isort)%energy_(idx) = rydberg *(eny(ie,li,isort,isp) - chem_pot)
                        bmti%atoms(isort)%core_overlap_(idx) = c_ov
                        !print *,idx, bmti%atoms(isort)%l_(idx), bmti%atoms(isort)%energy_(idx), isort

                    end do
                end do
            end do
        end do

        call bmti%store(selfpath%chars(), flush_opt=.True.)

        deallocate(work, bmti)
        res = selfpath
    end function

    function GetPartialBandStructureAndGradientImp(selfpath, gradpath, min_band, max_band, k) result(loc)
        use iso_c_binding
        use band_structure, only:GetBandStructure
        use LAPW, only:BandStructure
        use TransportOptics, only: Velocities
        use stringifor
        type(string),intent(in) :: selfpath, gradpath
        type(string) :: loc
        integer(kind=c_int), intent(in),value :: min_band, max_band
        integer(kind=c_int), intent(in),value :: k
        type(BandStructure),allocatable :: res
        type(Velocities),allocatable    :: res_gradient
        allocate(res)
        allocate(res_gradient)
        call GetBandStructure(res,res_gradient,min_band,max_band,k,.true.)
        call res%store(selfpath%chars(), flush_opt=.True.)
        call res_gradient%store(gradpath%chars(), flush_opt=.True.)

        ! the following solves a memory issue that causes significant slowdown in some GNU systems (IC in BNL).
        ! in ifort this is called by the finalizer of res, but in GNU we don't have finalizers yet.
#ifdef __GFORTRAN__
            call res%disconnect()
#endif
        deallocate(res, res_gradient)
        loc = selfpath
    end function

    function GetPartialBandStructureImp(selfpath, min_band, max_band, k) result(loc)
        use iso_c_binding
        use band_structure, only:GetBandStructure
        use LAPW, only:BandStructure
        use TransportOptics, only: Velocities
        use stringifor
        type(string),intent(in) :: selfpath
        type(string) :: loc
        integer(kind=c_int), intent(in),value :: min_band, max_band
        integer(kind=c_int), intent(in),value :: k
        type(BandStructure),allocatable :: res
        type(Velocities),allocatable    :: res_gradient
        allocate(res)
        call GetBandStructure(res,res_gradient,min_band,max_band,k,.false.)
        call res%store(selfpath%chars(), flush_opt=.True.)

        ! the following solves a memory issue that causes significant slowdown in some GNU systems (IC in BNL).
        ! in ifort this is called by the finalizer of res, but in GNU we don't have finalizers yet.
#ifdef __GFORTRAN__
            call res%disconnect()
#endif
        deallocate(res)
        loc = selfpath
    end function
             
    function GetKPathBandsAndGradientImp(selfpath, gradpath, kpathpath, min_band, max_band) result(loc)
        use iso_c_binding
        use band_structure, only:GetBandStructure, GetKpathBands
        use LAPW, only:BandStructure, KPath
        use TransportOptics, only: Velocities
        use stringifor
        type(string),intent(in) :: selfpath, gradpath, kpathpath
        type(string) :: loc
        integer(kind=c_int), intent(in),value :: min_band, max_band
        type(BandStructure),allocatable :: res
        type(Velocities),allocatable :: res_gradient
        type(KPath), allocatable :: kp
        allocate(res, res_gradient, kp)
        call kp%Load(kpathpath%chars())
    
        call GetKpathBands(res, res_gradient, kp, min_band,max_band, .true.)

        call res%store(selfpath%chars(), flush_opt=.True.)
        call res_gradient%store(gradpath%chars(), flush_opt=.True.)
        ! the following solves a memory issue that causes significant slowdown in some GNU systems (IC in BNL).
        ! in ifort this is called by the finalizer of res, but in GNU we don't have finalizers yet.
#ifdef __GFORTRAN__
            call res%disconnect()
            call res_gradient%disconnect()
#endif
        deallocate(res, res_gradient, kp)
        loc = selfpath
    end function

    function GetKPathBandsImp(selfpath, kpathpath, min_band, max_band) result(loc)
        use iso_c_binding
        use band_structure, only:GetBandStructure, GetKpathBands
        use LAPW, only:BandStructure, KPath
        use TransportOptics, only: Velocities
        use stringifor
        type(string),intent(in) :: selfpath, kpathpath
        type(string) :: loc
        integer(kind=c_int), intent(in),value :: min_band, max_band
        type(BandStructure),allocatable :: res
        type(Velocities),allocatable :: res_gradient
        type(KPath), allocatable :: kp
        allocate(res, kp)
        call kp%Load(kpathpath%chars())

        call GetKpathBands(res, res_gradient, kp, min_band,max_band, .false.)

        call res%store(selfpath%chars(), flush_opt=.True.)

        ! the following solves a memory issue that causes significant slowdown in some GNU systems (IC in BNL).
        ! in ifort this is called by the finalizer of res, but in GNU we don't have finalizers yet.
#ifdef __GFORTRAN__
            call res%disconnect()
#endif
        deallocate(res, kp)
        loc = selfpath
    end function


    ! atom_nubmer starts in 1
    subroutine FillEquivalentAtomSymmetries(eas)
        use LAPW, only:EquivalentAtomSymmetries
        use iso_c_binding
        use solid_mod
        use atom_mod
        use manager_mod

        type(EquivalentAtomSymmetries), intent(inout) :: eas
        integer :: ai,bi
        integer, allocatable :: dNum(:)

        eas%num_atoms = natom
        allocate(dNum(natom))

        ! find all distinct numbers atoms

        do ai =1, natom
            do bi =1, ai
                if (is(ai) == is(bi)) then
                    dNum(ai) = bi
                    exit
                end if
            end do
        end do


        call eas%allocate()

        do ai =1, natom

            if (dNum(ai) == ai) then
                eas%g_(ai) = 0  ! this is the identity operation
            else
                eas%g_(ai) = FindOperation(dNum(ai), ai) - 1
            end if

        end do

        deallocate(dNum)

    contains

        function FindOperation(distinct_num, atom_num) result(res)
            integer,intent(in)::distinct_num, atom_num
            integer::res, ig

            res = -1
            do ig=1,ngroup
                ! g_ig * R_atom_number = ai
                if (ip(distinct_num, ig) == atom_num) then
                    res = ig
                    return
                end if
            end do

            print *, "- ERROR: not found isometry from atom:", distinct_num, "to atom:", atom_num
        end function


    end subroutine

    function GetEquivalentAtomSymmetriesImp(selfpath) result(loc)
        use iso_c_binding
        use LAPW, only:EquivalentAtomSymmetries
        use stringifor
        type(string),intent(in) :: selfpath
        type(string) :: loc
        type(EquivalentAtomSymmetries),allocatable :: res

        allocate(res)

        call FillEquivalentAtomSymmetries(res)

        call res%store(selfpath%chars(), flush_opt=.True.)
        deallocate(res)

        loc = selfpath
    end function

    function GetWignerMatricesImp(selfpath) result(res)
        use solid_mod, only: maxwig
        use persistence
        use symmetry
        use flapw_groups

        type(string), intent(inout) :: selfpath
        type(string) :: res

        integer :: maxwig_save

        real(kind=dp),allocatable :: Wflat(:,:)
        complex(kind=dp),allocatable ::  WflatJ(:,:)

        integer :: iop
        type(SpaceGroup), allocatable :: group,group2
        !type(GL3Group), allocatable :: group2

        real*8 s(3,3), r(3,3)

        maxwig_save = maxwig

        ! maxwig is not available if not running rhobusta first
        maxwig = 1000

        allocate(group, group2)
        call group%load(selfpath%chars())
        allocate(Wflat(maxwig,group%num_ops), WflatJ(maxwig*2,group%num_ops))

        do iop=1,group%num_ops
            ! this permutation copied from group.f90 - it is
            ! changing from xyz -> yzx
            ! this is the convention for calling bld_sh_trans

            s = group%rots_(:,:,iop)
            r(1,:) = [s(2,2), s(2,3), s(2,1)]
            r(2,:)=  [s(3,2), s(3,3), s(3,1)]
            r(3,:) = [s(1,2), s(1,3), s(1,1)]


            Wflat(:,iop) = 0.d0
            WflatJ(:,iop) = 0.d0

            Wflat(1,iop) = 1.d0
            WflatJ(1,iop) = 1.d0
            ! only l=3 are needed for the projectors
            call bld_sh_trans(r,Wflat(:,iop),3)

        end do

        maxwig = maxwig_save

        group%hasWigJ = .False.

        group2%num_ops = group%num_ops
        call group2%allocate()
        group2%rots_(:,:,:) = group%rots_(:,:,:)
        group2%shifts_(:,:) = group%shifts_(:,:)

        call CalculateDjFromDl(3, group%rots_, group%shifts_, Wflat(:,:), WflatJ(:,:), .False.)

        do iop=1,group2%num_ops
            call InitWigDl(group2%WigD0_(:,:,iop), 0, Wflat(:,iop))
            call InitWigDl(group2%WigD1_(:,:,iop), 1, Wflat(:,iop))
            call InitWigDl(group2%WigD2_(:,:,iop), 2, Wflat(:,iop))
            call InitWigDl(group2%WigD3_(:,:,iop), 3, Wflat(:,iop))

            !!!!                                      L  2J
            call InitWigDj(group2%WigD0J_(:,:,iop),   0, 1, WflatJ(:,iop))

            call InitWigDj(group2%WigD1J_(:2,:2,iop), 1, -1, WflatJ(:,iop))
            call InitWigDj(group2%WigD1J_(3:,3:,iop), 1, 1, WflatJ(:,iop))

            call InitWigDj(group2%WigD2J_(:4,:4,iop), 2, -1, WflatJ(:,iop))
            call InitWigDj(group2%WigD2J_(5:,5:,iop), 2, 1, WflatJ(:,iop))

            call InitWigDj(group2%WigD3J_(:6,:6,iop), 3, -1, WflatJ(:,iop))
            call InitWigDj(group2%WigD3J_(7:,7:,iop), 3, 1, WflatJ(:,iop))

        end do

        group2%hasWigJ = .True.

        call group2%store(selfpath%chars(), flush_opt=.True.)
        res = selfpath

        deallocate(group, group2, Wflat, WflatJ)

    end function

    subroutine initFlapwMBPT(inp)
        use atom_mod
        use etot_mod
        use manager_mod
        use parallel_mod
        use solid_mod
        use units_mod
        
        use FlapwMBPT
        implicit none

        type(Input),intent(inout) :: inp

        integer :: iatom,isort,l,n,i1,ii

        restart = .False. ! is this the right choice?
        allfile = inp%allfile%chars()
        iter_dft = inp%ctrl%iter_dft
        iter_hf = inp%ctrl%iter_hf
        iter_gw = inp%ctrl%iter_gw
        iter_qp = inp%ctrl%iter_qp

        admix = inp%ctrl%admix
        adspin = inp%ctrl%adspin
        adm_gw = inp%ctrl%adm_gw
        acc_it_gw = inp%ctrl%acc_it_gw

		cut_lapw_ratio = inp%bas%cut_lapw_ratio
		cut_pb_ratio = inp%bas%cut_pb_ratio
		eps_pb = inp%bas%eps_pb

        iexch = inp%ctrl%iexch
        scal_spin = inp%ctrl%scal_spin

        nproc_tau = inp%ctrl%nproc_tau
        nproc_k = inp%ctrl%nproc_k
        irel = inp%ctrl%irel
		irel_core = inp%ctrl%irel_core
		
		if(irel==0) irel_core=0
        irel_core=max(irel,irel_core)
        nrel=1
        if(irel.eq.2) nrel=2
        nrel_core=1
        if(irel_core.eq.2) nrel_core=2

        clight = inp%ctrl%clight
        rel_interst = inp%ctrl%rel_interst

        nrel=1
        if(irel.eq.2) nrel=2
        temperature = inp%ctrl%temperature

        if(maswrk) print *, "- FLAPWMBPT temperature is:", temperature

        symgen = "input"

        par = 1.0 ! don't use, implementation seems to have issues in Flapw code
        nsort = inp%strct%nsort
        natom = inp%strct%natom
        istruc = inp%strct%istruc
        b_a = inp%strct%b_a
        c_a = inp%strct%c_a

        allocate(tau(3,natom))
        allocate(is(natom))
        allocate(txtel(nsort))
        allocate(z(nsort))
        allocate(z_dop(nsort))
        allocate(zcor(nsort))
        allocate(magn_shift(nsort))
        allocate(smt(nsort))
        allocate(h(nsort))
        allocate(nrad(nsort))
        allocate(lmb(nsort))
        allocate(lmpb(nsort))


        rbas(:,1) =inp%strct%a_(:)
        rbas(:,2) =inp%strct%b_(:)
        rbas(:,3) =inp%strct%c_(:)

        do iatom=1,natom
            tau(:,iatom) = inp%strct%tau_(:,iatom)
			is(iatom) = inp%strct%isA_(iatom)
        enddo


        mdiv  = inp%rsm%mdiv_
        nrdiv = inp%rsm%nrdiv_

        eps_pb = inp%bas%eps_pb

        nbndf = inp%zns%nbndf

        n_k_div = inp%bp%n_k_div

        emindos = inp%dos%emindos
        emaxdos = inp%dos%emaxdos
        ndos = inp%dos%ndos
        e_small = inp%dos%e_small

        ndiv = inp%kg%ndiv_
        metal = inp%kg%metal
        n_k_div = inp%kg%n_k_div
        k_line = inp%kg%k_line%chars()

        v_v0 = inp%mscf%v_v0

        b_extval = inp%mag%b_extval

        iter_h_ext = inp%mag%iter_h_ext
        b_ext = inp%mag%b_ext_


        ! -------- Transform to internal magnetic field --------------
        b_ext=b_ext/sqrt(dot_product(b_ext,b_ext))

        !w_sc_gw, w_sc_qp only relevant for the GW, QP loops

        n_tau =          inp%tmesh%n_tau

        n_omega_exa =    inp%omesh%n_omega_exa
        n_omega_asy =    inp%omesh%n_omega_asy
        omega_max =      inp%omesh%omega_max

        n_nu_exa =   inp%nmesh%n_nu_exa
        n_nu_asy =   inp%nmesh%n_nu_asy
        nu_max =     inp%nmesh%nu_max

        maxb     = maxval(inp%strct%ad(:)%lmb)
        maxpb    = maxval(inp%strct%ad(:)%lmpb)
        maxntle  = 0
        do isort=1,nsort
            !print *, isort, "----- ntle:"
            !print *, inp%strct%ad(isort)%ntle_(:)
            maxntle  = max(maxntle, maxval(inp%strct%ad(isort)%ntle_(:)))
        end do

            allocate(konfig(0:maxb,nsort))
            allocate(atoc(maxntle,0:maxb,nsort))
            allocate(idmd(maxntle,0:maxb,nsort))
            allocate(ntle(0:maxb,nsort))
            allocate(augm(maxntle,0:maxb,nsort))
            allocate(correlated(maxntle,0:maxb,nsort))
            allocate(ptnl(maxntle,0:maxb,nsort,2))
            allocate(lim_pb_mt(0:maxpb,nsort))

            h=0.d0
            nrad=0
            z=0.d0
            z_dop=0.d0
            atoc=0.d0
            do isort=1,nsort
                txtel(isort)      = inp%strct%ad(isort)%txtel%chars()
                z(isort)          = inp%strct%ad(isort)%z
                magn_shift(isort) = inp%strct%ad(isort)%magn_shift
                smt(isort)        = inp%strct%ad(isort)%smt
                h(isort)          = inp%strct%ad(isort)%h
                nrad(isort)       = inp%strct%ad(isort)%nrad
                z_dop(isort)      = inp%strct%ad(isort)%z_dop
                lmb(isort)        = inp%strct%ad(isort)%lmb
                lmpb(isort)       = inp%strct%ad(isort)%lmpb
                lim_pb_mt(0:lmpb(isort),isort)     = inp%strct%ad(isort)%lim_pb_mt_(:)

                ntle(0:lmb(isort),isort)                = inp%strct%ad(isort)%ntle_(0:lmb(isort))
                !print *, "ntle: ", ntle(0:lmb(isort),isort)
                ! ------- Reading the type of the functions for L <= L_b ------
                do l=0,lmb(isort)
                    konfig(l,isort)=10
                    do n=1,ntle(l,isort)
                        if (inp%strct%ad(isort)%augm_(n,l) == 0) then
                            augm(n,l,isort) = "LOC"
                        else
                            augm(n,l,isort) = "APW"
                        end if
                        atoc(n,l,isort) = inp%strct%ad(isort)%atoc_(n,l)
                        ptnl(n,l,isort,1) = inp%strct%ad(isort)%ptnl_(n,l)
                        idmd(n,l,isort) = inp%strct%ad(isort)%idmd_(n,l)

                        if (inp%strct%ad(isort)%correlated_(n,l) == 0) then
                            correlated(n,l,isort) = "N"
                        else
                            correlated(n,l,isort) = "Y"
                        end if

                        ptnl(n,l,isort,2) = ptnl(n,l,isort,1)
                        konfig(l,isort)=min(konfig(l,isort),int(ptnl(n,l,isort,1)))
                    enddo
                enddo
            enddo
			
        z=z+z_dop  ! ??? this is usually 0
        ! ------------------------------------------------------------------
        i1=len_trim(allfile)
        do ii=i1+1,72
            dosfile(ii:ii)=' '
            outfile(ii:ii)=' '
            rstfile(ii:ii)=' '
        enddo
        dosfile(1:i1+4)=allfile(1:i1)//'.dos'
        outfile(1:i1+4)=allfile(1:i1)//'.out'
        rstfile(1:i1+4)=allfile(1:i1)//'.rst'
        !

        ! go to end of the output file
        iun = 2
        open(iun,file=outfile)
        call goto_end(iun)
    end

    ! intel doesn't allow the string assignment inside a main function!
    subroutine MainSubroutine
        use stringifor
        use flapw_image
        !use DFTPlusSigma, only: OneIteration
        use stringifor
        implicit none


        type(string) :: input_file, opts
        integer :: i
        character(len=100) :: arg
        type(string) :: sigma_location

        input_file = "./ini.h5:/"

        do i=1, command_argument_count()
            call get_command_argument(i, arg)
            if (arg == 'restart') opts = 'restart'
        end do

        ! this runs with no workers
        input_file = ConnectFlapwMBPTImp(input_file, input_file, 1, opts)

        do i=1,10
            
             !call OneIteration(sigma_location, .True., .True., .True.)

        end do

        !call restart_solid(-1)
        call StoreImage()


        call memory_close

        call ending

    end subroutine

end module


