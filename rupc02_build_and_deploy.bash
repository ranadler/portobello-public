#!/bin/bash

# should be run in the destination directory on rupc02: compiles it and syncrhonizes it
# the path is taken from the scripts path
export PORTOBELLO_BASE=${0%/*}

# These are the environment variables used to configure the Makefiles
export OMP_NUM_THREADS=1
export INTEL_PATH=/opt/sw/ompi/intel/18.0/
export HDF5_BASE=${INTEL_PATH}/hdf5/
export HDF5_INTEL=${HDF5_BASE}
export MPI2_BASE=/opt/sw/ompi/intel/18.0

# runtime definitions
export TARGET_OUTPUT_DIR=${PORTOBELLO_BASE}/build/intel-prod/
export TARGET2_OUTPUT_DIR=${PORTOBELLO_BASE}/build/intel2-prod/
export LD_LIBRARY_PATH=${TARGET_OUTPUT_DIR}:${LD_LIBRARY_PATH}
export FORTRAN_INTEL_LD_PATH=librhobusta.so:libCTQMC.so
export PYTHONPATH=${PORTOBELLO_BASE}

export PATH=$PATH:${HOME}/bin/:.
export BOOST_DIR=/opt/intel/intelpython2/

# make everything else
echo "                         compiling on rupc02  "
make -j20 --no-print-directory -C ${TARGET_OUTPUT_DIR} 
echo "-------------------------------------------------------------------------------"
echo "                         compiling on b* nodes  "
# we compile directly on the nodes since otherwise ompi4 causes crashes
# note that b02 is preferred over b01 as it makes more compatible binaryq
ssh -l ${USER} b01 /usr/bin/make -j5 --no-print-directory -C ${TARGET_OUTPUT_DIR} -j10
echo "-------------------------------------------------------------------------------"
echo "                         compiling on n* nodes  "
# note that Viktor requested not to compile on n01
ssh -l ${USER} n02 /usr/bin/make -j5 --no-print-directory -C ${TARGET2_OUTPUT_DIR} -j10

# we used to do this before
# rsync -i -a -v -e ssh --delete  ${TARGET_OUTPUT_DIR}*.so ${USER}@rupc-caip-01.rutgers.edu:${TARGET_OUTPUT_DIR}

# call separately (even link takes a while):
# make -C ${PORTOBELLO_BASE} -f Makefile.CTQMC-intel-CPU

