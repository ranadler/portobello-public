#!/usr/bin/python
'''
Created on May 9, 2017

@author: adler
'''
import os

def main():
    f = os.popen("x")
    xhelp = f.read()
    commands = xhelp.split("FLAGS:")[0].split("executables:")[1].split(",")
    for c in commands:
        c = c.rstrip().lstrip().rstrip("\n").lstrip("\n")
        if c in ["struct2poscar","shifteig", "w2waddsp","plane","dftd3","w2w",
                  "cif2struct", "dmftproj", "hex2rhomb", "rhomb_in5", "symmetso"
                  "clmaddsub", "optimize", "pairhess", "sgroup", "supercell", "wannier90"]:
            continue
        os.system("x -f XX -d " + c)
        
        req = []
        targets = []
        # now read the def file, and determine dependencies
        df = open(c+".def")
        for line in df.readlines():
            flds = line.split(",")
            old = flds[2].split("'")[1]
            suffix = flds[1].split("'")[1].split("XX")[1]
            is_old = old=='old'
            if is_old:
                req.append('%'+suffix)
            else:
                targets.append('%'+suffix)
        print("### ")
        print("### rule for", c)
        print("### ")
        print("{targets}: {req}".format(targets=" ".join(targets), req=" ".join(req)))
        print("\t$(call EXE,{command})".format(command=c))
            
        # mixer has circular dependency on .inm (with lapw0, through .clmsum)
        # joinvec has circular dep (vector)
        # substitute in1c with in1
        # in lapw1.def klist should be old
    
if __name__ == '__main__':
    main()
    