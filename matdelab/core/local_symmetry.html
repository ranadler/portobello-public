<html>
<head>
<title>Local Symmetry Analysis</title>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
<script type="text/javascript" async
  src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_CHTML">
</script>
</head>
<body>
  <h2> Objective </h2>
  Our goal is to analyze the non-accidental symmetries of the potential around an atom in the crystal, so that we can detect which elements of the Green's function matrix will be non-zero, and find a rotation of the axes that maximizes the number of zero entries in the matrix.
  <p>
  Typically we are interested in a given orbitals $(s,p,d,f)$, and therefore $l$ is fixed.
  
  <h2> Outline </h2>

  We determine the nearest neighbors of the given atom (see below), and the point-group of the local environment. This is a purely geometrical / group theoretical calculation.
  <p>
    Next, we determine the optimal z-axis. Our (intuitive) claim is that this axis is the axis of highest rotational symmetry of the local environment, with one exception being tetrahedral symmetry. See the discussion below.
  <p>
    $\hat{x}$ needs to be defined. This amounts to picking one angle $\Phi$ which rotates the x-axis around the main axis. It turns out that the best choices are projections of the ligands on the plane perpendicular to $\hat{x}$ plus a finite set of angles relative to them: picking a direction "in between" is sub-optimal. To show this, let us assume that we picked $\hat{x}$, and see how we can determine the matrix symmetries.
    <p>
  
  Once the z-axis, $\hat{z}$ (also referred to as the "main axis" here) and an x-axis, $\hat{x}$, have been defined, the Coulomb ionic potential can be written in terms of Spherical Harmonic functions, defined in reference to $\hat{z},\hat{x}$:
  
$$V(\mathbf{r}) = \sum_{neighbors A^\prime} \frac{Q_{A^\prime}}{|\mathbf{r}-\mathbf{r}^\prime|}=\sum_{A^\prime} \sum_{lm}Q_{A^\prime}\frac{r_<^l}{r_>^{l+1}}\left(\frac{4\pi}{2l+1}\right)Y_l^m(\theta,\phi)Y_l^{m\ast}(\theta^\prime,\phi^\prime)$$

We are interested in matrix elements between local orbitals in for small $\mathbf{r}$ (in its Voronoi cell), which expand to:
    
	$$ \lt Y ^ {p^\prime}_l(\theta,\phi) \vert V(\theta,\phi) \vert  Y ^ {p}_l(\theta,\phi) \gt = 

	\sum_{A^\prime} \sum_{km}
	  Q_{A^\prime}
	  \frac{\lt r^k \gt}{R^{\prime k+1}}\left(\frac{4\pi}{2k+1}\right)
	   \lt Y ^ {p^\prime}_l(\theta,\phi) \vert Y_k^m(\theta,\phi) \vert  Y ^ {p}_l(\theta,\phi) \gt
	  Y_k^{m\ast}(\theta^\prime,\phi^\prime)
	  $$

	The triple product of $Y_l^m$ functions is proportional to a Clebsch-Gordan coefficient, and the only terms that survives have $m = p^\prime - p$ and $0 \leq k \leq 2 l$ . So the sum can be simplfied. We replace prime indices with $A$. Note that the triple product is independent of the choice of the $\Phi$ x-axis parameter, the only implicit dependence on it is in $\phi_A$.

	$$ = \sum_{A} \sum_{0 \leq k \leq 2 l} Q_A \frac{\lt r^k \gt}{R_A^{k+1}}\left(\frac{4\pi}{2k+1}\right)
	 \lt Y ^ {p^\prime}_l \vert Y_k^{p^\prime - p} \vert  Y ^ {p}_l \gt
	Y_k^{{p^\prime - p}\ast}(\theta_A,\phi_A)
        $$
	
	Now we want to determine for which $p$ and $p^\prime$ the above is nullified due to local symmetry.

	<p>
	  Now we use the symmetry group. We can define  $C$ to be the equivalence classes of local atoms under the point-group. Since this is a group of rotations, atoms $A \in C$ will have the same $R_A$ and $Q_A$, and changing the order of summation, we get:
	  
	  
	  $$ = \sum_{C} \sum_{A \in C} \sum_{0 \leq k \leq 2 l}Q_C
	  \frac{\lt r^k \gt}{R_C^{k+1}}\left(\frac{4\pi}{2k+1}\right)
	 \lt Y ^ {p^\prime}_l \vert Y_k^{p^\prime - p} \vert  Y ^ {p}_l \gt
	  Y_k^{{p^\prime - p}\ast}(\theta_A,\phi_A) =

          \sum_{C} 4\pi Q_C \sum_{0 \leq k \leq 2 l} \frac{1}{2k+1} \frac{\lt r^k \gt}{R_C^{k+1}}
	  \lt Y ^ {p^\prime}_l \vert Y_k^{p^\prime - p} \vert  Y ^ {p}_l \gt
	  \sum_{A \in C}
	  Y_k^{{p^\prime - p}\ast}(\theta_A,\phi_A)
	  $$

	  Since the choice of $Q_C$ is arbitrary if only symmetry is considered (note that this is choice of charge per equivalence class), the inner $(k)$ sum has to be $0\, \forall C$ for the matrix element to be $0$. Now in this sum, since $R_C$ in different classes can be scaled arbitrarily without changing the symmetry, this is polynomial of order $2l+1$ in ${1\over R_C}$ with $0$ intercept, and will sum to $0$ "non accidentally" (meaning for a range or $R_c$) only if all the coefficients are $0$:
	  $$V^{C,k}_{m,m^\prime} =
	  \lt Y ^ {m^\prime}_l \vert Y_k^{m^\prime - m} \vert  Y ^ {m}_l \gt
	  \sum_{A \in C}
	  Y_k^{{m^\prime - m}\ast}(\theta_A,\phi_A) = 0 \qquad \forall C, 0 \leq k \leq 2 l$$

	  This matrix contains all the symmetry information for given $C,k$. The first factor is proportional to a product of Clebsch-Gordan or Wigner coefficients, and it is equal to zero only for certain values of $k, l$:
	  $
	   \left( \begin{array}{l}
l  & k & l \\
0 & 0 & 0
	  \end{array}    \right)
	   \left( \begin{array}{l}
l  & k & l \\
m^\prime & m - m^\prime & -m
	  \end{array}    \right)

	  $.
	  
	  Now we can restore the explicit dependence on the direction of the x-axis $\Phi$, which appears only in the sum:

	  $$
	  Y_k^{{m^\prime - m}\ast}(\theta_A,\phi_A + \Phi)$$

	  We can see that the effect of nonzero $\Phi$ is multiplication by $e^{-i (p^\prime - p) \Phi}$, which means that we cannot nullify matrix elements of the Spherical Harmonics by rotation around $\hat{z}$, as expected. 

	  <p>
	  In fact we are interested in the REAL Spherical Harmonics matrix elements, see below for our notational convention:
	  
$$M_{\pm p,\pm p^\prime}^{C,k} = \lt Y ^ {p^\prime}_l \pm Y^{-p^\prime}_l \vert V^{C,k} \vert  Y ^ {p}_l \pm Y^{-p}_l \gt$$
up to a multiplicative constant.

Due to the mixing of different phases, there may be solutions $\Phi$ to the equations $M_{\pm p^\prime,\pm p}^{C,k}(\Phi) = 0$.
	  <p>
	    Contribution from different equivalence classes $C$ and different $k$ can be analyzed separately. Note that as far the detection algorithm goes, the classification into equivalent atoms is critical: if an equivalence class is divided up into smaller sets, the structure will be deemed to have less symmetry.

	  <p>
	    It is easy to see that:
	    $$
	   \tag{1}\label{1}  V_{-m,-m^\prime} = (-1)^{m+m^\prime} V^{*}_{m,m^\prime}$$

	   so we can write:
	    
	    $$
\tag{2}\label{2}
	    M_{\pm p,\pm p^\prime}(\Phi) =

	   \left \{ \begin{array}{l}
2 \, {\rm Re} \\
2 i \, {\rm Im}
	    \end{array}    \right \}

	    \{ V_{p^\prime,p} e^{i(p-p^\prime)\Phi}  \pm V_{-p^\prime,p} e^{i(p+p^\prime)\Phi} \}

	    $$

	    where we get the 2 choices (Real, Imaginary part) multiplied by 2 choices (plus or minus) for the 4 matrix elements. The first choice depends on the parity of $p+p^\prime$ multiplied by the signs of the corresponding states  $\pm p, \pm p^\prime$ . The second cohice depends on the sign of the left state $\pm p$.
	    <p>
	    Using the symmetry within a each equivalence class $C$, we claim that if the x-axis is set to be the projection of one of the ligands in $C$:
	    $$ \sum_{A \in C} Y_k^{{m^\prime - m}\ast}(\theta_A,\phi_A) \in \mathbb{R} $$
	    This is clearly true for rotational symmetry around the axis: the phases add up geometrically, and either sum up to a real number (integer multiplied by a Legendre polynomial) or $0$, and it can be shown also for the non-rotaional groups.
	    
	  <p>
	    Therefore we can assume $V_{m,m^\prime}$ is real if the x-axis is aligned with a ligand (projection).
	  <p>


	    Now we want to find angles $\Phi$ to make as many elements $\eqref{2}$ equal to $0$. While equations with $p \neq p^\prime$ and $V_{p^\prime,p}, V_{-p^\prime,p} \neq 0$ have non-trivial solutions, if we demand that at least pairs of equations for $p \neq p^\prime$ are satisfied simultaneously the set of possible solutions is reduced to the finite set of special directions:
	    
	    $$ \tag{3}\label{3} \Phi \in \{ {\pi f \over 2 q }  \vert f \in \mathbb{Z},  1 \leq q \leq 2l \}  $$

	    Clearly in general not all 4 equations can be satisfied simultaneously, unless one of $V_{p^\prime,p}, V_{-p^\prime,p}$ is $0$ (this is actually the typical case in $d$ orbitals, if the symmmetry is higher than $1$, but not in $f$ orbitals).
	  <p>
	    It is easy to see that the set of special directions for the special cases $p = 0$ or $p^\prime = 0$, where there are only 2 elements, is contained in $\eqref{3}$ as well.
	  <p>
	    As a conclusion, our algorithm can calculate the matrix $\eqref{2}$ for the finite set of $\Phi$ - either at the ligand projections, or at one of the special angles $\eqref{3}$ relative to it, and makes the optimal choice.
	    
  <h2></h2>


  <h2> Implementation Details </h2>

  <h3> Choice of Neighbors </h3>
  One of the inputs of the algorithm is the maximum nearest neighbor distance (in Angstrem), with a default value of 3.0.
  <p> 
  The reason why we need to have such an input parameter, is that the choice of radius for nearest neighbor affects the way the local environment looks. If the radius is set large enough, all the local neighbors will be preset, which makes the most faithful representation of the local environment. However this choice may result in a smaller symmetry group, which may lead to more matrix elements being non-zero. In other words, sometimes a choice of a smaller radius leads to a larger symmetry group, which simplifies the problem.
  <p>
   Note that non-nearest neighbors are never included in the local environment, even if the radius is set to include them. We remove them using a constructed Voronoi diagram.
 
  
   <h3> Determination of the Point Group </h3>
   Do determine the point group of the local "molecule" we use PyMatGen's algorithm, where we only tune the tolerance numbers. Note that the local environment can have point symmetry that is not crystalline. We only know that the crystal's non-translational subgroup is a subgroup of the local molecule's point group.

   <h3> Determination of the optimal Z axis </h3>
   Here we examine the local group that was determined, and find either its rotation axis (for rotational groups) or a mirror axis. This can be shown (analyzing case-by-case) to be the best choice for $\hat{z}$, except for the tetrahedral group. For this group the best choice is a bisector between 2 ligands (which are all equivalent).

<h3> Determination of the optimal X axis </h3>

For Complex Spherical Harmonics basis, and for the Relativistic basis, this choice cannot eliminate matrix elements, because the effect of rotation around $z$ is just multiplication of elements by a phase (see below).
  <p>
Only the Real Spherical Harmonics basis is more complicated, because it has special directions, as we have shown before. In this case we try all the special directions (which is a small finite set) and determine the optimal one.

<h3> Real Spherical Harmonics basis </h3>
  We write the real spherical harmonics basis in a non-conventional order, and use the letter $p$ to enumerate it, to distinguish it from the conventional enumeration by $m$.
  <p>
In our notation $$p \ge 0$$ and there is a second symbol to describe the state: $-$ or $+$. Therefore:


$$ \vert +p \gt =
\left\{
\begin{array}{l}
i  & \text{$p$ odd} \\
1 & \text{$p$ even}
\end{array}
\right\}
{1 \over \sqrt{2}} ( Y ^ {p}_l + Y^{-p}_l)
$$


$$ \vert -p \gt =
\left\{
\begin{array}{l}
1  & \text{$p$ odd} \\
i & \text{$p$ even}
\end{array}
\right\}
{1 \over \sqrt{2}} ( Y ^ {p}_l - Y^{-p}_l)
$$

$$ \vert 0 \gt  = Y^0_l =  {1 \over \sqrt{2}} \vert +0 \gt $$

<h3> Spherical Harmonic basis </h3>
In this basis, as we showed above, only the choice of the $\hat{z}$ direction matters, because rotating the $\hat{x}$ cannot reduce the number of non-zero elements.

<h3> Relativistic basis </h3>
This is the basis $\vert \! J,m_J \gt$ where $J \in \{L+{1 \over 2}, L-{1 \over 2} \}$. Just as in the case of Spherical Harmonic basis, no choice of $\hat{x}$ can reduce the number of non-zero elements. This is easy to see, because the matrix elements are linear combinations of 2 terms, but both term multiply by the same phase:

$$
\vert \! J=L \pm \frac12 ,m_J \! \gt = \lt \! J\! - \! \frac12, m_J \! - \! \frac12 \! \otimes \uparrow \vert L \pm \frac12 ,m_J \!  \gt  \vert J \! - \! \frac12 ,m_J \! - \! \frac12 \! \gt \otimes \uparrow +
\lt \! J\! + \! \frac12, m_J \! + \! \frac12 \! \otimes \downarrow \vert L \pm \frac12 ,m_J \!  \gt  \vert J \! + \! \frac12 ,m_J \! + \! \frac12 \! \gt \otimes \downarrow =
$$
$$
 C_{-}^{J,m_J} \vert J \! - \! \frac12 .m_J \! - \! \frac12 \! \gt \otimes \uparrow + C_{+}^{J , m_J}  \vert J \! + \! \frac12 ,m_J \! + \! \frac12 \! \gt \otimes \downarrow
$$

Since $Y^k_l$ are don't affect spin, only 2 terms survive: up and up, down and down, and they have the same $\Phi$-dependent phase because

  $ \lt \! J^\prime, p^\prime_J  \vert V^{C,k}(\theta,\phi) \vert J, p_J \! \gt = \\
\{
C_{-}^{J^\prime,p_J^\prime *} C_{-}^{J,p_J}
 \lt \! J^\prime \! - \! \frac12 , p_J^\prime \! - \! \frac12  \vert Y^{p^\prime - p}_k \vert J \! - \! \frac12, p_J \! - \! \frac12 \! \gt
+
C_{+}^{J^\prime,p_J^\prime *} C_{+}^{J,p_J}
 \lt \! J^\prime \! + \! \frac12 , p_J^\prime \! + \! \frac12  \vert Y^{p^\prime - p}_k \vert J \! + \! \frac12, p_J \! + \! \frac12 \! \gt
\}

	  \sum_{A \in C}
	  Y_k^{{p^\prime - p}\ast}(\theta_A,\phi_A + \Phi)
$

<h3> Computing the SigInd matrices </h3>

Since we can compute matrix elements for all $C,k$, to find SigInd we just have to transform these matrices into "sets" of equivalent elements (using a tolerance), interset these sets for all $C,k$, and then assign numbering to each one of the sets.

<h2> Reference Crystal Fields For One Atomic Element </h2>

Download from wikipedia

</body>
</html>


