'''
Created on Aug 12, 2016

@author: adler@physics.rutgers.edu

'''
from pymatgen.core.operations import SymmOp
from pymatgen.core.structure import Molecule
from pymatgen.symmetry.analyzer import PointGroupAnalyzer, SpacegroupAnalyzer
from math import atan2, sqrt
from numpy import array, float64, cross, dot, arccos, conjugate, complex128, sign, matrix
from numpy.linalg import norm
import numpy as np
from scipy.special import sph_harm
from cmath import pi
from sympy.physics.wigner import clebsch_gordan
from tabulate import tabulate
from scipy.cluster.hierarchy import fclusterdata
from itertools import combinations_with_replacement
from collections import defaultdict, OrderedDict
from enum import Enum
from numpy.linalg import eig
from itertools import product, combinations
import itertools

class BasisType(Enum):
    """ 
    """
    
    ## just the garden-variaty Spherical Harmonics
    Lm = 0,
    
    ## Real and Imaginary parts of Spherical Harmonics
    RealSphericalHarmonics = 1,  
    
    ## Tensor product of Spherical Harmonics and spin (up or down)
    Lm_S = 2,                    
    
    ## Relativistic \f$\vert \! J,m \! \gt \f$ basis
    Jm  = 3                         


class LocalMolecule(Molecule):
 
    def __init__(self, species, coords, charge=0,
                 spin_multiplicity=None, validate_proximity=False,
                 site_properties=None, center_site=None, neighbors=None):
        if not center_site:
            super(LocalMolecule,self).__init__(species, coords, charge, spin_multiplicity, validate_proximity,site_properties
                                              )
        else:
            species = [center_site.specie]
            coords = [(0.0,0.0,0.0)]
            for site in neighbors:
                species.append(site.specie)
                coords.append(site.coords - center_site.coords)
            super(LocalMolecule,self).__init__(species, coords)
            self.point_group = None
            self.equivalent_sites = None
    @property
    def center_of_mass(self):
        #print "getting center of mass"
        return (0.0,0.0,0.0)
 
 
    @classmethod
    def are_symmetrically_equivalent(cls, point_group, site, site2, symm_prec=1e-1):
        if site.species_and_occu != site2.species_and_occu:
            return False
        for op in point_group:
            image = op.operate(site.coords)
            # TODO: why is this tolerance so bad? This is the same number as in the group-element generation
            #       my guess is that the group elements are not normalized correctly
            if site2.distance_from_point(image) < symm_prec:
                return True
        return False
    
    def set_point_group(self, point_group):
        self.point_group = point_group
        # TODO: invert the algorithm by generating equivalent sites, this impl. is inefficient for LaVO3.struct for example
        equiv = []
        for site in self.sites:
            if np.linalg.norm(np.array(site.coords) - self.center_of_mass) < 1e-6:
                continue
            found = False
            for eqclass in equiv:
                if LocalMolecule.are_symmetrically_equivalent(self.point_group, eqclass[0],site):
                    eqclass.append(site)
                    found = True 
            if not found:
                equiv.append([site])
        self.equivalent_sites = equiv
    
    @property
    def get_equivalent_sites(self):
        return self.equivalent_sites
    
class MaxDistanceError(Exception):
    None

class IndMatrix(object):
    def __init__(self, dim, debug, tolerance=0.001, only_diagonal=False):
        self.dim = dim
        self.debug = debug
        self.Ind = [[0 for _ in range(dim)] for __ in range(dim)]
        self.indices = [[i,j] for i,j in combinations_with_replacement(list(range(dim)),2)]
        self.tolerance = tolerance
        self.only_diagonal = only_diagonal
        # only Ind[i1][i2] where i1<=i2 will be relevant
        # the rest will stay as 0
        self.NN = [[0 for _ in range(dim)] for __ in range(dim)]
        for i in range(0, self.dim):
            self.NN[i][i] = i+1
        index = self.dim
        for i,j in combinations_with_replacement(list(range(self.dim)),2):
            if i != j:
                index += 1
                self.NN[i][j] = index
                
    def NumNonzero(self):
        sigind = self.SigInd #TODO: remove multiple calculations of SigInd
        m = 0
        for i in range(self.dim):
            for j in range(self.dim):
                m = max(m, sigind[i][j])
        return m
        
    def Numbering(self, i, j):
        # number the diagonal first 
        return self.NN[i][j]
        
    def Cluster(self, M):
        X = [(M[i][j].real, M[i][j].imag) for i,j in combinations_with_replacement(list(range(self.dim)),2)]
        res = fclusterdata(X, self.tolerance, criterion='distance')
        # We assume indices in res are 1, 2, ...
        cluster = [[0 for _ in range(self.dim)] for __ in range(self.dim)]
        for i, r in enumerate(res):
            pair = self.indices[i]
            cluster[pair[0]][pair[1]] = r
        if self.debug:
            print(tabulate(cluster, tablefmt="simple"))
        return cluster
                                
        
    def SplitInd(self, M):
        # first cluster the values in the 
        cluster = self.Cluster(M)
        lowest_numbering = defaultdict(lambda: self.dim * self.dim + 1)
        new_ind = [[(0, self.Ind[i][j]) for i in range(self.dim)] for j in range(self.dim)]
        for i,j in combinations_with_replacement(list(range(self.dim)),2):
            ci = (cluster[i][j], self.Ind[i][j])
            new_ind[i][j] = ci
            #print "lnci=", lowest_numbering[ci]
            lowest_numbering[ci] = min(lowest_numbering[ci], self.Numbering(i,j))
        # Convert new_ind index classes to the lowest Numbering in the class
        for i,j in combinations_with_replacement(list(range(self.dim)),2):
            actual_ind = lowest_numbering[new_ind[i][j]]
            # one special indicator is the 0 one: it's just an "and" of 0 values
            if abs(M[i][j])<self.tolerance and self.Ind[i][j] == 0:
                actual_ind = 0
            self.Ind[i][j] = actual_ind
            self.Ind[j][i] = actual_ind
            
    def AddShortLabel(self, RSH_labels, label, i, j):
        if len(label):
            label += ","
        if i==j:
            label += RSH_labels[i]
        else:
            label += RSH_labels[i] + "|" + RSH_labels[j]
        return label
    
    def GetLabels(self, RSH_labels):
        # first join labels for entries in the same class
        unique_labels = defaultdict(str)
        for i,j in combinations_with_replacement(list(range(self.dim)),2):
            category = self.Ind[i][j]
            unique_labels[category] = self.AddShortLabel(RSH_labels,unique_labels[category], i,j)
        # shorten off-diagonal labels that are too long in unique way
        if not self.only_diagonal:
            for key, val in list(unique_labels.items()):
                if len(val.split("|")) > 2:
                    unique_labels[key] = 'H' + str(key)
        # now write labels in proper order
        return list(OrderedDict(unique_labels).values())[1:]  # do not return the 0 label
           
    @property
    
    def SigInd(self):
        dd = defaultdict(lambda: len(list(dd.keys())) + 1)
        # sigind contains the numbers compressed so that there would be no jumps, as DMFT expects it
        sigind = [[0 for _ in range(self.dim)] for __ in range(self.dim)]
        for i in range(0, self.dim):
            if self.Ind[i][i] == 0:
                continue
            sigind[i][i] = dd[self.Ind[i][i]]
        if self.only_diagonal:
            return sigind
        for i,j in combinations_with_replacement(list(range(self.dim)),2):
            if i == j or self.Ind[i][j] == 0:
                continue
            sigind[i][j] = dd[self.Ind[i][j]]
            sigind[j][i] = sigind[i][j]
        return sigind
      

class GeometricFactors(object): 
    """
    Calculates the sum
    \f$ \sum_{A \in C} Y_k^{{m^\prime - m}\ast}(\theta_A,\phi_A) \f$ for:
    - all \f$ C, 0 \leq k \leq 2L \f$ in the non-spin-polarized cases
    - all \f$ C, 0 \leq k \leq 2L+1 \f$ in the spin-polarized basis
    [see local_symmetry.html](../../matdelab/core/local_symmetry.html) 
    """
    def __init__(self, J, dim, mol, xhat, yhat, basis_type):
        # for each equivalence class, one matrix. rows: dm, columns: k square matrix size 2*J+1
        self.GF = array([[[0.0+0.0j for _ in range(self.dim)] for __ in range(self.dim)] for ___ in range(len(equivalent_sites))], complex128)
        self.mol = mol
        self.xhat = xhat
        self.yhat = yhat
        self.basis_type = basis_type
        
        
class SHMatrices(object):
    """
    
    """
    None

class MyAnalyzer(PointGroupAnalyzer):
       def _analyze(self):
        if len(self.centered_mol) == 1:
            self.sch_symbol = "Kh"
        else:
            inertia_tensor = np.zeros((3, 3))
            total_inertia = 0
            for site in self.centered_mol:
                c = site.coords
                wt = sqrt(sqrt(site.species_and_occu.weight)) # adler: this works better than the pure weight, when the atomic numbers are bigger
                for i in range(3):
                    inertia_tensor[i, i] += wt * (c[(i + 1) % 3] ** 2
                                                  + c[(i + 2) % 3] ** 2)
                for i, j in [(0, 1), (1, 2), (0, 2)]:
                    inertia_tensor[i, j] += -wt * c[i] * c[j]
                    inertia_tensor[j, i] += -wt * c[j] * c[i]
                total_inertia += wt * np.dot(c, c)

            # Normalize the inertia tensor so that it does not scale with size
            # of the system.  This mitigates the problem of choosing a proper
            # comparison tolerance for the eigenvalues.
            inertia_tensor /= total_inertia
            eigvals, eigvecs = np.linalg.eig(inertia_tensor)
            self.principal_axes = eigvecs.T
            self.eigvals = eigvals
            v1, v2, v3 = eigvals
            eig_zero = abs(v1 * v2 * v3) < self.eig_tol ** 3
            eig_all_same = abs(v1 - v2) < self.eig_tol and abs(
                v1 - v3) < self.eig_tol
            eig_all_diff = abs(v1 - v2) > self.eig_tol and abs(
                v1 - v3) > self.eig_tol and abs(v2 - v3) > self.eig_tol

            self.rot_sym = []
            self.symmops = [SymmOp(np.eye(4))]
            if eig_zero:
                self._proc_linear()
            elif eig_all_same:
                self._proc_sph_top()
            elif eig_all_diff:
                self._proc_asym_top()
            else:
                self._proc_sym_top()
                



class LocalAnalysis(object):
    '''
    Calculates the local rotation's new z and x axes, and prepares the SigInd matrix for the given basis, quantized
    in the local coordinate system.
    '''
    def __init__(self, pmg_struct, site, nn_radius, orbital_letter=None, weighted_vd=False,
                 only_diagonal=False,
                 rotation_angle=0.0, debug=False, from_indmfl=None):
        self.J = LocalAnalysis.LofOrbital(orbital_letter) 
        self.dim = 2*self.J + 1
        self.pmg_struct = pmg_struct
        self.site = site
        self.orbital_letter = orbital_letter
        self.debug = debug
        self.only_diagonal=only_diagonal
        
        # Calculate vd_set, voro, site_neighbors
        # voro, vd_set are used by graphics to illustrate this object
        self.vd_set = pmg_struct.get_sites_in_sphere(site.coords, nn_radius)
        if len(self.vd_set) <=1:
            raise MaxDistanceError("option max_distance {0} is too small to find any neighbors for site {1}".format(nn_radius, str(site)))

        self.vd_set = [i[0] for i in sorted(self.vd_set, key=lambda s: s[1])]
        if weighted_vd:
            qvoronoi_input = [LocalAnalysis.scaled_coords(s, site) for s in self.vd_set]
        else: 
            qvoronoi_input = [s.coords for s in self.vd_set]
        self.voro = VoronoiTess(qvoronoi_input)
        self.site_neighbors = []
        for nn, vind in list(self.voro.ridges.items()):
            if 0 in nn:
                if 0 in vind:
                    raise RuntimeError("Structure is pathological, inf. feature in the Voroni")
                self.site_neighbors.append(self.vd_set[nn[1]])
                
        # sets main_axis
        self.mol = LocalMolecule(None, None, center_site=self.site, neighbors=self.site_neighbors)
        # TODO: we could change this to FixedPointGroupAnalyzer if the implementation changes     
        pga = MyAnalyzer(self.mol,
                         #tolerance=0.05,       # in angstrem. This one is used in absolute terms sometimes ..
                         tolerance=0.1, # this causes PBE with r=2.5 to classify as tetra
                                        #TODO: make this a parameter of the program. This is related to 
                                        # how accurate the symmetry in the original input is.
                                        # Inputs like LaVO3 fail if this is smaller than 0.1
                         eigen_tolerance=0.05, # to determine axes equivalence (absolute)
                         matrix_tol=0.3)  #0.3 # this one goes only to group element generation (absolute)
        self.pga = pga
        # symmmetrize the molecule
        self.mol.set_point_group(pga.get_pointgroup())  
        
        if from_indmfl:
            ai = pmg_struct.sites.index(site) + 1    
            locrot, new_zx, vec_shift, Rmt2 = from_indmfl.atoms[ai]
            self.x_vec = self.Normalized((1.0,0.0,0.0))
            if locrot == 0:
                self.main_axis = self.Normalized((0.0,0.0,1.0))
            else:
                self.main_axis =  self.Normalized(pmg_struct.lattice.get_cartesian_coords(new_zx[0]))
            if locrot == 2:
                self.x_vec =  self.Normalized(pmg_struct.lattice.get_cartesian_coords(new_zx[1]))
            return
        
                
        self.equivalent_sites = self.mol.get_equivalent_sites
        
        def appendIfNotParallel(vectors, new_vec):
            parallel = False
            for ev in vectors:
                dp = np.dot(conjugate(ev), new_vec)
                if ((abs(dp - 1.0) < 1.0e-13) or (abs(dp + 1.0) < 1.0e-13)):
                    #print "dropping", new_vec, dp
                    parallel = True
                    break
            if not parallel:
                for i in range(3):
                    if np.imag(new_vec[i]) < 1.0e-10:
                        new_vec[i] = np.real(new_vec[i])
                print("adding", new_vec)
                vectors.append(new_vec)
    
        def same(w, ind1, ind2):
            return abs(w[ind1]-w[ind2]) < 1.0e-13
        
        
        evs = []
        appendIfNotParallel(evs, self.Normalized((1.0,0.0,0.0)))
        appendIfNotParallel(evs, self.Normalized((0.0,1.0,0.0)))
        appendIfNotParallel(evs, self.Normalized((0.0,0.0,1.0)))
        for v in pga.principal_axes:
            appendIfNotParallel(evs, self.Normalized(v))
    
        print("local group:", pga.sch_symbol, "operations:", len(pga.get_pointgroup()))
                  
        print("operations:", [x for x in pga.get_pointgroup()])    
                
        #cross_prods = []
        #for a,b in itertools.product(evs,evs):
        #    appendIfNotParallel(cross_prods, cross(a,b))
        #for v in cross_prods:
        #    appendIfNotParallel(evs, v)
                
        #sga = SpacegroupAnalyzer(pmg_struct)
        #matrices = [x.rotation_matrix for x in sga.get_point_group_operations()]
        
        #print "point group is:", sga.get_point_group_operations()
        
        
        matrices =  [x.rotation_matrix for x in pga.get_pointgroup()]    
        
        print("num of matrices:",len(matrices))
        for m in matrices:
            w, v = eig(m)
            #print "v,w=\n", v, w
            if (not same(w,0,1) and not same(w,1,2) and not same(w,0,2)):
                # if all eigenvalues are different, one has to be real, and the others are conjugates of each other
                # add the real one, and add the others only if they are real.
                for i in range(3):
                    if abs(np.imag(w[i])) < 1.0e-10:
                        appendIfNotParallel(evs, array(v[:,i]))
                    
            elif (same(w,0,1) and same(w,1,2) and same(w,0,2)):
                # if all eigenvalues are the same, we need not add anything, since x,y,z will be added outside.
                None
            else:
                # if we have a degenerate subspace of dim 2, then all eigenvalues are real.
                # we need to add only the normal, as well as the eigenvectors in the plane
                # these are axes of high symmetry
                for i in range(3):
                    if abs(np.imag(w[i])) < 1.0e-20:
                        # this is abnormal -> sometimes values here are complex with very small imaginary
                        appendIfNotParallel(evs, array(v[:,i]))                        
   
        print("candidate axes:", len(evs), ':', evs)
        adps = [[0.0 for ev1 in evs] for ev2 in evs]
        for i1,i2 in product(list(range(0,len(evs))), repeat=2):
            adps[i1][i2] = abs(np.dot(conjugate(evs[i1]),evs[i2]))
        
        all_bases = []
        # find all orthogonal triplets
        for i1,i2 in combinations(list(range(0,len(evs))), r=2):
            if adps[i1][i2] < 1.0e-10:
                z = cross(evs[i1],evs[i2])
                all_bases.append([evs[i1],evs[i2],z])
                all_bases.append([z,evs[i1],evs[i2]])
                all_bases.append([evs[i2],z,evs[i1]])
    
        def IsBetterBasis(newb, oldb):
            if not oldb or abs(newb[2][2]) > abs(oldb[2][2]):
                return True
            return False
    
        #print "we have", len(all_bases), "candidates for bases", all_bases
        best_basis = None
        num = self.dim * self.dim + 1
        for basis in all_bases:
            self.x_vec, self.y_vec, self.main_axis = basis
            self.debug = False
            self.CalculateRep()
            n = self.ind.NumNonzero()
            if ((n < num) or ((n == num) and IsBetterBasis(basis, best_basis))): # or they are equal, and the basis is better
                if self.debug: print("num, n", num,n, IsBetterBasis(basis, best_basis))
                num = n
                best_ind = self.ind
                best_basis = basis
        
#         
#         nonzero = []
#         for s in self.mol.sites:
#             if norm(s.coords) > 1.0e-6:
#                 nonzero.append(self.Normalized(s.coords))
#         self.main_axis = nonzero[0]
#         self.x_vec = self.Normalized(nonzero[1]- dot(nonzero[0], nonzero[1])*nonzero[0])
#         self.y_vec = np.cross(self.main_axis, self.x_vec)
#                
#             
#         x = self.x_vec
#         self.x_vec = self.main_axis 
#         self.main_axis = x
#             
        self.x_vec, self.y_vec, self.main_axis = best_basis
        
        # rotate x_vec by some angle if specifically specified (this is more for debugging)
        if rotation_angle != 0.0:
            rot_op  = SymmOp.from_axis_angle_and_translation(self.main_axis, rotation_angle)
            self.x_vec = self.Normalized(rot_op.operate(self.x_vec))
            self.y_vec = self.Normalized(rot_op.operate(self.y_vec))
#             

        self.CalculateRep(final=True)
        
        
        return
    
    
    def SHPrefactor(self, m):
        if m == 0:
            return 1
        s2 = 1/sqrt(2)
        if m > 0:
            if m %2 ==0:
                return s2
            return s2 *1j
        else:
            if m %2 == 1:
                return -s2
            return -s2 *1j
        
    def RSHName(self, p):
        if self.J == 0:
            return "s"
        elif self.J == 1:
            return ["x","z","y"][p+self.J]
        elif self.J == 2:
            return ["xy","xz","z^2","yz","x^2-y^2"][p+self.J]
        elif self.J == 3:
            return "f"+str(p) #TODO: implement
        return ""
        
     
    def get_header(self, list_of_real_sh):
        return [self.RSHName(i) for i in list_of_real_sh]
     
    def RSHLabels(self):
        return self.get_header([i-self.J for i in range(self.dim)])
    def SHLabels(self):
        return [str(l) for l in range(-self.dim, self.dim+1)]
    def DLabels(self):
        return [str(i) for i in range(self.dim)]
     
    def print_matrix(self, m, labels):
        str_matrix = [[str(x) for x in row] for row in m]
        print(tabulate(str_matrix, tablefmt="simple", headers=labels))
     
    @staticmethod
    def snap(angle):
        angle_tolerance = 0.2 #in radians - it's 5.7 degrees, this is a parameter to increase for idealization
        for n in range(1,200):
            pin = 2*pi / n
            for m in range(2*n):
                if abs(angle - m*pin) < angle_tolerance:
                    return m*pin
                if abs(angle + m*pin) < angle_tolerance:
                    return -m*pin
        return angle  
           
           
    # theta is angle with Z, phi is angle with x
    def cartesian_to_spherical(self, coords, phi_offset=0.0):   
        unit = self.Normalized(coords)
        z = dot(unit,self.main_axis)
        theta = arccos(z) # 0 to pi
        if np.isnan(theta):
            theta = 0.0 # at origin
        y = dot(self.y_vec, unit)
        x = dot(self.x_vec, unit)
        phi = atan2(y, x) + pi
        theta = self.snap(theta)
        if phi_offset != 0.0:
            phi = self.snap(phi - phi_offset)
        if self.debug:
            print("theta=", theta/pi, "phi=",phi/pi , "x,y=", x, y, " phi_offset", phi_offset/pi)
            print("     x=", x , " y=",y,  "z=", z)
            print("     unit=", unit)
        
        # TODO: make the theta more accurate, by returning -z, +z in one equivalence
        #       to fix the rotation dependence in example  (it shouldn't depend on rotation)
        #       pvesta ~/structs/NaFeAs.struct -sFe1 -v -r 6
        
        # Return the spherical coordinate vector.
        return [theta, phi]
       
    @classmethod
    def Normalized(cls, vec):
        nvec = array(vec, float64)
        return nvec / norm(nvec)
          
        
    @staticmethod
    def IsParallel(vec1, vec2):
        return norm(cross(LocalAnalysis.Normalized(vec1), LocalAnalysis.Normalized(vec2))) <= 1e-5
    
    @staticmethod
    def scaled_coords(site, center):
        return 2.0*(site.coords - center.coords) * center.specie.Z / (site.specie.Z+center.specie.Z) + center.coords
        
    @property
    def VoronoiDiagram(self):
        return self.voro
    
    @property
    def VoronoiDiagramSet(self):
        return self.vd_set
    
    
    @property
    def Neighbors(self):
        return self.site_neighbors
        
    @staticmethod
    def LofOrbital(letter):
        if letter == 's':
            return 0
        if letter == 'p':
            return 1
        if letter == 'd':
            return 2
        if letter == 'f':
            return 3
        # no local orbital analysis
        return 0

    @property
    def FracXAxis(self):
        return self.pmg_struct.lattice.get_fractional_coords(self.x_vec)
    
    @property
    def FracZAxis(self):
        return self.pmg_struct.lattice.get_fractional_coords(self.main_axis)
    
    @property
    def XAxis(self):
        return self.x_vec
    
    @property
    def ZAxis(self):
        return self.main_axis
    
    @property
    # return the basis transformation between RSH and SH
    def Transformation(self):
        T = [[0.0+0.0j for _ in range(self.dim)] for __ in range(self.dim)]
        for r in range(self.dim):
            m = r-self.J
            if m == 0:
                T[r][self.J] = 1.0
            else:
                pf = self.SHPrefactor(m)
                s = sign(m)
                T[r][2*self.J - r] = pf
                T[r][r] = s * pf
        return T
    
    @property
    def SigInd(self):
        return self.ind.SigInd
    
    @property
    def BasisLegendText(self):
        return self.ind.GetLabels(self.RSHLabels())
    
    @property
    def PrincipalAxes(self):  
        return self.pga.principal_axes
        
          
    def CalculateRep(self, final=False):
        
        # now analyze local matrix
        # for each equivalence class, one matrix. rows: dm, columns: k square matrix size 2*J+1
        FF = array([[[0.0+0.0j for _ in range(self.dim)] for __ in range(self.dim)] for ___ in range(len(self.equivalent_sites))], complex128)
        for ieq, eq in enumerate(self.equivalent_sites):
           # _, first_phi = self.cartesian_to_spherical(eq[0].coords, phi_offset=0.0)
            for k in range(self.dim):
                for dm in range(self.dim):
                    if dm > k:
                        continue  
                    for site in eq:
                        # Note: theta and phi are actually the opposite in scipy
                        theta, phi = self.cartesian_to_spherical(site.coords, phi_offset=0.0)
                        FF[ieq][k][dm] += conjugate(sph_harm(dm, k, phi, theta)) 
                        #print "sph_harm[", dm,k,theta, phi, "]=", sph_harm(dm, k, theta, phi)
                #for dm in range(self.dim):
                    #print "FF[", ieq, k, "]", FF[ieq][k][dm]
                    #if abs(FF[ieq][k][dm].imag) > 1e-6:
                    #    print "Symmetry detection problem: IMAGINARY FF",FF[ieq][k][dm].imag
                    #FF[ieq][k][dm] *= exp(-first_phi * dm* 1j)
    
        #for ieq, F in enumerate(FF): 
        #    for r1 in range(self.dim):
        #            for r2 in range(self.dim):
        #                nc = 0.0+0.0j
        #                if abs(F[r1][r2].real) >= 0.06:
        #                    nc += F[r1][r2].real
        #                if abs(F[r1][r2].imag) >= 0.06:
        #                    nc += F[r1][r2].imag * 1j
        #                F[r1][r2] = nc
        
        MM = [[[[0.0+0.0j for _ in range(self.dim)] for __ in range(self.dim)] for k in range(self.dim)]for ieq in FF]
        for ieq,eq in enumerate(FF):
            for k in range(self.dim):
                for r1 in range(self.dim):
                    for r2 in range(self.dim):
                        m1 = r1 - self.J
                        m2 = r2 - self.J
                        me = FF[ieq][k][abs(m1-m2)] * float(clebsch_gordan(self.J, k, self.J, m1, m2-m1, m2)) * \
                                                      float(clebsch_gordan(self.J, k, self.J, 0,0,0))
                        if m1 > m2:
                            MM[ieq][k][r1][r2] = me
                        else:
                            MM[ieq][k][r1][r2] = conjugate(me)
                            # Y_{-m} = {-1}^m Y_{m} 
                            if (m1-m2)%2 == 1:
                                MM[ieq][k][r1][r2] = -MM[ieq][k][r1][r2]
                            
        if final and self.debug:
            for ieq,eq in enumerate(FF):
                print("FF matrix for equivalence: ", ieq+1, self.equivalent_sites[ieq][0].coords, ieq, self.equivalent_sites[ieq][0].specie)
                print() 
                self.print_matrix(FF[ieq], self.DLabels())
                for k in range(self.dim):
                    print("V(r) in SH basis, k=",k)
                    self.print_matrix(MM[ieq][k], self.DLabels())
            
        ind_matrix = IndMatrix(self.dim, debug=self.debug, tolerance=1e-2, only_diagonal=self.only_diagonal)
        
        # transform FF matrices from spherical to real-spherical harmonics
        
        # handle the case where there are no special angles, which is the typical case.
        # Here we use the partial sums just to calculate if matrix elements are 0 or not 
        # TODO: use the partial sums to calculate the crystal fields symmetry, multiplying by <r^k>, and Q_j
        for ieq, _ in enumerate(FF):
            for k in range(self.dim): 
                S = [[0.0+1j*0.0 for _ in range(self.dim)] for __ in range(self.dim)]
                H = MM[ieq][k]
                for r1 in range(self.dim):
                    for r2 in range(self.dim):
                        m1 = r1 - self.J
                        m2 = r2 - self.J
                        min_m1 = 2*self.J - r1
                        min_m2 = 2*self.J - r2
                        if m1 == 0 and m2 == 0:
                            E = H[r1][r2]
                        elif m1 == 0:
                            if m2 > 0:
                                E = H[r1][r2] + H[r1][min_m2]
                            elif m2 < 0:
                                E = H[r1][r2] - H[r1][min_m2] 
                        elif m2 == 0:
                            if m1 > 0:
                                E = H[r1][r2] + H[min_m1][r2]
                            elif m1 < 0:
                                E = H[r1][r2] - H[min_m1][r2]
                        else: 
                            t1 = H[r1][r2]
                            t2 = H[min_m1][min_m2]
                            
                            # for debug
                            sgn = 1.0
                            if (m1 + m2)%2 != 0:
                                sgn = -1.0 
                                
                            if abs(t1.conjugate() - sgn*t2) >= 1e-6:
                                print("BUG: t1,t2", t1,t2, "k=", k)
                                self.print_matrix(H, self.DLabels())
                            
                            s1 = H[min_m1][r2]
                            s2 = H[r1][min_m2]
                            if abs(s1.conjugate() - sgn* s2) >= 1e-6:
                                print("BUG: s1,s2", s1,s2, "k=", k)
                                self.print_matrix(H, self.DLabels())
                                
                            if m1*m2 > 0:
                                E1 = t1 + t2
                                E2 = s1 + s2 
                            else:
                                E1 = t1 - t2
                                E2 = s1 - s2
                                
                            #print "E1,E2=",E1,E2
                            if r1 != r2 and abs(E1) > 1e-6 and abs(E2) > 1e-6:
                                #print "E1,E2",E1,E2, "at ieq=", ieq, "k=", k, "r1,r2=", r1,r2, "t1,t2,s1,s2=",t1,t2,s1,s2
                                None
                            if m1 > 0:
                                E = E1+E2
                            else:
                                E = E1-E2        
                        E = E * conjugate(self.SHPrefactor(m1)) * self.SHPrefactor(m2)
                            
                        if abs(E) >= 0.06 : # or ind2 != 0.0:
                            S[r1][r2] = E
                if final and self.debug:
                    print("RSH k=",k,"eq. class:",ieq+1)
                    self.print_matrix(S, self.RSHLabels())
                    print("diagonal:")
                    print(np.linalg.eigh(S)[0])
                    
                ind_matrix.SplitInd(S)
                
        self.ind = ind_matrix
        if final:
            self.print_matrix(self.ind.SigInd, self.RSHLabels())
            if self.debug:
                print("Transformation is:")
                self.print_matrix(self.Transformation, self.SHLabels())
                # this should produce the unit matrix. TODO: add exception if not unity
                T = matrix(self.Transformation)
                I = dot(T.H,  T)
                print("This one supposed to be I:")
                self.print_matrix(I.A, self.SHLabels())
            
            
