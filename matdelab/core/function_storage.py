#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
'''
Created on Oct 14, 2016

@author: adler@physics.rutgers.edu
'''



from pandas.core.frame import DataFrame

import numpy as np
from cmath import pi
import glob
import re
from builtins import set
from os import path

# need superclass interface for FunctionInText
# FunctionInText is actually DataFrame-based

class FunctionInText(object):
    def __init__(self, project, prefix, suffix, name, versions=[""]):
        self.project = project
        self.prefix = prefix
        self.suffix = suffix
        self.name = name
        self.versions = versions
        self.skip_header = 0
        self.df = None
        self.x_column = self.XVariableName()
        self.merge_equiv_atoms = False
    
    # never versioned
    def XVariableName(self):
        return self.x_column
    
    # BEFORE versioning, should return the variable names found in the given file
    # This may be known just by knowing the class (as in DMFT files), or may 
    # be read from the header of the file in some way
    def VariableNames(self, file_name):
        return None

    def combine_suffix(self, version):
        if version != "":
            return self.suffix + "." + version
        else:
            return self.suffix

    def GetNameVersionPair(self, column):
        return column.split("#")

    # designed for DF-backed implementation. 
    # df is the raw data read from a file. 
    def TransformDF(self, df):
        return df.columns

    def GetFilename(self, version):
        return self.project.Filename2(self.prefix, self.combine_suffix(version))

    def Initialize(self):
        
        # Transforms the raw df, and adds version strings to variable names
        def MergeDF(version, df):
            trans_columns = self.TransformDF(df)
            new_columns = []
            for column in trans_columns:
                if column == self.x_column:
                    new_columns.append(column)
                else:
                    new_columns.append(column + "#" + version) 
            df.columns = new_columns
            if self.df is None:
                self.df = df
            else:
                columns_to_merge = df.columns.difference(self.df.columns) | [self.x_column]
                self.df = self.df.merge(df[columns_to_merge], on=self.x_column)
            
        for version in self.versions:
            fn1 = self.GetFilename(version)
            if '*' in fn1 or  '?' in fn1:
                fns = glob.glob(fn1)
            else:
                fns = [fn1]
            for fn in fns:
                print("reading ", fn)
                renamed_columns = self.VariableNames(fn,version)
                if renamed_columns is None:
                    MergeDF(version, DataFrame(data=
                                               np.genfromtxt(fn, dtype=float,
                                                             names=True, 
                                                             skip_header=self.skip_header,
                                                             deletechars=r"#")))
                else:
                    raw_df = DataFrame(data=np.genfromtxt(fn, dtype=float, deletechars=r"#",
                                       skip_header=self.skip_header, comments=r"#"))
                    #print "raw: ", raw_df.columns
                    #print "renamed: ", renamed_columns
                    
                    # at the moment we don't have enough information to intelligently 
                    # merge the columns, we have to rely on positions 
                    # for example raw_df.columns are just ordinals, with no meta-data
                    # we should be careful about how we apply an INDMFL change operator,
                    # we are not careful now.
                    
                    raw_df.columns = renamed_columns[0:len(raw_df.columns)]
                    #print "raw columns:", raw_df.columns
                    #print "columns:", renamed_columns
                    for column in renamed_columns[len(raw_df.columns):len(renamed_columns)]:
                        #print "Creating new column " + column
                        raw_df[column] = 0.0
                    #print len(raw_df.columns), raw_df.columns
                    MergeDF(version, raw_df)
                    
                    #print len(self.df.columns), self.df.columns
                
    def GetDF(self):
        if self.df is None:
            self.Initialize()
        return self.df
    
    def AddColumn(self, column_name):
        pass
    
    def accept(self, visitor, reg_exp):
        df = self.GetDF()
        rdf = df.filter(regex=reg_exp) 
        for column in rdf:
            if column != self.x_column:
                visitor.Visit(self, df, column, *self.GetNameVersionPair(column))


# needs to support interpolation into another set of x-coords
class W2KDOS(FunctionInText):
    def __init__(self, project):
        super(W2KDOS, self).__init__(project, project.material, "dos*ev", "DOS")
        self.skip_header = 2
        self.column_re = re.compile('(\d+):(\D.*)')
        
    # variable names are initialized automatically from column names
    def VariableNames(self, file_name, version):
        None
        
    def XVariableName(self):
        return 'ω'
    
    # here we just rename columns in raw df
    def TransformDF(self, df):
        pmg_struct = self.project.struct.pmg_struct
        new_columns = []
        for column in df.columns:
            match = self.column_re.match(column)
            if match:
                anum = int(match.P__group(1)) - 1
                #label = struct.nonequiv_atoms[anum].label[0:2]
                label = str(anum+1) + str(pmg_struct.equivalent_sites[anum][0].specie)
                new_column_name = label + ' '+ match.P__group(2)
            elif column == "ENERGY":
                new_column_name = 'ω'
            elif column == 'TOTAL':
                new_column_name = 'KS DOS'
                
            new_columns.append(new_column_name)      
        df.columns = new_columns  
        return new_columns
        
    
    
class DMFTFunctionInText(FunctionInText):
        
    # DMFTFunctionInText uses legend from the INDMFL file for the hint
    # on column names. It is stored in self.legends
    
    def XVariableName(self):
        return self.project.Indmfl.XVariableName()
                
class LocalDMFTFunctionInText(DMFTFunctionInText):
    def LabelAndLegends(self, full_file_name, version):
        if self.suffix.find("*") != -1 or \
            self.suffix.find("?") != -1:
            # look for the number hint that notes the atom equiv. # 
            bn = path.relpath(full_file_name, self.project.dir_name)
            if version != "":
                bn = bn[:-len(version)-1]
            bn = bn[len(self.prefix)+1:]
            
            re_str =self.suffix.replace("*","(.*)").replace("?", "(.?)") + "$"
            
            regexp = re.compile(re_str)
            match = regexp.match(bn)
            #print "matching",re_str,"to",bn
            if match:
                #TODO: replace this by atom+#
                icp = int(match.P__group(1))
            else:
                # TODO: is this correct in the general case (multiple impurities)?
                icp = 1
        else:
            icp = 1
        indmfl = self.project.Indmfl
        return indmfl.GetAtomNumAndSpecie(icp, use_equiv_number=self.merge_equiv_atoms), indmfl.legends[icp]
        
    # ignores file_name nad reads the column names from the dmft legend
    def VariableNames(self, file_name, version):
        label,legend = self.LabelAndLegends(file_name,version)
        column_names = [self.x_column]
        for l in legend:
            column_names.append('Re {0}^{{{2}}}_{{{1}}}'.format(self.name, l.replace("\'",""), label))
            column_names.append('Im {0}^{{{2}}}_{{{1}}}'.format(self.name, l.replace("\'",""), label))
        return column_names
    
    
class DMFTSelfEnergy(DMFTFunctionInText):
    def VariableNames(self, file_name, version):
        sig_legends = self.project.Indmfl.GetSelfEnergyLegendsAndLabels()
        column_names = [self.x_column]
        for legend, label in sig_legends:
            column_names.append('Re {0}^{{{2}}}_{{{1}}}'.format(self.name, legend.replace("\'",""), label))
            column_names.append('Im {0}^{{{2}}}_{{{1}}}'.format(self.name, legend.replace("\'",""), label))
        return column_names
    
class DMFTSig(DMFTSelfEnergy):
    def __init__(self, project):
        super(DMFTSig, self).__init__(project, "sig", "inp", "Σ")
        fh = open(self.GetFilename(""),"r")
        self.s_oo = eval(fh.readline().split("=")[1])
        self.Edc  = eval(fh.readline().split("=")[1])
        fh.close()
      
    
    def store(self):
        df=self.GetDF()
        fn = self.GetFilename("")
        m = (len(df.columns)-1) /2 - len(self.s_oo)
        #TODO: fix this hack, use "ExtendSig" method instead
        self.s_oo += [0.0 for _ in range(m)]
        self.Edc += [0.0 for _ in range(m)]
        print("writing " + fn)
        fh = open(fn,"w")
        fh.write("# s_oo= " + str(self.s_oo) + "\n")
        fh.write("# Edc= " + str(self.Edc) + "\n")
        for row in df.itertuples():
            for v in row[1:]:  # skip indices
                fh.write("%14.14f  " % v)
            fh.write("\n")
        fh.flush()
        fh.close()

class DMFTSigOut(DMFTSelfEnergy):
    def __init__(self, project, iterations):
        super(DMFTSigOut, self).__init__(project, "imp.0/Sig", "out", "Σ", iterations)
      
class DMFTSigB(DMFTSelfEnergy):
    def __init__(self, project):
        super(DMFTSigB, self).__init__(project, "imp.0/Sig", "outb", "Σb")

class GFunction(LocalDMFTFunctionInText):
    def __init__(self,  project, prefix, suffix, name, versions, normalize_img):
        self.normalize_img = normalize_img
        super(GFunction, self).__init__(project, prefix, suffix, name, versions)
        
    def TransformDF(self, df):
        if not self.normalize_img:
            return df.columns
        columns = []
        for column in df.columns:
            if column.startswith("Im"):
                df[column] = -df[column] / pi
                columns.append("-{0}/π".format(column))
            else:
                columns.append(column)
        return columns

class DMFTDelta(LocalDMFTFunctionInText):
    def __init__(self, project):
        super(DMFTDelta, self).__init__(project, project.material, "dlt*", "Δ")
        
# TODO: for G functions prefix the columns by atom equivalence label
# TODO: extend this to non-correlated atoms, need all the densities of all the atoms
class DMFTG(GFunction):
    def __init__(self, project, normalize_img=False):
        super(DMFTG, self).__init__(project, project.material, "gc*", "G", [""], normalize_img)

class DMFTGImp(GFunction):
    def __init__(self, project, iterations, normalize_img=False):
        super(DMFTGImp, self).__init__(project, "imp.0/Gf", "out", "G_{imp}", iterations, normalize_img)

class DMFTGc(GFunction):
    def __init__(self, project, iterations=[""], normalize_img=False):
        super(DMFTGc, self).__init__(project, project.material, "gc*", "Gc", iterations, normalize_img)
        self.merge_equiv_atoms = True
        
class DMFTcDOS(DMFTFunctionInText):
    def __init__(self, project, iterations=[""]):
        super(DMFTcDOS, self).__init__(project, project.material, "cdos", "cDOS", iterations)
        self.orbital_re = re.compile(r'\s*(\d+)\s+L\s*=\s*(\d+)\s*')
        self.orbital = ['s','p','d','f','e']
    def VariableNames(self, file_name, version):
        column_names = [self.x_column, "DMFT DOS"]
        fh = self.project.open("cdos", "r")
        line = fh.readline()
        fh.close()
        pmg_struct = self.project.w2k_project.struct.pmg_struct
        #TODO: get real names of atoms,like in W2KDOS
        for cn in line.rstrip().split("a= ")[1:]:
            match = self.orbital_re.match(cn)
            if match:
                anum = int(match.P__group(1)) - 1
                name = "A{0} {1} {2}".format(anum+1,
                                            str(pmg_struct.sites[anum].specie),
                                            self.orbital[int(match.P__group(2))])
                column_names.append(name)
            else:
                column_names.append(cn)
        return column_names
    
    def GetDF(self):
        df = super(DMFTcDOS, self).GetDF()
        df["correlated#"] = df.filter(regex="A.*").sum(axis=1)
        return df
        
        
        