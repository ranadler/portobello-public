# -*- coding: utf-8 -*-
'''
Created on Aug 10, 2016

@author: adler@physics.rutgers.edu

'''


from builtins import set
import glob
from itertools import combinations_with_replacement, product
from numpy import zeros, array, mod, float16
import operator
from scipy import sign, log, linalg
import sys
from functools import reduce


def divmodulo(x,n):
    "We want to take modulo and divide in fortran way, so that it is compatible with fortran code"
    return ( sign(x)* (abs(x)/n) , sign(x)*mod(abs(x),n))

class Indmfl(object):
    '''
    This class represents a .indmfl file for one material
    '''
    def __init__(self, project):
        '''
        Constructor
        '''
        self.project = project
        self.hybr_emin = -10.0   # (eV) range of hybridization to pass on to impurity problem
        self.hybr_emax = 10.0    # (eV)
        self.Qrenorm   = 1       # whether or not to renormalize (what are we renormalizing?)
        self.projector = 2       # type of projection onto correlated space (0,1,2,3,4)
        self.matsubara = 0       # 0 = real axis, 1 = imaginary axis
        self.broadc    = 0.025   # (eV) broadening for correlated (applied to what -- self-energy?)
        self.broadnc   = 0.025   # (eV) broadening for noncorrelated
        self.om_npts   = 200     # number and range of default omega mesh (if no sig.inp file given)
        self.om_emin   = -3.0    # (eV)
        self.om_emax   = 1.0     # (eV)
        self.broken_sym = 0      # FM, AFM or ferrimagnetic run
        self.EF = 0.0
        
        self.atoms = {}
        self.cps = {}
        self.ucps = {}
        self.symclasses = {}     # group cps forming each ucp into symmetry classes (e.g. spin-up vs. spin-down)
        self.Lsa = []
        self.icpsa = []
        
        self.siginds = {}
        self.cftrans = {}
        self.legends = {}
   
    def XVariableName(self):
        if self.matsubara != 0:
            return '𝑖ω'
        else:
            return 'ω'
        
    def GetAtomNumAndSpecie(self, icp, use_equiv_number=False):
        struct = self.project.w2k_project.struct
        pmg_struct = struct.pmg_struct
        w2k_index = self.cps[icp][0][0]
        site = pmg_struct.sites[w2k_index-1]
        if use_equiv_number:
            return str(struct.EquivalenceIndex(w2k_index) + 1) +\
                str(site.specie)
        else:
            return str(w2k_index) + str(site.specie)
                
    # return ordered list of "element+Atom#", "legend(orbital)"
    def GetSelfEnergyLegendsAndLabels(self):
        ret = []
        pmg_struct = self.project.w2k_project.struct.pmg_struct
        covered_sites = set()
        
        for icp in sorted(self.siginds.keys()):
            w2k_index = self.cps[icp][0][0]
            site = pmg_struct.sites[w2k_index-1]
            if site in covered_sites:
                continue
            for legend in self.legends[icp]:
                label = str(w2k_index) + str(site.specie)
                ret.append((legend, label))
            for eq in pmg_struct.equivalent_sites:
                if site in eq:
                    for s in eq:
                        covered_sites.add(s)
        return ret
                     
    @classmethod
    def from_file(cls,project):
        indmfl = cls(project)
        indmfl.ReadFile(project.Filename("indmfl"))
        return indmfl
    
    def ReadFile(self,filename):
        lines = self.readlines(filename)
        self.parse_head(lines)
        self.parse_atomlist(lines)

        # read the big block of siginds and cftrans
        ncp, maxdim, maxsize = [int(e) for e in lines.next().split()]
        for i in range(ncp):
            icp, dim, sz = [int(e) for e in lines.next().split()]
            self.legends[icp] = lines.next().split("'")[1::2]
            self.siginds[icp] = array([[int(e) for e in lines.next().split()] for row in range(dim)])
            raw_cftrans = array([[float(e) for e in lines.next().split()] for row in range(dim)])
            self.cftrans[icp] = raw_cftrans[:,0::2] + raw_cftrans[:,1::2]*1j
        
        
    
    def readlines(self, filename = None):
        fname = filename if filename else self.filename()
        findmf = open(fname, 'r')
        lines = [line.split('#')[0].strip() for line in findmf.readlines()] # strip comments
        findmf.close()
        return (line for line in lines if line)  # strip blank lines & create generator expression

    def parse_head(self, lines):
        self.hybr_emin, self.hybr_emax, self.Qrenorm, self.projector = [float(x) for x in lines.next().split()]
        self.matsubara, self.broadc, self.broadnc, self.om_npts, self.om_emin, self.om_emax = [float(e) for e in lines.next().split()]
        self.matsubara = int(self.matsubara)  # recast these to integers
        self.om_npts   = int(self.om_npts) 

    def parse_atomlist(self, lines):
        self.Lsa=[]
        self.icpsa=[]
        natom = int(next(lines))
        for i in range(natom):
            
            dat=lines.next().split()
            iatom, nL, locrot_shift = [int(x) for x in dat[:3]]
            
            Rmt2=0
            if len(dat)>3:
                Rmt2 = float(dat[3])
                
            #locrot = locrot_shift % 3
            #shift  = locrot_shift / 3
            #if locrot_shift<0: 
            #    locrot=3
            #    shift= (locrot_shift+3)/3

            (shift,locrot) = divmodulo(locrot_shift,3)
            if locrot<0: locrot=3
            #print 'shift=', shift, 'locrot=', locrot
            # Ls, qsplits, icps = array([[int(x) for x in lines.next().split()] for i in range(nL)]).T
            
            (Ls, qsplits, icps) = (zeros(nL,dtype=int), zeros(nL,dtype=int), zeros(nL,dtype=int))
            for il in range(nL):
                (Ls[il], qsplits[il], icps[il]) = list(map(int, lines.next().split()[:3]))
                
            self.Lsa.append( Ls )
            self.icpsa.append( icps )
            
            new_zx = [[float(x) for x in lines.next().split()] for loro in range(abs(locrot))]
            vec_shift = [float(x) for x in lines.next().split()] if shift else None

            #print 'new_zx=', new_zx
            #print 'vec_shift=', vec_shift

            self.atoms[iatom] = (locrot, new_zx, vec_shift, Rmt2)
            for icp, L, qsplit in zip(icps, Ls, qsplits):
                if icp in self.cps:
                    self.cps[icp] += [(iatom, L, qsplit)]
                else:
                    self.cps[icp] = [(iatom, L, qsplit)]
                    
    def write_atomlist(self, lines):
        # create flat list of correlated orbitals (tricky because may have cluster problems)
        corbs = [[(icp,iatom,L,qsplit) for iatom,L,qsplit in v] for icp,v in self.cps.items()]
        corbs = reduce(operator.add, corbs)

        # list of atom-indices of correlated atoms
        icatoms = list(set(iatom for icp,iatom,L,qsplit in corbs))
        icatoms.sort()

        lines.append((str(len(icatoms)), "number of correlated atoms"))

        for iatom in icatoms:
            locrot, locrot_veclist, shift_vec, Rmt2 = self.atoms[iatom]
            orbs = [(icp,L,qsplit) for icp,iat,L,qsplit in corbs if iat==iatom]

            locrot_shift = 3+locrot if shift_vec else locrot
            if Rmt2>0:
                atom_header = ("%-3d %3d %3d  %f" % (iatom, len(orbs), locrot_shift, Rmt2), "iatom, nL, locrot Rmt2")
            else:
                atom_header = ("%-3d %3d %3d" % (iatom, len(orbs), locrot_shift), "iatom, nL, locrot")
            lines.append(atom_header)

            for icp,L,qsplit in orbs:
                orbstring = ("%3d %3d %3d" % (L, qsplit, icp), "L, qsplit, cix")
                lines.append(orbstring)

            locrot_labels = ["new z-axis", "new x-axis"][:len(locrot_veclist)]

            for vec,label in zip(locrot_veclist, locrot_labels):
                lines.append( ("%10.7f %10.7f %10.7f" % tuple(vec), label) )

            if shift_vec:
                lines.append( ("%4s %4s %4s" % tuple(shift_vec), "real-space shift in atom position") )

    def format(self, lines):
        # merge comments with values
        comment_column = max([len(entry) for entry,comment in lines])
        format = '%-' + str(comment_column) + 's  # %s\n'
        return [format % line for line in lines]

    def generate_indmfi(self):
        text = "{s}\n".format(s=len(list(self.siginds.keys())))
        for icp,sigind in self.siginds.items():
            text += "{s}\n".format(s=len(sigind))
            max_sigfig = 1 + int(log(max(sigind.flat))/log(10))
            format = '%' + str(max_sigfig) + 'd'
            for row in sigind:
                text += ' '.join([format % elem for elem in row]) + "\n"
        return text

    def generate(self):
        # generate text in two chunks, stored in text and text2
        #   text contains basic information about correlated problems
        #   text2 contains all the siginds, legends and crystal-field transformation matrices
        lines = []
        self.write_head(lines)
        self.write_atomlist(lines)
        text = self.format(lines)

        maxdim = max(len(s) for s in list(self.siginds.values())) # dimension of largest sigind matrix
        sizes = {}
        for icp,sigind in self.siginds.items():
            sizes[icp] = len([x for x in set(sigind.flat) if x != 0])
        maxsize = max(sizes.values())  # number of columns in largest

        text2 = [
            '#================ # Siginds and crystal-field transformations for correlated orbitals ================',
            '%-3d %3d %3d       # Number of independent kcix blocks, max dimension, max num-independent-components' % (len(self.cps), maxdim, maxsize)
            ]

        for icp,sigind in self.siginds.items():
            legend = self.legends[icp]
            text2 += [
                "%-3d %3d %3d       # %s" % (icp, len(sigind), sizes[icp], 'cix-num, dimension, num-independent-components'),
                '#---------------- # Independent components are --------------',
                "'%s' "*len(legend) % tuple(legend),
                ]

            text2.append('#---------------- # Sigind follows --------------------------')
            max_sigfig = 1 + int(log(max(sigind.flat))/log(10))
            format = '%' + str(max_sigfig) + 'd'
            for row in sigind:
                text2.append(' '.join([format % elem for elem in row]))

            # print local transform matrix (real & imag)
            text2.append('#---------------- # Transformation matrix follows -----------')
            for row in self.cftrans[icp]:
                text2.append(' '.join(["%11.8f %-11.8f" % (elem.real, elem.imag) for elem in row]))

        # join with first half; add \n to each line in text2
        text += [line+'\n' for line in text2]
        return text


    def write_head(self, lines):

        if abs(self.projector)<4: # This is the old scheme, where hybridization is cut-off by energy
            emin = self.hybr_emin+self.EF
            emax = self.hybr_emax+self.EF
            styp="%f "
            sdoc = "hybridization Emin and Emax, measured from FS, renormalize for interstitials, projection type"
            
            
        lines += [
            ( (styp+styp+"%d %d") % (emin, emax, self.Qrenorm, self.projector), sdoc),
            ("%1d %g %g %d %f %f" % (self.matsubara, self.broadc, self.broadnc, self.om_npts, self.om_emin, self.om_emax),
             "matsubara, broadening-corr, broadening-noncorr, nomega, omega_min, omega_max (in eV)")
            ]

            
    def write_header_advanced_projectors(self, lines):
        if abs(self.projector)>=4:
            import findNbands
            strfile = self.case+'.struct'
            enefiles = glob.glob(self.case+'.energyso')+glob.glob(self.case+'.energyso_'+'*')
            if not enefiles:  # Not spin-orbit run
                enefiles = glob.glob(self.case+'.energy') + glob.glob(self.case+'.energy_'+'*')
            enefiles = [fil for fil in enefiles if os.path.getsize(fil)>0] # Remove empty files
            
            print('all enefiles=', enefiles)
            
            #for fill in enefiles:
            #    if re.match(self.case+'.energyso', fil): # Spin-orbit on, remove non-spin-orbit files
            #        enefiles = filter(lambda fil: re.match(self.case+'.energyso', fil) is not None, enefiles) # Remove empty files
            #        break
            
            if len(enefiles)==0:
                print("ERROR : The case.energy* files should be present in this directory when using projector 5. Exiting....")
                sys.exit(1)
            
            (nemin,nemax) = findNbands.findNbands(self.hybr_emin+self.EF,self.hybr_emax+self.EF,enefiles,strfile)
            emin,emax = nemin,nemax
            styp="%d "
            sdoc = "hybridization band index nemin and nemax, renormalize for interstitials, projection type"
        
        if abs(self.projector)==5:
            import wavef
            Rm2=[self.atoms[iatom][3] for iatom in list(self.atoms.keys())] 

            print('Rm2=', Rm2, 'atms=', list(self.atoms.keys()), 'Lsa=', self.Lsa)
            wavef.main(self.case, list(self.atoms.keys()), self.Lsa, self.icpsa, Rm2)



qsplit_doc = """    Qsplit  Description
------  ------------------------------------------------------------
     0  average GF, non-correlated
     1  |j,mj> basis, no symmetry, except time reversal (-jz=jz)
    -1  |j,mj> basis, no symmetry, not even time reversal (-jz=jz)
     2  real harmonics basis, no symmetry, except spin (up=dn)
    -2  real harmonics basis, no symmetry, not even spin (up=dn)
     3  t2g orbitals 
    -3  eg orbitals
     4  |j,mj>, only l-1/2 and l+1/2
     5  axial symmetry in real harmonics
     6  hexagonal symmetry in real harmonics
     7  cubic symmetry in real harmonics
     8  axial symmetry in real harmonics, up different than down
     9  hexagonal symmetry in real harmonics, up different than down
    10  cubic symmetry in real harmonics, up different then down
    11  |j,mj> basis, non-zero off diagonal elements
    12  real harmonics, non-zero off diagonal elements
    13  J_eff=1/2 basis for 5d ions, non-magnetic with symmetry
    14  J_eff=1/2 basis for 5d ions, no symmetry
------  ------------------------------------------------------------
"""

dup = 1
no_op = 2
dup_shift = 3

qsplit_table = {
    #    2spin?  action     transtype        L = 0  L = 1          L = 2                  L = 3
    #    ------  ------     ---------        -----  -----          -----                  -----
    0  : (False, dup,       "none",         ([1],   [1,1,1],       [1,1,1,1,1],           [1,1,1,1,1,1,1]              )),  # averaged
    1  : (True,  no_op,     "relativistic", ([1,1], [1,1,2,3,3,2], [1,2,2,1,3,4,5,5,4,3], [1,2,3,3,2,1,4,5,6,7,7,6,5,4])),  # |j,mj>
    -1 : (True,  no_op,     "relativistic", ([1,2], list(range(1,7)),    list(range(1,11)),           list(range(1,15))                  )),  # |j,mj> no time-reversal symmetry
    2  : (False, dup,       "real",         ([1],   [1,2,3],       [1,2,3,4,5],           [1,2,3,4,5,6,7]              )),  # no symmetry, real
    -2 : (False, dup_shift, "real",         ([1],   [1,2,3],       [1,2,3,4,5],           [1,2,3,4,5,6,7]              )),  # no symmetry, real spin-polarized
    3  : (False, dup,       "t2g",          ([1],   [1,2,3],       [1,2,3,0,0],            [1,2,3,4,5,6,7]) ),              # t2gs
   -3  : (False, dup,       "eg",           ([1],   [1,2,3],       [1,2,0,0,0],            [1,2,3,4,5,6,7]) ),              # egs
    #3  : (False, dup_shift, "none",         ([1],   [1,2,3],       [1,2,3,4,5],           [1,2,3,4,5,6,7]              )),  # no symmetry, spherical
    4  : (True,  no_op,     "relativistic", ([1,1], [1,1,2,2,2,2], [1,1,1,1,2,2,2,2,2,2], [1,1,1,1,1,1,2,2,2,2,2,2,2,2])),  # |j,mj> jm's equivalent
    5  : (False, dup,       "real",         ([1],   [1,1,2],       [1,2,3,3,4],           [1,2,2,3,4,4,5]              )),  # axial    
    6  : (False, dup,       "real",         ([1],   [1,1,2],       [1,2,3,3,2],           [1,2,3,4,2,3,1]              )),  # hexagonal
    7  : (False, dup,       "real",         ([1],   [1,1,1],       [1,1,2,2,2],           [1,2,2,2,3,3,3]              )),  # cubic
    8  : (True,  dup_shift, "real",         ([1],   [1,1,2],       [1,2,3,3,4],           [1,2,2,3,4,4,5]              )),  # axial spin-polarized
    9  : (True,  dup_shift, "real",         ([1],   [1,1,2],       [1,2,3,3,2],           [1,2,3,4,2,3,1]              )),  # hexagonal spin-polarized
    10 : (True,  dup_shift, "real",         ([1],   [1,1,1],       [1,1,2,2,2],           [1,2,2,2,3,3,3]              )),  # cubic spin-polarized
    11 : (True,  no_op,     "relativistic", ([1,1], [1,1,2,3,3,2], [1,2,2,1,3,4,5,5,4,3], [1,2,3,3,2,1,4,5,6,7,7,6,5,4])),  # |j,mj> + off-diagonal
    12 : (False, dup_shift, "real",         ([1],   [1,2,3],       [1,2,3,4,5],           [1,2,3,4,5,6,7]              )),  # real + off-diagonal
    13 : (True,  no_op,     "5d-jeff",      ([1,1], [1,1,2,3,3,2], [1,1,2,2,3,3,4,4,5,5], [1,2,3,3,2,1,4,5,6,7,7,6,5,4])),  # |J_eff=1/2> 
    14 : (True,  no_op,     "5d-jeff",      ([1,2], [1,2,3,4,5,6], [1,2,3,4,5,6,7,8,9,10], [1,2,3,4,5,6,7,8,9,10,11,12,13,14])),  # |J_eff=1/2> 
    }


class NewIndmfl(Indmfl):
    
    @staticmethod
    def LofOrbital(letter):
        if letter == 's':
            return 0
        if letter == 'p':
            return 1
        if letter == 'd':
            return 2
        if letter == 'f':
            return 3
        
    def __init__(self, dmft_project):
        super(NewIndmfl,self).__init__(dmft_project)
        self.matsubara = 1
        # read from the associated w2k project's scf file
        self.EF = self.project.W2K.SCF2Info().FermiEnergy
        self.sig_offset = -1
        self.sig_size = 0
         
    def is_parallel(self, vec1, vec2):
        return linalg.norm(array(vec1) / linalg.norm(vec1) - array(vec2)/linalg.norm(vec2)) <= 1e-6
               
    def Increase(self, sig, offset):
        for i,j in product(list(range(sig.shape[0])),repeat=2):
            if sig[i][j] != 0:
                sig[i][j] += offset
        return sig
            
      
    def AddSubProblem(self, site, Lspdf, w2k_index, local_analysis):
        # TODO: should we handle more general stuff?
        #   self.ucps 
        #   self.symclasses 
        #   self.Lsa                
        # TODO: Handle shifts (what for)? Handle Rmt2 !=0 ?
        
        print("Adding ICP for ", site)
        
        self.icpsa.append([len(self.icpsa) + 1])
        self.Lsa.append([1])
        icp = len(self.icpsa)
        
        qsplit = 12
        
        locrot = 0
        if not self.is_parallel(local_analysis.ZAxis, (0.0,0.0,1)):
            locrot = 1
        if not self.is_parallel(local_analysis.XAxis, (1.0, 0.0, 0.0)):
            locrot = 2            
        self.atoms[w2k_index] = (locrot,  [local_analysis.FracZAxis, local_analysis.FracXAxis][:locrot], None, 0) # last are shift, Rmt2
        sigind = array(local_analysis.SigInd)
        self.sig_size = sigind.max()
        self.siginds[icp] = self.Increase(sigind, self.sig_offset)
        self.cps[icp] = [(w2k_index,NewIndmfl.LofOrbital(Lspdf),qsplit)]
        self.legends[icp] = local_analysis.BasisLegendText
        self.cftrans[icp] = local_analysis.Transformation   

    
    def AddSOrbital(self, st, w2k_index):  
        self.icpsa.append([len(self.icpsa) + 1])
        self.Lsa.append([1])
        icp = len(self.icpsa)
        qsplit = 0
        locrot = 0
        self.atoms[w2k_index] = (locrot,  [], None, 0) # last are shift, Rmt2
        self.cps[icp] = [(w2k_index,NewIndmfl.LofOrbital('s'),qsplit)]
        self.legends[icp] = ['s']
        self.siginds[icp] = array([[1]])
        self.cftrans[icp] = [[1.0]]
        
    def CompleteMatrix(self):
        # initialize sig_offset
        self.InitEQClass()
        # Here we assume there is only one impurity problem, and all the 
        # sigind  matrices are equivalent at the moment
        sig = self.siginds[1]
        legends = self.legends[1]
        for i,j in product(list(range(sig.shape[0])),repeat=2):
            if i<j and sig[i][j] == 0.0:
                self.sig_offset +=1
                sig[i][j] = self.sig_offset
                sig[j][i] = self.sig_offset
                legends.append("F"+str(self.sig_offset))
        for icp in list(self.legends.keys()):
            self.legends[icp] = legends
            self.siginds[icp] = sig
        
    def InitEQClass(self):
        if self.sig_offset == -1:
            # initialize by reading the max of the already-existing sigind
            m = 0
            for sigind in list(self.siginds.values()):
                m = max(m, sigind.max())
            self.sig_offset = m
        else:
            self.sig_offset += self.sig_size
            self.sig_size = 0
        
    
    #TODO: write indmfi file (DMFT seems to need it)
    def store(self, store_indmfi=True):
        self.outfile = self.project.open("indmfl",'w')
        self.outfile.writelines(self.generate())
        self.outfile.close()
        if store_indmfi:
            self.outfile = self.project.open("indmfi",'w')
            self.outfile.writelines(self.generate_indmfi())
            self.outfile.close()
            


def main():
    # unit test
    indmfl = Indmfl.from_file("/home/adler/work/BaCoS2/DMFT/BaCoS2.indmfl")
    for line in indmfl.generate():
        print(line)
        
if __name__ == '__main__': 
    main()
