'''
Created on Aug 12, 2016

@author: adler
'''
from matdelab.dmft.w2k_struct import W2KStruct
import os
import glob
from os.path import exists


class SCF2Info(object):
    
    class AtomInfo:
        def __init__(self, site):
            self.site = site
            self.valence_in_sphere = 0.0
            self.RMT = 0.0
            self.partial_charges = {}
            
        def total_charge_in_sphere(self):
            total = 0.0
            for orb in ['s','p','d','f']:
                total += self.partial_charges[orb]
            return total
            
    
    @staticmethod
    def last_token(line):
        return line.split()[-1]
    
    def atom_info(self, line):
        #print "site eqiv#=",int(line[4:7])
        return self.atoms_info[self.equivalent_sites[int(line[4:7])-1][0]]
    
    @property
    def FermiEnergy(self):
        return self.EF
    
    def __init__(self, infile, struct):
        self.atoms_info = {}
        self.pmg_struct = struct.pmg_struct
        self.Ry2eV = 13.60569193
        
        # data in scf2 is for equivalent sites, by equivalence index
        self.equivalent_sites = struct.equivalent_sites
        for equiv in self.equivalent_sites:
            atom_info = SCF2Info.AtomInfo(equiv[0])
            for site in equiv:
                self.atoms_info[site] = atom_info
            
        for line in infile.readlines():
            if line.startswith(":NOE"):
                self.noe = int(float(SCF2Info.last_token(line)))
            elif line.startswith(":FER"):
                self.EF = float(SCF2Info.last_token(line)) * self.Ry2eV
            elif line.startswith(":POS"):
                # check data consistency with the structure
                site = self.atom_info(line).site
                ending = line.split("=")[-1].split()
                zz = int(float(ending[0]))
                #TODO: remove this comment. there's a version-dependet bug here
                
                #el = ending[1][0:2] if len(ending[1])>=2 else ending[1]
                #if site.specie.symbol.lower() != el.lower() or site.specie.number != zz:
                #    raise Exception("SCF2 file inconsistent with struct, el={0} should be {1}, zz={2} should be {3}".format(el, site.specie.symbol, zz, site.specie.number))
            elif line.startswith(":CHA0"):
                info = self.atom_info(line)
                split1 = line.split("(RMT=")
                info.valence_in_sphere = float(split1[0].split()[-1])
                try:
                    info.RMT = float(split1[1].split()[0])
                except IndexError:
                    # this is in older W2K
                    info.RMT = 0.0
            elif line.startswith(":PCS"):
                orbs_str = line.split("=")[-1]
                # FIXME: Note this doesn't get the S orbital correctly: there is an extra line number
                orbs = orbs_str.split(",")[0:3]
            elif line.startswith(":QTL"):
                info = self.atom_info(line)
                to_split = line.split(":")[-1][1:]
                values = [to_split[i:i+7] for i in range(0, len(to_split), 7)]
                info.partial_charges = {orbs[i].lower():float(values[i]) for i in range(len(orbs))}
                print(info.partial_charges)

                
class W2KProject(object):
    '''
    classdocs
    '''
    W2K_ENV_SET = False
    
    @staticmethod
    # TODO: this is only for sub-processes, so that the current directory can be changed as well
    #       this design should have a method Execute(), which will set everything up for execution within project
    def SetW2KEnvironment():
        if not W2KProject.W2K_ENV_SET:
            # this could be read from CASE.scf:
            #    :LABEL3: using WIEN2k_14.2 (Release 15/10/2014) in /opt/apps/wien/wien2k_14
            wien_root = "/opt/apps/wien/wien2k_14/"
            os.environ["WIENROOT"] = wien_root
            os.environ["PATH"] = os.environ.get("PATH","") + ":" + wien_root
            # should this be optional? It is convenient. We could also ldd the executables to make sure they load.
            os.environ["LD_LIBRARY_PATH"] = os.environ.get("LD_LIBRARY_PATH","") + \
                ":/opt/intel/mkl/lib/intel64/:/opt/intel/lib/intel64/:/opt/intel/composer_xe_2013_sp1.5.212/mkl/lib/mic/:/usr/lib/x86_64-linux-gnu/"
            W2KProject.W2K_ENV_SET = True

    def __init__(self, dir_name):
        print("reading W2K project at", dir_name)
        struct_files =  glob.glob1(dir_name, "*.struct")
        if len(struct_files) > 1:
            raise  Exception("Multiple struct files in project dir " + dir_name)
        if len(struct_files) == 0:
            raise  Exception("no struct file in project dir " + dir_name)
        self.material = struct_files[0].split(".")[0]   
        if not self.material:
            raise Exception("no Wien2k struct file in directory, exiting.")
            
        self.struct = W2KStruct.from_file(os.path.join(dir_name,struct_files[0]))
        self.dir_name = dir_name
        self.scf2_info = None
        W2KProject.SetW2KEnvironment()
                
    def Filename(self, suffix):
        return os.path.join(self.dir_name, self.material + "." + suffix)
      
    def HasDOS(self):
        fn = self.Filename("dos1ev")
        return os.path.exists(fn) and os.path.getsize(fn) > 500
      
    def Filename1(self, full_name):
        return os.path.join(self.dir_name, full_name)
    
    def Filename2(self, label, suffix):
        return os.path.join(self.dir_name, label + "." + suffix)
            
    def open(self, suffix, mode):
        return open(os.path.join(self.dir_name, self.material + "." + suffix), mode)
    
    def has_suffix(self, suffix):
        return exists(os.path.join(self.dir_name, self.material + "." + suffix))
    
    @property
    def Material(self):
        return self.material
        
    @property
    def PMG_struct(self):
        return self.struct.PMG_struct
    
    @property
    def W2KStruct(self):
        return self.struct
  
    def SCF2Info(self):
        if not self.scf2_info:
            self.scf2_info = SCF2Info(self.open("scf2",'r'), self.struct)
        return self.scf2_info
    
    def run_command(self, str_command):
        print("running", str_command)
        os.system(str_command)
        
    