'''
Created on Aug 10, 2016

@author: adler
'''
from fortranformat.FortranRecordReader import FortranRecordReader
from pymatgen.core.lattice import Lattice
from pymatgen.core.structure import Structure
from pymatgen.core.operations import SymmOp
import numpy
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer

class InvalidStructFormat(Exception):
    None
    
class InvalidAtomEquivalence(Exception):
    None
    
class W2KStruct:
    
    @staticmethod
    def verify_equivalence(symm_struct, equivalent_site_indices):
        sites = symm_struct.sites
        for equiv in equivalent_site_indices:
            index = equiv[0]
            site = sites[index]
            equiv2 = symm_struct.find_equivalent_sites(site)
            # equiv and equiv2 are supposed to be the same list up to order, and type (equiv is just indices)
            for index1 in equiv:
                if not sites[index1] in equiv2:
                    raise InvalidAtomEquivalence("struct file equivalence ", equiv, "different from pymatgen's analysis:", equiv2)
        #print "verified site equivalence."
        
        
    @classmethod
    def from_file(cls, filename, verify_equivalent_sites=False):
        # From W2K's SRC_lapw0/readstruct.f
        FORMAT_1000 = FortranRecordReader("(A80)")                                                       
        FORMAT_1010 = FortranRecordReader("(A4,23X,I3,1x,a4,/,13X,A4,6X,A4)")                             
        FORMAT_1020 = FortranRecordReader("(6F10.6)")                
        FORMAT_1030 = FortranRecordReader("(4X,I4,4X,F10.8,3X,F10.8,3X,F10.8,/,15X,I2,17X,I2)")          
        FORMAT_1031 = FortranRecordReader("(4X,I4,4X,F10.7,3X,F10.7,3X,F10.7)")         
        FORMAT_1050 = FortranRecordReader("(A10,5X,I5,5X,F10.9,5X,F10.5,5X,F10.5)")               
        FORMAT_1051 = FortranRecordReader("(20X,3F10.8)")
        FORMAT_1151 = FortranRecordReader("(I4)")
        FORMAT_1101 = FortranRecordReader("(3(3I2,F11.8/),I8)")
    
        infile = open(filename, "r") # this will throw exception if file not found
        struct = cls()
        struct.title = FORMAT_1000.read(infile.readline())[0]
        struct.lattice_symbol, num_atoms, cform, struct.is_rel, units =\
             FORMAT_1010.read(infile.readline()+infile.readline())
        a,b,c,alpha, beta, gamma = FORMAT_1020.read(infile.readline())
        tol = 1e-5
        if alpha < tol:
            alpha = 90.0
        if beta < tol:
            beta = 90.0
        if gamma < tol:
            gamma = 90.0
        
        print("units:", units)
        if units == 'bohr' or units is None or units == "    ":
            print("converting lengths to angstrom")
            AuInA = 1.0/1.8897261254535 # convert to angstrem
        else:
            print("not converting lengths to angstrom")
            AuInA = 1.0
        lattice = Lattice.from_parameters(a * AuInA, b * AuInA, c * AuInA, alpha,beta,gamma)          
        coords = []
        nev_coords = []
        species = []
        nev_species = []
        struct.rotlocs = []
        equivalent_site_indices = []
        for _ in range(1,num_atoms+1):
            iatnr, pos_1, pos_2, pos_3, atom_mult, isplit = FORMAT_1030.read(infile.readline() + infile.readline())
            equivalent = [len(coords)]
            coords.append([pos_1, pos_2, pos_3])
            nev_coords.append([pos_1, pos_2, pos_3])
            if atom_mult <= 0:
                raise InvalidStructFormat() 
            
            for _ in range(1,atom_mult):
                iatnr, pos_1, pos_2, pos_3 = FORMAT_1031.read(infile.readline()) 
                equivalent.append(len(coords))
                coords.append([pos_1, pos_2, pos_3])
            
            equivalent_site_indices.append(equivalent)
            
            aname, jri, r0, rmt, zz = FORMAT_1050.read(infile.readline())
            nev_species.append(int(zz))
            for _ in range(atom_mult):
                species.append(int(zz))
            struct.rotlocs.append(FORMAT_1051.read(infile.readline()) + 
                                  FORMAT_1051.read(infile.readline()) +
                                  FORMAT_1051.read(infile.readline()))  
            
        # Symmetry group of the crystal
        struct.symm_ops = []
        nsym = FORMAT_1151.read(infile.readline())[0]
        for _ in range(1,nsym+1):
            matrix = FORMAT_1101.read(infile.readline()+infile.readline()+infile.readline()+infile.readline())
            struct.symm_ops.append(
                SymmOp(numpy.array(
                    [matrix[0:4],matrix[4:8],matrix[8:12],[0.0,0.0,0.0,0.0]])))
        
        # TODO: 1.record / verify the group operations, local rotations, r0, rmt 
        #       2. Mode for assigning all these values automatically, starting from bare structure, and bypassing
        #          w2k's init_lapw sequence
        
        sgnum = -1
        try:
            sgnum  = int(cform)
        except ValueError:
            None
        if sgnum > 0:
            print("Note: creating structure from space group", sgnum)
            st = Structure.from_spacegroup(sgnum, lattice, nev_species, nev_coords)
            print(st)
        else:
            st = Structure(lattice, species, coords)
        
        sga = SpacegroupAnalyzer(st)
        
        struct.pmg_struct = sga.get_symmetrized_structure()
        if verify_equivalent_sites:
            W2KStruct.verify_equivalence(struct.pmg_struct, equivalent_site_indices)
        
        # note that our equivalent sites has references to PMG sites, not indices
        #struct.equivalent_sites = [[struct.pmg_struct.sites[i] for i in equiv] for equiv in equivalent_site_indices]
        return st
        
        def EquivIndex(st,equiv):
            for i, eq in enumerate(equiv):
                if st in eq:
                    return i
        
        struct.equivalence_index = [EquivIndex(st,struct.pmg_struct.equivalent_sites) for st in struct.pmg_struct.sites]
        return struct

    def EquivalenceIndex(self, w2k_index):
        return self.equivalence_index[w2k_index-1]

    @property
    def PMG_struct(self):
        return self.pmg_struct
    
    @property
    def SymmOps(self):
        return self.symm_ops
        
    def __init__(self):
        None

    # atom index compatible with the struct file (and DMFT)
    def atom_index(self,site):
        return self.pmg_struct.index(site) + 1 
        
    def to_string(self):
        return ""
        
def main():
    # unit test
    st1 = W2KStruct.from_file("/home/adler/work/BaCoS2/DMFT/BaCoS2.struct")
    print(st1.title, st1.lattice_symbol, st1.is_rel)
    print(st1.PMG_struct)
    print(st1.SymmOps)
        
if __name__ == '__main__': 
    main()

