'''
Created on Aug 16, 2016

@author: adler
'''
import glob
import os
from os.path import exists, join
import re
from subprocess import call, Popen
import subprocess

from matdelab.dmft import indmfl
from matdelab.dmft.indmfl import Indmfl, NewIndmfl
from matdelab.dmft.w2k_project import W2KProject


class DMFTProject(object):
    '''
    classdocs
    '''
    
    @classmethod
    def NewProject(cls, dir_name, w2k_project, copy=True):
        project = cls(dir_name, w2k_project)
        if copy:
            # TODO: divide into mandatory files and optional ones, and raise exception appropriately
            project.copy_with_suffixes(w2k_project, ['struct', 'in0', 'clmsum', 
                                                     'inm','in1', 'in1c', 'klist', 'inso', 'in2c', 
                                                     'in2', 'kgen',
                                                     'inc','scf2'])
            return project
        
    @classmethod
    def NewProjectFromDMFT(cls, dir_name, dmft_project, copy=True):
        project = cls(dir_name, dmft_project.w2k_project)
        if copy:
            #TODO: COPY other dmft files, for now we use a utility but this should be cleaned up
            os.system("dmft_copy.py " + dmft_project.dir_name)
            return project
       
    def __init__(self, dir_name, w2k_project=None):
        '''
        Constructor
        '''
        self.dir_name = dir_name
        if w2k_project:
            self.w2k_project = w2k_project
        else:
            self.w2k_project = W2KProject(dir_name)
        self.material = self.w2k_project.material
            
    def Filename(self, suffix):
        return os.path.join(self.dir_name, self.material + "." + suffix)
      
    def Filename1(self, full_name):
        return os.path.join(self.dir_name, full_name)
    
    def Filename2(self, label, suffix):
        return os.path.join(self.dir_name, label + "." + suffix)
            
    def open(self, suffix, mode):
        return open(self.Filename(suffix), mode)
    
    def copy_with_suffixes(self, other_project, suffixes):
        if self.dir_name == other_project.dir_name:
            return
        for suffix in suffixes:
            if other_project.has_suffix(suffix):
                infile = other_project.open(suffix,'r')
                outfile = self.open(suffix, 'w')
                outfile.writelines(infile.readlines())
                outfile.close()
                infile.close()
    @property
    def W2K(self):
        return self.w2k_project
    
    @property
    def Material(self):
        return self.material
    
    def NewIndmfl(self):
        indmfl  = NewIndmfl(self)
        return indmfl
    
    # TODO: convert some fields like beta, U, J to float, also add fields like T
    class IParams(object):
        def __init__(self, dictionary):
            self.__dict__.update(dictionary)
    
    def iparams0(self):        
        vglobl={}
        vlocal={}
        fn = self.Filename1("params.dat")
        exec(compile(open(fn).read(), fn, 'exec'),vglobl, vlocal)
        return DMFTProject.IParams({k:v[0] for k,v in list(vlocal['iparams0'].items())})
    
    @property
    def Indmfl(self):
        return Indmfl.from_file(self)
            
    def has_suffix(self, suffix):
        return exists(self.Filename(suffix))
    
    def run_command(self, str_command):
        #print "running", str_command, "in", self.dir_name
        subprocess.call(str_command.split(" "), cwd=self.dir_name)
        #os.system(str_command)
              
    def HasDOS(self):
        fn = self.Filename("cdos")
        return os.path.exists(fn) and os.path.getsize(fn) > 500
        
    def GetIterations(self, from_iteration, num_iterations):
        alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
        convert = lambda text: int(text) if text.isdigit() else text.lower() 
        prefix = "sig.inp."
        sig_inp_iters = sorted(glob.glob1(self.dir_name, prefix+"*.*"),
                               key=alphanum_key)
            
        if from_iteration == -1:
            start_pos = -num_iterations
            end_pos = None
        else:
            start_pos = from_iteration
            end_pos = from_iteration + num_iterations
        return [fn[len(prefix):] for fn in sig_inp_iters][start_pos:end_pos]
        
        