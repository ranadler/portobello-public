#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on Aug 6, 2017

Manipulate the sig.inp file.

- Add columns at the end (not implemented yet)

- Delete columns (-D 1,2 for example deletes 1st and 2nd columns, that is both real and imag. parts of the 2 columns)

- Change temperature (or beta) - resamples the functions in the correct matsubara mesh, by interpolation.


@author: adler
'''


from optparse import OptionParser
from matdelab.dmft.dmft_project import DMFTProject
import os
import sys
from builtins import set
from numpy import arange
from math import pi
from scipy.interpolate.interpolate import interp1d


def read_sigfile(fh):
    val = eval(fh.readline().replace("#","").split("=")[1])
    sinf = [float(f) for f in val]
    val = eval(fh.readline().replace("#","").split("=")[1])
    edc = [float(f) for f in val]
    l = len(sinf)
    sigs = [[] for l in range(l*2)]
    omegas = []
    for line in fh.readlines():
        vals = line.split()
        omegas.append(float(vals[0]))
        for i in range(1,len(vals)):
            sigs[i-1].append(float(vals[i]))
    return sinf, edc, omegas, sigs
    

 
def main():
    parser = OptionParser()
    parser.add_option("-d", "--dmft_dir", dest="dmft_dir",
                      default = os.curdir,
                      help="Directory where there is a dmft project")   
    
    parser.add_option("-T", "--T_kelvin", dest="T", default=None, type=int, help="Temperature in Kelvin")
    parser.add_option("-b", "--beta", dest="beta", default=None, type=int, help="1/Temperature in 1/eV")

    parser.add_option("-D", "--delete", dest="delete", default='', help='comma separated list of orbitals to delete')
    parser.add_option("-A", "--add", dest="add", default=0, type=int, help='number of sigs to add at the end')

    parser.add_option("-o", "--out", dest="outfile", default="sig.inp.new")

    

    (options, _) = parser.parse_args(sys.argv[1:])
    
    dmft_proj = DMFTProject(options.dmft_dir)
    iparams = dmft_proj.iparams0()
   
    if options.T:
        options.beta = int(1/(options.T * 8.621738e-5))
        print("new beta=", options.beta)
    beta = iparams.beta
    if options.beta:
        beta = options.beta
        
    if beta != iparams.beta:
        print("changing temperature, setting beta to:", beta)
    
    sinf, edc, omegas, sigs = read_sigfile(open(dmft_proj.Filename1("sig.inp"), "r"))
    
    
    print(sinf, edc, len(sigs), len(omegas), len(sigs[0]))
    
    del_sigs = set()
    if options.delete:
        vals = options.delete.split(",")
        del_sigs = set([int(v) for v in vals])
    
    new_sinf = []
    new_edc = []
    new_sigs = []
    for i in range(len(sinf)):
        if i+1 not in del_sigs:
            new_sinf.append(sinf[i])
            new_edc.append(edc[i])
            new_sigs.append(sigs[2*i])
            new_sigs.append(sigs[2*i+1])
                
    if beta != iparams.beta:
        nom = (200.* beta-1)/(2.*pi)
        new_omegas = (arange(1,nom,1)*2-1)*pi/beta
        fS = interp1d(omegas, new_sigs,fill_value='extrapolate')
        new_sigs = fS(new_omegas)
    else:
        new_omegas = omegas
    
    outf = open(dmft_proj.Filename1(options.outfile),'w')
    outf.write("# s_oo = " + str(new_sinf) + "\n")
    outf.write("# Edc = " + str(new_edc) + "\n")
    for i,omega in enumerate(new_omegas):
        outf.write(str(omega))
        outf.write(" ")
        for s in new_sigs:
            outf.write(str(s[i])+' ')
        outf.write("\n")
    outf.close()
    
  
if __name__ == '__main__':
    main()
