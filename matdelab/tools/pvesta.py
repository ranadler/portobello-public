#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 15:10:30 2015

@author: adler@physics.rutgers.edu

"""

import warnings
warnings.filterwarnings("ignore")


from re import compile
from  pymatgen.core.structure import Structure
from optparse import OptionParser
import sys
import os
import tempfile
import subprocess
from pymatgen.vis import structure_vtk
import numpy as np
import colorsys
from numpy import Infinity
from _collections import defaultdict
from pymatgen.symmetry.analyzer import SpacegroupAnalyzer
from matdelab.core.old_local_analysis import LocalAnalysis
from matdelab.dmft.w2k_struct import W2KStruct
from matdelab.dmft.dmft_project import DMFTProject
from matdelab.dmft.indmfl import Indmfl

          
def cluster_sites(mol, tol):
    """
    Cluster sites based on distance and species type.

    Args:
        mol (Molecule): Molecule **with origin at center of mass**.
        tol (float): Tolerance to use.

    Returns:
        (origin_site, clustered_sites): origin_site is a site at the center
        of mass (None if there are no origin atoms). clustered_sites is a
        dict of {(avg_dist, species_and_occu): [list of sites]}
    """
    # Cluster works for dim > 2 data. We just add a dummy 0 for second
    # coordinate.
    dists = [[np.linalg.norm(site.coords), 0] for site in mol]
    import scipy.cluster as spcluster
    f = spcluster.hierarchy.fclusterdata(dists, tol, criterion='distance')
    clustered_dists = defaultdict(list)
    for i, site in enumerate(mol):
        clustered_dists[f[i]].append(dists[i])
    avg_dist = {label: np.mean(val) for label, val in list(clustered_dists.items())}
    clustered_sites = defaultdict(list)
    origin_site = None
    for i, site in enumerate(mol):
        if avg_dist[f[i]] < tol:
            origin_site = site
        else:
            clustered_sites[(avg_dist[f[i]],
                             site.species_and_occu)].append(site)
    return origin_site, clustered_sites

 
 
def get_colors(num_colors):
    colors=[]
    for i in np.arange(0., 360., 360. / num_colors):
        hue = i/360.
        lightness = (50 + np.random.rand() * 10)/100.
        saturation = (90 + np.random.rand() * 10)/100.
        colors.append(colorsys.hls_to_rgb(hue, lightness, saturation))
    return colors

def IsMetal(specie):
    return specie.is_alkali or specie.is_alkaline or specie.is_transition_metal or specie.is_lanthanoid or specie.is_actinoid
 
def site_label(st, site):
    ind = 0
    for s in st.sites:
        if s.specie == site.specie:
            ind += 1
            if s == site:
                return s.specie.symbol + str(ind)
    return None

def find_periodic_image(nn,st):
    for site in st.sites:
        if nn.is_periodic_image(site):
            return site
    return None
    
def WriteVestaFile(st, filename, f, options):
    f.write("#VESTA_FORMAT_VERSION 3.3.0\nCRYSTAL\nTITLE\n")
    f.write("{0}\n".format(filename))
    f.write("CELLP\n")
    lat = st.lattice
    f.write('{0} {1} {2} {3} {4} {5}\n'.format(lat.a, lat.b, lat.c, lat.alpha, lat.beta, lat.gamma))
    f.write('{0} {1} {2} {3} {4} {5}\n'.format(0, 0, 0, 0, 0, 0))
    f.write("STRUC\n")
    index = 0
    for site in st.sites:
        index += 1
        f.write('{0} {1} {2} 1.0000 {3} {4} {5}\n'.format(
            index, site.specie.symbol, site_label(st, site),  
            site.a, site.b, site.c))
        f.write("    0.0 0.0 0.0 0.0\n")
    f.write('{0} {1} {2} 0.0 {3} {4} {5}\n'.format(0,0,0,0,0,0,0))
    f.write("BOUND\n")
    f.write("0 {0} 0 {1} 0 {2}\n".format(len(options.a)+1, len(options.b)+1, len(options.c)+1))
    f.write("0 0 0 0 0 0\n")
    f.write("SBOND\n")
    index = 0
    species = {}
    for site in st.sites:
        if IsMetal(site.specie) and (site.specie not in species):
            index += 1
            #                             beoynd poly polyhera   color 
            f.write('{0} {1} XX {2} {3} 1 1      0    0          1     0.250 1.000  180 180 180\n'.format(
                index, site.specie.symbol,
                float(site.specie.atomic_radius) * 1.2,
                options.nn_radius))
            species[site.specie] = True
    f.write("0 0 0 0\n")
    f.flush()
        
def AddFeaturesToVTK(vis, st, indmfl, sites, options, element_colors):
    species = [] if not options.species else options.species.split(",")
    for index, site in enumerate(sites):
        if options.species and site.specie.symbol not in species and site_label(st, site) not in species:
            continue
        local_analysis = LocalAnalysis(st, site, nn_radius=options.nn_radius,
                                       weighted_vd=options.weighted_vd, orbital_letter=options.orbital,
                                       rotation_angle=options.rotation_angle,
                                       from_indmfl=indmfl,
                                       debug=options.debug)
        voro = local_analysis.VoronoiDiagram
        neighbors = local_analysis.VoronoiDiagramSet
        all_vertices = voro.vertices
        for nn, vind in list(voro.ridges.items()):
            if 0 in nn:
                symbol = neighbors[nn[1]].specie.symbol
                color = vis.el_color_mapping[symbol]
                vis.add_bonds(center=site,neighbors=[neighbors[nn[1]]], radius=0.03, opacity=0.5, 
                          color=color)
                neighbor = neighbors[nn[1]]
                vis.add_site(neighbor)
                # this allows to draw a radius
                #radius = float(neighbor.specie.atomic_radius) * 30
                #vis.add_partial_sphere(neighbor.coords, radius,
                #                       vis.el_color_mapping[neighbor.specie.symbol], 0, 360, 1.0)

                facet = [all_vertices[i] for i in vind]
                # fix colors
                color = element_colors[symbol]
                #opacity = 1.0
                opacity = 0.5
                vis.add_faces([facet], color=color, opacity=opacity)
                #edges += [facet]
       
        # Show the cartesian axes
        z = site.coords
        d = st.lattice.a *1.2
         
        vis.add_line(z,z + local_analysis.ZAxis *d, width=4, color=(1.0, 0.0, 0.0))
        vis.add_line(z,z + local_analysis.XAxis * d * 0.8, width=4, color=(0.1, 0.0, 0.0))
                
        if options.debug:
            for pa in local_analysis.PrincipalAxes:
                vis.add_line(z,z + pa, width=10, color=(0.0, 0.0, 1.0))
        
        # TODO: draw the orthogonal plane to z as a face
        
        #v2 = structure_vtk.StructureVis(element_color_mapping, show_unit_cell, show_bonds, show_polyhedron, poly_radii_tol_factor, excluded_bonding_elements)
        vis.show_help = False
        #vis.add_text(site.coords, pga.sch_symbol)# * 0.3+ site_neighbors[0].coords* 0.7, pga.sch_symbol)
        
        index = 0
        equivalen_sites = local_analysis.mol.get_equivalent_sites
        for equiv in equivalen_sites:
            index +=1
            for site in equiv:
                vis.add_text(z+site.coords, str(index))
            

def sum_squares(color):
    ssum = 0
    for f in color:
        ssum += f*f
    return ssum
                
def find_nearest_color(good_colors, color):
    best_color = color
    best_sum = Infinity
    for i in range(0,len(good_colors)):
        s = sum_squares([good_colors[i][j] - color[j] for j in range(0, len(color))] )
        if s < best_sum:
            best_sum = s 
            best_color = good_colors[i] 
    return best_color
                
def NewColorTable(st, vis):
    good_colors = get_colors(20)
    element_colors = {}
    for specie in st.species:
        if specie.symbol not in element_colors:
            color = vis.el_color_mapping[specie.symbol]
            best_color = color
            best_sum = Infinity
            for i in range(0,len(good_colors)):
                s = sum_squares([good_colors[i][j] - color[j] for j in range(0, len(color))] )
                if s < best_sum and good_colors[i] not in list(element_colors.values()):
                    best_sum = s 
                    best_color = good_colors[i] 
            element_colors[specie.symbol] =  best_color
    return element_colors
            
def ShowVis(st, indmfl, options):
    vis = structure_vtk.StructureVis(show_bonds=False, show_polyhedron=False, show_unit_cell=True)
    #vis.show_help = False
    element_colors = NewColorTable(st, vis)
    # create supercell options
    sc_options = [options.a, options.b, options.c]
    for i in range(0,3):
        vis.supercell[i][i] += len(sc_options[i])
        
    vis.set_structure(st, True)
    st.make_supercell([vis.supercell[i][i] for i in range(0,3)])
    AddFeaturesToVTK(vis, st, indmfl, st.sites, options,element_colors)
    # Show the cartesian axes
    z = (0.0,0.0,0.0)
    d = st.lattice.a /2.0
    vis.add_line(z,(d,0.0,0.0) , width=5)
    vis.add_line(z,(0.0,d,0.0) , width=5)
    vis.add_line(z,(0.0,0.0,d) , width=5)
    dh = d * 0.8
    vis.add_text((dh,0.0,0.0), "x")
    vis.add_text((0.0,dh,0.0), "y")
    vis.add_text((0.0,0.0,dh), "z")
 
    vis.show()
             
        
def Find(input_file_name, pos_number): 
    start_re = compile('EA(\d+)\s+.*Sym\.P__group\:.*')

    current_pos_str = None
    num = -1
    input_file = open(input_file_name)
    for line in input_file:
        match = start_re.match(line)
        if match:
            if current_pos_str:
                if num == pos_number:
                    return Structure.from_str(current_pos_str, 'POSCAR', primitive=True)
            current_pos_str = line
            num = int(match.P__group(1))
        else:
            current_pos_str += line
                
    # now pick up the last one
    if  num == pos_number:
        return Structure.from_str(current_pos_str, 'POSCAR', primitive=True)
    else:
        return None
   
def strcutrue_from_W2K(struct_filename):
    return W2KStruct.from_file(struct_filename)
   
        
def main():

    parser = OptionParser()
    parser.add_option("-s", "--species", dest="species", 
                      default=None)
    parser.add_option("-n", "--poscar_number", dest="number", type=int, default=-1)
    parser.add_option("-w", "--wvd", dest="weighted_vd", action="store_true", default=False)
    parser.add_option("-r", "--nn_radius", dest="nn_radius", type=float, default=3)
    parser.add_option("-v", "--vtk", dest="vtk",
                      action="store_true",
                      default=False,
                      help="use nearest neighbors to determine bonds, diplay in VTK VIS") 
    parser.add_option("-a", "--A", dest="a", help="a-axis supercell, -aa=>2 cells -aaa=>3 cells etc.", default="")
    parser.add_option("-b", "--B", dest="b", help="b-axis supercell, -bb=>2 cells -bbb=>3 cells etc.", default="")
    parser.add_option("-c", "--C", dest="c", help="c-axis supercell, -cc=>2 cells -ccc=>3 cells etc.", default="")
    
    parser.add_option("-o", "--out_vesta", dest="out_vesta",
                      action="store_true",
                      default=False,
                      help="print vesta file output") 
    
    parser.add_option("-p", "--primitive", dest="make_primitive",
                      action="store_true",
                      default=False,
                      help="reduce the unit cell to primitive cell") 
    
    parser.add_option("-O", "--orbital", dest="orbital", default='d')
    parser.add_option("-R", "--rot", dest="rotation_angle", type=float, default=0.0)
    
    parser.add_option("-D", "--dmft", dest="dmft_dir", default=None)
    
    parser.add_option("-d", "--debug", dest="debug",
                      action="store_true",
                      default=False,
                      help="add debug prints") 
    
    
    (options, args) = parser.parse_args(sys.argv[1:])
    
    poscar_file = None
    indmfl = None
    if args:
        poscar_file = args[0]
        
    
    if options.number >=0:
        st = Find(poscar_file, options.number)
    elif options.dmft_dir:
        dmft_project = DMFTProject(options.dmft_dir)
        st = dmft_project.w2k_project.PMG_struct
        indmfl = dmft_project.Indmfl
    elif poscar_file.lower().endswith(".struct"):
        st = strcutrue_from_W2K(poscar_file)
    elif poscar_file.upper().endswith("POSCAR") or poscar_file.upper().startswith("POSCAR"):
        st = Structure.from_str(open(poscar_file).read(), primitive=options.make_primitive,fmt='poscar')
    else:
        st = Structure.from_file(poscar_file, primitive=options.make_primitive)
        
    sga = SpacegroupAnalyzer(st, 1e-3, 5)
    print("space P__group:" ,sga.get_space_group_symbol(), " #", sga.get_space_group_number())
    print("density:", st.density)
        
    if not options.vtk:
        tf = tempfile.NamedTemporaryFile(suffix='.vesta', delete=False)
        WriteVestaFile(st, os.path.abspath(poscar_file), tf, options)
        if options.out_vesta:
            subprocess.check_call(["cat", tf.name])
        subprocess.check_call(["/home/adler/VESTA-x86_64/VESTA", tf.name])
    else:
        print(st)
        ShowVis(st, indmfl, options)
    

if __name__ == '__main__': 
    main()


    
