#!/usr/bin/python
# -*- coding: utf-8 -*-



from bokeh.client.session import push_session
from bokeh.layouts import row, column
from optparse import OptionParser
import os
import re
import sys

from matdelab.core.function_storage import W2KDOS, DMFTGc, DMFTcDOS, DMFTSig
from matdelab.dmft.w2k_project import W2KProject
from matdelab.tools.plotting import SimplePlot, tools_str


from bokeh.io import curdoc
from matdelab.dmft.dmft_project import DMFTProject
from collections import OrderedDict
from bokeh.models.widgets.groups import CheckboxGroup
from bokeh.models.widgets.markups import Div
from bokeh.models.renderers import GlyphRenderer
from bokeh.models.widgets.buttons import Button
from matdelab.tools.latex import LatexLabel
from bokeh.models.annotations import Span


class LegendManager(object):
    def __init__(self, graph):
        self.graph = graph
        self.fig = graph.Graph()
        self.all_line_legends = {line: self.fig.legend[0].items[i] for i, line in 
                                    enumerate(graph.lines.values())} 
    
    def UpdateLegend(self, active_lineg):
        new_legends = []
        fig = self.graph.Graph()
        for column_name in list(self.graph.lines.keys()):
            line = self.graph.lines[column_name]
            ll = self.all_line_legends[line]
            if column_name in list(active_lineg.active.keys()):
                if active_lineg.active[column_name]:
                    new_legends.append(ll)
            else:
                if line.visible:
                    new_legends.append(ll)
        self.fig.legend[0].items = new_legends
        # make sure it is refreshed, pick one line (the last) and refresh it
        line.visible = not line.visible
        line.visible = not line.visible
        
        
class ActivateLineGroup(object):
    def __init__(self, graph, legend_manager, data_table, label, regex):        
        self.title = Div(text="<h3>{0}</h3>".format(label), width=100)
        self.active = OrderedDict()
        self.legend_manager = legend_manager
        self.graph = graph
        self.names = []
        data_table.accept(self, regex)
        self.checkbox = CheckboxGroup(labels=self.names,
                                      active=list(range(len(list(self.active.keys())))),
                                      width=200) 
        self.checkbox.on_change('active', self.Update)
        self.clear_button = Button(label="Clear", button_type="success", width=40)
        self.clear_button.on_click(self.ClearAll)
        self.mpl_button = Button(label="MPL", button_type="success", width=40)
        self.mpl_button.on_click(self.MPL)
    
    def Visit(self, function_storage, df, column_name, name, version):
        self.active[column_name] = True
        self.names.append(name)
        
    def ClearAll(self):
        self.checkbox.active = []     
        line = list(self.graph.lines.values())[0]
        
    def MPL(self):
        print(self.checkbox.active)  
        print(list(self.graph.lines.values()))
        
    def Update(self,attr, old, new):
        for ix, column_name in enumerate(self.active.keys()):
            self.active[column_name] = ix in self.checkbox.active
        for column_name in list(self.active.keys()):
            line = self.graph.lines[column_name]
            line.visible = self.active[column_name]
        # update *ALL* line legends in the graph accordingly (not only our's)
        # this is a bokeh bug workaround
        self.legend_manager.UpdateLegend(self)
        
    def GetVisualElement(self):
        return row(column(row(self.title, self.clear_button), self.checkbox),self.mpl_button)


title_text = ""
tables = OrderedDict()
x_column_name = ""

def AddW2kDir(dir_name):
    w2k_proj = W2KProject(dir_name)
    if w2k_proj.HasDOS():
        global title_text,tables,x_column_name
        title_text += "<p> W2K at: " + os.path.abspath(dir_name)
        tables[W2KDOS(w2k_proj)] =  ("KS DOS", ".*")
        x_column_name = 'ω'
        return w2k_proj.material
    else:
        print("No DOS found in W2K project at " + dir_name)
        return None

def AddDMFTDir(dir_name):
    dmft_proj = DMFTProject(dir_name)
    if dmft_proj.HasDOS():
        global title_text,tables,x_column_name
        title_text += "<p> DMFT at: " + os.path.abspath(dir_name)
        tables[DMFTGc(dmft_proj, normalize_img=True)] = ("G", '-Im.*')
        #tables[DMFTcDOS(dmft_proj)] = ("DMFT DOS",".*")
        x_column_name = dmft_proj.Indmfl.XVariableName()
        return dmft_proj.material
    #(DMFTSig(dmft_proj), ("DMFT Σ", ".*"))
    else:
        print("No DOS found in DMFT project at " + dir_name)
        return None
        
def main():
    parser = OptionParser()
    parser.add_option("-w", "--w2k_dirs", dest="w2k_dirs", default=[], action="append")
    parser.add_option("-d", "--dmft_dirs", dest="dmft_dirs", default=[], action="append")
    parser.add_option("-b", "--wd", dest="both", default=[], action="append")
      
    (options, args) = parser.parse_args(sys.argv[1:])
    
    if len(options.w2k_dirs) == 0 and len(options.dmft_dirs) == 0:
        options.both = ["."]
        
    
    for dir_name in options.w2k_dirs + options.both:
        material = AddW2kDir(dir_name)
            
    for dir_name in options.dmft_dirs + options.both:
        material2 = AddDMFTDir(dir_name)
        if material2 is not None:
            material = material2
                
    global title_text,tables
    title_text = "<h1> DOS Plot {0}</h1>".format(material) + title_text

    print("x variable:", x_column_name)
    dos_graph = SimplePlot(x_column_name, width=450, height=300,
                           tools=tools_str + ",save",
                           x_range=[-5.0, 5.0])
    
    cbs = []
    for table in list(tables.keys()):
        dos_graph.VisitAllVariables(table, tables[table][1])
        
    legend_manager = LegendManager(dos_graph)
    
    # do this after all line legends are defined, to work around 
    # bokeh's legend bug
    for table in list(tables.keys()):
        cbs.append(ActivateLineGroup(dos_graph, legend_manager, 
                                     table, *tables[table]).GetVisualElement())
        
    plot = dos_graph.Graph() 
    
    plot.xaxis.axis_label = "Energy (eV)"
    plot.yaxis.axis_label = "DOS (States/eV)"
    plot.add_layout(Span(location=0,
                         dimension='height', line_color='grey',
                            line_dash='dashed', line_width=1))
    
    title = Div(text=title_text, width=600)
    layout = column(title, column(row([plot]), row(cbs)))
    curdoc().add_root(layout)
 
 
    session = push_session(curdoc())
    session.show()
    session.loop_until_closed()
    
    #show(layout, browser="google-chrome")

if __name__ == '__main__': 
    main()
