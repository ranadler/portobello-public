#!/usr/bin/python
# -*- coding: utf-8 -*-

from mako.template import Template
from optparse import OptionParser
import os
import sys
import numpy
from pymatgen.core import periodic_table
from pymatgen.core.structure import Molecule, Structure
from pymatgen.symmetry.structure import SymmetrizedStructure
from matdelab.core.local_analysis import LocalAnalysis
import pprint
from matdelab.dmft.dmft_project import DMFTProject
from matdelab.dmft.w2k_project import W2KProject


template_params_str = """

solver         =  'CTQMC'   # impurity solver
DCs            =  'fixn'    # double counting scheme

max_dmft_iterations = 1     # number of iteration of the dmft-loop only
max_lda_iterations  = 5    # number of iteration of the LDA-loop only, should be about 10*previous
finish         =  50        # number of iterations of full charge loop (1 = no charge self-consistency)

wbroad         =  0.02      # broadening of sigma on the imaginary axis
kbroad         =  0.1       # broadening of sigma on the imaginary axis

ntail          =  500       # on imaginary axis, number of points in the tail of the logarithmic mesh

cc             =  2e-4      # the charge density precision to stop the LDA+DMFT run
ec             =  2e-4      # the energy precision to stop the LDA+DMFT run
recomputeEF    =  1         # Recompute EF in dmft2 step. If recomputeEF = 2, it tries to find an insulating gap.
UpdateAtom     =  0

# Impurity problem number 0
iparams0={"exe"                : ["ctqmc"              , "# Name of the executable"],
          "U"                  : [${U}                 , "# Coulomb repulsion (F0)"],
          "J"                  : [${J}                 , "# Coulomb repulsion (F0)"],
          "nf0"                : [${N}                 , "# Double counting parameter"],
          "beta"               : [${Beta}              , "# Inverse temperature of ${T}K"],
          "Nmax"               : [1000                 , "# Maximum perturbation order allowed"],
          "atom_Nmax"          : [50                   , "# Maximum perturbation order allowed"],
          "M"                  : [25e6                 , "# Total number of Monte Carlo steps, 25M good for 50 processors"],
          "Ncout"              : [1000000              , "# How often to print out info"],
          "Naver"              : [100000000            , "# How often to print out debug info"],
          "nom"                : [${nom}               , "# Number of Matsubara frequency points sampled"],
          "aom"                : [3                    , "# Number of frequency points used to determin the value of sigma at nom"],
          "sderiv"             : [0.02                 , "# Maximum derivative mismatch accepted for tail concatenation"],
          "Ntau"               : [1000                 , "# Number of imaginary time points (only for debugging)"],
          "SampleGtau"         : [3000                 , "# How often to update G(tau)"],
          "GlobalFlip"         : [1000000              , "# How often to try a global flip"],
          "tsample"            : [15                   , "# How often to record measurements"],
          "warmup"             : [2e6                  , "# Warmup number of QMC steps"],
          "CleanUpdate"        : [10000000             , "# How often to make clean update"],
          "minM"               : [1e-10                , "# The smallest allowed value for the atomic trace"],
          "minD"               : [1e-10                , "# The smallest allowed value for the determinant"],
          "PChangeOrder"       : [0.9                  , "# Ratio between trial steps: add-remove-a-kink / move-a-kink"],
          "CoulombF"           : ["'Ising'"            , "# Form of Coulomb repulsion"],
          "OCA_G"              : [False                , "# Don't compute OCA diagrams for speed"],
        }

"""

def is_correlated_atom(el):
    return el.is_transition_metal or el.is_lanthanoid or el.is_actinoid
 
def find_partially_filled_orbital(el, correlated=True):
    es = el.full_electronic_structure
    full_occupation = {'s':2, 'p':6, 'd':10, 'f':14}
    for orb in es:
        if orb[2] == full_occupation[orb[1]]:
            continue
        if correlated and (orb[1] == 's' or orb[1] == 'p'):
            # not a correlated orbital
            continue
        return orb
    # return last orbital
    return orb
 
def main():
    parser = OptionParser()
    parser.add_option("-w", "--w2k_dir", dest="w2k_dir",
                      default = os.curdir,
                      help="Directory where there is a converged w2k calculation, defaults to current directory")
    # PHYSICS parameters
    parser.add_option("-T", "--T_kelvin", dest="T", default=400, type=int, help="Temperature in Kelvin")
    parser.add_option("-U", "--U_eV", dest="U", default=10.0, type=float, help="U in eV")
    parser.add_option("-J", "--J_eV", dest="J", default=1.0, type=float, help="J in eV")
    parser.add_option("-r", "--nn_radius", dest="nn_radius", default=6.0, type=float, help="Radius within which to find nearest neighbors")
    
    parser.add_option("-F", "--offdiagonals", dest="only_diagonal", default=True,  action="store_false")
    parser.add_option("-R", "--R", dest="Rot", default=0.0, type=float, help="Rotate around Z axis (deg)")
    (options, _) = parser.parse_args(sys.argv[1:])
        
    
    template_params = Template(template_params_str, strict_undefined=True)
  
    temp_ev = options.T * 8.621738e-5
    options.Beta = int(1/temp_ev)     #  1/(T in eV)
    options.nom = int(10*options.Beta / (2*numpy.pi))

    w2k_project = W2KProject(options.w2k_dir)
    
    # get the structure
    w2kstruct = w2k_project.struct
   
    # copies the DFT files if we are in a different directory
    dmft_project = DMFTProject.NewProject(".", w2k_project)
    new_indmfl = dmft_project.NewIndmfl()
    
    # Get an indmfl object from the dmft project, so that we can manipulate it
  
    # iterate through the equivalent sites in W2K, because this is the convention DMFT uses
    for equivalence in w2kstruct.equivalent_sites:
        site = equivalence[0]
        el = site.specie
        if is_correlated_atom(el):
            (n,Lspdf,occupation) = find_partially_filled_orbital(el)
            # can't use local analysis from other equivalent atoms
            local_analysis = None
            N = int(round(w2k_project.SCF2Info().atoms_info[site].partial_charges[Lspdf]))
            # DMFT wants this repetition
            new_indmfl.InitEQClass()
            for site in equivalence:
                w2k_index = w2kstruct.atom_index(site)
                print("Adding correlated orbital {0}{1}{2}".format(n,Lspdf,occupation), \
                    "in atom {0}#{1}".format(el, w2k_index), \
                    "to DMFT, with N={}".format(N))
                # we can do the analysis only once for each equivalence class
                if not local_analysis:
                    local_analysis = LocalAnalysis(w2kstruct.PMG_struct, site, 
                                                   options.nn_radius, Lspdf, 
                                                   only_diagonal=options.only_diagonal,
                                                   rotation_angle=options.Rot)
                new_indmfl.AddSubProblem(site, Lspdf, w2k_index, local_analysis)
        # handle multiple correlated atoms if they are the same (only one double counting param is allowed)
        
    #TODO: fix indmfi so that it writes only one matrix per equivalence class.
    # also fix the labels that we create
    new_indmfl.store()

    double_counting = options.U*(N -0.5) - options.J/2.0*(N - 1.0)  # VDC=U*(n-1/2)-J/2*(n-1)
    

    print("writing params.dat")
    params_dat = open("params.dat","w")       
    options.N = N  # for use in **options
    params_content = template_params.render(**options.__dict__)
    params_dat.write(params_content)
    params_dat.close()
    
    
    # use the dmft project to run a command 
    
    # szero likes to see case files, and indmfl, indmfi files around
    dmft_project.run_command("szero.py -e {0} -T {1}".format(double_counting, temp_ev)) 
    

                           
if __name__ == '__main__': 
    main()