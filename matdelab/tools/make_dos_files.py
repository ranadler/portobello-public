#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on Oct 18, 2016

@author: adler
'''
from optparse import OptionParser
import os
from os.path import samefile
import sys

from matdelab.dmft.dmft_project import DMFTProject
from matdelab.tools.make_dmft_files import find_partially_filled_orbital
from matdelab.core.local_analysis import LocalAnalysis
from matdelab.core.function_storage import DMFTSig


def main():
    parser = OptionParser()
    parser.add_option("-d", "--dmft_dir", dest="dmft_dir",
                      default = os.curdir,
                      help="Directory where there is a converged DMFT calculation, defaults to current directory")
    parser.add_option("-r", "--nn_radius", dest="nn_radius", default=8.0, type=float, help="Radius within which to find nearest neighbors")
    parser.add_option("-f", "--full", dest="full_matrix", default=False, action="store_true")
    
    parser.add_option("-x", "--execute", dest="execute", default=False, action="store_true")
    
    (options, _) = parser.parse_args(sys.argv[1:])
  
    if samefile(options.dmft_dir, "."):
        print("You should run this script in a different directory than the DMFT result dir, quitting.")
        return
    
    dmft_proj = DMFTProject(options.dmft_dir)
    new_dmft_project = DMFTProject.NewProjectFromDMFT(".", dmft_proj)

    indmfl = new_dmft_project.NewIndmfl()
    # we want to append to the old computation, not rewrite it
    indmfl.ReadFile(dmft_proj.Filename("indmfl"))

    # get the structure
    w2kstruct = new_dmft_project.w2k_project.struct
    
    if options.full_matrix:
        indmfl.CompleteMatrix()
    else:
        # prepare a new INDMFL which includes ALL the atoms (one per equivalence class)
        for equivalence in w2kstruct.equivalent_sites:
            site = equivalence[0]
            w2k_index = w2kstruct.atom_index(site)   
            # don't overwrite old entries      
            if w2k_index in indmfl.atoms:
                continue
            el = site.specie
            orb = find_partially_filled_orbital(el, correlated=False)
            (n,Lspdf,occupation) = orb
            if Lspdf == 's':
                indmfl.AddSOrbital(site, w2k_index)
            else:
                indmfl.InitEQClass()
                local_analysis = LocalAnalysis(w2kstruct.PMG_struct, site, options.nn_radius, Lspdf)
                indmfl.AddSubProblem(site, Lspdf, w2k_index, local_analysis)
                
    # first write the indmfl file
    indmfl.store()
    
    # reading the old sig with the new indmfl => 
    # will extend the sig with 0's
    # TODO: code this explicitly, where there is an old object with a smaller set of columns
    #       and the new object safely copies columns by name
    new_siginp = DMFTSig(new_dmft_project)
    new_siginp.store()
    
    if options.execute:
        material = new_dmft_project.material
        new_dmft_project.run_command("x lapw0 -f " +  material)
        new_dmft_project.run_command("x lapw1 -f " +  material)
        new_dmft_project.run_command("x_dmft.py dmft1")

if __name__ == '__main__':
    main()

