#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Typically this should be run when the DMFT calculation is converged.
Create a new directory (maybe called conclusion/) and run this inside, with -w pointing
to the DMFT calculation directory
"""

from mako.template import Template
from optparse import OptionParser
import os
import sys
import numpy
import fnmatch
import re

template_params_str = """
params={'statistics': 'fermi', # fermi/bose
        'Ntau'      : 400,     # Number of time points
        'L'         : 20.0,    # cutoff frequency on real axis
        'x0'        : 0.001,   # low energy cut-off
        'bwdth'     : 0.004,   # smoothing width
        'Nw'        : 400,     # number of frequency points on real axis
        'gwidth'    : 2*15.0,  # width of gaussian
        'idg'       : 1,       # error scheme: idg=1 -> sigma=deltag ; idg=0 -> sigma=deltag*G(tau)
        'deltag'    : 0.001,   # error
        'Asteps'    : 4000,    # anealing steps
        'alpha0'    : 1000,    # starting alpha
        'min_ratio' : 0.001,   # condition to finish, what should be the ratio
        'iflat'     : 1,       # iflat=0 : constant model, iflat=1 : gaussian of width gwidth, iflat=2 : input using file model$
        'Nitt'      : 1000,    # maximum number of outside iterations
        'Nr'        : 0,       # number of smoothing runs
        'Nf'        : 40,     # to perform inverse Fourier, high frequency limit is computed from the last Nf points
                               # adler: Nf=40 previously too small to create sharp insulating DOS 
  }
"""

def run_command(str_command):
    print("running", str_command, "in", os.path.abspath("."))
    os.system(str_command)


def add_prefix(root, lst):
    return [os.path.abspath(os.path.join(root,x)) for x in lst]

convert = lambda text: int(text) if text.isdigit() else text.lower() 
alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
 
 
def replace_matsubara_with_real(m):
    return '0'+ m.P__group(2)
 
def main():
    global _counter
    parser = OptionParser()
    parser.add_option("-w", "--dmft_dir", dest="dmft_dir",
                      help="Directory where there is a converged dmft calculation.")
    parser.add_option("-s", "--skip_lapw", dest="skip_lapw",
                      action="store_true",
                      default=False,
                      help="Do not run last steps"
                      )
    parser.add_option("-T", "--T_kelvin", dest="T", default=300.0, type=float, help="Temperature in Kelvin")
    parser.add_option("-U", "--U_eV", dest="U", default=10.0, type=float, help="U in eV")
    parser.add_option("-J", "--J_eV", dest="J", default=1.0, type=float, help="J in eV")
    parser.add_option("-i", "--iteration", dest="iter", type=int, default=-1)
    parser.add_option("-d", "--delta_iter", dest="delta_iter", type=int, default=5)
    
    (options, args) = parser.parse_args(sys.argv[1:])
    
    
    wien_root = "/opt/apps/wien/wien2k_14/"
    dmft_root = "/opt/apps/dmft/bin/"
    os.environ["WIENROOT"] = wien_root
    os.environ["WIEN_DMFT_ROOT"] = dmft_root
    os.environ["PATH"] = ":" + wien_root + ":" + dmft_root + ":" + os.environ.get("PATH","") 
    os.environ["LD_LIBRARY_PATH"] = "/opt/intel/mkl/lib/intel64/:/opt/intel/lib/intel64/:/opt/intel/composer_xe_2013_sp1.5.212/mkl/lib/mic/:/usr/lib/x86_64-linux-gnu/:" + \
                                os.environ.get("LD_LIBRARY_PATH","")
        
    if options.iter == -1:
        start_pos = -1-options.delta_iter+1
        end_pos = None
    else:
        start_pos = options.iter
        end_pos = options.iter+ options.delta_iter

    material = None
    Sig = None
    for root_name, d, files in os.walk(options.dmft_dir):
        if not material:
            for fn in fnmatch.filter(files, "*.struct"):
                material = fn.split(".")[0]
        if not Sig:
            Sig = add_prefix(root_name, sorted(fnmatch.filter(files, "sig.inp.[0123456789]*"), key=alphanum_key)[start_pos:end_pos])


    template_params = Template(template_params_str, strict_undefined=True)
  
    if not os.path.exists("maxent"):
        print("creating work dir: maxent")
        os.mkdir("maxent")
    os.chdir("maxent")
    
    # szero likes to see case files, and indmfl, indmfi files around
    run_command("saverage.py -o sig_average.inp " + " ".join(Sig))        
    params_content = template_params.render(**options.__dict__)
    
    # run maxent
    params_dat = open("maxent_params.dat","w")
    params_dat.write(params_content)
    params_dat.close()
    
    run_command("maxent_run.py sig_average.inp")
    
    if not os.path.exists("Sig.out"):
        print("maxent failed, aborting. You may rerun this program after fixing the problem.")
        exit(1)
    
    # run dmft1 on the real axis after preparing hte files, and running lapw0, lapw1
    os.chdir("..")
    print("preparing real-axis dmft in ", os.path.abspath("."))
    
    # first copy dmft files
    run_command("dmft_copy.py " + options.dmft_dir)
    
    # write a new params file on the real axis
    matsubara_indmfl_file = open(options.dmft_dir + "/" + material +".indmfl", "r")
    real_indmfl_file = open(material +".indmfl", "w")
    line = matsubara_indmfl_file.readline()
    real_indmfl_file.write(line)
    line = matsubara_indmfl_file.readline()
    line = re.sub("^(1)(\s+.*)", replace_matsubara_with_real, line)
    # TODO: also change the energy window for the purpose of optics / transport
    real_indmfl_file.write(line)
    real_indmfl_file.write(matsubara_indmfl_file.read())
    real_indmfl_file.close()
    matsubara_indmfl_file.close()
    
    # write sig.inp as a copy of maxent's Sig.out
    sig_inp = open("sig.inp","w")
    sig_inp.write(open("maxent/Sig.out", "r").read())
    sig_inp.close()
    
    
    if not options.skip_lapw:
        # running lapw
        run_command("x lapw0 -f " + material)
        run_command("x lapw1 -f " + material)

        # running dmft1
        run_command("x_dmft.py dmft1")
                           
if __name__ == '__main__': 
    main()