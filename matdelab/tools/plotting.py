#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on Oct 14, 2016

@author: adler@physics.rutgers.edu
'''



from bokeh.plotting.figure import figure
from collections import OrderedDict
from bokeh.palettes import Dark2_8, Paired12
from bokeh.models.annotations import Label
from hashlib import md5 

tools_str="wheel_zoom,pan,reset,resize"
GOOD_COLORS = ["lime", "red","indigo","gold",
               "darkred", "darkkhaki",
               "darkolivegreen",
               "darkorange","chocolate", 
               "blue","darkgray","black",
               "greenyellow", "chartreuse", "purple", "darkred","seagreen",
               "pink", "darkcyan", "fuchsia", "darkblue", "cyan","sienna"]

class SimplePlot(object):
    def __init__(self,x_column, tools, width, height, x_range=None, y_range=None):
        self.x_column = x_column
        self.width = width
        self.height = height
        self.x_range = x_range
        self.y_range = y_range
        self.graphs = OrderedDict()
        self.colors = {}
        self.lines = OrderedDict()
        self.fig = None
        self.tools_str = tools
    
    def DrawLine(self,fig, df, x_column, y_column, label):
        #print "draw line", x_column, y_column, label
        self.lines[y_column] = fig.line(x=df[x_column],y=df[y_column], 
                                    line_width=2,line_alpha=1.0,
                                    legend=label, line_color=self.colors[label])
    
    def Hash(self, name):
        return hash(name)
    
    def Visit(self, function_storage, df, column_name, name, version):
        if self.fig is None:
            self.fig= figure(width=self.width, height=self.height,x_range=self.x_range,
                             tools=self.tools_str, y_range=self.y_range)
            self.fig.toolbar.logo = None
            self.graphs['singleton'] = self.fig
        if name not in self.colors:
            h = self.Hash(column_name)
            self.colors[name] = GOOD_COLORS[h % len(GOOD_COLORS)]
            #print h, h % len(GOOD_COLORS), name, self.colors[name]
        self.DrawLine(self.fig, df, self.x_column, column_name, name)
    
    def VisitAllVariables(self, table_in_text,reg_exp):
        table_in_text.accept(self, reg_exp)
    
    def Generate(self, table_in_text, reg_exp):
        self.VisitAllVariables(table_in_text, reg_exp)
        ret = list(self.graphs.values())
        self.graphs.clear()
        return ret
    def Graph(self):
        return self.graphs['singleton']
    
class GraphArray(SimplePlot):
    def Visit(self, function_storage, df, column_name, name, version):
        if name in self.graphs:
            fig = self.graphs[name]
        else:
            fig = figure(width=self.width, height=self.height,x_range=self.x_range, tools=tools_str, y_range=self.y_range)
            self.graphs[name] = fig
        if version not in self.colors:
            self.colors[version] = GOOD_COLORS[len(list(self.colors.keys())) % len(GOOD_COLORS)]
            
        fig.title = name
        self.DrawLine(fig, df, self.x_column, column_name, version)
        fig.toolbar.logo = None
            
class GraphArrayOnIndex(GraphArray):
    def DrawLine(self,fig, df, x_column, y_column, version):
        fig.line(x=df.index,y=df[y_column], line_width=2, line_alpha=1.0,
                legend=version, line_color=self.colors[version])
        fig.xaxis.axis_label = "Matsubara Index"
            
            