#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
'''
Created on Dec 19, 2016

@author: adler
'''
from optparse import OptionParser
import os
import glob
import sys
from pandas.core.frame import DataFrame
from bokeh.plotting.figure import figure
from bokeh.io import show, output_file
from bokeh.models.layouts import Column
import re
from matdelab.tools.plotting import GOOD_COLORS
from collections import OrderedDict

tools_str="wheel_zoom,pan,reset,resize,save"
#tools_str="save"

if __name__ == '__main__':
    
    parser = OptionParser()
    parser.add_option("-b", "--base", dest="base_filename", default="RESULTS")
    parser.add_option("-x", "--x", dest="x", default="T")
    parser.add_option("-i", "--ignore", dest="ignore", default="case")
    parser.add_option("-A", "--auxiliary-files", dest="aux_files", default="EF.dat,Edc.dat")
    
    
    (options, files) = parser.parse_args(sys.argv[1:])
    
    ignored_labels = options.ignore.split(",")
    
    aux_files = []
    if options.aux_files:
        aux_files = options.aux_files.split(",")
    
    print("plotting "+ " ".join(files))
    
    rows = {}
    legends = {}
    titles = {}
    sis = {}
    for fn in files:
        print("reading file "+ fn)
        f = open(fn, 'r')
        # read subindex from filename
        si = 0
        all_indices = re.findall(r'\d+', fn.split(options.base_filename)[1])
        if len(all_indices) > 0:
            si = all_indices[-1]
        sis[si] = True 
        
        X = None
        row = OrderedDict()
        row['si']=si
        for line in f.readlines():
            line = line.rstrip()
            values = line.split()
            label=values[0]
            if label in ignored_labels:
                continue
            val = float(values[1])
            units = ""
            comment = ""
            if len(values) > 2:
                units = values[2]
                if len(values) > 3:
                    comment = " ".join(values[3:])
                
            
            if label == options.x:
                X = val
                
            row[label] = val #(val,units,comment)
                
            if label =='T':
                row['beta'] = 1/( 8.621738e-5 * float(val))
            if label =='\sigma(0)':
                row['\rho'] = 1/(float(val))
            if options.x == 'beta':
                X = val
                
            legends[label] = label + " ["+units+"]"
            legends['beta'] = 'beta [1/eV]'
            legends['\rho'] = 'Ohm*cm'
            title = label
            if comment and comment != "":
                title = comment.lstrip("# ")
            titles[label] = title
            titles['beta'] = '\beta'
            titles['\rho'] = '\\rho'
                
        rows[str(X)+'#'+str(si)] = row
        
    df = DataFrame(columns=list(row.keys()))
    for i, row in enumerate(rows.keys()):
        df.loc[i] = list(rows[row].values())
    df = df.sort("T")
    print(df)             

    figures = []
    for column in df.columns:
        if column == options.x or column == 'si':
            continue
        fig = figure(width=600, height=400,
                     title=titles[column],
                     tools=tools_str)
        fig.xaxis.axis_label = legends[options.x]
        fig.yaxis.axis_label = legends[column]
        fig.toolbar.logo=None
        
        for si in list(sis.keys()):
            fig.line(x=df[df.si == si][options.x], y=df[df.si==si][column],
                     line_width=1,line_alpha=1.0, #legend="blah",
                     legend=('(x+y+z)/3','x','y','z')[int(si)],
                     line_color=GOOD_COLORS[int(si)])
            fig.circle(x=df[df.si == si][options.x], y=df[df.si==si][column])
        figures.append(fig)
        

    
    g = Column(*figures)
         
    output_file("/tmp/dir-plot.html", title="")

    print("Openning page in browser\n")
    show(g,browser="google-chrome")
            
    