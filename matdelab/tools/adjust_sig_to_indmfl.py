#!/usr/bin/python
# -*- coding: utf-8 -*-

'''
Created on Nov 3, 2016

Given a DMFT directory with a result, and a new INDMFL file, make adjustment in the 
self energy file to reflect the new INDMFL:
 - if INDMFL has extra columns - set them to 0.0+0.0j in the sig
 - if some columns are dropped - drop the corresponding columns in the sig

@author: adler
'''
from optparse import OptionParser
import os

from matdelab.dmft.dmft_project import DMFTProject
from matdelab.core.function_storage import DMFTSig
import sys



def main():
    parser = OptionParser()
    parser.add_option("-d", "--dmft_dir", dest="dmft_dir",
                      default = os.curdir,
                      help="Directory where there is a converged DMFT calculation, defaults to current directory")

    (options, _) = parser.parse_args(sys.argv[1:])
    
    # first back up the INDMFL file, copy_dmft.py will write over it    
    dmft_proj = DMFTProject(options.dmft_dir)

    os.system("cp {0}.indmfl indmfl_backup".format(dmft_proj.material))
    new_dmft_project = DMFTProject.NewProjectFromDMFT(".", dmft_proj)
    
    #restore the INDMFL file
    os.system("cp indmfl_backup {0}.indmfl".format(dmft_proj.material))
    
    # Adjust siginp
    new_siginp = DMFTSig(new_dmft_project)
    new_siginp.store()
    
    
    
if __name__ == '__main__':
    main()
    