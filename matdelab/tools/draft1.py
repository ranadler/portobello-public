#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
'''
Created on Oct 1, 2016

@author: adler@physics.rutgers.edu

Design Goals:

 - Atoms are designated by label of element and position (in struct) (O1, Co, S2),
 - Atom X will designate *all* atoms with that elements unless the position is specified.
 - We want to define "what" to plot, rather than the implementation of where find it. 
   The actual details are provided by the implementation, and needs not be specified by the user.
 - In the same way we don't want the user to count indices to identify Real or Img parts, it is done
   by the implementation.
 - Also labels on the graph are using to the "what-to-plot" designation, not the implementation details.
 - Simple algebraic operations +-*/ and some constants (e, pi) should be supported
 
 Recognized symbols:

      DMFT - designates the DMFT context
      GA   - designates the GA context
     
      sigma  - self energy
      rho  - density of states
      g    - Green's functions of impurity
      G    - total Green's function
      
  Labels (interpreted in context):
      - atomic labels
      - orbital labels
          - index-addressible
          - should be named (for graphing)
          

Implementation:
  
     - How should mixed context taken into account?
     - How should multiple labels (atom, orbital) be specified?
     - How should the expression be translated to latex (or vice-versa)
  
'''

from bokeh.io import output_file, show
from bokeh.plotting import figure
import numpy as np
from pandas.core.frame import DataFrame
from optparse import OptionParser
import sys
import os
import re
import colorsys
from .dashboard import get_colors
import glob
from dmft.W2KProject import W2KProject

if __name__ == '__main__':
    pass


    show(fig, browser="google-chrome")
