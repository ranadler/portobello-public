#!/usr/bin/python2.7
# -*- coding: utf-8 -*-
"""
"""



from bokeh.charts.builders.boxplot_builder import BoxPlot
from bokeh.io import output_file, show, gridplot
from bokeh.models.layouts import Row, Column
from bokeh.models.widgets.markups import Div
from bokeh.plotting import figure
import subprocess
from optparse import OptionParser
import os
from pandas.core.frame import DataFrame
from random import randint
import re
import subprocess
import sys

from matdelab.core.function_storage import DMFTSig, DMFTG, DMFTDelta, DMFTSigOut, \
    DMFTSigB, DMFTGImp, DMFTGc, DMFTcDOS
from matdelab.dmft.dmft_project import DMFTProject
from matdelab.tools.plotting import tools_str, SimplePlot, GraphArray,\
    GraphArrayOnIndex
import numpy as np
from os.path import exists


def plot_prob(Prob, treshold):
    L_col = 'L'.encode('utf-8')
    p_col = 'p'.encode('utf-8')
    df = DataFrame()
    for filename in Prob:
        print('reading ' + filename)
        gft = np.genfromtxt(filename, dtype=[(L_col,int),('X'.encode('utf-8'),int),(p_col,float)])
        dfr = DataFrame(gft)
        #df = df.append(dfr[dfr.L < mx1][dfr.L >= mn1])
        df = df.append(dfr[dfr.p > treshold])
    if len(Prob):
        bp = BoxPlot(df, width=550, height=450, tools=tools_str, values=p_col,xscale='linear', label=L_col, title="Impurity Prob.", xlabel="Energy Level",legend=None) #, x_range=Range1d(start=mn, end=mx)
        bp.toolbar.logo = None
        return bp
    else:
        return None

def plot_info(Info):
    if not Info or not exists(Info):
        return [None,None]
    print('reading ' + Info) 
    gft = np.genfromtxt(Info,dtype=float,skip_header=1)
    #print gft
    if gft.size < 1 or gft[:,0].size < 1:
        return [None,None] # no data yet
    fig1 = figure(width=550, height=450, tools=tools_str)
    fig1.title = "μ"
    if gft.shape[1] > 7:
        fig1.line(gft[:,0], gft[:,3], line_color='olive', line_width=2)
    else:
        fig1.line(gft[:,0], gft[:,2], line_color='olive', line_width=2)
    
    fig2 = figure(width=550, height=450, tools=tools_str)
    if gft.shape[1] > 7:
        fig2.title = "N_latt vs. N_imp"
        fig2.line(gft[:,0], gft[:,8], line_color='olive', line_width=2)
        fig2.line(gft[:,0], gft[:,9], line_color='olive', line_dash='4 4', line_width=2)
    else:
        fig2.title = "nf"
        fig2.line(gft[:,0], gft[:,6], line_color='olive', line_width=2)
        
    
    fig1.toolbar.logo = None
    fig2.toolbar.logo = None
    #fig1.background_fill_color = "beige"
    #fig2.background_fill_color = "beige"
    return [fig1,fig2]
    
def plot_histogram(Hist):
    if not Hist or not exists(Hist):
        return None
    gft = np.genfromtxt(Hist,dtype=float)
    fig1 = figure(width=550, height=450, tools=tools_str)
    fig1.title = "Histogram"
    fig1.line(gft[:,0], gft[:,1], line_color='olive', line_width=2)
    fig1.toolbar.logo = None
    return fig1

ratio_re = re.compile('Ratio\s+to\s+renormalize=\s*(\d*.\d*) rho-rho_expected=\s*(\d*.\d*)')
def plot_convergence(Conv):
    if not Conv or not exists(Conv):
        return [None,None]
    f = open(Conv, 'r')
    i = []
    r = []
    d = []
    for line in f.readlines():
        match = ratio_re.match(line)
        if match:
            i.append(len(i))
            r.append(float(match.P__group(1)))
            d.append(float(match.P__group(2)))
    f.close()
    fig1 = figure(width=550, height=450,tools=tools_str)
    fig1.line(i, r, line_color='black', line_width=2)
    fig1.title = "Normalization"
    fig2 = figure(width=550, height=450,tools=tools_str)
    fig2.line(i, d, line_color='black', line_width=2)
    fig2.title = "ρ - ρ{0}".format(chr(0x2091))
    fig1.background_fill_color = "whitesmoke"
    fig2.background_fill_color = "whitesmoke"
    fig1.toolbar.logo = None
    fig2.toolbar.logo = None
    return [fig1,fig2]


def FindJob(options):
    command = "/usr/bin/ssh adler@rupc09 'source /opt/sge6/core/common/settings.sh;qstat'"
    _, out = subprocess.getstatusoutput(command)
    for line_no, line in enumerate(out.split("\n")):
        if line_no < 2:
            continue
        jobid, _, label, user, _, _, _, queue, _=  line.split()
        if label == options.job_label:
            node = queue.split('@')[1]
            print("found job", jobid, node)
            return (jobid, node, user)

def main():
    parser = OptionParser()
    parser.add_option("-c", "--conclusion_dir", dest="conclusion_dir", default=None)
    parser.add_option("-i", "--iteration", dest="iter", default=-1, type=int)
    parser.add_option("-d", "--delta_iter", dest="delta_iter", type=int, default=5)
         
    parser.add_option("-r", "--results_dir", dest="results_dir")#, default="/home/adler/work/BaCoSO/DMFT/converged-high-T/")
    parser.add_option("-l", "--job_label", dest="job_label")
    parser.add_option("-n", "--node", dest="node", type=str)
    parser.add_option("-j", "--job_id", dest="jobid", type=str)
    
    # enter something in -x if you want to select only some of the orbitals to display
    parser.add_option("-x", "--orbita_regexp", dest="orbital_regexp", default="")
       
    (options, _) = parser.parse_args(sys.argv[1:])
    
    username = 'adler' # job user name, by default
    
    if options.job_label and not options.results_dir:
        jobid, node, username = FindJob(options)
        options.jobid = jobid
        options.node = node
    
    if options.conclusion_dir:
        base_dir = options.conclusion_dir
    elif options.results_dir:
        base_dir = options.results_dir
    elif options.node:
        mnt_point = os.path.join("mnt/", options.node)
        mnt_dir = os.path.join("/home/adler/", mnt_point)
        if not os.path.exists(mnt_dir):
            print("making dir", mnt_dir)
            os.mkdir(mnt_dir)
        
        base_dir = os.path.join(mnt_dir, options.jobid + username)
        if not os.path.exists(base_dir):
            print("mounting dir", mnt_dir)
            subprocess.check_call(["/usr/bin/sshfs", 
                               "adler@rupc-caip-03:/" + mnt_point, 
                                mnt_dir,
                                "-o", "ro,nosuid,nodev,delay_connect,reconnect,uid=1000,gid=1000,idmap=user,sftp_server=/usr/lib/ssh/sftp-server"])
            print("reading dir", base_dir)
          
    dmft_proj = DMFTProject(base_dir)
    material = dmft_proj.material
    
    xcol = dmft_proj.Indmfl.XVariableName() 
        
    if not options.conclusion_dir:
        iterations = dmft_proj.GetIterations(options.iter, options.delta_iter)
    print("tail -f " + dmft_proj.dir_name + "/:log")
    dmft_proj.run_command("tail :log")
       
    html_title = "DMFT Dashboard for {0} in {1}".format(material, base_dir)
        
    if options.jobid:
        output_file("/tmp/dashboard-" + material + "-jobid=" + options.jobid +".html", title=html_title)
    else:
        output_file("/tmp/dashboard-" + material + "-" + str(randint(1000000,9999999)) + ".html", title=html_title)
        
    indmfl = dmft_proj.Indmfl
    iparams0 = dmft_proj.iparams0()
    job_label = "dir"
    if options.job_label:
        job_label = options.job_label
    
    Title = Div(text="""<h1>{M}({L})</h1><h2>{D}</h2><h2>beta={B}</h2><h2>U={U}</h2><h2>J={J}</h2><h2>x={X}</h2>""".format(
            L=job_label,
            M=material, D=os.path.abspath(base_dir),B=iparams0.beta,
            U=iparams0.U,J=iparams0.J,X=xcol))
    Title.width = 1000
        
        
    if options.conclusion_dir:
        cdos_table = DMFTcDOS(dmft_proj)
        
        sig_table = DMFTSig(dmft_proj)
        g_table = DMFTG(dmft_proj, normalize_img=True)
        delta_table = DMFTDelta(dmft_proj)
        
        print("Building graphs ...\n")
        dos_graph = SimplePlot(xcol, width=1200, height=450, tools=tools_str,x_range=[-5.0, 5.0])
        g_re_graph = SimplePlot(xcol, width=1000, height=450, tools=tools_str,x_range=[-5.0,5.0])
        g_im_graph = SimplePlot(xcol, width=1000, height=450, tools=tools_str,x_range=[-5.0,5.0])
        sig_re_graph = SimplePlot(xcol, width=1000, height=450, tools=tools_str,x_range=[-5.0,5.0])
        sig_im_graph = SimplePlot(xcol, width=1000, height=450, tools=tools_str,x_range=[-5.0,5.0])
        del_re_graph = SimplePlot(xcol, width=1000, height=450, tools=tools_str,x_range=[-5.0,5.0])
        del_im_graph = SimplePlot(xcol, width=1000, height=450, tools=tools_str,x_range=[-5.0,5.0])
        #delta_graph = ('ω', width=400, height=400, tools=tools_str,x_range=[-5.0,5.0])
       
        grid = [
            [Title],
            dos_graph.Generate(cdos_table, '.*'),
            
            g_re_graph.Generate(g_table, 'Re .*' + options.orbital_regexp + '.*'), 
            g_im_graph.Generate(g_table, '-Im .*' + options.orbital_regexp + '.*'),
            sig_re_graph.Generate(sig_table, 'Re .*' + options.orbital_regexp + '.*'),
            sig_im_graph.Generate(sig_table, 'Im .*' + options.orbital_regexp + '.*'),
            del_re_graph.Generate(delta_table, 'Re .*' + options.orbital_regexp + '.*'),
            del_im_graph.Generate(delta_table, 'Im .*' + options.orbital_regexp + '.*')  
        ]
    
         
    else:
              
        info_file = dmft_proj.Filename1("info.iterate")
        pli = plot_info(info_file)
        if not pli[0]:
            print("No data yet.")
            return
        
        log_file = dmft_proj.Filename1("dmft2_info.out")
        prob_files = [dmft_proj.Filename1("imp.0/Probability.dat.") + i for i in iterations ]
        histogram_file = dmft_proj.Filename1("imp.0/histogram.dat")
                    
        img_graph = GraphArray(xcol, width=550, height=450, tools=tools_str,x_range=[0.0,10.0])
        index_graph = GraphArrayOnIndex(xcol, width=550, height=450, tools=tools_str,x_range=[0.0,10.0])
        
        sig_table = DMFTSigOut(dmft_proj, iterations)
        sigb_table = DMFTSigB(dmft_proj)
        gf_table = DMFTGImp(dmft_proj, iterations, normalize_img=False)
        gc_table = DMFTGc(dmft_proj, iterations, normalize_img=False)
        cdos_table = DMFTcDOS(dmft_proj, iterations)
        
        
        grid = [
            [Title],
            pli +  [plot_prob(prob_files, 0.005)],
            plot_convergence(log_file) + 
            [plot_histogram(histogram_file)],
                    
            img_graph.Generate(sig_table, 'Re .*'),
            img_graph.Generate(sig_table, 'Im .*'),
            
            index_graph.Generate(sig_table, 'Re .*'),
            index_graph.Generate(sig_table, 'Im .*'),
            
            img_graph.Generate(sigb_table, 'Re .*'),
            img_graph.Generate(sigb_table, 'Im .*'),
                
            #img_graph.Generate(gf_table, 'Re .*'),
            #img_graph.Generate(gf_table, 'Im .*'),
                    
            #img_graph.Generate(gc_table, 'Re .*'),
            #img_graph.Generate(gc_table, 'Im .*'),
            
            
            img_graph.Generate(cdos_table, '.*'),
         ] 
  
    g = Column(*[Row(*row) for row in grid])
 
    print("Openning page in browser\n")
    show(g,browser="google-chrome")
    

if __name__ == '__main__': 
    main()
    
