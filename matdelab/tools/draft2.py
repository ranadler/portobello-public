#!/usr/bin/python

from bokeh.io import output_file, show, curdoc, set_curdoc
from bokeh.plotting import figure
import numpy as np
from pandas.core.frame import DataFrame
from optparse import OptionParser
import sys
import os
import re
import colorsys
import glob
from matdelab.dmft.w2k_project import W2KProject
from bokeh.palettes import Spectral11, Spectral10, Set3_12
from bokeh.layouts import row, column
from bokeh.layouts import column as clmn
from bokeh.models.widgets.groups import CheckboxGroup
from bokeh.client.session import push_session
from bokeh.models.annotations import Legend
from bokeh.models.widgets.markups import Div
from bokeh.models.widgets.buttons import Button
from bokeh.models.sources import ColumnDataSource
from collections import OrderedDict


def main():
    parser = OptionParser()
    parser.add_option("-r", "--recompute", dest="recompute_graph", default=False)
    #parser.add_option("-i", "--iteration", dest="iter", default=-1)
    #parser.add_option("-d", "--delta_iter", dest="delta_iter", type=int, default=5)
    parser.add_option("-w", "--results_dir", dest="results_dir", default=".")
       
    (options, args) = parser.parse_args(sys.argv[1:])
    
    if options.results_dir:
        root_name = os.path.abspath(options.results_dir)
    else:
        if args:
            root_name = os.path.abspath(args[0])
        else:
            raise  Exception("no results directory given")
            
    title = "DOS Plot " + root_name
    
    w2k_proj = W2KProject(root_name)
    material = w2k_proj.material
    struct = w2k_proj.struct
     
    #===========================================================================
    #  recompuation for up/down case:
    #
    # if options.recompute_graph:
    #     print "-- in", root_name, "subject is:", material, ".inq file is:"
    # 
    #     inq_file = os.path.join(root_name, material + ".inq")
    #     
    #     w2k_proj.run_command("cat " + inq_file)
    #     print "-- calling x qtl -up"
    #     w2k_proj.run_command("x qtl -up")
    #     print "-- calling x qtl -dn"
    #     w2k_proj.run_command("x qtl -dn")
    #     
    #     # qtl (partial charge) files are present.
    #     # read qtldn, qtlup header
    #     # create a file *.int with the orbitals we want tetra to project on
    #     #     for now use the default
    #     int_file = os.path.join(root_name, material + ".int")
    #     print "-- input file for tetra is ", int_file
    #     w2k_proj.run_command("cat " + int_file)
    #     print "-- calling x tetra -up"
    #     w2k_proj.run_command("x tetra -up")
    #     print "-- calling x tetra -dn"
    #     w2k_proj.run_command("x tetra -dn")
    #===========================================================================
        
    
    # Now .dos1ev* files should be present
    dos_files = [os.path.join(root_name, filename)  
                 for filename in  glob.glob1(root_name, "*"+ material + ".dos*ev")] #".dos*ev??"
    print("dos_files", dos_files)             
    column_re = re.compile('(\d+):(\D.*)')
   
    fig = figure(width=1300, height=1000, x_range=[-7.0,7.0])
    #fig.ygrid.band_fill_color="olive"
    #fig.ygrid.band_fill_alpha = 0.1
    fig.xaxis.axis_label = 'Energy (eV)'
    #fig.grid.grid_line_alpha=0
    fig.title.text = material + " DOS " + root_name
   
    dfrs = DataFrame()
    orbitals = {}
    all_columns = {}
    
    for dos_file in dos_files:
        #print "-- reading file", dos_file  
        #os.system("head "+ dos_file)
        gft = np.genfromtxt(dos_file,dtype=float,skip_header=2,
                            names=True,deletechars=r"#")
        dfr = DataFrame(gft)
        new_columns = []
        for column in dfr.columns:
            match = column_re.match(column)
            if match:
                anum = int(match.P__group(1)) - 1
                #label = struct.nonequiv_atoms[anum].label[0:2]
                label = str(anum+1) + str(struct.pmg_struct.equivalent_sites[anum][0].specie)
                new_column_name = label + ' '+ match.P__group(2)
                orbitals[new_column_name] = match.P__group(2)[0:1]
            else:
                # "ENERGY" or "TOTAL"
                label = column[0:2]
                new_column_name = column
                orbitals[new_column_name] = column[0:1]
                
            new_columns.append(new_column_name)
            plabel = label + ' '
            all_columns[new_column_name] = 0
                
        #print dfr.head()
        dfr.columns = new_columns
        if dos_file.find("evdn") != -1:
            for column in dfr.columns:
                if column != "ENERGY":
                    dfr[column] = -dfr[column]
        if dfrs.empty:
            dfrs = dfr
        else:
            dfrs = dfrs.merge(dfr)
    
    # Now read DMFT DOS files, if they exist
    
    
    cds = ColumnDataSource(dfrs)
    
    # Add lines for W2K orbital characters
    lines = OrderedDict()
    for idx, column in enumerate(dfrs.columns):
        if column != "ENERGY":
            line = fig.line(source=cds,
                            x='ENERGY', y=column, 
                            line_width=2, #widths[orbitals[column]],
                             #line_dash= [2,4],
                             legend=column, 
                             line_color=Set3_12[idx %11 + 1])
            lines[line] = column
    
    line_legends = {line: fig.legend[0].legends[i] for i, line in enumerate(lines.keys())} 
    
    checkbox_title = Div(text="<h3>KS orbitals</h3> ", width=150)
    checkbox = CheckboxGroup(labels=list(dfrs.columns[1:]), active=[ix for ix,_ in enumerate(lines.keys())], width=100)
    
    def update(attr, old, new):
        print(attr)
        # we manually maintain the legends because of Bokeh bugs
        new_legends = []
        for ix, line in enumerate(lines.keys()):
            line.visible = ix in checkbox.active
            if line.visible:
                new_legends.append(line_legends[line])
        fig.legend[0].legends = new_legends
        # force the legend to redraw - setting line visibility does it
        vis = line.visible
        line.visible = not vis
        line.visible = vis
        
    checkbox.on_change('active', update)
 
    sum_title = Div(text="<h3>Sums</h3> ", width=150)
    sum_checkbox = CheckboxGroup(labels=list(dfrs.columns[1:]), width=100)
    sum_button = Button(label="Add line", button_type="success")

    def make_sum():
        to_sum = []
        for ix, line in enumerate(lines.keys()):
            if ix in sum_checkbox.active:
                to_sum.append(lines[line])
        if len(to_sum) == 0:
            return
        name = "+".join(to_sum)
        dfrs[name] = dfrs[to_sum[0]]
        for c in to_sum[1:]:
            dfrs[name] += dfrs[c] 
        line = fig.line(dfrs['ENERGY'], dfrs[name], 
                        line_width=2, #widths[orbitals[column]],
                         #line_dash= [2,4],
                         legend=name,
                         line_dash= [2,4], 
                         line_color="black")
        lines[line] = name
        line_legends[line] = fig.legend[0].legends[-1]
        checkbox.labels.append(name)
        #checkbox.active.append(True)

    sum_button.on_click(make_sum)
      
    layout = row(clmn(checkbox_title, checkbox) ,fig, clmn(sum_title, sum_checkbox, sum_button))
    curdoc().add_root(layout)
 
 
    session = push_session(curdoc())
    session.show()
    session.loop_until_closed()
    
    #show(layout, browser="google-chrome")

if __name__ == '__main__': 
    main()
