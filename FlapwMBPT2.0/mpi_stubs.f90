

subroutine dgop(x, lenx, op, comm)
    use parallel_mod
    use mpi
    implicit none
    CHARACTER*3, intent(in) :: OP
    integer, intent(in) :: comm
    integer, intent(in) :: lenx
    integer :: ierror
    DOUBLE PRECISION, intent(inout) :: X(LENX)
    if (.not.goparr) return
    IF(OP(3:3).EQ.'+') THEN
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,x,lenx,MPI_DOUBLE_PRECISION, MPI_SUM, comm, ierror)
    else IF(OP(1:3).EQ.'min') THEN
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,X,1,MPI_DOUBLE_PRECISION,MPI_MIN,comm,IERROR)
    else IF(OP(1:3).EQ.'max') THEN
      CALL MPI_ALLREDUCE(MPI_IN_PLACE,X,1,MPI_DOUBLE_PRECISION,MPI_MAX,comm,IERROR)
    ELSE
      IF (MASWRK) THEN
        WRITE(6,*) 'DGOP/TCGSTB DOES NOT SUPPORT GLOBAL OP',OP
      END IF
      CALL MPI_ABORT(comm, 30, ierror)
    END IF
end subroutine