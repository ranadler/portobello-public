#!/usr/bin/python3

# Do not push this file!
# Change it locally (in a stash).
#
# This is a very simple script that should just print out the production directory (on rupc02 and nodes)
# This choice is used when synchronizing code, or when submitting a job through qsub-rho.
# if this file is removed, synchronization and job submission are disabled.
# Also note that typically one should have a different definition for branches in git, which are tested in different locations.
# Typically users will have their own definition of this (which is why it should be kept out of git push)

if __name__ == '__main__':
    print("/home/adler/portobello/")


