
//#include "__for_debug.h"

#include "base_types.h"
#include "LAPW.h"
#include "atomic_orbitals.h"

namespace wannier {

    struct WannierInput : atomic_orbitals::OrbitalsBandProjector {
        int num_k_full; // confusingly in the base class, num_k_all refers to num_k in the shard only
        
        base_types::Window dis_window;     // this is the disentanglement window

        string location_of_partial_bs_all_k;

        // we compute the Wannier A matrix in python from the orbitals
        cube<complex>   A{max_band-min_band+1, num_orbs, num_k_full};

        BusObject& Compute(const string& outputpath) __attribute__((section("FORTRAN,wannier_driver")));
    };

    struct WannierOutput :atomic_orbitals::OrbitalsBandProjector {

        vector<int> nearest_atom{num_orbs};
        matrix<float> distance_to_nearest_atom{3, num_orbs};
        matrix<float> centers{3, num_orbs};

        BusObject& WriteXSF() __attribute__((section("FORTRAN,visualize_orbitals")));
    };

    


}
