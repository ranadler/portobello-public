#ifndef MODELS_H_INCLUDED
#define MODELS_H_INCLUDED

#include "atomic_orbitals.h"
#include "PEScf.h"

namespace models {

/*
The purpose of the ModelShell is to let a lot of code be re-used 
However, quite a bit of atomic shell is not used in model shell
*/
struct ModelShell : atomic_orbitals::AtomicShell {

    vector<int> correlated_orb_indices{dim};
    

};

struct ModelState{

    string model_type = "wannier90"; //Default to this to deal with old runs which don't have this saved

};

}

#endif