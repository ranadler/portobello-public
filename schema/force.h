#ifndef FORCE_H_INCLUDED
#define FORCE_H_INCLUDED

namespace force {

struct FlapwForce {
    int num_atoms;
    int num_cont=5;


    cube<float> force_cont{3,num_atoms,num_cont};


    void CalculateForce()
        __attribute__((section("FORTRAN,flapw_driver")));
};

};

#endif // FORCE_H
