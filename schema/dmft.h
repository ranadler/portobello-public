#ifndef DMFT_H_INCLUDED
#define DMFT_H_INCLUDED

#include "self_energies.h"
#include "atomic_orbitals.h"
#include "base_types.h"
#include "PEScf.h"

namespace dmft {

    struct DMFTFreeEnergy{

        int num_temperatures;
        vector<float> temperatures{num_temperatures};
        vector<float> internal_energies{num_temperatures};

        float entropy;
        float result;

        bool ready;

    };

    struct DMFTState : public PEScf::SolverState {

        // note that this has to be defined for plotting (in real space)
        self_energies::LocalOmegaFunction sig;
        self_energies::LocalOmegaFunction hyb;

        self_energies::LocalOmegaFunction GImp;

    	int num_freqs;

        float sign; // last sign of the DMFT run
        float measurement_time; // time spent measureing the last DMFT result
    };

    struct ImpurityProblem : public PEScf::SolverProblem {

        string os_dir = "./Impurity";

    	int num_freqs;  // to be used when building objects

    	string hamiltonian_approximation = "ising"; // ising of none

    	float max_sampling_energy = 10.0; // should this be adaptive? 10.0 seems to work across the board

        int two_body_size;
        vector<complex> two_body{two_body_size};

        self_energies::LocalOmegaFunction hyb;
        // not necessary: self_energies::LocalOmegaFunction sig;

    };

    struct ImpurityMeasurement : public PEScf::SolverSolution {
    	int num_freqs;
    	int dim;
    	int num_si;
    	int num_orbs;
    	int num_x = 20; // in GAux(iw) continuation, this is the number of sampled frequencies

    	cube<complex> RhoImp{dim, dim, num_si};

        self_energies::LocalOmegaFunction GImp;
        self_energies::LocalOmegaFunction Sigma;
        self_energies::LocalOmegaFunction OccSusc;

        // co-variance matrix of GAux measurement between the "bins" (workers that is)
        // GAux should be calculated as an average among workers, and this is the (complex)
        // product of averages (unbiased) of the abs difference from the average GAux.
        // it is used in analytical continuation.
        array4<complex> CoVariance{num_x, num_x, num_orbs, num_si};

        // GAux = 1/(omega - (Sigma - Sigma_inf)) is used in analytical continuation
        self_energies::LocalOmegaFunction GAux;


        float sign;
        float lnZ;
        float k;

        int num_samples;
        vector<float> expansion_histogram{num_samples};

        float relative_error;
    };

};



#endif // DMFT_H_INCLUDED
