#ifndef BASIS_IDS_H_INCLUDED
#define BASIS_IDS_H_INCLUDED

namespace basis_ids {

struct BasisIds {

    int muffin_tin = 0;

    int bands = 0;

    int correlated_orbitals = 0;

    int k_mesh = 0;

    int omega_mesh = 0;


};

}


#endif
