
#ifndef CTQMC_INTERNAL_H_INCLUDED
#define CTQMC_INTERNAL_H_INCLUDED

namespace CTQMC_internal {

struct ComplexArray {
    int num;
    vector<float> imag{num};
    vector<float> real{num};

};


struct Hyb : ComplexArray {
    float beta;
};


struct ComplexSquareMatrix {
    int dim;
    matrix<float> imag{dim,dim};
    matrix<float> real{dim,dim};
};


struct BasisStruct {
    string orbitals;
    ComplexSquareMatrix transformation;
    string type = "product";
};

struct QuantumNumbersPlaceHolder {
};

struct CTQMCObservable {
    ComplexSquareMatrix one_body;
};

struct ObservablesPlaceHolder {
};

struct TwoBodyStruct {
    // TODO: why is kanamore U,J,Uprime not allowed for Ylm?
    float F0;
    float F2;
    float F4;
    float F6;
    string approximation = "none";
    string parametrisation = "slater-condon";
};

struct HlocStruct {
    ComplexSquareMatrix one_body;
    TwoBodyStruct two_body;
};


struct HybridisationStruct {
    int dim;
    string functions = "Hyb.json";
    matrix<int> matrix{dim,dim};
};

struct WormStruct {
    string basis = "matsubara"; // basis in which to measure (only matsubara for susceptibilities atm)
    string meas; //use improved estimators/not (["imprsum"/""]) (improved estimators not implemented for susceptibilities)
    bool diagonal = false;
};

struct OneTimeWormStruct : public WormStruct  {

    int cutoff = 50;  // number of frequencies to measure

};

struct MultTimeWormStruct : public WormStruct  {

    int fermion_cutoff = 50;  // number of frequencies to measure
    int boson_cutoff = 10;  // number of frequencies to measure

};


struct AnalyticalContinuation {
    int ntau = 400;
    int nf = 1000;
};


struct PartitionSpaceStruct  {

    float green_matsubara_cutoff = 10;  // in eV

    QuantumNumbersPlaceHolder quantum_numbers;
    ObservablesPlaceHolder observables;  // this will be a dictionary in runtime

    int susceptibility_cutoff = 10; // in eV
    int susceptibility_tail = 100;

    bool quantum_number_susceptibility = false;
    bool occupation_susceptibility = false;
    bool occupation_susceptibility_bulla = true;
    bool occupation_susceptibility_direct = false;
    bool green_bulla = true;
    bool density_matrix_precise = true;
    int sweepA = 50;
    int storeA = 100;
    int sweepB = 250;
    int storeB = 20;

    string green_basis = "matsubara";

    string probabilities = "";  // this actually turns into a list in the code (of strings)

};

struct Parameters {

    float beta;
    float mu;

    bool complex = true;
    BasisStruct basis;
    HlocStruct hloc;
    HybridisationStruct hybridisation;
    PartitionSpaceStruct partition;
    AnalyticalContinuation analytical_continuation;

    //worms
    OneTimeWormStruct green;
    OneTimeWormStruct susc_ph;
    OneTimeWormStruct susc_pp;
    MultTimeWormStruct hedin_ph;
    MultTimeWormStruct hedin_pp;
    MultTimeWormStruct vertex;

    //either steps or times should be provided, but not both
    //steps are for debugging, time for production runs
    int measurement_steps = 0; 
    int thermalisation_steps = 0; 

    int measurement_time = 20; // in minutes
    int thermalisation_time = 5; // in minutes
    int sim_per_device = 25;

    bool quad_insert=false;
    bool all_errors=false;

    string error = "parallel"; // error analysis

    int trunc_dim;
    int interaction_truncation;
};



}



#endif // CTQMC_INTERNAL_H_INCLUDED
