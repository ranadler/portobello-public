#ifndef LAPW_H_INCLUDED
#define LAPW_H_INCLUDED

#include "base_types.h"

namespace LAPW {

	struct PWOverlap {
		int num_pw;  // this is a per-k number

        matrix<complex> overlap{num_pw,num_pw};
	};



    struct Basis {
        int num_muffin_orbs; // number of muffin orbital indices
        int num_k_all;
        int num_k_irr;
        int num_k_shard; // number of kpoints in shard
        int num_si = 1; // number of spin indices, no spin index by default
        int nrel = 1;//


        // TODO: description of the plane waves
        array<PWOverlap> pw_overlap{num_k_irr};

        // description of the muffins
        vector<int> atom_number{num_muffin_orbs}; // number start at 1. Note that this contains
                                                  // non-distinct atoms in the unit cell
        vector<int> n{num_muffin_orbs};
        vector<int> l{num_muffin_orbs};
        vector<int> m{num_muffin_orbs};
        // 1 for correlated orbitals, 0 otherwise - used to construct the projector
        vector<int> correlated{num_muffin_orbs};

        // encodes the kind of the orbital:
        //  0 - LOC  (phi)
        //  1 - LAPW - phi
        //  2 - LAPW - dot(phi)
        vector<int> kind{num_muffin_orbs};
        matrix<float> mo_overlap{num_muffin_orbs,num_muffin_orbs};

        // note that we give only the in-shard weight
        //TODO: this should move to the DFT level
        vector<float> weight{num_k_shard};


        // actual value of k points
        matrix<float> KPoints{num_k_all, 3};

    };

    struct MTAtomInfo {
        int num_orbitals = 0; // = num_si*(nrel*max_l+1)
        vector<float> l{num_orbitals};
        vector<float> energy{num_orbitals}; // in eV, relative to the fermi energy
        vector<float> core_overlap{num_orbitals};
    };

    struct BasisMTInfo {
        int num_unique_atoms;

        array<MTAtomInfo> atoms{num_unique_atoms};
    };

    // This represents a general expansion in MT basis, of a set
    // which has (potentially) two indices - bounded by num_expanded and num_k
    // (a product basis).
    // => num_k should be 1 if the what is expanded is orbitals
    //    num_k should be the number of k-points for expansion in Bloch basis
    struct Expansion {
        bool is_sharded = false;
        int shard_offset = 0;
        int num_k;

        int num_expanded;
        int num_si = 1; // number of spin indices, no spin index by default

        int max_num_pw;      // number of plane waves indices
        int num_muffin_orbs; // number of muffin orbital indices

        //array4<complex> A{max_num_pw, num_expanded, num_k, num_si};
        array4<complex> Z{num_muffin_orbs, num_expanded, num_k, num_si};
    };

    // k-points path for plotting (input to get its band structure)
    struct KPath {
        int num_k;

        matrix<float> k{3, num_k};
    };

    // num_expanded is the number of bands (num_bands)
    struct BandStructure : Expansion {

        matrix<int> num_bands{num_k, num_si};

        // band energies - for each k num_bands[k] is the number
        // of energies in the array, and they are sorted in increasing order
        cube<float> energy{num_expanded, num_k, num_si};

        // submatrix of the occupation matrix in the bands
        array4<complex> Occ{num_expanded, num_expanded, num_k, num_si};

        //array4<float> Vxc{num_expanded, num_expanded, num_k, num_si};
        //array4<float> Vh{num_expanded, num_expanded, num_k, num_si};

        float chemical_potential;
    };

    struct EquivalentAtomSymmetries {
        int num_atoms;
        vector<int> g{num_atoms};
    };

}


#endif // LAPW_H_INCLUDED
