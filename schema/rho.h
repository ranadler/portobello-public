#ifndef RHO_H_INCLUDED
#define RHO_H_INCLUDED

// representation of the density operator in a band-window (usually corresponds to an energy window).
//

namespace Rho {

struct RhoInfo {
    string location;
};



struct RhoInBands {

    int min_band;  // starts with 1
    int max_band;
    int num_k;  // number of kpoints in the shard
    int num_si;

    //
    array4<complex> rho{extent(min_band,max_band),
                      extent(min_band,max_band),
					  num_k,
					  num_si};


    // these are in Ry
    cube<complex> Hqp{extent(min_band,max_band),
					  num_k,
					  num_si};


    //quasiparticle weights
    cube<complex> Z{extent(min_band,max_band),
					  num_k,
					  num_si};
};

}

#endif // RHO_H_INCLUDED
