#ifndef SUPER_H_INCLUDED
#define SUPER_H_INCLUDED

#include "base_types.h"

namespace MI {

    // used to control / automate higher level computations, such as
    // multi-impurity calculations and (not yet implemented) cluster calculations

    struct ImpurityProblem {
        string file_name;
        string description;
    };

    struct MultiImpuritySupervisor {
        int num_impurities;
        bool ready; // initially we don't know which method is in kind - it is empty
        // we instantiate the class dynamically with the given class and module names
        string solver_class; 
        string solver_module;
        array<ImpurityProblem> impurity_problem{num_impurities};
    };


};


#endif // SUPER_H_INCLUDED
