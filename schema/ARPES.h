#ifndef ARPES_H_INCLUDED
#define ARPES_H_INCLUDED

namespace ARPES{


struct label{
    string text;
};

//[H]igh [S]ymmetry [K] [P]oints or [P]ath
struct HSKP{ 

// if path, we represent every point on the path
// if plane, we represent only the hskp at the center of the plane
    int npoints; 
    array<label> labels{npoints};

};

struct ARPES{

    string title;    

    int num_equivalents; //number of equivalent + anti-equivalent impurities
    int num_directions; //3 for a vector observable: x,y,z in S_direction | 1 for a scalar observable (rep, LS)
    int num_values; // 0.5, -0.5 in Sz
    int nx; //number of k points in path | number of k_x in plane
    int ny; //number of frequency points | number of k_y in plane

    array5<float>  Z{num_equivalents, num_directions, num_values, nx, ny}; //ARPES data
    vector<float>  values{num_values};

    HSKP hskp;

};

struct PathARPES : ARPES{

    bool is_plane = false;
    vector<float>  omegas{ny};
    string xlabel = "k";
    string ylabel = "\\omega";

};

struct PlaneARPES : ARPES{

    bool is_plane = true;
    float width;
    string xlabel;
    string ylabel;

};

}

#endif // ARPES_H_INCLUDED
