#ifndef EDX_H_INCLUDED
#define EDX_H_INCLUDED

namespace edx {

struct Tensor2 {
    int len;
    vector<int> i1{len};
    vector<int> i2{len};
    vector<complex> value{len};
};

struct Tensor4 {
    int len;
    vector<int> i1{len};
    vector<int> i2{len};
    vector<int> i3{len};
    vector<int> i4{len};
    vector<complex> value{len};
};


// a compressed sparse row (CSR) matrix
// see https://en.wikipedia.org/wiki/Sparse_matrix
struct T_csr {
    // number of nonzero matrix elements
    int nnz;
    // number of rows of the matrix
    int m;
    // the shift of rows
    int row_shift;
    // the shift of columns
    int col_shift;
    // the row index
    vector<int> iaa{m+1};
    // the column index
    vector<int> jaa{nnz};
    // the matrix elements
    vector<complex> aa{nnz};

};

// the main data structure - this contains all blocks of the matrix
// (this matrix is only 1 row-like block in the bigger matrix maintained
//  by all processors)
struct CSRBlocks {
    int nblock;
    array<T_csr> ham_csr{nblock};
};

struct Setup {
    // this from config
    int ed_solver = 1; // 1- lanczos, 2 - arpack

    int num_val_orbs = 2; // for Gutz this is 2* the full dimension of the shell: 2*(2*(2l+1))
    int maxiter = 500;
    float eigval_tol = 1.0e-8;

    int nvector; // number of eigenvectors to obtain,  <= num_fock
    int neval; // number of eigenvalues to obtain,     <= num_fock


    int num_fock;  // ndim_i, for Gutz this is the number of orbitals with half-occupancy

    bool debug_print = false;

    vector<int> fock{num_fock};

    // the full coulomb tensor on the orbitals (2 body)
    Tensor4 coulomb;

    // the representation of the hopping terms (this decides which terms are non-zero)
    Tensor2 hopping_rep;

    int ncv; // for arpack

    // The prefix for V0 (sparse coulom matrix) files
    // should possibly reflect U,J, the accuracy threshold,
    // and the number of processors (so that it is valid if params are changed)
    string prefix = "V0-";

};

struct Input {

    // The actual hopping matrix:
    // - if incremental, it should be the current hopping matrix, minus the previous one
    // - if not, it is just the current hopping matrix
    Tensor2 hopping;
};

struct Output {
    bool is_valid;

    int num_val_orbs;
    int nvector;
    int neval;
    int num_fock;
    int num_orbital_fock_states;

    cube<complex> denmat{num_val_orbs,num_val_orbs, nvector};  // should we trace this in F?
    vector<complex> E{neval};
    matrix<complex> eigvecs{num_fock, nvector};
    vector<float> histogram{num_orbital_fock_states};

};

struct EDSolverState {
    int num_x;
    vector<float> x{num_x};
};


}

#endif // EDX_H_INCLUDED
