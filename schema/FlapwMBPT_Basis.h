#ifndef FLAPW_BASIS_H_INCLUDED
#define FLAPW_BASIS_H_INCLUDED

namespace FlapwMBPT_basis {

struct MuffinTin {

    int num_muffin_orbs;
    int num_radial_functions;
    int nrad;

    vector<int> lapw2self{num_muffin_orbs}; //maps lapw index to a radial function in this tin
    matrix<float> radial_functions{num_radial_functions,nrad};
    matrix<float> radial_functions2{num_radial_functions,nrad};
    vector<float> radial_mesh{nrad};
    vector<float> dr_mesh{nrad};

};

struct MuffinTinBasis{

    int nrel;
    int nsort;
    int natom;
    int nops;

    matrix<int> atom_to_equiv_under_op{natom,nops}; 
    array<MuffinTin> tins{nsort};

    void GetMtBasis()
        __attribute__((section("FORTRAN,flapw_basis")));

};

struct InterstitialBasis {

    int max_num_pw;
    int max_g_basis;
    int nrel;
    int num_expanded;
    int num_k_all;
    int num_k;
    int num_si;

    
    matrix<float> reciprical_lattice_vectors{3,max_g_basis};
    matrix<int> vector_index{max_num_pw, num_k_all}; // indices from 1
    vector<int> num_vectors_at_k{num_k}; // indices from 1

    array4<complex> A{max_num_pw*nrel, num_expanded, num_k, num_si};

    void GetInterstitialBasis(int min_band, int max_band)
        __attribute__((section("FORTRAN,flapw_basis")));

};

}

#endif //FLAPW_BASIS_H_INCLUDED
