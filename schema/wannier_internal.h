#ifndef WANNIER_INTERNAL_H_INCLUDED
#define WANNIER_INTERNAL_H_INCLUDED

#include "base_types.h"
#include "LAPW.h"

namespace wannier_internal {


    struct Wannier90Bounds {
        int min_band;
        int max_band;
        int num_bands;
        int num_wann;
        int num_k_all; // vs num_k_irr, this equals nqdiv in Kutepov terms
    };

    struct Wannier90Overlap : Wannier90Bounds {
        int nntot;

        array4<complex> M{num_bands, num_bands, nntot, num_k_all};
    };

    struct Wannier90SymAdapted : Wannier90Bounds {
        int num_k_irr; // number of k points in the Irreducible BZ
        int num_ops;   // number of operations in the space group
        array4<complex> KSRep{num_bands, num_bands, num_ops, num_k_irr};
        array4<complex> OrbitalRep{num_wann, num_wann, num_ops, num_k_irr};
    };

    struct Wannier90Workspace : Wannier90Bounds {
        int num_nnmax = 12;

        base_types::Window dis_window;

        matrix<float>  k_lat{3, num_k_all};

        matrix<int> nnlist{num_k_all,num_nnmax};
        cube<int> nncell{3,num_k_all,num_nnmax};

        matrix<float>   eigenvalues{num_bands,num_k_all};

        Wannier90Overlap overlap;
        //cube<complex>   A{num_bands, num_wann, num_k_all};

        cube<complex>   U{num_wann, num_wann, num_k_all};
        cube<complex>   U_opt{num_bands, num_wann, num_k_all};

        cube<complex>   M0{num_bands,num_bands,num_k_all};  // for debug

        //Wannier90SymAdapted sym_input;

        //LAPW::BandStructure expn;  // for all k points in BZ
    };

}

#endif // WANNIER_INTERNAL_H_INCLUDED
