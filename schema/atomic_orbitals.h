#ifndef ATOMIC_ORBITALS_H_INCLUDED
#define ATOMIC_ORBITALS_H_INCLUDED

#include "LAPW.h"
#include "base_types.h"

namespace atomic_orbitals {

struct OrbitalLabels {
    string short_label;
    string latex_label; // $ signs stripped
};

struct AtomicShell {
	int atom_number;
	int l;
	int lapw_kind;
	int num_equivalents;
	int num_anti_equivalents; // these follow the equivalents in the end
	int cluster_size=1;
	bool is_correlated;
	int nrel = 1;  // is either 1 or 2 for fully-relativistic computation
	int dim; // (2l+1) * nrel

	// start and end indices in the V matrix if this is part of a bigger SelectedOrbitals
	int start_index = 0;
	int end_index = 0;

	matrix<int>      raw_rep{dim,dim}; // the representation determined by the symmetry analysis
	matrix<int>      rep{dim,dim};     // the approximate rep. used in the subspace
	matrix<complex>  basis{dim,dim}; // the basis change, relative to to the native basis:
                // Columns express the new basis in terms of the "native basis"
                // the "native" basis is defined as RSH in non-relativistic DFT, and J-basis in relativistic DFT
                // This matrix is usually I (it is always in the factors)
    vector<OrbitalLabels> labels{dim};

	// TODO: fix the bug in the generator so that we could write this expression more simply
	vector<int> lapw_indices{dim*num_equivalents+dim*num_anti_equivalents};
	vector<int> det_signs{num_equivalents+num_anti_equivalents};

	// the isometries that describe the equivalence / anti-equivalence relationships between the
	// 1st atom and the equivalent/anti-equivalent atoms
	cube<float> rotations{num_equivalents+num_anti_equivalents,dim,dim};
	matrix<float> translations{3,num_equivalents+num_anti_equivalents};
};


struct SelectedOrbitals {
	int num_muffin_orbs;
	int num_shells;
	int total_dim;    // total dimension of the image of the projector
	string subspace_expr; // the subspace definition, see option "--projector-subspace" in the code
	float exclusion_margin = 2.0; // 2eV - should we set it to 0?
	
	matrix<complex> V{num_muffin_orbs, total_dim}; // basis change matrix between the MBPT-LAPW raw orbitals and our correlated basis
	vector<AtomicShell> shells{num_shells};
};


struct OrbitalsBandProjector {
    int num_k_all;  // number of ALL KPOINTS in BZ = nqdiv in Kutepov's code.
                    // note that the irreducible number is nqpnt
    int num_orbs;

    // these 2 band numbers start at 1 (**** beware python ***)
    int min_band;  // maximum band number - excluded below
    int max_band;  // minimum band number - excluded beyond


    int num_si = 1; // number of spin indices

    // a general projector representation
    array4<complex>   P{max_band-min_band+1, num_orbs, num_k_all, num_si};

};


struct OrbitalMTProjector : LAPW::Expansion {

};

}


#endif // ATOMIC_ORBITALS_H_INCLUDED
