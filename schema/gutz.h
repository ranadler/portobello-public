#ifndef GUTZ_H_INCLUDED
#define GUTZ_H_INCLUDED

#include "PEScf.h"

namespace gutz {

    struct GState : public PEScf::SolverState {
    	int num_k_all;
    	//int num_bands;
		cube<complex> Hloc{dim, dim, num_si};

    	cube<complex> R{dim, dim, num_si};
    	cube<complex> Lambda{dim, dim, num_si};
    	cube<complex> Z{dim, dim, num_si};

    	int success;

    	float egutz = 0.0;
    };

    struct GProblem : public PEScf::SolverProblem {
		int num_k;
		int num_k_irr;
		int num_bands;
		int norb2; // the spinfulcorrelated subspace size

		string gutz_name; // name of the gutzwiller file associated with this impurity

		// is in projection caluclations in the algorithm
		vector<float> weight{num_k};

		// representation of Hk in a rotated basis, where the correlated subspaces
		// (with equivalence) appear in the bottom right of the matrix - which is how
		// Gutzwiller formulations usually like it.
		array4<complex> Hk{num_k, num_bands, num_bands, num_si};

		// same as Hk, but in the band basis - stored here instead of storing all
		// the unitary transformations per k.
		array4<complex> HkBands{num_k, num_bands, num_bands, num_si};

		array4<complex> UTensor{norb2,norb2,norb2,norb2};
    };

    struct GSolution : public PEScf::SolverSolution {
        int dim;
        int num_si;

    	cube<complex> R{dim, dim, num_si};
    	cube<complex> Lambda{dim, dim, num_si};
    	cube<complex> Z{dim, dim, num_si};
    	cube<complex> RhoImp{dim, dim, num_si};

		float energyImp;

    	int success;
    };
};
#endif
