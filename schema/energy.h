#ifndef ENERGY_H_INCLUDED
#define ENERGY_H_INCLUDED

namespace energy {

struct FreeEnergy {
    int min_band; // from 1
    int max_band; // from 1
    int num_k_irr;
    int num_si;
    int num_reps = 1;

    int num_atoms = 1; // num of atoms in the cell

    cube<float> Hqp{extent(min_band,max_band), num_k_irr, num_si};

    // for each band, its overlap squared with the rep number (numbered from 0)
    matrix<float> weight{num_reps, num_k_irr};

    float corr_energy_term = 0.0;

    float Fx = 0.0;
    float Fc = 0.0;
    float z_vnucl = 0.0;
    float ro_vh_new = 0.0;
    float exch_dft = 0.0;
    float e_coul = 0.0;
    float tr_vxc_valence = 0.0;
    float tr_vh_valence = 0.0;  // actually trace (rho*V_C)
    float total_energy_core = 0.0;
    float e_h_core = 0.0;
    float e_xc_core = 0.0;
    float core_energy = 0.0;
    float muN = 0.0; //mu*Nvalance
    float correlated_term_ry = 0.0;

    // more for testing
    float trLogHloc = 0.0;
    float trLogHlocCorr = 0.0;
    float muRy; // in Ry
    float N; // Number of valence electrons
    float Nqp; //Gqp occupancy



    float result = 0.0;

    void CalculateFreeEnergyDFT()
        __attribute__((section("FORTRAN,flapw_driver")));
    void CalculateFreeEnergyGutz()
        __attribute__((section("FORTRAN,flapw_driver")));
    void CalculateFreeEnergyDMFT()
        __attribute__((section("FORTRAN,flapw_driver")));

};

};

#endif // ENERGY_H
