#ifndef QPGW_ADAPTER_H_INCLUDED
#define QPGW_ADAPTER_H_INCLUDED


#include "symmetry.h"
#include "LAPW.h"
#include "FlapwMBPT_Basis.h"


namespace FlapwMBPT_interface {


struct CrystalStructure {
    int num_atoms;
    int num_distinct;

    // in cartesian coords, no lattice parameter, in Bohr Radius units.
    vector<float> a{3};
    vector<float> b{3};
    vector<float> c{3};

    matrix<float> xyz{3,num_atoms};
    vector<float> Z{num_distinct};


    vector<int> distinct_number{num_atoms};  // numbers start with 1

    vector<int> max_l_per_distinct{num_distinct};
    int max_l;  // maximum l over all valence, semi-velance atoms

};


struct Convergence {
    int num_valence_electrons = 0;
    float sum_valence_electrons;
    float charge_convergence;
    float mag_convergence;
};


struct DFT {


    int num_k_all; // nqdiv

    int num_k_irr; // npnt

    int num_si = 1;  // number of spin indices used (either 1 or 2) = nspin
    int nrel = 1; // 1 - if non-relativistic, 2 - if relativistic dft

    symmetry::SpaceGroup sg;

    CrystalStructure st;

    bool is_sharded = false;
    int shard_offset = 0;
    int num_k;  // number of points in shard

    // the irreducible k for given k
    vector<int> k_irr{num_k_all}; // numbered like fortran, from 1
    // the number of symmetry operation in self.sg which rotates k_irr to this k
    vector<int> ig{num_k_all};  //numbered like fortran, from 1

    int num_bands;  // maximum number of bands
    int num_valence_electrons;


    void AdjustLAPWBasis() __attribute__((section("FORTRAN,DFTPlusSigma")));

    // if k>=1 then return the kth kpoint band structure
    void DFTPlusSigmaOneIteration(const string& sigma_location, int checkpoint,
                                  int search_mu, int update_lapw,
								  float admix,
                                  int save_potentials,
                                  const string& mu_search_variant) __attribute__((section("FORTRAN,DFTPlusSigma")));

    void ConnectFlapwMBPT(const string& inpfile, int num_workers, const string& opts) __attribute__((section("FORTRAN,flapw_driver")));

    void Reload()
        __attribute__((section("FORTRAN,flapw_driver")));


    const FlapwMBPT_interface::DFT& GetDFTInfo()
        __attribute__((section("FORTRAN,flapw_driver")));

    const LAPW::Basis& GetLAPWBasis()
        __attribute__((section("FORTRAN,flapw_driver")));

    const LAPW::Basis& GetBasisMTInfo()
        __attribute__((section("FORTRAN,flapw_driver")));

    // if k>=1 then return the kth kpoint band structure
    const LAPW::BandStructure& GetPartialBandStructure(int min_band, int max_band, int k)
        __attribute__((section("FORTRAN,flapw_driver")));

    // if k>=1 then return the kth kpoint band structure
    const LAPW::BandStructure& GetPartialBandStructureAndGradient(const string& gradpath, int min_band, int max_band, int k)
        __attribute__((section("FORTRAN,flapw_driver")));

    // if k>=1 then return the kth kpoint band structure
    const LAPW::BandStructure& GetKpathBands(const string& kpathpath, int min_band, int max_band)
        __attribute__((section("FORTRAN,flapw_driver")));

    const LAPW::BandStructure& GetKPathBandsAndGradient(const string& gradpath, const string& kpathpath, int min_band, int max_band)
        __attribute__((section("FORTRAN,flapw_driver")));

    const LAPW::EquivalentAtomSymmetries& GetEquivalentAtomSymmetries()
        __attribute__((section("FORTRAN,flapw_driver")));

    void CreatePlots()
        __attribute__((section("FORTRAN,flapw_driver")));

    const Convergence& GetConvergence() __attribute__((section("FORTRAN,DFTPlusSigma")));

    void Disconnect() __attribute__((section("FORTRAN,flapw_driver")));
};


}



#endif // QPGW_ADAPTER_H_INCLUDED
