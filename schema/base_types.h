#ifndef BASIC_TYPES_H_INCLUDED
#define BASIC_TYPES_H_INCLUDED

namespace base_types {

  struct EmptyObject {
  };

  struct ComputationInput {
    datetime   date_computed;
  };


  struct ComputationOutput {
    string     binary_name;
    string     binary_release;
    datetime   date_computed;
    string     researcher;
    string     description;
  };

    struct LDADescriptor : public ComputationOutput {
        string method;        // either W2K, GW-LDA, etc. it determines the meaning of the next fields
        string location;      // the location of the computation
        string seed_name;     // possibly if the method requires one
    };


    struct Window {
        float low, high;
    };
}

#endif
