#ifndef FERMI_SURFACE_H_INCLUDED
#define FERMI_SURFACE_H_INCLUDED

namespace fermi_surface {

struct LowEnergyHamiltonian {
    int min_band; // from 1
    int max_band; // from 1
    int num_k_irr;
    int num_si;

    string name;

    cube<float> eps{extent(min_band,max_band), num_k_irr, num_si};

    //fermisurfer uses this to color surface -- stores band index, lifetime, observable value
    cube<float> scalar{extent(min_band,max_band), num_k_irr, num_si}; 

    void WriteFRMSF()
        __attribute__((section("FORTRAN,fermisurfer_driver")));
};

}

#endif // FERMI_SURFACE_H_INCLUDED
