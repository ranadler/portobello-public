#ifndef MAXENT_H_INCLUDED
#define MAXENT_H_INCLUDED

#include "base_types.h"

namespace maxent {


// note these are fixed through the calculation (per orbital)
struct Input {

    // X is the variable of the signal, it is either iw_n (or rather w_n)
    // or tau between 0 and beta.
    int num_x;

    float beta;
    int nomega;

    // we also use the sign of the choice to tell if it is Fermion / Boson
    int FERMIONIC_TAU_KERNEL = 1;      // exponentially decreasing kernel
    int FERMIONIC_IOMEGA_KERNEL = 2;   // 1/(iw_n -w)
    int BOSONIC_TAU_KERNEL = 3;         // similar to fermionic tau, but for boson
    int BOSONIC_IOMEGA_KERNEL = 4;         // 2w^2/(w_n^2-w^2) (positive omega only -- enforces symmetry)
    int kernel_choice = 1;

    // this is the matrix whose inverse is ill-defined, linearly mapping between
    // the probability density (A) and the signal.
    matrix<complex> kernel{extent(-nomega, nomega), num_x};

    float normalization = 1.0;

    vector<float>   omega{extent(-nomega, nomega)};
    vector<float>   d_omega{extent(-nomega, nomega)};
    matrix<float>   dlda{2*nomega+1, 2*nomega+1}; // diag(Kernel.H * C.I * Kernel), C=covar(signal)
    vector<float>   model{extent(-nomega, nomega)};

    //for off diags (two models, one for A+, one for A-)
    vector<float>   Dp{extent(-nomega, nomega)};
    vector<float>   Dm{extent(-nomega, nomega)};

    vector<complex> signal{num_x}; // the signal that we try to reproduce as K*A
    vector<float>   invVar{num_x};
    vector<complex> X{num_x};

    void CompleteSetup()
        __attribute__((section("FORTRAN,maxent_driver")));

};

struct Workspace {

    int num_x;
    int nomega;

    float alpha = 1000.0;
    float rfac  = 1.0;

    float temp  = 10.0; // temperature

    // the (intermediary) result: the inverse of kernel() applied to the signal
    vector<float>   A{extent(-nomega, nomega)}; // current A(omega), starts as random

    //For off-diagonals, A = A+ - A-
    vector<float>   Ap{extent(-nomega, nomega)}; // current A(omega), starts as random
    vector<float>   Am{extent(-nomega, nomega)}; // current A(omega), starts as random
    vector<float>   CA{extent(-nomega, nomega)}; // current A(omega), starts as random

    // temporary vars in Maxent()
    vector<float>   dA{extent(-nomega, nomega)}; // delta A
    vector<float>   dAp{extent(-nomega, nomega)}; // delta A
    vector<float>   dAm{extent(-nomega, nomega)}; // delta A
    vector<float>   dCA{extent(-nomega, nomega)}; // delta A
    
    vector<complex> signal1{num_x};  //  A*K
    vector<complex> signal2{num_x};  // trial move sig(x)

    // calculated for the current iteration
    float entropy;
    float tr;

    void Maxent(int num_steps)
        __attribute__((section("FORTRAN,maxent_driver")));

    void MaxentOffDiag(int num_steps)
        __attribute__((section("FORTRAN,maxent_driver")));
};


}

#endif
