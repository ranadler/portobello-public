#ifndef FLAPWMBPT_H_INCLUDED
#define FLAPWMBPT_H_INCLUDED


namespace FlapwMBPT {

struct Control {
    int iter_dft = 80;
    int iter_hf = 0;
    int iter_gw = 0;
    int iter_qp = 0;

    int iter_psi = 0;
    int iter_bsp = 0;

    int restart_begin = 0;
    int restart_end = 0;

    float admix = 0.05;
    float adspin = 0.7;
    float adm_gw = 0.3;
    float acc_it_gw = 0.6;

    int iexch = 5;  /// identifies the functional 200=PBE

    float scal_spin = 1.0;
    int psi_fncl_use = 0;

    int nproc_tau = 1;
    int nproc_k = 1;
    int nproc_pbr = 1;

    int irel = 1;       /// 0 - non-rel, 1 - scalar relativistic, 2 -fully relativistic
    int irel_core = 1;
    float clight = 274.074;
    bool rel_interst = false;  /// relativistic interstitial

    float temperature = 900.0;
};

struct AtomicData {
    string txtel;
    float z;
    float z_dop = 0.0; // doping on top of z
    float magn_shift = 0.0;
    float smt;              /// radius of the muffin tin sphere
    float h = 0.0120;
    int nrad = 1200;

    int lmb;
    int lmpb;
    /// should be set to the max value in ntle before allocating
    int maxntle = 10;

    vector<int> lim_pb_mt{extent(0,lmpb)};
    vector<int> lim_pb_mt_red{extent(0,lmpb)};

    vector<int>   ntle{extent(0,lmb)};
    /// 0 -> LOC,  1 -> APW
    matrix<int>   augm{maxntle,  extent(0,lmb)};
    /// occupation
    matrix<float> atoc{maxntle, extent(0,lmb)};
    /// n.8
    matrix<float> ptnl{maxntle,  extent(0,lmb)};
    /// 0 -> not correlated, 1-> correlated
    matrix<int>   correlated{maxntle,  extent(0,lmb)};
    /// how to solve the radial equations
    matrix<float> idmd{maxntle,  extent(0,lmb)};


};

struct Structure {
    float par = 1.0;  // lattice constant
    float b_a = 1.0;
    float c_a = 1.0;

    int nsort;
    int natom;

    int istruc = -1;  // defines the k points for DOS path, should be set to > 0

    // in cartesian coords, no lattice parameter, in Bohr Radius units.
    vector<float> a{3};
    vector<float> b{3};
    vector<float> c{3};

    matrix<float> tau{3,natom};
    vector<int> isA{natom};

    array<AtomicData> ad{nsort};
};


struct Generator {
    int sign = 1;
    string kind = "R"; // otherwise "M", "I","E"
    // is either the axis of rotation or the normal to reflaction ("M")
    vector<float> v{3};
    int n; // degree of rotation

    // translation attached to the generator
    vector<float> t{3};
    
};

struct SpacegroupGens {

    int num_generators;
    // all cartesian in Angst.
    vector<Generator>  gens{num_generators};
    cube<float>  rots{3, 3 ,num_generators};

};

struct KGrid {
    vector<int> ndiv{3};  // number of kpoints in full BZ
    vector<int> ndiv_c{3};

    bool metal = true;
    int n_k_div = 9;

    string k_line = "010";  // 3 characters - direction in k space - for output
};

struct RealSpaceMeshes {
    vector<int> nrdiv{3};  /// Real mesh for integration in real unit cell (?)
                           /// => also mesh for PW Basis (reciprocally)
    					   /// see cut_gw_ratio.

    vector<int> mdiv{3};   /// mesh for the density (?) should be higher
    vector<int> nrdiv_red{3};  /// ?
};


struct Basis {
    float cut_lapw_ratio=0.610;
    float cut_pb_ratio=0.980;
    float eps_pb=1.0e-03;
};

struct Zones {
    int nbndf = 0;

    // band selection of low energy bands for vertex corrections
    vector<int> nbndf_bnd{2}; // first=0 means around Ef, otherwise it is a band index to start with this many bands
};

struct BandPlot {
    int n_k_div = 10;
};

struct DensityOfStates {
    float emindos = -2.0;
    float emaxdos = 2.0;
    int ndos = 600;
    float  e_small = 0.005;
};

struct MultiSCF {
    float v_v0 = 1.0; // relative volume V/V0

};

struct Magnetism {
    float b_extval = 0.0;  // [Ry/Mu_B]
    int iter_h_ext = 0;  // iteration numbers string, 5 digits
    vector<float> b_ext{3};
};

struct Coulomb {
    float eps_coul = 0.0001;
};

struct TauMesh {
    int n_tau = 46;  // number of points - it is an exponential mesh
    float exp_tau_gw = 4.0; // exponent
};

struct OmegaMesh {
    int n_omega_exa = 10;
    int n_omega_asy = 36;
    float omega_max = 50.0; // ! Ry
};

struct NuMesh {
    int n_nu_exa = 10;
    int n_nu_geom = 30;
    int n_nu_asy = 6;
    float nu_geom = 100.0;
    float nu_max = 400.0;
};

struct AFMSymmetry {
    float magmom;
    int site;
    int opposite_site;
    matrix<float> op{4,4}; // maps opposite_site to to this site
};


// this is stored separately, but under the "rhobusta" field
struct RhobustaExtra {
    // magnetic calculation parameters
    bool has_magnetic_field = false;
    bool is_corr_spin_polarized = false;
    bool is_dft_spin_polarized = false;

    // if true, the off diagonal elements in the local matrices are not discarded
    bool local_off_diagonals = false;

    // if true, the diagonal of the spinless calculation is used for spinful calculation.
    bool simplify_diagonal = false;

};

// this is stored separately, but under the "AFM" field
struct AFMExtra {

    int num_afm = 0;

    // all AFM symmetries (with negative moment), the positive ones are just the inverse
    vector<AFMSymmetry> afm_symm{num_afm};

};

struct Input {
    string text;
    string allfile;
    Control ctrl;
    Structure strct;
    SpacegroupGens sgg;
    RealSpaceMeshes rsm;
    Basis bas;
    Zones zns;
    BandPlot bp;
    DensityOfStates dos;
    KGrid kg;
    MultiSCF mscf;
    Magnetism mag;
    Coulomb coul;

    TauMesh tmesh;
    OmegaMesh omesh;
    NuMesh nmesh;
};

struct ExpansionSizes {

    int npnt, nbndf, nspin, nbasmpw, nfun;
};

struct Sizes : ExpansionSizes {

    int ncormax, nspin_0, nsort, maxmtcor;


    int maxel, natom;


    int maxwf, nlmb, maxntle, maxnrad;


    bool has_gf0 = false;
};

struct ElDensity {

    int maxmt;
    int nspin_0 = 1;
    int maxmtb = 1;
    int nplwro = 1;
    // for ro core
    int nrmax = 0;
    int nsort = 0;

    vector<float> ro{maxmt};
    matrix<complex> rointr{nplwro,nspin_0};


    // only magn ==2
    vector<float> spmt{maxmtb};
    matrix<complex> spintr{3,nplwro};
};

struct ElDensityWithCore  {
    // for ro core
    int nrmax = 0;
    int nsort = 0;
    int nspin_0 = 1;

    cube<complex> ro_core{extent(0,nrmax),nsort,nspin_0};

};


struct Image : public Sizes {

    float chem_pot;
    //string ubi;


    cube<float> e_core{ncormax,nspin_0,nsort};
    vector<float> pcor{maxmtcor};
    vector<float> qcor{maxmtcor};


    matrix<int> n_bnd{npnt, nspin_0};
    cube<float> e_bnd{nbndf,npnt,nspin_0};

    array4<complex> g_loc_0{maxel,maxel,natom,nspin_0};


    matrix<float> gfun{maxwf,nspin};
    array5<float> p_f{extent(0,maxnrad),maxntle,nlmb,nsort,nspin};
    array5<float> pd_f{extent(0,maxnrad),maxntle,nlmb,nsort,nspin};
    array5<float> pd2_f{extent(0,maxnrad),maxntle,nlmb,nsort,nspin};
    array5<float> q_f{extent(0,maxnrad),maxntle,nlmb,nsort,nspin};
    array5<float> qd_f{extent(0,maxnrad),maxntle,nlmb,nsort,nspin};
    array5<float> qd2_f{extent(0,maxnrad),maxntle,nlmb,nsort,nspin};

    matrix<float> gfund{maxwf,nspin};
    array4<float> eny{maxntle,nlmb,nsort,nspin_0};

    array7<float> ffsmt{2,2,maxntle,maxntle,nlmb,nsort,nspin};

    // density
    ElDensity rho;
};

struct FullEVB : public ExpansionSizes {
    array4<complex> evb{nbasmpw,nbndf,npnt,nspin};
};

struct FullZB : public ExpansionSizes {
    array4<complex> zb{nfun,nbndf,npnt,nspin};
};

struct FullGF0 : public ExpansionSizes {
    array4<complex> gf0{nbndf,nbndf,npnt,nspin};
};



};


#endif // FLAPWMBPT_H_INCLUDED
