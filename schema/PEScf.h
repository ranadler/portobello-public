#ifndef PESCF_H_INCLUDED
#define PESCF_H_INCLUDED

#include "self_energies.h"
#include "atomic_orbitals.h"
#include "base_types.h"
#include "FlapwMBPT_interface.h"

// data for Project-Embed SCF
namespace PEScf {

    struct Histogram {
        int num_configs; // typically 2^10, 2^14
        vector<float> p{num_configs};  // the probability
        vector<int> N{num_configs};
        vector<float> energy{num_configs};
        vector<float> Jz{num_configs};
    };


    struct SolverState {
    	int super_iter = 1;
    	int sub_iter = 0;

    	// number of equivalent atoms to this one in the unit cell
    	int num_equivalents;

    	// number of anti-equivalent atoms (AFM) atoms
    	// these are atoms that would be equivalent, if not for their spin being opposite to the representative
    	// atom of this class
        int num_anti_equivalents = 0;

        int dim;
        int num_si;
        int nrel = 1; // 1 or 2

        string double_counting_method;
        float nominal_dc;
        cube<complex> DC{dim,dim, num_si};
        float initial_spin_polarization;

    	string state = "start";

        float mu;  // from DFT or DFT+, this is used just for tracking the value

    	// the self-energy is saved per iteration in a sub-group

    	float U;
    	float J;
    	float beta;

    	float Nimp; // current N for Double Counting - as reported by the solver
    	float Nlatt;  // the lattice N
        bool ising = false; // truncate interaction to ising terms or not
        bool kanamori = false; // for models (instead of slater-condon)

        matrix<int> rep{dim, dim};  // like AnnotatedRepresentation.H
        int num_unique_orbs;


    	// energy window within which to confine the projector
    	// from bands to the orbitals (necessary for re-computation
    	// of the projector in band basis) - it can be made very large,
    	// but it's not useful to include the core states.
    	base_types::Window proj_energy_window;

        // more specificallly these are cached values of the min and max bands,
        // used within one cycle of dft+
        int window_min_band = -1;
        int window_max_band = -1;

        float energyImp;
        float NN; /// number susceptibility

    	cube<complex> RhoImp{dim, dim, num_si};

        // the energy of the bands inside this window
        // to get total energy: total_energy += -energyBands + energyImp
        float energyBands;

        // these are from the impurity solver, but stored only for the last calculation
        // because they are expensive

        Histogram hist;

        FlapwMBPT_interface::Convergence cvg;


    	float quality=0.0; // set to the relative error from the last measurement in DMFT
        
        // specification string for the occupation, for example 1-10 means from 1 to 10
        base_types::Window NRange; 
    };

    struct SolverProblem {

        string self_path;  // path to the hdf5 object

        bool isReal = false;

        float beta;

        float mu;  // always zero in our use

        float U;
        float J;
        float Uprime;


        float F0;
        float F2;
        float F4;

        int dim;
        int num_si;
        int nrel;

        cube<complex> E{dim, dim, num_si};

        atomic_orbitals::AtomicShell subshell;

        // identify the correlated orbitals basis
        int correlated_orbitals_id;

    };

    struct SolverSolution {

        float E;
        float N;
        float NN; /// number susceptibility


        Histogram hist;
    	float quality=0.0; // set to the relative error from the last measurement in DMFT
    };

};



#endif // PESCF_H_INCLUDED
