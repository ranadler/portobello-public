#ifndef TRANSPORT_H_INCLUDED
#define TRANSPORT_H_INCLUDED

#include "base_types.h"

// Note that energies are in Ry
namespace TransportOptics {

    struct Velocities {
        int num_expanded;
        int num_si;
        int num_k;
        array5<complex> data{num_k, num_si, num_expanded, num_expanded, 3}; 
    };

    struct Input {
        float Temperature = -1.0; //[Ry]

        int num_omega = 1; //number of \Omega points to output. If 0, nothing will be done! 1 - for transport (zero point), higher for more optics points.
        // This is an input given directly to the python wrapper.

        bool symmetrize_bz = true; //symmetrize

        float gamm = 0.0005;  // [Ry] Gamma used for non-correlated orbitals
        float gammc = 0.0005; // [Ry] Gamma used for correlated orbitals


        float delta = 0.001; // [Ry] spacing in [Ry] of mesh for integraion


        float omega_max = 0.15; // maximum omega in [eV] for integration mesh
        // note that this determines the number of points in the mesh, as omega_max/delta
        // also it needs to be calculated well, so that it includes a band of 4KT energy around the maximal point
        // that is requested ((num_omega - 1) * delta) (we take it as num_omega*delta)
        // Note: his is all prepared by the python wrapper

        // set by transport.py: max_omega_mesh = omega_mag / delta
        int max_omega_mesh = 0;

        // set by transport.py: 4KT / delta
        int active_omega_points = -1;


        int min_band = -1;
        int max_band = -1;

        string momentum_kind = "gradient"; // other options: "k", "const"

        bool test_velocity = false;  //if true, test velocity matrices to be hermitian

        int num_dirs = 3;

        // pairs of directions for calculation
        cube<float>  dirs{3,3,num_dirs};

        void ComputeFromPortobello()
           __attribute__((section("FORTRAN,PortobelloProvider")));

    };

    struct GreensFunction {
        int max_omega;
        int num_basis;
        int num_correlated;
        vector<float>  w{extent(-max_omega,max_omega)};
        vector<float>  a{extent(-max_omega,max_omega)};
        vector<float>  b{extent(-max_omega,max_omega)};

        // this one allocated for the maximum number of bands
        matrix<complex>  HPlus{num_basis, extent(-max_omega,max_omega)};

        // the unitary transforms
        cube<complex>  L{num_correlated, num_correlated, extent(-max_omega,max_omega)};
        cube<complex>  R{num_correlated, num_correlated, extent(-max_omega,max_omega)};

    };


    struct WorkArea {
        int num_dirs = 3;
        int maxpower = 2; // do not lower if transport is to be calculated
        int num_omega;

        // the units here are powers of 1/(ohm*cm)
        cube<float> total{num_dirs, extent(0,maxpower), extent(0,num_omega)};
        float total_weight;
    };

    struct IntegrandForDebug {
        int max_omega;
        vector<float> omega{extent(-max_omega,max_omega)};
        vector<complex> G{extent(-max_omega,max_omega)};
    };

    struct EmbeddedSelfEnergyOnRealOmega {

        int min_band;  // starts with 1
        int max_band;
        int shard_start;
        int shard_end;
        int num_si;
        int max_omega;

        vector<float> omegaRy{extent(-max_omega,max_omega)};

        // this is here for verification, since all the k's for all stars are concatenated here with no separator
        vector<int> actual_k{extent(shard_start, shard_end)};

        // M values are in Rydberg
        // Note that this matrix is only on the IRR BZ
        array5<complex> M{max_band-min_band+1,
                          max_band-min_band+1,
                          extent(-max_omega,max_omega),
                          extent(shard_start, shard_end),  // these are offseted like fortran by 1
                          num_si};

    };


    struct Output : public Input {
        int maxpower = 2;

        // omega_points changes from the input (if determined automatically).
        // Here it is always set.
        vector<float> omega{extent(0,num_omega)}; // in eV
        cube<float> A{extent(0,num_omega), num_dirs, extent(0,maxpower)}; // mixed units: 1/(Ohm*cm) DC conductivity * Ry^n, n=0,1,2

    };
}




#endif // TRANSPORT_H_INCLUDED
