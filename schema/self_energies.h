#ifndef SELF_ENERGIES_H_INCLUDED
#define SELF_ENERGIES_H_INCLUDED

namespace self_energies {

struct LocalOmegaFunction {

    bool isReal = false;

    int num_omega;
    int num_orbs;
    int num_si;

    vector<float> omega{num_omega};

    array4<complex> M{num_omega, num_orbs, num_orbs, num_si};  // M for "matrix"
    array4<float> moments{3, num_orbs, num_orbs, num_si}; // up to 3 moments, only 2 used for sigma
};

struct OmegaMesh {

    int n_omega;

    vector<float> omega{extent(0,n_omega)};
    vector<int> index{extent(0,n_omega)};

    // if k>=1 then return the kth kpoint band structure
    const OmegaMesh& GetOmegaMesh()
        __attribute__((section("FORTRAN,DFTPlusSigma")));

};


struct LocalOmegaFunctionInfo {
    string location;
};


struct OmegaFunctionInBands : OmegaMesh {

    bool isReal = false;

    int min_band;  // starts with 1
    int max_band;
    int num_k_irr;  // number of kpoints in irreducible zone
    int num_si;

    // M values are in Rydberg
    // Note that this matrix is only on the IRR BZ
    array5<complex> M{max_band-min_band+1,
                      max_band-min_band+1,
                      extent(0,n_omega),
					  num_k_irr,
    				  num_si};

     array4<complex> Minf{max_band-min_band+1,
                        max_band-min_band+1,
				  	    num_k_irr,
    				    num_si};
};
}


#endif // SELF_ENERGIES_H_INCLUDED
