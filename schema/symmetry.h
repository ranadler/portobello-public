#ifndef SYMMETRY_H_INCLUDED
#define SYMMETRY_H_INCLUDED

#include "base_types.h"


namespace symmetry {


struct Bootstrap {
    int num_ops;
    int max_l;

    int maxIndexDl;
    int maxIndexDj;

    matrix<float> Dl{maxIndexDl,num_ops};    // input
    matrix<complex> Dj{maxIndexDj,num_ops};  // output

    cube<float> rots{3,3,num_ops};
    matrix<float> shifts{3,num_ops};

    matrix<float> recip{3,3};  // reciprocal vectors (to multiply the shifts)
};

// general linear group
struct GL3Group {
    int num_ops;

    // all cartesian in Angst.
    cube<float>   rots{3,3,num_ops};

    // these multiply a (-l,-l+1 .. l) vector of spherical harmonics
    // coefficients to get a rotated one
    cube<float> WigD0{1,1,num_ops};  // l=0
    cube<float> WigD1{3,3,num_ops};  // l=1
    cube<float> WigD2{5,5,num_ops};  // l=2
    cube<float> WigD3{7,7,num_ops};  // l=3

    bool hasWigJ = false;

    cube<complex> WigD0J{2,2,num_ops};  // L=0 J=0.5
    cube<complex> WigD1J{6,6,num_ops};  // L=1 J=0.5, J=1.5
    cube<complex> WigD2J{10,10,num_ops};  // L=2 J=1.5 J=2.5
    cube<complex> WigD3J{14,14,num_ops}; // L=3 J=2.5  J=3.5

    const GL3Group& GetWignerMatrices()
          __attribute__((section("FORTRAN,flapw_driver")));
};


struct SpaceGroup : public GL3Group {
    matrix<float> shifts{3, num_ops};
};


// the group is a subgroup of the linear group in dim dimensions
struct GroupRepresentation : GL3Group {
    int dim;
    cube<complex> rep{dim, dim, num_ops};
};

struct OrbitalLabels {
    string short_label;
    string latex_label; // $ signs stripped
};

// it is not a subclass of representation,because we don't need
// the original group with the WigD's multiple times in Local Symmetry
struct AnnotatedRepresentation {
    int num_ops;

    int l;
    bool nrel;
    bool num_si;

    int dim; // the representation dimension
    int num_blocks;

    float blocking_tol; // the tolerance used to block-diagonalize the representation
    float G_tolerance; // tolerance of comparing chi^dagger*chi diagonal to num_ops


    cube<complex> rep{dim, dim, num_ops};

    // for each basis index, the block number
    vector<int> blocks{dim};

    matrix<complex> characters{num_blocks, num_ops};
    matrix<complex> chiHchi{num_blocks, num_blocks};


    // Entries of a matrix H such that [H,g]=0 forall g in G, according to Wigner's Theorem.
    // Entries are encoded in the following way:
    // 0       -> the value is actually zero (set only on non-diagonals)
    // 1,2 ... -> this is the number of the irrep for diagonals in irreps
    // -1      -> nonzero on the diagonal, not numbered because it is in a reducible block
    // -2      -> nonzero, because it is between 2 equivalent irreps
    // -3      -> nonzero, because it is between an irrep/reducible block and a reducible block
    //            for example, on the off-diagonal on non-reducible blocks
    matrix<int> H{dim,dim};

    // the relevant axis rotation and basis change matrices:

    // the quantization axis matrix that was used, in xyz basis
    matrix<float> rot{3,3};
    // the Wigner D rotation matrix corresponding to the rotation matrix in real spherical harmonic basis (the native basis)
    // iff J is the native basis, this matrix has dim=2*l, otherwise it has dim=l
    matrix<complex> rotD{dim,dim};

    // the basis change matrix from native to possibly another basis that was used in this representation
    matrix<complex> basisChangeD{dim,dim};

    vector<OrbitalLabels> labels{dim};
};

struct PeriodicSite {
    int site_index;  // original site index
    vector<float> xyz{3};
};


struct LocalSymmetry {
    int site_index;

    // this is the local group, which may be bigger than the point group
    GL3Group local_group;

    // a group representation for each l=1 to 3
    array<AnnotatedRepresentation> annotated_reps_l{extent(0,3)};
};

struct StructLocalSymmetry {
    int num_distinct;
    int num_atoms;

    int num_si;
    int nrel;

    // for each index of distinct atom, as in PyMatGen symmetrized structures
    array<LocalSymmetry> distinct_symm{num_distinct};
};


}

#endif // SYMMETRY_H_INCLUDED
