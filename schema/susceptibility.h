#ifndef SUSCEPTIBILITY_H_INCLUDED
#define SUSCEPTIBILITY_H_INCLUDED

#include "LAPW.h"

namespace susceptibility {

struct LatticeFunction{

    int num_omega; //number fermionic frequencies
    int num_nu; //number bosonic frequencies
    int num_bands;
    int num_si;
    int num_k;
    int num_q;

    vector<float> omega{num_omega}; //list of fermionic frequencies
    vector<float> nu{num_nu}; //list of bosonic frequencies

};

//naming _F(ermion)B(oson)... describes the number of times, their ordering, and their symmetry

struct LatticeGreensFunctions : LatticeFunction{ //G_ij,sigma(q, omega)

    array6<complex> M{num_k, num_si, num_omega, num_bands, num_bands };  // M for "matrix"

};

struct LatticeBubble_at_qnu : LatticeFunction { // Chi_ijkl(q, nu,omega))

    array6<complex> M{num_k, num_omega, num_bands, num_bands, num_bands, num_bands};  // M for "matrix"

};

struct IrreducibleVertex { 

    int num_omega;
    int num_bands;
    int num_kind;

    array5<complex> M{num_omega,num_omega,num_kind,num_bands,num_bands};  // M for "matrix"

};

struct LatticeSusceptibility : LatticeFunction { // Chi_ij(q, nu,omega))

    int num_kind;
    array6<complex> M{num_q, num_nu, num_k, num_kind, num_bands, num_bands};  // this is the remaining bubble part

};


struct ContinuedLatticeSusceptibility : LatticeFunction { // Chi(q, nu,omega))

    int num_kind;
    matrix<complex> M{num_q, num_nu};  // this is the remaining bubble part

};

struct SusceptibilityCalculation{

    string next_todo = ""; 

    int current_nu=0; 
    int current_q=0;

    int num_q=0; 
    int num_nu=0;
    int num_k=0;
    int num_omega=0;
    int num_bands=0;

    //LAPW::KPath qpath;
    //vector<int> qpath_indices{num_q};

};

};



#endif // SUSCEPTIBILITY_H_INCLUDED
